<?php


namespace App\EntityTranslation;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Gedmo\Translatable\Entity\Translation;

class EntityTranslationService
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * EntityTranslationService constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function filtered(array $filters = []): array
    {
        $dql = '
select distinct translation.objectClass, translation.field, translation.content, translation.foreignKey from Gedmo\Translatable\Entity\Translation translation
';
        $query = $this->manager->createQuery();

        $hasWhere = false;

        if (!empty($filters['classFilter'])) {
            $dql .= ($hasWhere ? ' and' : ' where') . ' translation.objectClass = :class';
            $query->setParameter('class', $filters['classFilter']);
            $hasWhere = true;
        }

        if (!empty($filters['foreignKeyFilter'])) {
            $dql .= ($hasWhere ? ' and' : ' where') . ' translation.foreignKey = :foreignKey';
            $query->setParameter('foreignKey', $filters['foreignKeyFilter']);
            $hasWhere = true;
        }

        if (!empty($filters['fieldFilter'])) {
            $dql .= ($hasWhere ? ' and' : ' where') . ' translation.field like :field';
            $query->setParameter('field', "%{$filters['fieldFilter']}%");
        }

        $dql .= ' order by translation.foreignKey';

        $query->setDQL($dql);

        return $query->execute();
    }

    /**
     * @param string $class
     * @param int $foreignKey
     * @param string $field
     * @param $locale
     * @return Translation
     * @throws NonUniqueResultException
     */
    public function find(string $class, int $foreignKey, string $field, $locale): Translation
    {
        $dql = '
select translation from Gedmo\Translatable\Entity\Translation translation
where translation.objectClass = :class and translation.foreignKey = :foreignKey and translation.field = :field and translation.locale = :locale
';
        $query = $this->manager->createQuery()
            ->setParameter('class', $class)
            ->setParameter('foreignKey', $foreignKey)
            ->setParameter('field', $field)
            ->setParameter('locale', $locale)
            ->setMaxResults(1);

        $query->setDQL($dql);

        if( $translation = $query->getOneOrNullResult()) {
            return $translation;
        } else {
            $translation = new Translation();
            $translation->setObjectClass($class)->setForeignKey($foreignKey)->setField($field)->setLocale($locale);

            return $translation;
        }
    }
}