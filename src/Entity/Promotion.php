<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromotionRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @UniqueEntity(fields={"slug", "deletedAt"})
 */
class Promotion implements Searchable
{
    use TimestampableEntity;
    use HasSeoData;
    use HasVisibilityConfiguration;
    use ChecksVisibility;
    use SoftDeleteableEntity;
	use HasImage;
	use TranslatableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $description;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Gedmo\Slug(fields={"title"}, unique=true)
     * @Gedmo\Translatable()
	 */
	private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $featured = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LandingPage")
     */
    private $landingPage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getFeatured(): ?bool
    {
        return $this->featured;
    }

    public function setFeatured(bool $featured): self
    {
        $this->featured = $featured;

        return $this;
    }

    public function text(): string
    {
        return $this->getTitle();
    }

    public function urlContext(): array
    {
        return ['promotions.show', ['slug' => $this->getSlug()]];
    }

    public function getLandingPage(): ?LandingPage
    {
        return $this->landingPage;
    }

    public function setLandingPage(?LandingPage $landingPage): self
    {
        $this->landingPage = $landingPage;

        return $this;
    }
}
