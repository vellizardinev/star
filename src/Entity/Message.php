<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\MessageRepository;

/**
 * @ORM\Entity(repositoryClass=MessageRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Message
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = false;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $updatedBy;

    /**
     * @ORM\Column(type="boolean")
     */
    private $toAllCustomers = false;

    /**
     * @ORM\OneToMany(targetEntity=MessageRecipient::class, mappedBy="message", orphanRemoval=true)
     */
    private $recipients;

    /**
     * @var \App\Entity\MessageReadByRecipient[]
     * @ORM\OneToMany(targetEntity=MessageReadByRecipient::class, mappedBy="message")
     */
    private $messageReadByRecipients;

    public function __construct()
    {
        $this->recipients = new ArrayCollection();
        $this->messageReadByRecipients = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Message
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @param bool $visible
     * @return Message
     */
    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User|null $createdBy
     * @return Message
     */
    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User|null $updatedBy
     * @return Message
     */
    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return Message
     */
    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getToAllCustomers(): ?bool
    {
        return $this->toAllCustomers;
    }

    public function setToAllCustomers(bool $toAllCustomers): self
    {
        $this->toAllCustomers = $toAllCustomers;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateRecipients()
    {
        if ($this->toAllCustomers === true) {
            $this->recipients = new ArrayCollection();
        }
    }

    /**
     * @return Collection|MessageRecipient[]
     */
    public function getRecipients(): Collection
    {
        return $this->recipients;
    }

    public function clearRecipients()
    {
        $this->recipients = new ArrayCollection();
    }

    public function addRecipient(MessageRecipient $recipient): self
    {
        if (!$this->recipients->contains($recipient)) {
            $this->recipients[] = $recipient;
            $recipient->setMessage($this);
        }

        return $this;
    }

    public function removeRecipient(MessageRecipient $recipient): self
    {
        if ($this->recipients->contains($recipient)) {
            $this->recipients->removeElement($recipient);
            // set the owning side to null (unless already changed)
            if ($recipient->getMessage() === $this) {
                $recipient->setMessage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MessageReadByRecipient[]
     */
    public function getMessageReadByRecipients(): Collection
    {
        return $this->messageReadByRecipients;
    }

    public function addMessageReadByRecipient(MessageReadByRecipient $messageReadByRecipient): self
    {
        if (!$this->messageReadByRecipients->contains($messageReadByRecipient)) {
            $this->messageReadByRecipients[] = $messageReadByRecipient;
            $messageReadByRecipient->setMessage($this);
        }

        return $this;
    }

    public function removeMessageReadByRecipient(MessageReadByRecipient $messageReadByRecipient): self
    {
        if ($this->messageReadByRecipients->contains($messageReadByRecipient)) {
            $this->messageReadByRecipients->removeElement($messageReadByRecipient);
            // set the owning side to null (unless already changed)
            if ($messageReadByRecipient->getMessage() === $this) {
                $messageReadByRecipient->setMessage(null);
            }
        }

        return $this;
    }
}
