<?php

namespace App\Entity;

use App\Enum\OfficeTypes;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfficeRepository")
 */
class Office implements Searchable, Translatable
{
    use TimestampableEntity;
    use TranslatableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(pattern="/^0[0-9-\s]+$/")
     */
    private $telephone;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7))
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(-90)
     * @Assert\LessThanOrEqual(90)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7))
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(-180)
     * @Assert\LessThanOrEqual(180)
     */
    private $longitude;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 * @Gedmo\Translatable()
	 */
	private $workingTime;

    /**
     * @ORM\Column(type="boolean")
     */
    private $showOnMap = true;

	/**
	 * @ORM\Column(type="string", length=190)
	 */
	private $type = OfficeTypes::ICREDIT;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude($latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude($longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

	public function getWorkingTime(): ?string
         	{
         		return $this->workingTime;
         	}

	public function setWorkingTime(string $workingTime): self
         	{
         		$this->workingTime = $workingTime;
         
         		return $this;
         	}

    public function text(): string
    {
        return sprintf(
            '%s, %s, %s, Tel.: %s',
            $this->getName(),
            $this->getCity()->getName(),
            strip_tags($this->getAddress()),
            $this->getTelephone() ?? 'n/a'
        );
    }

    public function urlContext(): array
    {
        return ['contact.show'];
    }

    public function getShowOnMap(): ?bool
    {
        return $this->showOnMap;
    }

    public function setShowOnMap(bool $showOnMap): self
    {
        $this->showOnMap = $showOnMap;

        return $this;
    }

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 * @return Office
	 */
	public function setType(string $type): Office
	{
		$this->type = $type;
		return $this;
	}
}
