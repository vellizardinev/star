<?php


namespace App\Entity;


trait MultibyteUppercaseFirst
{
    public function mbUcFirst($str)
    {
        $firstCharacter = mb_strtoupper(mb_substr($str, 0, 1));

        return $firstCharacter . mb_substr($str, 1);
    }
}