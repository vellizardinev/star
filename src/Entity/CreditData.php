<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CreditDataRepository")
 */
class CreditData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $periods;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $payment;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $paymentWithPenalty;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $total;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $totalWithPenalty;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable=true)
     */
    private $interestRateFirstPeriod;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable=true)
     */
    private $interestRateSecondPeriod;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable=true)
     */
    private $aprc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paymentSchedule;

    /**
     * @ORM\ManyToOne(targetEntity=Credit::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $credit;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $creditPeriodId;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $productId;


	public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return CreditData
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPeriods()
    {
        return $this->periods;
    }

    /**
     * @param mixed $periods
     * @return CreditData
     */
    public function setPeriods($periods)
    {
        $this->periods = $periods;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     * @return CreditData
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentWithPenalty()
    {
        return $this->paymentWithPenalty;
    }

    /**
     * @param mixed $paymentWithPenalty
     * @return CreditData
     */
    public function setPaymentWithPenalty($paymentWithPenalty)
    {
        $this->paymentWithPenalty = $paymentWithPenalty;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     * @return CreditData
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalWithPenalty()
    {
        return $this->totalWithPenalty;
    }

    /**
     * @param mixed $totalWithPenalty
     * @return CreditData
     */
    public function setTotalWithPenalty($totalWithPenalty)
    {
        $this->totalWithPenalty = $totalWithPenalty;

        return $this;
    }

    public function getInterestRateFirstPeriod(): ?string
    {
        return $this->interestRateFirstPeriod;
    }

    public function setInterestRateFirstPeriod(?string $interestRateFirstPeriod): self
    {
        $this->interestRateFirstPeriod = $interestRateFirstPeriod;

        return $this;
    }

    public function getInterestRateSecondPeriod(): ?string
    {
        return $this->interestRateSecondPeriod;
    }

    public function setInterestRateSecondPeriod(?string $interestRateSecondPeriod): self
    {
        $this->interestRateSecondPeriod = $interestRateSecondPeriod;

        return $this;
    }

    public function getAprc(): ?string
    {
        return $this->aprc;
    }

    public function setAprc(?string $aprc): self
    {
        $this->aprc = $aprc;

        return $this;
    }

    public function getPaymentSchedule(): ?string
    {
        return $this->paymentSchedule;
    }

    public function setPaymentSchedule(?string $paymentSchedule): self
    {
        $this->paymentSchedule = $paymentSchedule;

        return $this;
    }

    public function getCredit(): ?Credit
    {
        return $this->credit;
    }

    public function setCredit(?Credit $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

	public function getCreditPeriodId(): ?string
	{
		return $this->creditPeriodId;
	}

	public function setCreditPeriodId(?string $creditPeriodId): self
	{
		$this->creditPeriodId = $creditPeriodId;

		return $this;
	}

	public function getProductId(): ?string
	{
		return $this->productId;
	}

	public function setProductId(?string $productId): self
	{
		$this->productId = $productId;

		return $this;
	}

}
