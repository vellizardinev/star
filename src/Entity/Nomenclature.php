<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NomenclatureRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Nomenclature implements Translatable
{
    use TranslatableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $guid;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Translatable()
     * @Assert\NotBlank()
     */
    private $text;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = true;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    public function __construct(?string $guid = null, ?string $text = null, ?string $type = null)
    {
        $this->guid = $guid;
        $this->text = $text;
        $this->type = $type;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function updateVisibility()
    {
    	//TODO update with ukrainian response
        if ($this->text === 'Nie wybrano') {
            $this->visible = false;
        }

        if ($this->text === 'Nieznany') {
            $this->visible = false;
        }
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
