<?php

namespace App\Entity;

use App\File\FileManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image extends File
{
    public function getFilePath(): string
    {
        return FileManager::IMAGES_DIR . '/' . $this->getName();
    }
}
