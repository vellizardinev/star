<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


trait SortableEntity
{
    /**
     * @var integer
     * @Gedmo\SortablePosition()
     * @ORM\Column(type="integer")
     */
    protected $positionIndex;

    /**
     * @return int
     */
    public function getPositionIndex(): int
    {
        return $this->positionIndex;
    }

    /**
     * @param int $positionIndex
     * @return $this
     */
    public function setPositionIndex(int $positionIndex)
    {
        $this->positionIndex = $positionIndex;

        return $this;
    }
}
