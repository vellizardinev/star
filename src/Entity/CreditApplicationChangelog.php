<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class CreditApplicationChangelog
 * @package AppBundle\Entity
 * @ORM\Table(name="credit_application_changelog")
 * @ORM\Entity()
 */
class CreditApplicationChangelog
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

    /**
     * @var string
     * @ORM\Column(name="change_data", type="json", nullable=true)
     */
	private $changeData;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
	private $user;

	/**
	 * @var CreditShortApplication
	 * @ORM\ManyToOne(targetEntity="App\Entity\CreditShortApplication", inversedBy="creditApplicationChangelog")
	 * @ORM\JoinColumn(name="credit_application_id", referencedColumnName="id")
	 */
	private $creditApplication;

	/**
	 * @var DateTime
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	protected $createdAt;

	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * Sets createdAt.
	 *
	 * @param  DateTime $createdAt
	 * @return $this
	 */
	public function setCreatedAt(DateTime $createdAt): CreditApplicationChangelog
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Returns createdAt.
	 *
	 * @return DateTime
	 */
	public function getCreatedAt(): ?DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @return array
	 */
	public function getChangeData(): array
	{
		return $this->changeData;
	}

	/**
	 * @param array $changeData
	 * @return CreditApplicationChangelog
	 */
	public function setChangeData(array $changeData): CreditApplicationChangelog
	{
		$this->changeData = $changeData;
		return $this;
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 * @return CreditApplicationChangelog
	 */
	public function setUser(User $user): CreditApplicationChangelog
	{
		$this->user = $user;
		return $this;
	}

	/**
	 * @return CreditShortApplication
	 */
	public function getCreditApplication(): CreditShortApplication
	{
		return $this->creditApplication;
	}

	/**
	 * @param CreditShortApplication $creditApplication
	 * @return CreditApplicationChangelog
	 */
	public function setCreditApplication(CreditShortApplication $creditApplication): CreditApplicationChangelog
	{
		$this->creditApplication = $creditApplication;
		return $this;
	}
}