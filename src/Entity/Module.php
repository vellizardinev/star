<?php

namespace App\Entity;

use App\Enum\ModuleWidths;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ModuleRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @UniqueEntity(fields={"name"})
 */
class Module
{
    use TimestampableEntity;
    use HasVisibilityConfiguration;
    use ChecksVisibility;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=190, unique=true)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[A-Za-z0-9,\s]+$/")
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $data;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $width = ModuleWidths::STANDARD;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $backgroundColor = '#ffffff';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $backgroundImage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $backgroundImageMobile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType()
    {
        return 'built_in';
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(?string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    public function setBackgroundColor(?string $backgroundColor): self
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    public function getBackgroundImage(): ?string
    {
        return $this->backgroundImage;
    }

    public function setBackgroundImage(?string $backgroundImage): self
    {
        $this->backgroundImage = $backgroundImage;

        return $this;
    }

    public function getBackgroundImageMobile(): ?string
    {
        return $this->backgroundImageMobile;
    }

    public function setBackgroundImageMobile(?string $backgroundImageMobile): self
    {
        $this->backgroundImageMobile = $backgroundImageMobile;

        return $this;
    }
}
