<?php

namespace App\Entity;

use App\Entity\OnlineApplication\OnlineApplication;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FailedOnlineApplicationRequestRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class FailedOnlineApplicationRequest extends HttpRequest
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $application;

    public function getApplication(): ?OnlineApplication
    {
        return $this->application;
    }

    public function setApplication(OnlineApplication $application): self
    {
        $this->application = $application;

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     */
    public function updateSuccess()
    {
        if ($this->getLastResponseCode() >= 300) {
            return;
        }

        $responseData = json_decode($this->getLastResponse(), true);

        if (is_array($responseData) === false) {
            $this->setSuccess(false);

            return;
        }

        if ($responseData['RequestResult'] === 0 && !empty($responseData['Result'])) {
            $this->setSuccess(true);
        }
    }
}
