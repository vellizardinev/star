<?php

namespace App\Entity;

use App\Enum\CreditApplicationStatuses;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use ReflectionException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CreditApplicationRepository")
 */
class CreditShortApplication
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=190, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type("\DateTimeInterface")
     * @Assert\LessThan("today")
     */
    private $birthdayDate;

    /**
     * @ORM\Column(type="string", length=190, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(min="9", max="9", exactMessage="digits_required_9")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_9")
     */
    private $telephone;

    /**
     * @ORM\ManyToOne(targetEntity=Credit::class)
     */
    private $credit;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LandingPage")
     */
    private $landing;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Affiliate")
     */
    private $affiliate;

    /**
     * @ORM\Column(type="string", length=190)
     */
    private $status = CreditApplicationStatuses::UNPROCESSED;

    /**
     * @var CreditApplicationChangelog
     * @ORM\OneToMany(targetEntity=CreditApplicationChangelog::class, mappedBy="creditApplication")
     * @ORM\OrderBy({"createdAt"="desc"})
     */
    private $creditApplicationChangelog;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $personalIdentificationNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $preferredCallDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $level = 'Seles_Partner';

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sentToErp = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $apiResponse;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $jsonData;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRefinance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $source;

    public function __construct()
    {
        $this->creditApplicationChangelog = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return CreditShortApplication
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthdayDate()
    {
        return $this->birthdayDate;
    }

    /**
     * @param mixed $birthdayDate
     * @return CreditShortApplication
     */
    public function setBirthdayDate($birthdayDate): self
    {
        $this->birthdayDate = $birthdayDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    /**
     * @param string|null $telephone
     * @return CreditShortApplication
     */
    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getCredit(): ?Credit
    {
        return $this->credit;
    }

    public function setCredit(?Credit $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(?string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getLanding(): ?LandingPage
    {
        return $this->landing;
    }

    public function setLanding(?LandingPage $landing): self
    {
        $this->landing = $landing;

        return $this;
    }

    public function getAffiliate(): ?Affiliate
    {
        return $this->affiliate;
    }

    public function setAffiliate(?Affiliate $affiliate): self
    {
        $this->affiliate = $affiliate;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return $this
     * @throws ReflectionException
     */
    public function setStatus(?string $status): self
    {
        if (CreditApplicationStatuses::hasValue($status)) {
            $this->status = $status;
        }

        return $this;
    }

    public function getCreditApplicationChangelog()
    {
        return $this->creditApplicationChangelog;
    }

    /**
     * @param CreditApplicationChangelog $creditApplicationChangelog
     * @return CreditShortApplication
     */
    public function setCreditApplicationChangelog(CreditApplicationChangelog $creditApplicationChangelog
    ): CreditShortApplication {
        $this->creditApplicationChangelog = $creditApplicationChangelog;

        return $this;
    }

    /**
     * @param CreditApplicationChangelog $creditApplicationChangelog
     * @return CreditShortApplication
     */
    public function addCreditApplicationChangelog(CreditApplicationChangelog $creditApplicationChangelog
    ): CreditShortApplication {
        $this->creditApplicationChangelog[] = $creditApplicationChangelog;

        return $this;
    }

    /**
     * @return CreditApplicationChangelog
     */
    public function getLastChangelog(): ?CreditApplicationChangelog
    {
        if ($this->getCreditApplicationChangelog()->count() > 0) {
            return $this->getCreditApplicationChangelog()->first();
        }

        return null;
    }

    public function getPersonalIdentificationNumber(): ?string
    {
        return $this->personalIdentificationNumber;
    }

    public function setPersonalIdentificationNumber(?string $personalIdentificationNumber): self
    {
        $this->personalIdentificationNumber = $personalIdentificationNumber;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPreferredCallDate(): ?DateTimeInterface
    {
        return $this->preferredCallDate;
    }

    public function setPreferredCallDate(?DateTimeInterface $preferredCallDate): self
    {
        $this->preferredCallDate = $preferredCallDate;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getSentToErp(): ?bool
    {
        return $this->sentToErp;
    }

    public function setSentToErp(?bool $sentToErp): self
    {
        $this->sentToErp = $sentToErp;

        return $this;
    }

    public function getApiResponse(): ?string
    {
        return $this->apiResponse;
    }

    public function setApiResponse(?string $apiResponse): self
    {
        $this->apiResponse = $apiResponse;

        return $this;
    }

    public function getJsonData(): ?string
    {
        return $this->jsonData;
    }

    public function setJsonData(?string $jsonData): self
    {
        $this->jsonData = $jsonData;

        return $this;
    }

    public function getIsRefinance(): ?bool
    {
        return $this->isRefinance;
    }

    public function setIsRefinance(?bool $isRefinance): self
    {
        $this->isRefinance = $isRefinance;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }
}
