<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DepartmentRepository")
 */
class Department implements Searchable, Translatable
{
    use TimestampableEntity;
    use SortableEntity;
    use TranslatableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $director;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable()
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = true;

    /**
     * @ORM\OneToMany(targetEntity=JobOpening::class, mappedBy="department", orphanRemoval=true)
     */
    private $jobOpenings;

	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Image", cascade={"persist", "remove"})
	 */
	private $image;

    public function __construct()
    {
        $this->jobOpenings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDirector(): ?string
    {
        return $this->director;
    }

    public function setDirector(?string $director): self
    {
        $this->director = $director;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

	public function getImage(): ?Image
	{
		return $this->image;
	}

	public function setImage(?Image $image): self
	{
		$this->image = $image;

		return $this;
	}

    /**
     * @return Collection|JobOpening[]
     */
    public function getJobOpenings(): Collection
    {
        return $this->jobOpenings;
    }

    public function addJobOpening(JobOpening $jobOpening): self
    {
        if (!$this->jobOpenings->contains($jobOpening)) {
            $this->jobOpenings[] = $jobOpening;
            $jobOpening->setDepartment($this);
        }

        return $this;
    }

    public function removeJobOpening(JobOpening $jobOpening): self
    {
        if ($this->jobOpenings->contains($jobOpening)) {
            $this->jobOpenings->removeElement($jobOpening);
            // set the owning side to null (unless already changed)
            if ($jobOpening->getDepartment() === $this) {
                $jobOpening->setDepartment(null);
            }
        }

        return $this;
    }

    public function text(): string
    {
        return $this->getName();
    }

    public function urlContext(): array
    {
        return ['static_pages.home_page'];
    }
}
