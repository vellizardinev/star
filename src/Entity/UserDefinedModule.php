<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserDefinedModuleRepository")
 */
class UserDefinedModule extends Module
{
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getType()
    {
        return 'user_defined';
    }
}
