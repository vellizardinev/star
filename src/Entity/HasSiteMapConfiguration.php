<?php


namespace App\Entity;


trait HasSiteMapConfiguration
{
    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $includeInSiteMap = false;

    public function getIncludeInSiteMap(): bool
    {
        return $this->includeInSiteMap;
    }

    public function setIncludeInSiteMap(bool $includeInSiteMap): self
    {
        $this->includeInSiteMap = $includeInSiteMap;

        return $this;
    }
}