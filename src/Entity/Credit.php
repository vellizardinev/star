<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CreditRepository")
 */
class Credit implements Searchable
{
    use TimestampableEntity;
    use SortableEntity;
    use HasSeoData;
    use HasVisibilityConfiguration;
    use TranslatableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable()
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable()
     */
    private $fullDescription;

    /**
     * @ORM\Column(type="integer")
     * @Assert\GreaterThan(0)
     * @Assert\LessThanOrEqual(1000000)
     */
    private $maxAmount;

    /**
     * @ORM\OneToOne(targetEntity=Image::class, cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable()
     */
    private $parametersDescription;

    /**
     * @ORM\Column(type="boolean")
     */
    private $allowOnlineApplication = true;

	/**
	 * @Gedmo\Slug(fields={"title"}, unique=true)
	 * @ORM\Column(type="string", length=255)
     * @Gedmo\Translatable()
	 */
	private $slug;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $requirements;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $paymentSchedule;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pensioner = false;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $defaultAmount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getFullDescription(): ?string
    {
        return $this->fullDescription;
    }

    public function setFullDescription(?string $fullDescription): self
    {
        $this->fullDescription = $fullDescription;

        return $this;
    }

    public function getMaxAmount(): ?int
    {
        return $this->maxAmount;
    }

    public function setMaxAmount(int $maxAmount): self
    {
        $this->maxAmount = $maxAmount;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getParametersDescription(): ?string
    {
        return $this->parametersDescription;
    }

    public function setParametersDescription(?string $parametersDescription): self
    {
        $this->parametersDescription = $parametersDescription;

        return $this;
    }

    public function getAllowOnlineApplication(): ?bool
    {
        return $this->allowOnlineApplication;
    }

    public function setAllowOnlineApplication(bool $allowOnlineApplication): self
    {
        $this->allowOnlineApplication = $allowOnlineApplication;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getRequirements(): ?string
    {
        return $this->requirements;
    }

    public function setRequirements(?string $requirements): self
    {
        $this->requirements = $requirements;

        return $this;
    }

    public function getPaymentSchedule(): ?string
    {
        return $this->paymentSchedule;
    }

    public function setPaymentSchedule(string $paymentSchedule): self
    {
        $this->paymentSchedule = $paymentSchedule;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPensioner(): ?bool
    {
        return $this->pensioner;
    }

    public function setPensioner(bool $pensioner): self
    {
        $this->pensioner = $pensioner;

        return $this;
    }

    public function text(): string
    {
        return $this->getTitle();
    }

    public function urlContext(): array
    {
	    return ['credits.show', ['slug' => $this->getSlug()]];
    }

    public function getDefaultAmount(): ?string
    {
        return $this->defaultAmount;
    }

    public function setDefaultAmount(?string $defaultAmount): self
    {
        $this->defaultAmount = $defaultAmount;

        return $this;
    }
}
