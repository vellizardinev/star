<?php


namespace App\Entity;


use Carbon\Carbon;

trait ChecksVisibility
{
    public function isVisible(): bool
    {
        $now = Carbon::now();

        if ($this->startDate && $now < $this->startDate) {
            return false;
        }

        if ($this->endDate && $now > $this->endDate) {
            return false;
        }

        return true;
    }
}