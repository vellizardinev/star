<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 */
class Document extends File
{
	/**
	 * @ORM\Column(type="boolean")
	 */
	private $isSentToErp = false;

	public function getIsSentToErp(): ?bool
	{
		return $this->isSentToErp;
	}

	public function setIsSentToErp(bool $isSentToErp): self
	{
		$this->isSentToErp = $isSentToErp;

		return $this;
	}
}
