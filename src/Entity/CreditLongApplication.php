<?php

namespace App\Entity;


use App\Enum\EmploymentTypes;
use App\Enum\MethodsReceivingSalary;
use App\Enum\MethodsToReceiveCredit;
use App\Enum\PassportTypes;
use App\Enum\PensionCertificateTypes;
use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CreditLongApplicationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CreditLongApplication
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="9", max="9")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_9")
     */
    private $mobileTelephone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telephoneForNewContracts;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\Email()
     * @Assert\Length(min="0", max="255")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="10", max="10")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_10")
     */
    private $personalIdentificationNumber;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="0", max="255")
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $period;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $instalmentAmount;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="0", max="255")
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="0", max="255")
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $middleName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="0", max="255")
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $idCardNumber;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @Assert\Type("\DateTimeInterface")
     */
    private $idCardIssuedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max="255")
     */
    private $idCardIssuedBy;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    private $currentAddressSameAsPermanentAddress = true;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $companyPosition;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="9", max="9")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_9")
     */
    private $companyTelephone;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Length(min="1", max="3")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_max_3")
     */
    private $workExperienceCurrentEmployer;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     * @Assert\Length(min="1", max="3")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_max_3")
     */
    private $workExperienceTotal;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $monthlyIncome;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $otherIncome;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $howSalaryIsReceived;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesElectricityAndWater;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesInternetAndTv;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesTelephone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $typeOfHome;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disadvantaged = false;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $industry;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $employmentType;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $typeOfWork;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $education;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $purposeOfCredit;

    /**
     * @ORM\Column(type="boolean")
     */
    private $wantsDiscretion = false;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Assert\Range(min="0", max="20")
     */
    private $householdSize;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $familyStatus;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $howShouldMoneyBeReceived = MethodsToReceiveCredit::CASH;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPoliticalPerson = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isRelatedToPoliticalPerson = false;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $countryOfBirth = 'Ukraine';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nationality = 'Ukrainian';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $countryOfResidence = 'Ukraine';

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Positive()
     * @Assert\Range(min="1", max="31")
     */
    private $dayForReceivingSalary;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $frequencyOfReceivingSalary;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="16", max="16")
     * @Assert\Regex(pattern="/^\d+$/", message="telephone.only_numbers_allowed")
     */
    private $iban;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesFoodAndClothes;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesKids;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesRent;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesTransport;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesDrinksCigarettes;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $householdExpensesOtherCredits;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesOtherCreditsInstalmentAmount;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesHeating;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesOther;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expirationDateOfEmploymentContract;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $foundOutAboutTheCompanyFrom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $paybackCreditAmountMethod = 'Cash';

    /**
     * @ORM\Column(type="datetime")
     */
    private $livingOnAddressFromDate;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $bonusIncome = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $socialIncome = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $rentIncome = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $civilContractIncome = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $pension = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $incomesTotal;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $expensesTotal;

    /**
     * @ORM\Column(type="text")
     */
    private $currentAddressArea;

    /**
     * @ORM\Column(type="text")
     */
    private $currentAddressMunicipality;

    /**
     * @ORM\Column(type="text")
     */
    private $currentAddressCity;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(min="5", max="5")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_5")
     */
    private $currentAddressPostCode;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $currentAddressNeighbourhood;

    /**
     * @ORM\Column(type="text")
     */
    private $currentAddressBlockNumber;

    /**
     * @ORM\Column(type="text")
     */
    private $currentAddressStreet;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $currentAddressEntrance;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\PositiveOrZero()
     * @Assert\Range(min="0", max="99")
     */
    private $currentAddressFloor;

    /**
     * @ORM\Column(type="text")
     */
    private $currentAddressApartment;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $permanentAddressArea;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $permanentAddressMunicipality;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $permanentAddressCity;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(min="5", max="5")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_5")
     */
    private $permanentAddressPostCode;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $permanentAddressNeighbourhood;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $permanentAddressBlockNumber;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $permanentAddressStreet;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $permanentAddressEntrance;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\PositiveOrZero()
     * @Assert\Range(min="0", max="99")
     */
    private $permanentAddressFloor;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $permanentAddressApartment;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $workAddressArea;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $workAddressMunicipality;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $workAddressCity;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $workAddressPostCode;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $workAddressNeighbourhood;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $workAddressBlockNumber;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $workAddressStreet;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $workAddressEntrance;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\PositiveOrZero()
     * @Assert\Range(min="0", max="99")
     */
    private $workAddressFloor;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $workAddressApartment;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $contactPersonFirstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $contactPersonMiddleName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $contactPersonLastName;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     */
    private $contactPersonDateOfBirth;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="9", max="9")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_9")
     */
    private $contactPersonMobileTelephone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactPersonRelationship;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hasProperty;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasCar;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Positive()
     */
    private $numberOfCars;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $carYearOfManufacture;

    /**
     * @ORM\ManyToMany(targetEntity=Document::class)
     * @ORM\JoinTable(name="credit_long_application_files",
     *      joinColumns={@ORM\JoinColumn(name="credit_application_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $files;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $bankCardExpirationDate;

    private $bankCardExpirationDateMonth;

    private $bankCardExpirationDateYear;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $passportType;

    /** @var UploadedFile */
    public $oldStylePassportFirstPageUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $oldStylePassportFirstPage;

    /** @var UploadedFile */
    public $oldStylePassportSecondPageUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $oldStylePassportSecondPage;

    /** @var UploadedFile */
    public $oldStylePassportPhotoAt25Uploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $oldStylePassportPhotoAt25;

    /** @var UploadedFile */
    public $oldStylePassportPastedPhotoAt25Uploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $oldStylePassportPastedPhotoAt25;

    /** @var UploadedFile */
    public $oldStylePassportPhotoAt45Uploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $oldStylePassportPhotoAt45;

    /** @var UploadedFile */
    public $oldStylePassportPastedPhotoAt45Uploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $oldStylePassportPastedPhotoAt45;

    /** @var UploadedFile */
    public $oldStylePassportProofOfResidenceUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $oldStylePassportProofOfResidence;

    /** @var UploadedFile */
    public $oldStylePassportSelfieWithPassportUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $oldStylePassportSelfieWithPassport;

    /** @var UploadedFile */
    public $newStylePassportPhotoPageUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $newStylePassportPhotoPage;

    /** @var UploadedFile */
    public $newStylePassportValidityPageUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $newStylePassportValidityPage;

    /** @var UploadedFile */
    public $newStylePassportRegistrationAppendixUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $newStylePassportRegistrationAppendix;

    /** @var UploadedFile */
    public $newStylePassportSelfieWithIdCardUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $newStylePassportSelfieWithIdCard;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pensionCertificateType;

    /** @var UploadedFile */
    public $oldStylePensionCertificateFirstSpreadUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $oldStylePensionCertificateFirstSpread;

    /** @var UploadedFile */
    public $oldStylePensionCertificateValidityRecordUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $oldStylePensionCertificateValidityRecord;

    /** @var UploadedFile */
    public $newStylePensionCertificatePhotoAndValiditySpreadUploaded;

    /**
     * @ORM\OneToOne(targetEntity=Document::class, cascade={"persist", "remove"})
     */
    private $newStylePensionCertificatePhotoAndValiditySpread;

    /**
     * CreditApplication constructor.
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMobileTelephone(): ?string
    {
        return $this->mobileTelephone;
    }

    public function setMobileTelephone(string $mobileTelephone): self
    {
        $this->mobileTelephone = $mobileTelephone;
        $this->telephoneForNewContracts = $mobileTelephone;

        return $this;
    }

    public function getTelephoneForNewContracts(): ?string
    {
        return $this->telephoneForNewContracts;
    }

    public function setTelephoneForNewContracts(?string $telephoneForNewContracts): self
    {
        $this->telephoneForNewContracts = $telephoneForNewContracts;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPersonalIdentificationNumber(): ?string
    {
        return $this->personalIdentificationNumber;
    }

    public function setPersonalIdentificationNumber(?string $personalIdentificationNumber): self
    {
        $this->personalIdentificationNumber = $personalIdentificationNumber;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(?string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getProduct(): ?string
    {
        return $this->product;
    }

    public function setProduct(?string $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getPeriod(): ?int
    {
        return $this->period;
    }

    public function setPeriod(?int $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getInstalmentAmount(): ?string
    {
        return $this->instalmentAmount;
    }

    public function setInstalmentAmount(?string $instalmentAmount): self
    {
        $this->instalmentAmount = $instalmentAmount;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function setMiddleName(?string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getIdCardNumber(): ?string
    {
        return $this->idCardNumber;
    }

    public function setIdCardNumber(?string $idCardNumber): self
    {
        $this->idCardNumber = $idCardNumber;

        return $this;
    }

    public function getIdCardIssuedAt(): ?DateTimeInterface
    {
        return $this->idCardIssuedAt;
    }

    public function setIdCardIssuedAt(?DateTimeInterface $idCardIssuedAt): self
    {
        $this->idCardIssuedAt = $idCardIssuedAt;

        return $this;
    }

    public function getIdCardIssuedBy(): ?string
    {
        return $this->idCardIssuedBy;
    }

    public function setIdCardIssuedBy(?string $idCardIssuedBy): self
    {
        $this->idCardIssuedBy = $idCardIssuedBy;

        return $this;
    }

    public function getCurrentAddressSameAsPermanentAddress(): ?bool
    {
        return $this->currentAddressSameAsPermanentAddress;
    }

    public function setCurrentAddressSameAsPermanentAddress(?bool $currentAddressSameAsPermanentAddress): self
    {
        $this->currentAddressSameAsPermanentAddress = $currentAddressSameAsPermanentAddress;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getCompanyTelephone(): ?string
    {
        return $this->companyTelephone;
    }

    public function setCompanyTelephone(?string $companyTelephone): self
    {
        $this->companyTelephone = $companyTelephone;

        return $this;
    }

    public function getWorkExperienceCurrentEmployer(): ?int
    {
        return $this->workExperienceCurrentEmployer;
    }

    public function setWorkExperienceCurrentEmployer(?int $workExperienceCurrentEmployer): self
    {
        $this->workExperienceCurrentEmployer = $workExperienceCurrentEmployer;

        return $this;
    }

    public function getWorkExperienceTotal(): ?int
    {
        return $this->workExperienceTotal;
    }

    public function setWorkExperienceTotal(?int $workExperienceTotal): self
    {
        $this->workExperienceTotal = $workExperienceTotal;

        return $this;
    }

    public function getMonthlyIncome(): ?string
    {
        return $this->monthlyIncome;
    }

    public function setMonthlyIncome(?string $monthlyIncome): self
    {
        $this->monthlyIncome = $monthlyIncome;

        return $this;
    }

    public function getOtherIncome(): ?string
    {
        return $this->otherIncome;
    }

    public function setOtherIncome(?string $otherIncome): self
    {
        $this->otherIncome = $otherIncome;

        return $this;
    }

    public function getHowSalaryIsReceived(): ?string
    {
        return $this->howSalaryIsReceived;
    }

    public function setHowSalaryIsReceived(?string $howSalaryIsReceived): self
    {
        $this->howSalaryIsReceived = $howSalaryIsReceived;

        return $this;
    }

    public function getHouseholdExpensesElectricityAndWater(): ?float
    {
        return $this->householdExpensesElectricityAndWater;
    }

    public function setHouseholdExpensesElectricityAndWater(?float $householdExpensesElectricityAndWater): self
    {
        $this->householdExpensesElectricityAndWater = $householdExpensesElectricityAndWater;

        return $this;
    }

    public function getHouseholdExpensesInternetAndTv(): ?float
    {
        return $this->householdExpensesInternetAndTv;
    }

    public function setHouseholdExpensesInternetAndTv(?float $householdExpensesInternetAndTv): self
    {
        $this->householdExpensesInternetAndTv = $householdExpensesInternetAndTv;

        return $this;
    }

    public function getHouseholdExpensesTelephone(): ?float
    {
        return $this->householdExpensesTelephone;
    }

    public function setHouseholdExpensesTelephone(?float $householdExpensesTelephone): self
    {
        $this->householdExpensesTelephone = $householdExpensesTelephone;

        return $this;
    }

    public function getTypeOfHome(): ?string
    {
        return $this->typeOfHome;
    }

    public function setTypeOfHome(?string $typeOfHome): self
    {
        $this->typeOfHome = $typeOfHome;

        return $this;
    }

    public function getDisadvantaged(): ?bool
    {
        return $this->disadvantaged;
    }

    public function setDisadvantaged(?bool $disadvantaged): self
    {
        $this->disadvantaged = $disadvantaged;

        return $this;
    }

    public function getIndustry(): ?string
    {
        return $this->industry;
    }

    public function setIndustry(?string $industry): self
    {
        $this->industry = $industry;

        return $this;
    }

    public function getEmploymentType(): ?string
    {
        return $this->employmentType;
    }

    public function setEmploymentType(?string $employmentType): self
    {
        $this->employmentType = $employmentType;

        return $this;
    }

    public function getTypeOfWork(): ?string
    {
        return $this->typeOfWork;
    }

    public function setTypeOfWork(?string $typeOfWork): self
    {
        $this->typeOfWork = $typeOfWork;

        return $this;
    }

    public function getEducation(): ?string
    {
        return $this->education;
    }

    public function setEducation(?string $education): self
    {
        $this->education = $education;

        return $this;
    }

    public function getPurposeOfCredit(): ?string
    {
        return $this->purposeOfCredit;
    }

    public function setPurposeOfCredit(?string $purposeOfCredit): self
    {
        $this->purposeOfCredit = $purposeOfCredit;

        return $this;
    }

    public function getWantsDiscretion(): ?bool
    {
        return $this->wantsDiscretion;
    }

    public function setWantsDiscretion(?bool $wantsDiscretion): self
    {
        $this->wantsDiscretion = $wantsDiscretion;

        return $this;
    }

    public function getHouseholdSize(): ?int
    {
        return $this->householdSize;
    }

    public function setHouseholdSize(?int $householdSize): self
    {
        $this->householdSize = $householdSize;

        return $this;
    }

    public function getFamilyStatus(): ?string
    {
        return $this->familyStatus;
    }

    public function setFamilyStatus(?string $familyStatus): self
    {
        $this->familyStatus = $familyStatus;

        return $this;
    }

    public function getHowShouldMoneyBeReceived(): ?string
    {
        return $this->howShouldMoneyBeReceived;
    }

    public function setHowShouldMoneyBeReceived(?string $howShouldMoneyBeReceived): self
    {
        $this->howShouldMoneyBeReceived = $howShouldMoneyBeReceived;

        return $this;
    }

    public function getIsPoliticalPerson(): ?bool
    {
        return $this->isPoliticalPerson;
    }

    public function setIsPoliticalPerson(?bool $isPoliticalPerson): self
    {
        $this->isPoliticalPerson = $isPoliticalPerson;

        return $this;
    }

    public function getIsRelatedToPoliticalPerson(): ?bool
    {
        return $this->isRelatedToPoliticalPerson;
    }

    public function setIsRelatedToPoliticalPerson(?bool $isRelatedToPoliticalPerson): self
    {
        $this->isRelatedToPoliticalPerson = $isRelatedToPoliticalPerson;

        return $this;
    }

    public function getCountryOfBirth(): ?string
    {
        return $this->countryOfBirth;
    }

    public function setCountryOfBirth(?string $countryOfBirth): self
    {
        $this->countryOfBirth = $countryOfBirth;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(?string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getDayForReceivingSalary(): ?int
    {
        return $this->dayForReceivingSalary;
    }

    public function setDayForReceivingSalary(?int $dayForReceivingSalary): self
    {
        $this->dayForReceivingSalary = $dayForReceivingSalary;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getHouseholdExpensesFoodAndClothes(): ?string
    {
        return $this->householdExpensesFoodAndClothes;
    }

    public function setHouseholdExpensesFoodAndClothes(?string $householdExpensesFoodAndClothes): self
    {
        $this->householdExpensesFoodAndClothes = $householdExpensesFoodAndClothes;

        return $this;
    }

    public function getHouseholdExpensesOther(): ?string
    {
        return $this->householdExpensesOther;
    }

    public function setHouseholdExpensesOther(?string $householdExpensesOther): self
    {
        $this->householdExpensesOther = $householdExpensesOther;

        return $this;
    }

    public function getExpirationDateOfEmploymentContract(): ?DateTimeInterface
    {
        return $this->expirationDateOfEmploymentContract;
    }

    public function setExpirationDateOfEmploymentContract(?DateTimeInterface $expirationDateOfEmploymentContract): self
    {
        $this->expirationDateOfEmploymentContract = $expirationDateOfEmploymentContract;

        return $this;
    }

    public function getFoundOutAboutTheCompanyFrom(): ?string
    {
        return $this->foundOutAboutTheCompanyFrom;
    }

    public function setFoundOutAboutTheCompanyFrom(?string $foundOutAboutTheCompanyFrom): self
    {
        $this->foundOutAboutTheCompanyFrom = $foundOutAboutTheCompanyFrom;

        return $this;
    }

    public function getPaybackCreditAmountMethod(): ?string
    {
        return $this->paybackCreditAmountMethod;
    }

    public function setPaybackCreditAmountMethod(?string $paybackCreditAmountMethod): self
    {
        $this->paybackCreditAmountMethod = $paybackCreditAmountMethod;

        return $this;
    }

    public function getLivingOnAddressFromDate(): ?DateTimeInterface
    {
        return $this->livingOnAddressFromDate;
    }

    public function setLivingOnAddressFromDate(?DateTimeInterface $livingOnAddressFromDate): self
    {
        $this->livingOnAddressFromDate = $livingOnAddressFromDate;

        return $this;
    }

    public function getBonusIncome(): ?string
    {
        return $this->bonusIncome;
    }

    public function setBonusIncome(?string $bonusIncome): self
    {
        $this->bonusIncome = $bonusIncome;

        return $this;
    }

    public function getSocialIncome(): ?string
    {
        return $this->socialIncome;
    }

    public function setSocialIncome(?string $socialIncome): self
    {
        $this->socialIncome = $socialIncome;

        return $this;
    }

    public function getRentIncome(): ?string
    {
        return $this->rentIncome;
    }

    public function setRentIncome(?string $rentIncome): self
    {
        $this->rentIncome = $rentIncome;

        return $this;
    }

    public function getCivilContractIncome(): ?string
    {
        return $this->civilContractIncome;
    }

    public function setCivilContractIncome(?string $civilContractIncome): self
    {
        $this->civilContractIncome = $civilContractIncome;

        return $this;
    }

    public function getPension(): ?string
    {
        return $this->pension;
    }

    public function setPension(?string $pension): self
    {
        $this->pension = $pension;

        return $this;
    }

    public function getIncomesTotal(): ?string
    {
        return $this->incomesTotal;
    }

    public function setIncomesTotal(string $incomesTotal): self
    {
        $this->incomesTotal = $incomesTotal;

        return $this;
    }

    public function getExpensesTotal(): ?string
    {
        return $this->expensesTotal;
    }

    public function setExpensesTotal(?string $expensesTotal): self
    {
        $this->expensesTotal = $expensesTotal;

        return $this;
    }

    public function getCurrentAddressArea(): ?string
    {
        return $this->currentAddressArea;
    }

    public function setCurrentAddressArea(?string $currentAddressArea): self
    {
        $this->currentAddressArea = $currentAddressArea;

        return $this;
    }

    public function getCurrentAddressMunicipality(): ?string
    {
        return $this->currentAddressMunicipality;
    }

    public function setCurrentAddressMunicipality(?string $currentAddressMunicipality): self
    {
        $this->currentAddressMunicipality = $currentAddressMunicipality;

        return $this;
    }

    public function getCurrentAddressCity(): ?string
    {
        return $this->currentAddressCity;
    }

    public function setCurrentAddressCity(?string $currentAddressCity): self
    {
        $this->currentAddressCity = $currentAddressCity;

        return $this;
    }

    public function getCurrentAddressBlockNumber(): ?string
    {
        return $this->currentAddressBlockNumber;
    }

    public function setCurrentAddressBlockNumber(?string $currentAddressBlockNumber): self
    {
        $this->currentAddressBlockNumber = $currentAddressBlockNumber;

        return $this;
    }

    public function getCurrentAddressStreet(): ?string
    {
        return $this->currentAddressStreet;
    }

    public function setCurrentAddressStreet(?string $currentAddressStreet): self
    {
        $this->currentAddressStreet = $currentAddressStreet;

        return $this;
    }

    public function getCurrentAddressEntrance(): ?string
    {
        return $this->currentAddressEntrance;
    }

    public function setCurrentAddressEntrance(?string $currentAddressEntrance): self
    {
        $this->currentAddressEntrance = $currentAddressEntrance;

        return $this;
    }

    public function getCurrentAddressFloor(): ?string
    {
        return $this->currentAddressFloor;
    }

    public function setCurrentAddressFloor(?string $currentAddressFloor): self
    {
        $this->currentAddressFloor = $currentAddressFloor;

        return $this;
    }

    public function getCurrentAddressApartment(): ?string
    {
        return $this->currentAddressApartment;
    }

    public function setCurrentAddressApartment(?string $currentAddressApartment): self
    {
        $this->currentAddressApartment = $currentAddressApartment;

        return $this;
    }

    public function getPermanentAddressArea(): ?string
    {
        return $this->permanentAddressArea;
    }

    public function setPermanentAddressArea(?string $permanentAddressArea): self
    {
        $this->permanentAddressArea = $permanentAddressArea;

        return $this;
    }

    public function getPermanentAddressMunicipality(): ?string
    {
        return $this->permanentAddressMunicipality;
    }

    public function setPermanentAddressMunicipality(?string $permanentAddressMunicipality): self
    {
        $this->permanentAddressMunicipality = $permanentAddressMunicipality;

        return $this;
    }

    public function getPermanentAddressCity(): ?string
    {
        return $this->permanentAddressCity;
    }

    public function setPermanentAddressCity(?string $permanentAddressCity): self
    {
        $this->permanentAddressCity = $permanentAddressCity;

        return $this;
    }

    public function getPermanentAddressBlockNumber(): ?string
    {
        return $this->permanentAddressBlockNumber;
    }

    public function setPermanentAddressBlockNumber(?string $permanentAddressBlockNumber): self
    {
        $this->permanentAddressBlockNumber = $permanentAddressBlockNumber;

        return $this;
    }

    public function getPermanentAddressStreet(): ?string
    {
        return $this->permanentAddressStreet;
    }

    public function setPermanentAddressStreet(?string $permanentAddressStreet): self
    {
        $this->permanentAddressStreet = $permanentAddressStreet;

        return $this;
    }

    public function getPermanentAddressEntrance(): ?string
    {
        return $this->permanentAddressEntrance;
    }

    public function setPermanentAddressEntrance(?string $permanentAddressEntrance): self
    {
        $this->permanentAddressEntrance = $permanentAddressEntrance;

        return $this;
    }

    public function getPermanentAddressFloor(): ?string
    {
        return $this->permanentAddressFloor;
    }

    public function setPermanentAddressFloor(?string $permanentAddressFloor): self
    {
        $this->permanentAddressFloor = $permanentAddressFloor;

        return $this;
    }

    public function getPermanentAddressApartment(): ?string
    {
        return $this->permanentAddressApartment;
    }

    public function setPermanentAddressApartment(?string $permanentAddressApartment): self
    {
        $this->permanentAddressApartment = $permanentAddressApartment;

        return $this;
    }

    public function getWorkAddressArea(): ?string
    {
        return $this->workAddressArea;
    }

    public function setWorkAddressArea(?string $workAddressArea): self
    {
        $this->workAddressArea = $workAddressArea;

        return $this;
    }

    public function getWorkAddressMunicipality(): ?string
    {
        return $this->workAddressMunicipality;
    }

    public function setWorkAddressMunicipality(?string $workAddressMunicipality): self
    {
        $this->workAddressMunicipality = $workAddressMunicipality;

        return $this;
    }

    public function getWorkAddressCity(): ?string
    {
        return $this->workAddressCity;
    }

    public function setWorkAddressCity(?string $workAddressCity): self
    {
        $this->workAddressCity = $workAddressCity;

        return $this;
    }

    public function getWorkAddressBlockNumber(): ?string
    {
        return $this->workAddressBlockNumber;
    }

    public function setWorkAddressBlockNumber(?string $workAddressBlockNumber): self
    {
        $this->workAddressBlockNumber = $workAddressBlockNumber;

        return $this;
    }

    public function getWorkAddressStreet(): ?string
    {
        return $this->workAddressStreet;
    }

    public function setWorkAddressStreet(?string $workAddressStreet): self
    {
        $this->workAddressStreet = $workAddressStreet;

        return $this;
    }

    public function getWorkAddressEntrance(): ?string
    {
        return $this->workAddressEntrance;
    }

    public function setWorkAddressEntrance(?string $workAddressEntrance): self
    {
        $this->workAddressEntrance = $workAddressEntrance;

        return $this;
    }

    public function getWorkAddressFloor(): ?string
    {
        return $this->workAddressFloor;
    }

    public function setWorkAddressFloor(?string $workAddressFloor): self
    {
        $this->workAddressFloor = $workAddressFloor;

        return $this;
    }

    public function getWorkAddressApartment(): ?string
    {
        return $this->workAddressApartment;
    }

    public function setWorkAddressApartment(?string $workAddressApartment): self
    {
        $this->workAddressApartment = $workAddressApartment;

        return $this;
    }

    public function getCountryOfResidence(): string
    {
        return $this->countryOfResidence;
    }

    public function setCountryOfResidence(string $countryOfResidence): self
    {
        $this->countryOfResidence = $countryOfResidence;

        return $this;
    }

    public function getCurrentAddressPostCode(): ?string
    {
        return $this->currentAddressPostCode;
    }

    public function setCurrentAddressPostCode(?string $currentAddressPostCode): self
    {
        $this->currentAddressPostCode = $currentAddressPostCode;

        return $this;
    }

    public function getCurrentAddressNeighbourhood(): ?string
    {
        return $this->currentAddressNeighbourhood;
    }

    public function setCurrentAddressNeighbourhood(?string $currentAddressNeighbourhood): self
    {
        $this->currentAddressNeighbourhood = $currentAddressNeighbourhood;

        return $this;
    }

    public function getPermanentAddressPostCode(): ?string
    {
        return $this->permanentAddressPostCode;
    }

    public function setPermanentAddressPostCode(?string $permanentAddressPostCode): self
    {
        $this->permanentAddressPostCode = $permanentAddressPostCode;

        return $this;
    }

    public function getPermanentAddressNeighbourhood(): ?string
    {
        return $this->permanentAddressNeighbourhood;
    }

    public function setPermanentAddressNeighbourhood(?string $permanentAddressNeighbourhood): self
    {
        $this->permanentAddressNeighbourhood = $permanentAddressNeighbourhood;

        return $this;
    }

    public function getWorkAddressPostCode(): ?string
    {
        return $this->workAddressPostCode;
    }

    public function setWorkAddressPostCode(?string $workAddressPostCode): self
    {
        $this->workAddressPostCode = $workAddressPostCode;

        return $this;
    }

    public function getWorkAddressNeighbourhood(): ?string
    {
        return $this->workAddressNeighbourhood;
    }

    public function setWorkAddressNeighbourhood(?string $workAddressNeighbourhood): self
    {
        $this->workAddressNeighbourhood = $workAddressNeighbourhood;

        return $this;
    }

    public function getCompanyPosition(): ?string
    {
        return $this->companyPosition;
    }

    public function setCompanyPosition(string $companyPosition): self
    {
        $this->companyPosition = $companyPosition;

        return $this;
    }

    public function getFrequencyOfReceivingSalary(): ?string
    {
        return $this->frequencyOfReceivingSalary;
    }

    public function setFrequencyOfReceivingSalary(string $frequencyOfReceivingSalary): self
    {
        $this->frequencyOfReceivingSalary = $frequencyOfReceivingSalary;

        return $this;
    }

    public function fullName(): string
    {
        return sprintf('%s %s %s', $this->getFirstName(), $this->getMiddleName(), $this->getLastName());
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTotalIncome()
    {
        $this->incomesTotal = $this->getBonusIncome() +
            $this->getMonthlyIncome() +
            $this->getOtherIncome() +
            $this->getBonusIncome() +
            $this->getSocialIncome() +
            $this->getRentIncome() +
            $this->getCivilContractIncome() +
            $this->getPension()
        ;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTotalExpenses()
    {
        $this->expensesTotal =
            $this->getHouseholdExpensesFoodAndClothes() +
            $this->getHouseholdExpensesKids() +
            $this->getHouseholdExpensesElectricityAndWater() +
            $this->getHouseholdExpensesHeating() +
            $this->getHouseholdExpensesInternetAndTv() +
            $this->getHouseholdExpensesTelephone() +
            $this->getHouseholdExpensesRent() +
            $this->getHouseholdExpensesTransport() +
            $this->getHouseholdExpensesDrinksCigarettes() +
            $this->getHouseholdExpensesOtherCreditsInstalmentAmount() +
            $this->getHouseholdExpensesOther();
    }

    public function getContactPersonFirstName(): ?string
    {
        return $this->contactPersonFirstName;
    }

    public function setContactPersonFirstName(string $contactPersonFirstName): self
    {
        $this->contactPersonFirstName = $contactPersonFirstName;

        return $this;
    }

    public function getContactPersonMiddleName(): ?string
    {
        return $this->contactPersonMiddleName;
    }

    public function setContactPersonMiddleName(string $contactPersonMiddleName): self
    {
        $this->contactPersonMiddleName = $contactPersonMiddleName;

        return $this;
    }

    public function getContactPersonLastName(): ?string
    {
        return $this->contactPersonLastName;
    }

    public function setContactPersonLastName(string $contactPersonLastName): self
    {
        $this->contactPersonLastName = $contactPersonLastName;

        return $this;
    }

    public function getContactPersonDateOfBirth(): ?DateTimeInterface
    {
        return $this->contactPersonDateOfBirth;
    }

    public function setContactPersonDateOfBirth(?DateTimeInterface $contactPersonDateOfBirth): self
    {
        $this->contactPersonDateOfBirth = $contactPersonDateOfBirth;

        return $this;
    }

    public function getContactPersonMobileTelephone(): ?string
    {
        return $this->contactPersonMobileTelephone;
    }

    public function setContactPersonMobileTelephone(string $contactPersonMobileTelephone): self
    {
        $this->contactPersonMobileTelephone = $contactPersonMobileTelephone;

        return $this;
    }

    public function getContactPersonRelationship(): ?string
    {
        return $this->contactPersonRelationship;
    }

    public function setContactPersonRelationship(string $contactPersonRelationship): self
    {
        $this->contactPersonRelationship = $contactPersonRelationship;

        return $this;
    }

    public function getHouseholdExpensesKids(): ?float
    {
        return $this->householdExpensesKids;
    }

    public function setHouseholdExpensesKids(?float $householdExpensesKids): self
    {
        $this->householdExpensesKids = $householdExpensesKids;

        return $this;
    }

    public function getHouseholdExpensesRent(): ?float
    {
        return $this->householdExpensesRent;
    }

    public function setHouseholdExpensesRent(float $householdExpensesRent): self
    {
        $this->householdExpensesRent = $householdExpensesRent;

        return $this;
    }

    public function getHouseholdExpensesTransport(): ?float
    {
        return $this->householdExpensesTransport;
    }

    public function setHouseholdExpensesTransport(float $householdExpensesTransport): self
    {
        $this->householdExpensesTransport = $householdExpensesTransport;

        return $this;
    }

    public function getHouseholdExpensesDrinksCigarettes(): ?float
    {
        return $this->householdExpensesDrinksCigarettes;
    }

    public function setHouseholdExpensesDrinksCigarettes(float $householdExpensesDrinksCigarettes
    ): self {
        $this->householdExpensesDrinksCigarettes = $householdExpensesDrinksCigarettes;

        return $this;
    }

    public function getHouseholdExpensesOtherCredits(): ?string
    {
        return $this->householdExpensesOtherCredits;
    }

    public function setHouseholdExpensesOtherCredits(string $householdExpensesOtherCredits): self
    {
        $this->householdExpensesOtherCredits = $householdExpensesOtherCredits;

        return $this;
    }

    public function getHouseholdExpensesOtherCreditsInstalmentAmount(): ?float
    {
        return $this->householdExpensesOtherCreditsInstalmentAmount;
    }

    public function setHouseholdExpensesOtherCreditsInstalmentAmount(
        float $householdExpensesOtherCreditsInstalmentAmount
    ): self {
        $this->householdExpensesOtherCreditsInstalmentAmount = $householdExpensesOtherCreditsInstalmentAmount;

        return $this;
    }

    public function getHouseholdExpensesHeating(): ?float
    {
        return $this->householdExpensesHeating;
    }

    public function setHouseholdExpensesHeating(float $householdExpensesHeating): self
    {
        $this->householdExpensesHeating = $householdExpensesHeating;

        return $this;
    }

    public function getHasProperty(): ?string
    {
        return $this->hasProperty;
    }

    public function setHasProperty(string $hasProperty): self
    {
        $this->hasProperty = $hasProperty;

        return $this;
    }

    public function getHasCar(): ?bool
    {
        return $this->hasCar;
    }

    public function setHasCar(bool $hasCar): self
    {
        $this->hasCar = $hasCar;

        return $this;
    }

    public function getNumberOfCars(): ?int
    {
        return $this->numberOfCars;
    }

    public function setNumberOfCars(?int $numberOfCars): self
    {
        $this->numberOfCars = $numberOfCars;

        return $this;
    }

    public function getCarYearOfManufacture(): ?int
    {
        return $this->carYearOfManufacture;
    }

    public function setCarYearOfManufacture(?int $carYearOfManufacture): self
    {
        $this->carYearOfManufacture = $carYearOfManufacture;

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): ?Collection
    {
        return $this->files;
    }

    public function setFiles(?ArrayCollection $files): self
    {
        $this->files = $files;

        return $this;
    }

    public function addFile(Document $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
        }

        return $this;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateIban(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHowShouldMoneyBeReceived() === MethodsToReceiveCredit::BANK_ACCOUNT) {
            if (empty($this->getIban())) {
                $context->buildViolation('iban.required_when_money_are_received_in_bank_account')
                    ->atPath('iban')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateContractExpirationDate(ExecutionContextInterface $context, $payload)
    {
        if ($this->getEmploymentType() === EmploymentTypes::FIXED_TERM_CONTRACT) {
            if (empty($this->getExpirationDateOfEmploymentContract())) {
                $context->buildViolation('field_required_when_employed_on_fixed_term_contract')
                    ->atPath('expirationDateOfEmploymentContract')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateFrequencyOfReceivingSalary(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHowSalaryIsReceived() === MethodsReceivingSalary::BANK_ACCOUNT ||
            $this->getHowSalaryIsReceived() === MethodsReceivingSalary::CASH) {
            if (empty($this->getFrequencyOfReceivingSalary())) {
                $context->buildViolation('field_required_when_salary_is_received_in_cash_or_bank_account')
                    ->atPath('frequencyOfReceivingSalary')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateDayOfReceivingSalary(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHowSalaryIsReceived() === MethodsReceivingSalary::BANK_ACCOUNT ||
            $this->getHowSalaryIsReceived() === MethodsReceivingSalary::CASH) {
            if (empty($this->getDayForReceivingSalary())) {
                $context->buildViolation('field_required_when_salary_is_received_in_cash_or_bank_account')
                    ->atPath('dayForReceivingSalary')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNumberOfCars(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHasCar() === true) {
            if (empty($this->getNumberOfCars())) {
                $context->buildViolation('required_when_client_has_a_car')
                    ->atPath('numberOfCars')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateCarYearOfManufacture(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHasCar() === true) {
            if (empty($this->getCarYearOfManufacture())) {
                $context->buildViolation('required_when_client_has_a_car')
                    ->atPath('carYearOfManufacture')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateBankCardExpirationDate(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHowShouldMoneyBeReceived() === MethodsToReceiveCredit::BANK_ACCOUNT) {
            if (empty($this->getBankCardExpirationDateMonth())) {
                $context->buildViolation('bankCardExpirationDateMonth.required_when_money_are_received_in_bank_account')
                    ->atPath('bankCardExpirationDateMonth')
                    ->addViolation();
            }

            if (empty($this->getBankCardExpirationDateYear())) {
                $context->buildViolation('bankCardExpirationDateMonth.required_when_money_are_received_in_bank_account')
                    ->atPath('bankCardExpirationDateYear')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateCurrentAddressFields(ExecutionContextInterface $context, $payload)
    {
        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressArea())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressArea')
                    ->addViolation();
            }
        }

        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressMunicipality())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressMunicipality')
                    ->addViolation();
            }
        }

        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressCity())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressCity')
                    ->addViolation();
            }
        }

        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressStreet())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressStreet')
                    ->addViolation();
            }
        }

        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressBlockNumber())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressBlockNumber')
                    ->addViolation();
            }
        }

        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressApartment())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressApartment')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportFirstPageUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if (null === $this->oldStylePassportFirstPageUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePassportFirstPageUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportSecondPageUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if (null === $this->oldStylePassportSecondPageUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePassportSecondPageUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportPhotoAt25Uploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if ($this->age() >= 25) {
                if (null === $this->oldStylePassportPhotoAt25Uploaded) {
                    $context->buildViolation('you_need_to_select_file')
                        ->atPath('oldStylePassportPhotoAt25Uploaded')
                        ->addViolation();
                }
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportPastedPhotoAt25Uploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if ($this->age() >= 25) {
                if (null === $this->oldStylePassportPastedPhotoAt25Uploaded) {
                    $context->buildViolation('you_need_to_select_file')
                        ->atPath('oldStylePassportPastedPhotoAt25Uploaded')
                        ->addViolation();
                }
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportPhotoAt45Uploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if ($this->age() >= 45) {
                if (null === $this->oldStylePassportPhotoAt45Uploaded) {
                    $context->buildViolation('you_need_to_select_file')
                        ->atPath('oldStylePassportPhotoAt45Uploaded')
                        ->addViolation();
                }
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportPastedPhotoAt45Uploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if ($this->age() >= 45) {
                if (null === $this->oldStylePassportPastedPhotoAt45Uploaded) {
                    $context->buildViolation('you_need_to_select_file')
                        ->atPath('oldStylePassportPastedPhotoAt45Uploaded')
                        ->addViolation();
                }
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportProofOfResidenceUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if (null === $this->oldStylePassportProofOfResidenceUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePassportProofOfResidenceUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportSelfieWithPassportUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if (null === $this->oldStylePassportSelfieWithPassportUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePassportSelfieWithPassportUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNewStylePassportPhotoPageUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::NEW_STYLE) {
            if (null === $this->newStylePassportPhotoPageUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('newStylePassportPhotoPageUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNewStylePassportValidityPageUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::NEW_STYLE) {
            if (null === $this->newStylePassportValidityPageUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('newStylePassportValidityPageUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNewStylePassportRegistrationAppendixUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::NEW_STYLE) {
            if (null === $this->newStylePassportRegistrationAppendixUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('newStylePassportRegistrationAppendixUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNewStylePassportSelfieWithIdCardUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::NEW_STYLE) {
            if (null === $this->newStylePassportSelfieWithIdCardUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('newStylePassportSelfieWithIdCardUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validatePensionCertificateType(ExecutionContextInterface $context, $payload)
    {
        if ($this->getProduct() === 'Pensioner') {
            if (null === $this->getPensionCertificateType()) {
                $context->buildViolation('field_is_required')
                    ->atPath('pensionCertificateType')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePensionCertificateFirstSpreadUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPensionCertificateType() === PensionCertificateTypes::OLD_STYLE) {
            if (null === $this->oldStylePensionCertificateFirstSpreadUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePensionCertificateFirstSpreadUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePensionCertificateValidityRecordUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPensionCertificateType() === PensionCertificateTypes::OLD_STYLE) {
            if (null === $this->oldStylePensionCertificateValidityRecordUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePensionCertificateValidityRecordUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNewStylePensionCertificatePhotoAndValiditySpreadUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPensionCertificateType() === PensionCertificateTypes::NEW_STYLE) {
            if (null === $this->newStylePensionCertificatePhotoAndValiditySpreadUploaded) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('newStylePensionCertificatePhotoAndValiditySpreadUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateCurrentAddressFields()
    {
        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            return;
        }

        $this->setCurrentAddressArea($this->getPermanentAddressArea());
        $this->setCurrentAddressMunicipality($this->getPermanentAddressMunicipality());
        $this->setCurrentAddressCity($this->getPermanentAddressCity());
        $this->setCurrentAddressPostCode($this->getPermanentAddressPostCode());
        $this->setCurrentAddressNeighbourhood($this->getPermanentAddressNeighbourhood());
        $this->setCurrentAddressStreet($this->getPermanentAddressStreet());
        $this->setCurrentAddressBlockNumber($this->getPermanentAddressBlockNumber());
        $this->setCurrentAddressEntrance($this->getPermanentAddressEntrance());
        $this->setCurrentAddressFloor($this->getPermanentAddressFloor());
        $this->setCurrentAddressApartment($this->getPermanentAddressApartment());
    }

    /**
     * @ORM\PrePersist()
     */
    public function updatePhoneNumbers()
    {
        $this->mobileTelephone = '+380' . $this->mobileTelephone;
        $this->companyTelephone = '+380' . $this->companyTelephone;
        $this->contactPersonMobileTelephone = '+380' . $this->contactPersonMobileTelephone;

        $this->telephoneForNewContracts = $this->mobileTelephone;
    }

    /**
     * @ORM\PrePersist()
     */
    public function updateBankCardExpirationDate()
    {
        if ($this->getHowShouldMoneyBeReceived() !== MethodsToReceiveCredit::BANK_ACCOUNT) {
            return;
        }

        $dateString = sprintf(
            '%s-%s-%s',
            $this->getBankCardExpirationDateYear(),
            $this->getBankCardExpirationDateMonth(),
            1
        );

        $date = Carbon::createFromFormat('Y-m-d', $dateString)->endOfMonth();

        $this->setBankCardExpirationDate($date);
    }

    /**
     * @ORM\PrePersist()
     */
    public function updateCasing()
    {
        $this->iban = strtoupper($this->iban);
        $this->firstName = ucfirst($this->firstName);
        $this->middleName = ucfirst($this->middleName);
        $this->lastName = ucfirst($this->lastName);
        $this->idCardIssuedAt = ucfirst($this->idCardIssuedAt);

        $this->permanentAddressCity = ucfirst($this->permanentAddressCity);
        $this->currentAddressCity = ucfirst($this->currentAddressCity);
        $this->workAddressCity = ucfirst($this->workAddressCity);

        $this->permanentAddressNeighbourhood = ucfirst($this->permanentAddressNeighbourhood);
        $this->currentAddressNeighbourhood = ucfirst($this->currentAddressNeighbourhood);
        $this->workAddressNeighbourhood = ucfirst($this->workAddressNeighbourhood);

        $this->permanentAddressStreet = ucfirst($this->permanentAddressStreet);
        $this->currentAddressStreet = ucfirst($this->currentAddressStreet);
        $this->workAddressStreet = ucfirst($this->workAddressStreet);

        $this->permanentAddressBlockNumber = ucfirst($this->permanentAddressBlockNumber);
        $this->currentAddressBlockNumber = ucfirst($this->currentAddressBlockNumber);
        $this->workAddressBlockNumber = ucfirst($this->workAddressBlockNumber);

        $this->permanentAddressEntrance = ucfirst($this->permanentAddressEntrance);
        $this->currentAddressEntrance = ucfirst($this->currentAddressEntrance);
        $this->workAddressEntrance = ucfirst($this->workAddressEntrance);

        $this->permanentAddressApartment = ucfirst($this->permanentAddressApartment);
        $this->currentAddressApartment = ucfirst($this->currentAddressApartment);
        $this->workAddressApartment = ucfirst($this->workAddressApartment);

        $this->companyName = ucfirst($this->companyName);
        $this->companyPosition = ucfirst($this->companyPosition);
        $this->householdExpensesOtherCredits = ucfirst($this->householdExpensesOtherCredits);
    }

    public function getBankCardExpirationDate(): ?DateTimeInterface
    {
        return $this->bankCardExpirationDate;
    }

    public function setBankCardExpirationDate(?DateTimeInterface $bankCardExpirationDate): self
    {
        if ($bankCardExpirationDate) {
            $this->bankCardExpirationDate = Carbon::instance($bankCardExpirationDate)->endOfMonth();
        } else {
            $this->bankCardExpirationDate = $bankCardExpirationDate;
        }

        return $this;
    }

    public function getBankCardExpirationDateMonth()
    {
        return $this->bankCardExpirationDateMonth;
    }

    public function setBankCardExpirationDateMonth($bankCardExpirationDateMonth)
    {
        $this->bankCardExpirationDateMonth = $bankCardExpirationDateMonth;

        return $this;
    }

    public function getBankCardExpirationDateYear()
    {
        return $this->bankCardExpirationDateYear;
    }

    public function setBankCardExpirationDateYear($bankCardExpirationDateYear)
    {
        $this->bankCardExpirationDateYear = $bankCardExpirationDateYear;

        return $this;
    }

    public function getPassportType(): ?string
    {
        return $this->passportType;
    }

    public function setPassportType(string $passportType): self
    {
        $this->passportType = $passportType;

        return $this;
    }

    public function getOldStylePassportFirstPage(): ?Document
    {
        return $this->oldStylePassportFirstPage;
    }

    public function setOldStylePassportFirstPage(?Document $oldStylePassportFirstPage): self
    {
        $this->oldStylePassportFirstPage = $oldStylePassportFirstPage;

        return $this;
    }

    public function getOldStylePassportSecondPage(): ?Document
    {
        return $this->oldStylePassportSecondPage;
    }

    public function setOldStylePassportSecondPage(?Document $oldStylePassportSecondPage): self
    {
        $this->oldStylePassportSecondPage = $oldStylePassportSecondPage;

        return $this;
    }

    public function getOldStylePassportPhotoAt25(): ?Document
    {
        return $this->oldStylePassportPhotoAt25;
    }

    public function setOldStylePassportPhotoAt25(?Document $oldStylePassportPhotoAt25): self
    {
        $this->oldStylePassportPhotoAt25 = $oldStylePassportPhotoAt25;

        return $this;
    }

    public function getOldStylePassportPastedPhotoAt25(): ?Document
    {
        return $this->oldStylePassportPastedPhotoAt25;
    }

    public function setOldStylePassportPastedPhotoAt25(?Document $oldStylePassportPastedPhotoAt25): self
    {
        $this->oldStylePassportPastedPhotoAt25 = $oldStylePassportPastedPhotoAt25;

        return $this;
    }

    public function getOldStylePassportPhotoAt45(): ?Document
    {
        return $this->oldStylePassportPhotoAt45;
    }

    public function setOldStylePassportPhotoAt45(?Document $oldStylePassportPhotoAt45): self
    {
        $this->oldStylePassportPhotoAt45 = $oldStylePassportPhotoAt45;

        return $this;
    }

    public function getOldStylePassportPastedPhotoAt45(): ?Document
    {
        return $this->oldStylePassportPastedPhotoAt45;
    }

    public function setOldStylePassportPastedPhotoAt45(?Document $oldStylePassportPastedPhotoAt45): self
    {
        $this->oldStylePassportPastedPhotoAt45 = $oldStylePassportPastedPhotoAt45;

        return $this;
    }

    public function getOldStylePassportProofOfResidence(): ?Document
    {
        return $this->oldStylePassportProofOfResidence;
    }

    public function setOldStylePassportProofOfResidence(?Document $oldStylePassportProofOfResidence): self
    {
        $this->oldStylePassportProofOfResidence = $oldStylePassportProofOfResidence;

        return $this;
    }

    public function getOldStylePassportSelfieWithPassport(): ?Document
    {
        return $this->oldStylePassportSelfieWithPassport;
    }

    public function setOldStylePassportSelfieWithPassport(?Document $oldStylePassportSelfieWithPassport): self
    {
        $this->oldStylePassportSelfieWithPassport = $oldStylePassportSelfieWithPassport;

        return $this;
    }

    public function getNewStylePassportPhotoPage(): ?Document
    {
        return $this->newStylePassportPhotoPage;
    }

    public function setNewStylePassportPhotoPage(?Document $newStylePassportPhotoPage): self
    {
        $this->newStylePassportPhotoPage = $newStylePassportPhotoPage;

        return $this;
    }

    public function getNewStylePassportValidityPage(): ?Document
    {
        return $this->newStylePassportValidityPage;
    }

    public function setNewStylePassportValidityPage(?Document $newStylePassportValidityPage): self
    {
        $this->newStylePassportValidityPage = $newStylePassportValidityPage;

        return $this;
    }

    public function getNewStylePassportRegistrationAppendix(): ?Document
    {
        return $this->newStylePassportRegistrationAppendix;
    }

    public function setNewStylePassportRegistrationAppendix(?Document $newStylePassportRegistrationAppendix): self
    {
        $this->newStylePassportRegistrationAppendix = $newStylePassportRegistrationAppendix;

        return $this;
    }

    public function getNewStylePassportSelfieWithIdCard(): ?Document
    {
        return $this->newStylePassportSelfieWithIdCard;
    }

    public function setNewStylePassportSelfieWithIdCard(?Document $newStylePassportSelfieWithIdCard): self
    {
        $this->newStylePassportSelfieWithIdCard = $newStylePassportSelfieWithIdCard;

        return $this;
    }

    public function getPensionCertificateType(): ?string
    {
        return $this->pensionCertificateType;
    }

    public function setPensionCertificateType(string $pensionCertificateType): self
    {
        $this->pensionCertificateType = $pensionCertificateType;

        return $this;
    }

    public function getOldStylePensionCertificateFirstSpread(): ?Document
    {
        return $this->oldStylePensionCertificateFirstSpread;
    }

    public function setOldStylePensionCertificateFirstSpread(?Document $oldStylePensionCertificateFirstSpread): self
    {
        $this->oldStylePensionCertificateFirstSpread = $oldStylePensionCertificateFirstSpread;

        return $this;
    }

    public function getOldStylePensionCertificateValidityRecord(): ?Document
    {
        return $this->oldStylePensionCertificateValidityRecord;
    }

    public function setOldStylePensionCertificateValidityRecord(?Document $oldStylePensionCertificateValidityRecord): self
    {
        $this->oldStylePensionCertificateValidityRecord = $oldStylePensionCertificateValidityRecord;

        return $this;
    }

    public function getNewStylePensionCertificatePhotoAndValiditySpread(): ?Document
    {
        return $this->newStylePensionCertificatePhotoAndValiditySpread;
    }

    public function setNewStylePensionCertificatePhotoAndValiditySpread(?Document $newStylePensionCertificatePhotoAndValiditySpread): self
    {
        $this->newStylePensionCertificatePhotoAndValiditySpread = $newStylePensionCertificatePhotoAndValiditySpread;

        return $this;
    }

    private function age(): int
    {
        if (strlen($this->personalIdentificationNumber) < 5) {
            return 0;
        }

        $days = intval(substr($this->personalIdentificationNumber, 0, 5));
        $dateOfBirth = Carbon::parse('01 Jan 1900')->addDays($days);
        $age = Carbon::now()->diffInDays($dateOfBirth) / 365;

        return intval($age);
    }
}
