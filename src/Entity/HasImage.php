<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait HasImage
{
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Url()
     */
    private $image;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $showImage = true;

    /**
     * @return string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return HasImage
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return bool
     */
    public function isShowImage(): bool
    {
        return $this->showImage;
    }

    /**
     * @param bool $showImage
     * @return HasImage
     */
    public function setShowImage(bool $showImage): self
    {
        $this->showImage = $showImage;

        return $this;
    }
}