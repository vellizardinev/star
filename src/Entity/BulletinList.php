<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\BulletinListRepository;

/**
 * @ORM\Entity(repositoryClass=BulletinListRepository::class)
 * @UniqueEntity(fields={"subscriberEmail"}, message="You have already subscribed for the bulletin.")
 */
class BulletinList
{
	use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=190, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $subscriberEmail;

    public function getId(): ?int
    {
        return $this->id;
    }

	/**
	 * @return mixed
	 */
	public function getSubscriberEmail(): ?string
	{
		return $this->subscriberEmail;
	}

	/**
	 * @param mixed $subscriberEmail
	 * @return BulletinList
	 */
	public function setSubscriberEmail(string $subscriberEmail): self
	{
		$this->subscriberEmail = $subscriberEmail;
		return $this;
	}
}
