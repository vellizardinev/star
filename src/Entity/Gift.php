<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GiftRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Gift implements Searchable, Translatable
{
    use TimestampableEntity;
    use SortableEntity;
    use SoftDeleteableEntity;
	use TranslatableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $code;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $price;

	/**
	 * @ORM\Column(type="boolean", options={"default" : false})
	 */
	private $new = false;

	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Image", cascade={"persist", "remove"})
	 */
	private $image;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

	public function isNew(): bool
	{
		return $this->new;
	}

	public function setNew(bool $new): self
	{
		$this->new = $new;

		return $this;
	}

	public function getImage(): ?Image
	{
		return $this->image;
	}

	public function setImage(?Image $image): self
	{
		$this->image = $image;

		return $this;
	}

    public function text(): string
    {
        return $this->getName();
    }

    public function urlContext(): array
    {
        return ['loyalty_program.gifts'];
    }
}
