<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PollAnswerRepository")
 */
class PollAnswer
{
    use TimestampableEntity;
    use SortableEntity;
    use TranslatableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"main"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Gedmo\Translatable()
     * @Groups({"main"})
     * @Assert\NotBlank(message="answer_text_should_not_be_blank")
     */
    private $text;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"main"})
     */
    private $hasComment = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PollQuestion", inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"main"})
     */
    private $commentHelpText;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getHasComment(): ?bool
    {
        return $this->hasComment;
    }

    public function setHasComment(bool $hasComment): self
    {
        $this->hasComment = $hasComment;

        return $this;
    }

    public function getQuestion(): ?PollQuestion
    {
        return $this->question;
    }

    public function setQuestion(?PollQuestion $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getCommentHelpText(): ?string
    {
        return $this->commentHelpText;
    }

    public function setCommentHelpText(?string $commentHelpText): self
    {
        $this->commentHelpText = $commentHelpText;

        return $this;
    }
}
