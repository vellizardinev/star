<?php


namespace App\Entity;


interface Searchable
{
    public function text(): string;

    public function urlContext(): array;
}