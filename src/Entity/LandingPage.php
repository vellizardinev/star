<?php

namespace App\Entity;

use App\Enum\PageTypes;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LandingPageRepository")
 */
class LandingPage extends Page
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ModuleItem", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $header;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ModuleItem", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $footer;

    public function getHeader(): ?ModuleItem
    {
        return $this->header;
    }

    public function setHeader(ModuleItem $header): self
    {
        $this->header = $header;

        return $this;
    }

    public function getFooter(): ?ModuleItem
    {
        return $this->footer;
    }

    public function setFooter(ModuleItem $footer): self
    {
        $this->footer = $footer;

        return $this;
    }

    public function getType(): string
    {
        return PageTypes::LANDING_PAGE;
    }
}
