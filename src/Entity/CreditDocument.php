<?php

namespace App\Entity;

use App\Enum\CreditDocumentType;
use App\File\FileManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CreditDocumentRepository")
 */
class CreditDocument extends File
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $erpId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSigned = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type = CreditDocumentType::INITIAL;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $erpKey;

    public function getErpId(): ?string
    {
        return $this->erpId;
    }

    public function setErpId(string $erpId): self
    {
        $this->erpId = $erpId;

        return $this;
    }

    public function getIsSigned(): ?bool
    {
        return $this->isSigned;
    }

    public function setIsSigned(bool $isSigned): self
    {
        $this->isSigned = $isSigned;

        return $this;
    }

    public function getFilePath(): string
    {
        return FileManager::CREDIT_DOCUMENT_DIR . '/' . $this->getName();
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getErpKey(): ?int
    {
        return $this->erpKey;
    }

    public function setErpKey(?int $erpKey): self
    {
        $this->erpKey = $erpKey;

        return $this;
    }
}
