<?php

namespace App\Entity;

use App\Enum\Roles;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Customer implements UserInterface, EquatableInterface
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min="0", max="255")
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯăâîșşțţĂÂÎȘŞȚŢ]+$/")
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min="0", max="255")
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯăâîșşțţĂÂÎȘŞȚŢ]+$/")
     */
    private $middleName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min="0", max="255")
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯăâîșşțţĂÂÎȘŞȚŢ]+$/")
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min="10", max="10")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_10")
     */
    private $personalIdentificationNumber;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^\d{9}$/", message="digits_required_9")
     */
    private $telephone;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Length(min="0", max="255")
     */
    private $email;

    /**
     * @var string
     */
    private $token;

    /**
     * @var bool
     */
    private $hasUserAccount = false;

    /**
     * @var bool
     */
    private $isEmailActive= false;

    /**
     * @var bool
     */
    private $isPhoneActive = false;

    /**
     * @var string
     */
    private $password;

    /** @var string */
    private $oldTelephone;

    /** @var string */
    private $oldEmail;

    /**
     * @var array
     */
    private $roles = [Roles::CUSTOMER_NOT_FULLY_AUTHENTICATED];

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function setMiddleName(?string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPersonalIdentificationNumber(): ?string
    {
        return $this->personalIdentificationNumber;
    }

    public function setPersonalIdentificationNumber(?string $personalIdentificationNumber): self
    {
        $this->personalIdentificationNumber = $personalIdentificationNumber;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->oldTelephone = $this->telephone;

        $this->telephone = $telephone;

        return $this;
    }

    public function isTelephoneChanged(): bool
    {
        if ($this->oldTelephone !== null && $this->oldTelephone !== $this->telephone) {
            return true;
        }

        return false;
    }

	public function telephoneWithLeadingZero(): string
	{
		return '0' . $this->getTelephone();
	}

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->oldEmail = $this->email;

        $this->email = $email;

        return $this;
    }

    public function isEmailChanged(): bool
    {
        if ($this->oldEmail !== null && $this->oldEmail !== $this->email) {
            return true;
        }

        return false;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getHasUserAccount(): ?bool
    {
        return $this->hasUserAccount;
    }

    public function setHasUserAccount(?bool $hasUserAccount): self
    {
        $this->hasUserAccount = $hasUserAccount;

        return $this;
    }

    public function getIsEmailActive(): ?bool
    {
        return $this->isEmailActive;
    }

    public function setIsEmailActive(?bool $isEmailActive): self
    {
        $this->isEmailActive = $isEmailActive;

        return $this;
    }

    public function getIsPhoneActive(): ?bool
    {
        return $this->isPhoneActive;
    }

    public function setIsPhoneActive(?bool $isPhoneActive): self
    {
        $this->isPhoneActive = $isPhoneActive;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // not needed when as customer's data is not persisted to DB
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->personalIdentificationNumber;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    public function fullName(): string
    {
        return sprintf(
            '%s %s %s',
            $this->firstName,
            $this->middleName,
            $this->lastName
        );
    }

    public function __toString()
    {
        return sprintf(
            '%s, email: %s',
            $this->fullName(),
            $this->email
        );
    }

    public function isActive(): bool
    {
        return $this->isEmailActive && $this->isPhoneActive;
    }

    public function isEqualTo(UserInterface $user)
    {
        if ($this->getEmail() === null && $user->getUsername() !== null) {
            return true;
        }

        return $this->getPersonalIdentificationNumber() === $user->getUsername();
    }

    public function hasRole( string $role): bool
    {
        return in_array($role, $this->getRoles());
    }

    public function firstAndLastName(): string
    {
        return sprintf(
          '%s %s',
          $this->firstName,
          $this->lastName
        );
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->email ?? '';
    }
}
