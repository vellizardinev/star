<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class JobApplication
{
    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $fullName;

	/**
	 * @ORM\Column(type="string", length=190, nullable=true)
	 * @Assert\NotBlank()
     * @Assert\Length(min="9", max="9")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_9")
	 */
	private $telephone;

    /**
     * @var string
     * @Assert\Email()
     */
    private $email;

    /**
     * @Assert\File(mimeTypesMessage="cv.mime_types", mimeTypes={"application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"}, maxSize="2M", maxSizeMessage="cv.max_size")
     */
    private $cv;

    /**
     * @var string
     */
    private $comment;

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCv(): ?UploadedFile
    {
        return $this->cv;
    }

    public function setCv(?UploadedFile $cv): self
    {
        $this->cv = $cv;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
