<?php

namespace App\Entity;

use Gedmo\Mapping\Annotation as Gedmo;

trait TranslatableEntity
{
    /**
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * Sets translatable locale
     *
     * @param string $locale
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
}
