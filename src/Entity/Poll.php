<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PollRepository")
 */
class Poll
{
    use TimestampableEntity;
    use HasVisibilityConfiguration;
    use TranslatableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"main", "poll_participation"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Translatable()
     * @Assert\NotBlank()
     * @Groups({"main", "poll_participation"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable()
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=PollQuestion::class, mappedBy="poll", orphanRemoval=true, cascade={"persist"})
     * @Groups("main")
     * @ORM\OrderBy({"positionIndex" = "ASC"})
     */
    private $questions;

    /**
     * @ORM\OneToMany(targetEntity=PollEntry::class, mappedBy="poll", orphanRemoval=true)
     */
    private $entries;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->entries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|PollQuestion[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(PollQuestion $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setPoll($this);
        }

        return $this;
    }

    public function removeQuestion(PollQuestion $question): self
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
            // set the owning side to null (unless already changed)
            if ($question->getPoll() === $this) {
                $question->setPoll(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PollEntry[]
     */
    public function getEntries(): Collection
    {
        return $this->entries;
    }

    public function addEntry(PollEntry $entry): self
    {
        if (!$this->entries->contains($entry)) {
            $this->entries[] = $entry;
            $entry->setPoll($this);
        }

        return $this;
    }

    public function removeEntry(PollEntry $entry): self
    {
        if ($this->entries->contains($entry)) {
            $this->entries->removeElement($entry);
            // set the owning side to null (unless already changed)
            if ($entry->getPoll() === $this) {
                $entry->setPoll(null);
            }
        }

        return $this;
    }
}
