<?php

namespace App\Entity\OnlineApplication;

use App\Enum\MethodsToReceiveCredit;
use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use ReflectionException;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\CreditDetailsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CreditDetails
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", mappedBy="creditDetails", cascade={"persist", "remove"})
     * @MaxDepth(2)
     */
    private $onlineApplication;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="0", max="255")
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $period;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $instalmentAmount;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $paybackCreditAmountMethod = '67585214-aec5-4d7e-90a5-4f5739c4d245';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $iban;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $creditProductId;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $creditPeriodId;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $howShouldMoneyBeReceived;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $bankCardExpirationDate;

    private $bankCardExpirationDateMonth;

    private $bankCardExpirationDateYear;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $confirmedAccuracyOfCardData;

    /**
     * @ORM\OneToMany(targetEntity=UserPaymentInstrument::class, mappedBy="creditDetails", orphanRemoval=true, cascade={"persist"})
     */
    private $paymentInstruments;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $selectedPaymentInstrument;

    public function __construct()
    {
        $this->paymentInstruments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnlineApplication(): ?OnlineApplication
    {
        return $this->onlineApplication;
    }

    public function setOnlineApplication(?OnlineApplication $onlineApplication): self
    {
        $this->onlineApplication = $onlineApplication;

        // set (or unset) the owning side of the relation if necessary
        $newCreditDetails = null === $onlineApplication ? null : $this;
        if ($onlineApplication->getCreditDetails() !== $newCreditDetails) {
            $onlineApplication->setCreditDetails($newCreditDetails);
        }

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(?string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getProduct(): ?string
    {
        return $this->product;
    }

    public function setProduct(?string $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getPeriod(): ?int
    {
        return $this->period;
    }

    public function setPeriod(?int $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getInstalmentAmount(): ?string
    {
        return $this->instalmentAmount;
    }

    public function setInstalmentAmount(?string $instalmentAmount): self
    {
        $this->instalmentAmount = $instalmentAmount;

        return $this;
    }

    public function getPaybackCreditAmountMethod(): ?string
    {
        return $this->paybackCreditAmountMethod;
    }

    public function setPaybackCreditAmountMethod(?string $paybackCreditAmountMethod): self
    {
        $this->paybackCreditAmountMethod = $paybackCreditAmountMethod;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getBankCardExpirationDate(): ?string
    {
        if ($this->bankCardExpirationDate) {
            return sprintf(
                '%s/%s',
                Carbon::instance($this->bankCardExpirationDate)->month,
                Carbon::instance($this->bankCardExpirationDate)->year
            );
        }

        return $this->bankCardExpirationDate;
    }

    public function setBankCardExpirationDate(?DateTimeInterface $bankCardExpirationDate): self
    {
        if ($bankCardExpirationDate) {
            $this->bankCardExpirationDate = Carbon::instance($bankCardExpirationDate)->endOfMonth();
        } else {
            $this->bankCardExpirationDate = $bankCardExpirationDate;
        }

        return $this;
    }

    public function getBankCardExpirationDateMonth()
    {
        if ($this->bankCardExpirationDate instanceof DateTimeInterface) {
            return Carbon::instance($this->bankCardExpirationDate)->month;
        }

        return $this->bankCardExpirationDateMonth;
    }

    public function setBankCardExpirationDateMonth($bankCardExpirationDateMonth)
    {
        $this->bankCardExpirationDateMonth = $bankCardExpirationDateMonth;

        return $this;
    }

    public function getBankCardExpirationDateYear()
    {
        if ($this->bankCardExpirationDate instanceof DateTimeInterface) {
            return Carbon::instance($this->bankCardExpirationDate)->year;
        }

        return $this->bankCardExpirationDateYear;
    }

    public function setBankCardExpirationDateYear($bankCardExpirationDateYear)
    {
        $this->bankCardExpirationDateYear = $bankCardExpirationDateYear;

        return $this;
    }

    public function getHowShouldMoneyBeReceived(): ?string
    {
        return $this->howShouldMoneyBeReceived;
    }

    public function setHowShouldMoneyBeReceived(?string $howShouldMoneyBeReceived): self
    {
        $this->howShouldMoneyBeReceived = $howShouldMoneyBeReceived;

        return $this;
    }

	public function getCreditProductId(): ?string
	{
		return $this->creditProductId;
	}

	public function setCreditProductId(string $creditProductId): self
	{
		$this->creditProductId = $creditProductId;

		return $this;
	}

	public function getCreditPeriodId(): ?string
	{
		return $this->creditPeriodId;
	}

	public function setCreditPeriodId(string $creditPeriodId): self
	{
		$this->creditPeriodId = $creditPeriodId;

		return $this;
	}

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateCasing()
    {
        $this->iban = mb_convert_case($this->iban, MB_CASE_UPPER);
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateConfirmedAccuracyOfCardData()
    {
        if ($this->getHowShouldMoneyBeReceived() !== MethodsToReceiveCredit::BANK_ACCOUNT) {
            $this->confirmedAccuracyOfCardData = null;
        }
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function toArray(): array
    {
        return [
            'amount' => $this->amount,
            'product' => $this->product,
            'period' => $this->period,
            'instalmentAmount' => $this->instalmentAmount,
            'paybackCreditAmountMethod' => MethodsToReceiveCredit::getTranslationKey($this->paybackCreditAmountMethod),
            'iban' => $this->iban,
            'howShouldMoneyBeReceived' => MethodsToReceiveCredit::getTranslationKey($this->howShouldMoneyBeReceived),
            'bankCardExpirationDate' => $this->bankCardExpirationDate,
        ];
    }

    /**
     * @return bool
     */
    public function getConfirmedAccuracyOfCardData(): ?bool
    {
        return $this->confirmedAccuracyOfCardData;
    }

    /**
     * @param bool $confirmedAccuracyOfCardData
     * @return $this
     */
    public function setConfirmedAccuracyOfCardData(?bool $confirmedAccuracyOfCardData): self
    {
        $this->confirmedAccuracyOfCardData = $confirmedAccuracyOfCardData;

        return $this;
    }

    /**
     * @return Collection|UserPaymentInstrument[]
     */
    public function getPaymentInstruments(): Collection
    {
        return $this->paymentInstruments;
    }

    public function addPaymentInstrument(UserPaymentInstrument $paymentInstrument): self
    {
        if (!$this->paymentInstruments->contains($paymentInstrument)) {
            $this->paymentInstruments[] = $paymentInstrument;
            $paymentInstrument->setCreditDetails($this);
        }

        return $this;
    }

    public function removePaymentInstrument(UserPaymentInstrument $paymentInstrument): self
    {
        if ($this->paymentInstruments->contains($paymentInstrument)) {
            $this->paymentInstruments->removeElement($paymentInstrument);
            // set the owning side to null (unless already changed)
            if ($paymentInstrument->getCreditDetails() === $this) {
                $paymentInstrument->setCreditDetails(null);
            }
        }

        return $this;
    }

    public function getSelectedPaymentInstrument(): ?string
    {
        return $this->selectedPaymentInstrument;
    }

    public function setSelectedPaymentInstrument(?string $selectedPaymentInstrument): self
    {
        $this->selectedPaymentInstrument = $selectedPaymentInstrument;

        return $this;
    }
}
