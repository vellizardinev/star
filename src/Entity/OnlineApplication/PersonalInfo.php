<?php

namespace App\Entity\OnlineApplication;

use App\Entity\MultibyteUppercaseFirst;
use App\Service\MobileApi\ConvertsDatesToString;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\PersonalInfoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PersonalInfo
{
    use TimestampableEntity;
    use MultibyteUppercaseFirst;
    use ConvertsDatesToString;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", mappedBy="personalInfo", cascade={"persist", "remove"})
     */
    private $onlineApplication;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="0", max="255")
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="0", max="255")
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $middleName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="0", max="255")
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     *@\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $idCardNumber;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @Assert\Type("\DateTimeInterface")
     */
    private $idCardIssuedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max="50")
     */
    private $idCardIssuedBy;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="10", max="10")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_10")
     */
    private $personalIdentificationNumber;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disadvantaged = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $wantsDiscretion = false;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $mobileTelephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\Email()
     * @Assert\Length(min="0", max="255")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telephoneForNewContracts;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnlineApplication(): ?OnlineApplication
    {
        return $this->onlineApplication;
    }

    public function setOnlineApplication(?OnlineApplication $onlineApplication): self
    {
        $this->onlineApplication = $onlineApplication;

        // set (or unset) the owning side of the relation if necessary
        $newPersonalInfo = null === $onlineApplication ? null : $this;
        if ($onlineApplication->getPersonalInfo() !== $newPersonalInfo) {
            $onlineApplication->setPersonalInfo($newPersonalInfo);
        }

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function setMiddleName(?string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getIdCardNumber(): ?string
    {
        return $this->idCardNumber;
    }

    public function setIdCardNumber(?string $idCardNumber): self
    {
        $this->idCardNumber = $idCardNumber;

        return $this;
    }

    public function getIdCardIssuedAt(): ?DateTimeInterface
    {
        return $this->idCardIssuedAt;
    }

    public function setIdCardIssuedAt(?DateTimeInterface $idCardIssuedAt): self
    {
        $this->idCardIssuedAt = $idCardIssuedAt;

        return $this;
    }

    public function getIdCardIssuedBy(): ?string
    {
        return $this->idCardIssuedBy;
    }

    public function setIdCardIssuedBy(?string $idCardIssuedBy): self
    {
        $this->idCardIssuedBy = $idCardIssuedBy;

        return $this;
    }

    public function getPersonalIdentificationNumber(): ?string
    {
        return $this->personalIdentificationNumber;
    }

    public function setPersonalIdentificationNumber(?string $personalIdentificationNumber): self
    {
        $this->personalIdentificationNumber = $personalIdentificationNumber;

        return $this;
    }

    public function fullName(): string
    {
        return sprintf('%s %s %s', $this->getFirstName(), $this->getMiddleName(), $this->getLastName());
    }

    public function getWantsDiscretion(): ?bool
    {
        return $this->wantsDiscretion;
    }

    public function setWantsDiscretion(?bool $wantsDiscretion): self
    {
        $this->wantsDiscretion = $wantsDiscretion;

        return $this;
    }

    public function getDisadvantaged(): ?bool
    {
        return $this->disadvantaged;
    }

    public function setDisadvantaged(?bool $disadvantaged): self
    {
        $this->disadvantaged = $disadvantaged;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateCasing()
    {
        $this->firstName = $this->mbUcFirst($this->firstName);
        $this->middleName = $this->mbUcFirst($this->middleName);
        $this->lastName = $this->mbUcFirst($this->lastName);
        $this->idCardIssuedBy = $this->mbUcFirst($this->idCardIssuedBy);
    }

    public function getMobileTelephone(): ?string
    {
        return $this->mobileTelephone;
    }

    public function setMobileTelephone(string $mobileTelephone): self
    {
        $this->mobileTelephone = $mobileTelephone;
        $this->telephoneForNewContracts = $mobileTelephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephoneForNewContracts(): ?string
    {
        return $this->telephoneForNewContracts;
    }

    public function setTelephoneForNewContracts(?string $telephoneForNewContracts): self
    {
        $this->telephoneForNewContracts = $telephoneForNewContracts;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updatePhoneNumbers()
    {
        $this->telephoneForNewContracts = $this->mobileTelephone;
    }

    public function toArray(): array
    {
        return [
            'firstName' => $this->firstName,
            'middleName' => $this->middleName,
            'lastName' => $this->lastName,
            'idCardNumber' => $this->idCardNumber,
            'idCardIssuedAt' => $this->idCardIssuedAt,
            'idCardIssuedBy' => $this->idCardIssuedBy,
            'personalIdentificationNumber' => $this->personalIdentificationNumber,
            'disadvantaged' => $this->disadvantaged,
            'wantsDiscretion' => $this->wantsDiscretion,
            'mobileTelephone' => $this->mobileTelephone,
            'email' => $this->email,
            'telephoneForNewContracts' => $this->telephoneForNewContracts,
        ];
    }

    public static function fromPreviousApplicationData(?array $data): self
    {
        $object = new self;

        if (empty($data)) {
            return $object;
        }

        if (isset($data['IDCardNumber']) && !empty($data['IDCardNumber']))  $object->setIdCardNumber($data['IDCardNumber']);
        if (isset($data['IDCardIssuedBy']) && !empty($data['IDCardIssuedBy']))  $object->setIdCardIssuedBy($data['IDCardIssuedBy']);
        if (isset($data['IDCardIssueDate']) && !empty($data['IDCardIssueDate']))  $object->setIdCardIssuedAt($object->stringToDate($data['IDCardIssueDate']));

        return $object;
    }

    public function getMobileTelephoneWithLeadingZero(): string
    {
        if (preg_match('/^\+380/', $this->mobileTelephone) === 1) {
            $telephone = substr($this->mobileTelephone, 4);

            return '0' . $telephone;
        }

        return '0' . $this->mobileTelephone;
    }

    public function getMobileTelephoneWithCountryCode(): string
    {
        if (preg_match('/^\+380/', $this->mobileTelephone) === 1) {
            $telephone = substr($this->mobileTelephone, 4);

            return '380' . $telephone;
        }

        if (preg_match('/^\380/', $this->mobileTelephone) === 1) {
            return $this->mobileTelephone;
        }

        return '380' . $this->mobileTelephone;
    }
}
