<?php

namespace App\Entity\OnlineApplication;

use App\Enum\PropertyType;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use ReflectionException;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\PropertyRepository")
 */
class Property
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", mappedBy="property", cascade={"persist", "remove"})
     */
    private $onlineApplication;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hasProperty = PropertyType::I_DONT_OWN_A_PROPERTY;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasCar = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Positive()
     */
    private $numberOfCars = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $carYearOfManufacture;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnlineApplication(): ?OnlineApplication
    {
        return $this->onlineApplication;
    }

    public function setOnlineApplication(?OnlineApplication $onlineApplication): self
    {
        $this->onlineApplication = $onlineApplication;

        // set (or unset) the owning side of the relation if necessary
        $newProperty = null === $onlineApplication ? null : $this;
        if ($onlineApplication->getProperty() !== $newProperty) {
            $onlineApplication->setProperty($newProperty);
        }

        return $this;
    }

    public function getHasProperty(): ?string
    {
        return $this->hasProperty;
    }

    public function setHasProperty(string $hasProperty): self
    {
        $this->hasProperty = $hasProperty;

        return $this;
    }

    public function getHasCar(): ?bool
    {
        return $this->hasCar;
    }

    public function setHasCar(bool $hasCar): self
    {
        $this->hasCar = $hasCar;

        return $this;
    }

    public function getNumberOfCars(): ?int
    {
        return $this->numberOfCars;
    }

    public function setNumberOfCars(?int $numberOfCars): self
    {
        $this->numberOfCars = $numberOfCars;

        return $this;
    }

    public function getCarYearOfManufacture(): ?int
    {
        return $this->carYearOfManufacture;
    }

    public function setCarYearOfManufacture(?int $carYearOfManufacture): self
    {
        $this->carYearOfManufacture = $carYearOfManufacture;

        return $this;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNumberOfCars(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHasCar() === true) {
            if (empty($this->getNumberOfCars())) {
                $context->buildViolation('required_when_client_has_a_car')
                    ->atPath('numberOfCars')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateCarYearOfManufacture(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHasCar() === true) {
            if (empty($this->getCarYearOfManufacture())) {
                $context->buildViolation('required_when_client_has_a_car')
                    ->atPath('carYearOfManufacture')
                    ->addViolation();
            }
        }
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function toArray(): array
    {
        return [
            'hasProperty' => PropertyType::getTranslationKey($this->hasProperty),
            'hasCar' => $this->hasCar,
            'numberOfCars' => $this->numberOfCars,
            'carYearOfManufacture' => $this->carYearOfManufacture,
        ];
    }
}
