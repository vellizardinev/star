<?php

namespace App\Entity\OnlineApplication;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\ConfirmationRepository")
 */
class Confirmation
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", mappedBy="confirmation", cascade={"persist", "remove"})
     */
    private $onlineApplication;

    /**
     * @ORM\Column(type="boolean")
     */
    private $agreedToProcessingOfPersonalData = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnlineApplication(): ?OnlineApplication
    {
        return $this->onlineApplication;
    }

    public function setOnlineApplication(?OnlineApplication $onlineApplication): self
    {
        $this->onlineApplication = $onlineApplication;

        // set (or unset) the owning side of the relation if necessary
        $newConfirmation = null === $onlineApplication ? null : $this;
        if ($onlineApplication->getConfirmation() !== $newConfirmation) {
            $onlineApplication->setConfirmation($newConfirmation);
        }

        return $this;
    }

    public function getAgreedToProcessingOfPersonalData(): ?bool
    {
        return $this->agreedToProcessingOfPersonalData;
    }

    public function setAgreedToProcessingOfPersonalData(bool $agreedToProcessingOfPersonalData): self
    {
        $this->agreedToProcessingOfPersonalData = $agreedToProcessingOfPersonalData;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'agreedToProcessingOfPersonalData' => $this->agreedToProcessingOfPersonalData,
        ];
    }
}
