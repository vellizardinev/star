<?php

namespace App\Entity\OnlineApplication;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\UserPaymentInstrumentRepository")
 */
class UserPaymentInstrument
{
    const NEW_CARD = 'new_card';

    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $instrumentInfo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $kycSessionId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OnlineApplication\CreditDetails", inversedBy="paymentInstruments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creditDetails;

    public function __construct(string $instrumentInfo, string $kycSessionId)
    {
        $this->instrumentInfo = $instrumentInfo;
        $this->kycSessionId = $kycSessionId;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInstrumentInfo(): ?string
    {
        return $this->instrumentInfo;
    }

    public function setInstrumentInfo(string $instrumentInfo): self
    {
        $this->instrumentInfo = $instrumentInfo;

        return $this;
    }

    public function getKycSessionId(): ?string
    {
        return $this->kycSessionId;
    }

    public function setKycSessionId(string $kycSessionId): self
    {
        $this->kycSessionId = $kycSessionId;

        return $this;
    }

    public function getCreditDetails(): ?CreditDetails
    {
        return $this->creditDetails;
    }

    public function setCreditDetails(?CreditDetails $creditDetails): self
    {
        $this->creditDetails = $creditDetails;

        return $this;
    }
}
