<?php

namespace App\Entity\OnlineApplication;

use App\Service\RandomString;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use ReflectionException;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\OnlineApplicationRepository")
 * @UniqueEntity(fields={"hash"})
 */
class OnlineApplication
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $erpId;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $isSentToErp = false;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\CreditDetails", inversedBy="onlineApplication", cascade={"persist", "remove"})
     */
    private $creditDetails;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\PersonalInfo", inversedBy="onlineApplication", cascade={"persist", "remove"})
     */
    private $personalInfo;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\UploadedDocuments", inversedBy="onlineApplication", cascade={"persist", "remove"})
     */
    private $uploadedDocuments;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\Address", inversedBy="onlineApplication", cascade={"persist", "remove"})
     */
    private $address;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\WorkDetails", inversedBy="onlineApplication", cascade={"persist", "remove"})
     */
    private $workDetails;

    /**
     * @ORM\OneToOne(targetEntity="Expenses", inversedBy="onlineApplication", cascade={"persist", "remove"})
     */
    private $incomesAndExpenses;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\Property", inversedBy="onlineApplication", cascade={"persist", "remove"})
     */
    private $property;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\AdditionalInfo", inversedBy="onlineApplication", cascade={"persist", "remove"})
     */
    private $additionalInfo;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\ContactPerson", inversedBy="onlineApplication", cascade={"persist", "remove"})
     */
    private $contactPerson;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\Confirmation", inversedBy="onlineApplication", cascade={"persist", "remove"})
     */
    private $confirmation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCompleted = false;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hash;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $apiResponse;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $jsonData;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $previousApplicationData = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $kycSessionId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $kycUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cardGuid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRefinance = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="change", field="isCompleted", value=true)
     */
    private $completedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasSentReminderEmail = false;

    /**
     * OnlineApplication constructor.
     */
    public function __construct()
    {
        $this->hash = RandomString::generate(16);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

	public function getErpId(): ?string
                                                      	{
                                                      		return $this->erpId;
                                                      	}

	public function setErpId(?string $erpId): self
                                                      	{
                                                      		$this->erpId = $erpId;
                                                      
                                                      		return $this;
                                                      	}

	public function getIsSentToErp(): ?bool
                                                      	{
                                                      		return $this->isSentToErp;
                                                      	}

	public function setIsSentToErp(bool $isSentToErp): self
                                                      	{
                                                      		$this->isSentToErp = $isSentToErp;
                                                      
                                                      		return $this;
                                                      	}

    public function getCreditDetails(): ?CreditDetails
    {
        return $this->creditDetails;
    }

    public function setCreditDetails(?CreditDetails $creditDetails): self
    {
        $this->creditDetails = $creditDetails;

        return $this;
    }

    public function getPersonalInfo(): ?PersonalInfo
    {
        return $this->personalInfo;
    }

    public function setPersonalInfo(?PersonalInfo $personalInfo): self
    {
        $this->personalInfo = $personalInfo;

        return $this;
    }

    public function getUploadedDocuments(): ?UploadedDocuments
    {
        return $this->uploadedDocuments;
    }

    public function setUploadedDocuments(?UploadedDocuments $uploadedDocuments): self
    {
        $this->uploadedDocuments = $uploadedDocuments;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getWorkDetails(): ?WorkDetails
    {
        return $this->workDetails;
    }

    public function setWorkDetails(?WorkDetails $workDetails): self
    {
        $this->workDetails = $workDetails;

        return $this;
    }

    public function getIncomesAndExpenses(): ?Expenses
    {
        return $this->incomesAndExpenses;
    }

    public function setIncomesAndExpenses(?Expenses $incomesAndExpenses): self
    {
        $this->incomesAndExpenses = $incomesAndExpenses;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getAdditionalInfo(): ?AdditionalInfo
    {
        return $this->additionalInfo;
    }

    public function setAdditionalInfo(?AdditionalInfo $additionalInfo): self
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

    public function getContactPerson(): ?ContactPerson
    {
        return $this->contactPerson;
    }

    public function setContactPerson(?ContactPerson $contactPerson): self
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    public function getConfirmation(): ?Confirmation
    {
        return $this->confirmation;
    }

    public function setConfirmation(?Confirmation $confirmation): self
    {
        $this->confirmation = $confirmation;

        return $this;
    }

    public function uncompletedStepBefore(string $currentStep): string
    {
        $steps = [
            'personalInfo' => $this->getPersonalInfo(),
            'address' => $this->getAddress(),
            'contactPerson' => $this->getContactPerson(),
            'additionalInfo' => $this->getAdditionalInfo(),
            'confirmation' => $this->getConfirmation(),
        ];

        foreach ($steps as $stepName => $stepValue) {
            if ($stepName === $currentStep) {
                return $stepName;
            }

            if ($stepValue === null) {
                return $stepName;
            }
        }

        return $currentStep;
    }

    public function isReadyToFinalise(): bool
    {
        $uncompletedStep = $this->uncompletedStepBefore('confirmation');

        if ($uncompletedStep === 'confirmation') {
            return true;
        }

        return false;
    }

    public function getIsCompleted(): ?bool
    {
        return $this->isCompleted;
    }

    public function setIsCompleted(bool $isCompleted): self
    {
        $this->isCompleted = $isCompleted;

        return $this;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

	public function getApiResponse(): ?string
                                                      	{
                                                      		return $this->apiResponse;
                                                      	}

	public function setApiResponse(?string $apiResponse): self
                                                      	{
                                                      		$this->apiResponse = $apiResponse;
                                                      
                                                      		return $this;
                                                      	}

	public function getJsonData(): ?string
                                                      	{
                                                      		return $this->jsonData;
                                                      	}

	public function setJsonData(?string $jsonData): self
                                                      	{
                                                      		$this->jsonData = $jsonData;
                                                      
                                                      		return $this;
                                                      	}

    /**
     * @return array
     * @throws ReflectionException
     */
    public function toArray(): array
    {
        $result = array_merge(
            $this->getAdditionalInfo()->toArray(),
            $this->getAddress()->toArray(),
            $this->getConfirmation()->toArray(),
            $this->getContactPerson()->toArray(),
            $this->getCreditDetails()->toArray(),
            $this->getIncomesAndExpenses()->toArray(),
            $this->getPersonalInfo()->toArray(),
            $this->getProperty()->toArray(),
            $this->getWorkDetails()->toArray()
        );

        $result['createdAt'] = $this->getCreatedAt();
        $result['updatedAt'] = $this->getUpdatedAt();

        return $result;
    }

    public function getPreviousApplicationData(): ?array
    {
        return $this->previousApplicationData;
    }

    public function setPreviousApplicationData(?array $previousApplicationData): self
    {
        $this->previousApplicationData = $previousApplicationData;

        return $this;
    }

    public function getKycSessionId(): ?string
    {
        return $this->kycSessionId;
    }

    public function setKycSessionId(?string $kycSessionId): self
    {
        $this->kycSessionId = $kycSessionId;

        return $this;
    }

    public function getKycUrl(): ?string
    {
        return $this->kycUrl;
    }

    public function setKycUrl(?string $kycUrl): self
    {
        $this->kycUrl = $kycUrl;

        return $this;
    }

    public function getCardGuid(): ?string
    {
        return $this->cardGuid;
    }

    public function setCardGuid(?string $cardGuid): self
    {
        $this->cardGuid = $cardGuid;

        return $this;
    }

    public function getIsRefinance(): ?bool
    {
        return $this->isRefinance;
    }

    public function setIsRefinance(?bool $isRefinance): self
    {
        $this->isRefinance = $isRefinance;

        return $this;
    }

    public function getCompletedAt(): ?\DateTimeInterface
    {
        return $this->completedAt;
    }

    public function setCompletedAt(?\DateTimeInterface $completedAt): self
    {
        $this->completedAt = $completedAt;

        return $this;
    }

    public function completedAtOrUpdatedAt()
    {
        if ($this->completedAt) {
            return $this->completedAt;
        }

        return $this->updatedAt;
    }

    public function getHasSentReminderEmail(): ?bool
    {
        return $this->hasSentReminderEmail;
    }

    public function setHasSentReminderEmail(?bool $hasSentReminderEmail): self
    {
        $this->hasSentReminderEmail = $hasSentReminderEmail;

        return $this;
    }
}
