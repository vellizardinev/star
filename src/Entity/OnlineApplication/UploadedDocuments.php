<?php

namespace App\Entity\OnlineApplication;

use App\Entity\Document;
use App\Enum\PassportTypes;
use App\Enum\PensionCertificateTypes;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\UploadedDocumentsRepository")
 */
class UploadedDocuments
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", mappedBy="uploadedDocuments", cascade={"persist", "remove"})
     */
    private $onlineApplication;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $passportType;

    /** @var UploadedFile */
    public $individualTaxNumberUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="individual_tax_number_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $individualTaxNumber;

    /** @var UploadedFile */
    public $selfieUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="selfie_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $selfie;

    /** @var UploadedFile */
    public $oldStylePassportFirstPageUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="old_style_passport_first_page_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $oldStylePassportFirstPage;

    /** @var UploadedFile */
    public $oldStylePassportSecondPageUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="old_style_passport_second_page_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $oldStylePassportSecondPage;

    /** @var UploadedFile */
    public $oldStylePassportPhotoAt25Uploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="old_style_passport_photo_at_25_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $oldStylePassportPhotoAt25;

    /** @var UploadedFile */
    public $oldStylePassportPastedPhotoAt25Uploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="old_style_passport_pasted_photo_at_25_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $oldStylePassportPastedPhotoAt25;

    /** @var UploadedFile */
    public $oldStylePassportPhotoAt45Uploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="old_style_passport_photo_at_45_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $oldStylePassportPhotoAt45;

    /** @var UploadedFile */
    public $oldStylePassportPastedPhotoAt45Uploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="old_style_passport_pasted_photo_at_45_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $oldStylePassportPastedPhotoAt45;

    /** @var UploadedFile */
    public $oldStylePassportProofOfResidenceUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="old_style_passport_proof_of_residence_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $oldStylePassportProofOfResidence;

    /** @var UploadedFile */
    public $oldStylePassportSelfieWithPassportUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="old_style_passport_selfie_with_passport_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $oldStylePassportSelfieWithPassport;

    /** @var UploadedFile */
    public $newStylePassportPhotoPageUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="new_style_passport_photo_page_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $newStylePassportPhotoPage;

    /** @var UploadedFile */
    public $newStylePassportValidityPageUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="new_style_passport_validity_page_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $newStylePassportValidityPage;

    /** @var UploadedFile */
    public $newStylePassportRegistrationAppendixUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="new_style_passport_registration_appendix_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $newStylePassportRegistrationAppendix;

    /** @var UploadedFile */
    public $newStylePassportSelfieWithIdCardUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="new_style_passport_selfie_with_id_card_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $newStylePassportSelfieWithIdCard;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pensionCertificateType;

    /** @var UploadedFile */
    public $oldStylePensionCertificateFirstSpreadUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="old_style_pension_certificate_first_spread_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $oldStylePensionCertificateFirstSpread;

    /** @var UploadedFile */
    public $oldStylePensionCertificateValidityRecordUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="old_style_pension_certificate_validity_record_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $oldStylePensionCertificateValidityRecord;

    /** @var UploadedFile */
    public $newStylePensionCertificatePhotoAndValiditySpreadUploaded;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="new_style_pension_certificate_photo_and_validity_spread_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $newStylePensionCertificatePhotoAndValiditySpread;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnlineApplication(): ?OnlineApplication
    {
        return $this->onlineApplication;
    }

    public function setOnlineApplication(?OnlineApplication $onlineApplication): self
    {
        $this->onlineApplication = $onlineApplication;

        // set (or unset) the owning side of the relation if necessary
        $newUploadedDocuments = null === $onlineApplication ? null : $this;
        if ($onlineApplication->getUploadedDocuments() !== $newUploadedDocuments) {
            $onlineApplication->setUploadedDocuments($newUploadedDocuments);
        }

        return $this;
    }

    public function getPassportType(): ?string
    {
        return $this->passportType;
    }

    public function setPassportType(string $passportType): self
    {
        $this->passportType = $passportType;

        return $this;
    }

    public function getIndividualTaxNumber(): ?Document
    {
        return $this->individualTaxNumber;
    }

    public function setIndividualTaxNumber(?Document $individualTaxNumber): self
    {
        $this->individualTaxNumber = $individualTaxNumber;

        return $this;
    }

    public function getSelfie(): ?Document
    {
        return $this->selfie;
    }

    public function setSelfie(?Document $selfie): self
    {
        $this->selfie = $selfie;

        return $this;
    }

    public function getOldStylePassportFirstPage(): ?Document
    {
        return $this->oldStylePassportFirstPage;
    }

    public function setOldStylePassportFirstPage(?Document $oldStylePassportFirstPage): self
    {
        $this->oldStylePassportFirstPage = $oldStylePassportFirstPage;

        return $this;
    }

    public function getOldStylePassportSecondPage(): ?Document
    {
        return $this->oldStylePassportSecondPage;
    }

    public function setOldStylePassportSecondPage(?Document $oldStylePassportSecondPage): self
    {
        $this->oldStylePassportSecondPage = $oldStylePassportSecondPage;

        return $this;
    }

    public function getOldStylePassportPhotoAt25(): ?Document
    {
        return $this->oldStylePassportPhotoAt25;
    }

    public function setOldStylePassportPhotoAt25(?Document $oldStylePassportPhotoAt25): self
    {
        $this->oldStylePassportPhotoAt25 = $oldStylePassportPhotoAt25;

        return $this;
    }

    public function getOldStylePassportPastedPhotoAt25(): ?Document
    {
        return $this->oldStylePassportPastedPhotoAt25;
    }

    public function setOldStylePassportPastedPhotoAt25(?Document $oldStylePassportPastedPhotoAt25): self
    {
        $this->oldStylePassportPastedPhotoAt25 = $oldStylePassportPastedPhotoAt25;

        return $this;
    }

    public function getOldStylePassportPhotoAt45(): ?Document
    {
        return $this->oldStylePassportPhotoAt45;
    }

    public function setOldStylePassportPhotoAt45(?Document $oldStylePassportPhotoAt45): self
    {
        $this->oldStylePassportPhotoAt45 = $oldStylePassportPhotoAt45;

        return $this;
    }

    public function getOldStylePassportPastedPhotoAt45(): ?Document
    {
        return $this->oldStylePassportPastedPhotoAt45;
    }

    public function setOldStylePassportPastedPhotoAt45(?Document $oldStylePassportPastedPhotoAt45): self
    {
        $this->oldStylePassportPastedPhotoAt45 = $oldStylePassportPastedPhotoAt45;

        return $this;
    }

    public function getOldStylePassportProofOfResidence(): ?Document
    {
        return $this->oldStylePassportProofOfResidence;
    }

    public function setOldStylePassportProofOfResidence(?Document $oldStylePassportProofOfResidence): self
    {
        $this->oldStylePassportProofOfResidence = $oldStylePassportProofOfResidence;

        return $this;
    }

    public function getOldStylePassportSelfieWithPassport(): ?Document
    {
        return $this->oldStylePassportSelfieWithPassport;
    }

    public function setOldStylePassportSelfieWithPassport(?Document $oldStylePassportSelfieWithPassport): self
    {
        $this->oldStylePassportSelfieWithPassport = $oldStylePassportSelfieWithPassport;

        return $this;
    }

    public function getNewStylePassportPhotoPage(): ?Document
    {
        return $this->newStylePassportPhotoPage;
    }

    public function setNewStylePassportPhotoPage(?Document $newStylePassportPhotoPage): self
    {
        $this->newStylePassportPhotoPage = $newStylePassportPhotoPage;

        return $this;
    }

    public function getNewStylePassportValidityPage(): ?Document
    {
        return $this->newStylePassportValidityPage;
    }

    public function setNewStylePassportValidityPage(?Document $newStylePassportValidityPage): self
    {
        $this->newStylePassportValidityPage = $newStylePassportValidityPage;

        return $this;
    }

    public function getNewStylePassportRegistrationAppendix(): ?Document
    {
        return $this->newStylePassportRegistrationAppendix;
    }

    public function setNewStylePassportRegistrationAppendix(?Document $newStylePassportRegistrationAppendix): self
    {
        $this->newStylePassportRegistrationAppendix = $newStylePassportRegistrationAppendix;

        return $this;
    }

    public function getNewStylePassportSelfieWithIdCard(): ?Document
    {
        return $this->newStylePassportSelfieWithIdCard;
    }

    public function setNewStylePassportSelfieWithIdCard(?Document $newStylePassportSelfieWithIdCard): self
    {
        $this->newStylePassportSelfieWithIdCard = $newStylePassportSelfieWithIdCard;

        return $this;
    }

    public function getPensionCertificateType(): ?string
    {
        return $this->pensionCertificateType;
    }

    public function setPensionCertificateType(string $pensionCertificateType): self
    {
        $this->pensionCertificateType = $pensionCertificateType;

        return $this;
    }

    public function getOldStylePensionCertificateFirstSpread(): ?Document
    {
        return $this->oldStylePensionCertificateFirstSpread;
    }

    public function setOldStylePensionCertificateFirstSpread(?Document $oldStylePensionCertificateFirstSpread): self
    {
        $this->oldStylePensionCertificateFirstSpread = $oldStylePensionCertificateFirstSpread;

        return $this;
    }

    public function getOldStylePensionCertificateValidityRecord(): ?Document
    {
        return $this->oldStylePensionCertificateValidityRecord;
    }

    public function setOldStylePensionCertificateValidityRecord(?Document $oldStylePensionCertificateValidityRecord): self
    {
        $this->oldStylePensionCertificateValidityRecord = $oldStylePensionCertificateValidityRecord;

        return $this;
    }

    public function getNewStylePensionCertificatePhotoAndValiditySpread(): ?Document
    {
        return $this->newStylePensionCertificatePhotoAndValiditySpread;
    }

    public function setNewStylePensionCertificatePhotoAndValiditySpread(?Document $newStylePensionCertificatePhotoAndValiditySpread): self
    {
        $this->newStylePensionCertificatePhotoAndValiditySpread = $newStylePensionCertificatePhotoAndValiditySpread;

        return $this;
    }

    public function age(): int
    {
        $pin = $this->getOnlineApplication()->getPersonalInfo()->getPersonalIdentificationNumber();

        if (strlen($pin) < 5) {
            return 0;
        }

        $days = intval(substr($pin, 0, 5));
        $dateOfBirth = Carbon::parse('01 Jan 1900')->addDays($days);
        $age = Carbon::now()->diffInDays($dateOfBirth) / 365;

        return intval($age);
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateIndividualTaxNumberUploaded(ExecutionContextInterface $context, $payload)
    {
        if (null === $this->individualTaxNumberUploaded && !$this->individualTaxNumber instanceof Document) {
            $context->buildViolation('you_need_to_select_file')
                ->atPath('individualTaxNumberUploaded')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateSelfieUploaded(ExecutionContextInterface $context, $payload)
    {
        if (null === $this->selfieUploaded && !$this->selfie instanceof Document) {
            $context->buildViolation('you_need_to_select_file')
                ->atPath('selfieUploaded')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportFirstPageUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if (null === $this->oldStylePassportFirstPageUploaded && !$this->oldStylePassportFirstPage instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePassportFirstPageUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportSecondPageUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if (null === $this->oldStylePassportSecondPageUploaded && !$this->oldStylePassportSecondPage instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePassportSecondPageUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportPhotoAt25Uploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if ($this->age() >= 25) {
                if (null === $this->oldStylePassportPhotoAt25Uploaded && !$this->oldStylePassportPhotoAt25 instanceof Document) {
                    $context->buildViolation('you_need_to_select_file')
                        ->atPath('oldStylePassportPhotoAt25Uploaded')
                        ->addViolation();
                }
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportPastedPhotoAt25Uploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if ($this->age() >= 25) {
                if (null === $this->oldStylePassportPastedPhotoAt25Uploaded && !$this->oldStylePassportPastedPhotoAt25 instanceof Document) {
                    $context->buildViolation('you_need_to_select_file')
                        ->atPath('oldStylePassportPastedPhotoAt25Uploaded')
                        ->addViolation();
                }
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportPhotoAt45Uploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if ($this->age() >= 45) {
                if (null === $this->oldStylePassportPhotoAt45Uploaded && !$this->oldStylePassportPhotoAt45 instanceof Document) {
                    $context->buildViolation('you_need_to_select_file')
                        ->atPath('oldStylePassportPhotoAt45Uploaded')
                        ->addViolation();
                }
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportPastedPhotoAt45Uploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if ($this->age() >= 45) {
                if (null === $this->oldStylePassportPastedPhotoAt45Uploaded && !$this->oldStylePassportPastedPhotoAt45 instanceof Document) {
                    $context->buildViolation('you_need_to_select_file')
                        ->atPath('oldStylePassportPastedPhotoAt45Uploaded')
                        ->addViolation();
                }
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportProofOfResidenceUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if (null === $this->oldStylePassportProofOfResidenceUploaded && !$this->oldStylePassportProofOfResidence instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePassportProofOfResidenceUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePassportSelfieWithPassportUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::OLD_STYLE) {
            if (null === $this->oldStylePassportSelfieWithPassportUploaded && !$this->oldStylePassportSelfieWithPassport instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePassportSelfieWithPassportUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNewStylePassportPhotoPageUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::NEW_STYLE) {
            if (null === $this->newStylePassportPhotoPageUploaded && !$this->newStylePassportPhotoPage instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('newStylePassportPhotoPageUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNewStylePassportValidityPageUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::NEW_STYLE) {
            if (null === $this->newStylePassportValidityPageUploaded && !$this->newStylePassportValidityPage instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('newStylePassportValidityPageUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNewStylePassportRegistrationAppendixUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::NEW_STYLE) {
            if (null === $this->newStylePassportRegistrationAppendixUploaded && !$this->newStylePassportRegistrationAppendix instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('newStylePassportRegistrationAppendixUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNewStylePassportSelfieWithIdCardUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPassportType() === PassportTypes::NEW_STYLE) {
            if (null === $this->newStylePassportSelfieWithIdCardUploaded && !$this->newStylePassportSelfieWithIdCard instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('newStylePassportSelfieWithIdCardUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validatePensionCertificateType(ExecutionContextInterface $context, $payload)
    {
        if ($this->getOnlineApplication()->getCreditDetails()->getProduct() === 'Pensioner') {
            if (null === $this->getPensionCertificateType()) {
                $context->buildViolation('field_is_required')
                    ->atPath('pensionCertificateType')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePensionCertificateFirstSpreadUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPensionCertificateType() === PensionCertificateTypes::OLD_STYLE) {
            if (null === $this->oldStylePensionCertificateFirstSpreadUploaded && !$this->oldStylePensionCertificateFirstSpread instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePensionCertificateFirstSpreadUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateOldStylePensionCertificateValidityRecordUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPensionCertificateType() === PensionCertificateTypes::OLD_STYLE) {
            if (null === $this->oldStylePensionCertificateValidityRecordUploaded && !$this->oldStylePensionCertificateValidityRecord instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('oldStylePensionCertificateValidityRecordUploaded')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateNewStylePensionCertificatePhotoAndValiditySpreadUploaded(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPensionCertificateType() === PensionCertificateTypes::NEW_STYLE) {
            if (null === $this->newStylePensionCertificatePhotoAndValiditySpreadUploaded && !$this->newStylePensionCertificatePhotoAndValiditySpread instanceof Document) {
                $context->buildViolation('you_need_to_select_file')
                    ->atPath('newStylePensionCertificatePhotoAndValiditySpreadUploaded')
                    ->addViolation();
            }
        }
    }
}
