<?php

namespace App\Entity\OnlineApplication;

use App\Entity\MultibyteUppercaseFirst;
use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\AddressRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Address
{
    use TimestampableEntity;
    use MultibyteUppercaseFirst;

    const MAX_LENGTH_STREET_STREET_NUMBER = 50;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", mappedBy="address", cascade={"persist", "remove"})
     */
    private $onlineApplication;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $permanentAddressArea;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $permanentAddressMunicipality;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $permanentAddressCity;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(min="5", max="5")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_5")
     */
    private $permanentAddressPostCode;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $permanentAddressNeighbourhood;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $permanentAddressBlockNumber;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Assert\Length(max="50", maxMessage="street_name_and_street_number_should_be_shorter")
     */
    private $permanentAddressStreet;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $permanentAddressEntrance;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\PositiveOrZero()
     * @Assert\Range(min="0", max="99")
     */
    private $permanentAddressFloor;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $permanentAddressApartment;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    private $currentAddressSameAsPermanentAddress = true;

    /**
     * @ORM\Column(type="text")
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $currentAddressArea;

    /**
     * @ORM\Column(type="text")
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $currentAddressMunicipality;

    /**
     * @ORM\Column(type="text")
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $currentAddressCity;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(min="5", max="5")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_5")
     */
    private $currentAddressPostCode;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $currentAddressNeighbourhood;

    /**
     * @ORM\Column(type="text")
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $currentAddressBlockNumber;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(max="50", maxMessage="street_name_and_street_number_should_be_shorter")
     */
    private $currentAddressStreet;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $currentAddressEntrance;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\PositiveOrZero()
     * @Assert\Range(min="0", max="99")
     */
    private $currentAddressFloor;

    /**
     * @ORM\Column(type="text")
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $currentAddressApartment;

    /**
     * @ORM\Column(type="datetime")
     */
    private $livingOnAddressFromDate;

    /**
     * Address constructor.
     */
    public function __construct()
    {
        $this->livingOnAddressFromDate = Carbon::now();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnlineApplication(): ?OnlineApplication
    {
        return $this->onlineApplication;
    }

    public function setOnlineApplication(?OnlineApplication $onlineApplication): self
    {
        $this->onlineApplication = $onlineApplication;

        // set (or unset) the owning side of the relation if necessary
        $newAddress = null === $onlineApplication ? null : $this;
        if ($onlineApplication->getAddress() !== $newAddress) {
            $onlineApplication->setAddress($newAddress);
        }

        return $this;
    }

    public function getPermanentAddressArea(): ?string
    {
        return $this->permanentAddressArea;
    }

    public function setPermanentAddressArea(?string $permanentAddressArea): self
    {
        $this->permanentAddressArea = $permanentAddressArea;

        return $this;
    }

    public function getPermanentAddressMunicipality(): ?string
    {
        return $this->permanentAddressMunicipality;
    }

    public function setPermanentAddressMunicipality(?string $permanentAddressMunicipality): self
    {
        $this->permanentAddressMunicipality = $permanentAddressMunicipality;

        return $this;
    }

    public function getPermanentAddressCity(): ?string
    {
        return $this->permanentAddressCity;
    }

    public function setPermanentAddressCity(?string $permanentAddressCity): self
    {
        $this->permanentAddressCity = $permanentAddressCity;

        return $this;
    }

    public function getPermanentAddressBlockNumber(): ?string
    {
        return $this->permanentAddressBlockNumber;
    }

    public function setPermanentAddressBlockNumber(?string $permanentAddressBlockNumber): self
    {
        $this->permanentAddressBlockNumber = $permanentAddressBlockNumber;

        return $this;
    }

    public function getPermanentAddressStreet(): ?string
    {
        return $this->permanentAddressStreet;
    }

    public function setPermanentAddressStreet(?string $permanentAddressStreet): self
    {
        $this->permanentAddressStreet = $permanentAddressStreet;

        return $this;
    }

    public function getPermanentAddressEntrance(): ?string
    {
        return $this->permanentAddressEntrance;
    }

    public function setPermanentAddressEntrance(?string $permanentAddressEntrance): self
    {
        $this->permanentAddressEntrance = $permanentAddressEntrance;

        return $this;
    }

    public function getPermanentAddressFloor(): ?string
    {
        return $this->permanentAddressFloor;
    }

    public function setPermanentAddressFloor(?string $permanentAddressFloor): self
    {
        $this->permanentAddressFloor = $permanentAddressFloor;

        return $this;
    }

    public function getPermanentAddressApartment(): ?string
    {
        return $this->permanentAddressApartment;
    }

    public function setPermanentAddressApartment(?string $permanentAddressApartment): self
    {
        $this->permanentAddressApartment = $permanentAddressApartment;

        return $this;
    }

    public function getPermanentAddressPostCode(): ?string
    {
        return $this->permanentAddressPostCode;
    }

    public function setPermanentAddressPostCode(?string $permanentAddressPostCode): self
    {
        $this->permanentAddressPostCode = $permanentAddressPostCode;

        return $this;
    }

    public function getPermanentAddressNeighbourhood(): ?string
    {
        return $this->permanentAddressNeighbourhood;
    }

    public function setPermanentAddressNeighbourhood(?string $permanentAddressNeighbourhood): self
    {
        $this->permanentAddressNeighbourhood = $permanentAddressNeighbourhood;

        return $this;
    }

    public function getCurrentAddressArea(): ?string
    {
        return $this->currentAddressArea;
    }

    public function setCurrentAddressArea(?string $currentAddressArea): self
    {
        $this->currentAddressArea = $currentAddressArea;

        return $this;
    }

    public function getCurrentAddressMunicipality(): ?string
    {
        return $this->currentAddressMunicipality;
    }

    public function setCurrentAddressMunicipality(?string $currentAddressMunicipality): self
    {
        $this->currentAddressMunicipality = $currentAddressMunicipality;

        return $this;
    }

    public function getCurrentAddressCity(): ?string
    {
        return $this->currentAddressCity;
    }

    public function setCurrentAddressCity(?string $currentAddressCity): self
    {
        $this->currentAddressCity = $currentAddressCity;

        return $this;
    }

    public function getCurrentAddressBlockNumber(): ?string
    {
        return $this->currentAddressBlockNumber;
    }

    public function setCurrentAddressBlockNumber(?string $currentAddressBlockNumber): self
    {
        $this->currentAddressBlockNumber = $currentAddressBlockNumber;

        return $this;
    }

    public function getCurrentAddressStreet(): ?string
    {
        return $this->currentAddressStreet;
    }

    public function setCurrentAddressStreet(?string $currentAddressStreet): self
    {
        $this->currentAddressStreet = $currentAddressStreet;

        return $this;
    }

    public function getCurrentAddressEntrance(): ?string
    {
        return $this->currentAddressEntrance;
    }

    public function setCurrentAddressEntrance(?string $currentAddressEntrance): self
    {
        $this->currentAddressEntrance = $currentAddressEntrance;

        return $this;
    }

    public function getCurrentAddressFloor(): ?string
    {
        return $this->currentAddressFloor;
    }

    public function setCurrentAddressFloor(?string $currentAddressFloor): self
    {
        $this->currentAddressFloor = $currentAddressFloor;

        return $this;
    }

    public function getCurrentAddressApartment(): ?string
    {
        return $this->currentAddressApartment;
    }

    public function setCurrentAddressApartment(?string $currentAddressApartment): self
    {
        $this->currentAddressApartment = $currentAddressApartment;

        return $this;
    }

    public function getCurrentAddressPostCode(): ?string
    {
        return $this->currentAddressPostCode;
    }

    public function setCurrentAddressPostCode(?string $currentAddressPostCode): self
    {
        $this->currentAddressPostCode = $currentAddressPostCode;

        return $this;
    }

    public function getCurrentAddressNeighbourhood(): ?string
    {
        return $this->currentAddressNeighbourhood;
    }

    public function setCurrentAddressNeighbourhood(?string $currentAddressNeighbourhood): self
    {
        $this->currentAddressNeighbourhood = $currentAddressNeighbourhood;

        return $this;
    }

    public function getLivingOnAddressFromDate(): ?DateTimeInterface
    {
        return $this->livingOnAddressFromDate;
    }

    public function setLivingOnAddressFromDate(?DateTimeInterface $livingOnAddressFromDate): self
    {
        $this->livingOnAddressFromDate = $livingOnAddressFromDate;

        return $this;
    }

    public function getCurrentAddressSameAsPermanentAddress(): ?bool
    {
        return $this->currentAddressSameAsPermanentAddress;
    }

    public function setCurrentAddressSameAsPermanentAddress(?bool $currentAddressSameAsPermanentAddress): self
    {
        $this->currentAddressSameAsPermanentAddress = $currentAddressSameAsPermanentAddress;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateCurrentAddressFields()
    {
        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            return;
        }

        $this->setCurrentAddressArea($this->getPermanentAddressArea());
        $this->setCurrentAddressMunicipality($this->getPermanentAddressMunicipality());
        $this->setCurrentAddressCity($this->getPermanentAddressCity());
        $this->setCurrentAddressPostCode($this->getPermanentAddressPostCode());
        $this->setCurrentAddressNeighbourhood($this->getPermanentAddressNeighbourhood());
        $this->setCurrentAddressStreet($this->getPermanentAddressStreet());
        $this->setCurrentAddressBlockNumber($this->getPermanentAddressBlockNumber());
        $this->setCurrentAddressEntrance($this->getPermanentAddressEntrance());
        $this->setCurrentAddressFloor($this->getPermanentAddressFloor());
        $this->setCurrentAddressApartment($this->getPermanentAddressApartment());
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateCasing()
    {
        $this->permanentAddressArea = $this->mbUcFirst($this->permanentAddressArea);
        $this->currentAddressArea = $this->mbUcFirst($this->currentAddressArea);

        $this->permanentAddressMunicipality = $this->mbUcFirst($this->permanentAddressMunicipality);
        $this->currentAddressMunicipality = $this->mbUcFirst($this->currentAddressMunicipality);

        $this->permanentAddressPostCode = $this->mbUcFirst($this->permanentAddressPostCode);
        $this->currentAddressPostCode = $this->mbUcFirst($this->currentAddressPostCode);

        $this->permanentAddressCity = $this->mbUcFirst($this->permanentAddressCity);
        $this->currentAddressCity = $this->mbUcFirst($this->currentAddressCity);

        $this->permanentAddressNeighbourhood = $this->mbUcFirst($this->permanentAddressNeighbourhood);
        $this->currentAddressNeighbourhood = $this->mbUcFirst($this->currentAddressNeighbourhood);

        $this->permanentAddressStreet = $this->mbUcFirst($this->permanentAddressStreet);
        $this->currentAddressStreet = $this->mbUcFirst($this->currentAddressStreet);

        $this->permanentAddressBlockNumber = $this->mbUcFirst($this->permanentAddressBlockNumber);
        $this->currentAddressBlockNumber = $this->mbUcFirst($this->currentAddressBlockNumber);

        $this->permanentAddressEntrance = $this->mbUcFirst($this->permanentAddressEntrance);
        $this->currentAddressEntrance = $this->mbUcFirst($this->currentAddressEntrance);

        $this->permanentAddressApartment = $this->mbUcFirst($this->permanentAddressApartment);
        $this->currentAddressApartment = $this->mbUcFirst($this->currentAddressApartment);
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateCurrentAddressFields(ExecutionContextInterface $context, $payload)
    {
        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressArea())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressArea')
                    ->addViolation();
            }
        }

        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressMunicipality())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressMunicipality')
                    ->addViolation();
            }
        }

        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressCity())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressCity')
                    ->addViolation();
            }
        }

        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressStreet())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressStreet')
                    ->addViolation();
            }
        }

        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressBlockNumber())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressBlockNumber')
                    ->addViolation();
            }
        }

        if ($this->getCurrentAddressSameAsPermanentAddress() === false) {
            if (empty($this->getCurrentAddressApartment())) {
                $context->buildViolation('field_is_required')
                    ->atPath('currentAddressApartment')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateStreetAndBlockNumberLength(ExecutionContextInterface $context, $payload)
    {
        if (mb_strlen($this->getPermanentAddressStreet() . $this->permanentAddressBlockNumber) > self::MAX_LENGTH_STREET_STREET_NUMBER) {
            $context->buildViolation('street_name_and_street_number_should_be_shorter')
                ->atPath('permanentAddressStreet')
                ->addViolation();
        }

        if (mb_strlen($this->getCurrentAddressStreet() . $this->currentAddressBlockNumber) > self::MAX_LENGTH_STREET_STREET_NUMBER) {
            $context->buildViolation('street_name_and_street_number_should_be_shorter')
                ->atPath('currentAddressStreet')
                ->addViolation();
        }
    }

    public function toArray(): array
    {
        return [
            'permanentAddressArea' => $this->permanentAddressArea,
            'permanentAddressMunicipality' => $this->permanentAddressMunicipality,
            'permanentAddressCity' => $this->permanentAddressCity,
            'permanentAddressPostCode' => $this->permanentAddressPostCode,
            'permanentAddressNeighbourhood' => $this->permanentAddressNeighbourhood,
            'permanentAddressBlockNumber' => $this->permanentAddressBlockNumber,
            'permanentAddressStreet' => $this->permanentAddressStreet,
            'permanentAddressEntrance' => $this->permanentAddressEntrance,
            'permanentAddressFloor' => $this->permanentAddressFloor,
            'permanentAddressApartment' => $this->permanentAddressApartment,
            'currentAddressSameAsPermanentAddress' => $this->currentAddressSameAsPermanentAddress,
            'currentAddressArea' => $this->currentAddressArea,
            'currentAddressMunicipality' => $this->currentAddressMunicipality,
            'currentAddressCity' => $this->currentAddressCity,
            'currentAddressPostCode' => $this->currentAddressPostCode,
            'currentAddressNeighbourhood' => $this->currentAddressNeighbourhood,
            'currentAddressBlockNumber' => $this->currentAddressBlockNumber,
            'currentAddressStreet' => $this->currentAddressStreet,
            'currentAddressEntrance' => $this->currentAddressEntrance,
            'currentAddressFloor' => $this->currentAddressFloor,
            'currentAddressApartment' => $this->currentAddressApartment,
            'livingOnAddressFromDate' => $this->livingOnAddressFromDate,
        ];
    }

    public static function fromPreviousApplicationData(?array $data): self
    {
        $object = new self;

        if (empty($data)) {
            return $object;
        }

        $object
            ->setPermanentAddressArea($data['ClientPassportAddress']['Area'])
            ->setPermanentAddressMunicipality($data['ClientPassportAddress']['Municipality'])
            ->setPermanentAddressCity($data['ClientPassportAddress']['City'])
            ->setPermanentAddressPostCode($data['ClientPassportAddress']['PostCode'])
            ->setPermanentAddressStreet($data['ClientPassportAddress']['Street'])
            ->setPermanentAddressBlockNumber($data['ClientPassportAddress']['BlockNumber'])
            ->setPermanentAddressApartment($data['ClientPassportAddress']['Apartment'])

            ->setCurrentAddressArea($data['ClientCurrentAddress']['Area'])
            ->setCurrentAddressMunicipality($data['ClientCurrentAddress']['Municipality'])
            ->setCurrentAddressCity($data['ClientCurrentAddress']['City'])
            ->setCurrentAddressPostCode($data['ClientCurrentAddress']['PostCode'])
            ->setCurrentAddressStreet($data['ClientCurrentAddress']['Street'])
            ->setCurrentAddressBlockNumber($data['ClientCurrentAddress']['BlockNumber'])
            ->setCurrentAddressApartment($data['ClientCurrentAddress']['Apartment'])
        ;

        return $object;
    }
}
