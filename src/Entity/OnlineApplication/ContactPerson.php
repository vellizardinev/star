<?php

namespace App\Entity\OnlineApplication;

use App\Entity\MultibyteUppercaseFirst;
use App\Enum\RelationshipTypes;
use App\Service\MobileApi\ConvertsDatesToString;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use ReflectionException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\ContactPersonRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ContactPerson
{
    use TimestampableEntity;
    use MultibyteUppercaseFirst;
    use ConvertsDatesToString;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", mappedBy="contactPerson", cascade={"persist", "remove"})
     */
    private $onlineApplication;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $contactPersonFirstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $contactPersonMiddleName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $contactPersonLastName;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     */
    private $contactPersonDateOfBirth;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="9", max="9")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_9")
     */
    private $contactPersonMobileTelephone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactPersonRelationship;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnlineApplication(): ?OnlineApplication
    {
        return $this->onlineApplication;
    }

    public function setOnlineApplication(?OnlineApplication $onlineApplication): self
    {
        $this->onlineApplication = $onlineApplication;

        // set (or unset) the owning side of the relation if necessary
        $newContactPerson = null === $onlineApplication ? null : $this;
        if ($onlineApplication->getContactPerson() !== $newContactPerson) {
            $onlineApplication->setContactPerson($newContactPerson);
        }

        return $this;
    }

    public function getContactPersonFirstName(): ?string
    {
        return $this->contactPersonFirstName;
    }

    public function setContactPersonFirstName(string $contactPersonFirstName): self
    {
        $this->contactPersonFirstName = $contactPersonFirstName;

        return $this;
    }

    public function getContactPersonMiddleName(): ?string
    {
        return $this->contactPersonMiddleName;
    }

    public function setContactPersonMiddleName(string $contactPersonMiddleName): self
    {
        $this->contactPersonMiddleName = $contactPersonMiddleName;

        return $this;
    }

    public function getContactPersonLastName(): ?string
    {
        return $this->contactPersonLastName;
    }

    public function setContactPersonLastName(string $contactPersonLastName): self
    {
        $this->contactPersonLastName = $contactPersonLastName;

        return $this;
    }

    public function getContactPersonDateOfBirth(): ?DateTimeInterface
    {
        return $this->contactPersonDateOfBirth;
    }

    public function setContactPersonDateOfBirth(?DateTimeInterface $contactPersonDateOfBirth): self
    {
        $this->contactPersonDateOfBirth = $contactPersonDateOfBirth;

        return $this;
    }

    public function getContactPersonMobileTelephone(): ?string
    {
        return $this->contactPersonMobileTelephone;
    }

    public function setContactPersonMobileTelephone(string $contactPersonMobileTelephone): self
    {
        $this->contactPersonMobileTelephone = $contactPersonMobileTelephone;

        return $this;
    }

    public function getContactPersonRelationship(): ?string
    {
        return $this->contactPersonRelationship;
    }

    public function setContactPersonRelationship(string $contactPersonRelationship): self
    {
        $this->contactPersonRelationship = $contactPersonRelationship;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateCasing()
    {
        $this->contactPersonFirstName = $this->mbUcFirst($this->contactPersonFirstName);
        $this->contactPersonMiddleName = $this->mbUcFirst($this->contactPersonMiddleName);
        $this->contactPersonLastName = $this->mbUcFirst($this->contactPersonLastName);
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function toArray(): array
    {
        return [
            'contactPersonFirstName' => $this->contactPersonFirstName,
            'contactPersonMiddleName' => $this->contactPersonMiddleName,
            'contactPersonLastName' => $this->contactPersonLastName,
            'contactPersonDateOfBirth' => $this->contactPersonDateOfBirth,
            'contactPersonMobileTelephone' => $this->contactPersonMobileTelephone,
            'contactPersonRelationship' => RelationshipTypes::getTranslationKey($this->contactPersonRelationship),
        ];
    }

    public static function fromPreviousApplicationData(?array $data): self
    {
        $object = new self;

        if (empty($data)) {
            return $object;
        }

        if (isset($data['PartnerFirstName']) && !empty($data['PartnerFirstName']))  $object->setContactPersonFirstName($data['PartnerFirstName']);
        if (isset($data['PartnerSecondName']) && !empty($data['PartnerSecondName']))  $object->setContactPersonMiddleName($data['PartnerSecondName']);
        if (isset($data['PartnerLastName']) && !empty($data['PartnerLastName']))  $object->setContactPersonLastName($data['PartnerLastName']);
        if (isset($data['PartnerDateOfBirth']) && !empty($data['PartnerDateOfBirth']))  $object->setContactPersonDateOfBirth($object->stringToDate($data['PartnerDateOfBirth']));
        if (isset($data['PartnerPhone']) && !empty($data['PartnerPhone']))  $object->setContactPersonMobileTelephone($object->telephoneWithoutLeadingZero($data['PartnerPhone']));
        if (isset($data['PartnerType']) && !empty($data['PartnerType']))  $object->setContactPersonRelationship($data['PartnerType']);

        return $object;
    }

    private function telephoneWithoutLeadingZero(string $telephone): string
    {
        return ltrim($telephone, '0');
    }
}
