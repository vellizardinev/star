<?php

namespace App\Entity\OnlineApplication;

use App\Enum\ActivityAreas;
use App\Enum\CreditPurposes;
use App\Enum\EducationTypes;
use App\Enum\FamilyStatuses;
use App\Enum\HomeTypes;
use App\Enum\HouseholdSizes;
use App\Enum\InformationSources;
use App\Enum\PositionTypes;
use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use ReflectionException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\AdditionalInfoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AdditionalInfo
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", mappedBy="additionalInfo", cascade={"persist", "remove"})
     */
    private $onlineApplication;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $industry = ActivityAreas::OTHER_SERVICES;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expirationDateOfEmploymentContract;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $typeOfWork = PositionTypes::ADMINISTRATIVE_SUPPORT;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $education = EducationTypes::BACHELOR_DEGREE;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $purposeOfCredit;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $typeOfHome = HomeTypes::OTHER;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 */
    private $householdSize;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $familyStatus;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $foundOutAboutTheCompanyFrom;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $monthlyIncome;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $otherIncome;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $bonusIncome = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $socialIncome = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $rentIncome = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $civilContractIncome = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $pension = 0;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $incomesTotal;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $iAmPep = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $iAmMilitaryPerson = false;

    /**
     * AdditionalInfo constructor.
     */
    public function __construct()
    {
        $this->expirationDateOfEmploymentContract = Carbon::now();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnlineApplication(): ?OnlineApplication
    {
        return $this->onlineApplication;
    }

    public function setOnlineApplication(?OnlineApplication $onlineApplication): self
    {
        $this->onlineApplication = $onlineApplication;

        // set (or unset) the owning side of the relation if necessary
        $newAdditionalInfo = null === $onlineApplication ? null : $this;
        if ($onlineApplication->getAdditionalInfo() !== $newAdditionalInfo) {
            $onlineApplication->setAdditionalInfo($newAdditionalInfo);
        }

        return $this;
    }

    public function getIndustry(): ?string
    {
        return $this->industry;
    }

    public function setIndustry(?string $industry): self
    {
        $this->industry = $industry;

        return $this;
    }

    public function getExpirationDateOfEmploymentContract(): ?DateTimeInterface
    {
        return $this->expirationDateOfEmploymentContract;
    }

    public function setExpirationDateOfEmploymentContract(?DateTimeInterface $expirationDateOfEmploymentContract): self
    {
        $this->expirationDateOfEmploymentContract = $expirationDateOfEmploymentContract;

        return $this;
    }

    public function getTypeOfWork(): ?string
    {
        return $this->typeOfWork;
    }

    public function setTypeOfWork(?string $typeOfWork): self
    {
        $this->typeOfWork = $typeOfWork;

        return $this;
    }

    public function getEducation(): ?string
    {
        return $this->education;
    }

    public function setEducation(?string $education): self
    {
        $this->education = $education;

        return $this;
    }

    public function getPurposeOfCredit(): ?string
    {
        return $this->purposeOfCredit;
    }

    public function setPurposeOfCredit(?string $purposeOfCredit): self
    {
        $this->purposeOfCredit = $purposeOfCredit;

        return $this;
    }

    public function getTypeOfHome(): ?string
    {
        return $this->typeOfHome;
    }

    public function setTypeOfHome(?string $typeOfHome): self
    {
        $this->typeOfHome = $typeOfHome;

        return $this;
    }

    public function getHouseholdSize(): ?string
    {
        return $this->householdSize;
    }

    public function setHouseholdSize(?string $householdSize): self
    {
        $this->householdSize = $householdSize;

        return $this;
    }

    public function getFamilyStatus(): ?string
    {
        return $this->familyStatus;
    }

    public function setFamilyStatus(?string $familyStatus): self
    {
        $this->familyStatus = $familyStatus;

        return $this;
    }

    public function getFoundOutAboutTheCompanyFrom(): ?string
    {
        return $this->foundOutAboutTheCompanyFrom;
    }

    public function setFoundOutAboutTheCompanyFrom(?string $foundOutAboutTheCompanyFrom): self
    {
        $this->foundOutAboutTheCompanyFrom = $foundOutAboutTheCompanyFrom;

        return $this;
    }

    public function getMonthlyIncome(): ?string
    {
        return $this->monthlyIncome;
    }

    public function setMonthlyIncome(?string $monthlyIncome): self
    {
        $this->monthlyIncome = $monthlyIncome;

        return $this;
    }

    public function getOtherIncome(): ?string
    {
        return $this->otherIncome;
    }

    public function setOtherIncome(?string $otherIncome): self
    {
        $this->otherIncome = $otherIncome;

        return $this;
    }

    public function getBonusIncome(): ?string
    {
        return $this->bonusIncome;
    }

    public function setBonusIncome(?string $bonusIncome): self
    {
        $this->bonusIncome = $bonusIncome;

        return $this;
    }

    public function getSocialIncome(): ?string
    {
        return $this->socialIncome;
    }

    public function setSocialIncome(?string $socialIncome): self
    {
        $this->socialIncome = $socialIncome;

        return $this;
    }

    public function getRentIncome(): ?string
    {
        return $this->rentIncome;
    }

    public function setRentIncome(?string $rentIncome): self
    {
        $this->rentIncome = $rentIncome;

        return $this;
    }

    public function getCivilContractIncome(): ?string
    {
        return $this->civilContractIncome;
    }

    public function setCivilContractIncome(?string $civilContractIncome): self
    {
        $this->civilContractIncome = $civilContractIncome;

        return $this;
    }

    public function getPension(): ?string
    {
        return $this->pension;
    }

    public function setPension(?string $pension): self
    {
        $this->pension = $pension;

        return $this;
    }

    public function getIncomesTotal(): ?string
    {
        return $this->incomesTotal;
    }

    public function setIncomesTotal(string $incomesTotal): self
    {
        $this->incomesTotal = $incomesTotal;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTotalIncome()
    {
        $this->incomesTotal = $this->getBonusIncome() +
            $this->getMonthlyIncome() +
            $this->getOtherIncome() +
            $this->getBonusIncome() +
            $this->getSocialIncome() +
            $this->getRentIncome() +
            $this->getCivilContractIncome() +
            $this->getPension()
        ;
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function toArray(): array
    {
        return [
            'industry' => ActivityAreas::getTranslationKey($this->industry),
            'expirationDateOfEmploymentContract' => $this->expirationDateOfEmploymentContract,
            'typeOfWork' => PositionTypes::getTranslationKey($this->typeOfWork),
            'education' => EducationTypes::getTranslationKey($this->education),
            'purposeOfCredit' => CreditPurposes::getTranslationKey($this->purposeOfCredit),
            'typeOfHome' => HomeTypes::getTranslationKey($this->typeOfHome),
            'householdSize' =>HouseholdSizes::getSize($this->householdSize),
            'familyStatus' => FamilyStatuses::getTranslationKey($this->familyStatus),
            'foundOutAboutTheCompanyFrom' => InformationSources::getTranslationKey($this->foundOutAboutTheCompanyFrom),
            'monthlyIncome' => $this->monthlyIncome,
            'otherIncome' => $this->otherIncome,
            'bonusIncome' => $this->bonusIncome,
            'socialIncome' => $this->socialIncome,
            'rentIncome' => $this->rentIncome,
            'civilContractIncome' => $this->civilContractIncome,
            'pension' => $this->pension,
            'incomesTotal' => $this->incomesTotal,
            'iAmPep' => $this->iAmPep,
            'iAmMilitaryPerson' => $this->iAmMilitaryPerson,
        ];
    }

    public static function fromPreviousApplicationData(?array $data): self
    {
        $object = new self;

        if (empty($data)) {
            return $object;
        }

        if (isset($data['ClientSallary']) && !empty($data['ClientSallary']))  $object->setMonthlyIncome($data['ClientSallary']);
        if (isset($data['CreditPurpose']) && !empty($data['CreditPurpose']))  $object->setPurposeOfCredit($data['CreditPurpose']);
        if (isset($data['FamilyCount']) && !empty($data['FamilyCount']))  $object->setHouseholdSize($data['FamilyCount']);
        if (isset($data['FamilyStatus']) && !empty($data['FamilyStatus']))  $object->setFamilyStatus($data['FamilyStatus']);

        if (isset($data['IncomeOther']) && !empty($data['IncomeOther']))  $object->setOtherIncome($data['IncomeOther']);
        if (isset($data['LearnedAboutTheCompanyFromGuid']) && !empty($data['LearnedAboutTheCompanyFromGuid']))  $object->setFoundOutAboutTheCompanyFrom($data['LearnedAboutTheCompanyFromGuid']);

        return $object;
    }

    public function getIAmPep(): ?bool
    {
        return $this->iAmPep;
    }

    public function setIAmPep(?bool $iAmPep): self
    {
        $this->iAmPep = $iAmPep;

        return $this;
    }

    public function getIAmMilitaryPerson(): ?bool
    {
        return $this->iAmMilitaryPerson;
    }

    public function setIAmMilitaryPerson(?bool $iAmMilitaryPerson): self
    {
        $this->iAmMilitaryPerson = $iAmMilitaryPerson;

        return $this;
    }
}
