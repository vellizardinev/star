<?php

namespace App\Entity\OnlineApplication;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\ExpensesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Expenses
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", mappedBy="incomesAndExpenses", cascade={"persist", "remove"})
     */
    private $onlineApplication;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesFoodAndClothes = 1500;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesKids = 0;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesRent = 0;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesTransport = 300;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesDrinksCigarettes = 0;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $householdExpensesOtherCredits = '0';

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesOtherCreditsInstalmentAmount = 0;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesHeating = 500;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesElectricityAndWater = 600;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesInternetAndTv = 200;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesTelephone = 150;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero()
     */
    private $householdExpensesOther = 300;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\PositiveOrZero()
     */
    private $expensesTotal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnlineApplication(): ?OnlineApplication
    {
        return $this->onlineApplication;
    }

    public function setOnlineApplication(?OnlineApplication $onlineApplication): self
    {
        $this->onlineApplication = $onlineApplication;

        // set (or unset) the owning side of the relation if necessary
        $newIncomesAndExpenses = null === $onlineApplication ? null : $this;
        if ($onlineApplication->getIncomesAndExpenses() !== $newIncomesAndExpenses) {
            $onlineApplication->setIncomesAndExpenses($newIncomesAndExpenses);
        }

        return $this;
    }

    public function getHouseholdExpensesFoodAndClothes(): ?string
    {
        return $this->householdExpensesFoodAndClothes;
    }

    public function setHouseholdExpensesFoodAndClothes(?string $householdExpensesFoodAndClothes): self
    {
        $this->householdExpensesFoodAndClothes = $householdExpensesFoodAndClothes;

        return $this;
    }

    public function getHouseholdExpensesOther(): ?string
    {
        return $this->householdExpensesOther;
    }

    public function setHouseholdExpensesOther(?string $householdExpensesOther): self
    {
        $this->householdExpensesOther = $householdExpensesOther;

        return $this;
    }

    public function getHouseholdExpensesKids(): ?float
    {
        return $this->householdExpensesKids;
    }

    public function setHouseholdExpensesKids(?float $householdExpensesKids): self
    {
        $this->householdExpensesKids = $householdExpensesKids;

        return $this;
    }

    public function getHouseholdExpensesRent(): ?float
    {
        return $this->householdExpensesRent;
    }

    public function setHouseholdExpensesRent(float $householdExpensesRent): self
    {
        $this->householdExpensesRent = $householdExpensesRent;

        return $this;
    }

    public function getHouseholdExpensesTransport(): ?float
    {
        return $this->householdExpensesTransport;
    }

    public function setHouseholdExpensesTransport(float $householdExpensesTransport): self
    {
        $this->householdExpensesTransport = $householdExpensesTransport;

        return $this;
    }

    public function getHouseholdExpensesDrinksCigarettes(): ?float
    {
        return $this->householdExpensesDrinksCigarettes;
    }

    public function setHouseholdExpensesDrinksCigarettes(float $householdExpensesDrinksCigarettes
    ): self {
        $this->householdExpensesDrinksCigarettes = $householdExpensesDrinksCigarettes;

        return $this;
    }

    public function getHouseholdExpensesOtherCredits(): ?string
    {
        return $this->householdExpensesOtherCredits;
    }

    public function setHouseholdExpensesOtherCredits(string $householdExpensesOtherCredits): self
    {
        $this->householdExpensesOtherCredits = $householdExpensesOtherCredits;

        return $this;
    }

    public function getHouseholdExpensesOtherCreditsInstalmentAmount(): ?float
    {
        return $this->householdExpensesOtherCreditsInstalmentAmount;
    }

    public function setHouseholdExpensesOtherCreditsInstalmentAmount(
        float $householdExpensesOtherCreditsInstalmentAmount
    ): self {
        $this->householdExpensesOtherCreditsInstalmentAmount = $householdExpensesOtherCreditsInstalmentAmount;

        return $this;
    }

    public function getHouseholdExpensesHeating(): ?float
    {
        return $this->householdExpensesHeating;
    }

    public function setHouseholdExpensesHeating(float $householdExpensesHeating): self
    {
        $this->householdExpensesHeating = $householdExpensesHeating;

        return $this;
    }

    public function getExpensesTotal(): ?string
    {
        return $this->expensesTotal;
    }

    public function setExpensesTotal(?string $expensesTotal): self
    {
        $this->expensesTotal = $expensesTotal;

        return $this;
    }

    public function getHouseholdExpensesElectricityAndWater(): ?float
    {
        return $this->householdExpensesElectricityAndWater;
    }

    public function setHouseholdExpensesElectricityAndWater(?float $householdExpensesElectricityAndWater): self
    {
        $this->householdExpensesElectricityAndWater = $householdExpensesElectricityAndWater;

        return $this;
    }

    public function getHouseholdExpensesInternetAndTv(): ?float
    {
        return $this->householdExpensesInternetAndTv;
    }

    public function setHouseholdExpensesInternetAndTv(?float $householdExpensesInternetAndTv): self
    {
        $this->householdExpensesInternetAndTv = $householdExpensesInternetAndTv;

        return $this;
    }

    public function getHouseholdExpensesTelephone(): ?float
    {
        return $this->householdExpensesTelephone;
    }

    public function setHouseholdExpensesTelephone(?float $householdExpensesTelephone): self
    {
        $this->householdExpensesTelephone = $householdExpensesTelephone;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTotalExpenses()
    {
        $this->expensesTotal =
            $this->getHouseholdExpensesFoodAndClothes() +
            $this->getHouseholdExpensesKids() +
            $this->getHouseholdExpensesElectricityAndWater() +
            $this->getHouseholdExpensesHeating() +
            $this->getHouseholdExpensesInternetAndTv() +
            $this->getHouseholdExpensesTelephone() +
            $this->getHouseholdExpensesRent() +
            $this->getHouseholdExpensesTransport() +
            $this->getHouseholdExpensesDrinksCigarettes() +
            $this->getHouseholdExpensesOtherCreditsInstalmentAmount() +
            $this->getHouseholdExpensesOther();
    }

    public function toArray(): array
    {
        return [
            'householdExpensesFoodAndClothes' => $this->householdExpensesFoodAndClothes,
            'householdExpensesKids' => $this->householdExpensesKids,
            'householdExpensesRent' => $this->householdExpensesRent,
            'householdExpensesTransport' => $this->householdExpensesTransport,
            'householdExpensesDrinksCigarettes' => $this->householdExpensesDrinksCigarettes,
            'householdExpensesOtherCredits' => $this->householdExpensesOtherCredits,
            'householdExpensesOtherCreditsInstalmentAmount' => $this->householdExpensesOtherCreditsInstalmentAmount,
            'householdExpensesHeating' => $this->householdExpensesHeating,
            'householdExpensesElectricityAndWater' => $this->householdExpensesElectricityAndWater,
            'householdExpensesInternetAndTv' => $this->householdExpensesInternetAndTv,
            'householdExpensesTelephone' => $this->householdExpensesTelephone,
            'householdExpensesOther' => $this->householdExpensesOther,
            'expensesTotal' => $this->expensesTotal,
        ];
    }
}
