<?php

namespace App\Entity\OnlineApplication;

use App\Entity\MultibyteUppercaseFirst;
use App\Enum\EmploymentTypes;
use App\Enum\FrequencyOfReceivingSalary;
use App\Enum\MethodsReceivingSalary;
use Doctrine\ORM\Mapping as ORM;
use ReflectionException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OnlineApplication\WorkDetailsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class WorkDetails
{
    use MultibyteUppercaseFirst;

	const INVALID = 'Invalid';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OnlineApplication\OnlineApplication", mappedBy="workDetails", cascade={"persist", "remove"})
     */
    private $onlineApplication;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @\Symfony\Component\Validator\Constraints\Length(max=50)
     */
    private $companyPosition;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $workAddressArea = self::INVALID;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $workAddressMunicipality = self::INVALID;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $workAddressCity = self::INVALID;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $workAddressPostCode = "00000";

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $workAddressNeighbourhood = self::INVALID;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $workAddressBlockNumber = 1;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $workAddressStreet = self::INVALID;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $workAddressEntrance = self::INVALID;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $workAddressFloor;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $workAddressApartment = 1;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Length(min="1", max="3")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_max_3")
     */
    private $workExperienceCurrentEmployer = 36;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     * @Assert\Length(min="1", max="3")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_max_3")
     */
    private $workExperienceTotal = 100;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $howSalaryIsReceived = MethodsReceivingSalary::BANK_ACCOUNT;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Positive()
     * @Assert\Range(min="1", max="31")
     */
    private $dayForReceivingSalary = 10;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $frequencyOfReceivingSalary = FrequencyOfReceivingSalary::MONTHLY;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="9", max="9")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_9")
     */
    private $companyTelephone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $employmentType;

    public function getCompanyTelephone(): ?string
    {
        return $this->companyTelephone;
    }

    public function setCompanyTelephone(?string $companyTelephone): self
    {
        $this->companyTelephone = $companyTelephone;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnlineApplication(): ?OnlineApplication
    {
        return $this->onlineApplication;
    }

    public function setOnlineApplication(?OnlineApplication $onlineApplication): self
    {
        $this->onlineApplication = $onlineApplication;

        // set (or unset) the owning side of the relation if necessary
        $newWorkDetails = null === $onlineApplication ? null : $this;
        if ($onlineApplication->getWorkDetails() !== $newWorkDetails) {
            $onlineApplication->setWorkDetails($newWorkDetails);
        }

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getCompanyPosition(): ?string
    {
        return $this->companyPosition;
    }

    public function setCompanyPosition(string $companyPosition): self
    {
        $this->companyPosition = $companyPosition;

        return $this;
    }

    public function getWorkAddressArea(): ?string
    {
        return $this->workAddressArea;
    }

    public function setWorkAddressArea(?string $workAddressArea): self
    {
        $this->workAddressArea = $workAddressArea;

        return $this;
    }

    public function getWorkAddressMunicipality(): ?string
    {
        return $this->workAddressMunicipality;
    }

    public function setWorkAddressMunicipality(?string $workAddressMunicipality): self
    {
        $this->workAddressMunicipality = $workAddressMunicipality;

        return $this;
    }

    public function getWorkAddressCity(): ?string
    {
        return $this->workAddressCity;
    }

    public function setWorkAddressCity(?string $workAddressCity): self
    {
        $this->workAddressCity = $workAddressCity;

        return $this;
    }

    public function getWorkAddressBlockNumber(): ?string
    {
        return $this->workAddressBlockNumber;
    }

    public function setWorkAddressBlockNumber(?string $workAddressBlockNumber): self
    {
        $this->workAddressBlockNumber = $workAddressBlockNumber;

        return $this;
    }

    public function getWorkAddressStreet(): ?string
    {
        return $this->workAddressStreet;
    }

    public function setWorkAddressStreet(?string $workAddressStreet): self
    {
        $this->workAddressStreet = $workAddressStreet;

        return $this;
    }

    public function getWorkAddressEntrance(): ?string
    {
        return $this->workAddressEntrance;
    }

    public function setWorkAddressEntrance(?string $workAddressEntrance): self
    {
        $this->workAddressEntrance = $workAddressEntrance;

        return $this;
    }

    public function getWorkAddressFloor(): ?string
    {
        return $this->workAddressFloor;
    }

    public function setWorkAddressFloor(?string $workAddressFloor): self
    {
        $this->workAddressFloor = $workAddressFloor;

        return $this;
    }

    public function getWorkAddressApartment(): ?string
    {
        return $this->workAddressApartment;
    }

    public function setWorkAddressApartment(?string $workAddressApartment): self
    {
        $this->workAddressApartment = $workAddressApartment;

        return $this;
    }

    public function getWorkAddressPostCode(): ?string
    {
        return $this->workAddressPostCode;
    }

    public function setWorkAddressPostCode(?string $workAddressPostCode): self
    {
        $this->workAddressPostCode = $workAddressPostCode;

        return $this;
    }

    public function getWorkAddressNeighbourhood(): ?string
    {
        return $this->workAddressNeighbourhood;
    }

    public function setWorkAddressNeighbourhood(?string $workAddressNeighbourhood): self
    {
        $this->workAddressNeighbourhood = $workAddressNeighbourhood;

        return $this;
    }

    public function getWorkExperienceCurrentEmployer(): ?int
    {
        return $this->workExperienceCurrentEmployer;
    }

    public function setWorkExperienceCurrentEmployer(?int $workExperienceCurrentEmployer): self
    {
        $this->workExperienceCurrentEmployer = $workExperienceCurrentEmployer;

        return $this;
    }

    public function getWorkExperienceTotal(): ?int
    {
        return $this->workExperienceTotal;
    }

    public function setWorkExperienceTotal(?int $workExperienceTotal): self
    {
        $this->workExperienceTotal = $workExperienceTotal;

        return $this;
    }

    public function getHowSalaryIsReceived(): ?string
    {
        return $this->howSalaryIsReceived;
    }

    public function setHowSalaryIsReceived(?string $howSalaryIsReceived): self
    {
        $this->howSalaryIsReceived = $howSalaryIsReceived;

        return $this;
    }

    public function getFrequencyOfReceivingSalary(): ?string
    {
        return $this->frequencyOfReceivingSalary;
    }

    public function setFrequencyOfReceivingSalary(string $frequencyOfReceivingSalary): self
    {
        $this->frequencyOfReceivingSalary = $frequencyOfReceivingSalary;

        return $this;
    }

    public function getDayForReceivingSalary(): ?int
    {
        return $this->dayForReceivingSalary;
    }

    public function setDayForReceivingSalary(?int $dayForReceivingSalary): self
    {
        $this->dayForReceivingSalary = $dayForReceivingSalary;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateCasing()
    {
        $this->workAddressCity = $this->mbUcFirst($this->workAddressCity);
        $this->workAddressNeighbourhood = $this->mbUcFirst($this->workAddressNeighbourhood);
        $this->workAddressStreet = $this->mbUcFirst($this->workAddressStreet);
        $this->workAddressBlockNumber = $this->mbUcFirst($this->workAddressBlockNumber);
        $this->workAddressEntrance = $this->mbUcFirst($this->workAddressEntrance);
        $this->workAddressApartment = $this->mbUcFirst($this->workAddressApartment);

        $this->companyName = $this->mbUcFirst($this->companyName);
        $this->companyPosition = $this->mbUcFirst($this->companyPosition);
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateFrequencyOfReceivingSalary(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHowSalaryIsReceived() === MethodsReceivingSalary::BANK_ACCOUNT ||
            $this->getHowSalaryIsReceived() === MethodsReceivingSalary::CASH) {
            if (empty($this->getFrequencyOfReceivingSalary())) {
                $context->buildViolation('field_required_when_salary_is_received_in_cash_or_bank_account')
                    ->atPath('frequencyOfReceivingSalary')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validateDayOfReceivingSalary(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHowSalaryIsReceived() === MethodsReceivingSalary::BANK_ACCOUNT ||
            $this->getHowSalaryIsReceived() === MethodsReceivingSalary::CASH) {
            if (empty($this->getDayForReceivingSalary())) {
                $context->buildViolation('field_required_when_salary_is_received_in_cash_or_bank_account')
                    ->atPath('dayForReceivingSalary')
                    ->addViolation();
            }
        }
    }

    public function getEmploymentType(): ?string
    {
        return $this->employmentType;
    }

    public function setEmploymentType(?string $employmentType): self
    {
        $this->employmentType = $employmentType;

        return $this;
    }

	public function resetFieldsWithDefaultValues()
	{
		if ($this->companyName === self::INVALID) {
			$this->companyName = null;
		}

		if ($this->workAddressCity === self::INVALID) {
			$this->workAddressCity = null;
		}

		if ($this->workAddressPostCode === '00000') {
			$this->workAddressPostCode = null;
		}

		if ($this->workAddressStreet === self::INVALID) {
			$this->workAddressStreet = null;
		}

		if ($this->workAddressBlockNumber === self::INVALID) {
			$this->workAddressBlockNumber = null;
		}

		if ($this->companyPosition === self::INVALID) {
			$this->companyPosition = null;
		}
	}

    /**
     * @return array
     * @throws ReflectionException
     */
    public function toArray(): array
    {
        return [
            'companyName' => $this->companyName,
            'companyPosition' => $this->companyPosition,
            'workAddressArea' => $this->workAddressArea,
            'workAddressMunicipality' => $this->workAddressMunicipality,
            'workAddressCity' => $this->workAddressCity,
            'workAddressPostCode' => $this->workAddressPostCode,
            'workAddressNeighbourhood' => $this->workAddressNeighbourhood,
            'workAddressBlockNumber' => $this->workAddressBlockNumber,
            'workAddressStreet' => $this->workAddressStreet,
            'workAddressEntrance' => $this->workAddressEntrance,
            'workAddressFloor' => $this->workAddressFloor,
            'workAddressApartment' => $this->workAddressApartment,
            'workExperienceCurrentEmployer' => $this->workExperienceCurrentEmployer,
            'workExperienceTotal' => $this->workExperienceTotal,
            'howSalaryIsReceived' => MethodsReceivingSalary::getTranslationKey($this->howSalaryIsReceived),
            'dayForReceivingSalary' => $this->dayForReceivingSalary,
            'frequencyOfReceivingSalary' => FrequencyOfReceivingSalary::getTranslationKey($this->frequencyOfReceivingSalary),
            'companyTelephone' => $this->companyTelephone,
            'employmentType' => EmploymentTypes::getTranslationKey($this->employmentType),
        ];
    }

    public static function fromPreviousApplicationData(?array $data): self
    {
        $object = new self;

        if (empty($data)) {
            return $object;
        }

        if (isset($data['Workplace']) && !empty($data['Workplace']))  $object->setCompanyName($data['Workplace']);
        if (isset($data['WorkPhone']) && !empty($data['WorkPhone']))  $object->setCompanyTelephone($object->telephoneWithoutLeadingZero($data['WorkPhone']));
        if (isset($data['WorkContract']) && !empty($data['WorkContract']))  $object->setEmploymentType($data['WorkContract']);
        if (isset($data['WorkPosition']) && !empty($data['WorkPosition']))  $object->setCompanyPosition($data['WorkPosition']);

        return $object;
    }

    private function telephoneWithoutLeadingZero(string $telephone): string
    {
        return ltrim($telephone, '0');
    }
}
