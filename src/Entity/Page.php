<?php

namespace App\Entity;

use App\Enum\PageTypes;
use App\Validator\Constraints\ValidPagePath;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Translatable\Translatable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @UniqueEntity(fields={"path"})
 */
class Page implements Translatable
{
    use TimestampableEntity;
    use HasSeoData;
    use HasVisibilityConfiguration;
    use ChecksVisibility;
    use TranslatableEntity;
    use HasSiteMapConfiguration;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     * @ValidPagePath()
     */
    private $name;

	/**
	 * @Gedmo\Slug(fields={"name"}, unique=true)
     * @Gedmo\Translatable()
	 * @ORM\Column(type="string", length=255)
	 * @ValidPagePath()
	 */
    private $path;

    /**
     * @ORM\OneToMany(targetEntity=ModuleItem::class, mappedBy="page", orphanRemoval=true)
     * @ORM\OrderBy({"positionIndex" = "ASC"})
     */
    private $moduleItems;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $internalName;

    public function __construct()
    {
        $this->moduleItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return Collection|ModuleItem[]
     */
    public function getModuleItems(): Collection
    {
        return $this->moduleItems;
    }

    public function addModuleItem(ModuleItem $moduleItem): self
    {
        if (!$this->moduleItems->contains($moduleItem)) {
            $this->moduleItems[] = $moduleItem;
            $moduleItem->setPage($this);
        }

        return $this;
    }

    public function removeModuleItem(ModuleItem $moduleItem): self
    {
        if ($this->moduleItems->contains($moduleItem)) {
            $this->moduleItems->removeElement($moduleItem);
            // set the owning side to null (unless already changed)
            if ($moduleItem->getPage() === $this) {
                $moduleItem->setPage(null);
            }
        }

        return $this;
    }

    public function getType(): string
    {
        return PageTypes::PAGE;
    }

    public function __toString()
    {
        return sprintf('%s [ID=%s]', $this->getName(), $this->getId());
    }

    public function getInternalName(): ?string
    {
        return $this->internalName;
    }

    public function setInternalName(?string $internalName): self
    {
        $this->internalName = $internalName;

        return $this;
    }
}
