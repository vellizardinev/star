<?php

namespace App\Entity;

use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HttpRequestRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\InheritanceType("SINGLE_TABLE")
 */
class HttpRequest
{
    const METHOD = 'GET';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $method = self::METHOD;

    /**
     * @ORM\Column(type="array")
     */
    private $options = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $success = false;

    /**
     * @ORM\Column(type="integer")
     */
    private $attempts = 0;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastAttemptedOn;

    /**
     * @ORM\Column(type="integer")
     */
    private $intervalInSeconds = 60;

    /**
     * @ORM\Column(type="integer")
     */
    private $multiplier = 0;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lastResponse;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxAttempts = 3;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lastResponseCode;

    /**
     * @ORM\Column(type="boolean")
     */
    private $completed = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $nextAttemptOn;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function getSuccess(): ?bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    public function getAttempts(): ?int
    {
        return $this->attempts;
    }

    public function setAttempts(int $attempts): self
    {
        $this->attempts = $attempts;

        return $this;
    }

    public function getLastAttemptedOn(): ?DateTimeInterface
    {
        return $this->lastAttemptedOn;
    }

    public function setLastAttemptedOn(?DateTimeInterface $lastAttemptedOn): self
    {
        $this->lastAttemptedOn = $lastAttemptedOn;

        return $this;
    }

    public function getIntervalInSeconds(): ?int
    {
        return $this->intervalInSeconds;
    }

    public function setIntervalInSeconds(int $intervalInSeconds): self
    {
        $this->intervalInSeconds = $intervalInSeconds;

        return $this;
    }

    public function getMultiplier(): ?int
    {
        return $this->multiplier;
    }

    public function setMultiplier(int $multiplier): self
    {
        $this->multiplier = $multiplier;

        return $this;
    }

    public function getLastResponse(): ?string
    {
        return $this->lastResponse;
    }

    public function setLastResponse(?string $lastResponse): self
    {
        $this->lastResponse = $lastResponse;

        return $this;
    }

    public function getMaxAttempts(): ?int
    {
        return $this->maxAttempts;
    }

    public function setMaxAttempts(int $maxAttempts): self
    {
        $this->maxAttempts = $maxAttempts;

        return $this;
    }

    public function addAttempt(): self
    {
        $this->attempts++;

        return $this;
    }

    public function getLastResponseCode(): ?int
    {
        return $this->lastResponseCode;
    }

    public function setLastResponseCode(?int $lastResponseCode): self
    {
        $this->lastResponseCode = $lastResponseCode;

        return $this;
    }

    public function getCompleted(): ?bool
    {
        return $this->completed;
    }

    public function setCompleted(bool $completed): self
    {
        $this->completed = $completed;

        return $this;
    }

    public function getNextAttemptOn(): ?DateTimeInterface
    {
        return $this->nextAttemptOn;
    }

    public function setNextAttemptOn(?DateTimeInterface $nextAttemptOn): self
    {
        $this->nextAttemptOn = $nextAttemptOn;

        return $this;
    }

    public function isFailed(): bool
    {
        return $this->isCompleted() === true &&
            $this->success === false;
    }

    public function isCompleted(): bool
    {
        if ($this->success === true) {
            return true;
        }

        if ($this->attempts >= $this->maxAttempts) {
            return true;
        }

        return false;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     */
    public function updateNextAttemptOn()
    {
        if ($this->lastAttemptedOn === null) {
            return;
        }

        $seconds = $this->intervalInSeconds +
            ($this->intervalInSeconds * $this->attempts * $this->multiplier)
        ;

        $this->nextAttemptOn = Carbon::instance($this->lastAttemptedOn)
            ->addSeconds($seconds);
    }

    public function remainingAttempts(): int
    {
        return $this->maxAttempts - $this->attempts;
    }
}
