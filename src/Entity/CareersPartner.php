<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Translatable\Translatable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\CareersPartnerRepository;

/**
 * @ORM\Entity(repositoryClass=CareersPartnerRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @UniqueEntity(fields={"name"})
 */
class CareersPartner implements Translatable
{
    use TimestampableEntity;
    use SortableEntity;
    use HasVisibilityConfiguration;
    use SoftDeleteableEntity;
    use TranslatableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string $name
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Gedmo\Translatable()
     */
    private $name;

	/**
	 * @var string $description
	 * @ORM\Column(type="text")
     * @Gedmo\Translatable()
	 * @Assert\NotBlank()
	 */
	private $description;

	/**
	 * @var string $discount
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank()
	 */
	private $discount;

	/**
	 * @ORM\OneToOne(targetEntity=Image::class, cascade={"persist", "remove"})
	 */
	private $image;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

	public function getImage(): ?Image
	{
		return $this->image;
	}

	public function setImage(?Image $image): self
	{
		$this->image = $image;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getName(): ?string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return CareersPartner
	 */
	public function setName(?string $name): self
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return CareersPartner
	 */
	public function setDescription(?string $description): self
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDiscount(): ?string
	{
		return $this->discount;
	}

	/**
	 * @param string $discount
	 * @return LoyaltyPartner
	 */
	public function setDiscount(?string $discount): self
	{
		$this->discount = $discount;
		return $this;
	}
}
