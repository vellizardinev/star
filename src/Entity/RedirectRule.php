<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RedirectRuleRepository")
 * @UniqueEntity(fields={"oldUrl"}, message="redirect_rule.already_exists")
 */
class RedirectRule
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[a-zA-ZÀ-ž0-9-#\/_:\.]+$/", message="redirect_rule.invalid_symbols")
     */
    private $oldUrl;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[a-zA-Z0-9-#\/_:\.]+$/", message="redirect_rule.invalid_symbols")
     */
    private $newUrl;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOldUrl(): ?string
    {
        return $this->oldUrl;
    }

    public function setOldUrl(string $oldUrl): self
    {
        $this->oldUrl = $oldUrl;

        return $this;
    }

    public function getNewUrl(): ?string
    {
        return $this->newUrl;
    }

    public function setNewUrl(string $newUrl): self
    {
        $this->newUrl = $newUrl;

        return $this;
    }
}
