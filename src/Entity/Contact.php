<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

class Contact
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $about;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[\s\-a-zA-ZабвгдежзийклмнопрстуфхцчшщъьюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЮЯҐґЄєІіЇї]+$/")
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

	/**
	 * @ORM\Column(type="string", length=190, nullable=true)
	 * @Assert\NotBlank()
     * @Assert\Length(min="9", max="9")
     * @Assert\Regex(pattern="/^\d+$/", message="digits_required_9")
	 */
	private $telephone;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $feedbackStrategy;

    /**
     * @var string
     */
    private $comment;

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(string $about): self
    {
        $this->about = $about;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getFeedbackStrategy(): ?string
    {
        return $this->feedbackStrategy;
    }

    public function setFeedbackStrategy(string $feedbackStrategy): self
    {
        $this->feedbackStrategy = $feedbackStrategy;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
