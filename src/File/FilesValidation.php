<?php


namespace App\File;


use App\Entity\Setting;
use App\Enum\FileTypes;
use App\Enum\Settings;
use App\Repository\SettingRepository;
use Exception;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;

class FilesValidation
{
    protected $rules = [];

    /**
     * @var SettingRepository
     */
    protected $settingRepository;

    /**
     * SettingsValidation constructor.
     * @param SettingRepository $settingRepository
     */
    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
        $this->initRules();
    }

    /**
     * @param $fileType
     * @return mixed
     * @throws Exception
     */
    public function get($fileType)
    {
        if (!array_key_exists($fileType, $this->rules)) {
            throw new Exception("Unknown file type: $fileType");
        }

        return $this->rules[$fileType];
    }

    protected function initRules()
    {
        /** @var Setting $setting */
        $fileUploadSizeSetting = $this->settingRepository->get(Settings::MAX_UPLOAD_SIZE);
	    $imageUploadSizeSetting = $this->settingRepository->get(Settings::IMAGE_MAX_UPLOAD_SIZE);

        $this->rules = [
            FileTypes::FILE => [new File(['maxSize' => $fileUploadSizeSetting->getValue()])],
            FileTypes::IMAGE => [new Image(['maxSize' => $imageUploadSizeSetting->getValue()])],
            FileTypes::CV => [new File([
            	'maxSize' => $fileUploadSizeSetting->getValue(),
	            'mimeTypes'=> [
	            	'application/pdf',
		            'application/msword',
		            'application/rtf',
		            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		            'application/octet-stream'
	            ]
            ])],
        ];
    }
}