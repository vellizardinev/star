<?php


namespace App\File;


use App\Entity\CreditDocument;
use App\Entity\Document;
use App\Entity\File;
use App\Entity\Image;
use Exception;
use Gedmo\Sluggable\Util\Urlizer;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileManager
{
    const FILES_DIR = 'files';
    const IMAGES_DIR = 'images';
    const CREDIT_DOCUMENT_DIR = 'credit_documents';

    /**
     * @var string
     */
    protected $uploadsDir;

    /**
     * @var RequestStackContext
     */
    private $requestStackContext;

    /**
     * @var FilesystemInterface
     */
    private $fileSystem;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $uploadsBaseUrl;
    /**
     * @var string
     */
    private $projectDir;

    /**
     * FileManager constructor.
     * @param FilesystemInterface $publicFileSystem
     * @param RequestStackContext $requestStackContext
     * @param LoggerInterface $logger
     * @param string $uploadsBaseUrl
     * @param string $projectDir
     */
    public function __construct(FilesystemInterface $publicFileSystem, RequestStackContext $requestStackContext, LoggerInterface $logger, string $uploadsBaseUrl, string $projectDir)
    {
        $this->requestStackContext = $requestStackContext;
        $this->fileSystem = $publicFileSystem;
        $this->logger = $logger;
        $this->uploadsBaseUrl = $uploadsBaseUrl;
        $this->projectDir = $projectDir;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return File
     * @throws FileExistsException
     */
    public function uploadFile(UploadedFile $uploadedFile): File
    {
        $file = new File;
        return $this->upload($uploadedFile, $file);
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return Document
     * @throws FileExistsException
     */
    public function uploadDocument(UploadedFile $uploadedFile): Document
    {
        $file = new Document;

        /** @var Document $document */
        $document = $this->upload($uploadedFile, $file);

        return $document;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return Image
     * @throws FileExistsException
     */
    public function uploadImage(UploadedFile $uploadedFile): Image
    {
        $image = new Image;

        return $this->upload($uploadedFile, $image);
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return CreditDocument
     * @throws FileExistsException
     */
    public function uploadCreditDocument(UploadedFile $uploadedFile): CreditDocument
    {
        $file = new CreditDocument();
        /** @var CreditDocument $creditDocument */
        $creditDocument = $this->upload($uploadedFile, $file);

        return $creditDocument;
    }

    /**
     * @param File $file
     * @throws Exception
     */
    public function delete(File $file)
    {
        $path = $file->getFilePath();

        if ($this->fileSystem->has($path)) {
            $this->fileSystem->delete($path);
        } else {
            $this->logger->alert(sprintf('Could not delete file object for file entity: id=%s, name=%s.', $file->getId(), $file->getName()));
        }
    }

    public function publicFilePath(File $file)
    {
        return $this->requestStackContext->getBasePath() .
            $this->uploadsBaseUrl . '/' . $file->getFilePath();
    }

    public function publicImagePath(Image $image)
    {
        return $this->requestStackContext->getBasePath() .
            $this->uploadsBaseUrl . '/' . $image->getFilePath();
    }

    public function publicCreditDocumentPath(CreditDocument $file)
    {
        return $this->requestStackContext->getBasePath() .
            $this->uploadsBaseUrl . '/' . $file->getFilePath();
    }

    /**
     * @param File $file
     * @return false|resource
     * @throws FileNotFoundException
     * @throws Exception
     */
    public function readStream(File $file)
    {
        $path = $file->getFilePath();

        $resource = $this->fileSystem->readStream($path);

        if ($resource === false) {
            throw new Exception(sprintf('Error opening stream for %s.', $path));
        }

        return $resource;
    }

    public function systemFilePath(File $file): string
    {
        return $this->projectDir .
            DIRECTORY_SEPARATOR .
            'public' .
            DIRECTORY_SEPARATOR .
            'uploads' .
            DIRECTORY_SEPARATOR .
            $file->getFilePath();
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param File $entity
     * @return File|Image
     * @throws FileExistsException
     * @throws Exception
     */
    protected function upload(UploadedFile $uploadedFile, File $entity)
    {
        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $newFilename = Urlizer::urlize($originalFilename).'-'.uniqid().'.'.$uploadedFile->guessExtension();

        $entity
            ->setName($newFilename)
            ->setOriginalName($originalFilename)
            ->setMime($uploadedFile->guessExtension())
            ->setSize($uploadedFile->getSize());

        $path = $this->pathForEntity($entity, $newFilename);

        $stream = fopen($uploadedFile->getPathname(), 'r');

        $result = $this->fileSystem->writeStream(
            $path,
            $stream
        );

        if ($result === false) {
            throw new Exception(sprintf("Could not upload file %s.", $originalFilename));
        }

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $entity;
    }

    protected function pathForEntity(File $entity, $fileName): string
    {
        if ($entity instanceof Image) {
            return self::IMAGES_DIR . DIRECTORY_SEPARATOR . $fileName;
        }

        if ($entity instanceof CreditDocument) {
            return self::CREDIT_DOCUMENT_DIR . DIRECTORY_SEPARATOR . $fileName;
        }

        return self::FILES_DIR . DIRECTORY_SEPARATOR . $fileName;
    }
}