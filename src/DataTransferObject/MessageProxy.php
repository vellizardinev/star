<?php


namespace App\DataTransferObject;


use App\Entity\Message;

class MessageProxy
{
    private $message;

    private $isRead;

    public function __construct(Message $message, bool $isRead = false)
    {
        $this->message = $message;
        $this->isRead = $isRead;
    }

    public function getMessage(): Message
    {
        return $this->message;
    }

    public function isRead(): bool
    {
        return $this->isRead;
    }
}
