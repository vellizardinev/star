<?php


namespace App\DataTransferObject;


use App\Entity\Credit;

class ShortApplicationParameters
{
    private $credit;

    private $amount;

    public function __construct(Credit $credit, float $amount)
    {
        $this->credit = $credit;
        $this->amount = $amount;
    }

    public function getCredit(): Credit
    {
        return $this->credit;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }
}