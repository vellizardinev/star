<?php


namespace App\DataTransferObject;


use Symfony\Component\Validator\Constraints as Assert;

class PaymentAmount
{
    /**
     * @var float
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(1)
     */
    private $amount;

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}