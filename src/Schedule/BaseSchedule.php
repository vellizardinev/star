<?php


namespace App\Schedule;


use App\Entity\Scheduler\Schedule;
use Carbon\Carbon;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

date_default_timezone_set("Europe/Sofia");
ini_set("date.timezone","Europe/Sofia");

abstract class BaseSchedule implements ScheduleInterface
{
    const PERIOD_MINUTES = 'minutes';
    const PERIOD_MONTHS = 'months';
    const PERIOD_QUARTERS = 'quarters';
    const PERIOD_YEARS = 'years';

    /**
     * @var int
     */
    protected $interval;

    /**
     * @var DateTime
     */
    protected $startDate;

    /** @var EntityManagerInterface */
    protected $manager;

    /**
     * @param int $interval
     * @param DateTime|null $startDate
     * @throws Exception
     */
    public function __construct(int $interval, ?DateTime $startDate = null)
    {
        $this->interval = $interval;

        if ($startDate) {
            $this->startDate = $startDate;
        } else {
            $this->startDate = new DateTime();
        }
    }

    public function setEntityManager(EntityManagerInterface $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @param string $name
     * @param DateTime $referenceDate
     * @param int $interval
     * @param string $period
     * @return bool
     */
    protected function periodHaveElapsed(string $name, DateTime $referenceDate, int $interval, string $period = self::PERIOD_MINUTES): bool
    {
        $reference = Carbon::instance($referenceDate);

        // Check if the DB contains data about the last time the command was run
        $lastExecutedAt = $this->manager->getRepository(Schedule::class)->lastExecutedAt($name);

        // If there is no data, the command has never been executed before
        if (!$lastExecutedAt) {
            $entry = (new Schedule)
                ->setName($name)
                ->setExecutedAt($this->startDate);

            $this->manager->persist($entry);
            $this->manager->flush();

            $lastExecutedAt = $this->startDate;
        }

        $lastRunTime = Carbon::instance($lastExecutedAt);

        switch ($period) {
            case self::PERIOD_MINUTES:
                $intervalHasElapsed = $lastRunTime->addMinutes($interval)->lessThanOrEqualTo($reference);
                break;
            case self::PERIOD_MONTHS:
                $intervalHasElapsed = $lastRunTime->addMonthsNoOverflow($interval)->lessThanOrEqualTo($reference);
                break;
            case self::PERIOD_QUARTERS:
                $intervalHasElapsed = $lastRunTime->addQuartersNoOverflow($interval)->lessThanOrEqualTo($reference);
                break;
            default:
                $intervalHasElapsed = $lastRunTime->addYearsNoOverflow($interval)->lessThanOrEqualTo($reference);
                break;
        }

        return $intervalHasElapsed;
    }
}