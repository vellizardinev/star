<?php


namespace App\Schedule;


use App\Entity\Scheduler\Schedule;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;

date_default_timezone_set("Europe/Sofia");
ini_set("date.timezone", "Europe/Sofia");

class Scheduler
{
    const SCHEDULER_CACHE_KEY = 'scheduler_cache_key';

    /** @var ScheduledCommand[] */
    protected $commands = [];

    protected $kernel;

    protected $manager;

    private $logger;

    private $cache;

    public function __construct(
        Kernelinterface $kernel,
        EntityManagerInterface $manager,
        LoggerInterface $schedulerLogger,
        AdapterInterface $cache
    )
    {
        $this->kernel = $kernel;
        $this->manager = $manager;

        $this->register();

        $this->logger = $schedulerLogger;
        $this->cache = $cache;
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        if ($this->isStillRunning()) {
            return;
        }

        $this->start();

        foreach ($this->commands as $name => $command) {
            $schedule = $command->getSchedule();

            $schedule->setEntityManager($this->manager);

            try {
                if ($schedule->shouldExecute($name, Carbon::now())) {
                    $result = $this->execute($command);
                    $this->logger->info($result);

                    $entry = (new Schedule)
                        ->setName($name)
                        ->setExecutedAt(Carbon::now());

                    $this->manager->persist($entry);
                    $this->manager->flush();
                }
            } catch (Exception $exception) {
                $this->logger->error(
                    sprintf(
                        'The Scheduler failed to execute command \'%s\'. Error message: %s',
                        $name,
                        $exception->getMessage()
                    )
                );

                $this->end();
            }
        }

        $this->end();
    }

    public function end(): void
    {
        $item = $this->cache->getItem(self::SCHEDULER_CACHE_KEY);

        $item->set(false);

        $this->cache->save($item);
    }

    protected function register()
    {
        $this->commands = [
            // Delete unfinished online applications older than 3 days every hour
            'icredit:online-applications:delete-unfinished' => new ScheduledCommand('icredit:online-applications:delete-unfinished', new EveryXMinutes(15, Carbon::today()->startOfDay())),

            // Delete images for completed online applications older than 7 days every hour
            'icredit:online-applications:delete-images-for-completed-applications' => new ScheduledCommand('icredit:online-applications:delete-images-for-completed-applications', new EveryXMinutes(15, Carbon::today()->startOfDay())),

            // Retry http requests every minute
            'http-requests:retry' => new ScheduledCommand('http-requests:retry', new EveryXMinutes(1, Carbon::today()->startOfDay())),

            // Send reminder emails to unfinished online application from previous day at 9:30am
            'icredit:online-applications:send-reminder-email-1' => new ScheduledCommand('icredit:online-applications:send-reminder-email', new EveryXDays(1, Carbon::today()->startOfDay()->addHours(9)->addMinutes(30))),

            // Send reminder emails to unfinished online application from previous day at 10:00am
            'icredit:online-applications:send-reminder-email-2' => new ScheduledCommand('icredit:online-applications:send-reminder-email', new EveryXDays(1, Carbon::today()->startOfDay()->addHours(10))),

            // Send reminder emails to unfinished online application from previous day at 10:30am
            'icredit:online-applications:send-reminder-email-3' => new ScheduledCommand('icredit:online-applications:send-reminder-email', new EveryXDays(1, Carbon::today()->startOfDay()->addHours(10)->addMinutes(30))),

            // Send reminder emails to unfinished online application from previous day at 11:00am
            'icredit:online-applications:send-reminder-email-4' => new ScheduledCommand('icredit:online-applications:send-reminder-email', new EveryXDays(1, Carbon::today()->startOfDay()->addHours(11)->addMinutes(0))),

            // Send reminder emails to unfinished online application from previous day at 11:30am
            'icredit:online-applications:send-reminder-email-5' => new ScheduledCommand('icredit:online-applications:send-reminder-email', new EveryXDays(1, Carbon::today()->startOfDay()->addHours(11)->addMinutes(30))),
        ];
    }

    /**
     * @param $command
     * @return string
     * @throws Exception
     */
    protected function execute(ScheduledCommand $command): ?string
    {
        $this->logger->info(
            sprintf('Command "%s" started at %s.', $command->getCommand(), Carbon::now()->format('d-m-Y H:i'))
        );

        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(
            [
                'command' => $command->getCommand(),
            ]
        );

        $output = new BufferedOutput();
        $application->run($input, $output);

        return $output->fetch();
    }

    private function isStillRunning(): bool
    {
        $item = $this->cache->getItem(self::SCHEDULER_CACHE_KEY);

        if ($item->isHit()) {
            return $item->get() === true;
        } else {
            return false;
        }
    }

    private function start(): void
    {
        $item = $this->cache->getItem(self::SCHEDULER_CACHE_KEY);

        $item->set(true);

        $this->cache->save($item);
    }
}