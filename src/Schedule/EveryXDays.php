<?php


namespace App\Schedule;


use DateTime;

class EveryXDays extends BaseSchedule
{
    /**
     * @param string $name
     * @param DateTime $referenceDate
     * @return bool
     */
    public function shouldExecute(string $name, DateTime $referenceDate): bool
    {
        return $this->periodHaveElapsed($name, $referenceDate, $this->interval * 60 * 24, self::PERIOD_MINUTES);
    }
}