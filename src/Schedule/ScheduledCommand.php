<?php


namespace App\Schedule;


class ScheduledCommand
{
    /** @var string */
    private $command;

    /** @var ScheduleInterface */
    private $schedule;

    /**
     * ScheduledCommand constructor.
     * @param string $command
     * @param ScheduleInterface $schedule
     */
    public function __construct(string $command, ScheduleInterface $schedule)
    {
        $this->command = $command;
        $this->schedule = $schedule;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @return ScheduleInterface
     */
    public function getSchedule(): ScheduleInterface
    {
        return $this->schedule;
    }
}