<?php


namespace App\Schedule;


use DateTime;

class EveryXMinutes extends BaseSchedule
{
    /**
     * @param string $name
     * @param DateTime $referenceDate
     * @return bool
     */
    public function shouldExecute(string $name, DateTime $referenceDate): bool
    {
        return $this->periodHaveElapsed($name, $referenceDate, $this->interval, self::PERIOD_MINUTES);
    }
}