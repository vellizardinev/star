<?php


namespace App\Schedule;


use DateTime;
use Doctrine\ORM\EntityManagerInterface;

interface ScheduleInterface
{
    public function shouldExecute(string $name, DateTime $referenceDate): bool;

    public function setEntityManager(EntityManagerInterface $manager);
}