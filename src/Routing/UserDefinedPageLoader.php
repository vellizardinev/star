<?php


namespace App\Routing;


use App\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class UserDefinedPageLoader extends Loader
{
    private $isLoaded = false;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * UserDefinedPageLoader constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->isLoaded) {
            throw new RuntimeException('Do not add the "user_defined_page" loader twice');
        }

        $routes = new RouteCollection();

        $userDefinedPages = $this->manager->getRepository(Page::class)->findAll();

        foreach ($userDefinedPages as $userDefinedPage) {
            // prepare a new route
            $path = '/{path}';

            $defaults = [
                '_controller' => 'App\Controller\UserDefinedPageController::show',
            ];

            $route = new Route($path, $defaults);

            // add the new route to the route collection
            $routeName = 'user_defined_page.' . str_replace('-', '_', $userDefinedPage->getPath());
            $routes->add($routeName, $route);
        }

        $this->isLoaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'user_defined_page' === $type;
    }
}
