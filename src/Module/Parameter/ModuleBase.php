<?php


namespace App\Module\Parameter;


use App\Enum\ModuleWidths;
use Symfony\Component\Validator\Constraints as Assert;

abstract class ModuleBase
{
    use HasVisibilityParameters;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $width = ModuleWidths::STANDARD;

    /**
     * @var string
     */
    public $backgroundColor = '#f6f6f6';

    /**
     * @var string
     */
    public $backgroundImage = null;

    /**
     * @var string
     */
    public $backgroundImageMobile = null;
}