<?php


namespace App\Module\Parameter;

use Doctrine\Common\Collections\ArrayCollection;

class WhyYouShouldTrustUs extends ModuleBase
{
	/**
	 * @var ArrayCollection|Reason[]
	 */
	public $reasons;

    /** @var string */
    public $title;

    /**
	 * WhyYouShouldTrustUs constructor.
	 * @param Reason[]|ArrayCollection $reasons
	 */
	public function __construct()
	{
		$this->reasons = new ArrayCollection();
		for($i = 0; $i < 4; $i++) {
			$this->addReason(new Reason());
		}
	}

	/**
	 * @return Reason[]|ArrayCollection
	 */
	public function getReasons()
	{
		return $this->reasons;
	}

	/**
	 * @param Reason[]|ArrayCollection $reasons
	 * @return WhyYouShouldTrustUs
	 */
	public function setReasons($reasons)
	{
		$this->reasons = $reasons;
		return $this;
	}

	public function addReason(Reason $reason): self
	{
		if (!$this->reasons->contains($reason)) {
			$this->reasons->add($reason);
		}

		return $this;
	}
}