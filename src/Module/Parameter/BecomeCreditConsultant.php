<?php


namespace App\Module\Parameter;

use Symfony\Component\Validator\Constraints as Assert;

class BecomeCreditConsultant extends ModuleBase
{
	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public $headline;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public $content;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public $buttonText = 'Learn more';

	/**
	 * @var string
	 */
	public $image;
}