<?php


namespace App\Module\Parameter;


use Doctrine\Common\Collections\ArrayCollection;

class Header
{
    use HasVisibilityParameters;

    /**
     * @var ArrayCollection|Link[]
     */
    public  $topRow;

    /**
     * @var ArrayCollection|Link[]
     */
    public  $bottomRow;

    /**
     * Header constructor.
     */
    public function __construct()
    {
        $this->topRow = new ArrayCollection();
        $this->bottomRow = new ArrayCollection();
    }

    /**
     * @return Link[]|ArrayCollection
     */
    public function getTopRow()
    {
        return $this->topRow;
    }

    /**
     * @param Link[]|ArrayCollection $topRow
     * @return Header
     */
    public function setTopRow($topRow)
    {
        $this->topRow = $topRow;

        return $this;
    }

    /**
     * @return Link[]|ArrayCollection
     */
    public function getBottomRow()
    {
        return $this->bottomRow;
    }

    /**
     * @param Link[]|ArrayCollection $bottomRow
     * @return Header
     */
    public function setBottomRow($bottomRow)
    {
        $this->bottomRow = $bottomRow;

        return $this;
    }

    public function addTopLink(Link $link): self
    {
        if (!$this->topRow->contains($link)) {
            $this->topRow->add($link);
        }

        return $this;
    }

    public function addBottomLink(Link $link): self
    {
        if (!$this->bottomRow->contains($link)) {
            $this->bottomRow->add($link);
        }

        return $this;
    }
}