<?php


namespace App\Module\Parameter;

use Doctrine\Common\Collections\ArrayCollection;

class iCreditAsEmployer extends ModuleBase
{
	/**
	 * @var ArrayCollection|ExtendedReason[]
	 */
	public $reasons;

	public function __construct()
	{
		$this->reasons = new ArrayCollection();
		for($i = 0; $i < 4; $i++) {
			$this->addReason(new ExtendedReason());
		}
	}

	/**
	 * @return ExtendedReason[]|ArrayCollection
	 */
	public function getReasons()
	{
		return $this->reasons;
	}

	public function setReasons($reasons): self
	{
		$this->reasons = $reasons;
		return $this;
	}

	public function addReason(ExtendedReason $reason): self
	{
		if (!$this->reasons->contains($reason)) {
			$this->reasons->add($reason);
		}

		return $this;
	}
}