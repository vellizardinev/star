<?php


namespace App\Module\Parameter;


use Doctrine\Common\Collections\ArrayCollection;

class Footer
{
    use HasVisibilityParameters;

    /**
     * @var ArrayCollection|Link[]
     */
    public  $firstColumn;

    /**
     * @var ArrayCollection|Link[]
     */
    public  $secondColumn;

    /**
     * @var ArrayCollection|Link[]
     */
    public  $thirdColumn;

    /**
     * Header constructor.
     */
    public function __construct()
    {
        $this->firstColumn = new ArrayCollection();
        $this->secondColumn = new ArrayCollection();
        $this->thirdColumn = new ArrayCollection();
    }

    /**
     * @return Link[]|ArrayCollection
     */
    public function getFirstColumn()
    {
        return $this->firstColumn;
    }

    /**
     * @param $firstColumn
     * @return Footer
     */
    public function setFirstColumn($firstColumn): self
    {
        $this->firstColumn = $firstColumn;

        return $this;
    }

    /**
     * @return Link[]|ArrayCollection
     */
    public function getSecondColumn()
    {
        return $this->secondColumn;
    }

    /**
     * @param $secondColumn
     * @return $this
     */
    public function setSecondColumn($secondColumn): self
    {
        $this->secondColumn = $secondColumn;

        return $this;
    }

    public function addFirstColumnLink(Link $link): self
    {
        if (!$this->firstColumn->contains($link)) {
            $this->firstColumn->add($link);
        }

        return $this;
    }

    public function addSecondColumnLink(Link $link): self
    {
        if (!$this->secondColumn->contains($link)) {
            $this->secondColumn->add($link);
        }

        return $this;
    }

    public function addThirdColumnLink(Link $link): self
    {
        if (!$this->thirdColumn->contains($link)) {
            $this->thirdColumn->add($link);
        }

        return $this;
    }
}