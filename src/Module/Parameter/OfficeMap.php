<?php


namespace App\Module\Parameter;

use Symfony\Component\Validator\Constraints as Assert;

class OfficeMap extends ModuleBase
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $address = 'Ukraine';

	/**
	 * @var int
	 * @Assert\NotBlank()
	 */
	public $zoom = 6;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public $type;

	/**
	 * @return string
	 */
	public function getAddress(): ?string
	{
		return $this->address;
	}

	/**
	 * @param string $address
	 * @return OfficeMap
	 */
	public function setAddress(?string $address): OfficeMap
	{
		$this->address = $address;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getZoom(): ?int
	{
		return (int) $this->zoom;
	}

	/**
	 * @param int $zoom
	 * @return OfficeMap
	 */
	public function setZoom(?int $zoom): OfficeMap
	{
		$this->zoom = $zoom;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getType(): ?string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 * @return OfficeMap
	 */
	public function setType(string $type): OfficeMap
	{
		$this->type = $type;
		return $this;
	}
}