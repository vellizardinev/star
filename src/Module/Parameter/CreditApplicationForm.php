<?php


namespace App\Module\Parameter;

use App\Enum\HorizontalPosition;
use App\Enum\Styles;
use Symfony\Component\Validator\Constraints as Assert;

class CreditApplicationForm extends ModuleBase
{
	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public  $position = HorizontalPosition::RIGHT;

	public $redirectUrl = '#';

	public $text;

	public $backgroundColorForm = '';

    /**
     * CreditApplicationForm constructor.
     * @param string $position
     */
    public function __construct(string $position = HorizontalPosition::RIGHT)
    {
        $this->position = $position;
    }
}