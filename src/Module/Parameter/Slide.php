<?php


namespace App\Module\Parameter;

class Slide
{
	/**
	 * @var string
	 */
	public $headline;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @return string
	 */
	public function getHeadline(): ?string
	{
		return $this->headline;
	}

	/**
	 * @param string $headline
	 * @return Slide
	 */
	public function setHeadline(?string $headline): Slide
	{
		$this->headline = $headline;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return Slide
	 */
	public function setDescription(?string $description): Slide
	{
		$this->description = $description;
		return $this;
	}
}