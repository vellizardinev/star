<?php


namespace App\Module\Parameter;


use Doctrine\Common\Collections\ArrayCollection;

class HeaderLandingPage
{
    use HasVisibilityParameters;

    /**
     * @var ArrayCollection|Link[]
     */
    public  $links;

    public function __construct()
    {
        $this->links = new ArrayCollection();
    }

    /**
     * @return Link[]|ArrayCollection
     */
    public function getLinks()
    {
        return $this->links;
    }

    public function setLinks($links): self
    {
        $this->links = $links;

        return $this;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links->add($link);
        }

        return $this;
    }
}