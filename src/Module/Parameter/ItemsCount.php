<?php


namespace App\Module\Parameter;


use Symfony\Component\Validator\Constraints as Assert;

class ItemsCount extends ModuleBase
{
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     * @Assert\LessThanOrEqual(10)
     */
    public  $itemsCount = 3;
}