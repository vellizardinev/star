<?php


namespace App\Module\Parameter;

use Doctrine\Common\Collections\ArrayCollection;

class Slider extends ModuleBase
{
    /**
     * @var ArrayCollection|Slide[]
     */
    public  $slides;

    /** @var string  */
    public $textColor = 'white';

    public $size = 'medium';

    /**
     * Header constructor.
     */
    public function __construct()
    {
        $this->slides = new ArrayCollection();
        for($i = 0; $i < 5; $i++){
        	$slide = new Slide();
        	$this->addSlide($slide);
        }
    }

    /**
     * @return Slide[]|ArrayCollection
     */
    public function getSlides()
    {
        return $this->slides;
    }

    /**
     * @param Slide[]|ArrayCollection $slides
     * @return Slider
     */
    public function setSlides($slides): self
    {
        $this->slides = $slides;

        return $this;
    }

    public function addSlide(Slide $slide): self
    {
        if (!$this->slides->contains($slide)) {
            $this->slides->add($slide);
        }

        return $this;
    }
}