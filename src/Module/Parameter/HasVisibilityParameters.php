<?php


namespace App\Module\Parameter;


use Carbon\Carbon;
use DateTimeInterface;
use Symfony\Component\Validator\Constraints as Assert;

trait HasVisibilityParameters
{
    /**
     * @var DateTimeInterface
     * @Assert\Type("\DateTimeInterface")
     */
    public $startDate;

    /**
     * @var DateTimeInterface
     * @Assert\Type("\DateTimeInterface")
     */
    public $endDate;

    public function isVisible(): bool
    {
        $greaterThanStartDate = true;
        $lessThanEndDate = true;
        $now = Carbon::now();

        if ($this->startDate) {
            $greaterThanStartDate = $now->greaterThan($this->startDate);
        }

        if ($this->endDate) {
            $lessThanEndDate = $now->lessThan($this->endDate);
        }

        return $greaterThanStartDate && $lessThanEndDate;
    }
}