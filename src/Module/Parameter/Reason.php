<?php


namespace App\Module\Parameter;

use Symfony\Component\Validator\Constraints as Assert;

class Reason extends ModuleBase
{
	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public $number;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public $statement;
}