<?php


namespace App\Module\Parameter;


use App\Enum\Target;
use Symfony\Component\Validator\Constraints as Assert;

class Link
{
    /**
     * @var string
     */
    public  $text = '';

    /**
     * @var string
     * @Assert\Regex(pattern="/^[a-z0-9-]+$/")
     */
    public $url = '';

	/**
	 * @var Target
	 */
	public  $target = Target::same_tab;


	/**
     * Link constructor.
     * @param string $text
     * @param string $url
	 * @param string $target|null
     */
    public function __construct(string $text, string $url, string $target = Target::same_tab)
    {
        $this->text = $text;
        $this->url = $url;
        $this->target = $target;
    }
}