<?php


namespace App\Module\Parameter;

use Symfony\Component\Validator\Constraints as Assert;

class ExpandableContent extends ModuleBase
{
	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public $title;

	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public $content;
}