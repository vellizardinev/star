<?php


namespace App\Module\Parameter;

use Symfony\Component\Validator\Constraints as Assert;

class Content extends ModuleBase
{
	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	public  $content = '';

	/**
	 * @var string
	 */
	public  $contentBackgroundColor = '#ffffff';

	public $mobileOnly = false;
}