<?php


namespace App\Module\Parameter;


class FooterLandingPage
{
    use HasVisibilityParameters;

    /**
     * @var string
     */
    private $text;

    /**
     * FooterLandingPage constructor.
     * @param string $text
     */
    public function __construct(string $text)
    {
        $this->text = $text;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }
}