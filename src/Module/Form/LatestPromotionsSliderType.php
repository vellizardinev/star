<?php

namespace App\Module\Form;

use App\Module\Parameter\ItemsCount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LatestPromotionsSliderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('itemsCount', NumberType::class, [
                'label' => 'label.promotion_count',
            ])
            ->add('generalSettings', ModuleBaseType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ItemsCount::class,
            'translation_domain' => 'form',
        ]);
    }
}
