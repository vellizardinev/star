<?php

namespace App\Module\Form;

use App\Module\Parameter\Footer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FooterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstColumn', CollectionType::class, [
                'label' => false,
                'entry_type' => LinkType::class
            ])
            ->add('secondColumn', CollectionType::class, [
                'label' => false,
                'entry_type' => LinkType::class
            ])
            ->add('thirdColumn', CollectionType::class, [
                'label' => false,
                'entry_type' => LinkType::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Footer::class,
            'translation_domain' => 'form',
        ]);
    }
}
