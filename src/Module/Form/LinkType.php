<?php

namespace App\Module\Form;

use App\Enum\Target;
use App\Module\Parameter\Link;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LinkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', TextType::class, [
                'label' => 'label.text',
                'required' => false,
            ])
            ->add('url', TextType::class, [
                'label' => 'label.url',
                'required' => false,
            ])
	        ->add('target', ChoiceType::class, [
		        'label' => 'label.target',
		        'choices' => array_flip(Target::toTranslationKeys()),
	        ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Link::class,
            'translation_domain' => 'form',
        ]);
    }
}
