<?php

namespace App\Module\Form;

use App\Enum\SliderSizes;
use App\Form\CustomFields\PickrType;
use App\Module\Parameter\Slider;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SliderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sizeChoices = [];
        $keys = array_flip(SliderSizes::toTranslationKeys());
        $sizeChoices = array_merge($sizeChoices, $keys);

        $builder
            ->add('slides', CollectionType::class, [
                'label' => false,
                'entry_type' => SlideType::class,
	            'required' => false
            ])
	        ->add('textColor', PickrType::class, [
		        'label' => 'label.text_color',
	        ])
            ->add('size', ChoiceType::class, [
                'label' => 'label.size',
                'choices' => $sizeChoices,
            ])
	        ->add('generalSettings', ModuleBaseType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Slider::class,
            'translation_domain' => 'form',
        ]);
    }
}
