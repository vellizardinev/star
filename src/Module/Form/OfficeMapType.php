<?php

namespace App\Module\Form;

use App\Enum\MapTypes;
use App\Module\Parameter\OfficeMap;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfficeMapType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('address', TextType::class, [
				'label' => 'label.office_map.address',
			])
			->add('zoom', IntegerType::class, [
				'label' => 'label.office_map.zoom',
			])
			->add('type', ChoiceType::class, [
				'label' => 'label.office_map.type',
				'choices' => array_flip(MapTypes::toTranslationKeys()),
			])
            ->add('generalSettings', ModuleBaseType::class)
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => OfficeMap::class,
			'translation_domain' => 'form',
		]);
	}
}
