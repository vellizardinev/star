<?php

namespace App\Module\Form;

use App\Form\CustomFields\CKEditorType;
use App\Form\CustomFields\ImageGalleryType;
use App\Module\Parameter\BecomeCreditConsultant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BecomeCreditConsultantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('headline', TextType::class, [
                'label' => 'label.headline',
            ])
	        ->add('content', CKEditorType::class, [
		        'label' => 'label.content',
		        'label_attr' => ['class' => 'required']
	        ])
	        ->add('buttonText', TextType::class, [
		        'label' => 'label.button',
	        ])
	        ->add('image', ImageGalleryType::class, [
		        'label' => 'label.image',
		        'required' => false
	        ])
            ->add('generalSettings', ModuleBaseType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BecomeCreditConsultant::class,
            'translation_domain' => 'form',
        ]);
    }
}
