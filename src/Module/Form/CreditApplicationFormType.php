<?php

namespace App\Module\Form;

use App\Enum\HorizontalPosition;
use App\Form\CustomFields\CKEditorType;
use App\Form\CustomFields\PickrType;
use App\Module\Parameter\CreditApplicationForm;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreditApplicationFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $positionChoices = [];
        $keys = array_flip(HorizontalPosition::toTranslationKeys());
        $positionChoices = array_merge($positionChoices, $keys);

        $builder
            ->add('position', ChoiceType::class, [
                'label' => 'label.application_form_position',
                'choices' => $positionChoices,
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('backgroundColorForm', PickrType::class, [
                'label' => 'label.background_color_form',
                'required' => false,
            ])
            ->add('redirectUrl', TextType::class, [
                'label' => 'label.redirect_url',
            ])
            ->add('text', CKEditorType::class, [
                'label' => 'label.text',
            ])
            ->add('generalSettings', ModuleBaseType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CreditApplicationForm::class,
            'translation_domain' => 'form',
        ]);
    }
}
