<?php

namespace App\Module\Form;

use App\Form\CustomFields\CKEditorType;
use App\Module\Parameter\FooterLandingPage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FooterLandingPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', CKEditorType::class, [
                'label' => 'label.text',
	            'label_attr' => ['class' => 'required']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FooterLandingPage::class,
            'translation_domain' => 'form',
        ]);
    }
}
