<?php

namespace App\Module\Form;

use App\Module\Parameter\iCreditAsEmployer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ICreditAsEmployerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reasons', CollectionType::class, [
                'label' => 'false',
	            'entry_type' => ExtendedReasonType::class
            ])
            ->add('generalSettings', ModuleBaseType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => iCreditAsEmployer::class,
            'translation_domain' => 'form',
        ]);
    }
}
