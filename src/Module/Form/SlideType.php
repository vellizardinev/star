<?php

namespace App\Module\Form;

use App\Form\CustomFields\CKEditorType;
use App\Module\Parameter\Slide;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SlideType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('headline', TextType::class, [
                'label' => 'label.headline',
            ])
	        ->add('description', CKEditorType::class, [
		        'label' => 'label.description',
	        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Slide::class,
            'translation_domain' => 'form',
        ]);
    }
}
