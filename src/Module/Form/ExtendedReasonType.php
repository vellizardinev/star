<?php

namespace App\Module\Form;

use App\Module\Parameter\ExtendedReason;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExtendedReasonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prefix', TextType::class, [
                'required' => false,
            ])
            ->add('number', TextType::class, [
                'required' => false,
            ])
	        ->add('statement', TextType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExtendedReason::class,
            'translation_domain' => 'form',
	        'label_format' => 'label.reason.%name%'
        ]);
    }
}
