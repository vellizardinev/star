<?php

namespace App\Module\Form;

use App\Module\Parameter\Header;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HeaderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('topRow', CollectionType::class, [
                'label' => false,
                'entry_type' => LinkType::class
            ])
            ->add('bottomRow', CollectionType::class, [
                'label' => false,
                'entry_type' => LinkType::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Header::class,
            'translation_domain' => 'form',
        ]);
    }
}
