<?php

namespace App\Module\Form;

use App\Enum\ModuleWidths;
use App\Form\CustomFields\BootstrapDatepickerType;
use App\Form\CustomFields\ImageGalleryType;
use App\Form\CustomFields\PickrType;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleBaseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $widthChoices = [];
        $keys = array_flip(ModuleWidths::toTranslationKeys());
        $widthChoices = array_merge($widthChoices, $keys);

        $builder
            ->add('width', ChoiceType::class, [
                'choices' => $widthChoices,
                'expanded' => false,
                'multiple' => false,
                'label' => 'label.width',
            ])
            ->add('backgroundColor', PickrType::class, [
                'label' => 'label.background_color',
                'required' => false,
            ])
            ->add('backgroundImage', ImageGalleryType::class, [
                'label' => 'label.background_image',
                'required' => false,
            ])
        ;

	    if ($options['show_background_image_mobile']) {
		    $builder
			    ->add('backgroundImageMobile', ImageGalleryType::class, [
				    'label' => 'label.background_image_for_mobile',
				    'required' => false,
			    ]);
	    }

	    $builder
            ->add('startDate', BootstrapDatepickerType::class, [
                'label' => 'label.start_date',
                'required' => false,
            ])
            ->add('endDate', BootstrapDatepickerType::class, [
                'label' => 'label.end_date',
                'required' => false,
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true,
            'translation_domain' => 'form',
	        'show_background_image_mobile' => true
        ]);
    }
}
