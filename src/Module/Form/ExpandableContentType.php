<?php

namespace App\Module\Form;

use App\Form\CustomFields\CKEditorType;
use App\Module\Parameter\ExpandableContent;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpandableContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('title', TextType::class, [
		        'label' => 'label.title',
	        ])
            ->add('content', CKEditorType::class, [
                'label' => 'label.content',
	            'label_attr' => ['class' => 'required']
            ])
            ->add('generalSettings', ModuleBaseType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExpandableContent::class,
            'translation_domain' => 'form',
        ]);
    }
}
