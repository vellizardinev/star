<?php

namespace App\Module\Form;

use App\Module\Parameter\WhyYouShouldTrustUs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WhyYouShouldTrustUsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'label.title',
                'required' => false,
            ])
            ->add('reasons', CollectionType::class, [
                'label' => 'false',
	            'entry_type' => ReasonType::class
            ])
            ->add('generalSettings', ModuleBaseType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WhyYouShouldTrustUs::class,
            'translation_domain' => 'form',
        ]);
    }
}
