<?php

namespace App\Module\Form;

use App\Form\CustomFields\CKEditorType;
use App\Module\Parameter\Reason;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReasonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', TextType::class, [
                'required' => false,
            ])
	        ->add('statement', CKEditorType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reason::class,
            'translation_domain' => 'form',
	        'label_format' => 'label.reason.%name%'
        ]);
    }
}
