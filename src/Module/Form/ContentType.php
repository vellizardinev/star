<?php

namespace App\Module\Form;

use App\Form\CustomFields\CKEditorType;
use App\Form\CustomFields\PickrType;
use App\Module\Parameter\Content;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', CKEditorType::class, [
                'label' => 'label.content',
	            'label_attr' => ['class' => 'required']
            ])
	        ->add('contentBackgroundColor', PickrType::class, [
		        'label' => 'label.content_background_color',
		        'required' => false
	        ])
            ->add('mobileOnly', CheckboxType::class,
            [
                'label' => 'label.mobile_only',
                'required' => false,
            ])
            ->add('generalSettings', ModuleBaseType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Content::class,
            'translation_domain' => 'form',
        ]);
    }
}
