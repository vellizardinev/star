<?php


namespace App\Module\Renderer;

use App\Entity\ModuleItem;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class WhyYouShouldTrustUs extends BaseRenderer
{
    /**
     * @param ModuleItem $moduleItem
     * @return string|null
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(ModuleItem $moduleItem): ?string
    {
        /** @var \App\Module\Parameter\WhyYouShouldTrustUs $data */
        $data = $this->dataForCurrentLocale($moduleItem);

        if ($data->isVisible() === false) {
            return null;
        }

        $template = $this->extractTemplatePath($moduleItem);

        return $this->environment->render(
            $template,
            [
                'data' => $data,
                'id' => $moduleItem->getId(),
            ]
        );
    }
}