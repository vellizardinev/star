<?php


namespace App\Module\Renderer;


use App\Entity\ModuleItem;

interface ModuleRenderer
{
    public function render(ModuleItem $moduleItem): ?string;
}