<?php


namespace App\Module\Renderer;


use App\Entity\ModuleItem;
use App\Entity\Promotion;
use App\Module\Parameter\ItemsCount;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class LatestPromotions extends BaseRenderer
{
    /**
     * @param ModuleItem $moduleItem
     * @return string|null
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(ModuleItem $moduleItem): ?string
    {
        /** @var ItemsCount $data */
        $data = $this->dataForCurrentLocale($moduleItem);

        if ($data->isVisible() === false) {
            return null;
        }

        $promotions = $this->manager->getRepository(Promotion::class)->latest($data->itemsCount);
        $template = $this->extractTemplatePath($moduleItem);

        $content = $this->environment->render($template, [
            'promotions' => $promotions,
            'data' => $data,
            'id' => $moduleItem->getId(),
        ]);

        return $content;
    }
}