<?php


namespace App\Module\Renderer;

use App\Entity\ModuleItem;
use App\Entity\Page;
use App\Enum\Pages;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ICreditAsEmployer extends BaseRenderer
{
	/**
	 * @param ModuleItem $moduleItem
	 * @return string|null
	 * @throws LoaderError
	 * @throws RuntimeError
	 * @throws SyntaxError
	 */
	public function render(ModuleItem $moduleItem): ?string
	{
		/** @var \App\Module\Parameter\iCreditAsEmployer $data */
		$data = $this->dataForCurrentLocale($moduleItem);

        if ($data->isVisible() === false) {
            return null;
        }

		$iCreditAsEmployerPage = $this->manager->getRepository(Page::class)->findOneBy(['name' => Pages::ICREDIT_AS_EMPLOYER]);

		$motivationProgramPage = $this->manager->getRepository(Page::class)->findOneBy(['name' => Pages::MOTIVATION_PROGRAMME]);

		$template = $this->extractTemplatePath($moduleItem);

		$content = $this->environment->render($template, [
		    'data' => $data,
		    'id' => $moduleItem->getId(),
			'motivationProgramPage' => $motivationProgramPage,
			'iCreditAsEmployerPage' => $iCreditAsEmployerPage
        ]);

		return $content;
	}
}