<?php


namespace App\Module\Renderer;

use App\Entity\CreditShortApplication;
use App\Entity\ModuleItem;
use App\Form\CreditApplicationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class CreditApplicationForm extends BaseRenderer
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @param Environment $environment
     * @param EntityManagerInterface $manager
     * @param SerializerInterface $serializer
     * @param RequestStack $requestStack
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(
        Environment $environment,
        EntityManagerInterface $manager,
        SerializerInterface $serializer,
        RequestStack $requestStack,
        FormFactoryInterface $formFactory
    ) {
        $this->environment = $environment;
        $this->manager = $manager;
        $this->serializer = $serializer;

        parent::__construct($environment, $manager, $serializer, $requestStack);
        $this->formFactory = $formFactory;
    }

    /**
     * @param ModuleItem $moduleItem
     * @return string|null
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(ModuleItem $moduleItem): ?string
    {
        $data = $this->dataForCurrentLocale($moduleItem);

        if ($data->isVisible() === false) {
            return null;
        }

        $creditShortApplication = new CreditShortApplication();

        $form = $this->formFactory->create(CreditApplicationType::class, $creditShortApplication);

        $template = $this->extractTemplatePath($moduleItem);

        return $this->environment->render(
            $template,
            [
                'data' => $data,
                'form' => $form->createView(),
                'landingId' => $moduleItem->getPage()->getId(),
                'id' => $moduleItem->getId(),
            ]
        );
    }
}