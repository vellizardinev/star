<?php


namespace App\Module\Renderer;


use App\Entity\City;
use App\Entity\ModuleItem;
use App\Entity\Office;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class OfficeMap extends BaseRenderer
{
    /**
     * @param ModuleItem $moduleItem
     * @param array|null $props
     * @return string|null
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(ModuleItem $moduleItem, ?array $props = []): ?string
    {
        /** @var \App\Module\Parameter\OfficeMap $data */
        $data = $this->dataForCurrentLocale($moduleItem);

        if ($data->isVisible() === false) {
            return null;
        }

        $offices = $this->manager->getRepository(Office::class)->findAllOffices();
        $cities = $this->manager->getRepository(City::class)->citiesWithOffices();

        $gmapOffices = $this->serializer->normalize($offices, null, ['intention' => 'gmap']);
        $officesJson = $this->serializer->serialize($gmapOffices, 'json');
        $citiesJson = $this->serializer->serialize($cities, 'json');

        $template = $this->extractTemplatePath($moduleItem);

        return $this->environment->render(
            $template,
            [
                'offices' => $officesJson,
                'cities' => $citiesJson,
                'data' => $data,
                'id' => $moduleItem->getId(),
                'props' => $props,
            ]
        );
    }
}