<?php


namespace App\Module\Renderer;

use App\Entity\ModuleItem;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class DefaultRenderer extends BaseRenderer
{
	/**
	 * @param ModuleItem $moduleItem
	 * @return string|null
	 * @throws LoaderError
	 * @throws RuntimeError
	 * @throws SyntaxError
	 */
	public function render(ModuleItem $moduleItem): ?string
	{
		$data = $this->dataForCurrentLocale($moduleItem);

        if ($data->isVisible() === false) {
            return null;
        }

		$template = $this->extractTemplatePath($moduleItem);

		$content = $this->environment->render($template, ['data' => $data, 'id' => $moduleItem->getId()]);

		return $content;
	}
}