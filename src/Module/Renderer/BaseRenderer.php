<?php


namespace App\Module\Renderer;


use App\Entity\ModuleItem;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerInterface;
use Twig\Environment;

abstract class BaseRenderer implements ModuleRenderer
{
    /**
     * @var Environment
     */
    protected $environment;

    /**
     * @var EntityManagerInterface
     */
    protected $manager;

    /**
     * @var SerializerInterface
     */
    protected $serializer;
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * BaseRenderer constructor.
     * @param Environment $environment
     * @param EntityManagerInterface $manager
     * @param SerializerInterface $serializer
     * @param RequestStack $requestStack
     */
    public function __construct(
        Environment $environment,
        EntityManagerInterface $manager,
        SerializerInterface $serializer,
        RequestStack $requestStack
    ) {
        $this->environment = $environment;
        $this->manager = $manager;
        $this->serializer = $serializer;
        $this->requestStack = $requestStack;
    }

    protected function extractTemplatePath(ModuleItem $moduleItem)
    {
        $name = $moduleItem->getModule() !== null ? $moduleItem->getModule()->getName() : $moduleItem->getName();

        $templatePath = preg_split("/[\s,]+/", $name);
        $templatePath = array_map(
            function ($item) {
                return strtolower($item);
            },
            $templatePath
        );

        $templatePath = 'modules/views/' . implode('_', $templatePath) . '.html.twig';

        return $templatePath;
    }

    protected function dataForCurrentLocale(ModuleItem $moduleItem)
    {
        $locale = $this->requestStack->getMasterRequest()->getLocale();
        $dataArray = unserialize($moduleItem->getData());

        return $dataArray[$locale];
    }
}