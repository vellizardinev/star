<?php


namespace App\Module;


use App\Entity\Module;
use App\Entity\ModuleItem;

trait ExtractsModulePaths
{
    public function extractFormClass(ModuleItem $moduleItem): string
    {
        $name = $moduleItem->getModule() !== null ? $moduleItem->getModule()->getName() : $moduleItem->getName();

        $formClass = preg_split("/[\s,]+/", $name);
        $formClass = array_map(
            function ($item) {
                return ucfirst($item);
            }, $formClass);
        $formClass = 'App\\Module\\Form\\' . implode('', $formClass) . 'Type';

        return $formClass;
    }

    public function extractFormTemplate(ModuleItem $moduleItem): string
    {
        $name = $moduleItem->getModule() !== null ? $moduleItem->getModule()->getName() : $moduleItem->getName();

        $formTemplate = preg_split("/[\s,]+/", $name);
        $formTemplate = array_map(
            function ($item) {
                return strtolower($item);
            }, $formTemplate);

        $formTemplate = '/modules/parameters/' . implode('_', $formTemplate) . '.html.twig';

        return $formTemplate;
    }
}