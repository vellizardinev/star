<?php

namespace App\Controller;

use App\Breadcrumbs\BreadcrumbsService;
use App\Breadcrumbs\VerificationSuccess;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Service\MobileApi\KycService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * @package App\Controller
 * @Route("verification")
 */
class VerificationController extends BaseController
{
    /**
     * @Route("/success", name="verification.success")
     * @param BreadcrumbsService $breadcrumbsService
     * @return Response
     */
    public function success(BreadcrumbsService $breadcrumbsService)
    {
        $breadcrumbsService->create(new VerificationSuccess());

        return $this->render('verification/success.html.twig', []);
    }

    /**
     * @Route("/failed", name="verification.failed")
     * @param BreadcrumbsService $breadcrumbsService
     * @return Response
     */
    public function failed(BreadcrumbsService $breadcrumbsService)
    {
        $breadcrumbsService->create(new VerificationSuccess());

        return $this->render('verification/failed.html.twig', []);
    }

    /**
     * @Route("/{hash}/initiate", name="verification.initiate", methods={"GET"})
     * @param OnlineApplication $application
     * @return Response
     */
    public function initiate(OnlineApplication $application): Response
    {
        return $this->render('verification/initiate.html.twig', [
            'application' => $application
        ]);
    }

    /**
     * @Route("/notification", name="verification.notifiction", methods={"POST"})
     * @param Request $request
     * @param KycService $kycService
     * @param LoggerInterface $creditApplicationsLog
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function verificationNotification(Request $request, KycService $kycService, LoggerInterface $creditApplicationsLog): JsonResponse
    {
        $creditApplicationsLog->info(sprintf(
            'Received notification from EasyPay. Request content is: %s',
            $request->getContent()
        ));
        $data = json_decode($request->getContent(), true);

        $kycSessionId = $data['phone'];
        $cardGuid = $data['cardGuid'];

        $application = $this->manager()
            ->getRepository(OnlineApplication::class)
            ->findOneBy(['kycSessionId' => $kycSessionId]);

        if (!$application) {
            return new JsonResponse();
        }

        $application->setCardGuid($cardGuid);
        $this->manager()->flush();

        $kycService->saveKycData($kycSessionId, $cardGuid, $application->getPersonalInfo()->getMobileTelephoneWithCountryCode());

        return new JsonResponse();
    }
}
