<?php

namespace App\Controller;

use App\Entity\BulletinList;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class NewsController
 * @package App\Controller
 * @Route("/bulletin-subscription")
 */
class BulletinSubscriptionController extends BaseController
{
    /**
     * @Route("", name="bulletin_subscription.subscribe", methods={"POST"}, condition="request.isXmlHttpRequest()")
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param TranslatorInterface $translator
     * @return RedirectResponse|Response
     */
    public function subscribe(Request $request, ValidatorInterface $validator, TranslatorInterface $translator)
    {
        $bulletin = new BulletinList();
        $bulletin->setSubscriberEmail($request->get('email'));

	    $errors = $validator->validate($bulletin);

	    if (count($errors) > 0){
		    return $this->json($errors[0]->getMessage(), 422);
	    }

        $this->manager()->persist($bulletin);
	    $this->manager()->flush();

	    return $this->json($translator->trans('bulletin_subscriber.success_message',[],'footer'), 200);
    }
}
