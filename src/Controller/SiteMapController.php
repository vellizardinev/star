<?php

namespace App\Controller;

use App\SiteMap\SiteMapService;
use Doctrine\Common\Annotations\AnnotationException;
use Psr\Cache\InvalidArgumentException;
use ReflectionException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class SiteMapController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->breadcrumbs = $breadcrumbs;
		$this->translator = $translator;
	}

    /**
     * @Route("/site-map", name="site_map")
     * @param SiteMapService $siteMapService
     * @param Request $request
     * @return Response
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function siteMap(SiteMapService $siteMapService, Request $request)
    {
    	$collection = $siteMapService->routesForSiteMap()->itemsForLocale($request->getLocale());

        return $this->render('site_map/site_map.html.twig', [
        	'collection' => $collection,
        ]);
    }
}
