<?php

namespace App\Controller;

use App\Entity\LandingPage;
use App\Entity\ModuleItem;
use App\Entity\Page;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class UserDefinedPageController extends BaseController
{
    /**
     * @param string $path
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function show(string $path, Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $page = $this->manager()->getRepository(Page::class)->findOneBy(['path' => $path]);

        if (!$page) {
            throw $this->createNotFoundException();
        }

        if (!$page->isVisible()) {
            throw $this->createNotFoundException();
        }

        $breadcrumbs->addItem($translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
        $breadcrumbs->addItem($page->getName());

        if ($page instanceof LandingPage) {
            $moduleItems = $this->manager()->getRepository(ModuleItem::class)->findForPage($page);

            return $this->render('user_defined_page/show_landing_page.html.twig', ['page' => $page, 'moduleItems' => $moduleItems]);
        } else {
            return $this->render('user_defined_page/show.html.twig', ['page' => $page]);
        }
    }
}
