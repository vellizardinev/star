<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @package App\Controller
 */
class CreditRouteController extends BaseController
{
    /**
     * @Route("/get-application-route/{slug}/{locale}", name="credits.get_application_route")
     * @param string $slug
     * @param string $locale
     * @param Request $request
     * @param UrlGeneratorInterface $urlGenerator
     * @return JsonResponse
     */
    public function getApplicationRoute(string $slug, string $locale, Request $request, UrlGeneratorInterface $urlGenerator): JsonResponse
    {
        $route = $urlGenerator->generate('credits.credit_apply', ['slug' => $slug, '_locale' => $locale]);

        return new JsonResponse($route, 200);
    }
}
