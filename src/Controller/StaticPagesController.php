<?php

namespace App\Controller;

use App\Annotation\SiteMap;
use App\Entity\Credit;
use App\Entity\Department;
use App\Entity\OnlineApplication\OnlineApplication;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class StaticPagesController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="static_pages.home_page")
     * @SiteMap()
     * @return Response
     * @throws Exception
     */
    public function homePageAction()
    {
        $credits = $this->manager()->getRepository(Credit::class)->findAll();

        return $this->render(
            'static_pages/home_page.html.twig',
            [
                'credits' => $credits,
                'loyaltyProgramUrl' => $this->generateUrl('loyalty_program.index'),
            ]
        );
    }

    /**
     * @return Response
     * @Route({"en": "/about-us", "ua": "/pro-nas", "ru": "/about-us"}, name="static_pages.about_us")
     * @SiteMap()
     */
    public function aboutUsAction()
    {
        $this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
        $this->breadcrumbs->addItem($this->translator->trans('static_pages.title.about_us', [], 'text'), null);

        $departments = $this->manager()->getRepository(Department::class)->findVisible();
        $hasDepartments = count($departments) > 0;
        $mainDepartment = array_shift($departments);

        return $this->render(
            'static_pages/about_us.html.twig',
            [
                'mainDepartment' => $mainDepartment,
                'departments' => $departments,
                'hasDepartments' => $hasDepartments,
            ]
        );
    }
}
