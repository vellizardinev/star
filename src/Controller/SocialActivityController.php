<?php

namespace App\Controller;

use App\Annotation\SiteMap;
use App\Entity\Page;
use App\Enum\Pages;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class SocialActivityController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */ 
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->breadcrumbs = $breadcrumbs;
		$this->translator = $translator;
	}

    /**
     * @Route({"en": "/social-activity", "ua": "/sotsialna-aktyvnist", "ru": "/social-activity"}, name="social_activity.index")
     * @SiteMap()
     * @return RedirectResponse|Response
     */
	public function index(): Response
	{

		$this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.social_activity', [], 'text'));

		$financialCulturePage = $this->manager()->getRepository(Page::class)->findOneBy(['name' => Pages::FINANCIAL_CULTURE]);
		$financialDictionaryPage = $this->manager()->getRepository(Page::class)->findOneBy(['name' => Pages::FINANCIAL_DICTIONARY]);
		$familyFinancesPage = $this->manager()->getRepository(Page::class)->findOneBy(['name' => Pages::FAMILY_FINANCES]);

		return $this->render('social_activity/index.html.twig',[
			'financialCulturePage' => $financialCulturePage,
			'financialDictionaryPage' => $financialDictionaryPage,
			'familyFinancesPage' => $familyFinancesPage
		]);
	}
}
