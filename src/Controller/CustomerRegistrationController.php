<?php

namespace App\Controller;

use App\Breadcrumbs\BreadcrumbsService;
use App\Breadcrumbs\CustomerRegistration;
use App\Breadcrumbs\CustomerVerification;
use App\Entity\Customer;
use App\Enum\Flash;
use App\Form\CustomerRegistrationType;
use App\Form\CustomerVerificationType;
use App\Security\CustomerLoginFormAuthenticator;
use App\Service\MobileApi\CustomerPersister;
use App\Service\MobileApi\Customers;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use stdClass;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CustomerRegistrationController extends BaseController
{
    use AddsCustomerAccountApiErrors;

    private $breadcrumbsService;

    private $translator;

    public function __construct(BreadcrumbsService $breadcrumbsService, TranslatorInterface $translator)
    {
        $this->breadcrumbsService = $breadcrumbsService;
        $this->translator = $translator;
    }

    /**
     * @Route("/customer/registration", name="customer_registration.registration")
     * @param Request $request
     * @param Customers $customersService
     * @param GuardAuthenticatorHandler $authenticatorHandler
     * @param CustomerLoginFormAuthenticator $loginFormAuthenticator
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function registration(Request $request, Customers $customersService, GuardAuthenticatorHandler $authenticatorHandler, CustomerLoginFormAuthenticator $loginFormAuthenticator): Response
    {
        $user = $this->getUser();

        if ($user !== null) {
            return $this->redirectToRoute('static_pages.home_page');
        }

        $customer = new Customer;
        $form = $this->createForm(CustomerRegistrationType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customer->setPassword($form->get('plainPassword')->getData());
            $response = $customersService->createAccount($customer);

	        if ($response->isSuccess()) {
	            $this->addFlash(Flash::SUCCESS, $this->translator->trans('Registration was successfull.', [], 'messages'));

		        return $this->redirectToRoute('customer_login.login');
	        } else {
                $validationErrors = $response->getErrors();

                if (empty($validationErrors)) {
                    $this->addFlash(Flash::ERROR, 'customer_registration.failed');
                    return $this->redirectToRoute('static_pages.home_page');
                } else {
                    $this->addApiError($form, $validationErrors, $this->translator);
                }
            }
        }

        $this->breadcrumbsService->create(new CustomerRegistration());

        return $this->render('customer_registration/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/customer/registration/confirmation", name="customer_registration.verification")
     * @IsGranted("ROLE_CUSTOMER")
     * @param Request $request
     * @param Customers $customersService
     * @param CustomerPersister $customerPersister
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function verification(Request $request, Customers $customersService, CustomerPersister $customerPersister)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $code = new stdClass();
        $code->code = null;
        $form = $this->createForm(CustomerVerificationType::class, $code);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $response = $customersService->activateAccount($customer, $code->code);

            if ($response->isSuccess()) {
                $this->addFlash(Flash::SUCCESS, $this->translator->trans('customer_registration.account_was_activated', [], 'messages'));

                $customer->setIsPhoneActive(true);
                $customerPersister->save($customer);

                return $this->redirectToRoute('online_application.credit_details', ['slug' => $this->defaultCreditSlug()]);
            } else {
                $form->get('code')->addError(new FormError(
                    $this->translator->trans('customer_profile.telephone_was_not_verified', [], 'messages')
                ));
            }
        }

        $this->breadcrumbsService->create(new CustomerVerification());

        return $this->render('customer_registration/verification.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/customer/registration/proceed-to-application", name="customer_registration.proceed_to_application")
     * @IsGranted("ROLE_CUSTOMER")
     * @return Response
     */
    public function proceedToApplication()
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $canProceed = true;

        if ($customer->getIsPhoneActive() === false) {
            $this->addFlash(Flash::ERROR, $this->translator->trans('customer_profile.verify_telephone_to_proceed', [], 'messages'));
            $canProceed = false;
        }

        if ($customer->getIsEmailActive() === false) {
            $this->addFlash(Flash::ERROR, $this->translator->trans('customer_profile.verify_email_to_proceed', [], 'messages'));
            $canProceed = false;
        }

        if ($canProceed) {
            return $this->redirectToRoute('online_application.credit_details', ['slug' => $this->defaultCreditSlug()]);
        } else {
            return $this->redirectToRoute('customer_registration.verification');
        }
    }
}
