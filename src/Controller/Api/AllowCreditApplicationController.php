<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Entity\Customer;
use App\Service\MobileApi\AllowCustomerToApplyOnline;
use ReflectionException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * @package App\Controller
 * @Route("/api/allow-credit-application")
 */
class AllowCreditApplicationController extends BaseController
{
    /**
     * @Route("", name="api.allow_credit_application.check", methods={"GET"})
     * @param AllowCustomerToApplyOnline $allowCustomerToApplyOnline
     * @param bool $checkCustomerAllowedOnlineApplication
     * @return Response
     * @throws \ReflectionException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function check(AllowCustomerToApplyOnline $allowCustomerToApplyOnline, bool $checkCustomerAllowedOnlineApplication)
    {
        $customer = $this->getUser();

        if ($customer === null || !($customer instanceof Customer)) {
            return new JsonResponse([], 403);
        }

        if ($checkCustomerAllowedOnlineApplication === false) {
            return new JsonResponse(['isDenied' => false], 200);
        }

        $isDenied = $allowCustomerToApplyOnline->isDenied($customer);

        return new JsonResponse(['isDenied' => $isDenied], 200);
    }
}
