<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package App\Controller
 * @Route("/api/easy-pay")
 */
class EasyPayController extends BaseController
{
    /**
     * @Route("/confirm", name="api.easy_pay.confirm_payment", methods={"GET", "POST"})
     * @return Response
     */
    public function confirm()
    {
        return new JsonResponse();
    }
}
