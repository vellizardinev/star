<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use App\Entity\CreditShortApplication;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Response\ResponseService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package App\Controller
 */
class CreditApplicationController extends BaseController
{
    private $allowedIPs = [
        '127.0.0.1',
        '94.26.48.63',
        '10.120.0.89',
        '91.216.95.122',
    ];

    /**
     * @Route("/api/credit-application/{id}", name="api.credit_application.list", methods={"GET"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param ResponseService $responseService
     * @param string $callFlowKey
     * @param LoggerInterface $callFlowLog
     * @param int|null $id
     * @return Response
     */
    public function list(Request $request, ResponseService $responseService, string $callFlowKey, LoggerInterface $callFlowLog, ?int $id = null)
    {
        if (array_search($request->getClientIp(), $this->allowedIPs) === false) {
            $callFlowLog->alert(sprintf('Received a credit applications request from unauthorized IP: %s.', $request->getClientIp()));
            return $this->json(null, 403);
        }

        if (!$request->headers->has('X-Auth-Token') || $request->headers->get('X-Auth-Token') !== $callFlowKey) {
            $callFlowLog->alert(sprintf('Received a credit applications request with invalid X-Auth-Token: %s.', $request->getClientIp()));
            return $this->json(null, 403);
        }

        $callFlowLog->info(sprintf('Received a credit applications request with parameter ID=%s.', $id));

        $creditApplications = $this->manager()
            ->getRepository(CreditShortApplication::class)
            ->withIdGreaterThan($id);

        return $responseService->successResponse($creditApplications);
    }

    /**
     * @Route("/api/online-application/{id}", name="api.online_application.list", methods={"GET"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param ResponseService $responseService
     * @param string $callFlowKey
     * @param LoggerInterface $callFlowLog
     * @param int|null $id
     * @return Response
     */
    public function listOnlineApplications(Request $request, ResponseService $responseService, string $callFlowKey, LoggerInterface $callFlowLog, ?int $id = null)
    {
        if (array_search($request->getClientIp(), $this->allowedIPs) === false) {
            $callFlowLog->alert(sprintf('Received a online applications request from unauthorized IP: %s.', $request->getClientIp()));
            return $this->json(null, 403);
        }

        if (!$request->headers->has('X-Auth-Token') || $request->headers->get('X-Auth-Token') !== $callFlowKey) {
            $callFlowLog->alert(sprintf('Received a online applications request with invalid X-Auth-Token: %s.', $request->getClientIp()));
            return $this->json(null, 403);
        }

        $callFlowLog->info(sprintf('Received a online applications request with parameter ID=%s.', $id));

        $onlineApplications = $this->manager()
            ->getRepository(OnlineApplication::class)
            ->withConfirmationIdGreaterThan($id);

        return $responseService->successResponse($onlineApplications);
    }
}
