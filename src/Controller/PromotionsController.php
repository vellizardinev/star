<?php

namespace App\Controller;

use App\Annotation\SiteMap;
use App\Entity\Promotion;
use Doctrine\ORM\NonUniqueResultException;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class PromotionsController
 * @package AppBundle\Controller
 * @Route({"en": "/promotions", "ua": "/aktsii", "ru": "/promotions"})
 */
class PromotionsController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */ 
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->translator = $translator;
		$this->breadcrumbs = $breadcrumbs;
	}

    /**
     * @Route("", name="promotions.index")
     * @SiteMap()
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     * @throws NonUniqueResultException
     */
	public function index(PaginatorInterface $paginator, Request $request): Response
	{
		$paginatedPromotions = $paginator->paginate(
			$this->manager()->getRepository(Promotion::class)->filtered([], true, true),
			$request->query->getInt('page', 1),
			$request->query->get('limit') ?? 9
		);

		$this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.promotions', [], 'text'), $this->generateUrl('promotions.index'));

		return $this->render('promotions/index.html.twig', [
		    'promotions' => $paginatedPromotions,
            'featuredPromo' => $this->manager()->getRepository(Promotion::class)->featured(),
        ]);
	}

    /**
     * @Route("/{slug}", name="promotions.show")
     * @Entity("promotion", expr="repository.findbySlug(slug)")
     * @param Promotion $promotion
     * @return Response
     * @throws NonUniqueResultException
     */
	public function show(Promotion $promotion): Response
	{
        if ($promotion->isVisible() === false) {
            return $this->redirectToRoute('promotions.index');
        }

        if ($promotion->getLandingPage()) {
            return $this->redirect('/' . $promotion->getLandingPage()->getPath());
        }

		$this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.promotions', [], 'text'), $this->generateUrl('promotions.index'));
		$this->breadcrumbs->addItem($promotion->getTitle());

		$promotionRepo = $this->manager()->getRepository(Promotion::class);
		$previousPromotion = $promotionRepo->getSibling($promotion);
		$nextPromotion = $promotionRepo->getSibling($promotion, false);

		return $this->render('promotions/show.html.twig', [
			'promotion' => $promotion,
			'previousPromotion' => $previousPromotion,
			'nextPromotion' => $nextPromotion
		]);
	}
}
