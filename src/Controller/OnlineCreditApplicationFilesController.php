<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Entity\OnlineApplication\UploadedDocuments;
use App\Enum\Flash;
use App\File\FileManager;
use App\Service\MobileApi\UploadFile;
use Exception;
use League\Flysystem\FileExistsException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/online-application-files")
 * @IsGranted("ROLE_CUSTOMER")
 */
class OnlineCreditApplicationFilesController extends BaseController
{
    /**
     * @Route("/replace/{erpId}/{type}", name="admin.online_credit_application_files.replace", methods={"POST"})
     * @param Request $request
     * @param string $erpId
     * @param string $type
     * @param ValidatorInterface $validator
     * @param FileManager $fileManager
     * @param UploadFile $uploadFileService
     * @param TranslatorInterface $translator
     * @return JsonResponse
     * @throws FileExistsException
     * @throws Exception
     */
    public function replace(
        Request $request,
        string $erpId,
        string $type,
        ValidatorInterface $validator,
        FileManager $fileManager,
        UploadFile $uploadFileService,
        TranslatorInterface $translator
    )
    {
        $application = $this->manager()
            ->getRepository(OnlineApplication::class)
            ->findOneBy(['erpId' => $erpId]);

        // Upload the file and create Document entity
        $uploadedFile = $request->files->get('file');

        $errors = $validator->validate($uploadedFile, [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M'])]);

        if ($errors->count() > 0) {
            return $this->json($errors, 422);
        }

        $document = $fileManager->uploadDocument($uploadedFile);
        $this->manager()->persist($document);
        $this->manager()->flush();

        // Send the new Document via the API
        $success = $uploadFileService->uploadAsBase64String($document, $erpId, $type);

        if ($application) {
            // Delete the old Document of the same type
            $this->deleteUploadedDocument($application->getUploadedDocuments(), $fileManager, $type);
            $this->manager()->flush();

            // Associate the Document with the credit application
            $this->addUploadedDocument($application->getUploadedDocuments(), $document, $type);
            $this->manager()->flush();
        }

        if ($success) {
            $this->addFlash(Flash::SUCCESS, $translator->trans('File was uploaded', [], 'messages'));
            // Return success response
            return new JsonResponse(null, 200);
        } else {
            // Return success response
            return new JsonResponse(['detail' => $translator->trans('The file upload failed.', [], 'messages')], 400);
        }
    }

    /**
     * @param UploadedDocuments $uploadedDocuments
     * @param FileManager $fileManager
     * @param string $type
     * @throws Exception
     */
    private function deleteUploadedDocument(UploadedDocuments $uploadedDocuments, FileManager $fileManager, string $type): void
    {
        switch ($type) {
            case UploadFile::FILE_TYPE_PASSPORT_FIRST_PAGE:
                $fileManager->delete($uploadedDocuments->getOldStylePassportFirstPage());
                $this->manager()->remove($uploadedDocuments->getOldStylePassportFirstPage());
                break;
            case UploadFile::FILE_TYPE_PASSPORT_SECOND_PAGE:
                $fileManager->delete($uploadedDocuments->getOldStylePassportSecondPage());
                $this->manager()->remove($uploadedDocuments->getOldStylePassportSecondPage());
                break;
            case UploadFile::FILE_TYPE_PASSPORT_PHOTO_AT_25:
                $fileManager->delete($uploadedDocuments->getOldStylePassportPhotoAt25());
                $this->manager()->remove($uploadedDocuments->getOldStylePassportPhotoAt25());
                break;
            case UploadFile::FILE_TYPE_PASSPORT_PASTED_PHOTO_AT_25:
                $fileManager->delete($uploadedDocuments->getOldStylePassportPastedPhotoAt25());
                $this->manager()->remove($uploadedDocuments->getOldStylePassportPastedPhotoAt25());
                break;
            case UploadFile::FILE_TYPE_PASSPORT_PHOTO_AT_45:
                $fileManager->delete($uploadedDocuments->getOldStylePassportPhotoAt45());
                $this->manager()->remove($uploadedDocuments->getOldStylePassportPhotoAt45());
                break;
            case UploadFile::FILE_TYPE_PASSPORT_PASTED_PHOTO_AT_45:
                $fileManager->delete($uploadedDocuments->getOldStylePassportPastedPhotoAt45());
                $this->manager()->remove($uploadedDocuments->getOldStylePassportPastedPhotoAt45());
                break;
            case UploadFile::FILE_TYPE_PASSPORT_SELFIE_WITH_PASSPORT:
                $fileManager->delete($uploadedDocuments->getOldStylePassportSelfieWithPassport());
                $this->manager()->remove($uploadedDocuments->getOldStylePassportSelfieWithPassport());
                break;
            case UploadFile::FILE_TYPE_PASSPORT_PROOF_OF_RESIDENCE:
                $fileManager->delete($uploadedDocuments->getOldStylePassportProofOfResidence());
                $this->manager()->remove($uploadedDocuments->getOldStylePassportProofOfResidence());
                break;
            case UploadFile::FILE_TYPE_ID_CARD_REGISTRATION_APPENDIX:
                $fileManager->delete($uploadedDocuments->getNewStylePassportRegistrationAppendix());
                $this->manager()->remove($uploadedDocuments->getNewStylePassportRegistrationAppendix());
                break;
            case UploadFile::FILE_TYPE_ID_CARD_SELFIE:
                $fileManager->delete($uploadedDocuments->getNewStylePassportSelfieWithIdCard());
                $this->manager()->remove($uploadedDocuments->getNewStylePassportSelfieWithIdCard());
                break;
            case UploadFile::FILE_TYPE_ID_CARD_PHOTO:
                $fileManager->delete($uploadedDocuments->getNewStylePassportPhotoPage());
                $this->manager()->remove($uploadedDocuments->getNewStylePassportPhotoPage());
                break;
            case UploadFile::FILE_TYPE_ID_CARD_VALIDITY:
                $fileManager->delete($uploadedDocuments->getNewStylePassportValidityPage());
                $this->manager()->remove($uploadedDocuments->getNewStylePassportValidityPage());
                break;
            case UploadFile::FILE_TYPE_PENSION_CERTIFICATE_FIRST_SPREAD:
                $fileManager->delete($uploadedDocuments->getOldStylePensionCertificateFirstSpread());
                $this->manager()->remove($uploadedDocuments->getOldStylePensionCertificateFirstSpread());
                break;
            case UploadFile::FILE_TYPE_PENSION_CERTIFICATE_VALIDITY_RECORD:
                $fileManager->delete($uploadedDocuments->getOldStylePensionCertificateValidityRecord());
                $this->manager()->remove($uploadedDocuments->getOldStylePensionCertificateValidityRecord());
                break;
            case UploadFile::FILE_TYPE_PENSION_CERTIFICATE_PHOTO_AND_VALIDITY_SPREAD:
                $fileManager->delete($uploadedDocuments->getNewStylePensionCertificatePhotoAndValiditySpread());
                $this->manager()->remove($uploadedDocuments->getNewStylePensionCertificatePhotoAndValiditySpread());
                break;
            default :
                break;
        }
    }

    private function addUploadedDocument(UploadedDocuments $uploadedDocuments, Document $document, string $type): void
    {
        switch ($type) {
            case UploadFile::FILE_TYPE_PASSPORT_FIRST_PAGE:
                $uploadedDocuments->setOldStylePassportFirstPage($document);
                break;
            case UploadFile::FILE_TYPE_PASSPORT_SECOND_PAGE:
                $uploadedDocuments->setOldStylePassportSecondPage($document);
                break;
            case UploadFile::FILE_TYPE_PASSPORT_PHOTO_AT_25:
                $uploadedDocuments->setOldStylePassportPhotoAt25($document);
                break;
            case UploadFile::FILE_TYPE_PASSPORT_PASTED_PHOTO_AT_25:
                $uploadedDocuments->setOldStylePassportPastedPhotoAt25($document);
                break;
            case UploadFile::FILE_TYPE_PASSPORT_PHOTO_AT_45:
                $uploadedDocuments->setOldStylePassportPhotoAt45($document);
                break;
            case UploadFile::FILE_TYPE_PASSPORT_PASTED_PHOTO_AT_45:
                $uploadedDocuments->setOldStylePassportPastedPhotoAt45($document);
                break;
            case UploadFile::FILE_TYPE_PASSPORT_SELFIE_WITH_PASSPORT:
                $uploadedDocuments->setOldStylePassportSelfieWithPassport($document);
                break;
            case UploadFile::FILE_TYPE_PASSPORT_PROOF_OF_RESIDENCE:
                $uploadedDocuments->setOldStylePassportProofOfResidence($document);
                break;
            case UploadFile::FILE_TYPE_ID_CARD_REGISTRATION_APPENDIX:
                $uploadedDocuments->setNewStylePassportRegistrationAppendix($document);
                break;
            case UploadFile::FILE_TYPE_ID_CARD_SELFIE:
                $uploadedDocuments->setNewStylePassportSelfieWithIdCard($document);
                break;
            case UploadFile::FILE_TYPE_ID_CARD_PHOTO:
                $uploadedDocuments->setNewStylePassportPhotoPage($document);
                break;
            case UploadFile::FILE_TYPE_ID_CARD_VALIDITY:
                $uploadedDocuments->setNewStylePassportValidityPage($document);
                break;
            case UploadFile::FILE_TYPE_PENSION_CERTIFICATE_FIRST_SPREAD:
                $uploadedDocuments->setOldStylePensionCertificateFirstSpread($document);
                break;
            case UploadFile::FILE_TYPE_PENSION_CERTIFICATE_VALIDITY_RECORD:
                $uploadedDocuments->setOldStylePensionCertificateValidityRecord($document);
                break;
            case UploadFile::FILE_TYPE_PENSION_CERTIFICATE_PHOTO_AND_VALIDITY_SPREAD:
                $uploadedDocuments->setNewStylePensionCertificatePhotoAndValiditySpread($document);
                break;
            default :
                break;
        }
    }
}
