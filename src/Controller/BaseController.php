<?php

namespace App\Controller;

use App\Entity\Credit;
use App\Entity\Setting;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class BaseController
 * @method User getUser
 */
abstract class BaseController extends AbstractController
{
    /**
     * @return ObjectManager
     */
    public function manager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param $setting
     * @return string|null
     */
    public function getSetting($setting)
    {
        $settingEntity = $this->manager()->getRepository(Setting::class)->get($setting);

        if (!$settingEntity) {
            return '';
        }

        return $settingEntity->getValue();
    }

    public function redirectBack(string $defaultUrl)
    {
        if ($redirectUrl = $this->get('request_stack')->getMasterRequest()->getSession()->get('redirect_back')) {
            $this->get('request_stack')->getMasterRequest()->getSession()->remove('redirect_back');

            return $this->redirect($redirectUrl);
        }

        return $this->redirect($defaultUrl);
    }

    public function addRedirect(string $url)
    {
        $this->get('request_stack')->getMasterRequest()->getSession()->set('redirect_back', $url);
    }

	public function defaultCreditSlug(): string
	{
		$slug = 'icredit';
		$credit = $this->manager()->getRepository(Credit::class)->findOneBy(['slug' => $slug]);

		if (null !== $credit) {
			return $slug;
		}

		$credit = $this->manager()->getRepository(Credit::class)->findOneBy([]);

		return $credit->getSlug();
	}
}
