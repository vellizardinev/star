<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller
 * @Route("pay-online")
 */
class PaymentController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("/success", name="pay_online.success", methods={"GET", "POST"})
     */
    public function success(): Response
    {
        return $this->render('pay_online/success.html.twig');
    }

    /**
     * @Route("/failed", name="pay_online.failed", methods={"GET", "POST"})
     */
    public function failed(): Response
    {
        return $this->render('pay_online/failed.html.twig');
    }
}
