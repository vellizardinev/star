<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Enum\Flash;
use App\Form\ProfileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProfileController extends BaseController
{
    /**
     * @Route("/profile", name="profile.update", methods={"POST", "GET"})
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return Response
     * @IsGranted("ROLE_USER")
     */
    public function update(Request $request, TranslatorInterface $translator)
    {
        $user = $this->getUser();
        $profile = $user->getProfile();

        if (!$profile) {
	        $profile = (new Profile())->setUser($user);
            $user->setProfile($profile);
        }

        $form = $this->createForm(ProfileType::class, $profile, ['add_captcha' => true]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($profile);
            $this->manager()->flush();

            $this->addFlash(Flash::SUCCESS, $translator->trans('Your profile was updated.', [], 'messages'));

            return $this->redirectToRoute('static_pages.home_page');
        }

        return $this->render(
            'profile/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
