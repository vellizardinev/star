<?php

namespace App\Controller;

use App\Entity\Poll;
use App\Entity\PollEntry;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Mail\MailService;
use App\Response\ResponseService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class PollController
 * @package App\Controller
 * @Route("/poll")
 */
class PollController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("/active", name="poll.active", methods={"GET"})
     * @param RouterInterface $router
     * @param Request $request
     * @return Response
     */
    public function active(RouterInterface $router, Request $request)
    {
        $locale = $request->query->get('locale', 'ua');

        $activePolls = $this->manager()
            ->getRepository(Poll::class)
            ->filtered([], true)
            ->getResult();

        $data = [];

        foreach ($activePolls as $poll) {
            /** @var $poll Poll */
            $data[] = [
                'id' => $poll->getId(),
                'title' => $poll->getTitle(),
                'route' => $router->generate('poll.complete', ['id' => $poll->getId(), '_locale' => $locale]),
            ];
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/{id}/complete", name="poll.complete", methods={"GET", "POST"}, requirements={"id": "\d+"})
     * @param Poll $poll
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param MailService $mailService
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function complete(Poll $poll, Request $request, ValidatorInterface $validator, MailService $mailService)
    {
        if ($request->getMethod() === 'POST') {
            $pollEntry = (new PollEntry)
                ->setPoll($poll)
                ->setEmail($request->request->get('email'))
                ->setFullName($request->request->get('fullName'));

            $data = $request->request->all();
            unset($data['email']);
            unset($data['fullName']);
            $pollEntry->setData($data);

            $errors = $validator->validate($pollEntry);

            if ($errors->count() === 0) {
                $this->manager()->persist($pollEntry);
                $this->manager()->flush();
                $this->addFlash(Flash::SUCCESS, $this->translator->trans('The poll was submitted. Thank you for your time.', [], 'messages'));

                $mailService
                    ->to($this->getSetting(Settings::SEND_POLL_ENTRY_TO))
                    ->send(new \App\Mail\PollEntry($poll, $pollEntry->getData()));

                return $this->redirectToRoute('static_pages.home_page');
            }
        }

        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.home', [], 'text'),
            $this->generateUrl('static_pages.home_page')
        );
        $this->breadcrumbs->addItem($this->translator->trans('title.poll', [], 'text'));

        return $this->render(
            'poll/show.html.twig',
            [
                'poll' => $poll,
                'errors' => $errors ?? null,
            ]
        );
    }
}
