<?php

namespace App\Controller;

use App\Annotation\SiteMap;
use App\Entity\CareersPartner;
use App\Entity\JobApplication;
use App\Entity\JobOpening;
use App\Entity\Page;
use App\Enum\Flash;
use App\Enum\Pages;
use App\Enum\Settings;
use App\Form\JobApplicationType;
use App\Mail\MailService;
use App\Service\RabotaConnector;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class CareersController
 * @package AppBundle\Controller
 * @Route({"en": "/career", "ua": "/kariera", "ru": "/career"})
 */
class CareersController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->translator = $translator;
		$this->breadcrumbs = $breadcrumbs;
	}

    /**
     * @Route("", name="careers.index")
     * @SiteMap()
     * @param RabotaConnector $connector
     * @return Response
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
	public function index(RabotaConnector $connector): Response
	{
        $data = $connector->vacancies(0);

        $jobOpenings = array_slice($data['vacancies'], 0, 20);

		$this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.careers', [], 'text'),  $this->generateUrl('careers.index'));

		return $this->render('careers/index.html.twig', [
			'jobOpenings' => $jobOpenings,
		]);
	}

    /**
     * @Route({"en": "/all", "ua": "/vsi", "ru": "/all"}, name="career.all", methods={"GET"})
     * @SiteMap()
     * @param Request $request
     * @param RabotaConnector $connector
     * @param PaginatorInterface $paginator
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
	public function all(Request $request, RabotaConnector $connector, PaginatorInterface $paginator)
	{
	    $page = $request->query->getInt('page', 1);
	    $cityId = $request->query->get('city', null);
	    $cityId = empty($cityId) ? null : $cityId;

		$data = $connector->vacancies($page - 1, $cityId);

		$jobOpenings = $data['vacancies'];
		$total = $data['total'];

		$jobOpenings = $paginator->paginate(
		    $jobOpenings,
            $page,
        20
        );

		$jobOpenings->setTotalItemCount($total);

		$cities = $connector->cities();

		$this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.careers', [], 'text'),  $this->generateUrl('careers.index'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.job_openings', [], 'text'));

		return $this->render('careers/job_opening/index.html.twig', [
			'jobOpenings' => $jobOpenings,
			'cities' => $cities,
		]);
	}

    /**
     * @Route({"en": "/details/{jobOpeningId}", "ua": "/деталі/{jobOpeningId}", "ru": "/детали/{jobOpeningId}"}, name="career.vacancy_details", methods={"GET"}, options={ "utf8": true })
     * @param string $jobOpeningId
     * @param RabotaConnector $connector
     * @return Response
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
	public function details(string $jobOpeningId, RabotaConnector $connector)
    {
        $jobOpening = $connector->vacancyDetails($jobOpeningId);

        $this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
        $this->breadcrumbs->addItem($this->translator->trans('public.title.careers', [], 'text'),  $this->generateUrl('careers.index'));
        $this->breadcrumbs->addItem($this->translator->trans('public.title.job_openings', [], 'text'), $this->generateUrl('career.all'));
        $this->breadcrumbs->addItem($jobOpening['title']);

        return $this->render('careers/job_opening/details.html.twig', [
            'jobOpening' => $jobOpening,
            'applicationUrl' => $connector->applicationUrl($jobOpeningId),
        ]);
    }

    /**
     * @param JobOpening $jobOpening
     * @param Request $request
     * @param MailService $mailService
     * @return JsonResponse|RedirectResponse|Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @Route({"en": "/{reference}/apply", "ua": "/{reference}/pryiniaty", "ru": "/{reference}/apply"}, name="career.apply")
     */
	public function apply(JobOpening $jobOpening, Request $request, MailService $mailService)
    {
        $jobApplication = new JobApplication;

        $form = $this->createForm(JobApplicationType::class, $jobApplication);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
	        $recipient = $this->getSetting(Settings::SEND_JOB_APPLICATIONS_TO);

	        $mail = $mailService
		        ->to($recipient);

	        if ($jobApplication->getCv()) {
		        $mail->attach($jobApplication->getCv());
	        }

	        $mail->send(new \App\Mail\JobApplication($jobApplication, $jobOpening));


	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('Your application was send. We will get back to you soon.', [], 'messages'));

	        return $this->redirect($this->getSetting(Settings::JOB_APPLICATION_REDIRECT_PATH));
        }

        $this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
        $this->breadcrumbs->addItem($this->translator->trans('public.title.careers', [], 'text'), $this->generateUrl('careers.index'));
        $this->breadcrumbs->addItem($this->translator->trans('public.title.apply_for_a_job', [], 'text'), null);

        return $this->render('careers/apply.html.twig', [
            'form' => $form->createView(),
            'jobOpening' => $jobOpening,
        ]);
    }

	/**
	 * @Route({"en": "/apply-for-job", "ua": "/vlashtuvatysia-na-robotu", "ru": "/apply-for-job"}, name="career.apply_for_job")
     * @SiteMap()
	 * @param Request $request
	 * @param MailService $mailService
	 * @return Response
	 * @throws LoaderError
	 * @throws RuntimeError
	 * @throws SyntaxError
	 */
	public function applyForAJob(Request $request, MailService $mailService): Response
	{
		$jobApplication = new JobApplication;

		$form = $this->createForm(JobApplicationType::class, $jobApplication);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$recipient = $this->getSetting(Settings::SEND_JOB_APPLICATIONS_TO);

			$jobOpening = (new JobOpening())
				->setPosition($this->translator->trans('apply_for_job.position', [], 'text'));

			$mail = $mailService
				->to($recipient);

			if ($jobApplication->getCv()) {
				$mail->attach($jobApplication->getCv());
			}

			$mail->send(new \App\Mail\JobApplication($jobApplication, $jobOpening));

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('Your application was send. We will get back to you soon.', [], 'messages'));

			return $this->redirect($this->getSetting(Settings::JOB_APPLICATION_REDIRECT_PATH));
		}

		$this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.careers', [], 'text'), $this->generateUrl('careers.index'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.apply_for_job', [], 'text'), null);

		return $this->render('careers/apply_for_job/index.html.twig', [
			'form' => $form->createView(),
		]);
	}

    /**
     * @param Request $request
     * @param MailService $mailService
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @Route({"en": "/become-a-credit-consultant", "ua": "/staty-kredytnym-konsultantom", "ru": "/become-a-credit-consultant"}, name="career.become_credit_consultant")
     * @SiteMap()
     */
	public function becomeCreditConsultant(Request $request, MailService $mailService)
	{
		$jobApplication = new JobApplication();

		$form = $this->createForm(JobApplicationType::class, $jobApplication);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$recipient = $this->getSetting(Settings::SEND_JOB_APPLICATIONS_TO);

			$jobOpening = (new JobOpening())
				->setPosition($this->translator->trans('become_credit_consultant.position', [], 'text'));

			$mail = $mailService
                ->to($recipient);

            if ($jobApplication->getCv()) {
                $mail->attach($jobApplication->getCv());
            }

            $mail->send(new \App\Mail\JobApplication($jobApplication, $jobOpening));

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('Your application was send. We will get back to you soon.', [], 'messages'));

			return $this->redirect($this->getSetting(Settings::JOB_APPLICATION_REDIRECT_PATH));
		}

		$this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.careers', [], 'text'), $this->generateUrl('careers.index'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.become_credit_consultant', [], 'text'), null);

		return $this->render('careers/credit_consultant/index.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @param Request $request
	 * @return Response
	 * @Route({"en": "/partners-and-discounts", "ua": "/partners-and-discounts", "ru": "/partners-and-discounts"}, name="career.partners.index")
	 */
	public function partners(Request $request)
	{
		// Page will not be available at launch of the site
		throw $this->createNotFoundException();

		$partners = $this->manager()->getRepository(CareersPartner::class)->filtered($request->query->all(), true)->getResult();

		$this->breadcrumbs->addItem(
			$this->translator->trans('public.title.home', [], 'text'),
			$this->generateUrl('static_pages.home_page')
		);
		$this->breadcrumbs->addItem(
			$this->translator->trans('public.title.careers', [], 'text'),
			$this->generateUrl('careers.index')
		);
		$this->breadcrumbs->addItem($this->translator->trans('public.title.partners_and_discounts', [], 'text'), null);

		return $this->render('careers/partners_and_discounts/index.html.twig', ['partners' => $partners]);
	}

	/**
	 * @param Request $request
	 * @return Response
	 * @Route({"en": "/internship-program", "ua": "/internship-program", "ru": "/internship-program"}, name="career.internship_program.index")
	 */
	public function internshipProgram(Request $request)
	{
	    // Page will not be available at launch
	    throw $this->createNotFoundException();

		$page = $this->manager()->getRepository(Page::class)->findOneBy(['name' => Pages::INTERNSHIP_PROGRAMME]);

		$this->breadcrumbs->addItem(
			$this->translator->trans('public.title.home', [], 'text'),
			$this->generateUrl('static_pages.home_page')
		);
		$this->breadcrumbs->addItem(
			$this->translator->trans('public.title.careers', [], 'text'),
			$this->generateUrl('careers.index')
		);
		$this->breadcrumbs->addItem($page->getName(), null);

		return $this->render('careers/custom_page.html.twig', ['page' => $page]);
	}

	/**
	 * @param Request $request
	 * @return Response
	 */
	// @Route({"en": "/motivation-program", "ua": "/motyvatsiini-prohramy", "ru": "/motivation-program"}, name="career.motivation_program.index")
	public function motivationProgram(Request $request)
	{
		$page = $this->manager()->getRepository(Page::class)->findOneBy(['name' => Pages::MOTIVATION_PROGRAMME]);

		$this->breadcrumbs->addItem(
			$this->translator->trans('public.title.home', [], 'text'),
			$this->generateUrl('static_pages.home_page')
		);
		$this->breadcrumbs->addItem(
			$this->translator->trans('public.title.careers', [], 'text'),
			$this->generateUrl('careers.index')
		);
		$this->breadcrumbs->addItem($page->getName(), null);

		return $this->render('careers/custom_page.html.twig', ['page' => $page]);
	}

	/**
	 * @param Request $request
	 * @return Response
    // @SiteMap()
	 */
    // @Route({"en": "/icredit-as-an-employer", "ua": "/icredit-yak-robotodavets", "ru": "/icredit-as-an-employer"}, name="career.i_credit_as_an_employer.index")
	public function iCreditAsAnEmployer(Request $request)
	{
		$page = $this->manager()->getRepository(Page::class)->findOneBy(['name' => Pages::ICREDIT_AS_EMPLOYER]);

		$this->breadcrumbs->addItem(
			$this->translator->trans('public.title.home', [], 'text'),
			$this->generateUrl('static_pages.home_page')
		);
		$this->breadcrumbs->addItem(
			$this->translator->trans('public.title.careers', [], 'text'),
			$this->generateUrl('careers.index')
		);
		$this->breadcrumbs->addItem($page->getName(), null);

		return $this->render('careers/custom_page.html.twig', ['page' => $page]);
	}
}
