<?php

namespace App\Controller;

use App\Annotation\NeedsActiveCustomerAccount;
use App\Breadcrumbs\BreadcrumbsService;
use App\Breadcrumbs\CustomerProfileAcceptCreditOffer;
use App\Breadcrumbs\CustomerProfileSignRegularDocuments;
use App\Entity\CreditDocument;
use App\Entity\Customer;
use App\Enum\CreditDocumentType;
use App\Enum\Flash;
use App\Form\AcceptCreditOfferType;
use App\Form\SignCreditDocumentsType;
use App\Service\MobileApi\CreditDocuments;
use App\Service\MobileApi\CustomerCredits;
use App\Service\MobileApi\DataTransferObject\DocumentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use stdClass;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @package App\Controller
 * @Route({"en": "/credit-documents", "ua": "/credit-documents", "ru": "/credit-documents"})
 * @IsGranted("ROLE_CUSTOMER")
 */
class CreditDocumentsController extends BaseController
{
    private $breadcrumbsService;

    private $translator;

    public function __construct(BreadcrumbsService $breadcrumbsService, TranslatorInterface $translator)
    {
        $this->breadcrumbsService = $breadcrumbsService;
        $this->translator = $translator;
    }

    /**
     * @Route("/accept-offer/{erpId}", name="credit_documents.accept_offer")
     * @NeedsActiveCustomerAccount()
     * @param string $erpId
     * @param CreditDocuments $creditDocumentsService
     * @param Request $request
     * @param CustomerCredits $creditsService
     * @return Response
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function acceptOffer(string $erpId, CreditDocuments $creditDocumentsService, Request $request, CustomerCredits $creditsService)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $currentApplication = $creditsService->getCredits($customer)->creditApplication($erpId);

        if (null === $currentApplication || $currentApplication->isPendingOfferConfirmation() === false) {
            throw $this->createNotFoundException();
        }

        $documentType = new DocumentType(CreditDocuments::DOCUMENT_TYPE_OFFER, CreditDocuments::DOCUMENT_TYPE_OFFER_NAME);

        $form = $this->createForm(AcceptCreditOfferType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $isAccepted = $creditDocumentsService->acceptCreditOffer($erpId);

            if ($isAccepted) {
                $this->addFlash(Flash::SUCCESS, $this->translator->trans('credit_documents.credit_offer_was_accepted', [], 'messages'));

                return $this->redirectToRoute('credit_documents.sign_regular_documents', ['erpId' => $erpId]);

            } else {
                $this->addFlash(Flash::ERROR, $this->translator->trans('credit_documents.there_was_a_problem_accepting_the_credit_offer', [], 'messages'));

                return $this->redirectToRoute('customer_profile.my_credits');
            }
        }

        $this->breadcrumbsService->create(new CustomerProfileAcceptCreditOffer);

        return $this->render(
            'credit_documents/accept_offer.html.twig',
            [
                'type' => $documentType,
                'erpId' => $erpId,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/sign-credit-documents/{erpId}", name="credit_documents.sign_regular_documents")
     * @NeedsActiveCustomerAccount()
     * @param string $erpId
     * @param CreditDocuments $creditDocumentsService
     * @param Request $request
     * @param \App\Service\MobileApi\CustomerCredits $creditsService
     * @return Response
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function signRegularDocuments(string $erpId, CreditDocuments $creditDocumentsService, Request $request, CustomerCredits $creditsService)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $currentApplication = $creditsService->getCredits($customer)->creditApplication($erpId);

        if (null === $currentApplication || $currentApplication->isPendingSigningRegularDocs() === false) {
            throw $this->createNotFoundException();
        }

        $regularDocumentTypes = $creditDocumentsService->getRegularDocumentTypes($erpId)->getDocumentTypes();

        $code = new stdClass();
        $code->code = null;
        $form = $this->createForm(SignCreditDocumentsType::class, $code);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $response = $creditDocumentsService->signRegularDocuments($erpId, $code->code, $request->getClientIp());

            if ($response->isSuccess()) {
                $this->addFlash(Flash::SUCCESS, $this->translator->trans('credit_documents.credit_documents_were_signed', [], 'messages'));

                return $this->redirectToRoute('customer_profile.my_credits');
            } else {
                if ($response->isCodeWrong()) {
                    $form->get('code')->addError(
                        new FormError(
                            $this->translator->trans('credit_documents.invalid_code', [], 'validators')
                        )
                    );
                } elseif ($response->hasCodeExpired()) {
                    $form->get('code')->addError(
                        new FormError(
                            $this->translator->trans('credit_documents.code_has_expired', [], 'validators')
                        )
                    );
                } elseif ($response->hasAlreadySigned()) {
                    $this->addFlash(Flash::ERROR, $this->translator->trans('credit_documents.already_signed', [], 'messages'));
                } else {
                    $this->addFlash(Flash::ERROR, $this->translator->trans('credit_documents.signing_credit_documents_failed', [], 'messages'));
                }
            }
        }

        $this->breadcrumbsService->create(new CustomerProfileSignRegularDocuments);

        return $this->render(
            'credit_documents/sign_regular_documents.html.twig',
            [
                'regularDocumentTypes' => $regularDocumentTypes,
                'erpId' => $erpId,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/stored-document/{documentId}", name="credit_documents.stored_document", methods={"GET"})
     * @param string $documentId
     * @param CreditDocuments $creditDocumentsService
     * @return Response
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function storedDocument(string $documentId, CreditDocuments $creditDocumentsService)
    {
        $response = new Response($creditDocumentsService->getStoredDocument($documentId)->getContent());
        $response->headers->set('Content-Type', 'application/pdf');

        return $response;
    }

    public function documentsForApplication(string $erpId): Response
    {
        $documents = $this->manager()
            ->getRepository(CreditDocument::class)
            ->forCreditApplication($erpId, CreditDocumentType::ALL);

        return $this->render('credit_documents/_documents.html.twig', [
            'documents' => $documents,
        ]);
    }

    public function documentsForCredit(string $erpId, CreditDocuments $creditDocumentsService): Response
    {
        $documents = $creditDocumentsService->storedDocumentsList($erpId)->getStoredDocuments();

        return $this->render('credit_documents/_documents_for_credit.html.twig', [
            'documents' => $documents,
        ]);
    }
}
