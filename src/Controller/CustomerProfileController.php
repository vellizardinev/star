<?php

namespace App\Controller;

use App\Breadcrumbs\BreadcrumbsService;
use App\Breadcrumbs\CustomerProfileChangePassword;
use App\Breadcrumbs\CustomerProfileMessages;
use App\Breadcrumbs\CustomerProfileMyCredits;
use App\Breadcrumbs\CustomerProfilePersonalInfo;
use App\Entity\CreditDocument;
use App\Entity\Customer;
use App\Entity\Message;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Enum\ActiveCreditStatus;
use App\Enum\CreditApplicationStatuses;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\CustomerChangePasswordType;
use App\Form\CustomerUpdateType;
use App\Service\EasyPay;
use App\Service\MobileApi\CreditDocuments;
use App\Service\MobileApi\CustomerCredits;
use App\Service\MobileApi\CustomerPersister;
use App\Service\MobileApi\Customers;
use App\Service\MobileApi\DataTransferObject\CreditApplication;
use App\Service\MobileApi\UploadFile;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use stdClass;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @package App\Controller
 * @Route({"en": "/customer-profile", "ua": "/customer-profile"})
 * @IsGranted("ROLE_CUSTOMER")
 */
class CustomerProfileController extends BaseController
{
    use AddsCustomerAccountApiErrors;

    private $breadcrumbsService;

    private $translator;

    public function __construct(BreadcrumbsService $breadcrumbsService, TranslatorInterface $translator)
    {
        $this->breadcrumbsService = $breadcrumbsService;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="customer_profile.index")
     */
    public function index()
    {
        return $this->redirectToRoute('customer_profile.my_credits');
    }

    /**
     * @Route({"en": "/personal-info", "ua": "/personal-info"}, name="customer_profile.personal_info", methods={"POST", "GET"})
     * @param Request $request
     * @param Customers $customersService
     * @param CustomerPersister $customerPersister
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function personalInfo(Request $request, Customers $customersService, CustomerPersister $customerPersister)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();
        
        $customerClone =clone $customer;
        $form = $this->createForm(CustomerUpdateType::class, $customerClone);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->has('smsCode') && !empty($form->get('smsCode')->getData())) {
                // Verify telephone
                $code = $form->get('smsCode')->getData();
                $activateAccountResponse = $customersService->activateAccount($customer, $code);

                if ($activateAccountResponse->isSuccess()) {
                    $this->addFlash(Flash::SUCCESS, $this->translator->trans('customer_profile.telephone_was_verified', [], 'messages'));

                    $customer->setIsPhoneActive(true);
                    $customerPersister->save($customer);

                    return $this->redirectToRoute('customer_profile.personal_info');
                } else {
                    $form->get('smsCode')->addError(new FormError(
                        $this->translator->trans('customer_profile.telephone_was_not_verified', [], 'messages')
                    ));
                }
            } else {
                // Update personal info
                $updateAccountResponse = $customersService->updateAccount($customerClone);

                if ($updateAccountResponse->isSuccess()) {
                    $this->addFlash(Flash::SUCCESS, $this->translator->trans('customer_profile.personal_info_updated', [], 'messages'));

                    if ($customerClone->isTelephoneChanged()) {
                        $customerClone->setIsPhoneActive(false);
                    }

                    if ($customerClone->isEmailChanged()) {
                        $customerClone->setIsEmailActive(false);
                    }

                    $customerPersister->save($customerClone);

                    return $this->redirectToRoute('customer_profile.personal_info');
                } else {
                    $validationErrors = $updateAccountResponse->getErrors();

                    if (empty($validationErrors)) {
                        $this->addFlash(Flash::ERROR, 'customer_profile.update_personal_info_failed');
                        return $this->redirectToRoute('static_pages.home_page');
                    } else {
                        $this->addApiError($form, $validationErrors, $this->translator);
                    }
                }
            }
        }

        $this->breadcrumbsService->create(new CustomerProfilePersonalInfo());

        return $this->render(
            'customer_profile/personal_info.html.twig',
            [
                'form' => $form->createView(),
                'customer' => $customer,
            ]
        );
    }

    /**
     * @Route({"en": "/my-credits", "ua": "/my-credits"}, name="customer_profile.my_credits", methods={"POST", "GET"})
     * @param CustomerCredits $creditsService
     * @param UploadFile $uploadFileService
     * @param BreadcrumbsService $breadcrumbsService
     * @param CreditDocuments $creditDocumentsService
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function myCredits(
        CustomerCredits $creditsService,
        UploadFile $uploadFileService,
        BreadcrumbsService $breadcrumbsService,
        CreditDocuments $creditDocumentsService
    )
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $breadcrumbsService->create(new CustomerProfileMyCredits());

        $response = $creditsService->getCredits($customer);//dd($response);
        $activeCredits = $response->getActiveCredits();

        $creditApplications = $response->getCreditApplications();

        $localCreditApplications = $this->manager()
            ->getRepository(OnlineApplication::class)
            ->forEmail($customer->getEmail());

        foreach ($creditApplications as $application) {
            // If the application is in a state PENDING_SIGNING_INITIAL_DOCUMENTS, but there are initial documents already downloaded, we need to delete the old ones and download the new ones.
            // This situation happens when the approved credit parameters are different than the ones the customer applied for
            $this->deleteInitialDocsIfNeeded($application);

            $filesResponse = $uploadFileService->fileStatusForCredit($application->getCreditId());
            $application->setApplicationFileStatuses($filesResponse->getApplicationFileStatuses());

            if ($application->hasRejectedFiles()) {
                $application->setStatus(ActiveCreditStatus::CORRECTION_NEEDED);
            }
            // dump($application->getStatus());
        }

        // Include credit applications from our DB that are not yet processed by the ERP
        // and not yet returned from the API
        $creditIds = $response->getCreditIds();

        $localCreditApplications = array_filter($localCreditApplications,
            function (OnlineApplication $application) use ($creditIds) {
                return !in_array($application->getErpId(), $creditIds);
            });

        foreach ($localCreditApplications as $onlineApplication) {
            $creditApplications[] = CreditApplication::fromOnlineApplication($onlineApplication);
        }

        return $this->render(
            'customer_profile/my_credits.html.twig',
            [
                'activeCredits' => $activeCredits,
                'lastCompletedCredit' => $response->getLastCompletedCredit(),
                'creditApplications' => $creditApplications,
                'bonusPoints' => $response->getBonusPoints(),
            ]
        );
    }

    /**
     * @Route({"en": "/change-password", "ua": "/change-password"}, name="customer_profile.change_password", methods={"POST", "GET"})
     * @param Request $request
     * @param Customers $customersService
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function changePassword(Request $request, Customers $customersService)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $passwordsObject = new stdClass();
        $passwordsObject->newPassword = null;
        $passwordsObject->oldPassword = null;

        $form = $this->createForm(CustomerChangePasswordType::class, $passwordsObject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $response = $customersService->changePassword($customer, $passwordsObject->oldPassword, $passwordsObject->newPassword);

	        if ($response->isSuccess()) {
		        $this->addFlash(Flash::SUCCESS, $this->translator->trans('customer_profile.password_was_changed', [], 'messages'));
		        return $this->redirectToRoute('customer_profile.index');
	        } else {
		        $form->get('oldPassword')->addError(new FormError(
			        $this->translator->trans('old_password_does_not_match', [], 'validators')
		        ));
	        }
        }

        $this->breadcrumbsService->create(new CustomerProfileChangePassword());

        return $this->render(
            'customer_profile/change_password.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route({"en": "/messages", "ua": "/messages"}, name="customer_profile.messages", methods={"GET"})
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param BreadcrumbsService $breadcrumbsService
     * @return Response
     */
    public function messages(PaginatorInterface $paginator, Request $request, BreadcrumbsService $breadcrumbsService)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $paginatedMessages = $paginator->paginate(
            $this->manager()->getRepository(Message::class)->messagesForCustomer($customer),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $breadcrumbsService->create(new CustomerProfileMessages());

        return $this->render(
            'customer_profile/messages.twig',
            [
                'messages' => $paginatedMessages,
                'customer' => $customer,
            ]
        );
    }

    /**
     * @Route("/resend-verification-mail", name="customer_profile.resend_verification_mail", methods={"POST"})
     * @param Customers $customersService
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function resendAccountVerificationEmail(Customers $customersService)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        if ($customer->getIsEmailActive() === true) {
            throw new BadRequestHttpException();
        }

        $customersService->resendConfirmationEmail($customer);

        $this->addFlash(Flash::SUCCESS, $this->translator->trans('customer_profile.verification_mail_was_resent', [], 'messages'));

        return new JsonResponse();
    }

    /**
     * @Route("/resend-verification-sms", name="customer_profile.resend_verification_sms", methods={"POST"})
     * @param Customers $customersService
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function resendAccountVerificationSms(Customers $customersService)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        if ($customer->getIsPhoneActive() === true) {
            throw new BadRequestHttpException();
        }

        $customersService->resendVerificationCode($customer);

        $this->addFlash(Flash::SUCCESS, $this->translator->trans('customer_profile.verification_sms_was_resent', [], 'messages'));

        return new JsonResponse();
    }

    /**
     * @Route("/{contract}/calculate-repayment", name="customer_profile.calculate_repayment", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @param string $contract
     * @param CustomerCredits $customerCreditsService
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function creditRepayment(string $contract, CustomerCredits $customerCreditsService)
    {
        $response = $customerCreditsService->calculateRepayment($contract);

        return new JsonResponse(['repaymentSum' => $response->getRepayment()], 200);
    }

    /**
     * @Route("/repayment-url", name="customer_profile.repayment_url", methods={"POST"}, condition="request.isXmlHttpRequest()")
     * @param Request $request
     * @param EasyPay $easyPayService
     * @param ValidatorInterface $validator
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function repaymentUrl(Request $request, EasyPay $easyPayService, ValidatorInterface $validator)
    {
        $contract = $request->request->get('contract');
        $amount = $request->request->get('amount');
        $isEarlyRepayment = $request->request->get('isEarlyRepayment') === 'true';

        $errors = $validator->validate($amount, [new NotBlank(), new GreaterThanOrEqual(1)]);

        if ($errors->count() > 0) {
            $errorsArray = [];

            foreach ($errors as $error) {
                /** @var ConstraintViolation $error */
                $errorsArray[] = $error->getMessage();
            }

            return new JsonResponse(['errors' => $errorsArray], 422);
        }

        $response = $easyPayService->generatePaymentUrl($contract, $amount, $isEarlyRepayment);

        if ($response->isSuccess()) {
            return new JsonResponse(['repaymentUrl' => $response->getUrl()], 200);
        } else {
            return new JsonResponse(['errors' => ['Operation failed.']], 422);
        }
    }

    /**
     * @Route("/{contract}/calculate-refinance", name="customer_profile.calculate_refinance", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @param string $contract
     * @param CustomerCredits $customerCreditsService
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function creditRefinance(string $contract, CustomerCredits $customerCreditsService)
    {
        $response = $customerCreditsService->calculateRefinance($contract);

        return new JsonResponse(['refinanceSum' => $response->getRefinance()], 200);
    }

    /**
     * @Route("/check-email-is-confirmed", name="customer_profile.check_email_is_confirmed", methods={"POST"})
     * @param Customers $customersService
     * @param CustomerPersister $customerPersister
     * @return JsonResponse
     */
    public function checkIsEmailConfirmed(Customers $customersService, CustomerPersister $customerPersister)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $isEmailConfirmed = $customersService->IsEmailConfirmed($customer);

        if ($isEmailConfirmed === true) {
            $customer->setIsEmailActive(true);
            $customerPersister->save($customer);
        }

        return $this->json(['isEmailConfirmed' => $isEmailConfirmed]);
    }

    private function deleteInitialDocsIfNeeded(CreditApplication $application)
    {
        if ($application->getStatus() !== ActiveCreditStatus::PENDING_SIGNING_INITIAL_DOCUMENTS) {
            return;
        }
        
        $existingInitialDocs = $this->manager()
            ->getRepository(CreditDocument::class)
            ->forCreditApplication($application->getCreditId());

        foreach ($existingInitialDocs as $document) {
            $this->manager()->remove($document);
        }

        $this->manager()->flush();
    }
}
