<?php

namespace App\Controller;

use App\Annotation\SiteMap;
use App\Entity\Contact;
use App\Entity\Faq;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\ContactType;
use App\Mail\MailService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class FaqsController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */ 
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->breadcrumbs = $breadcrumbs;
		$this->translator = $translator;
	}

	/**
	 * @Route({"en": "/questions", "ua": "/pytannia", "ru": "/questions"}, name="faqs.index")
     * @SiteMap()
	 * @param Request $request
	 * @param MailService $mailService
	 * @return RedirectResponse|Response
	 * @throws LoaderError
	 * @throws RuntimeError
	 * @throws SyntaxError
	 */
	public function index(Request $request, MailService $mailService): Response
	{
		$faqs = $this->manager()->getRepository(Faq::class)->filtered([], true)->getResult();

		$openQuestion = $request->query->get('question', null);

		$contact = new Contact;

		$form = $this->createForm(ContactType::class, $contact);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$recipient = $this->getSetting(Settings::SEND_CONTACT_MESSAGES_TO);

			$mailService
				->to($recipient)
				->send(new \App\Mail\Contact($contact, $this->translator));

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('Your message was send. We will get back to you soon.', [], 'messages'));

			return $this->redirect($this->getSetting(Settings::CONTACT_APPLICATION_REDIRECT_PATH));
		}

		$this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
		$this->breadcrumbs->addItem($this->translator->trans('public.title.faq', [], 'text'));

		return $this->render('faq/index.html.twig', ['faqs' => $faqs, 'form' => $form->createView(), 'openQuestion' => $openQuestion]);
	}
}
