<?php

namespace App\Controller;

use App\Annotation\NeedsActiveCustomerAccount;
use App\Breadcrumbs\BreadcrumbsService;
use App\Breadcrumbs\CustomerProfileConfirmCredit;
use App\Entity\Customer;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Entity\OnlineApplication\UserPaymentInstrument;
use App\Enum\Flash;
use App\Enum\MethodsToReceiveCredit;
use App\Form\SignCreditDocumentsType;
use App\Service\EasyPay;
use App\Service\MobileApi\CreditDocuments;
use App\Service\MobileApi\CustomerCredits;
use App\Service\MobileApi\KycService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use stdClass;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @package App\Controller
 * @Route("/confirm-credit")
 * @IsGranted("ROLE_CUSTOMER")
 */
class ConfirmCreditController extends BaseController
{
    private $breadcrumbsService;

    private $translator;

    public function __construct(BreadcrumbsService $breadcrumbsService, TranslatorInterface $translator)
    {
        $this->breadcrumbsService = $breadcrumbsService;
        $this->translator = $translator;
    }

    /**
     * @Route("/{erpId}", name="confirm_credit.confirm")
     * @NeedsActiveCustomerAccount()
     * @param string $erpId
     * @param CreditDocuments $creditDocumentsService
     * @param Request $request
     * @param KycService $kycService
     * @param EasyPay $easyPayService
     * @param \App\Service\MobileApi\CustomerCredits $creditsService
     * @return Response
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function confirmCredit(string $erpId, CreditDocuments $creditDocumentsService, Request $request, KycService $kycService, EasyPay $easyPayService, CustomerCredits $creditsService)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $currentApplication = $creditsService->getCredits($customer)->creditApplication($erpId);

        if (null === $currentApplication || $currentApplication->isPendingSigningInitialDocs() === false) {
            throw $this->createNotFoundException();
        }

        $initialDocumentTypes = $creditDocumentsService->getInitialDocumentTypes()->getDocumentTypes();

        $code = new stdClass();
        $code->code = null;
        $form = $this->createForm(SignCreditDocumentsType::class, $code);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $response = $creditDocumentsService->signInitialDocuments($erpId, $code->code, $request->getClientIp());

            if ($response->isSuccess()) {
                $this->addFlash(Flash::SUCCESS, $this->translator->trans('confirm_credit.confirmation_was_successful', [], 'messages'));

                $application = $this->manager()
                    ->getRepository(OnlineApplication::class)
                    ->findOneBy(['erpId' => $erpId]);

                // Check if credit amount is to be received via bank account and the user has not selected already available payment instrument
                if ($application->getCreditDetails()->getHowShouldMoneyBeReceived() === MethodsToReceiveCredit::BANK_ACCOUNT &&
                    $application->getCreditDetails()->getSelectedPaymentInstrument() === UserPaymentInstrument::NEW_CARD
                ) {
                    return $this->redirect($this->prepareForKycIfNeeded($application, $kycService, $easyPayService));
                } else {
                    // Redirect customer to his/her profile page
                    return $this->redirectToRoute('customer_profile.my_credits');
                }
            } else {
                if ($response->isCodeWrong()) {
                    $form->get('code')->addError(
                        new FormError(
                            $this->translator->trans('confirm_credit.invalid_code', [], 'validators')
                        )
                    );
                } elseif ($response->hasCodeExpired()) {
                    $form->get('code')->addError(
                        new FormError(
                            $this->translator->trans('confirm_credit.code_has_expired', [], 'validators')
                        )
                    );
                } else {
                    $this->addFlash(Flash::ERROR, $this->translator->trans('confirm_credit.confirmation_failed', [], 'messages'));
                }
            }
        }

        $this->breadcrumbsService->create(new CustomerProfileConfirmCredit());

        return $this->render('confirm_credit/confirm.html.twig', [
            'initialDocumentTypes' => $initialDocumentTypes,
            'erpId' => $erpId,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/on-approval/{erpId}", name="confirm_credit.confirm_on_approval")
     * @NeedsActiveCustomerAccount()
     * @param string $erpId
     * @param CreditDocuments $creditDocumentsService
     * @param Request $request
     * @param \App\Service\MobileApi\CustomerCredits $creditsService
     * @return Response
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function confirmCreditOnApproval(string $erpId, CreditDocuments $creditDocumentsService, Request $request, CustomerCredits $creditsService)
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $currentApplication = $creditsService->getCredits($customer)->creditApplication($erpId);

        if (null === $currentApplication || $currentApplication->isPendingSigningInitialDocsOnApproval() === false) {
            throw $this->createNotFoundException();
        }

        $initialDocumentTypesOnApproval = $creditDocumentsService->getInitialDocumentTypesOnApproval()->getDocumentTypes();

        $code = new stdClass();
        $code->code = null;
        $form = $this->createForm(SignCreditDocumentsType::class, $code);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $response = $creditDocumentsService->signInitialDocuments($erpId, $code->code, $request->getClientIp());

            if ($response->isSuccess()) {
                $this->addFlash(Flash::SUCCESS, $this->translator->trans('confirm_credit.confirmation_was_successful', [], 'messages'));

                return $this->redirectToRoute('customer_profile.my_credits');
            } else {
                if ($response->isCodeWrong()) {
                    $form->get('code')->addError(
                        new FormError(
                            $this->translator->trans('confirm_credit.invalid_code', [], 'validators')
                        )
                    );
                } elseif ($response->hasCodeExpired()) {
                    $form->get('code')->addError(
                        new FormError(
                            $this->translator->trans('confirm_credit.code_has_expired', [], 'validators')
                        )
                    );
                } else {
                    $this->addFlash(Flash::ERROR, $this->translator->trans('confirm_credit.confirmation_failed', [], 'messages'));
                }
            }
        }

        $this->breadcrumbsService->create(new CustomerProfileConfirmCredit());

        return $this->render('confirm_credit/confirm.html.twig', [
            'initialDocumentTypes' => $initialDocumentTypesOnApproval,
            'erpId' => $erpId,
            'form' => $form->createView(),
        ]);
    }

    private function prepareForKycIfNeeded(OnlineApplication $application, KycService $kycService, EasyPay $easyPayService): string
    {
        if ($application->getKycSessionId() !== null && $application->getKycUrl() !== null) {
            return $this->generateUrl('customer_profile.my_credits');
        }

        // Generate KycSessionId
        $success = $kycService->generateSessionId($application);

        // If KycSessionId is generated successfully generate EasyPay verification URL
        if ($success) {
            // Redirect customer to verification Url
            $kycUrl = $easyPayService->generateKycUrl($application->getKycSessionId());
            $application->setKycUrl($kycUrl);
            $this->manager()->flush();

            return $this->generateUrl('verification.initiate', ['hash' => $application->getHash()]);
        } else {
            return $this->generateUrl('customer_profile.my_credits');
        }
    }
}
