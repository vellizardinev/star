<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\MessageReadByRecipient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package App\Controller
 * @Route({"en": "/customer-message", "ua": "/customer-message", "ru": "/customer-message"})
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_CUSTOMER")
 */
class CustomerMessageController extends BaseController
{
    /**
     * @Route({"en": "/{id}/mark-as-read", "ua": "/{id}/mark-as-read", "ru": "/{id}/mark-as-read"}, name="customer_message.mark_as_read", methods={"POST"})
     * @return Response
     */
    public function markAsRead(Message $message)
    {
        /** @var \App\Entity\Customer $customer */
        $customer = $this->getUser();

        $messageReadBy = (new MessageReadByRecipient())
            ->setEmail($customer->getEmail())
            ->setMessage($message)
        ;

        $this->manager()->persist($messageReadBy);
        $this->manager()->flush();

        return new JsonResponse();
    }
}
