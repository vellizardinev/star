<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Message;
use App\Entity\MessageRecipient;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\MessageType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/messages")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_MESSAGES_OPERATOR")
 */
class MessagesController extends BaseController
{
    private $breadcrumbs;

    private $translator;

    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.messages.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
	    $paginatedMessages = $paginator->paginate(
		    $this->manager()->getRepository(Message::class)->filtered($request->query->all()),
		    $request->query->getInt('page', 1),
		    $this->getSetting(Settings::PAGINATION_PER_PAGE)
	    );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.messages', [], 'text'));

        return $this->render(
            'admin/messages/index.html.twig',
            [
                'messages' => $paginatedMessages
            ]
        );
    }

    /**
     * @Route("/create", name="admin.messages.create", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $message = new Message();

        $form = $this->createForm(MessageType::class, $message);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($message);

        	$message->setCreatedBy($this->getUser());

            $this->updateRecipients($message, $form->get('recipientEmails')->getData());

            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('Message was created.', [], 'messages'));

            return $this->redirectToRoute('admin.messages.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.messages', [], 'text'), $this->generateUrl('admin.messages.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_message', [], 'text'));

        return $this->render(
            'admin/messages/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Message $message
     * @param Request $request
     * @return Response
     * @Route("/{id}/update", name="admin.messages.update", methods={"GET", "POST"})
     */
    public function update(Message $message, Request $request)
    {
	    $form = $this->createForm(MessageType::class, $message);

	    $form->handleRequest($request);

	    if ($form->isSubmitted() && $form->isValid()) {
	    	$message->setUpdatedBy($this->getUser());

	    	$this->updateRecipients($message, $form->get('recipientEmails')->getData());

		    $this->manager()->flush();

		    $this->addFlash(Flash::SUCCESS, $this->translator->trans('The message\'s content was updated.', [], 'messages'));

		    return $this->redirectToRoute('admin.messages.index');
	    }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.messages', [], 'text'), $this->generateUrl('admin.messages.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_message', [], 'text'));

        return $this->render(
            'admin/messages/update.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

	/**
	 * @param Message $message
	 * @return JsonResponse
	 * @Route("/{id}/delete", name="admin.messages.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
	 */
	public function delete(Message $message)
	{
		$this->manager()->remove($message);
		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The message was deleted.', [], 'messages'));

		return $this->json(null, 200);
	}

	public function updateRecipients(Message $message, array $emails)
    {
        $message->clearRecipients();

        $this->manager()->flush();

        foreach ($emails as $email) {
            $recipient = (new MessageRecipient)
                ->setEmail($email);

            $message->addRecipient($recipient);

            $this->manager()->persist($recipient);
        }

        $this->manager()->flush();
    }
}
