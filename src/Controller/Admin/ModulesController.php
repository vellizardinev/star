<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Module;
use App\Entity\UserDefinedModule;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\UserDefinedModuleType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class CustomPagesController
 * @package App\Controller\Admin
 * @Route("/admin/modules")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_MODULES")
 */
class ModulesController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CustomPagesController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.modules.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedModules = $paginator->paginate(
            $this->manager()->getRepository(Module::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.modules', [], 'text'));

        return $this->render(
            'admin/modules/index.html.twig',
            [
                'modules' => $paginatedModules,
            ]
        );
    }

    /**
     * @Route("/create", name="admin.modules.create", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $module = new UserDefinedModule;

        $form = $this->createForm(UserDefinedModuleType::class, $module);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($module);
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The module was created.', [], 'messages'));

            return $this->redirectToRoute('admin.modules.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.modules', [], 'text'), $this->generateUrl('admin.modules.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_module', [], 'text'));

        return $this->render(
            'admin/modules/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param UserDefinedModule $userDefinedModule
     * @param Request $request
     * @return Response
     * @Route("/{id}/update", name="admin.modules.update", methods={"GET", "POST"})
     */
    public function update(UserDefinedModule $userDefinedModule, Request $request)
    {
        $form = $this->createForm(UserDefinedModuleType::class, $userDefinedModule);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The module was updated.', [], 'messages'));

	        return $this->redirectBack($this->generateUrl('admin.modules.index'));

        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.modules', [], 'text'), $this->generateUrl('admin.modules.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_module', [], 'text'));

        return $this->render(
            'admin/modules/update.html.twig',
            [
                'module' => $userDefinedModule,
                'form' => $form->createView(),
            ]
        );
    }
}
