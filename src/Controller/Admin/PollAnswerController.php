<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\PollAnswer;
use App\Entity\PollQuestion;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @package App\Controller\Admin
 * @Route("/admin/answers")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_POLLS")
 */
class PollAnswerController extends BaseController
{
    /**
     * @Route("/{id}", name="admin.answer.get", requirements={"id"="\d+"}, methods={"GET"})
     * @param PollAnswer $answer
     * @return Response
     */
    public function getAnswer(PollAnswer $answer)
    {
        return $this->json($answer, 200, [], ['groups' => ['main']]);
    }

    /**
     * @Route("/new", name="admin.answer.create", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function create(Request $request, ValidatorInterface $validator, TranslatorInterface $translator)
    {
        $questionId = $request->request->get('questionId', null);
        $question = $this->manager()->getRepository(PollQuestion::class)->find($questionId);

        if (!$question) {
            return $this->json($translator->trans('not_found', [], 'messages'), 404);
        }

        $answer = (new PollAnswer)
            ->setQuestion($question)
            ->setText($request->request->get('text'))
            ->setHasComment($request->request->get('hasComment') === 'true' ? true : false)
            ->setCommentHelpText($request->request->get('commentHelpText'));

        $errors = $validator->validate($answer);

        if ($errors->count() > 0) {
            return $this->json($errors[0]->getMessage(), 422);
        }

        $this->manager()->persist($answer);
        $this->manager()->flush();

        $message = $translator->trans('poll_answer_was_created', [], 'messages');

        return $this->json([
            'message' => $message,
            'payload' => ['id' => $answer->getId(), 'text' => $answer->getText(), 'hasComment' => $answer->getHasComment(), 'commentHelpText' => $answer->getCommentHelpText()],
        ], 201, ['groups' => ['main']]);
    }

    /**
     * @Route("/{id}/update", name="admin.answer.update", methods={"POST"})
     * @param PollAnswer $answer
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function update(PollAnswer $answer, Request $request, ValidatorInterface $validator, TranslatorInterface $translator)
    {
        $answer
            ->setText($request->request->get('text'))
            ->setHasComment($request->request->get('hasComment') === 'true' ? true : false)
            ->setCommentHelpText($request->request->get('commentHelpText'));

        $errors = $validator->validate($answer);

        if ($errors->count() > 0) {
            return $this->json($errors[0]->getMessage(), 422);
        }

        $this->manager()->flush();

        return $this->json($translator->trans('poll_answer_was_updated', [], 'messages'), 200);
    }

    /**
     * @Route("/{id}/delete", name="admin.answer.delete", methods={"POST"})
     * @param PollAnswer $answer
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function delete(PollAnswer $answer, TranslatorInterface $translator)
    {
        $this->manager()->remove($answer);

        $this->manager()->flush();

        return $this->json($translator->trans('poll_answer_was_deleted', [], 'messages'), 200);
    }
}
