<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Image;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ImageController
 * @package App\Controller\Admin
 * @Route("/admin/image")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_USER")
 */
class ImageController extends BaseController
{
    /**
     * @Route("", name="image.index", condition="request.isXmlHttpRequest()")
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function index(SerializerInterface $serializer)
    {
        $images = $this->manager()->getRepository(Image::class)->getGalleryImages();

        return new JsonResponse($serializer->serialize($images, 'json'), 200);
    }
}
