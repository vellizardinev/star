<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\CreditShortApplication;
use App\Enum\CreditApplicationSources;
use App\Enum\CreditApplicationStatuses;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\CreditApplicationDetailsType;
use App\Form\CreditDetailsType;
use App\Message\SendShortApplicationToApi;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use ReflectionException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/credit-applications")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_TELEPHONE_APPLICATIONS")
 */
class CreditApplicationsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.credit_applications.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     * @throws ReflectionException
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedCreditApplications = $paginator->paginate(
            $this->manager()->getRepository(CreditShortApplication::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem(
            $this->translator->trans('title.administration', [], 'text'),
            $this->generateUrl('admin.dashboard')
        );
        $this->breadcrumbs->addItem($this->translator->trans('title.credit_applications', [], 'text'));

        $sources = array_flip(CreditApplicationSources::toTranslationKeys());

        return $this->render(
            'admin/credit_applications/index.html.twig',
            [
                'creditApplications' => $paginatedCreditApplications,
                'sources' => $sources,
                'count' => $paginatedCreditApplications->getTotalItemCount(),
            ]
        );
    }

    /**
     * @param CreditShortApplication $creditApplication
     * @param Request $request
     * @return Response
     * @throws ReflectionException
     * @Route("/{id}/details", name="admin.credit_applications.details", methods={"GET", "POST"})
     */
    public function details(CreditShortApplication $creditApplication, Request $request)
    {
        $this->breadcrumbs->addItem(
            $this->translator->trans('title.administration', [], 'text'),
            $this->generateUrl('admin.dashboard')
        );
        $this->breadcrumbs->addItem(
            $this->translator->trans('title.credit_applications', [], 'text'),
            $this->generateUrl('admin.credit_applications.index')
        );
        $this->breadcrumbs->addItem($this->translator->trans('title.credit_applications.details', [], 'text'));

        $creditApplicationDetailsForm = $this->createForm(CreditApplicationDetailsType::class, $creditApplication);
        $creditForm = $this->createForm(CreditDetailsType::class, $creditApplication->getCredit());

        $creditApplicationDetailsForm->handleRequest($request);

        if ($creditApplicationDetailsForm->isSubmitted() && $creditApplicationDetailsForm->isValid()) {
            $this->manager()->flush();

            $this->addFlash(Flash::SUCCESS, $this->translator->trans('Credit application status was changed.', [], 'messages'));

            return $this->redirectToRoute('admin.credit_applications.details', ['id' => $creditApplication->getId()]);
        }

        return $this->render(
            'admin/credit_applications/details.html.twig',
            [
                'creditApplicationForm' => $creditApplicationDetailsForm->createView(),
                'creditForm' => $creditForm->createView(),
                'creditApplicationChangelog' => $creditApplication->getCreditApplicationChangelog(),
                'creditApplication' => $creditApplication,
                'statuses' => CreditApplicationStatuses::toTranslationKeys(),
            ]
        );
    }

    /**
     * @param Request $request
     * @param CreditShortApplication $creditApplication
     * @return Response
     * @throws ReflectionException
     * @Route("/{id}/status", name="admin.credit_applications.status", methods={"POST"})
     */
    public function status(Request $request, CreditShortApplication $creditApplication)
    {
        $creditApplication->setStatus($request->request->get('status', CreditApplicationStatuses::UNPROCESSED));

        $this->manager()->flush();

        $this->addFlash(
            Flash::SUCCESS,
            $this->translator->trans('Credit application status was changed.', [], 'messages')
        );

        return $this->redirectToRoute('admin.credit_applications.details', ['id' => $creditApplication->getId()]);
    }

    /**
     * @param Request $request
     * @Route("/export-xls", name="admin.credit_applications.export_xls", methods={"GET"})
     * @return Response
     * @throws Exception
     */
    public function exportXLS(Request $request)
    {
        $creditApplications = $this->manager()->getRepository(CreditShortApplication::class)->getExportData(
            $request->query->all()
        );

        $rows = array();

        $rows[] = ['Name', 'Telephone', 'Amount', 'Source', 'Status', 'Application date', 'Editor'];

        $results = array_map(
            function ($creditApplication) {
                return [
                    $creditApplication['name'],
                    $creditApplication['telephone'],
                    $creditApplication['amount'],
                    $this->getCreditApplicationSource($creditApplication),
                    $creditApplication['status'],
                    $creditApplication['createdAt'],
                    $creditApplication['editor'],
                ];
            },
            $creditApplications
        );
        $rows = array_merge($rows, $results);

        $dateTime = new \DateTime();

        $fileName = 'credit_applications_list_' . $dateTime->format('U') . '.xls';

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()
            ->setCreator('iCredit Ukraine')
            ->setLastModifiedBy('iCredit Ukraine')
            ->setTitle('Credit applications')
            ->setSubject('Credit applications')
            ->setDescription('List of credit applications');

        $sheet = $spreadsheet->getActiveSheet();

        $columnIndexes = range('A', 'G');

        $sheet
            ->setTitle('Credit applications');

        foreach ($columnIndexes as $column) {
            $sheet->getColumnDimension($column)->setAutoSize(true);
        }

        foreach ($rows as $rowIndex => $row) {
            foreach ($row as $columnIndex => $rowElement) {
                $cellCoordinates = $columnIndexes[$columnIndex] . ($rowIndex + 1);
                $sheet->setCellValue($cellCoordinates, $rowElement);

                if (is_numeric($rowElement)) {
                    if (strpos($rowElement, '380') === 0 && strpos($rowElement, '.') === false) {
                        $sheet
                            ->getStyle($cellCoordinates)
                            ->getNumberFormat()
                            ->setFormatCode(NumberFormat::FORMAT_NUMBER);
                    } else {
                        $sheet
                            ->getStyle($cellCoordinates)
                            ->getNumberFormat()
                            ->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
                    }
                }
            }
        }

        if (($request->query->get('start_date')) || $request->query->get('end_date') ||
            $request->query->get('name') || $request->query->get('telephone') ||
            ($request->query->get('source') && $request->query->get('source') !== 'any')
        ) {
            $fileName = 'filtered_' . $fileName;
        }

        $writer = new Xls($spreadsheet);
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName)->deleteFileAfterSend(true);
    }

    /**
     * @Route("/resend/{id}", name="admin.credit_applications.resend", methods={"POST"})
     * @param CreditShortApplication $application
     * @param MessageBusInterface $messageBus
     * @return Response
     */
    public function resend(CreditShortApplication $application, MessageBusInterface $messageBus)
    {
        if ($application->getSentToErp() === true) {
            throw new BadRequestHttpException();
        }

        $messageBus->dispatch(new SendShortApplicationToApi($application->getId()));

        $this->addFlash(Flash::SUCCESS, 'The online application has been resent.');

        return new JsonResponse();
    }

    /**
     * @param CreditShortApplication $application
     * @return BinaryFileResponse
     * @Route("/{id}/download-request-data", name="admin.credit_applications.download_request_data", methods={"POST"})
     */
    public function downloadRequestData(CreditShortApplication $application)
    {
        $data = $application->getJsonData();

        $file = tempnam(sys_get_temp_dir(), 'request_data_');
        file_put_contents($file, $data);

        return $this->file($file)->deleteFileAfterSend();
    }

    private function getCreditApplicationSource(array $creditApplication)
    {
        if ($affiliate = $creditApplication['affiliate']) {
            $source = $this->translator->trans('creditapplicationsources.affiliate', [], 'form')
                . ': ' . $affiliate;

            return $source;
        }
        if ($landing = $creditApplication['landing']) {
            $source = $this->translator->trans('creditapplicationsources.landing', [], 'form')
                . ': ' . $landing;

            return $source;
        }

        return $this->translator->trans('creditapplicationsources.internal', [], 'form');
    }
}
