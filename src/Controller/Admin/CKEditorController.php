<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Image;
use App\Enum\FileTypes;
use App\Enum\Settings;
use App\File\FileManager;
use App\File\FilesValidation;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CKEditorController
 * @package App\Controller\Admin
 * @Route("/admin/ckeditor")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_USER")
 */
class CKEditorController extends BaseController
{
    /**
     * @Route("/browse-images", name="admin.ckeditor.browse_images")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function ckeditorImageBrowse(Request $request, PaginatorInterface $paginator)
    {
        $paginatedImages = $paginator->paginate(
            $this->manager()->getRepository(Image::class)->filtered(),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $CKEditorFuncNum = $request->get('CKEditorFuncNum');

        return $this->render(
            'admin/ckeditor/browse_images.html.twig',
            [
                'CKEditorFuncNum' => $CKEditorFuncNum,
                'images' => $paginatedImages,
            ]
        );
    }

    /**
     * @Route("/upload", name="admin.ckeditor.upload")
     * @param Request $request
     * @param FileManager $fileManager
     * @param ValidatorInterface $validator
     * @param FilesValidation $rules
     * @return JsonResponse
     * @throws Exception
     */
    public function ckeditorUpload(Request $request, FileManager $fileManager, ValidatorInterface $validator, FilesValidation $rules)
    {
        $uploadedFile = $request->files->get('upload');

        $errors = $validator->validate($uploadedFile, $rules->get(FileTypes::IMAGE));

        if ($errors->count() > 0) {
            $errorsString = '';

            foreach ($errors as $error) {
                $errorsString .= $error->getMessage() . "\n";
            }

            $response = [
                'uploaded' => 0,
                'error' => [
                    'message' => $errorsString,
                ],
            ];

            return new JsonResponse($response, 422);
        }

        $image = $fileManager->uploadImage($uploadedFile);
        $this->manager()->persist($image);
        $this->manager()->flush();

        $response = [
            'url' => $fileManager->publicImagePath($image),
        ];

        return new JsonResponse($response, 201);
    }
}
