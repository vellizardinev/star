<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\FailedOnlineApplicationRequest;
use App\Entity\Nomenclature;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Message\SendOnlineApplicationToApi;
use DateTime;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use ReflectionException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/online-applications")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_ONLINE_APPLICATIONS")
 */
class OnlineCreditApplicationsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.online_credit_applications.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     * @throws ReflectionException
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedCreditApplications = $paginator->paginate(
            $this->manager()->getRepository(OnlineApplication::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        foreach ($paginatedCreditApplications as $application) {
            /** @var $application OnlineApplication */

            $array = $application->toArray();
            $data = [];

            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    if (array_key_exists('name', $value)) {
                        $translatedKey = $this->translator->trans($key, [], 'long_application');
                        $data[$translatedKey] = $value['name'];
                        continue;
                    }

                    continue;
                }

                if (strpos($key, 'Uploaded') !== false) {
                    continue;
                }

                $translatedKey = $this->translator->trans($key, [], 'long_application');

                if (is_bool($value)) {
                    $data[$translatedKey] = $this->normalizeBoolean($value);
                } else {
                    $data[$translatedKey] = $value;
                }
            }

            ksort($data);

            $application->arrayData = $data;
        }

        $this->breadcrumbs->addItem(
            $this->translator->trans('title.administration', [], 'text'),
            $this->generateUrl('admin.dashboard')
        );
        $this->breadcrumbs->addItem($this->translator->trans('title.online_credit_applications', [], 'text'));

        return $this->render(
            'admin/online_credit_applications/index.html.twig',
            [
                'creditApplications' => $paginatedCreditApplications,
                'count' => $paginatedCreditApplications->getTotalItemCount(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="admin.online_credit_applications.show", methods={"GET"})
     * @param OnlineApplication $application
     * @return Response
     * @throws ReflectionException
     */
	public function show(OnlineApplication $application)
	{
        $array = $application->toArray();

		$data = [];

		foreach ($array as $key => $value) {
			if (is_array($value)) {
				if (array_key_exists('name', $value)) {
					$translatedKey = $this->translator->trans($key, [], 'long_application');
					$data[$translatedKey] = $value['name'];
					continue;
				}

				continue;
			}

			if (strpos($key, 'Uploaded') !== false) {
				continue;
			}

			$translatedKey = $this->translator->trans($key, [], 'long_application');

			if (is_bool($value)) {
                $data[$translatedKey] = $this->normalizeBoolean($value);
            } else if ($this->isGuid($value)) {
                $nomenclatureObject = $this->manager()
                    ->getRepository(Nomenclature::class)
                    ->findOneBy(['guid' => $value]);

                if (null !== $nomenclatureObject) {
                    /** @var $nomenclatureObject Nomenclature */
                    $data[$translatedKey] = $nomenclatureObject->getText();
                }
			} else {
				$data[$translatedKey] = $value;
			}
		}

		ksort($data);

		$application->arrayData = $data;

		$httpRequest = $this->manager()->getRepository(FailedOnlineApplicationRequest::class)
            ->findOneBy(['application' => $application]);

		$this->breadcrumbs->addItem(
			$this->translator->trans('title.administration', [], 'text'),
			$this->generateUrl('admin.dashboard')
		);

		$this->breadcrumbs->addItem(
			$this->translator->trans('title.online_credit_applications', [], 'text'),
			$this->generateUrl('admin.online_credit_applications.index')
		);

		$this->breadcrumbs->addItem(
			$this->translator->trans('title.online_credit_applications.details', [], 'text')
		);

		return $this->render(
			'admin/online_credit_applications/show.html.twig',
			[
				'application' => $application,
                'httpRequest' => $httpRequest,
			]
		);
	}

	/**
	 * @Route("/resend/{id}", name="admin.online_credit_applications.resend", methods={"POST"})
	 * @param OnlineApplication $application
	 * @param MessageBusInterface $messageBus
	 * @return Response
	 */
	public function resend(OnlineApplication $application, MessageBusInterface $messageBus)
	{
		if ($application->getIsSentToErp() === true) {
			throw new BadRequestHttpException();
		}

		$messageBus->dispatch(new SendOnlineApplicationToApi($application->getId()));

		$this->addFlash(Flash::SUCCESS, 'The online application has been resent.');

		return new JsonResponse();
	}

    /**
     * @param OnlineApplication $application
     * @Route("/{hash}/download-request-data", name="admin.online_credit_applications.download_request_data", methods={"POST"})
     * @return BinaryFileResponse
     */
	public function downloadRequestData(OnlineApplication $application)
    {
        $data = $application->getJsonData();

        $file = tempnam(sys_get_temp_dir(), 'request_data_');
        file_put_contents($file, $data);

        return $this->file($file)->deleteFileAfterSend();
    }

    /**
     * @param SerializerInterface $serializer
     * @param Request $request
     * @Route("/export/csv", name="admin.online_credit_applications.export_csv", methods={"GET"})
     * @return Response
     * @throws Exception
     */
    public function export(Request $request, SerializerInterface $serializer)
    {
        $applications = $this
            ->manager()
            ->getRepository(OnlineApplication::class)
            ->getExportData(
            $request->query->all()
        );

        $rows = array();

        $rows[] = ['Email', 'Date'];

        $results = array_map(
            function ($application) {
                /** @var $application OnlineApplication */
                return [
                    $application->getPersonalInfo()->getEmail(),
                    $application->getCreatedAt()->format('Y-m-d H:i:s'),
                ];
            },
            $applications
        );
        $rows = array_merge($rows, $results);

        $dateTime = new DateTime();

        $fileName = 'online_applications_list_' . $dateTime->format('U') . '.csv';

        $response = new Response($serializer->encode($rows, 'csv', [CsvEncoder::NO_HEADERS_KEY => true,]));
        $response->headers->set('Content-Type', 'text/csv');
        $disposition = HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_ATTACHMENT, $fileName);
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    private function normalizeBoolean(bool $value): string
    {
        if ($value) {
            return $this->translator->trans('value.true', [], 'text');
        } else {
            return $this->translator->trans('value.false', [], 'text');
        }
    }

    private function isGuid($value): bool
    {
        if (is_string($value)) {
            return preg_match('/^\{?[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}\}?$/', $value);
        }

        return false;
    }
}
