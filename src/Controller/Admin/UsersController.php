<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Profile;
use App\Entity\User;
use App\Enum\Flash;
use App\Enum\Roles;
use App\Enum\Settings;
use App\Form\ProfileType;
use App\Form\UserPasswordType;
use App\Form\UserPermissionGroupsType;
use App\Form\UserType;
use Knp\Component\Pager\PaginatorInterface;
use ReflectionException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class UsersController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/users")
 */
class UsersController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * UsersController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.users.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     * @throws ReflectionException
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedUsers = $paginator->paginate(
            $this->manager()->getRepository(User::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbsForIndex();

        return $this->render(
            'admin/users/index.html.twig',
            [
                'users' => $paginatedUsers,
                'roles' => Roles::toTranslationKeys(),
            ]
        );
    }

    /**
     * @Route("/profile/{id}", name="admin.users.profile", methods={"GET", "POST"})
     * @param User $user
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function profile(User $user, Request $request)
    {
        $profile = $user->getProfile();

        if (!$profile) {
            $profile = (new Profile())->setUser($user);
            $user->setProfile($profile);
        }

        $form = $this->createForm(ProfileType::class, $profile);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($profile);
            $this->manager()->persist($user);
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('User profile was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.users.index');
        }

        $this->breadcrumbsForProfile();

        return $this->render(
            'admin/users/profile.html.twig',
            [
                'form' => $form->createView(),
                'user' => $user,
            ]
        );
    }

    /**
     * @Route("/{id}/toggle-status", name="admin.users.toggle_status", methods={"POST"})
     * @param User $user
     * @return RedirectResponse
     */
    public function toggleActive(User $user)
    {
        $user->setActive(!$user->getActive());

        $this->manager()->persist($user);
        $this->manager()->flush();

	    $this->addFlash(Flash::SUCCESS, $this->translator->trans('User status was updated.', [], 'messages'));

        return $this->redirectToRoute('admin.users.index');
    }

    /**
     * @Route("/{id}/manage-roles", name="admin.users.manage_roles", methods={"GET", "POST"})
     * @param User $user
     * @param Request $request
     * @return Response
     * @throws ReflectionException
     */
    public function roles(User $user, Request $request)
    {
        if ($request->getMethod() === 'POST') {
            $roles = $request->request->get('roles', [Roles::USER]);
            $roles[] = Roles::USER;
            $roles = array_unique($roles);

            $user->setRoles($roles);
            $this->manager()->persist($user);
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('User roles were updated.', [], 'messages'));

            return $this->redirectToRoute('admin.users.index');
        }

        $this->breadcrumbsForRoles();

        return $this->render(
            'admin/users/manage_roles.html.twig',
            [
                'user' => $user,
                'roles' => Roles::toTranslationKeys(),
            ]
        );
    }

    /**
     * @Route("/{id}/manage-groups", name="admin.users.manage_groups", methods={"GET", "POST"})
     * @param User $user
     * @param Request $request
     * @return Response
     */
    public function groups(User $user, Request $request)
    {
        $form = $this->createForm(UserPermissionGroupsType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('User permission groups were updated.', [], 'messages'));

            return $this->redirectToRoute('admin.users.index');
        }

        $this->breadcrumbsForGroups();

        return $this->render(
            'admin/users/manage_permission_groups.html.twig',
            [
                'user' => $user,
                'form' => $form->createView(),
            ]
        );
    }

	/**
	 * @Route("/create", name="admin.users.create", methods={"GET", "POST"})
	 * @param Request $request
	 * @param UserPasswordEncoderInterface $passwordEncoder
	 * @return Response
	 */
	public function createUser(Request $request, UserPasswordEncoderInterface $passwordEncoder)
	{
		$user = new User();

		$form = $this->createForm(UserType::class, $user);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->manager()->persist($user);

			$user->setPassword(
				$passwordEncoder->encodePassword(
					$user,
					$form->get('password')->get('plain_password')->getData()
				)
			);

			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('User was created.', [], 'messages'));

			return $this->redirectToRoute('admin.users.index');
		}

		$this->breadcrumbsForCreateUser();

		return $this->render(
			'admin/users/create.html.twig',
			[
				'form' => $form->createView(),
			]
		);
	}

	/**
	 * @Route("/change-password/{id}", name="admin.users.change_password", methods={"GET", "POST"})
	 * @param User $user
	 * @param Request $request
	 * @param UserPasswordEncoderInterface $passwordEncoder
	 * @return RedirectResponse|Response
	 * @IsGranted("ROLE_ADMIN")
	 */
	public function changePassword(User $user, Request $request, UserPasswordEncoderInterface $passwordEncoder)
	{
		$form = $this->createForm(UserPasswordType::class, $user);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$user->setPassword(
				$passwordEncoder->encodePassword(
					$user,
					$form->get('plain_password')->getData()
				)
			);

			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('User password was updated.', [], 'messages'));

			return $this->redirectToRoute('admin.users.index');
		}

		$this->breadcrumbsForChangePassword();

		return $this->render(
			'admin/users/change_password.html.twig',
			[
				'form' => $form->createView(),
				'user' => $user,
			]
		);
	}

    /**
     * @param User $user
     * @return JsonResponse
     * @Route("/{id}/delete", name="admin.users.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
     */
    public function delete(User $user)
    {
        $currentUser = $this->getUser();

        if ($currentUser === $user) {
            $this->addFlash(Flash::ERROR, $this->translator->trans('You are not allowed to delete your own account.', [], 'messages'));

            return $this->json(null, 400);
        }

        $this->manager()->remove($user);
        $this->manager()->flush();

        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The user was deleted.', [], 'messages'));

        return $this->json(null, 200);
    }


    private function breadcrumbsForIndex()
    {
        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.users', [], 'text'), $this->generateUrl('admin.users.index'));
    }

    private function breadcrumbsForRoles()
    {
        $this->breadcrumbsForIndex();
        $this->breadcrumbs->addItem($this->translator->trans('title.manage_roles', [], 'text'));
    }

    private function breadcrumbsForProfile()
    {
        $this->breadcrumbsForIndex();
        $this->breadcrumbs->addItem($this->translator->trans('title.user_profile', [], 'text'));
    }

    private function breadcrumbsForGroups()
    {
        $this->breadcrumbsForIndex();
        $this->breadcrumbs->addItem($this->translator->trans('title.manage_user_permission_groups', [], 'text'));
    }

	private function breadcrumbsForCreateUser()
	{
		$this->breadcrumbsForIndex();
		$this->breadcrumbs->addItem($this->translator->trans('title.create_user', [], 'text'));
	}

	private function breadcrumbsForChangePassword()
	{
		$this->breadcrumbsForIndex();
		$this->breadcrumbs->addItem($this->translator->trans('title.change_password', [], 'text'));
	}
}
