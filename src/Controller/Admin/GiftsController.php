<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Gift;
use App\Enum\FileTypes;
use App\Enum\Flash;
use App\File\FileManager;
use App\File\FilesValidation;
use App\Form\GiftType;
use App\Service\EntityTranslator;
use Exception;
use League\Flysystem\FileExistsException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/gifts")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_GIFTS")
 */
class GiftsController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->breadcrumbs = $breadcrumbs;
		$this->translator = $translator;
	}

	/**
	 * @Route("", name="admin.gifts.index")
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$gifts = $this->manager()->getRepository(Gift::class)->filtered($request->query->all())->getResult();

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.gifts', [], 'text'));

		return $this->render(
			'admin/gifts/index.html.twig',
			[
				'gifts' => $gifts,
                'filtered' => count($request->query->all()) > 0,
			]
		);
	}

	/**
	 * @Route("/create", name="admin.gifts.create", methods={"GET", "POST"})
	 * @param Request $request
	 * @param  EntityTranslator $entityTranslator
	 * @return Response
	 */
	public function create(Request $request, EntityTranslator $entityTranslator)
	{
		$gift = new Gift();

		$form = $this->createForm(GiftType::class, $gift);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->manager()->persist($gift);
			$this->manager()->flush();

			$entityTranslator->translateGift($gift);

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('Gift was created.', [], 'messages'));

			return $this->redirectToRoute('admin.gifts.index');
		}

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.gifts', [], 'text'), $this->generateUrl('admin.gifts.index'));
		$this->breadcrumbs->addItem($this->translator->trans('title.create_gift', [], 'text'));

		return $this->render(
			'admin/gifts/create.html.twig',
			[
				'form' => $form->createView(),
			]
		);
	}

	/**
	 * @param Gift $gift
	 * @param Request $request
	 * @param FormFactoryInterface $formFactory
	 * @param string $locales
	 * @return Response
	 * @Route("/{id}/update", name="admin.gifts.update", methods={"GET", "POST"})
	 */
	public function update(Gift $gift, Request $request, FormFactoryInterface $formFactory, string $locales)
	{
		$localesArray = explode('|', $locales);

		$contentForms = null;

		foreach ($localesArray as $locale)
		{
			$gift->setTranslatableLocale($locale);
			$this->manager()->refresh($gift);

			$localeForm = $formFactory->createNamed($locale, GiftType::class, $gift);

			if ($result = $this->handleContentForm($localeForm, $request, $gift, $locale)) return $result;

			$contentForms[$locale] = $localeForm->createView();
		}

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.gifts', [], 'text'), $this->generateUrl('admin.gifts.index'));
		$this->breadcrumbs->addItem($this->translator->trans('title.update_gift', [], 'text'));

		return $this->render(
			'admin/gifts/update.html.twig',
			[
				'contentForms' => $contentForms,
			]
		);
	}

	/**
	 * @param Gift $gift
	 * @return JsonResponse
	 * @Route("/{id}/delete", name="admin.gifts.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
	 */
	public function delete(Gift $gift)
	{
		$this->manager()->remove($gift);
		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The gift was deleted.', [], 'messages'));

		return $this->json(null, 200);
	}

	/**
	 * @param Request $request
	 * @return Response
	 * @Route("/order", name="admin.gifts.order", methods={"POST"})
	 */
	public function order(Request $request)
	{
		$sortedGifts = $request->request->get('sortedGifts', []);

		$unsortedGifts = $this->manager()->getRepository(Gift::class)->findAll();

		if (empty($sortedGifts)) {
			$this->addFlash(Flash::ERROR, $this->translator->trans('Please, add some gifts first.', [], 'messages'));

			return $this->redirectToRoute('admin.gifts.index');
		}

		foreach ($unsortedGifts as $unsortedGift) {
			$index = array_search($unsortedGift->getId(), $sortedGifts);

			$unsortedGift->setPositionIndex($index);
		}

		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The order of gifts was updated.', [], 'messages'));

		return $this->redirectToRoute('admin.gifts.index');
	}

	/**
	 * @param Gift $gift
	 * @param Request $request
	 * @param FileManager $fileManager
	 * @param ValidatorInterface $validator
	 * @param FilesValidation $rules
	 * @return JsonResponse
	 * @throws FileExistsException
	 * @throws Exception
	 * @Route("/{id}/upload-cover-image", name="admin.gifts.upload_cover_image", methods={"POST"}, condition="request.isXmlHttpRequest()")
	 */
	public function uploadCoverImage(Gift $gift, Request $request, FileManager $fileManager, ValidatorInterface $validator, FilesValidation $rules)
	{
		$this->deleteImageIfExists($gift, $fileManager);

		$uploadedFile = $request->files->get('file');

		$errors = $validator->validate($uploadedFile, $rules->get(FileTypes::IMAGE));

		if ($errors->count() > 0) {
			return $this->json($errors, 422);
		}

		$image = $fileManager->uploadImage($uploadedFile);
		$gift->setImage($image);

		$this->manager()->persist($image);

		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The cover image was updated.', [], 'messages'));

		return $this->json(null, 201);
	}

	/**
	 * @param Gift $gift
	 * @param FileManager $fileManager
	 * @throws Exception
	 */
	private function deleteImageIfExists(Gift $gift, FileManager $fileManager)
	{
		if (!$gift->getImage()) {
			return;
		}

		$image = $gift->getImage();

		try {
			$fileManager->delete($image);
			$gift->setImage(null);
			$this->manager()->remove($image);
			$this->manager()->flush();
		} catch (Exception $e) {
			throw $e;
		}
	}

	private function handleContentForm(FormInterface $form, Request $request, Gift $gift, string $locale)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$gift->setTranslatableLocale($locale);

			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('The gift content was updated.', [], 'messages'));

			return $this->redirectToRoute('admin.gifts.update', ['id' => $form->getData()->getId(), '_fragment' => $locale]);
		}

		return null;
	}
}
