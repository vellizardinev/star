<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\City;
use App\Entity\News;
use App\Entity\Office;
use App\Enum\Flash;
use App\Enum\OfficeTypes;
use App\Enum\Settings;
use App\Form\OfficeType;
use App\Message\DeleteOffice;
use App\Service\EntityTranslator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/offices")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_OFFICES")
 */
class OfficesController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.offices.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedOffices = $paginator->paginate(
            $this->manager()->getRepository(Office::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.offices', [], 'text'));

        return $this->render(
            'admin/offices/index.html.twig',
            [
                'offices' => $paginatedOffices,
                'cities' => $this->manager()->getRepository(City::class)->findBy([], ['name' => 'ASC']),
            ]
        );
    }

    /**
     * @Route("/create", name="admin.offices.create", methods={"GET", "POST"})
     * @param Request $request
     * @param EntityTranslator $entityTranslator
     * @return Response
     */
    public function create(Request $request, EntityTranslator $entityTranslator)
    {
        $office = new Office;

        $form = $this->createForm(OfficeType::class, $office);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($office);
            $this->manager()->flush();

	        $entityTranslator->translateOffice($office);

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The office was created.', [], 'messages'));

            return $this->redirectToRoute('admin.offices.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.offices', [], 'text'), $this->generateUrl('admin.offices.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_office', [], 'text'));

        return $this->render(
            'admin/offices/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Office $office
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param string $locales
     * @return Response
     * @Route("/{id}/update", name="admin.offices.update", methods={"GET", "POST"})
     */
    public function update(Office $office, Request $request, FormFactoryInterface $formFactory, string $locales)
    {
	    $localesArray = explode('|', $locales);

	    $contentForms = [];

	    foreach ($localesArray as $locale)
	    {
		    $office->setTranslatableLocale($locale);
		    $this->manager()->refresh($office);

		    $localeForm = $formFactory->createNamed($locale, OfficeType::class, $office);

		    if ($result = $this->handleContentForm($localeForm, $request, $office, $locale)) return $result;

		    $contentForms[$locale] = $localeForm->createView();
	    }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.offices', [], 'text'), $this->generateUrl('admin.offices.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_office', [], 'text'));

        return $this->render(
            'admin/offices/update.html.twig',
            [
                'office' => $office,
                'contentForms' => $contentForms,
            ]
        );
    }

    /**
     * @param Office $office
     * @param MessageBusInterface $messageBus
     * @return JsonResponse
     * @Route("/{id}/delete", name="admin.offices.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
     */
    public function delete(Office $office, MessageBusInterface $messageBus)
    {
        $messageBus->dispatch(new DeleteOffice($office->getId()));

        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The office was deleted.', [], 'messages'));

        return $this->json(null, 200);
    }

    private function handleContentForm(FormInterface $form, Request $request, Office $office, string $locale)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
	        $office->setTranslatableLocale($locale);

            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The office data was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.offices.update', [
                'id' => $form->getData()->getId(),
                '_fragment' => $locale
            ]);
        }

        return null;
    }
}
