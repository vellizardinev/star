<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\News;
use App\Enum\Flash;
use App\Enum\NewsCategories;
use App\Enum\Settings;
use App\Form\CoverImageType;
use App\Form\NewsType;
use App\Form\SeoType;
use App\Form\VisibilityType;
use App\Message\ResetFeaturedNews;
use App\Service\EntityTranslator;
use Knp\Component\Pager\PaginatorInterface;
use ReflectionException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/news")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_NEWS")
 */
class NewsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CustomPagesController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.news.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     * @throws ReflectionException
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedNews = $paginator->paginate(
            $this->manager()->getRepository(News::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.news', [], 'text'));

        return $this->render(
            'admin/news/index.html.twig',
            [
                'newsItems' => $paginatedNews,
                'categories' => NewsCategories::toTranslationKeys(),
            ]
        );
    }

    /**
     * @Route("/create", name="admin.news.create", methods={"GET", "POST"})
     * @param Request $request
     * @param MessageBusInterface $messageBus
     * @param EntityTranslator $entityTranslator
     * @return Response
     */
    public function create(Request $request, MessageBusInterface $messageBus, EntityTranslator $entityTranslator)
    {
        $news = new News;

        $form = $this->createForm(NewsType::class, $news, ['show_slug' => false]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($news);
            $this->manager()->flush();

            $entityTranslator->translateNews($news);

	        $this->resetFeaturedNews($news, $messageBus);

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('News article was created.', [], 'messages'));

            return $this->redirectToRoute('admin.news.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.news', [], 'text'), $this->generateUrl('admin.news.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_news_article', [], 'text'));

        return $this->render(
            'admin/news/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param News $news
     * @param Request $request
     * @param MessageBusInterface $messageBus
     * @param FormFactoryInterface $formFactory
     * @param string $locales
     * @return Response
     * @Route("/{id}/update", name="admin.news.update", methods={"GET", "POST"})
     */
    public function updateContent(News $news, Request $request, MessageBusInterface $messageBus, FormFactoryInterface $formFactory, string $locales)
    {
        $seoForm = $this->createForm(SeoType::class, $news, ['method' => 'post']);
        $visibilityForm = $this->createForm(VisibilityType::class, $news, ['method' => 'post']);
        $coverImageForm = $this->createForm(CoverImageType::class, $news, ['method' => 'post']);

        $localesArray = explode('|', $locales);

        $contentForms = [];

        foreach ($localesArray as $locale)
        {
            $news->setTranslatableLocale($locale);
            $this->manager()->refresh($news);

            $localeForm = $formFactory->createNamed($locale, NewsType::class, $news);

            if ($result = $this->handleContentForm($localeForm, $request, $news, $locale, $messageBus)) return $result;

            $contentForms[$locale] = $localeForm->createView();
        }

        if ($result = $this->handleSeoForm($seoForm, $request)) return $result;
        if ($result = $this->handleVisibilityForm($visibilityForm, $request)) return $result;
        if ($result = $this->handleCoverImageForm($coverImageForm, $request)) return $result;

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.news', [], 'text'), $this->generateUrl('admin.news.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_news_article', [], 'text'));

        return $this->render(
            'admin/news/update.html.twig',
            [
                'page' => $news,
                'contentForms' => $contentForms,
                'seoForm' => $seoForm->createView(),
                'visibilityForm' => $visibilityForm->createView(),
                'coverImageForm' => $coverImageForm->createView(),
            ]
        );
    }

	/**
	 * @param News $news
	 * @return JsonResponse
	 * @Route("/{id}/delete", name="admin.news.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
	 */
	public function delete(News $news)
	{
		$this->manager()->remove($news);
		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The news article was deleted.', [], 'messages'));

		return $this->json(null, 200);
	}

    private function handleContentForm(FormInterface $form, Request $request, News $news, string $locale, MessageBusInterface $messageBus)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news->setTranslatableLocale($locale);

            $this->manager()->flush();

            $this->resetFeaturedNews($news, $messageBus);

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The article\'s content was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.news.update', ['id' => $form->getData()->getId(), '_fragment' => $locale]);
        }

        return null;
    }

    private function handleSeoForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The SEO configuration was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.news.update', ['id' => $form->getData()->getId(), '_fragment' => 'seo']);
        }

        return null;
    }

    private function handleVisibilityForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The visibility configuration was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.news.update', ['id' => $form->getData()->getId(), '_fragment' => 'visibility']);
        }

        return null;
    }

    private function handleCoverImageForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

            $this->addFlash(Flash::SUCCESS, $this->translator->trans('The cover image was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.news.update', ['id' => $form->getData()->getId(), '_fragment' => 'cover-image']);
        }

        return null;
    }

    private function resetFeaturedNews(News $news, MessageBusInterface $messageBus): void
    {
	    if ($news->getFeatured()) {
		    $messageBus->dispatch(new ResetFeaturedNews($news->getId()));
	    }
    }
}
