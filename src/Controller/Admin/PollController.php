<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Poll;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\PollType;
use App\Form\VisibilityType;
use App\Service\EntityTranslator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/polls")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_POLLS")
 */
class PollController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CustomPagesController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.poll.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedPolls = $paginator->paginate(
            $this->manager()->getRepository(Poll::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.polls', [], 'text'));

        return $this->render(
            'admin/polls/index.html.twig',
            [
                'polls' => $paginatedPolls,
            ]
        );
    }

    /**
     * @Route("/create", name="admin.poll.create", methods={"GET", "POST"})
     * @param Request $request
     * @param EntityTranslator $entityTranslator
     * @return Response
     */
    public function create(Request $request, EntityTranslator $entityTranslator)
    {
        $poll = new Poll;

        $form = $this->createForm(PollType::class, $poll);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($poll);
            $this->manager()->flush();

            $entityTranslator->translatePoll($poll);

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The poll was created.', [], 'messages'));

            return $this->redirectToRoute('admin.poll.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.polls', [], 'text'), $this->generateUrl('admin.poll.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_poll', [], 'text'));

        return $this->render(
            'admin/poll/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Poll $poll
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param string $locales
     * @return Response
     * @Route("/{id}/update", name="admin.poll.update", methods={"GET", "POST"})
     */
    public function update(Poll $poll, Request $request, FormFactoryInterface $formFactory, string $locales)
    {
        $visibilityForm = $this->createForm(VisibilityType::class, $poll, ['method' => 'post', 'data_class' => Poll::class]);

        $localesArray = explode('|', $locales);

        $contentForms = [];

        foreach ($localesArray as $locale)
        {
            $poll->setTranslatableLocale($locale);
            $this->manager()->refresh($poll);

            $localeForm = $formFactory->createNamed($locale, PollType::class, $poll);

            if ($result = $this->handleContentForm($localeForm, $request, $poll, $locale)) return $result;

            $contentForms[$locale] = $localeForm->createView();
        }

        if ($result = $this->handleVisibilityForm($visibilityForm, $request)) return $result;

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.polls', [], 'text'), $this->generateUrl('admin.poll.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_poll', [], 'text'));

        return $this->render(
            'admin/poll/update.html.twig',
            [
                'poll' => $poll,
                'contentForms' => $contentForms,
                'visibilityForm' => $visibilityForm->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="admin.poll.get")
     * @param Poll $poll
     * @return Response
     */
    public function getPoll(Poll $poll)
    {
        return $this->json($poll, 200, [], ['groups' => ['main']]);
    }

    /**
     * @Route("/{id}/sort-questions", name="admin.poll.sort_questions", methods={"POST"})
     * @param Poll $poll
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function sortQuestions(Poll $poll, Request $request, TranslatorInterface $translator)
    {
        $sortedQuestions = $request->request->get('sortedQuestions', []);

        $unsortedQuestions = $poll->getQuestions();

        foreach ($unsortedQuestions as $unsortedQuestion) {
            $index = array_search($unsortedQuestion->getId(), $sortedQuestions);

            $unsortedQuestion->setPositionIndex($index);
        }

        $this->manager()->flush();

        return $this->json($translator->trans('poll.questions_were_sorted', [], 'messages'), 200);
    }

    private function handleContentForm(FormInterface $form, Request $request, Poll $poll, string $locale)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $poll->setTranslatableLocale($locale);

            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The poll was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.poll.update', [
                'id' => $form->getData()->getId(),
                '_fragment' => $locale
            ]);
        }

        return null;
    }

    private function handleVisibilityForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The visibility configuration was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.poll.update', [
                'id' => $form->getData()->getId(),
                '_fragment' => $this->translator->trans('visibility', [], 'tabs')
            ]);
        }

        return null;
    }
}
