<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\CareersPartner;
use App\Entity\Credit;
use App\Entity\CreditShortApplication;
use App\Entity\Department;
use App\Entity\Faq;
use App\Entity\File;
use App\Entity\Gift;
use App\Entity\JobOpening;
use App\Entity\News;
use App\Entity\Office;
use App\Entity\LoyaltyPartner;
use App\Entity\Poll;
use App\Entity\Promotion;
use App\Entity\LandingPage;
use App\Enum\DatePeriodTypes;
use App\Service\RabotaConnector;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class DashboardController
 * @package App\Controller\Admin
 */
class DashboardController extends BaseController
{
    /**
     * @return Response
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @Route("/admin", name="admin.start")
     * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_USER")
     */
    public function start()
    {
        if ($this->isGranted("ROLE_DASHBOARD", $this->getUser())) {
            return $this->redirectToRoute('admin.dashboard');
        }

        return $this->render('admin/dashboard/start.html.twig');
    }

    /**
     * @param RouterInterface $router
     * @param RabotaConnector $connector
     * @return Response
     * @throws InvalidArgumentException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @Route("/admin/dashboard", name="admin.dashboard")
     * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_DASHBOARD")
     */
	public function dashboard(RouterInterface $router, RabotaConnector $connector)
    {
	    $generalData['news']['count'] = $this->manager()->getRepository(News::class)->count([]);
	    $generalData['news']['list'] = $router->generate('admin.news.index');
	    $generalData['news']['add'] = $router->generate('admin.news.create');
	    $generalData['news']['icon'] = 'fa-newspaper-o';

	    $vacancyData = $connector->vacancies();
	    $generalData['careers']['count'] = $vacancyData['total'];
	    $generalData['careers']['list'] = $router->generate('career.all');
//	    $generalData['careers']['add'] = $router->generate('admin.job_openings.create');
	    $generalData['careers']['icon'] = 'fa-black-tie';

	    $generalData['polls']['count'] = $this->manager()->getRepository(Poll::class)->count([]);
	    $generalData['polls']['list'] = $router->generate('admin.poll.index');
	    $generalData['polls']['add'] = $router->generate('admin.poll.create');
	    $generalData['polls']['icon'] = 'fa-check-square-o';

	    $generalData['questions']['count'] = $this->manager()->getRepository(Faq::class)->count([]);
	    $generalData['questions']['list'] = $router->generate('admin.faqs.index');
	    $generalData['questions']['add'] = $router->generate('admin.faqs.create');
	    $generalData['questions']['icon'] = 'fa-comments';

	    $generalData['offices']['count'] = $this->manager()->getRepository(Office::class)->count([]);
	    $generalData['offices']['list'] = $router->generate('admin.offices.index');
	    $generalData['offices']['add'] = $router->generate('admin.offices.create');
	    $generalData['offices']['icon'] = 'fa-map-marker';

	    $generalData['departments']['count'] = $this->manager()->getRepository(Department::class)->count([]);
	    $generalData['departments']['list'] = $router->generate('admin.departments.index');
	    $generalData['departments']['add'] = $router->generate('admin.departments.create');
	    $generalData['departments']['icon'] = 'fa-cubes';

	    $generalData['files']['count'] = $this->manager()->getRepository(File::class)->count([]);
	    $generalData['files']['list'] = $router->generate('admin.files.index');
	    $generalData['files']['icon'] = 'fa-picture-o';

	    $productAndServicesData['credits']['count'] = $this->manager()->getRepository(Credit::class)->count([]);
	    $productAndServicesData['credits']['list'] = $router->generate('admin.credits.index');
	    $productAndServicesData['credits']['add'] = $router->generate('admin.credits.create');
	    $productAndServicesData['credits']['icon'] = 'fa-money';

	    $productAndServicesData['promotions']['count'] = $this->manager()->getRepository(Promotion::class)->count([]);
	    $productAndServicesData['promotions']['list'] = $router->generate('admin.promotions.index');
	    $productAndServicesData['promotions']['add'] = $router->generate('admin.promotions.create');
	    $productAndServicesData['promotions']['icon'] = 'fa-star';

	    $productAndServicesData['gifts']['count'] = $this->manager()->getRepository(Gift::class)->count([]);
	    $productAndServicesData['gifts']['list'] = $router->generate('admin.gifts.index');
	    $productAndServicesData['gifts']['add'] = $router->generate('admin.gifts.create');
	    $productAndServicesData['gifts']['icon'] = 'fa-gift';

	    $productAndServicesData['landing_pages']['count'] = $this->manager()->getRepository(LandingPage::class)->count([]);
	    $productAndServicesData['landing_pages']['list'] = $router->generate('admin.pages.index', ['type' => 'landing_page']);
	    $productAndServicesData['landing_pages']['add'] = $router->generate('admin.pages.create', ['type' => 'landing_page']);
	    $productAndServicesData['landing_pages']['icon'] = 'fa-file-text';

	    $productAndServicesData['family_finances']['count'] = 8;
	    $productAndServicesData['family_finances']['icon'] = 'fa-users';

//	    $productAndServicesData['social_campaigns']['count'] = 17;
//	    $productAndServicesData['social_campaigns']['icon'] = 'fa-handshake-o';

//		$loyaltyPartnersCount = $this->manager()->getRepository(LoyaltyPartner::class)->count([]);
//		$careersPartnersCount = $this->manager()->getRepository(CareersPartner::class)->count([]);

//	    $productAndServicesData['loyalty_discounts']['count'] = $loyaltyPartnersCount;
//	    $productAndServicesData['loyalty_discounts']['list'] = $router->generate('admin.loyalty_partners.index');
//	    $productAndServicesData['loyalty_discounts']['add'] = $router->generate('admin.loyalty_partners.create');
//	    $productAndServicesData['loyalty_discounts']['icon'] = 'fa-shopping-bag';
//
//	    $productAndServicesData['careers_discounts']['count'] = $careersPartnersCount;
//	    $productAndServicesData['careers_discounts']['list'] = $router->generate('admin.careers_partners.index');
//	    $productAndServicesData['careers_discounts']['add'] = $router->generate('admin.careers_partners.create');
//	    $productAndServicesData['careers_discounts']['icon'] = 'fa-shopping-bag';

	    $creditApplications['week']['count'] = $this->manager()->getRepository(CreditShortApplication::class)->countByPeriod(DatePeriodTypes::WEEK);
	    $creditApplications['month']['count'] = $this->manager()->getRepository(CreditShortApplication::class)->countByPeriod(DatePeriodTypes::MONTH);
	    $creditApplications['year']['count'] = $this->manager()->getRepository(CreditShortApplication::class)->countByPeriod(DatePeriodTypes::YEAR);

        return $this->render('admin/dashboard/dashboard.html.twig', [
        	'general' => $generalData,
	        'productAndServices' => $productAndServicesData,
	        'creditApplications' => $creditApplications
        ]);
    }
}
