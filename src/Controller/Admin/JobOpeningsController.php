<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\City;
use App\Entity\Department;
use App\Entity\JobOpening;
use App\Entity\Office;
use App\Enum\FileTypes;
use App\Enum\Flash;
use App\Enum\Settings;
use App\File\FileManager;
use App\File\FilesValidation;
use App\Form\CoverImageType;
use App\Form\JobOpeningType;
use App\Form\SeoType;
use App\Form\VisibilityType;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use League\Flysystem\FileExistsException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 */
// @Route("/admin/jobs")
class JobOpeningsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    //     @Route("", name="admin.job_openings.index")
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedJobOpenings = $paginator->paginate(
            $this->manager()->getRepository(JobOpening::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.job_openings', [], 'text'));

        return $this->render(
            'admin/job_openings/index.html.twig',
            [
                'jobOpenings' => $paginatedJobOpenings,
                'offices' => $this->manager()->getRepository(Office::class)->findBy([], ['name' => 'ASC']),
                'departments' => $this->manager()->getRepository(Department::class)->findBy([], ['name' => 'ASC']),
                'cities' => $this->manager()->getRepository(City::class)->findBy([], ['name' => 'ASC']),
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    //     @Route("/create", name="admin.job_openings.create", methods={"GET", "POST"})
    public function create(Request $request)
    {
        $jobOpening = new JobOpening;

        $form = $this->createForm(JobOpeningType::class, $jobOpening, ['show_slug' => false]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($jobOpening);
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The job opening was created.', [], 'messages'));

            return $this->redirectToRoute('admin.job_openings.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.job_openings', [], 'text'), $this->generateUrl('admin.job_openings.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_job_opening', [], 'text'));

        return $this->render(
            'admin/job_openings/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param JobOpening $jobOpening
     * @param Request $request
     * @return Response
     */
    //     @Route("/{id}/update", name="admin.job_openings.update", methods={"GET", "POST"})
    public function update(JobOpening $jobOpening, Request $request)
    {
        $contentForm = $this->createForm(JobOpeningType::class, $jobOpening);
        $visibilityForm = $this->createForm(VisibilityType::class, $jobOpening, ['method' => 'post', 'data_class' => JobOpening::class]);
        $seoForm = $this->createForm(SeoType::class, $jobOpening, ['method' => 'post', 'data_class' => JobOpening::class]);
        $coverImageForm = $this->createForm(CoverImageType::class, $jobOpening, ['method' => 'post', 'data_class' => JobOpening::class]);

        if ($result = $this->handleContentForm($contentForm, $request)) return $result;
        if ($result = $this->handleVisibilityForm($visibilityForm, $request)) return $result;
        if ($result = $this->handleSeoForm($seoForm, $request)) return $result;
        if ($result = $this->handleCoverImageForm($coverImageForm, $request)) return $result;

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.job_openings', [], 'text'), $this->generateUrl('admin.job_openings.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_job_opening', [], 'text'));

        return $this->render(
            'admin/job_openings/update.html.twig',
            [
                'page' => $jobOpening,
                'contentForm' => $contentForm->createView(),
                'seoForm' => $seoForm->createView(),
                'visibilityForm' => $visibilityForm->createView(),
                'coverImageForm' => $coverImageForm->createView(),
            ]
        );
    }

    /**
     * @param JobOpening $jobOpening
     * @param Request $request
     * @param FileManager $fileManager
     * @param ValidatorInterface $validator
     * @param FilesValidation $rules
     * @return JsonResponse
     * @throws FileExistsException
     * @throws Exception
     */
    //     @Route("/{id}/upload-cover-image", name="admin.job_openings.upload_cover_image", methods={"POST"}, condition="request.isXmlHttpRequest()")
    public function uploadCoverImage(JobOpening $jobOpening, Request $request, FileManager $fileManager, ValidatorInterface $validator, FilesValidation $rules)
    {
        $this->deleteImageIfExists($jobOpening, $fileManager);

        $uploadedFile = $request->files->get('file');

        $errors = $validator->validate($uploadedFile, $rules->get(FileTypes::IMAGE));

        if ($errors->count() > 0) {
            return $this->json($errors, 422);
        }

        $image = $fileManager->uploadImage($uploadedFile);
        $jobOpening->setImage($image);

        $this->manager()->persist($image);

        $this->manager()->flush();

        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The cover image was updated.', [], 'messages'));

        return $this->json(null, 201);
    }

    private function handleContentForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The Job opening was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.job_openings.update', [
                'id' => $form->getData()->getId(),
                '_fragment' => $this->translator->trans('content', [], 'tabs')
            ]);
        }

        return null;
    }

    private function handleSeoForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The SEO configuration was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.job_openings.update', [
                'id' => $form->getData()->getId(),
                '_fragment' => $this->translator->trans('seo', [], 'tabs')
            ]);
        }

        return null;
    }

    private function handleVisibilityForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The visibility configuration was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.job_openings.update', [
                'id' => $form->getData()->getId(),
                '_fragment' => $this->translator->trans('visibility', [], 'tabs'),
            ]);
        }

        return null;
    }

    /**
     * @param JobOpening $jobOpening
     * @param FileManager $fileManager
     * @throws Exception
     */
    private function deleteImageIfExists(JobOpening $jobOpening, FileManager $fileManager)
    {
        if (!$jobOpening->getImage()) {
            return;
        }

        $image = $jobOpening->getImage();

        try {
            $fileManager->delete($image);
            $jobOpening->setImage(null);
            $this->manager()->remove($image);
            $this->manager()->flush();
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function handleCoverImageForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

            $this->addFlash(Flash::SUCCESS, $this->translator->trans('The cover image was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.job_openings.update', ['id' => $form->getData()->getId(), '_fragment' => 'cover-image']);
        }

        return null;
    }
}
