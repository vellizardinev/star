<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Permission\Group;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\PermissionGroupType;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class GroupsController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/permission-groups")
 */
class GroupsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * GroupsController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.permission_groups.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedPermissionGroups = $paginator->paginate(
            $this->manager()->getRepository(Group::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.permission_groups', [], 'text'));

        return $this->render(
            'admin/permission_groups/index.html.twig',
            [
                'permissionGroups' => $paginatedPermissionGroups,
            ]
        );
    }

    /**
     * @Route("/create", name="admin.permission_groups.create", methods={"GET", "POST"})
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $group = new Group();

        $form = $this->createForm(PermissionGroupType::class, $group);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($group);
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('Permission Group was created.', [], 'messages'));

            return $this->redirectToRoute('admin.permission_groups.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.permission_groups', [], 'text'), $this->generateUrl('admin.permission_groups.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_permission_group', [], 'text'));

        return $this->render('admin/permission_groups/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/update", name="admin.permission_groups.update", methods={"GET", "POST"})
     * @param Request $request
     *
     * @param Group $group
     * @return RedirectResponse|Response
     */
    public function update(Request $request, Group $group)
    {
        $form = $this->createForm(PermissionGroupType::class, $group);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('Permission Group was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.permission_groups.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.permission_groups', [], 'text'), $this->generateUrl('admin.permission_groups.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_permission_group', [], 'text'));

        return $this->render('admin/permission_groups/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
