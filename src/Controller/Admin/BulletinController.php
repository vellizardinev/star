<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\BulletinList;
use App\Enum\Settings;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Exception;

/**
 * @package App\Controller\Admin
 * @Route("/admin/bulletins")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_BULLETIN")
 */
class BulletinController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->breadcrumbs = $breadcrumbs;
		$this->translator = $translator;
	}

	/**
	 * @Route("", name="admin.bulletins.index")
	 * @param Request $request
	 * @param PaginatorInterface $paginator
	 * @return Response
	 */
	public function index(Request $request, PaginatorInterface $paginator)
	{
		$paginatedBulletins = $paginator->paginate(
			$this->manager()->getRepository(BulletinList::class)->filtered($request->query->all()),
			$request->query->getInt('page', 1),
			$this->getSetting(Settings::PAGINATION_PER_PAGE)
		);

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.bulletins', [], 'text'));

		return $this->render(
			'admin/bulletins/index.html.twig',
			[
				'bulletins' => $paginatedBulletins,
			]
		);
	}

	/**
	 * @param BulletinList $bulletinList
	 * @return JsonResponse
	 * @Route("/{id}/delete", name="admin.bulletins.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
	 */
	public function delete(BulletinList $bulletinList)
	{
		$this->manager()->remove($bulletinList);
		$this->manager()->flush();

		return $this->json(null, 200);
	}

	/**
	 * @param SerializerInterface $serializer
	 * @param Request $request
	 * @Route("/export-csv", name="admin.bulletins.export_csv", methods={"GET"})
	 * @return Response
	 * @throws Exception
	 */
	public function export(Request $request, SerializerInterface $serializer)
	{
		$bulletinList = $this->manager()->getRepository(BulletinList::class)->filtered($request->query->all())->getResult();

		$rows = array();

		$rows[] = ['email', 'date'];

		foreach ($bulletinList as $bulletin) {
			$data = array($bulletin->getSubscriberEmail(), $bulletin->getCreatedAt()->format('Y-m-d H:i:s'));

			$rows[] = $data;
		}

		$dateTime = new \DateTime();

		$fileName = 'bulletins_list_' . $dateTime->format('U') . '.csv';

		if($request->query->get('start_date') || $request->query->get('end_date') )
		{
			$fileName = 'filtered_' . $fileName;
		}
		
		$response = new Response($serializer->encode($rows, 'csv', [CsvEncoder::NO_HEADERS_KEY => true,]));
		$response->headers->set('Content-Type', 'text/csv');
		$disposition = HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_ATTACHMENT, $fileName);
		$response->headers->set('Content-Disposition', $disposition);

		return $response;
	}
}
