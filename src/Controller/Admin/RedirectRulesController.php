<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Page;
use App\Entity\RedirectRule;
use App\Enum\Flash;
use App\Form\RedirectRuleType;
use App\Service\RedirectRulesService;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/redirect-rules")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_REDIRECT_RULES")
 */
class RedirectRulesController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.redirect_rules.index")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $rules = $this->manager()->getRepository(RedirectRule::class)->filtered($request->query->all())->getResult();

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.redirect_rules', [], 'text'));

        return $this->render(
            'admin/redirect_rules/index.html.twig',
            [
                'rules' => $rules,
            ]
        );
    }

    /**
     * @Route("/create", name="admin.redirect_rules.create", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $redirectRule = new RedirectRule();

        $form = $this->createForm(RedirectRuleType::class, $redirectRule);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($redirectRule);
            $this->manager()->flush();

            $this->oldUrlUsedInPageMessage($redirectRule);

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The redirect rule was created.', [], 'messages'));

            return $this->redirectToRoute('admin.redirect_rules.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.redirect_rules', [], 'text'), $this->generateUrl('admin.redirect_rules.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_redirect_rule', [], 'text'));

        return $this->render(
            'admin/redirect_rules/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param RedirectRule $redirectRule
     * @param Request $request
     * @return Response
     * @Route("/{id}/update", name="admin.redirect_rules.update", methods={"GET", "POST"})
     */
    public function update(RedirectRule $redirectRule, Request $request)
    {
        $contentForm = $this->createForm(RedirectRuleType::class, $redirectRule);

        if ($result = $this->handleContentForm($contentForm, $request, $redirectRule)) return $result;

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.redirect_rules', [], 'text'), $this->generateUrl('admin.redirect_rules.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_redirect_rule', [], 'text'));

        return $this->render(
            'admin/redirect_rules/update.html.twig',
            [
                'page' => $redirectRule,
                'form' => $contentForm->createView(),
            ]
        );
    }

    /**
     * @param RedirectRule $redirectRule
     * @return JsonResponse
     * @Route("/{id}/delete", name="admin.redirect_rules.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
     */
	public function delete(RedirectRule $redirectRule)
	{
		$this->manager()->remove($redirectRule);
		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The Redirect Rule was deleted.', [], 'messages'));

		return $this->json(null, 200);
	}

    /**
     * @param RedirectRulesService $redirectRuleService
     * @return RedirectResponse
     * @Route("/apply", name="admin.redirect_rules.apply", methods={"POST"})
     */
    public function apply(RedirectRulesService $redirectRuleService)
    {
        $redirectRuleService->applyRules();

        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The Redirect Rules were applied.', [], 'messages'));

        return $this->redirectToRoute('admin.redirect_rules.index');
    }

    private function handleContentForm(FormInterface $form, Request $request, RedirectRule $redirectRule)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->oldUrlUsedInPageMessage($redirectRule);

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The Redirect Rule was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.redirect_rules.update', ['id' => $form->getData()->getId()]);
        }

        return null;
    }

	private function oldUrlUsedInPageMessage(RedirectRule $redirectRule): void
	{
		$page = $this->manager()->getRepository(Page::class)->findOneBy(['path' => $redirectRule->getOldUrl()]);
		if ($page) {
			$this->addFlash(Flash::ERROR, $this->translator->trans('Redirect rule old url is used as page path!', [], 'messages'));
		}
	}
}
