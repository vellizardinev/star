<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LocaleController
 * @package App\Controller\Admin
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_USER")
 */
class LocaleController extends BaseController
{
    /**
     * @Route("/admin/locale/{locale}", name="admin.locale.change", methods={"POST"}, condition="request.isXmlHttpRequest()")
     * @param Request $request
     * @param string $locale
     * @return JsonResponse
     */
	public function change(Request $request, string $locale): JsonResponse
	{
        $request->getSession()->set('_locale', $locale);

        return new JsonResponse(null, 200);
	}
}
