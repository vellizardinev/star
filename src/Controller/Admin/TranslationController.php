<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Enum\Flash;
use Exception;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @package App\Controller\Admin
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_UI_TRANSLATIONS")
 */
class TranslationController extends BaseController
{
    /**
     * @Route("/admin/translations/index", name="admin.translations.index", methods={"GET"})
     * @param string $siriusTranslationsAuthToken
     * @param string $siriusTranslationsUrl
     * @return Response
     */
    public function index(string $siriusTranslationsAuthToken, string $siriusTranslationsUrl)
    {
        return $this->render('admin/translation/index.html.twig', [
            'siriusTranslationsAuthToken' => $siriusTranslationsAuthToken,
            'siriusTranslationsUrl' => $siriusTranslationsUrl
        ]);
    }

    /**
     * @Route("/admin/translations/clear-cache", name="admin.translations.clear_cache", methods={"POST"})
     * @param KernelInterface $kernel
     * @return Response
     * @throws Exception
     */
    public function clearCache(KernelInterface $kernel)
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'cache:clear'
        ]);

        $output = new BufferedOutput();
        $application->run($input, $output);

        return $this->redirectToRoute('admin.translations.index');
    }

    /**
     * @Route("/admin/translations/download-from-remote", name="admin.translations.download_from_remote", methods={"POST"})
     * @param KernelInterface $kernel
     * @param TranslatorInterface $translator
     * @return Response
     * @throws Exception
     */
    public function downloadFromRemote(KernelInterface $kernel, TranslatorInterface $translator)
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'sirius:translation:download'
        ]);

        $output = new BufferedOutput();
        $application->run($input, $output);

        $this->addFlash(Flash::SUCCESS, $translator->trans('Translations were downloaded.', [], 'messages'));

        return $this->redirectToRoute('admin.translations.index');
    }

    /**
     * @Route("/admin/translations/upload-to-remote", name="admin.translations.upload_to_remote", methods={"POST"})
     * @param KernelInterface $kernel
     * @param TranslatorInterface $translator
     * @return Response
     * @throws Exception
     */
    public function uploadToRemote(KernelInterface $kernel, TranslatorInterface $translator)
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'sirius:translation:upload'
        ]);

        $output = new BufferedOutput();
        $application->run($input, $output);

        $this->addFlash(Flash::SUCCESS, $translator->trans('Translations were uploaded.', [], 'messages'));

        return $this->redirectToRoute('admin.translations.index');
    }
}
