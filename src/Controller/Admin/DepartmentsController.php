<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Department;
use App\Enum\FileTypes;
use App\Enum\Flash;
use App\File\FileManager;
use App\File\FilesValidation;
use App\Form\DepartmentType;
use App\Service\EntityTranslator;
use League\Flysystem\FileExistsException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Exception;

/**
 * @package App\Controller\Admin
 * @Route("/admin/departments")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_DEPARTMENTS")
 */
class DepartmentsController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->breadcrumbs = $breadcrumbs;
		$this->translator = $translator;
	}

	/**
	 * @Route("", name="admin.departments.index")
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$departments = $this->manager()->getRepository(Department::class)->filtered($request->query->all())->getResult();

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.departments', [], 'text'));

		return $this->render(
			'admin/departments/index.html.twig',
			[
				'departments' => $departments,
			]
		);
	}

	/**
	 * @Route("/create", name="admin.departments.create", methods={"GET", "POST"})
	 * @param Request $request
	 * @param EntityTranslator $entityTranslator
	 * @return Response
	 */
	public function create(Request $request, EntityTranslator $entityTranslator)
	{
		$department = new Department();

		$form = $this->createForm(DepartmentType::class, $department);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->manager()->persist($department);
			$this->manager()->flush();

			$entityTranslator->translateDepartment($department);

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('Department was created.', [], 'messages'));

			return $this->redirectToRoute('admin.departments.index');
		}

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.departments', [], 'text'), $this->generateUrl('admin.departments.index'));
		$this->breadcrumbs->addItem($this->translator->trans('title.create_department', [], 'text'));

		return $this->render(
			'admin/departments/create.html.twig',
			[
				'form' => $form->createView(),
			]
		);
	}

	/**
	 * @param Department $department
	 * @param Request $request
	 * @param FormFactoryInterface $formFactory
	 * @param string $locales
	 * @return Response
	 * @Route("/{id}/update", name="admin.departments.update", methods={"GET", "POST"})
	 */
	public function update(Department $department, Request $request, FormFactoryInterface $formFactory, string $locales)
	{
		$localesArray = explode('|', $locales);

		$contentForms = null;

		foreach ($localesArray as $locale)
		{
			$department->setTranslatableLocale($locale);
			$this->manager()->refresh($department);

			$localeForm = $formFactory->createNamed($locale, DepartmentType::class, $department);

			if ($result = $this->handleContentForm($localeForm, $request, $department, $locale)) return $result;

			$contentForms[$locale] = $localeForm->createView();
		}

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.departments', [], 'text'), $this->generateUrl('admin.departments.index'));
		$this->breadcrumbs->addItem($this->translator->trans('title.update_department', [], 'text'));

		return $this->render(
			'admin/departments/update.html.twig',
			[
				'contentForms' => $contentForms,
			]
		);
	}

	/**
	 * @param Request $request
	 * @return Response
	 * @Route("/order", name="admin.departments.order", methods={"POST"})
	 */
	public function order(Request $request)
	{
		$sortedDepartments = $request->request->get('sortedDepartments', []);

		$unsortedDepartments = $this->manager()->getRepository(Department::class)->findAll();

		if (empty($sortedDepartments)) {
			$this->addFlash(Flash::ERROR, $this->translator->trans('Please, add some departments first.', [], 'messages'));

			return $this->redirectToRoute('admin.departments.index');
		}

		foreach ($unsortedDepartments as $unsortedDepartment) {
			$index = array_search($unsortedDepartment->getId(), $sortedDepartments);

			$unsortedDepartment->setPositionIndex($index);
		}

		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The order of departments was updated.', [], 'messages'));

		return $this->redirectToRoute('admin.departments.index');
	}

	/**
	 * @param Department $department
	 * @param Request $request
	 * @param FileManager $fileManager
	 * @param ValidatorInterface $validator
	 * @param FilesValidation $rules
	 * @return JsonResponse
	 * @throws FileExistsException
	 * @throws Exception
	 * @Route("/{id}/upload-cover-image", name="admin.departments.upload_cover_image", methods={"POST"}, condition="request.isXmlHttpRequest()")
	 */
	public function uploadCoverImage(Department $department, Request $request, FileManager $fileManager, ValidatorInterface $validator, FilesValidation $rules)
	{
		$this->deleteImageIfExists($department, $fileManager);

		$uploadedFile = $request->files->get('file');

		$errors = $validator->validate($uploadedFile, $rules->get(FileTypes::IMAGE));

		if ($errors->count() > 0) {
			return $this->json($errors, 422);
		}

		$image = $fileManager->uploadImage($uploadedFile);
		$department->setImage($image);

		$this->manager()->persist($image);

		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The cover image was updated.', [], 'messages'));

		return $this->json(null, 201);
	}

	/**
	 * @param Department $department
	 * @param FileManager $fileManager
	 * @throws Exception
	 */
	private function deleteImageIfExists(Department $department, FileManager $fileManager)
	{
		if (!$department->getImage()) {
			return;
		}

		$image = $department->getImage();

		try {
			$fileManager->delete($image);
			$department->setImage(null);
			$this->manager()->remove($image);
			$this->manager()->flush();
		} catch (Exception $e) {
			throw $e;
		}
	}

	private function handleContentForm(FormInterface $form, Request $request, Department $department, string $locale)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$department->setTranslatableLocale($locale);

			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('The department content was updated.', [], 'messages'));

			return $this->redirectToRoute('admin.departments.update', ['id' => $form->getData()->getId(), '_fragment' => $locale]);
		}

		return null;
	}
}
