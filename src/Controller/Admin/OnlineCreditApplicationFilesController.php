<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Document;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Enum\Flash;
use App\Service\MobileApi\UploadFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @package App\Controller\Admin
 * @Route("/admin/online-application-files")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_ONLINE_APPLICATIONS")
 */
class OnlineCreditApplicationFilesController extends BaseController
{
    /**
     * @Route("/resend/{id}/{hash}/{type}", name="admin.online_credit_application_files.resend", methods={"POST"})
     * @param Document $document
     * @param string $hash
     * @param string $type
     * @param TranslatorInterface $translator
     * @param UploadFile $uploadFileService
     * @return Response
     */
    public function resend(
        Document $document,
        string $hash,
        string $type,
        TranslatorInterface $translator,
        UploadFile $uploadFileService
    )
    {
        $application = $this->manager()
            ->getRepository(OnlineApplication::class)
            ->findOneBy(['hash' => $hash]);

        if (!$application) {
            throw $this->createNotFoundException();
        }

        $result = $uploadFileService->uploadAsBase64String($document, $application->getErpId(), $type);

        if ($result === true) {
            $this->addFlash(Flash::SUCCESS, 'The file has been resent.');
        } else {
            $this->addFlash(Flash::ERROR, 'The file upload failed.');
        }

        return $this->redirectToRoute('admin.online_credit_applications.show', [
            'id' => $application->getId(),
            '_fragment' => $translator->trans('tab.files', [], 'text')
        ]);
    }
}
