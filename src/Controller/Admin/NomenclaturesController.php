<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Nomenclature;
use App\Enum\NomenclatureTypes;
use App\Enum\Settings;
use Knp\Component\Pager\PaginatorInterface;
use ReflectionException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/nomenclatures")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_NOMENCLATURES")
 */
class NomenclaturesController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.nomenclatures.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param string $locales
     * @return Response
     * @throws ReflectionException
     */
    public function index(Request $request, PaginatorInterface $paginator, string $locales)
    {
        $locales = explode('|', $locales);

        $nomenclatures = $paginator->paginate(
            $this->manager()->getRepository(Nomenclature::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.nomenclatures', [], 'text'));

        return $this->render(
            'admin/nomenclatures/index.html.twig',
            [
                'nomenclatures' => $nomenclatures,
                'nomenclatureTypes' => NomenclatureTypes::toTranslationKeys(),
                'locales' => $locales,
            ]
        );
    }

    /**
     * @param Nomenclature $nomenclature
     * @param string $locale
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @Route("/{id}/update-text/{locale}", name="admin.nomenclatures.update_text", methods={"POST"})
     */
    public function updateText(Nomenclature $nomenclature, string $locale, Request $request, ValidatorInterface $validator)
    {
        $value = $request->request->get('value', null);

        $errors = $validator->validate($value, new NotBlank());

        if ($errors->count() > 0) {
            return $this->json($errors, 422);
        }

        $nomenclature->setTranslatableLocale($locale);
        $nomenclature->setText($value);
        $this->manager()->flush();

        return $this->json(null, 200);
    }

    /**
     * @param Nomenclature $nomenclature
     * @param Request $request
     * @return Response
     * @Route("/{id}/update-visible", name="admin.nomenclatures.update_visible", methods={"POST"})
     */
    public function updateVisible(Nomenclature $nomenclature, Request $request)
    {
        $value = $request->request->get('value', 'true');
        $value = $value === 'true';

        $nomenclature->setVisible($value);
        $this->manager()->flush();

        return $this->json(null, 200);
    }
}
