<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\File;
use App\Enum\FileTypes;
use App\Enum\Flash;
use App\Enum\Settings;
use App\File\FileManager;
use App\File\FilesValidation;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class FilesController
 * @package App\Controller\Admin
 * @Route("/admin/files")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_FILES")
 */
class FilesController extends BaseController
{
    /**
     * @Route("", name="admin.files.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator, Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $paginatedFiles = $paginator->paginate(
            $this->manager()->getRepository(File::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $breadcrumbs->addItem($translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $breadcrumbs->addItem($translator->trans('title.files', [], 'text'), $this->generateUrl('admin.files.index'));

        return $this->render(
            'admin/files/index.html.twig',
            [
                'files' => $paginatedFiles,
            ]
        );
    }

    /**
     * @Route("/create", name="admin.files.create", methods={"POST"}, condition="request.isXmlHttpRequest()")
     * @param Request $request
     * @param FileManager $fileManager
     * @param ValidatorInterface $validator
     * @param FilesValidation $rules
     * @param TranslatorInterface $translator
     * @return JsonResponse
     * @throws Exception
     */
    public function create(Request $request, FileManager $fileManager, ValidatorInterface $validator, FilesValidation $rules, TranslatorInterface $translator)
    {
        $uploadedFile = $request->files->get('file');

        $errors = $validator->validate($uploadedFile, $rules->get(FileTypes::FILE));

        if ($errors->count() > 0) {
            return $this->json($errors, 422);
        }

        $file = $fileManager->uploadFile($uploadedFile);
        $this->manager()->persist($file);
        $this->manager()->flush();

	    $this->addFlash(Flash::SUCCESS, $translator->trans('File was uploaded', [], 'messages'));

        return $this->json(null, 201);
    }

    /**
     * @Route("/{id}", name="admin.files.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
     * @param File $file
     * @param TranslatorInterface $translator
     * @return JsonResponse
     */
    public function delete(File $file, TranslatorInterface $translator)
    {
        try {
            $this->manager()->remove($file);

            $this->manager()->flush();

            return $this->json(null, 200);
        } catch (Exception $exception) {
            $this->addFlash(Flash::ERROR, $translator->trans('The file is being used and could not be deleted.', [], 'messages'));
            return $this->json(null, 400);
        }
    }

    /**
     * @Route("/{id}/download", name="admin.files.download", methods={"GET"})
     * @param File $file
     * @param FileManager $fileManager
     * @return StreamedResponse
     */
    public function download(File $file, FileManager $fileManager)
    {
        $response = new StreamedResponse(function () use ($file, $fileManager) {
            $outputStream = fopen('php://output', 'wb');
            $fileStream = $fileManager->readStream($file);

            stream_copy_to_stream($fileStream, $outputStream);
        });

        $response->headers->set('Content-Type', $file->getMime());

        $response->headers->set('Content-Disposition', $this->createAttachmentDisposition($file->getOriginalName() . '.' . $file->getMime()));

        return $response;
    }

	/**
	 * @Route("/image/create", name="admin.files.image.create", methods={"POST"}, condition="request.isXmlHttpRequest()")
	 * @param Request $request
	 * @param FileManager $fileManager
	 * @param ValidatorInterface $validator
	 * @param FilesValidation $rules
	 * @param TranslatorInterface $translator
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function createImage(Request $request, FileManager $fileManager, ValidatorInterface $validator, FilesValidation $rules, TranslatorInterface $translator)
	{
		$uploadedFile = $request->files->get('file');

		$errors = $validator->validate($uploadedFile, $rules->get(FileTypes::IMAGE));

		if ($errors->count() > 0) {
			return $this->json($errors, 422);
		}

		$file = $fileManager->uploadImage($uploadedFile);
		$this->manager()->persist($file);
		$this->manager()->flush();

		$showFlash = (bool) $request->query->get('flash', 1);

        if ($showFlash) {
            $this->addFlash(Flash::SUCCESS, $translator->trans('Image was uploaded', [], 'messages'));
        }

        return $this->json($file, 201);
	}

	private function createAttachmentDisposition(string $fileName)
    {
        return sprintf('attachment; filename="%s"', $fileName);
    }
}
