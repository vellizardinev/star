<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Mail;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\MailType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class MailsController
 * @package App\Controller\Admin
 * @Route("/admin/mails")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_EMAILS")
 */
class MailsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * MailsController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.mails.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param string $locales
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator, string $locales)
    {
        $paginatedMails = $paginator->paginate(
            $this->manager()->getRepository(Mail::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbsForIndex();

        return $this->render(
            'admin/mails/index.html.twig',
            [
                'mails' => $paginatedMails,
                'locales' => explode('|', $locales),
            ]
        );
    }

    /**
     * @param Mail $mail
     * @param Request $request
     * @return Response
     * @Route("/{id}/update", name="admin.mails.update", methods={"GET", "POST"})
     */
    public function update(Mail $mail, Request $request)
    {
        $form = $this->createForm(MailType::class, $mail);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($mail);
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('Mail was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.mails.index');
        }

        $this->breadcrumbsForUpdate();

        return $this->render(
            'admin/mails/update.html.twig',
            [
                'mail' => $mail,
                'form' => $form->createView(),
            ]
        );
    }

    private function breadcrumbsForIndex()
    {
        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.mails', [], 'text'), $this->generateUrl('admin.mails.index'));
    }

    private function breadcrumbsForUpdate()
    {
        $this->breadcrumbsForIndex();
        $this->breadcrumbs->addItem($this->translator->trans('title.update_mail', [], 'text'));

    }
}
