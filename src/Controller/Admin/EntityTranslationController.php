<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\EntityTranslation\EntityTranslationService;
use App\Enum\Settings;
use App\Enum\TranslatableEntities;
use Doctrine\ORM\NonUniqueResultException;
use Knp\Component\Pager\PaginatorInterface;
use ReflectionException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class EntityTranslationController
 * @package App\Controller\Admin
 * @Route("/admin/entity-translations")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_ENTITY_TRANSLATIONS")
 */
class EntityTranslationController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * EntityTranslationController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.entity_translation.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param string $locales
     * @param EntityTranslationService $entityTranslationService
     * @return Response
     * @throws ReflectionException
     */
    public function index(Request $request, PaginatorInterface $paginator, string $locales, EntityTranslationService $entityTranslationService)
    {
        $paginatedTranslations = $paginator->paginate(
            $entityTranslationService->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.entity_translations', [], 'text'), $this->generateUrl('admin.entity_translation.index'));

        return $this->render('admin/entity_translation/index.html.twig', [
            'translations' => $paginatedTranslations,
            'locales' => explode('|', $locales),
            'classes' => TranslatableEntities::toSelectArray(),
        ]);
    }

    /**
     * @Route("/{class}/{foreignKey}/{field}/edit", name="admin.entity_translation.edit")
     * @param $class
     * @param $foreignKey
     * @param $field
     * @param string $locales
     * @return Response
     */
    public function edit($class, $foreignKey, $field, string $locales)
    {
        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.entity_translations', [], 'text'), $this->generateUrl('admin.entity_translation.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_entity_translation', [], 'text'));

        return $this->render('admin/entity_translation/update.html.twig', [
            'locales' => $locales,
            'class' => $class,
            'foreignKey' => $foreignKey,
            'field' => $field,
        ]);
    }

    /**
     * @Route("/get/{class}/{foreignKey}/{field}/{locale}", name="admin.entity_translations.get", methods={"GET"})
     * @param $class
     * @param $foreignKey
     * @param $field
     * @param $locale
     * @param EntityTranslationService $entityTranslationService
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function getTranslation($class, $foreignKey, $field, $locale, EntityTranslationService $entityTranslationService)
    {
        $translation = $entityTranslationService->find($class, $foreignKey, $field, $locale);

        return $this->json($translation, 200);
    }

    /**
     * @Route("/{class}/{foreignKey}/{field}/{locale}", name="admin.entity_translations.update", methods={"POST"})
     * @param $class
     * @param $foreignKey
     * @param $field
     * @param $locale
     * @param EntityTranslationService $entityTranslationService
     * @param Request $request
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function updateTranslation($class, $foreignKey, $field, $locale, EntityTranslationService $entityTranslationService, Request $request)
    {
        $translation = $entityTranslationService->find($class, $foreignKey, $field, $locale);

        $content = $request->request->get('content', '');

        $translation->setContent($content);
        $this->manager()->persist($translation);
        $this->manager()->flush();

        return $this->json(null, 200);
    }
}
