<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Affiliate;
use App\Enum\Flash;
use App\Form\AffiliateType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/affiliates")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_AFFILIATES")
 */
class AffiliatesController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->breadcrumbs = $breadcrumbs;
		$this->translator = $translator;
	}

	/**
	 * @Route("", name="admin.affiliates.index")
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		$affiliates = $this->manager()->getRepository(Affiliate::class)->filtered($request->query->all())->getResult();

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.affiliates', [], 'text'));

		return $this->render(
			'admin/affiliate/index.html.twig',
			[
				'affiliates' => $affiliates,
			]
		);
	}

	/**
	 * @Route("/create", name="admin.affiliates.create", methods={"GET", "POST"})
	 * @param Request $request
	 * @return Response
	 */
	public function create(Request $request)
	{
		$affiliate = new Affiliate();

		$form = $this->createForm(AffiliateType::class, $affiliate);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->manager()->persist($affiliate);
			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('Affiliate was created.', [], 'messages'));

			return $this->redirectToRoute('admin.affiliates.index');
		}

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.affiliates', [], 'text'), $this->generateUrl('admin.affiliates.index'));
		$this->breadcrumbs->addItem($this->translator->trans('title.create_affiliate', [], 'text'));

		return $this->render(
			'admin/affiliate/create.html.twig',
			[
				'form' => $form->createView(),
			]
		);
	}

	/**
	 * @param Affiliate $affiliate
	 * @param Request $request
	 * @return Response
	 * @Route("/{id}/update", name="admin.affiliates.update", methods={"GET", "POST"})
	 */
	public function update(Affiliate $affiliate, Request $request)
	{
		$form = $this->createForm(AffiliateType::class, $affiliate);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('The affiliate content was updated.', [], 'messages'));

			return $this->redirectToRoute('admin.affiliates.index');
		}

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.affiliates', [], 'text'), $this->generateUrl('admin.affiliates.index'));
		$this->breadcrumbs->addItem($this->translator->trans('title.update_affiliate', [], 'text'));

		return $this->render(
			'admin/affiliate/update.html.twig',
			[
				'form' => $form->createView(),
                'affiliate' => $affiliate,
			]
		);
	}
}
