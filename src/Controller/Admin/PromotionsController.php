<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Promotion;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\CoverImageType;
use App\Form\PromotionType;
use App\Form\SeoType;
use App\Form\VisibilityType;
use App\Message\ResetFeaturedPromotions;
use App\Service\EntityTranslator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/promotions")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_PROMOTIONS")
 */
class PromotionsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CustomPagesController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.promotions.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedPromotions = $paginator->paginate(
            $this->manager()->getRepository(Promotion::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.promotions', [], 'text'));

        return $this->render(
            'admin/promotions/index.html.twig',
            [
                'promotions' => $paginatedPromotions,
            ]
        );
    }

    /**
     * @Route("/create", name="admin.promotions.create", methods={"GET", "POST"})
     * @param Request $request
     * @param MessageBusInterface $messageBus
     * @param EntityTranslator $entityTranslator
     * @return Response
     */
    public function create(Request $request, MessageBusInterface $messageBus, EntityTranslator $entityTranslator)
    {
        $promotion = new Promotion;

        $form = $this->createForm(PromotionType::class, $promotion, ['show_slug' => false]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($promotion);
            $this->manager()->flush();

            $entityTranslator->translatePromotion($promotion);

	        $this->resetFeaturedPromotions($promotion, $messageBus);

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('Promotion was created.', [], 'messages'));

            return $this->redirectToRoute('admin.promotions.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.promotions', [], 'text'), $this->generateUrl('admin.promotions.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_promotion', [], 'text'));

        return $this->render(
            'admin/promotions/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Promotion $promotion
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param string $locales
     * @return Response
     * @Route("/{id}/update", name="admin.promotions.update", methods={"GET", "POST"})
     */
    public function update(Promotion $promotion, Request $request, FormFactoryInterface $formFactory, string $locales)
    {
        $seoForm = $this->createForm(SeoType::class, $promotion, ['method' => 'post', 'data_class' => Promotion::class]);
        $visibilityForm = $this->createForm(VisibilityType::class, $promotion, ['method' => 'post', 'data_class' => Promotion::class]);
        $coverImageForm = $this->createForm(CoverImageType::class, $promotion, ['method' => 'post', 'data_class' => Promotion::class]);

        $localesArray = explode('|', $locales);

        $contentForms = [];

        foreach ($localesArray as $locale)
        {
            $promotion->setTranslatableLocale($locale);
            $this->manager()->refresh($promotion);

            $localeForm = $formFactory->createNamed($locale, PromotionType::class, $promotion);

            if ($result = $this->handleContentForm($localeForm, $request, $promotion, $locale)) return $result;

            $contentForms[$locale] = $localeForm->createView();
        }

        if ($result = $this->handleSeoForm($seoForm, $request)) return $result;
        if ($result = $this->handleVisibilityForm($visibilityForm, $request)) return $result;
        if ($result = $this->handleCoverImageForm($coverImageForm, $request)) return $result;

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.promotions', [], 'text'), $this->generateUrl('admin.promotions.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_credit', [], 'text'));

        return $this->render(
            'admin/promotions/update.html.twig',
            [
                'page' => $promotion,
                'contentForms' => $contentForms,
                'seoForm' => $seoForm->createView(),
                'visibilityForm' => $visibilityForm->createView(),
                'coverImageForm' => $coverImageForm->createView(),
            ]
        );
    }

	/**
	 * @param Promotion $promotion
	 * @return JsonResponse
	 * @Route("/{id}/delete", name="admin.promotions.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
	 */
	public function delete(Promotion $promotion)
	{
		$this->manager()->remove($promotion);
		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The promotion was deleted.', [], 'messages'));

		return $this->json(null, 200);
	}

    private function handleContentForm(FormInterface $form, Request $request, Promotion $promotion, string $locale)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $promotion->setTranslatableLocale($locale);
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The promotion\'s content was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.promotions.update', ['id' => $form->getData()->getId(), '_fragment' => $locale]);
        }

        return null;
    }

    private function handleSeoForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The SEO configuration was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.promotions.update', ['id' => $form->getData()->getId(), '_fragment' => 'seo']);
        }

        return null;
    }

    private function handleVisibilityForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The visibility configuration was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.promotions.update', ['id' => $form->getData()->getId(), '_fragment' => 'visibility']);
        }

        return null;
    }

    private function handleCoverImageForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

            $this->addFlash(Flash::SUCCESS, $this->translator->trans('The cover image was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.promotions.update', ['id' => $form->getData()->getId(), '_fragment' => 'cover-image']);
        }

        return null;
    } 

    private function resetFeaturedPromotions(Promotion $promotion, MessageBusInterface $messageBus): void
    {
	    if ($promotion->getFeatured()) {
		    $messageBus->dispatch(new ResetFeaturedPromotions($promotion->getId()));
	    }
    }
}
