<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\ModuleItem;
use App\Enum\Flash;
use App\Module\ExtractsModulePaths;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @Route("/admin/managed-modules")
 * Class ModuleItemController
 * @package App\Controller
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_MANAGED_MODULES")
 */
class ModuleItemController extends BaseController
{
    use ExtractsModulePaths;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * PageItemController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("", name="module_item.managed_module_items", methods={"GET"})
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function managed(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $moduleItems = $this->manager()
            ->getRepository(ModuleItem::class)
            ->namedItems();

        $breadcrumbs->addItem($translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $breadcrumbs->addItem($translator->trans('title.managed_modules', [], 'text'), 'text');

        return $this->render('/module-item/managed_module_items.html.twig', [
            'moduleItems' => $moduleItems,
        ]);
    }

    /**
     * @Route("/{slug}/configure", name="module_item.configure", methods={"POST", "GET"})
     * @param ModuleItem $moduleItem
     * @param Request $request
     * @param Breadcrumbs $breadcrumbs
     * @param FormFactoryInterface $formFactory
     * @return Response
     */
    public function configure(ModuleItem $moduleItem, Request $request, Breadcrumbs $breadcrumbs, FormFactoryInterface $formFactory)
    {
        $formClass = $this->extractFormClass($moduleItem);
        $formTemplate = $this->extractFormTemplate($moduleItem);

        $uaParameter = $this->getDataForLanguage($moduleItem, 'ua');
        $ruParameter = $this->getDataForLanguage($moduleItem, 'ru');
        $enParameter = $this->getDataForLanguage($moduleItem, 'en');

        $uaForm = $formFactory->createNamedBuilder('ua', $formClass, $uaParameter)->getForm();
        $ruForm = $formFactory->createNamedBuilder('ru', $formClass, $ruParameter)->getForm();
        $enForm = $formFactory->createNamedBuilder('en', $formClass, $enParameter)->getForm();

        if ($result = $this->handleForm($moduleItem, $uaForm, $request, 'ua')) return $result;
        if ($result = $this->handleForm($moduleItem, $ruForm, $request, 'ru')) return $result;
        if ($result = $this->handleForm($moduleItem, $enForm, $request, 'en')) return $result;

        $breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $breadcrumbs->addItem($this->translator->trans('title.managed_modules', [], 'text'), $this->generateUrl('module_item.managed_module_items'));
        $breadcrumbs->addItem($moduleItem->getName(), 'text');

        return $this->render('/module-item/configure.html.twig', [
            'uaForm' => $uaForm->createView(),
            'ruForm' => $ruForm->createView(),
            'enForm' => $enForm->createView(),
            'cancelUrl' => $this->generateUrl('module_item.managed_module_items'),
            'formTemplate' => $formTemplate,
            'moduleItem' => $moduleItem,
        ]);
    }

    private function getDataForLanguage($module, string $locale)
    {
        $dataArray = unserialize($module->getData());

        return $dataArray[$locale];
    }

    private function setDataForLanguage(ModuleItem $moduleItem, $data, string $locale)
    {
        if ($moduleItem->getData()) {
            $dataArray = unserialize($moduleItem->getData());
        } else {
            $dataArray = unserialize($moduleItem->getModule()->getData());
        }

        $dataArray[$locale] = $data;
        $moduleItem->setData(serialize($dataArray));
    }

    private function handleForm(ModuleItem $moduleItem, FormInterface $form, Request $request, string $language)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->setDataForLanguage($moduleItem, $form->getData(), $language);

            $this->manager()->flush();
            $this->addFlash(Flash::SUCCESS, $this->translator->trans('The module was updated.', [], 'messages'));

            return $this->redirectToRoute('module_item.configure', ['slug' => $moduleItem->getSlug(), '_fragment' => $language]);
        }

        return null;
    }
}
