<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Module;
use App\Entity\ModuleItem;
use App\Entity\Page;
use App\Entity\RedirectRule;
use App\Enum\Flash;
use App\Enum\PageTypes;
use App\Enum\Settings;
use App\Form\PageType;
use App\Form\SeoType;
use App\Form\SiteMapType;
use App\Form\VisibilityType;
use App\LandingPage\LandingPageManager;
use Knp\Component\Pager\PaginatorInterface;
use ReflectionException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class CustomPagesController
 * @package App\Controller\Admin
 * @Route("/admin/pages")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_PAGES")
 */
class PagesController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CustomPagesController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.pages.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     * @throws ReflectionException
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedPages = $paginator->paginate(
            $this->manager()->getRepository(Page::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.pages', [], 'text'));

        return $this->render(
            'admin/pages/index.html.twig',
            [
                'pages' => $paginatedPages,
                'pageTypes' => PageTypes::toTranslationKeys(),
            ]
        );
    }

	/**
	 * @Route("/create", name="admin.pages.create", methods={"GET", "POST"})
	 * @param Request $request
	 * @param LandingPageManager $landingPageManager
	 * @return Response
	 */
	public function create(Request $request, LandingPageManager $landingPageManager)
	{
		$page = new Page;

		$pageType = $request->query->get('type', 'page');

		$form = $this->createForm(PageType::class, $page, ['show_path' => false, 'show_type' => $pageType]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$type = $form->get('type')->getData();

			if ($type === PageTypes::LANDING_PAGE) {
				$landingPage = $landingPageManager->createLandingPage($page);
				$this->translatePage($landingPage);

				$this->pathUsedInRedirectRuleMessage($landingPage);
			} else {
				$this->manager()->persist($page);
				$this->manager()->flush();
				$this->translatePage($page);

				$this->pathUsedInRedirectRuleMessage($page);
			}

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('Page was created.', [], 'messages'));

			return $this->redirectToRoute('admin.pages.index');
		}

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.pages', [], 'text'), $this->generateUrl('admin.pages.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_page', [], 'text'));

        return $this->render(
            'admin/pages/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Page $page
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param string $locales
     * @return Response
     * @Route("/{id}/update", name="admin.pages.update", methods={"GET", "POST"})
     */
    public function update(Page $page, Request $request, FormFactoryInterface $formFactory, string $locales)
    {
	    $seoForm = $this->createForm(SeoType::class, $page, ['method' => 'post', 'data_class' => Page::class]);
        $visibilityForm = $this->createForm(VisibilityType::class, $page, ['method' => 'post', 'data_class' => Page::class]);
        $siteMapForm = $this->createForm(SiteMapType::class, $page, ['method' => 'post', 'data_class' => Page::class]);

        $localesArray = explode('|', $locales);

        $contentForms = null;

	    foreach ($localesArray as $locale)
	    {
		    $page->setTranslatableLocale($locale);
		    $this->manager()->refresh($page);

		    $localeForm = $formFactory->createNamed($locale, PageType::class, $page);

		    if ($result = $this->handleContentForm($localeForm, $request, $page, $locale)) return $result;

		    $contentForms[$locale] = $localeForm->createView();
	    }

	    if ($result = $this->handleSeoForm($seoForm, $request)) return $result;
        if ($result = $this->handleVisibilityForm($visibilityForm, $request)) return $result;
        if ($result = $this->handleSiteMapForm($siteMapForm, $request)) return $result;

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.pages', [], 'text'), $this->generateUrl('admin.pages.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_page', [], 'text'));

        return $this->render(
            'admin/pages/update.html.twig',
            [
                'page' => $page,
                'contentForms' => $contentForms,
                'seoForm' => $seoForm->createView(),
                'visibilityForm' => $visibilityForm->createView(),
                'siteMapForm' => $siteMapForm->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}/manage-content", name="admin.pages.manage_content")
     * @param Page $page
     * @return string
     */
    public function manageContent(Page $page)
    {
        $modules = $this->manager()->getRepository(Module::class)->filtered([])->getResult();

        $pageModules = $this->manager()->getRepository(ModuleItem::class)->findForPage($page);

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.pages', [], 'text'), $this->generateUrl('admin.pages.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.manage_content', [], 'text'));

        $this->addRedirect($this->generateUrl('admin.pages.manage_content', ['id' => $page->getId()]));

        return $this->render('admin/pages/manage_content.html.twig', [
            'page' => $page,
            'modules' => $modules,
            'pageModules' => $pageModules,
        ]);
    }

    /**
     * @Route("/update-module-item-name/{id}", name="admin.pages.update_module_item_name", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @param ModuleItem $moduleItem
     * @param Request $request
     * @return JsonResponse
     */
    public function updateModuleItemName(ModuleItem $moduleItem, Request $request)
    {
        $name = $request->request->get('value');

        $moduleItem->setUiName($name);
        $this->manager()->flush();

        return $this->json(null);
    }

    private function handleContentForm(FormInterface $form, Request $request, Page $page, string $locale)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
		    $page->setTranslatableLocale($locale);

			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('The page was updated.', [], 'messages'));

			return $this->redirectToRoute('admin.pages.update', ['id' => $form->getData()->getId(), '_fragment' => $locale]);
		}

		return null;
	}

	private function handleSeoForm(FormInterface $form, Request $request)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('The SEO configuration was updated.', [], 'messages'));

			return $this->redirectToRoute('admin.pages.update', ['id' => $form->getData()->getId(), '_fragment' => 'seo']);
		}

		return null;
	}

    private function handleVisibilityForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

            $this->addFlash(Flash::SUCCESS, $this->translator->trans('The visibility configuration was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.pages.update', ['id' => $form->getData()->getId(), '_fragment' => 'visibility']);
        }

        return null;
    }

    private function handleSiteMapForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

            $this->addFlash(Flash::SUCCESS, $this->translator->trans('The Site Map configuration of the page was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.pages.update', ['id' => $form->getData()->getId(), '_fragment' => 'site-map']);
        }

        return null;
    }

    private function translatePage(Page $page): Page
    {
        $repository = $this->manager()->getRepository('Gedmo\\Translatable\\Entity\\Translation');
        $name = $page->getName();
        $path = $page->getPath();

        $repository
            ->translate($page, 'name', 'ru', $name)
	        ->translate($page, 'path', 'ru', $path)
	        ->translate($page, 'name', 'en', $name)
	        ->translate($page, 'path', 'en', $path);

	    $this->manager()->persist($page);
	    $this->manager()->flush();

	    return $page;
    }

	private function pathUsedInRedirectRuleMessage(Page $page): void
	{
		$rule = $this->manager()->getRepository(RedirectRule::class)->findOneBy(['oldUrl' => $page->getPath()]);
		if ($rule) {
			$this->addFlash(Flash::ERROR, $this->translator->trans('Page path is used as redirect rule!', [], 'messages'));
		}
	}
}
