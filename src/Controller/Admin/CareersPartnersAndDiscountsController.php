<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\CareersPartner;
use App\Enum\FileTypes;
use App\Enum\Flash;
use App\File\FileManager;
use App\File\FilesValidation;
use App\Form\PartnerType;
use App\Form\VisibilityType;
use Exception;
use League\Flysystem\FileExistsException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/career-partners-and-discounts")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_ADMIN")
 */
class CareersPartnersAndDiscountsController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->breadcrumbs = $breadcrumbs;
		$this->translator = $translator;
	}

	/**
	 * @Route("", name="admin.careers_partners.index")
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		// Page will not be available at launch of the site
		throw $this->createNotFoundException();

		$partners = $this->manager()->getRepository(CareersPartner::class)->filtered($request->query->all())->getResult();

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.careers_partners', [], 'text'));

		return $this->render(
			'admin/careers_partners_and_discounts/index.html.twig',
			[
				'partners' => $partners,
			]
		);
	}

	/**
	 * @Route("/create", name="admin.careers_partners.create", methods={"GET", "POST"})
	 * @param Request $request
	 * @return Response
	 */
	public function create(Request $request)
	{
		// Page will not be available at launch of the site
		throw $this->createNotFoundException();

		$partner = new CareersPartner();

		$form = $this->createForm(PartnerType::class, $partner, ['data_class' => CareersPartner::class]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->manager()->persist($partner);
			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('Partner was created.', [], 'messages'));

			return $this->redirectToRoute('admin.careers_partners.index');
		}

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.careers_partners', [], 'text'), $this->generateUrl('admin.careers_partners.index'));
		$this->breadcrumbs->addItem($this->translator->trans('title.create_careers_partners', [], 'text'));

		return $this->render(
			'admin/careers_partners_and_discounts/create.html.twig',
			[
				'form' => $form->createView(),
			]
		);
	}

	/**
	 * @param CareersPartner $partner
	 * @param Request $request
	 * @return Response
	 * @Route("/{id}/update", name="admin.careers_partners.update", methods={"GET", "POST"})
	 */
	public function update(CareersPartner $partner, Request $request)
	{
		// Page will not be available at launch of the site
		throw $this->createNotFoundException();

		$contentForm = $this->createForm(PartnerType::class, $partner, ['data_class' => CareersPartner::class]);
		$visibilityForm = $this->createForm(VisibilityType::class, $partner, ['method' => 'post', 'data_class' => CareersPartner::class]);

		if ($result = $this->handleContentForm($contentForm, $request)) return $result;
		if ($result = $this->handleVisibilityForm($visibilityForm, $request)) return $result;

		$this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
		$this->breadcrumbs->addItem($this->translator->trans('title.careers_partners', [], 'text'), $this->generateUrl('admin.careers_partners.index'));
		$this->breadcrumbs->addItem($this->translator->trans('title.update_careers_partners', [], 'text'));

		return $this->render(
			'admin/careers_partners_and_discounts/update.html.twig',
			[
				'form' => $contentForm->createView(),
				'visibilityForm' => $visibilityForm->createView(),
			]
		);
	}

	/**
	 * @param CareersPartner $partner
	 * @return JsonResponse
	 * @Route("/{id}/delete", name="admin.careers_partners.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
	 */
	public function delete(CareersPartner $partner)
	{
		// Page will not be available at launch of the site
		throw $this->createNotFoundException();

		$this->manager()->remove($partner);
		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('Partner was deleted.', [], 'messages'));

		return $this->json(null, 200);
	}

	/**
	 * @param Request $request
	 * @return Response
	 * @Route("/order", name="admin.careers_partners.order", methods={"POST"})
	 */
	public function order(Request $request)
	{
		// Page will not be available at launch of the site
		throw $this->createNotFoundException();

		$sortedPartners = $request->request->get('sortedPartners', []);

		$unsortedPartners = $this->manager()->getRepository(CareersPartner::class)->findAll();

		if (empty($sortedPartners)) {
			$this->addFlash(Flash::ERROR, $this->translator->trans('Please, add some partners first.', [], 'messages'));

			return $this->redirectToRoute('admin.careers_partners.index');
		}

		foreach ($unsortedPartners as $unsortedPartner) {
			$index = array_search($unsortedPartner->getId(), $sortedPartners);

			$unsortedPartner->setPositionIndex($index);
		}

		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The order of partners was updated.', [], 'messages'));

		return $this->redirectToRoute('admin.careers_partners.index');
	}

	/**
	 * @param CareersPartner $partner
	 * @param Request $request
	 * @param FileManager $fileManager
	 * @param ValidatorInterface $validator
	 * @param FilesValidation $rules
	 * @return JsonResponse
	 * @throws FileExistsException
	 * @throws Exception
	 * @Route("/{id}/upload-cover-image", name="admin.careers_partners.upload_cover_image", methods={"POST"}, condition="request.isXmlHttpRequest()")
	 */
	public function uploadCoverImage(CareersPartner $partner, Request $request, FileManager $fileManager, ValidatorInterface $validator, FilesValidation $rules)
	{
		// Page will not be available at launch of the site
		throw $this->createNotFoundException();

		$this->deleteImageIfExists($partner, $fileManager);

		$uploadedFile = $request->files->get('file');

		$errors = $validator->validate($uploadedFile, $rules->get(FileTypes::IMAGE));

		if ($errors->count() > 0) {
			return $this->json($errors, 422);
		}

		$image = $fileManager->uploadImage($uploadedFile);
		$partner->setImage($image);

		$this->manager()->persist($image);

		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The cover image was updated.', [], 'messages'));

		return $this->json(null, 201);
	}

	private function handleContentForm(FormInterface $form, Request $request)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('The partner\'s content was updated.', [], 'messages'));

			return $this->redirectToRoute('admin.careers_partners.update', ['id' => $form->getData()->getId(), '_fragment' => 'content']);
		}

		return null;
	}

	private function handleVisibilityForm(FormInterface $form, Request $request)
	{
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->manager()->flush();

			$this->addFlash(Flash::SUCCESS, $this->translator->trans('The visibility configuration was updated.', [], 'messages'));

			return $this->redirectToRoute('admin.careers_partners.update', ['id' => $form->getData()->getId(), '_fragment' => 'visibility']);
		}

		return null;
	}

	/**
	 * @param CareersPartner $partner
	 * @param FileManager $fileManager
	 * @throws Exception
	 */
	private function deleteImageIfExists(CareersPartner $partner, FileManager $fileManager)
	{
		if (!$partner->getImage()) {
			return;
		}

		$image = $partner->getImage();

		try {
			$fileManager->delete($image);
			$partner->setImage(null);
			$this->manager()->remove($image);
			$this->manager()->flush();
		} catch (Exception $e) {
			throw $e;
		}
	}
}
