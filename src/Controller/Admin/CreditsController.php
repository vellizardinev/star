<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Credit;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\CreditType;
use App\Form\SeoType;
use App\Service\EntityTranslator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/credits")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_CREDITS")
 */
class CreditsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.credits.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedCredits = $paginator->paginate(
            $this->manager()->getRepository(Credit::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem(
            $this->translator->trans('title.administration', [], 'text'),
            $this->generateUrl('admin.dashboard')
        );
        $this->breadcrumbs->addItem($this->translator->trans('title.credits', [], 'text'));

        return $this->render(
            'admin/credits/index.html.twig',
            [
                'credits' => $paginatedCredits,
                'filtered' => count($request->query->all()) > 0,
            ]
        );
    }

    /**
     * @Route("/create", name="admin.credits.create", methods={"GET", "POST"})
     * @param Request $request
     * @param EntityTranslator $entityTranslator
     * @return Response
     */
    public function create(Request $request, EntityTranslator $entityTranslator)
    {
        $credit = new Credit;

        $form = $this->createForm(CreditType::class, $credit, ['show_slug' => false]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($credit);
            $this->manager()->flush();

            $entityTranslator->translateCredit($credit);

            $this->addFlash(Flash::SUCCESS, $this->translator->trans('Credit was created.', [], 'messages'));

            return $this->redirectToRoute('admin.credits.index');
        }

        $this->breadcrumbs->addItem(
            $this->translator->trans('title.administration', [], 'text'),
            $this->generateUrl('admin.dashboard')
        );
        $this->breadcrumbs->addItem(
            $this->translator->trans('title.credits', [], 'text'),
            $this->generateUrl('admin.credits.index')
        );
        $this->breadcrumbs->addItem($this->translator->trans('title.create_credit', [], 'text'));

        return $this->render(
            'admin/credits/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Credit $credit
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param string $locales
     * @return Response
     * @Route("/{id}/update", name="admin.credits.update", methods={"GET", "POST"})
     */
    public function update(Credit $credit, Request $request, FormFactoryInterface $formFactory, string $locales)
    {
        $seoForm = $this->createForm(SeoType::class, $credit, ['method' => 'post', 'data_class' => Credit::class]);

        $localesArray = explode('|', $locales);

        $contentForms = [];

        foreach ($localesArray as $locale) {
            $credit->setTranslatableLocale($locale);
            $this->manager()->refresh($credit);

            $localeForm = $formFactory->createNamed($locale, CreditType::class, $credit);

            if ($result = $this->handleContentForm($localeForm, $request, $credit, $locale)) {
                return $result;
            }

            $contentForms[$locale] = $localeForm->createView();
        }

        if ($result = $this->handleSeoForm($seoForm, $request)) {
            return $result;
        }

        $this->breadcrumbs->addItem(
            $this->translator->trans('title.administration', [], 'text'),
            $this->generateUrl('admin.dashboard')
        );
        $this->breadcrumbs->addItem(
            $this->translator->trans('title.credits', [], 'text'),
            $this->generateUrl('admin.credits.index')
        );
        $this->breadcrumbs->addItem($this->translator->trans('title.update_credit', [], 'text'));

        return $this->render(
            'admin/credits/update.html.twig',
            [
                'page' => $credit,
                'contentForms' => $contentForms,
                'seoForm' => $seoForm->createView(),
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/order", name="admin.credits.order", methods={"POST"})
     */
    public function order(Request $request)
    {
        $sortedCredits = $request->request->get('sortedCredits', []);

        $unsortedCredits = $this->manager()->getRepository(Credit::class)->findAll();

        if (empty($sortedCredits)) {
            $this->addFlash(Flash::ERROR, $this->translator->trans('Please, add some credits first.', [], 'messages'));

            return $this->redirectToRoute('admin.credits.index');
        }

        foreach ($unsortedCredits as $unsortedCredit) {
            $index = array_search($unsortedCredit->getId(), $sortedCredits);

            $unsortedCredit->setPositionIndex($index);
        }

        $this->manager()->flush();

        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The order of credits was updated.', [], 'messages'));

        return $this->redirectToRoute('admin.credits.index');
    }

    private function handleContentForm(FormInterface $form, Request $request, Credit $credit, string $locale)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $credit->setTranslatableLocale($locale);

            $this->manager()->flush();

            $this->addFlash(
                Flash::SUCCESS,
                $this->translator->trans('The credit\'s content was updated.', [], 'messages')
            );

            return $this->redirectToRoute(
                'admin.credits.update',
                ['id' => $form->getData()->getId(), '_fragment' => $locale]
            );
        }

        return null;
    }

    private function handleSeoForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

            $this->addFlash(
                Flash::SUCCESS,
                $this->translator->trans('The SEO configuration was updated.', [], 'messages')
            );

            return $this->redirectToRoute(
                'admin.credits.update',
                ['id' => $form->getData()->getId(), '_fragment' => 'seo']
            );
        }

        return null;
    }
}
