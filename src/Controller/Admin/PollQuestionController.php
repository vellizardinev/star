<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Poll;
use App\Entity\PollQuestion;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @package App\Controller\Admin
 * @Route("/admin/questions")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_POLLS")
 */
class PollQuestionController extends BaseController
{
    /**
     * @Route("/{id}", name="admin.question.get", requirements={"id"="\d+"}, methods={"GET"})
     * @param PollQuestion $question
     * @return Response
     */
    public function getAnswer(PollQuestion $question)
    {
        return $this->json($question, 200, [], ['groups' => ['main']]);
    }

    /**
     * @Route("/new", name="admin.question.create", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function create(Request $request, ValidatorInterface $validator, TranslatorInterface $translator)
    {
        $pollId = $request->request->get('pollId', null);
        $poll = $this->manager()->getRepository(Poll::class)->find($pollId);

        if (!$poll) {
            return $this->json($translator->trans('not_found', [], 'messages'), 404);
        }

        $question = (new PollQuestion)
            ->setPoll($poll)
            ->setText($request->request->get('text'))
            ->setType($request->request->get('type'));

        $errors = $validator->validate($question);

        if ($errors->count() > 0) {
            return $this->json($errors[0]->getMessage(), 422);
        }

        $this->manager()->persist($question);

        $this->manager()->flush();

        $message = $translator->trans('poll_question_was_created', [], 'messages');

        return $this->json([
            'message' => $message,
            'payload' => ['id' => $question->getId(), 'text' => $question->getText(), 'type' => $question->getType()],
        ], 201, ['groups' => ['main']]);
    }

    /**
     * @Route("/{id}/update", name="admin.question.update", methods={"POST"})
     * @param PollQuestion $question
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function update(PollQuestion $question, Request $request, ValidatorInterface $validator, TranslatorInterface $translator)
    {
        $question
            ->setText($request->request->get('text'))
            ->setType($request->request->get('type'));

        $errors = $validator->validate($question);

        if ($errors->count() > 0) {
            return $this->json($errors[0]->getMessage(), 422);
        }

        $this->manager()->flush();

        return $this->json($translator->trans('poll_question_was_updated', [], 'messages'), 200);
    }

    /**
     * @Route("/{id}/delete", name="admin.question.delete", methods={"POST"})
     * @param PollQuestion $question
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function delete(PollQuestion $question, TranslatorInterface $translator)
    {
        $this->manager()->remove($question);

        $this->manager()->flush();

        return $this->json($translator->trans('poll_question_was_deleted', [], 'messages'), 200);
    }

    /**
     * @Route("/{id}/sort-answers", name="admin.question.sort_answers", methods={"POST"})
     * @param PollQuestion $question
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function sortAnswers(PollQuestion $question, Request $request, TranslatorInterface $translator)
    {
        $sortedAnswers = $request->request->get('sortedAnswers', []);

        $unsortedAnswers = $question->getAnswers();

        foreach ($unsortedAnswers as $unsortedAnswer) {
            $index = array_search($unsortedAnswer->getId(), $sortedAnswers);

            $unsortedAnswer->setPositionIndex($index);
        }

        $this->manager()->flush();

        return $this->json($translator->trans('poll_question.answers_were_sorted', [], 'messages'), 200);
    }
}
