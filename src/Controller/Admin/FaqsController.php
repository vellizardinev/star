<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Faq;
use App\Enum\Flash;
use App\Form\FaqType;
use App\Form\VisibilityType;
use App\Service\EntityTranslator;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller\Admin
 * @Route("/admin/faqs")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_FAQS")
 */
class FaqsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.faqs.index")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $faqs = $this->manager()->getRepository(Faq::class)->filtered($request->query->all())->getResult();

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.faqs', [], 'text'));

        return $this->render(
            'admin/faqs/index.html.twig',
            [
                'faqs' => $faqs,
                'filtered' => count($request->query->all()) > 0,
            ]
        );
    }

    /**
     * @Route("/create", name="admin.faqs.create", methods={"GET", "POST"})
     * @param Request $request
     * @param EntityTranslator $entityTranslator
     * @return Response
     */
    public function create(Request $request, EntityTranslator $entityTranslator)
    {
        $faq = new Faq;

        $form = $this->createForm(FaqType::class, $faq);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($faq);
            $this->manager()->flush();

            $entityTranslator->translateFaq($faq);

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('FAQ was created.', [], 'messages'));

            return $this->redirectToRoute('admin.faqs.index');
        }

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.faqs', [], 'text'), $this->generateUrl('admin.faqs.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_faq', [], 'text'));

        return $this->render(
            'admin/faqs/create.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Faq $faq
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param string $locales
     * @return Response
     * @Route("/{id}/update", name="admin.faqs.update", methods={"GET", "POST"})
     */
    public function update(Faq $faq, Request $request, FormFactoryInterface $formFactory, string $locales)
    {
        $visibilityForm = $this->createForm(VisibilityType::class, $faq, ['method' => 'post', 'data_class' => Faq::class]);

        $localesArray = explode('|', $locales);

        $contentForms = [];

        foreach ($localesArray as $locale)
        {
            $faq->setTranslatableLocale($locale);
            $this->manager()->refresh($faq);

            $localeForm = $formFactory->createNamed($locale, FaqType::class, $faq);

            if ($result = $this->handleContentForm($localeForm, $request, $faq, $locale)) return $result;

            $contentForms[$locale] = $localeForm->createView();
        }

        if ($result = $this->handleVisibilityForm($visibilityForm, $request)) return $result;

        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.faqs', [], 'text'), $this->generateUrl('admin.faqs.index'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_faq', [], 'text'));

        return $this->render(
            'admin/faqs/update.html.twig',
            [
                'page' => $faq,
                'contentForms' => $contentForms,
                'visibilityForm' => $visibilityForm->createView(),
            ]
        );
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/order", name="admin.faqs.order", methods={"POST"})
     */
    public function order(Request $request)
    {
        $sortedQuestions = $request->request->get('sortedQuestions', []);

        $unsortedQuestions = $this->manager()->getRepository(Faq::class)->findAll();

        if (empty($sortedQuestions)) {
	        $this->addFlash(Flash::ERROR, $this->translator->trans('Please, add some questions first.', [], 'messages'));

            return $this->redirectToRoute('admin.faqs.index');
        }

        foreach ($unsortedQuestions as $unsortedQuestion) {
            $index = array_search($unsortedQuestion->getId(), $sortedQuestions);

            $unsortedQuestion->setPositionIndex($index);
        }

        $this->manager()->flush();

	    $this->addFlash(Flash::SUCCESS, $this->translator->trans('The order of questions was updated.', [], 'messages'));

        return $this->redirectToRoute('admin.faqs.index');
    }

	/**
	 * @param Faq $faq
	 * @return JsonResponse
	 * @Route("/{id}/delete", name="admin.faqs.delete", methods={"DELETE"}, condition="request.isXmlHttpRequest()")
	 */
	public function delete(Faq $faq)
	{
		$this->manager()->remove($faq);
		$this->manager()->flush();

		$this->addFlash(Flash::SUCCESS, $this->translator->trans('The FAQ was deleted.', [], 'messages'));

		return $this->json(null, 200);
	}

    private function handleContentForm(FormInterface $form, Request $request, Faq $faq, string $locale)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $faq->setTranslatableLocale($locale);

            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The FAQ\'s content was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.faqs.update', ['id' => $form->getData()->getId(), '_fragment' => $locale]);
        }

        return null;
    }

    private function handleVisibilityForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The visibility configuration was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.faqs.update', ['id' => $form->getData()->getId(), '_fragment' => 'visibility']);
        }

        return null;
    }
}
