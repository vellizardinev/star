<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Module;
use App\Entity\ModuleItem;
use App\Entity\Page;
use App\Entity\UserDefinedModule;
use App\Enum\Flash;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class PageItemController
 * @package App\Controller
 * @Route("/page-item")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_PAGES")
 */
class PageItemController extends BaseController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * PageItemController constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/{id}", name="page_item.create", methods={"POST"})
     * @param Page $page
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function create(Page $page, Request $request)
    {
        $moduleId = $request->request->get('selectedModule', null);

        $module = $this->manager()->getRepository(Module::class)->find($moduleId);

        if (!$module) {
            throw $this->createNotFoundException("Could not find module with Id=$moduleId/.");
        }

        if ($module instanceof UserDefinedModule || ($module instanceof Module && $module->getData() === null)) {
            $moduleItem = (new ModuleItem)
                ->setModule($module)
                ->setPage($page);

            $this->manager()->persist($moduleItem);
            $page->addModuleItem($moduleItem);

            $this->manager()->flush();

            $this->addFlash(Flash::SUCCESS, $this->translator->trans('Module was added to the page.', [], 'messages'));

            return $this->redirectToRoute('admin.pages.manage_content', ['id' => $page->getId()]);
        }

        return $this->redirectToRoute(
            'page_item.configure_and_create',
            ['pageId' => $page->getId(), 'moduleId' => $module->getId()]
        );
    }

    /**
     * @Route("/{pageId}/configure-and-create/{moduleId}/", name="page_item.configure_and_create", methods={"POST", "GET"})
     * @param int $pageId
     * @param int $moduleId
     * @param Request $request
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    public function configureAndCreate(int $pageId, int $moduleId, Request $request, Breadcrumbs $breadcrumbs)
    {
        $language = $request->query->get('language', 'ua');

        $page = $this->manager()->getRepository(Page::class)->find($pageId);
        $module = $this->manager()->getRepository(Module::class)->find($moduleId);

        if (!$page || !$module) {
            throw $this->createNotFoundException();
        }

        $formClass = $this->extractFormClass($module);
        $formTemplate = $this->extractFormTemplate($module);

        $moduleParameter = $this->getDataForLanguage($module, $language);

        $form = $this->createForm($formClass, $moduleParameter);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $moduleItem = (new ModuleItem)
                ->setPage($page)
                ->setModule($module);

            $this->setDataForLanguage($moduleItem, $moduleParameter, $language);

            $this->manager()->persist($moduleItem);

            $page->addModuleItem($moduleItem);

            $this->manager()->flush();

            $this->addFlash(Flash::SUCCESS, $this->translator->trans('Module was added to the page.', [], 'messages'));

            return $this->redirectToRoute('admin.pages.manage_content', ['id' => $page->getId()]);
        }

        $breadcrumbs->addItem(
            $this->translator->trans('title.administration', [], 'text'),
            $this->generateUrl('admin.dashboard')
        );
        $breadcrumbs->addItem(
            $this->translator->trans('title.pages', [], 'text'),
            $this->generateUrl('admin.pages.index')
        );
        $breadcrumbs->addItem(
            $this->translator->trans('title.manage_content', [], 'text'),
            $this->generateUrl('admin.pages.manage_content', ['id' => $page->getId()])
        );
        $breadcrumbs->addItem($this->translator->trans('title.configure_module', [], 'text'));

        return $this->render(
            '/page-item/configure.html.twig',
            [
                'form' => $form->createView(),
                'formTemplate' => $formTemplate,
                'cancelUrl' => $this->generateUrl('admin.pages.manage_content', ['id' => $pageId]),
                'module' => $module,
            ]
        );
    }

    /**
     * @Route("/{id}/configure", name="page_item.configure", methods={"POST", "GET"})
     * @param ModuleItem $moduleItem
     * @param Request $request
     * @param Breadcrumbs $breadcrumbs
     * @param FormFactoryInterface $formFactory
     * @return Response
     */
    public function configure(ModuleItem $moduleItem, Request $request, Breadcrumbs $breadcrumbs, FormFactoryInterface $formFactory)
    {
        $module = $moduleItem->getModule();
        $formClass = $this->extractFormClass($module);
        $formTemplate = $this->extractFormTemplate($module);

        $uaParameter = $this->getDataForLanguage($moduleItem, 'ua');
        $ruParameter = $this->getDataForLanguage($moduleItem, 'ru');
        $enParameter = $this->getDataForLanguage($moduleItem, 'en');

        $uaForm = $formFactory->createNamedBuilder('ua', $formClass, $uaParameter)->getForm();
        $ruForm = $formFactory->createNamedBuilder('ru', $formClass, $ruParameter)->getForm();
        $enForm = $formFactory->createNamedBuilder('en', $formClass, $enParameter)->getForm();

        if ($result = $this->handleForm($moduleItem, $uaForm, $request, 'ua')) return $result;
        if ($result = $this->handleForm($moduleItem, $ruForm, $request, 'ru')) return $result;
        if ($result = $this->handleForm($moduleItem, $enForm, $request, 'en')) return $result;

        $breadcrumbs->addItem(
            $this->translator->trans('title.administration', [], 'text'),
            $this->generateUrl('admin.dashboard')
        );
        $breadcrumbs->addItem(
            $this->translator->trans('title.pages', [], 'text'),
            $this->generateUrl('admin.pages.index')
        );
        $breadcrumbs->addItem(
            $this->translator->trans('title.manage_content', [], 'text'),
            $this->generateUrl('admin.pages.manage_content', ['id' => $moduleItem->getPage()->getId()])
        );
        $breadcrumbs->addItem($this->translator->trans('title.configure_module', [], 'text'));

        return $this->render(
            '/page-item/configure.html.twig',
            [
                'uaForm' => $uaForm->createView(),
                'ruForm' => $ruForm->createView(),
                'enForm' => $enForm->createView(),
                'formTemplate' => $formTemplate,
                'cancelUrl' => $this->generateUrl(
                    'admin.pages.manage_content',
                    ['id' => $moduleItem->getPage()->getId()]
                ),
                'module' => $module,
            ]
        );
    }

    /**
     * @Route("/{pageId}/remove/{moduleItemId}", name="page_item.remove", methods={"POST"})
     * @param int $pageId
     * @param int $moduleItemId
     * @return RedirectResponse
     */
    public function remove(int $pageId, int $moduleItemId)
    {
        $page = $this->manager()->getRepository(Page::class)->find($pageId);
        $moduleItem = $this->manager()->getRepository(ModuleItem::class)->find($moduleItemId);

        if (!$page || !$moduleItem) {
            throw $this->createNotFoundException();
        }

        $page->removeModuleItem($moduleItem);
        $this->manager()->remove($moduleItem);
        $this->manager()->flush();

        $this->addFlash(Flash::SUCCESS, $this->translator->trans('Module was removed to the page.', [], 'messages'));

        return $this->redirectToRoute('admin.pages.manage_content', ['id' => $page->getId()]);
    }

    /**
     * @Route("/{pageId}/copy/{moduleItemId}", name="page_item.copy", methods={"POST"})
     * @param int $pageId
     * @param int $moduleItemId
     * @return RedirectResponse
     */
    public function copy(int $pageId, int $moduleItemId)
    {
        $page = $this->manager()->getRepository(Page::class)->find($pageId);
        $moduleItem = $this->manager()->getRepository(ModuleItem::class)->find($moduleItemId);

        if (!$page || !$moduleItem) {
            throw $this->createNotFoundException();
        }

        $copiedModuleItem = clone $moduleItem;
        $page->addModuleItem($copiedModuleItem);

        $this->manager()->persist($copiedModuleItem);
        $this->manager()->flush();

        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The module was copied.', [], 'messages'));

        return $this->redirectToRoute('admin.pages.manage_content', ['id' => $page->getId()]);
    }

    /**
     * @Route("/{id}/order", name="page_item.save_order", methods={"POST"})
     * @param Page $page
     * @param Request $request
     * @return RedirectResponse
     */
    public function order(Page $page, Request $request)
    {
        $selectedModules = $request->request->get('selectedModules', []);

        if (empty($selectedModules)) {
            $this->addFlash(
                Flash::ERROR,
                $this->translator->trans('Please, add some modules to the page first.', [], 'messages')
            );

            return $this->redirectToRoute('admin.pages.manage_content', ['id' => $page->getId()]);
        }

        foreach ($page->getModuleItems() as $moduleItem) {
            $index = array_search($moduleItem->getId(), $selectedModules);

            $moduleItem->setPositionIndex($index);
        }

        $this->manager()->flush();

        $this->addFlash(Flash::SUCCESS, $this->translator->trans('The order of modules was updated.', [], 'messages'));

        return $this->redirectToRoute('admin.pages.manage_content', ['id' => $page->getId()]);
    }

    private function extractFormClass(Module $module): string
    {
        $formClass = preg_split("/[\s,]+/", $module->getName());
        $formClass = array_map(
            function ($item) {
                return ucfirst($item);
            },
            $formClass
        );
        $formClass = 'App\\Module\\Form\\' . implode('', $formClass) . 'Type';

        return $formClass;
    }

    private function extractFormTemplate(Module $module): string
    {
        $formTemplate = preg_split("/[\s,]+/", $module->getName());
        $formTemplate = array_map(
            function ($item) {
                return strtolower($item);
            },
            $formTemplate
        );

        $formTemplate = '/modules/parameters/' . implode('_', $formTemplate) . '.html.twig';

        return $formTemplate;
    }

    private function getDataForLanguage($module, string $locale)
    {
        $dataArray = unserialize($module->getData());

        return $dataArray[$locale];
    }

    private function setDataForLanguage(ModuleItem $moduleItem, $data, string $locale)
    {
        if ($moduleItem->getData()) {
            $dataArray = unserialize($moduleItem->getData());
        } else {
            $dataArray = unserialize($moduleItem->getModule()->getData());
        }

        $dataArray[$locale] = $data;
        $moduleItem->setData(serialize($dataArray));
    }

    private function handleForm(ModuleItem $moduleItem, FormInterface $form, Request $request, string $language)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->setDataForLanguage($moduleItem, $form->getData(), $language);

            $this->manager()->flush();
            $this->addFlash(Flash::SUCCESS, $this->translator->trans('The module was updated.', [], 'messages'));

            return $this->redirectToRoute('page_item.configure', ['id' => $moduleItem->getId(), '_fragment' => $language]);
        }

        return null;
    }
}
