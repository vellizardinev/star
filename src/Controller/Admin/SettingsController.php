<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Setting;
use App\Enum\Settings;
use App\Settings\SettingsValidation;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * Class SettingsController
 * @package App\Controller\Admin
 * @Route("/admin/settings")
 * @\Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted("ROLE_SETTINGS")
 */
class SettingsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * SettingsController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.settings.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedSettings = $paginator->paginate(
            $this->manager()->getRepository(Setting::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbsForIndex();

        return $this->render(
            'admin/settings/index.html.twig',
            [
                'settings' => $paginatedSettings,
            ]
        );
    }

    /**
     * @Route("/update/{setting}", name="admin.settings.update", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @param Request $request
     * @param $setting
     * @param ValidatorInterface $validator
     * @param SettingsValidation $rules
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Request $request, $setting, ValidatorInterface $validator, SettingsValidation $rules)
    {
        $settingEntity = $this->manager()->getRepository(Setting::class)->findOneBy(['name' => $setting]);

        if (!$settingEntity) {
            return new JsonResponse(null, 404);
        }

        $value = $request->request->get('value', null);

        $errors = $validator->validate($value, $rules->get($setting));

        if ($errors->count() > 0) {
            return $this->json($errors, 422);
        }

        $settingEntity->setValue($value);
        $this->manager()->persist($settingEntity);
        $this->manager()->flush();

        return $this->json(null, 200);
    }

	/**
	 * @Route("/clear-cache", name="admin.settings.clear_cache")
	 * @param KernelInterface $kernel
	 * @return Response
	 * @throws Exception
	 */
	public function clearCache(KernelInterface $kernel)
	{
		$application = new Application($kernel);
		$application->setAutoExit(false);

		$input = new ArrayInput([
			'command' => 'cache:clear'
		]);

		$output = new BufferedOutput();
		$application->run($input, $output);

		return $this->redirectToRoute('admin.settings.index');
	}


	private function breadcrumbsForIndex()
    {
        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.settings', [], 'text'), $this->generateUrl('admin.settings.index'));
    }
}
