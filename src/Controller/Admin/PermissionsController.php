<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Permission\Permission;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\PermissionType;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class PermissionsController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/permissions")
 */
class PermissionsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * PermissionsController constructor.
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="admin.permissions.index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $paginatedPermissions = $paginator->paginate(
            $this->manager()->getRepository(Permission::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbsForIndex();

        return $this->render(
            'admin/permissions/index.html.twig',
            [
                'permissions' => $paginatedPermissions,
            ]
        );
    }

    /**
     * @Route("/create", name="admin.permissions.create", methods={"GET", "POST"})
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $permission = new Permission;

        $form = $this->createForm(PermissionType::class, $permission);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($permission);
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('Permission was created.', [], 'messages'));

            return $this->redirectToRoute('admin.permissions.index');
        }

        $this->breadcrumbsForCreate();

        return $this->render('admin/permissions/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/update", name="admin.permissions.update", methods={"GET", "POST"})
     * @param Request $request
     *
     * @param Permission $permission
     * @return RedirectResponse|Response
     */
    public function update(Request $request, Permission $permission)
    {
        $form = $this->createForm(PermissionType::class, $permission);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($permission);
            $this->manager()->flush();

	        $this->addFlash(Flash::SUCCESS, $this->translator->trans('Permission was updated.', [], 'messages'));

            return $this->redirectToRoute('admin.permissions.index');
        }

        $this->breadcrumbsForUpdate();

        return $this->render('admin/permissions/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function breadcrumbsForIndex()
    {
        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.permissions', [], 'text'), $this->generateUrl('admin.permissions.index'));
    }

    private function breadcrumbsForUpdate()
    {
        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.update_permission', [], 'text'));
    }

    private function breadcrumbsForCreate()
    {
        $this->breadcrumbs->addItem($this->translator->trans('title.administration', [], 'text'), $this->generateUrl('admin.dashboard'));
        $this->breadcrumbs->addItem($this->translator->trans('title.create_permission', [], 'text'));
    }
}
