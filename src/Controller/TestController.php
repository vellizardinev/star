<?php

namespace App\Controller;

use App\Service\MobileApi\Customers;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends BaseController
{
    /**
     * @Route("test")
     * @param Customers $customers
     * @return Response
     */
    public function test(Customers $customers)
    {
        $customers->IsEmailConfirmed($this->getUser());
    }
}
