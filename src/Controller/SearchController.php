<?php

namespace App\Controller;

use App\Entity\Credit;
use App\Entity\Department;
use App\Entity\Faq;
use App\Entity\JobOpening;
use App\Entity\News;
use App\Entity\Office;
use App\Entity\Promotion;
use App\Entity\Searchable;
use App\Enum\Settings;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;


/**
 * Class SearchController
 */
class SearchController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     * @Route({"en": "/search", "ua": "/poshuk", "ru": "/search"}, name="search.index", methods={"GET"})
     */
    public function searchIndex(PaginatorInterface $paginator, Request $request)
    {
	    $term = trim($request->get('term'));

	    $results = [];

	    $results = array_merge($results, $this->manager()->getRepository(News::class)->search($term));
	    $results = array_merge($results, $this->manager()->getRepository(Credit::class)->search($term));
	    $results = array_merge($results, $this->manager()->getRepository(JobOpening::class)->search($term));
	    $results = array_merge($results, $this->manager()->getRepository(Promotion::class)->search($term));
	    $results = array_merge($results, $this->manager()->getRepository(Office::class)->search($term));
	    $results = array_merge($results, $this->manager()->getRepository(Department::class)->search($term));
	    $results = array_merge($results, $this->manager()->getRepository(Faq::class)->search($term));

	    foreach ($results as $result) {
		    /** @var $result Searchable */
		    $result->url = $this->generateUrl(...$result->urlContext());
	    }

        $paginatedResults = $paginator->paginate(
            $results,
            $request->query->getInt('page', 1),
            $this->getSetting(Settings::PAGINATION_PER_PAGE)
        );

        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.home', [], 'text'),
            $this->generateUrl('static_pages.home_page')
        );

        $this->breadcrumbs->addItem($this->translator->trans('public.title.search', [], 'text'), null);

        return $this->render('search/index.html.twig', ['results' => $paginatedResults]);
    }
}
