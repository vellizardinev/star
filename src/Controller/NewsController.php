<?php

namespace App\Controller;

use App\Annotation\SiteMap;
use App\Entity\News;
use App\Enum\NewsCategories;
use Doctrine\ORM\NonUniqueResultException;
use Knp\Component\Pager\PaginatorInterface;
use ReflectionException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class NewsController
 * @package App\Controller
 * @Route({"en": "/news", "ua": "/novyny", "ru": "/news"})
 */
class NewsController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="news.index")
     * @SiteMap()
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     * @throws ReflectionException
     * @throws NonUniqueResultException
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $paginatedNews = $paginator->paginate(
            $this->manager()->getRepository(News::class)->filtered($request->query->all(), true, true),
            $request->query->getInt('page', 1),
            $request->query->get('limit') ?? 9
            );

        $this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
        $this->breadcrumbs->addItem($this->translator->trans('title.news', [], 'text'));

        return $this->render('news/index.html.twig', [
            'newsItems' => $paginatedNews,
            'categories' => NewsCategories::toTranslationKeys(),
            'featuredNews' => $this->manager()->getRepository(News::class)->featured($request->query->get('category', NewsCategories::NEWS)),
        ]);
    }

    /**
     * @param News $news
     * @return Response
     * @throws NonUniqueResultException
     * @Route("/{slug}", name="news.show")
     * @Entity("news", expr="repository.findbySlug(slug)")
     */
    public function show(News $news)
    {
        if ($news->isVisible() === false) {
            return $this->redirectToRoute('news.index');
        }

	    $newsRepository = $this->manager()->getRepository(News::class);
	    $previousNews = $newsRepository->getSibling($news);
	    $nextNews = $newsRepository->getSibling($news, false);

        $this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
        $this->breadcrumbs->addItem($this->translator->trans('title.news', [], 'text'), $this->generateUrl('news.index'));
        $this->breadcrumbs->addItem($this->translator->trans('news.article', [], 'text'));

        return $this->render('news/show.html.twig', [
        	'news' => $news,
	        'previousNews' => $previousNews,
	        'nextNews' => $nextNews
        ]);
    }
}
