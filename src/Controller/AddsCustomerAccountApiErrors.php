<?php


namespace App\Controller;


use App\Enum\CreateUserAccountErrors;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

trait AddsCustomerAccountApiErrors
{
    public function addApiError(FormInterface $form, ?array $validationErrors, TranslatorInterface $translator)
    {
        if (empty($validationErrors)) {
            return;
        }

        foreach ($validationErrors as $error) {
            switch ($error) {
                case CreateUserAccountErrors::ERROR_EGN_EXISTS:
                    $form->get('personalIdentificationNumber')->addError(new FormError(
                        $translator->trans('error_egn_exists', [], 'validators')
                    ));
                    break;
                case CreateUserAccountErrors::ERROR_EMAIL_EXISTS:
                    $form->get('email')->addError(new FormError(
                        $translator->trans('error_email_exists', [], 'validators')
                    ));
                    break;
                case CreateUserAccountErrors::ERROR_INVALID_PASSWORD:
                    $form->get('plainPassword')->get('first')->addError(new FormError(
                        $translator->trans('error_invalid_password', [], 'validators')
                    ));
                    break;
                case CreateUserAccountErrors::ERROR_INVALID_EGN:
                    $form->get('personalIdentificationNumber')->addError(new FormError(
                        $translator->trans('error_invalid_egn', [], 'validators')
                    ));
                    break;
                case CreateUserAccountErrors::ERROR_INVALID_TELEPHONE:
                    $form->get('telephone')->addError(new FormError(
                        $translator->trans('error_invalid_telephone', [], 'validators')
                    ));
                    break;
                case CreateUserAccountErrors::EXISTING_REGISTRATIONW_WITH_PHONE_NUMBER:
                    $form->get('telephone')->addError(new FormError(
                        $translator->trans('error_existing_registration_with_phone_number', [], 'validators')
                    ));
                    break;
                case CreateUserAccountErrors::EMAIL_IS_IN_BLACKLISTED_DOMAINS:
                    $form->get('email')->addError(new FormError(
                        $translator->trans('error_email_is_in_blacklisted_domains', [], 'validators')
                    ));
                    break;
                default :
                    break;
            }
        }
    }
}