<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @package App\Controller
 * @Route("/frontend-validation")
 */
class FrontendValidationController extends BaseController
{
    /**
     * @Route("/validate", name="frontend_validation.validate", methods={"POST"}, condition="request.isXmlHttpRequest()")
     * @param ValidatorInterface $validator
     * @param Request $request
     * @return Response
     */
    public function validate(ValidatorInterface $validator, Request $request)
    {
        $property = $request->request->get('property', null);
        $entity = $request->request->get('entity', null);
        $value = $request->request->get('value');

        if (empty($property) || empty($entity)) {
            return new JsonResponse(['errors' => ['Validation failed. Missing "property" and/or "entity" data.']], 400);
        }

        if (!class_exists($entity)) {
            return new JsonResponse(['errors' => [
                sprintf(
                    'Class \'%s\' does not exist.',
                    $entity
                )
            ]], 400);
        }

        $errors = $validator->validatePropertyValue($entity, $property, $value);

        if ($errors->count() === 0) {
            return new JsonResponse();
        }

        $errorsData = [];

        foreach ($errors as $error) {
            $errorsData[] = $error->getMessage();
        }

        return new JsonResponse(['errors' => $errorsData], 400);
    }
}
