<?php

namespace App\Controller;

use App\Annotation\SiteMap;
use App\DataTransferObject\ShortApplicationParameters;
use App\Entity\Affiliate;
use App\Entity\Credit;
use App\Entity\CreditData;
use App\Entity\CreditShortApplication;
use App\Entity\Customer;
use App\Entity\LandingPage;
use App\Enum\Flash;
use App\Enum\Settings;
use App\Form\CreditApplicationType;
use App\Service\MobileApi\SavePPZShort;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class CreditController
 * @package App\Controller
 * @Route({"en": "/credit", "ua": "/kredyty", "ru": "/credit"})
 */
class CreditController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="credits.index", methods={"GET"})
     * @SiteMap()
     * @param Credit|null $credit
     * @return Response
     */
    public function index(Credit $credit = null): Response
    {
        $credits = $this->manager()->getRepository(Credit::class)->filtered([])->getResult();

        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.home', [], 'text'),
            $this->generateUrl('static_pages.home_page')
        );
        $this->breadcrumbs->addItem($this->translator->trans('public.title.credits', [], 'text'));

        return $this->render('credit/index.html.twig', [
        	'credits' => $credits,
	        'expandedCredit' => $credit
        ]);
    }

    /**
     * @Route({"en": "/apply-now", "ua": "/apply-now", "ru": "/apply-now"}, name="credits.apply_now")
     * @param Request $request
     * @param SavePPZShort $savePPZShortService
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function applyNow(Request $request, SavePPZShort $savePPZShortService): Response
    {
        $customer = $this->getUser();

        $parameters = $this->getCreditParameters($request);

        if (null === $parameters || null === $customer || ! $customer instanceof Customer) {
            return $this->redirectToRoute('credits.credit_apply');
        }

        $application = (new CreditShortApplication)
            ->setCredit($parameters->getCredit())
            ->setName($customer->fullName())
            ->setTelephone($customer->getTelephone())
            ->setAmount($parameters->getAmount())
            ->setSource($this->translator->trans('website', [], 'call_flow'))
        ;

        $customer = $this->getCustomer();

        if ($customer) {
            $application->setPersonalIdentificationNumber($customer->getPersonalIdentificationNumber());
            $application->setEmail($customer->getEmail());
        }

        $isSent = $savePPZShortService->saveShortApplication($application, $customer);

        if ($isSent) {
            $application->setSentToErp(true);
            $this->manager()->persist($application);
            $this->manager()->flush();

            $this->addFlash(
                Flash::SUCCESS,
                $this->translator->trans(
                    'Your credit application was submitted. We will get back to you soon.',
                    [],
                    'messages'
                )
            );

            return $this->redirect($this->getSetting(Settings::CREDIT_APPLICATION_REDIRECT_PATH));
        } else {
            return $this->redirectToRoute('credits.credit_apply', ['telephoneApplicationIsDenied' => true]);
        }
    }

    /**
     * @Route({"en": "/apply/{slug}", "ua": "/pryiniaty/{slug}", "ru": "/apply/{slug}"}, name="credits.credit_apply", defaults={"slug": null})
     * @Entity("credit", expr="repository.findbySlug(slug)")
     * @param Request $request
     * @param SavePPZShort $savePPZShortService
     * @param Credit|null $credit
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function apply(Request $request, SavePPZShort $savePPZShortService, Credit $credit = null): Response
    {
        if (!$credit) {
            $credit = $this->manager()->getRepository(Credit::class)->findOneBy([]);
        }

        $isRefinance = $request->query->getBoolean('isRefinance', false);

        $application = new CreditShortApplication();
        $application->setIsRefinance($isRefinance);

        $amount = $request->query->get('amount', null);

        if (!$amount) {
            $amount = $credit->getDefaultAmount();
        }

        $application->setAmount($amount);

        $form = $this->createForm(CreditApplicationType::class, $application);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $application->setCredit($credit);

            $customer = $this->getCustomer();

            if ($customer) {
                $application->setPersonalIdentificationNumber($customer->getPersonalIdentificationNumber());
                $application->setEmail($customer->getEmail());
            }

            $isSent = $savePPZShortService->saveShortApplication($application);

            if ($isSent) {
                $application->setSentToErp(true);

                $this->manager()->persist($application);
                $this->manager()->flush();

                $this->addFlash(
                    Flash::SUCCESS,
                    $this->translator->trans(
                        'Your credit application was submitted. We will get back to you soon.',
                        [],
                        'messages'
                    )
                );

                return $this->redirect($this->getSetting(Settings::CREDIT_APPLICATION_REDIRECT_PATH));
            } else {
                $telephoneApplicationIsDenied = true;
            }
        }

        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.home', [], 'text'),
            $this->generateUrl('static_pages.home_page')
        );
        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.credits', [], 'text'),
            $this->generateUrl('credits.index')
        );
        $this->breadcrumbs->addItem(
            $credit->getTitle(),
            $this->generateUrl('credits.show', ['slug' => $credit->getSlug()])
        );
        $this->breadcrumbs->addItem($this->translator->trans('public.title.apply', [], 'text'));

        if (!$this->getUser() instanceof Customer) {
            return $this->render(
                'credit/short_application.html.twig',
                [
                    'form' => $form->createView(),
                    'credit' => $credit,
                    'telephoneApplicationIsDenied' => isset($telephoneApplicationIsDenied) ?
                        $telephoneApplicationIsDenied : $request->query->getInt('telephoneApplicationIsDenied', 0) === 1
                ]
            );
        }

        return $this->render(
            'credit/customer_credit_application.html.twig',
            [
                'form' => $form->createView(),
                'credit' => $credit,
                'telephoneApplicationIsDenied' => isset($telephoneApplicationIsDenied) ?
                    $telephoneApplicationIsDenied : $request->query->getInt('telephoneApplicationIsDenied', 0) === 1
            ]
        );
    }

    /**
     * @Route("/application-form/{code}", name="credits.application_form", methods={"GET", "POST"})
     * @param Request $request
     * @param SavePPZShort $savePPZShortService
     * @param Environment $twig
     * @param string|null $code
     * @return RedirectResponse|Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws LoaderError
     * @throws RedirectionExceptionInterface
     * @throws RuntimeError
     * @throws ServerExceptionInterface
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
	public function applicationForm(Request $request, SavePPZShort $savePPZShortService, Environment $twig, ?string $code = null): Response
	{
		$affiliate = null;
		$customCss = null;

		if ($code) {
			$affiliate = $this->manager()->getRepository(Affiliate::class)->findOneBy(['code' => $code]);

			if ($affiliate) {
				$customCss = $affiliate->getCustomCSS();
			}
		}

		$creditShortApplication = new CreditShortApplication();

        $form = $this->createForm(CreditApplicationType::class, $creditShortApplication, ['csrf_protection' => $affiliate === null]);

		$form->handleRequest($request);

        if ($request->getMethod() === 'POST') {
            if ($form->isValid()) {
                $this->processLandingPage($request, $creditShortApplication);
                $this->processAffiliate($creditShortApplication, $affiliate);

                $isSent = $savePPZShortService->saveShortApplication($creditShortApplication);

                if ($isSent) {
                    $creditShortApplication->setSentToErp(true);
                    $this->manager()->persist($creditShortApplication);
                    $this->manager()->flush();

                    return $this->json(
                        [
                            'success' => true,
                            'content' => $twig->render('credit/application_success.html.twig', ['customCss' => $customCss]),
                        ],
                        200
                    );
                } else {
                    $formContent = $twig->render(
                        'credit/application_failure.html.twig',
                        [
                            'form' => $form->createView(),
                            'customCss' => $customCss,
                        ]
                    );

                    return $this->json(['success' => false, 'content' => $formContent], 200);
                }
            } else {
                $formContent = $twig->render(
                    'credit/_application_form.html.twig',
                    [
                        'form' => $form->createView(),
                        'customCss' => $customCss,
                    ]
                );

                return $this->json(['success' => false, 'content' => $formContent], 200);
            }
        } else {
            return $this->render(
                'credit/iframe_application_form.html.twig',
                [
                    'form' => $form->createView(),
                    'code' => $code,
                    'customCss' => $customCss,
                ]
            );
        }
	}

	private function processLandingPage(Request $request, CreditShortApplication $application)
	{
		$landingId = $request->query->get('landing', null);

		if ($landingId) {
			$landingPage = $this->manager()->getRepository(LandingPage::class)->find($landingId);
			$application->setLanding($landingPage);
		}
	}

	private function processAffiliate(CreditShortApplication $application, ?Affiliate $affiliate = null)
	{
		if ($affiliate) {
			$application->setAffiliate($affiliate);
		}
	}

	/**
	 * @Route("/{slug}", name="credits.show", methods={"GET"})
     * @Entity("credit", expr="repository.findbySlug(slug)")
	 * @param Credit $credit
	 * @return Response
	 */
	public function show(Credit $credit): Response
	{
		$this->breadcrumbs->addItem(
			$this->translator->trans('public.title.home', [], 'text'),
			$this->generateUrl('static_pages.home_page')
		);
		$this->breadcrumbs->addItem($this->translator->trans('public.title.credits', [], 'text'), $this->generateUrl('credits.index'));
		$this->breadcrumbs->addItem($credit->getTitle());

		return $this->render('credit/show.html.twig', [
			'credit' => $credit,
		]);
	}

    private function getCreditParameters(Request $request): ?ShortApplicationParameters
    {
        $amount = $request->query->get('amount', null);
        $creditName = $request->query->get('credit', null);

        // If any of the 2 parameters is missing, return the default credit parameters
        if (empty($amount) || empty($creditName)) {
            /** @var Credit $credit */
            $credit = $this->manager()
                ->getRepository(Credit::class)
                ->findOneBy([]);

            $defaults = $this->manager()
                ->getRepository(CreditData::class)
                ->getDefaults($credit);

            return new ShortApplicationParameters($credit, $defaults->getAmount());
        }

        // If all 2 parameters are present, check if they are valid
        $credit = $this->manager()
            ->getRepository(Credit::class)
            ->findOneBy(['title' => $creditName]);

        if (!$credit) {
            return null;
        }

        $creditData = $this->manager()
            ->getRepository(CreditData::class)
            ->findOneBy(['credit' => $credit, 'amount' => $amount]);

        if (!$creditData) {
            return null;
        }

        return new ShortApplicationParameters($credit, $amount);
    }

    private function getCustomer(): ?Customer
    {
        $user = $this->getUser();

        if ($user instanceof Customer) {
            return $user;
        }

        return null;
    }
}
