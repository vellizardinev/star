<?php

namespace App\Controller;

use App\Annotation\SiteMap;
use App\Entity\Gift;
use App\Entity\LoyaltyPartner;
use App\Enum\ItemsPerPage;
use App\Enum\Sort;
use Knp\Component\Pager\PaginatorInterface;
use ReflectionException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class LoyaltyProgramController
 * @package App\Controller
 * @Route({"en": "/loyalty-program", "ua": "/prohrama-loialnosti", "ru": "/loyalty-program"})
 */
class LoyaltyProgramController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="loyalty_program.index")
     * @SiteMap()
     * @return Response
     */
    public function index()
    {
        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.home', [], 'text'),
            $this->generateUrl('static_pages.home_page')
        );
        $this->breadcrumbs->addItem($this->translator->trans('public.title.loyalty_program', [], 'text'));

        return $this->render('loyalty_program/index.html.twig');
    }

    /**
     * @param Request $request
     * @return Response
     * @Route({"en": "/partners-and-discounts", "ua": "/partners-and-discounts", "ru": "/partners-and-discounts"}, name="loyalty_program.partners.index")
     */
    public function partners(Request $request)
    {
        // Page will not be available at launch of the site
        throw $this->createNotFoundException();

        $partners = $this->manager()->getRepository(LoyaltyPartner::class)->filtered($request->query->all(), true)->getResult();

        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.home', [], 'text'),
            $this->generateUrl('static_pages.home_page')
        );
        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.loyalty_program', [], 'text'),
            $this->generateUrl('loyalty_program.index')
        );
        $this->breadcrumbs->addItem($this->translator->trans('public.title.partners_and_discounts', [], 'text'), null);

        return $this->render('loyalty_program/partners_and_discounts/index.html.twig', ['partners' => $partners]);
    }

    /**
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     * @throws ReflectionException
     * @Route({"en": "/gifts", "ua": "/podarunky", "ru": "/gifts"}, name="loyalty_program.gifts")
     * @SiteMap()
     */
    public function gifts(PaginatorInterface $paginator, Request $request)
    {
        $paginatedGifts = $paginator->paginate(
            $this->manager()->getRepository(Gift::class)->filtered($request->query->all()),
            $request->query->getInt('page', 1),
            $request->query->get('limit', ItemsPerPage::getValue('ITEMS_PER_PAGE_9'))
        );

        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.home', [], 'text'),
            $this->generateUrl('static_pages.home_page')
        );
        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.loyalty_program', [], 'text'),
            $this->generateUrl('loyalty_program.index')
        );
        $this->breadcrumbs->addItem($this->translator->trans('public.title.gifts', [], 'text'), null);

        return $this->render(
            'loyalty_program/gifts/gifts.html.twig',
            [
                'gifts' => $paginatedGifts,
                'sortOptions' => Sort::toTranslationKeys(),
            ]
        );
    }

    /**
     * @return Response
     * @Route({"en": "/i-bonus", "ua": "/i-bonus", "ru": "/i-bonus"}, name="loyalty_program.i_bonus")
     * @SiteMap()
     */
    public function iBonus()
    {
        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.home', [], 'text'),
            $this->generateUrl('static_pages.home_page')
        );
        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.loyalty_program', [], 'text'),
            $this->generateUrl('loyalty_program.index')
        );
        $this->breadcrumbs->addItem($this->translator->trans('public.title.i_bonus', [], 'text'), null);

        return $this->render('loyalty_program/static_pages/i_bonus.html.twig');
    }
}
