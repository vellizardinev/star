<?php

namespace App\Controller;

use App\Annotation\NeedsActiveCustomerAccount;
use App\Annotation\RedirectToCorrectOnlineApplicationStep;
use App\Entity\Credit;
use App\Entity\CreditData;
use App\Entity\CreditLongApplication;
use App\Entity\Customer;
use App\Entity\File;
use App\Entity\OnlineApplication\AdditionalInfo;
use App\Entity\OnlineApplication\Address;
use App\Entity\OnlineApplication\Confirmation;
use App\Entity\OnlineApplication\ContactPerson;
use App\Entity\OnlineApplication\CreditDetails;
use App\Entity\OnlineApplication\Expenses;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Entity\OnlineApplication\PersonalInfo;
use App\Entity\OnlineApplication\Property;
use App\Entity\OnlineApplication\UploadedDocuments;
use App\Entity\OnlineApplication\UserPaymentInstrument;
use App\Entity\OnlineApplication\WorkDetails;
use App\Enum\Flash;
use App\Enum\MethodsToReceiveCredit;
use App\Enum\PassportTypes;
use App\Enum\PensionCertificateTypes;
use App\Enum\Settings;
use App\File\FileManager;
use App\Form\CreditLongApplicationType;
use App\Form\OnlineApplication\AddressType;
use App\Form\OnlineApplication\ConfirmationType;
use App\Form\OnlineApplication\ContactPersonType;
use App\Form\OnlineApplication\CreditDetailsType;
use App\Form\OnlineApplication\PersonalInfoDocumentsType;
use App\Form\OnlineApplication\WorkDetailsAdditionalInfoType;
use App\Message\SendLongCreditApplication;
use App\Message\SendOnlineApplicationFileToApi;
use App\Message\SendOnlineApplicationToApi;
use App\Service\MobileApi\AllowCustomerToApplyOnline;
use App\Service\MobileApi\Customers;
use App\Service\MobileApi\GetCreditProposal;
use App\Service\MobileApi\UploadFile;
use Carbon\Carbon;
use Exception;
use League\Flysystem\FileExistsException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @package App\Controller
 * @Route("apply-online")
 */
class OnlineApplicationController extends BaseController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     */
    public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("/{slug}", name="online_application.apply")
     * @param Request $request
     * @param Credit $credit
     * @param MessageBusInterface $messageBus
     * @param FileManager $fileManager
     * @return RedirectResponse|Response
     * @throws FileExistsException
     */
    public function applyOnline(
        Request $request,
        Credit $credit,
        MessageBusInterface $messageBus,
        FileManager $fileManager
    ): Response {
//        return $this->createNotFoundException();

        $application = new CreditLongApplication();

        [$amount, $period, $payment] = $this->getParameters($credit, $request);

        if (!$amount) {
            $amount = $credit->getDefaultAmount();
        }

        $application
            ->setAmount($amount)
            ->setProduct($credit->getTitle())
            ->setPeriod($period)
            ->setInstalmentAmount($payment);

        $form = $this->createForm(
            CreditLongApplicationType::class,
            $application,
            [
                'action' => $this->generateUrl(
                    'online_application.apply',
                    [
                        'slug' => $credit->getSlug(),
                        'amount' => $amount,
                        'payment' => $payment,
                        'period' => $period,
                    ]
                ),
            ]
        );

        $form->handleRequest($request);

        $formIsValid = true;

        if ($form->isSubmitted() && $formIsValid = $form->isValid()) {
            $this->manager()->persist($application);

            $this->uploadFiles($application, $fileManager);

            $this->manager()->flush();

            $messageBus->dispatch(new SendLongCreditApplication($application->getId()));

            $this->addFlash(
                Flash::SUCCESS,
                $this->translator->trans(
                    'Your credit application was submitted. We will get back to you soon.',
                    [],
                    'messages'
                )
            );

            return $this->redirect($this->getSetting(Settings::ONLINE_CREDIT_APPLICATION_REDIRECT_PATH));
        }

        $this->breadcrumbs($credit);

        return $this->render(
            'credit/long_application.html.twig',
            [
                'form' => $form->createView(),
                'credit' => $credit,
                'formIsValid' => $formIsValid,
            ]
        );
    }

    /**
     * @Route("/{slug}/credit-details/{isRefinance}", name="online_application.credit_details", methods={"GET", "POST"})
     * @NeedsActiveCustomerAccount()
     * @param Request $request
     * @param Credit $credit
     * @param GetCreditProposal $creditProposalService
     * @param Customers $customersService
     * @param bool $checkCustomerAllowedOnlineApplication
     * @param \App\Service\MobileApi\AllowCustomerToApplyOnline $allowCustomerToApplyOnline
     * @param bool|null $isRefinance
     * @return Response
     * @throws \ReflectionException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function creditDetails(
        Request $request,
        Credit $credit,
        GetCreditProposal $creditProposalService,
        Customers $customersService,
        bool $checkCustomerAllowedOnlineApplication,
        AllowCustomerToApplyOnline $allowCustomerToApplyOnline,
        ?bool $isRefinance = false
    ): Response
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        if ($checkCustomerAllowedOnlineApplication === true) {
            if  ($allowCustomerToApplyOnline->isDenied($customer)) {
                throw new UnauthorizedHttpException('Unauthorized.');
            }
        }

        $applicationHash = $request->query->get('application', null);

        if ($applicationHash) {
            $application = $this->manager()->getRepository(OnlineApplication::class)->findOneBy(
                ['hash' => $applicationHash]
            );
            $creditDetails = $application->getCreditDetails();
            $confirmedAccuracyOfCardData = $creditDetails->getConfirmedAccuracyOfCardData();
            $amount = $creditDetails->getAmount();
            $period = $creditDetails->getPeriod();
            $payment = $creditDetails->getInstalmentAmount();
            $availablePaymentInstruments = $creditDetails->getPaymentInstruments()->toArray();

            $action = $this->generateUrl(
                'online_application.credit_details',
                [
                    'slug' => $credit->getSlug(),
                    'application' => $application->getHash(),
                ]
            );
        } else {
            $application = new OnlineApplication();
            $application->setIsRefinance($isRefinance);
            $creditDetails = new CreditDetails();
            $confirmedAccuracyOfCardData = $request->request->has('confirmedAccuracyOfCardData');

            [$amount, $period, $payment] = $this->getParameters($credit, $request);

            if (!$amount) {
                $amount = $credit->getDefaultAmount();
            }

            $creditDetails
                ->setAmount($amount)
                ->setProduct($credit->getTitle())
                ->setPeriod($period)
                ->setInstalmentAmount($payment);

            $action = $this->generateUrl(
                'online_application.credit_details',
                [
                    'isRefinance' => $isRefinance,
                    'slug' => $credit->getSlug(),
                    'amount' => $amount,
                    'payment' => $payment,
                    'period' => $period,
                ]
            );

            $paymentInstrumentsResponse = $customersService->getPaymentInstruments($customer);
            $availablePaymentInstruments = $paymentInstrumentsResponse->getUserPaymentInstruments();

            if ($paymentInstrumentsResponse->isSuccess()) {
                foreach ($availablePaymentInstruments as $instrument) {
                    /** @var UserPaymentInstrument $instrument */
                    $creditDetails->addPaymentInstrument($instrument);
                }
            }
        }

        $form = $this->createForm(CreditDetailsType::class, $creditDetails, [
            'action' => $action,
            'availablePaymentInstruments' => $availablePaymentInstruments,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Customer $customer */
            $customer = $this->getUser();
            $response = $creditProposalService->getCreditProposal($customer);

            if ($response->isSuccess()) {
                $application->setPreviousApplicationData($response->getData());
            }

            $this->manager()->persist($application);

            $this->manager()->persist($creditDetails);
            $application->setCreditDetails($creditDetails);
            $creditDetails->setOnlineApplication($application);

            if ($creditDetails->getHowShouldMoneyBeReceived() === MethodsToReceiveCredit::BANK_ACCOUNT) {
                $creditDetails->setUpdatedAt(Carbon::now());
                $creditDetails->setConfirmedAccuracyOfCardData($request->request->has('confirmedAccuracyOfCardData'));
            }

            if ($application->getIncomesAndExpenses() === null) {
                $expenses = new Expenses();
                $expenses->setOnlineApplication($application);
                $application->setIncomesAndExpenses($expenses);
                $this->manager()->persist($expenses);
            }

            if ($application->getProperty() === null) {
                $property = new Property();
                $property->setOnlineApplication($application);
                $application->setProperty($property);
                $this->manager()->persist($property);
            }

            $this->manager()->flush();

            if ($request->request->has('updateAndFinalise')) {
                return $this->redirectToRoute('online_application.confirmation', ['hash' => $application->getHash()]);
            } else {
                return $this->redirectToRoute('online_application.personal_info', ['hash' => $application->getHash()]);
            }
        }

        $this->breadcrumbs($credit);

        return $this->render(
            'online_application/credit_details.html.twig',
            [
                'form' => $form->createView(),
                'credit' => $credit,
                'amount' => $amount,
                'period' => $period,
                'payment' => $payment,
                'application' => $application,
                'confirmedAccuracyOfCardData' => $confirmedAccuracyOfCardData,
            ]
        );
    }

    /**
     * @Route("/{hash}/personal-info", name="online_application.personal_info", methods={"GET", "POST"})
     * @NeedsActiveCustomerAccount()
     * @RedirectToCorrectOnlineApplicationStep()
     * @param Request $request
     * @param OnlineApplication $application
     * @param FileManager $fileManager
     * @return Response
     * @throws FileExistsException
     */
    public function personalInfo(Request $request, OnlineApplication $application, FileManager $fileManager): Response
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        $credit = $this->manager()
            ->getRepository(Credit::class)
            ->findOneBy(['title' => $application->getCreditDetails()->getProduct()]);

        $personalInfo = $application->getPersonalInfo() === null ?
            PersonalInfo::fromPreviousApplicationData($application->getPreviousApplicationData()) : $application->getPersonalInfo();

        $personalInfo->setFirstName($customer->getFirstName());
        $personalInfo->setMiddleName($customer->getMiddleName());
        $personalInfo->setLastName($customer->getLastName());
        $personalInfo->setMobileTelephone($customer->getTelephone());
        $personalInfo->setPersonalIdentificationNumber($customer->getPersonalIdentificationNumber());
        $personalInfo->setEmail($customer->getEmail());
        $application->setPersonalInfo($personalInfo);
        $personalInfo->setOnlineApplication($application);

        if ($application->getUploadedDocuments() instanceof UploadedDocuments) {
            $uploadedDocuments = $application->getUploadedDocuments();
        } else {
            $uploadedDocuments = new UploadedDocuments();
            $this->manager()->persist($uploadedDocuments);
            $uploadedDocuments->setOnlineApplication($application);
        }

        $form = $this->createForm(PersonalInfoDocumentsType::class, $application);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($personalInfo);
            $this->manager()->persist($uploadedDocuments);

            $this->uploadFilesForOnlineApplication($uploadedDocuments, $fileManager);

            $this->manager()->flush();

            if ($request->request->has('updateAndFinalise')) {
                return $this->redirectToRoute('online_application.confirmation', ['hash' => $application->getHash()]);
            } else {
                return $this->redirectToRoute(
                    'online_application.address',
                    ['hash' => $application->getHash()]
                );
            }
        }

        $this->breadcrumbs($credit);

        return $this->render(
            'online_application/personal_info.html.twig',
            [
                'form' => $form->createView(),
                'credit' => $credit,
                'application' => $application,
                'uploadedDocuments' => $uploadedDocuments,
            ]
        );
    }

    /**
     * @Route("/{hash}/address", name="online_application.address", methods={"GET", "POST"})
     * @NeedsActiveCustomerAccount()
     * @RedirectToCorrectOnlineApplicationStep()
     * @param Request $request
     * @param OnlineApplication $application
     * @return Response
     */
    public function address(Request $request, OnlineApplication $application): Response
    {
        $credit = $this->manager()
            ->getRepository(Credit::class)
            ->findOneBy(['title' => $application->getCreditDetails()->getProduct()]);

        $address = $application->getAddress() === null ?
            Address::fromPreviousApplicationData($application->getPreviousApplicationData()) : $application->getAddress();

        $form = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($address);

            $application->setAddress($address);
            $address->setOnlineApplication($application);

            $this->manager()->flush();

            if ($request->request->has('updateAndFinalise')) {
                return $this->redirectToRoute('online_application.confirmation', ['hash' => $application->getHash()]);
            } else {
                return $this->redirectToRoute('online_application.contact_person', ['hash' => $application->getHash()]);
            }

        }

        $this->breadcrumbs($credit);

        return $this->render(
            'online_application/address.html.twig',
            [
                'form' => $form->createView(),
                'credit' => $credit,
                'application' => $application,
            ]
        );
    }

    /**
     * @Route("/{hash}/contact-person", name="online_application.contact_person", methods={"GET", "POST"})
     * @NeedsActiveCustomerAccount()
     * @RedirectToCorrectOnlineApplicationStep()
     * @param Request $request
     * @param OnlineApplication $application
     * @return Response
     */
    public function contactPerson(Request $request, OnlineApplication $application): Response
    {
        $credit = $this->manager()
            ->getRepository(Credit::class)
            ->findOneBy(['title' => $application->getCreditDetails()->getProduct()]);

        $contactPerson = $application->getContactPerson() === null ?
            ContactPerson::fromPreviousApplicationData($application->getPreviousApplicationData()) : $application->getContactPerson();

        $form = $this->createForm(ContactPersonType::class, $contactPerson);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($contactPerson);
            $application->setContactPerson($contactPerson);
            $contactPerson->setOnlineApplication($application);

            $this->manager()->flush();

            if ($request->request->has('updateAndFinalise')) {
                return $this->redirectToRoute('online_application.confirmation', ['hash' => $application->getHash()]);
            } else {
                return $this->redirectToRoute(
                    'online_application.additional_info',
                    ['hash' => $application->getHash()]
                );
            }
        }

        $this->breadcrumbs($credit);

        return $this->render(
            'online_application/contact_person.html.twig',
            [
                'form' => $form->createView(),
                'credit' => $credit,
                'application' => $application,
            ]
        );
    }

    /**
     * @Route("/{hash}/additional-info", name="online_application.additional_info", methods={"GET", "POST"})
     * @NeedsActiveCustomerAccount()
     * @RedirectToCorrectOnlineApplicationStep()
     * @param Request $request
     * @param OnlineApplication $application
     * @return Response
     */
    public function additionalInfo(Request $request, OnlineApplication $application): Response
    {
        $credit = $this->manager()
            ->getRepository(Credit::class)
            ->findOneBy(['title' => $application->getCreditDetails()->getProduct()]);

        $workDetails = $application->getWorkDetails() === null ?
            WorkDetails::fromPreviousApplicationData($application->getPreviousApplicationData()) : $application->getWorkDetails();
        $workDetails->setOnlineApplication($application);

        $additionalInfo = $application->getAdditionalInfo() === null ?
            AdditionalInfo::fromPreviousApplicationData($application->getPreviousApplicationData()) : $application->getAdditionalInfo();
        $additionalInfo->setOnlineApplication($application);

        $form = $this->createForm(WorkDetailsAdditionalInfoType::class, $application);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager()->persist($workDetails);
            $this->manager()->persist($additionalInfo);

            $this->manager()->flush();

            if ($request->request->has('updateAndFinalise')) {
                return $this->redirectToRoute('online_application.confirmation', ['hash' => $application->getHash()]);
            } else {
                return $this->redirectToRoute('online_application.confirmation', ['hash' => $application->getHash()]);
            }

        }

        $this->breadcrumbs($credit);

        return $this->render(
            'online_application/additional_info.html.twig',
            [
                'form' => $form->createView(),
                'credit' => $credit,
                'application' => $application,
            ]
        );
    }

    /**
     * @Route("/{hash}/confirmation", name="online_application.confirmation", methods={"GET", "POST"})
     * @NeedsActiveCustomerAccount()
     * @RedirectToCorrectOnlineApplicationStep()
     * @param Request $request
     * @param OnlineApplication $application
     * @param MessageBusInterface $messageBus
     * @return Response
     */
    public function confirmation(
        Request $request,
        OnlineApplication $application,
        MessageBusInterface $messageBus
    ): Response {
        $credit = $this->manager()
            ->getRepository(Credit::class)
            ->findOneBy(['title' => $application->getCreditDetails()->getProduct()]);

        $confirmation = $application->getConfirmation() === null ? new Confirmation() : $application->getConfirmation();

        $form = $this->createForm(ConfirmationType::class, $confirmation);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $confirmation->setAgreedToProcessingOfPersonalData(true);

            $this->manager()->persist($confirmation);
            $application->setConfirmation($confirmation);
            $confirmation->setOnlineApplication($application);

            $application->setIsCompleted(true);

            $this->manager()->flush();

            $messageBus->dispatch(new SendOnlineApplicationToApi($application->getId()));

            $this->sendOnlineApplicationUploadedDocumentsToApi($application, $messageBus);

            $this->addFlash(
                Flash::SUCCESS,
                $this->translator->trans(
                    'Your credit application was submitted. We will get back to you soon.',
                    [],
                    'messages'
                )
            );

            return $this->redirect($this->getSetting(Settings::ONLINE_CREDIT_APPLICATION_REDIRECT_PATH));
        }

        $this->breadcrumbs($credit);

        return $this->render(
            'online_application/confirmation.html.twig',
            [
                'form' => $form->createView(),
                'credit' => $credit,
                'application' => $application,
            ]
        );
    }

    private function getParameters(Credit $credit, Request $request): array
    {
        $amount = $request->query->get('amount', null);
        $payment = $request->query->get('payment', null);
        $period = $request->query->get('period', null);

        // If any of the 3 parameters is missing, return the default parameters for the credit
        if (empty($amount) || empty($payment) || empty($period)) {
            $defaults = $this->manager()
                ->getRepository(CreditData::class)
                ->getDefaults($credit);

            return [$defaults->getAmount(), $defaults->getPeriods(), $defaults->getPayment()];
        }

        // If all 3 parameters are present, check if they are valid for the credit
        if ($this->manager()
            ->getRepository(CreditData::class)
            ->exists($credit, $amount, $payment, $period)) {
            // If they are valid, return them
            return [$amount, $period, $payment];
        } else {
            // If they are not valid, return the default parameters for the credit
            $defaults = $this->manager()
                ->getRepository(CreditData::class)
                ->getDefaults($credit);

            return [$defaults->getAmount(), $defaults->getPeriods(), $defaults->getPayment()];
        }
    }

    /**
     * @param CreditLongApplication $application
     * @param FileManager $fileManager
     * @throws FileExistsException
     */
    private function uploadFiles(CreditLongApplication $application, FileManager $fileManager)
    {
        if ($file = $application->oldStylePassportFirstPageUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setOldStylePassportFirstPage($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->oldStylePassportSecondPageUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setOldStylePassportSecondPage($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->oldStylePassportPhotoAt25Uploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setOldStylePassportPhotoAt25($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->oldStylePassportPastedPhotoAt25Uploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setOldStylePassportPastedPhotoAt25($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->oldStylePassportPhotoAt45Uploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setOldStylePassportPhotoAt45($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->oldStylePassportPastedPhotoAt45Uploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setOldStylePassportPastedPhotoAt45($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->oldStylePassportProofOfResidenceUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setOldStylePassportProofOfResidence($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->oldStylePassportSelfieWithPassportUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setOldStylePassportSelfieWithPassport($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->newStylePassportPhotoPageUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setNewStylePassportPhotoPage($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->newStylePassportValidityPageUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setNewStylePassportValidityPage($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->newStylePassportRegistrationAppendixUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setNewStylePassportRegistrationAppendix($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->newStylePassportSelfieWithIdCardUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setNewStylePassportSelfieWithIdCard($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->oldStylePensionCertificateFirstSpreadUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setOldStylePensionCertificateFirstSpread($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->oldStylePensionCertificateValidityRecordUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setOldStylePensionCertificateValidityRecord($file);
            $this->manager()->persist($file);
        }

        if ($file = $application->newStylePensionCertificatePhotoAndValiditySpreadUploaded) {
            $file = $fileManager->uploadDocument($file);
            $application->setNewStylePensionCertificatePhotoAndValiditySpread($file);
            $this->manager()->persist($file);
        }
    }

    /**
     * @param UploadedDocuments $uploadedDocuments
     * @param FileManager $fileManager
     * @throws FileExistsException
     * @throws Exception
     */
    private function uploadFilesForOnlineApplication(UploadedDocuments $uploadedDocuments, FileManager $fileManager)
    {
        if ($file = $uploadedDocuments->selfieUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getSelfie(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setSelfie($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->individualTaxNumberUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getIndividualTaxNumber(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setIndividualTaxNumber($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->oldStylePassportFirstPageUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportFirstPage(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setOldStylePassportFirstPage($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->oldStylePassportSecondPageUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportSecondPage(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setOldStylePassportSecondPage($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->oldStylePassportPhotoAt25Uploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportPhotoAt25(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setOldStylePassportPhotoAt25($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->oldStylePassportPastedPhotoAt25Uploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportPastedPhotoAt25(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setOldStylePassportPastedPhotoAt25($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->oldStylePassportPhotoAt45Uploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportPhotoAt45(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setOldStylePassportPhotoAt45($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->oldStylePassportPastedPhotoAt45Uploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportPastedPhotoAt45(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setOldStylePassportPastedPhotoAt45($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->oldStylePassportProofOfResidenceUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportProofOfResidence(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setOldStylePassportProofOfResidence($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->oldStylePassportSelfieWithPassportUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportSelfieWithPassport(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setOldStylePassportSelfieWithPassport($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->newStylePassportPhotoPageUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getNewStylePassportPhotoPage(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setNewStylePassportPhotoPage($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->newStylePassportValidityPageUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getNewStylePassportValidityPage(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setNewStylePassportValidityPage($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->newStylePassportRegistrationAppendixUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getNewStylePassportRegistrationAppendix(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setNewStylePassportRegistrationAppendix($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->newStylePassportSelfieWithIdCardUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getNewStylePassportSelfieWithIdCard(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setNewStylePassportSelfieWithIdCard($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->oldStylePensionCertificateFirstSpreadUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getOldStylePensionCertificateFirstSpread(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setOldStylePensionCertificateFirstSpread($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->oldStylePensionCertificateValidityRecordUploaded) {
            $this->deleteFileIfExists($uploadedDocuments->getOldStylePensionCertificateValidityRecord(), $fileManager);
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setOldStylePensionCertificateValidityRecord($file);
            $this->manager()->persist($file);
        }

        if ($file = $uploadedDocuments->newStylePensionCertificatePhotoAndValiditySpreadUploaded) {
            $this->deleteFileIfExists(
                $uploadedDocuments->getNewStylePensionCertificatePhotoAndValiditySpread(),
                $fileManager
            );
            $file = $fileManager->uploadDocument($file);
            $uploadedDocuments->setNewStylePensionCertificatePhotoAndValiditySpread($file);
            $this->manager()->persist($file);
        }
    }

    /**
     * @param File|null $file
     * @param FileManager $fileManager
     * @throws Exception
     */
    private function deleteFileIfExists(?File $file, FileManager $fileManager)
    {
        if ($file) {
            $fileManager->delete($file);
            $this->manager()->remove($file);
        }
    }

    /**
     * @param Credit $credit
     */
    private function breadcrumbs(Credit $credit): void
    {
        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.home', [], 'text'),
            $this->generateUrl('static_pages.home_page')
        );

        $this->breadcrumbs->addItem(
            $this->translator->trans('public.title.credits', [], 'text'),
            $this->generateUrl('credits.index')
        );

        $this->breadcrumbs->addItem(
            $credit->getTitle(),
            $this->generateUrl('credits.show', ['slug' => $credit->getSlug()])
        );

        $this->breadcrumbs->addItem($this->translator->trans('public.title.online_application', [], 'text'));
    }

    /**
     * @param OnlineApplication $application
     * @param MessageBusInterface $messageBus
     */
    private function sendOnlineApplicationUploadedDocumentsToApi(
        OnlineApplication $application,
        MessageBusInterface $messageBus
    ) {
        if ($application->getIsSentToErp() !== true) {
            return;
        }

        $uploadedDocuments = $application->getUploadedDocuments();

        if ($uploadedDocuments->getPassportType() === PassportTypes::OLD_STYLE) {
            $this->dispatchOldStylePassportDocuments($application, $uploadedDocuments, $messageBus);
        }

        if ($uploadedDocuments->getPassportType() === PassportTypes::NEW_STYLE) {
            $this->dispatchNewStylePassportDocuments($application, $uploadedDocuments, $messageBus);
        }

        $messageBus->dispatch(
            new SendOnlineApplicationFileToApi(
                $uploadedDocuments->getIndividualTaxNumber()->getId(),
                $application->getId(),
                UploadFile::FILE_TYPE_INDIVIDUAL_TAX_NUMBER
            )
        );

        $messageBus->dispatch(
            new SendOnlineApplicationFileToApi(
                $uploadedDocuments->getSelfie()->getId(),
                $application->getId(),
                UploadFile::FILE_TYPE_SELFIE
            )
        );

        if ($uploadedDocuments->getPensionCertificateType() === PensionCertificateTypes::OLD_STYLE) {
            $messageBus->dispatch(
                new SendOnlineApplicationFileToApi(
                    $uploadedDocuments->getOldStylePensionCertificateFirstSpread()->getId(),
                    $application->getId(),
                    UploadFile::FILE_TYPE_PENSION_CERTIFICATE_FIRST_SPREAD
                )
            );

            $messageBus->dispatch(
                new SendOnlineApplicationFileToApi(
                    $uploadedDocuments->getOldStylePensionCertificateValidityRecord()->getId(),
                    $application->getId(),
                    UploadFile::FILE_TYPE_PENSION_CERTIFICATE_VALIDITY_RECORD
                )
            );
        }

        if ($uploadedDocuments->getPensionCertificateType() === PensionCertificateTypes::NEW_STYLE) {
            $messageBus->dispatch(
                new SendOnlineApplicationFileToApi(
                    $uploadedDocuments->getNewStylePensionCertificatePhotoAndValiditySpread()->getId(),
                    $application->getId(),
                    UploadFile::FILE_TYPE_PENSION_CERTIFICATE_PHOTO_AND_VALIDITY_SPREAD
                )
            );
        }
    }

    /**
     * @param OnlineApplication $application
     * @param UploadedDocuments $uploadedDocuments
     * @param MessageBusInterface $messageBus
     */
    private function dispatchOldStylePassportDocuments(
        OnlineApplication $application,
        UploadedDocuments $uploadedDocuments,
        MessageBusInterface $messageBus
    ) {
        $messageBus->dispatch(
            new SendOnlineApplicationFileToApi(
                $uploadedDocuments->getOldStylePassportFirstPage()->getId(),
                $application->getId(),
                UploadFile::FILE_TYPE_PASSPORT_FIRST_PAGE
            )
        );

        $messageBus->dispatch(
            new SendOnlineApplicationFileToApi(
                $uploadedDocuments->getOldStylePassportSecondPage()->getId(),
                $application->getId(),
                UploadFile::FILE_TYPE_PASSPORT_SECOND_PAGE
            )
        );

        $messageBus->dispatch(
            new SendOnlineApplicationFileToApi(
                $uploadedDocuments->getOldStylePassportProofOfResidence()->getId(),
                $application->getId(),
                UploadFile::FILE_TYPE_PASSPORT_PROOF_OF_RESIDENCE
            )
        );

        $messageBus->dispatch(
            new SendOnlineApplicationFileToApi(
                $uploadedDocuments->getOldStylePassportSelfieWithPassport()->getId(),
                $application->getId(),
                UploadFile::FILE_TYPE_PASSPORT_SELFIE_WITH_PASSPORT
            )
        );

        if ($uploadedDocuments->age() >= 25) {
            $messageBus->dispatch(
                new SendOnlineApplicationFileToApi(
                    $uploadedDocuments->getOldStylePassportPastedPhotoAt25()->getId(),
                    $application->getId(),
                    UploadFile::FILE_TYPE_PASSPORT_PASTED_PHOTO_AT_25
                )
            );

            $messageBus->dispatch(
                new SendOnlineApplicationFileToApi(
                    $uploadedDocuments->getOldStylePassportPhotoAt25()->getId(),
                    $application->getId(),
                    UploadFile::FILE_TYPE_PASSPORT_PHOTO_AT_25
                )
            );
        }

        if ($uploadedDocuments->age() >= 45) {
            $messageBus->dispatch(
                new SendOnlineApplicationFileToApi(
                    $uploadedDocuments->getOldStylePassportPastedPhotoAt45()->getId(),
                    $application->getId(),
                    UploadFile::FILE_TYPE_PASSPORT_PASTED_PHOTO_AT_45
                )
            );

            $messageBus->dispatch(
                new SendOnlineApplicationFileToApi(
                    $uploadedDocuments->getOldStylePassportPhotoAt45()->getId(),
                    $application->getId(),
                    UploadFile::FILE_TYPE_PASSPORT_PHOTO_AT_45
                )
            );
        }
    }

    /**
     * @param OnlineApplication $application
     * @param UploadedDocuments $uploadedDocuments
     * @param MessageBusInterface $messageBus
     */
    private function dispatchNewStylePassportDocuments(
        OnlineApplication $application,
        UploadedDocuments $uploadedDocuments,
        MessageBusInterface $messageBus
    ) {
        $messageBus->dispatch(
            new SendOnlineApplicationFileToApi(
                $uploadedDocuments->getNewStylePassportPhotoPage()->getId(),
                $application->getId(),
                UploadFile::FILE_TYPE_ID_CARD_PHOTO
            )
        );

        $messageBus->dispatch(
            new SendOnlineApplicationFileToApi(
                $uploadedDocuments->getNewStylePassportValidityPage()->getId(),
                $application->getId(),
                UploadFile::FILE_TYPE_ID_CARD_VALIDITY
            )
        );

        $messageBus->dispatch(
            new SendOnlineApplicationFileToApi(
                $uploadedDocuments->getNewStylePassportRegistrationAppendix()->getId(),
                $application->getId(),
                UploadFile::FILE_TYPE_ID_CARD_REGISTRATION_APPENDIX
            )
        );

        $messageBus->dispatch(
            new SendOnlineApplicationFileToApi(
                $uploadedDocuments->getNewStylePassportSelfieWithIdCard()->getId(),
                $application->getId(),
                UploadFile::FILE_TYPE_ID_CARD_SELFIE
            )
        );
    }
}
