<?php

namespace App\Controller;

use App\Service\MobileApi\ConfirmEmail;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ConfirmEmailController
 * @package App\Controller
 * @Route("confirm-email")
 */
class ConfirmEmailController extends BaseController
{
    /**
     * @Route("/{confirmationId}", name="confirm_email.index")
     * @param string $confirmationId
     * @param ConfirmEmail $confirmEmailService
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function confirmEmail(string $confirmationId, ConfirmEmail $confirmEmailService): Response
    {
        $success = $confirmEmailService->checkEmailConfirmation($confirmationId);

        return $this->render('confirm_email/index.html.twig', [
            'success' => $success,
        ]);
    }
}
