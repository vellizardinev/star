<?php

namespace App\Controller;

use App\Breadcrumbs\BreadcrumbsService;
use App\Breadcrumbs\CustomerChangePassword;
use App\Breadcrumbs\CustomerLogin;
use App\Breadcrumbs\CustomerLoginStepTwo;
use App\Enum\Flash;
use App\Enum\Roles;
use App\Form\CustomerChangeForgottenPasswordType;
use App\Form\CustomerForgotPasswordType;
use App\Security\CustomerProvider;
use App\Service\MobileApi\Customers;
use stdClass;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CustomerLoginController extends BaseController
{
    use TargetPathTrait;

	private $translator;

	public function __construct(TranslatorInterface $translator)
	{
		$this->translator = $translator;
	}

    /**
     * @Route({"en": "/login", "ua": "/login"}, name="customer_login.login")
     * @param AuthenticationUtils $authenticationUtils
     * @param BreadcrumbsService $breadcrumbsService
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils, BreadcrumbsService $breadcrumbsService)
    {
        if (null !== $this->getUser()) {
            return $this->redirectToRoute('static_pages.home_page');
        }

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        $breadcrumbsService->create(new CustomerLogin());

        return $this->render('customer_login/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }

    /**
     * @Route({"en": "/login/step-two", "ua": "/login/step-two", "ru": "/login/step-two"}, name="customer_login.step_two")
     * @param BreadcrumbsService $breadcrumbsService
     * @param Customers $customersService
     * @param Request $request
     * @param TokenStorageInterface $tokenStorage
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function stepTwo(BreadcrumbsService $breadcrumbsService, Customers $customersService, Request $request, TokenStorageInterface $tokenStorage)
    {
        $lastUsername = $request->getSession()->get(Security::LAST_USERNAME);

        if (empty($lastUsername)) {
            return $this->redirectToRoute('customer_login.login');
        }

        $invalidPassword = false;

        $oneTimePassword = $request->request->get('password');

        if ($request->getMethod() === 'POST') {
            $response = $customersService->loginStepTwo($lastUsername, $oneTimePassword);

            if ($response->isSuccess()) {
                $customer = $response->getCustomer();
                $customer->setRoles([Roles::CUSTOMER]);
                $serializedCustomer = serialize($customer);
                $request->getSession()->set(CustomerProvider::CUSTOMER_SESSION_KEY, $serializedCustomer);

                $token = new UsernamePasswordToken(
                    $customer,
                    null,
                    'main',
                    $customer->getRoles()
                );

                $tokenStorage->setToken($token);

                if ($targetPath = $this->getTargetPath($request->getSession(), 'main')) {
                    return new RedirectResponse($targetPath);
                }

                return new RedirectResponse($this->generateUrl('static_pages.home_page'));
            } else {
                $invalidPassword = true;
            }
        }

        $breadcrumbsService->create(new CustomerLoginStepTwo());

        return $this->render('customer_login/step_two.html.twig', [
            'invalidPassword' => $invalidPassword,
        ]);
    }

    /**
     * @Route({"en": "/customer/forgot-password", "ua": "/customer/forgot-password"}, name="customer_login.forgot_password")
     * @param Request $request
     * @param BreadcrumbsService $breadcrumbsService
     * @param Customers $customersService
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function forgotPassword(Request $request, BreadcrumbsService $breadcrumbsService, Customers $customersService)
    {
        if (null !== $this->getUser()) {
            return $this->redirectToRoute('static_pages.home_page');
        }

        $emailObject = new stdClass();
        $emailObject->email = null;

        $form = $this->createForm(CustomerForgotPasswordType::class, $emailObject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $response = $customersService->forgotPassword($emailObject->email);

	        if ($response->isSuccess()) {
		        $this->addFlash(Flash::SUCCESS, $this->translator->trans('password_recover_email_was_sent', [], 'messages'));

		        return $this->redirectToRoute('static_pages.home_page');
	        } else {
		        $this->addFlash(Flash::ERROR, $this->translator->trans('password_recover_email_was_not_sent', [], 'messages'));

		        return $this->redirectToRoute('customer_login.forgot_password');
	        }
        }

        $breadcrumbsService->create(new CustomerLogin());

        return $this->render('customer_login/forgot_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/customer/change-password/{verificationId}", name="customer_login.change_password")
     * @param string $verificationId
     * @param Request $request
     * @param BreadcrumbsService $breadcrumbsService
     * @param Customers $customersService
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function changePassword(string $verificationId, Request $request, BreadcrumbsService $breadcrumbsService, Customers $customersService)
    {
        if (null !== $this->getUser()) {
            return $this->redirectToRoute('static_pages.home_page');
        }

        $passwordsObject = new stdClass();
        $passwordsObject->newPassword = null;

        $form = $this->createForm(CustomerChangeForgottenPasswordType::class, $passwordsObject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $response = $customersService->changeForgottenPassword(
                $verificationId,
                $passwordsObject->newPassword
            );

            if ($response->isSuccess()) {
                $this->addFlash(
                    Flash::SUCCESS,
                    $this->translator->trans('password_was_changed', [], 'messages')
                );

                return $this->redirectToRoute('customer_profile.index');
            } else {
                $this->addFlash(
                    Flash::ERROR,
                    $this->translator->trans('password_change_failed', [], 'messages')
                );

                return $this->redirectToRoute('customer_login.change_password', ['verificationId' => $verificationId]);
            }
        }

        $breadcrumbsService->create(new CustomerChangePassword());

        return $this->render('customer_login/change_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}
