<?php

namespace App\Controller;

use App\Annotation\NeedsActiveCustomerAccount;
use App\Enum\ApiEndpoints;
use App\Service\MobileApi\BasicAuthHttpClientFactory;
use App\Service\MobileApi\DataTransferObject\DocumentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package App\Controller
 * @Route({"en": "/credit-documents-download", "ua": "/credit-documents-download", "ru": "/credit-documents-download"})
 * @IsGranted("ROLE_CUSTOMER")
 */
class CreditDocumentsDownloadController extends BaseController
{
    const CHANNEL = 'F93C4B69-EA31-461F-B533-8411459AD1CD';

    private $mobileApiUrl;

    private $httpClient;

    public function __construct(string $mobileApiUsername, string $mobileApiPassword, string $mobileApiUrl)
    {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
    }

    /**
     * @Route("/{erpId}/{key}/{name}", name="credit_documents_download.download")
     * @NeedsActiveCustomerAccount()
     * @param string $erpId
     * @param string $key
     * @param string $name
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function download(string $erpId, string $key, string $name)
    {
        $data = [
            'channel' => self::CHANNEL,
            'creditID' => $erpId,
            'docType' => $key,
        ];

        $apiResponse = $this->httpClient->request('POST', $this->mobileApiUrl .ApiEndpoints::GET_SINGLE_DOCUMENT, [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $documentType = new DocumentType($key, $name);

        $tempFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $documentType->fileNameNoExtension() . '_' . $erpId . '.' . $documentType->fileExtension();
        file_put_contents($tempFilePath, $apiResponse->getContent());

        $response = new BinaryFileResponse($tempFilePath);
        $response->headers->set('Content-Type', 'application/pdf');
        $response->deleteFileAfterSend(true);

        return $response;
    }
}
