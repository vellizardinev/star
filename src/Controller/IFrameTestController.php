<?php

namespace App\Controller;

use App\Entity\Affiliate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class IFrameTestController
 * @package App\Controller
 * @Route("iframe-test")
 * @IsGranted("ROLE_ADMIN")
 */
class IFrameTestController extends BaseController
{
	/**
	 * @var Breadcrumbs
	 */ 
	private $breadcrumbs;
	/**
	 * @var TranslatorInterface
	 */
	private $translator;

	/**
	 * @param Breadcrumbs $breadcrumbs
	 * @param TranslatorInterface $translator
	 */
	public function __construct(Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
	{
		$this->breadcrumbs = $breadcrumbs;
		$this->translator = $translator;
	}

    /**
     * @Route("/{id}", name="iframe_test.index")
     * @param Affiliate $affiliate
     * @return RedirectResponse|Response
     */
	public function test(Affiliate $affiliate): Response
	{
		$this->breadcrumbs->addItem($this->translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
		$this->breadcrumbs->addItem($this->translator->trans('title.iframe_test', [], 'text'));

		return $this->render('iframe_test/index.html.twig',[
		    'code' => $affiliate->getCode(),
            'customCss' => $affiliate->getCustomCSS(),
        ]);
	}
}
