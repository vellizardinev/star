<?php

namespace App\Controller;

use App\Entity\Credit;
use App\Entity\CreditData;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CreditDataController
 * @package App\Controller
 * @Route("/credit-data")
 */
class CreditDataController extends BaseController
{
    /**
     * @Route("", name="credit_data.get", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function ranges(Request $request)
    {
        $credits = $this->manager()->getRepository(Credit::class)->getCreditByParameters($request->query->all());

        $amountSteps = $this->manager()->getRepository(CreditData::class)
            ->getAmountSteps($credits);

        $amount = $request->query->get('amount', $amountSteps[0]);

        $paymentSteps = $this->manager()->getRepository(CreditData::class)
            ->getPaymentSteps($credits, $amount);

        $creditData = $this->manager()->getRepository(CreditData::class)
            ->get($request->query->all());

        $data = [
            'amountSteps' => $amountSteps,
            'paymentSteps' => $paymentSteps,
            'creditData' => $creditData,
        ];

        return $this->json($data, 200);
    }

    /**
     * @Route("/details", name="credit_data.details", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function details(Request $request)
    {
        $creditData = $this->manager()->getRepository(CreditData::class)
            ->get($request->query->all());

        return $this->json($creditData, 200);
    }
}
