<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class SecurityController extends BaseController
{
    /**
     * @param AuthenticationUtils $authenticationUtils
     * @param Breadcrumbs $breadcrumbs
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils, Breadcrumbs $breadcrumbs, TranslatorInterface $translator)
    {
        if (null !== $this->getUser()) {
            return $this->redirectToRoute('static_pages.home_page');
        }

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        $breadcrumbs->addItem($translator->trans('public.title.home', [], 'text'), $this->generateUrl('static_pages.home_page'));
        $breadcrumbs->addItem($translator->trans('title.login', [], 'text'));

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }

    /**
     * @Route("/logout", name="app.logout")
     */
    public function logout()
    {
    }
}
