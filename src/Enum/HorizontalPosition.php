<?php


namespace App\Enum;


class HorizontalPosition extends Enum
{
    const LEFT= 'left';
    const RIGHT= 'right';
}