<?php


namespace App\Enum;


class PaymentSchedule extends Enum
{
    const WEEKLY = 'weekly';
    const BIWEEKLY = 'biweekly';
    const MONTHLY = 'monthly';
}