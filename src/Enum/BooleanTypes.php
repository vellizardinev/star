<?php


namespace App\Enum;


class BooleanTypes extends Enum
{
    const YES = true;
    const FALSE = false;
}