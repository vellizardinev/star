<?php

namespace App\Enum;


class PollQuestionType extends Enum
{
    const SINGLE_ANSWER = 'single_answer';
    const MULTIPLE_ANSWERS = 'multiple_answers';
}