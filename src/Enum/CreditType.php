<?php


namespace App\Enum;


class CreditType extends Enum
{
    const PERSONAL = 'personal';
    const BUSINESS = 'business';
}