<?php


namespace App\Enum;


class CreditDocumentType extends Enum
{
    const INITIAL = 'initial_document';
    const INITIAL_ON_APPROVAL = 'initial_on_approval';
    const CREDIT_OFFER = 'credit_offer';
    const REGULAR = 'regular_document';
    const ALL = 'all';
}