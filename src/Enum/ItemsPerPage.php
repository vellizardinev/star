<?php


namespace App\Enum;


class ItemsPerPage extends Enum
{
    const ITEMS_PER_PAGE_9 = 9;
    const ITEMS_PER_PAGE_18 = 18;
    const ITEMS_PER_PAGE_36 = 36;
}