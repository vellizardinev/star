<?php


namespace App\Enum;


class CreditApplicationStatuses extends Enum
{
    const UNPROCESSED = 'unprocessed';
    const APPROVED = 'approved';
    const REJECTED = 'rejected';
    const CANCELLED = 'cancelled';
}