<?php

namespace App\Enum;


class Pages extends Enum
{
    const ICREDIT_AS_EMPLOYER = 'iCredit як роботодавець';
    const MOTIVATION_PROGRAMME = 'Мотиваційна програма';
    const TERMS_AND_CONDITIONS = 'Юридична інформація';
    const FINANCIAL_CULTURE = 'Фінансова культура';
    const FINANCIAL_DICTIONARY = 'Фінансовий словник';
    const CREDIT_THANK_YOU = 'Credit application submitted';
    const JOB_THANK_YOU = 'Job application submitted';
    const CONTACT_THANK_YOU = 'Contact form submitted';
	const MONEY_SHOP = 'MoneyShop';
	const VERO_CASH = 'VeroCash';
    const CUSTOMER_FEEDBACK = 'Відгуки клієнтів';
	const FAMILY_FINANCES = 'Сімейні фінанси';
    const CREDIT_LONG_THANK_YOU = 'Online credit application submitted';
}