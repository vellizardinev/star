<?php

namespace App\Enum;


class FileTypes extends Enum
{
    const FILE = 'file';
    const IMAGE = 'image';
    const CV = 'cv';
}