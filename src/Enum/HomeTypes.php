<?php


namespace App\Enum;


class HomeTypes extends Enum
{
	const OTHER = 'e4380857-362e-40dc-a55c-795c88ce623a';
    const RENT = 'Rent';
    const WITH_RELATIVES = 'With relatives';
    const WITH_FRIENDS = 'With friends';
    const WITH_PARENTS = 'With parents';
    const HIS_HER_OWN_HOUSE = 'His/Her own house';
}