<?php


namespace App\Enum;


class PositionTypes extends Enum
{
    const ADMINISTRATIVE_SUPPORT = 'Administrative/Support';
    const EXPERT_NOT_A_MANAGER_POSITION = 'Expert (not a manager position)';
    const UNEMPLOYED = 'Unemployed';
    const POOR_SKILLED_EMPLOYEE = 'Poor skilled employee';
    const MANAGER = 'Manager';
    const BUSINESS_OWNER = 'Business owner';
    const STUDENT_TRAINEE = 'Student/Trainee';
}