<?php


namespace App\Enum;


class Sort extends Enum
{
    const PRICE_ASC = 'price:asc';
    const PRICE_DESC = 'price:desc';
	const CODE_ASC = 'code:asc';
	const CODE_DESC = 'code:desc';
}