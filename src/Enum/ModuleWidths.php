<?php


namespace App\Enum;


class ModuleWidths extends Enum
{
    const FULL= 'full';
    const STANDARD= 'standard';
}