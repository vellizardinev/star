<?php


namespace App\Enum;


class HouseholdSizes
{
    private static $sizes = [
        '6eeb5265-fdff-40a2-9d60-043abba5db4d' => 1,
        '424a5878-b746-4c8a-9240-101e858bc03e' => 3,
        'aea80986-ea01-4dc4-a480-16ede2cc99ab' => 12,
        '587e2eee-e3f8-407c-83d4-33573cc11bde' => 13,
        '6fd4f3b5-50cc-4170-b656-33da821cac98' => 7,
        '29f843ae-b018-4403-9f18-3575443e0d53' => 4,
        '3e728e20-9402-488b-9c53-524378ded4e8' => 11,
        '855df70c-2ed3-4679-852e-6ca555751724' => 10,
        '48c528ec-f381-469f-b10f-6f99cb8cd86b' => 5,
        '91b86dae-7bd1-4b91-9eb1-6faa56f69575' => 15,
        'c6fe83e9-6f89-4d82-ab7a-77f1b02cba60' => 14,
        '853d2f76-2308-4090-9d19-e47f4797f77b' => 6,
        'fd08c93e-eece-412a-a4ac-e71f9a5924e2' => 2,
        '4b14dc61-8911-4321-aa3c-f8fa6e52259c' => 8,
        '199c2fc7-0046-4e59-8752-fc208f910cf5' => 9,
    ];

    public static function getSize(string $guid): ?int
    {
        if (!array_key_exists($guid, self::$sizes)) {
            return null;
        }

        return self::$sizes[$guid];
    }
}