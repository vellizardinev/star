<?php


namespace App\Enum;


class EducationTypes extends Enum
{
    const NO_EDUCATION = 'No education';
    const UNIVERSITY_COLLEGE_DEGREE = 'University/College degree';
	const BACHELOR_DEGREE = 'c758299a-9466-4480-b3c9-7f207d173c43';
    const DOCTORAL_DEGREE = 'Doctoral degree';
    const MASTERS_DEGREE = 'Master\'s degree';
    const ELEMENTARY_EDUCATION = 'Elementary education';
    const PRIMARY_EDUCATION = 'Primary education';
    const SEMI_HIGHER_EDUCATION = 'Semi-higher education';
    const SECONDARY_EDUCATION = 'Secondary education';
}