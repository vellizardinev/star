<?php


namespace App\Enum;


class RelationshipTypes extends Enum
{
    const HUSBAND_WIFE = 'Husband/Wife';
    const COMMON_LAW_HUSBAND_WIFE = 'Common-Law Husband/Wife';
    const MOTHER_FATHER = 'Mother/Father';
    const SON_DAUGHTER = 'Son/Daughter';
    const GRANDFATHER_GRANDMOTHER = 'Grandfather/Grandmother';
    const GRANDSON_GRANDDAUGHTER = 'Grandson/Granddaughter';
    const SISTER_BROTHER = 'Sister/Brother';
    const COUSIN = 'Cousin';
    const UNCLE_AUNT = 'Uncle/Aunt';
    const CLOSE_FAMILY_FRIEND = 'Close Family Friend';
    const COLLEAGUE = 'Colleague';
    const OTHER = 'Other';
}