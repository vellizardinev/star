<?php

namespace App\Enum;

use BadMethodCallException;
use ReflectionClass;
use App\Exception\InvalidEnumMemberException;
use ReflectionException;

abstract class Enum
{
    /**
     * The key of one of the enum members.
     *
     * @var mixed
     */
    public $key;

    /**
     * The value of one of the enum members.
     *
     * @var mixed
     */
    public $value;

    /**
     * The description of one of the enum members.
     *
     * @var mixed
     */
    public $description;

    /**
     * Constants cache.
     *
     * @var array
     */
    protected static $constCacheArray = [];

    /**
     * Construct an Enum instance.
     *
     * @param mixed $enumValue
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function __construct($enumValue)
    {
        if (!static::hasValue($enumValue)) {
            throw new InvalidEnumMemberException($enumValue, $this);
        }

        $this->value = $enumValue;
        $this->key = static::getKey($enumValue);
        $this->description = static::getDescription($enumValue);
    }

    /**
     * Attempt to instantiate an enum by calling the enum key as a static method.
     *
     * @param string $method
     * @param mixed $parameters
     * @return mixed
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public static function __callStatic($method, $parameters)
    {
        if (static::hasKey($method)) {
            $enumValue = static::getValue($method);
            return new static($enumValue);
        }

        throw new BadMethodCallException("Cannot create an enum instance for $method. The enum value $method does not exist.");
    }

    /**
     * Checks the equality of the value against the enum instance.
     *
     * @param mixed $enumValue
     * @return bool
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function is($enumValue): bool
    {
        if (!static::hasValue($enumValue)) {
            throw new InvalidEnumMemberException($enumValue, $this);
        }

        return $this->value === $enumValue;
    }

    /**
     * Return a new Enum instance,
     *
     * @param mixed $enumValue
     * @return static
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public static function getInstance($enumValue): Enum
    {
        return new static($enumValue);
    }

    /**
     * Return instances of all the contained values.
     *
     * @return static[]
     * @throws ReflectionException
     */
    public static function getInstances(): array
    {
        return array_map(
            function($constantValue) {
                return new static($constantValue);
            },
            static::getConstants()
        );
    }

    /**
     * Get all of the constants defined on the class.
     *
     * @return array
     * @throws ReflectionException
     */
    protected static function getConstants(): array
    {
        $calledClass = get_called_class();

        if (!array_key_exists($calledClass, static::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            static::$constCacheArray[$calledClass] = $reflect->getConstants();
        }

        return static::$constCacheArray[$calledClass];
    }

    /**
     * Get all of the enum keys.
     *
     * @return array
     * @throws ReflectionException
     */
    public static function getKeys(): array
    {
        return array_keys(static::getConstants());
    }

    /**
     * Get all of the enum values.
     *
     * @return array
     * @throws ReflectionException
     */
    public static function getValues(): array
    {
        return array_values(static::getConstants());
    }

    /**
     * Get the key for a single enum value.
     *
     * @param mixed $value
     * @return string
     * @throws ReflectionException
     */
    public static function getKey($value): string
    {
        return array_search($value, static::getConstants(), true);
    }

    /**
     * Get the value for a single enum key
     *
     * @param string $key
     * @return mixed
     * @throws ReflectionException
     */
    public static function getValue(string $key)
    {
        return static::getConstants()[$key];
    }

    /**
     * Get the description for an enum value
     *
     * @param mixed $value
     * @return string
     * @throws ReflectionException
     */
    public static function getDescription($value): string
    {
        return static::getFriendlyKeyName(static::getKey($value));
    }

    /**
     * Get a random key from the enum.
     *
     * @return string
     * @throws ReflectionException
     */
    public static function getRandomKey(): string
    {
        $keys = static::getKeys();

        return $keys[array_rand($keys)];
    }

    /**
     * Get a random value from the enum.
     *
     * @return mixed
     * @throws ReflectionException
     */
    public static function getRandomValue()
    {
        $values = static::getValues();

        return $values[array_rand($values)];
    }

    /**
     * Return the enum as an array.
     *
     * [string $key => mixed $value]
     *
     * @return array
     * @throws ReflectionException
     */
    public static function toArray(): array
    {
        return static::getConstants();
    }

    /**
     * Get the enum as an array formatted for a select.
     *
     * [mixed $value => string description]
     *
     * @return array
     * @throws ReflectionException
     */
    public static function toSelectArray(): array
    {
        $array = static::toArray();
        $selectArray = [];

        foreach ($array as $key => $value) {
            $selectArray[static::getDescription($value)] = $value;
        }

        return $selectArray;
    }

    /**
     * Get the enum as an array in the form key => translation.key
     *
     * @return array
     * @throws ReflectionException
     */
    public static function toTranslationKeys(): array
    {
        $array = static::toArray();
        $selectArray = [];

        foreach ($array as $key => $value) {
            $selectArray[$value] = strtolower(str_replace(__NAMESPACE__ . '\\', '', get_called_class())) . '.' . strtolower($key);
        }

        return $selectArray;
    }

    /**
     * Check that the enum contains a specific key.
     *
     * @param string $key
     * @return bool
     * @throws ReflectionException
     */
    public static function hasKey(string $key): bool
    {
        return in_array($key, static::getKeys(), true);
    }

    /**
     * Check that the enum contains a specific value
     *
     * @param mixed $value
     * @param bool $strict (Optional, defaults to True)
     * @return bool
     * @throws ReflectionException
     */
    public static function hasValue($value, ?bool $strict = true): bool
    {
        $validValues = static::getValues();

        if ($strict) {
            return in_array($value, $validValues, true);
        }

        return in_array((string) $value, array_map('strval', $validValues), true);
    }

    /**
     * Transform the key name into a friendly, formatted version.
     *
     * @param  string  $key
     * @return string
     */
    protected static function getFriendlyKeyName(string $key): string
    {
        $key = strtolower($key);

        return ucfirst(str_replace('_', ' ', $key));
    }


	/**
	 * Get the enum as an array in the form value => value
	 *
	 * [string $value => string $value]
	 *
	 * @return array
	 * @throws ReflectionException
	 */
	public static function toValueArray(): array
	{
		$array = static::toArray();
		$selectArray = [];

		foreach ($array as $key => $value) {
			$selectArray[$value] = $value;
		}

		return $selectArray;
	}

	/**
	 * Get the enum value as translation.key
	 * @param string $value
	 * @return string
	 * @throws ReflectionException
	 */
	public static function getTranslationKey(string $value): string
	{
		if(static::hasValue($value))
		{
			return strtolower(str_replace(__NAMESPACE__ . '\\', '', get_called_class())) . '.' . strtolower(static::getKey($value));
		}

		return $value;
	}
}