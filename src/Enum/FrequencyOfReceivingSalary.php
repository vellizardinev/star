<?php


namespace App\Enum;


class FrequencyOfReceivingSalary extends Enum
{
    const DAILY = 'Daily';
    const WEEKLY = 'Weekly';
    const TWICE_A_MONTH = 'Twice a month';
    const MONTHLY = 'Monthly';
}