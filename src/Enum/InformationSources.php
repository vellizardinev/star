<?php


namespace App\Enum;


class InformationSources extends Enum
{
    const CREDIT_CONSULTANT = 'Credit consultant';
    const BILLBOARD = 'Billboard';
    const LEAFLET = 'Leaflet';
    const EXISTING_CUSTOMER = 'Existing customer';
    const NEWSPAPER = 'Newspaper';
    const ADVERTISEMENTS = 'Advertisements';
    const DIRECT_SALES = 'Direct sales';
    const OTHER = '8c7f781b-119c-4f40-873e-dec0a3a8fee7';
	const INTERNET = '37539555-3b37-4286-9982-65fdb77d0343';
	const CALL_CENTER = 'Call center';
    const STICKER = 'Sticker';
    const LETTER = 'Letter';
    const POSTER = 'Poster';
    const PREVIOUS_CREDIT = 'Previous credit';
    const FRIEND = 'Friend';
    const RADIO = 'Radio';
    const MAGAZINE = 'Magazine';
    const TELEVISION = 'Television';
}