<?php

namespace App\Enum;


class Flash extends Enum
{
    const SUCCESS = 'success';
    const ERROR = 'error';
}