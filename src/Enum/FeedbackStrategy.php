<?php


namespace App\Enum;


class FeedbackStrategy extends Enum
{
    const via_mail = 'email';
    const via_telephone = 'telephone';
    const personal = 'personal';
}