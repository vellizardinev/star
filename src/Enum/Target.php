<?php


namespace App\Enum;


class Target extends Enum
{
	const new_tab = '_BLANK';
	const same_tab = '_SELF';
}