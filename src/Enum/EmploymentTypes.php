<?php


namespace App\Enum;


class EmploymentTypes extends Enum
{
	const UNEMPLOYED_WITHOUT_SOCIAL_HELP = 'fd7357ec-cf63-43a9-9d2b-a218980e61bd';
	const UNEMPLOYED_WITH_SOCIAL_HELP = '18ed0d58-e2c8-43cd-b5f6-4970a74e7d51';
	const CONTRACT_WITHOUT_FIXED_TERM = 'ae0a2698-0322-47b6-979d-11314746a4b6';
	const CIVIL_CONTRACT = '84ec9b6e-f9bc-44b3-a098-004ed79819a9';
	const MANAGEMENT_CONTRACT = '17095073-cb4f-48a1-abf0-da19b1bcda99';
	const OTHER = '560b033f-e67d-4345-ac8b-b0d737d5bb25';
	const MATERNITY_LEAVE = '7524d274-db66-44a5-9d8f-e95f8cd48c23';
	const PENSIONER_BECAUSE_OF_ILLNESS_STILL_WORKING = 'Pensioner because of illness (still working)';
	const PENSIONER_BECAUSE_OF_ILLNESS_NOW_WORKING = 'Pensioner because of illness (now working)';
	const PENSIONER_STILL_WORKING = '8f2936e0-639a-42cc-a360-0e38b0a24dba';
	const PENSIONER_NOT_WORKING = '16561368-406c-463f-9cc6-170271c7d723';
	const WORKING_WITHOUT_CONTRACT = '94d48f96-673d-4ed6-9808-6ee6dc0aeaeb';
	const SELF_INSURED = 'Self-insured';
	const SELF_EMPLOYED = '2c53b814-d1ef-432c-ab60-1ef65fe857ce';
	const FIXED_TERM_CONTRACT = '11c74e27-2b65-4784-992b-6d79f186d416';
}