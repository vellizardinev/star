<?php


namespace App\Enum;


class ActiveCreditStatus extends Enum
{
    const PENDING_CHANGES = '0';
    const APPROVED = '1';
    const AVAILABLE_FOR_UTILIZATION = '2';
    const UNCOMPLETED_PPZ = '3';
    const COMPLETED_PPZ = '4';
    const VERIFIED_PPZBY_CODE = '5';
    const VERIFIED_PPZBY_MAIL = '6';
    const PAID = '7';
    const UTILIZED = '8';
    const OVERDUE = '9';
    const NOT_ACTIVE = '10';
    const CORRECTION_NEEDED = '77';
    const PENDING_SIGNING_INITIAL_DOCUMENTS = '12';
    const PENDING_SIGNING_REGULAR_DOCS = '13';
    const REJECTED = '11';
    const PENDING_CONFIRM_OFFER = '14';
    const PENDING_SIGNING_INITIAL_DOCS_ON_APPROVAL = '15';
}