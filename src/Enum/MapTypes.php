<?php

namespace App\Enum;


class MapTypes extends Enum
{
    const ROADMAP = 'roadmap';
    const SATELLITE = 'satellite';
    const HYBRID = 'hybrid';
    const TERRAIN = 'terrain';
}