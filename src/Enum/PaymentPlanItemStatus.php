<?php


namespace App\Enum;


class PaymentPlanItemStatus extends Enum
{
    const NOT_PAID = 'not_paid';
    const NOT_REACHED_DEADLINE = 'not_reached_deadline';
    const PAID = 'paid';
    const PARTIALLY_PAID = 'partially_paid';
}