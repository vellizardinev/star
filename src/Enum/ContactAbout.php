<?php


namespace App\Enum;


class ContactAbout extends Enum
{
    const PRAISE = 'praise';
    const COMPLAINT = 'complaint';
    const PARTNERSHIP_INVITATION = 'partnership_invitation';
    const RECOMMENDATION = 'recommendation';
    const OTHER = 'other';
}