<?php


namespace App\Enum;


class PropertyType extends Enum
{
    const I_DONT_OWN_A_PROPERTY = 'I don\'t own a property';
    const HOUSE = 'House';
    const APARTMENT = 'Apartment';
    const PARCEL = 'Parcel';
    const AGRICULTURAL_AREA = 'Agricultural area';
}