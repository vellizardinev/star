<?php


namespace App\Enum;


class CreditApplicationSources extends Enum
{
    const LANDING = 'landing';
    const AFFILIATE = 'affiliate';
    const WEBSITE = 'website';
}