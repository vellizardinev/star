<?php


namespace App\Enum;


class PassportTypes extends Enum
{
    const OLD_STYLE = 'old_style';
    const NEW_STYLE = 'new_style';
}