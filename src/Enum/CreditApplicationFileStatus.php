<?php


namespace App\Enum;


class CreditApplicationFileStatus extends Enum
{
    const APPROVED = 'ec08024f-8a62-4fbe-ae55-703cb4ce14aa';
    const REJECTED = '15cdaf5f-c893-4236-8508-b543ad4689cb';
    const WAITING_CONFIRMATION = '256373f2-c0ed-4def-b2ef-d2acdb03b427';
    const DO_NOT_TAKE_INTO_ACCOUNT = '11c5ae18-586e-4c6c-be72-d6af2275e743';
}