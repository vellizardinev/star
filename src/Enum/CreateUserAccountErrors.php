<?php


namespace App\Enum;


class CreateUserAccountErrors extends Enum
{
    const ERROR_EGN_EXISTS = 'ExistingRegistrationWithEGN';
    const ERROR_EMAIL_EXISTS = 'ExistingRegistrationWithEmail';
    const ERROR_INVALID_PASSWORD = 'InvalidPassword';
    const ERROR_INVALID_EGN = 'InvalidEGN';
    const ERROR_INVALID_TELEPHONE = 'InvalidMobilePhoneNumber';
    const ERROR_UNKNOWN= 'error_unknown';
    const USER_ACCOUNT_DOES_NOT_EXIST = 'UserAccountDoesNotExist';
    const EXISTING_REGISTRATIONW_WITH_PHONE_NUMBER = 'ExistingRegistrationwWithPhoneNumber';
    const EMAIL_IS_IN_BLACKLISTED_DOMAINS = 'EmailIsInBlacklistedDomains';
}