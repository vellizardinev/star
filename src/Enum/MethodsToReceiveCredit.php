<?php


namespace App\Enum;


class MethodsToReceiveCredit extends Enum
{
	const CASH = '67585214-aec5-4d7e-90a5-4f5739c4d245';
	const BANK_ACCOUNT = '3e7115f4-eb2e-42b2-8df2-e5c5ed34fad3';
}