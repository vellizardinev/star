<?php


namespace App\Enum;


class CreditPurposes extends Enum
{
	const OTHER = 'c9b4a975-e100-4db0-939a-96427f7f840d';
	const EDUCATION = '7802d573-2cad-40d9-8acd-743d6a5b641b';
    const PAYING_OTHER_CREDITS = 'c9b4a975-e100-4db0-939a-96427f7f840d';
    const UTILITY_BILLS = 'af3125ec-d615-44d3-94fd-ef3f53e93cc6';
    const MEDICATIONS = '15c1fc6e-8010-4780-b324-6d2a9b1313a2';
    const BUSINESS_NEEDS = '373f7141-df14-4eb5-b352-75da5559d72c';
    const TECHNOLOGY = 'Technology';
    const EQUIPMENT = 'Equipment';
    const CAR = 'Car';
    const TRANSPORT = 'Transport';
    const FUN_HOLIDAYS = '77cf14ad-40b1-48fd-9ba8-d3c6fb653c2a';
    const EMPLOYEES_EXPENSES = 'Employees expenses';
    const BUSINESS_EXPANSION = 'Business expansion';
    const REPAIR_FURNISHING = '28376a57-57ea-4d36-ade8-0f7a4bce8754';
}