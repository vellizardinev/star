<?php


namespace App\Enum;


class PageTypes extends Enum
{
    const PAGE = 'page';
    const LANDING_PAGE = 'landing_page';
}