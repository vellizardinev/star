<?php


namespace App\Enum;


class SliderSizes extends Enum
{
    const SMALL = 'small';
    const MEDIUM = 'medium';
    const LARGE = 'large';
}