<?php


namespace App\Enum;


class PensionCertificateTypes extends Enum
{
    const OLD_STYLE = 'old_style';
    const NEW_STYLE = 'new_style';
}