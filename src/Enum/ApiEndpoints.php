<?php


namespace App\Enum;


class ApiEndpoints extends Enum
{
    const LOGIN = 'Login';
    const LOGIN_WITH_ONE_TIME_PASSWORD = 'LoginWithOneTimePassword';
    const LOGOUT = 'LogOut';
    const CREATE_USER_ACCOUNT = 'CreateUserAccount';
    const ACTIVATE_USER_ACCOUNT = 'ActivateUserAccount';
    const UPDATE_ACCOUNT = 'UpdateAccount';
    const UPDATE_USER = 'UpdateUser';
    const CHANGE_ACCOUNT_PASSWORD = 'ChangeAccountPassword';
    const FORGOTTEN_PASSWORD = 'ForgottenPassword';
    const RESEND_ACCOUNT_CONFIRMATION_EMAIL = 'ResendAccountConfirmationEmail';
    const RESEND_VERIFICATION_SMS = 'GeneratePesonalCode';
    const GET_CUSTOMER_CREDITS = 'GetClientCredits';
    const GET_REFINANCE_SUM = 'GetRefinanceSum';
    const GET_REPAYMENT_SUM = "GetRepaymentSum";
    const GET_IDCARDS_BY_CREDIT_ID = "GetIDCardsByCreditID";
    const GET_CREDIT_PROPOSAL = "GetCreditProposal";
    const GET_INITIAL_DOCUMENT_TYPES = "GetInitialDocumentTypes";
    const GET_INITIAL_DOCUMENT_TYPES_ON_APPROVAL = "GetInitialDocumentTypesOnApproval";
    const GET_SINGLE_DOCUMENT = "GetSingleDocument";
    const GET_REGULAR_DOCUMENT_TYPES = "GetRegularDocumentTypes";
    const SIGN_INITIAL_DOCUMENTS = "SignInitialDocuments";
    const SIGN_REGULAR_DOCUMENTS = "SignRegularDocuments";
    const SET_DAYS_DELAY_FOR_SIGNING = "SetDaysDelayForSigning";
    const SAVE_PPZ_SHORT = "SavePPZShort";
    const EMAIL_CONFIRMATION = 'EmailConfirmation';
    const CHANGE_FORGOTTEN_PASSWORD  = 'ChangeForgottenPassword';
    const CREATE_USER  = 'CreateUser';
    const SAVE_PPZ_FULL = 'SaveProposalFullData';
    const GENERATE_KYC_SESSION_ID = 'GenerateKycSessionID';
    const CONFIRM_OFFER = 'ConfirmOffer';
    const GET_USER_PAYMENT_INSTRUMENTS = 'GetUserPaymentInstruments';
    const GET_CREDIT_STORED_DOCUMENTS = 'GetCreditStoredDocuments';
    const GET_STORED_DOCUMENT = 'GetStoredDocument';
    const UPLOAD_FILE_AS_BASE_64_STRING = 'UploadFileAsBase64String';
    const SAVE_KYC_DATA = 'SaveKYCData';
    const CHECK_IS_USER_ACCOUNT_EMAIL_CONFIRMED = 'CheckIsUserAccountEmailConfirmed';
}