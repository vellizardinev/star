<?php

namespace App\Enum;


class Settings extends Enum
{
    const PAGINATION_PER_PAGE = 'pagination.per_page';
    const DATE_TIME_FORMAT = 'date_time.date_time_format';
    const DATE_FORMAT = 'date_time.date_format';
    const MAX_UPLOAD_SIZE = 'file.max_upload_size';
    const IMAGE_MAX_UPLOAD_SIZE = 'image.max_upload_size';
    const ALLOWED_MIMES = 'file.allowed_mimes';
    const DEFAULT_FROM_EMAIL = 'default_from_email';
    const SEND_CONTACT_MESSAGES_TO = 'send_contact_messages_to';
    const SEND_JOB_APPLICATIONS_TO = 'send_job_applications_to';
    const SEND_CREDIT_APPLICATIONS_TO = 'send_credit_applications_to';
    const SEND_POLL_ENTRY_TO = 'send_poll_entry_to';
    const ZENDESK_API_KEY = 'zendesk_api_key';
    const FACEBOOK_LINK = 'facebook_link';
    const TWITTER_LINK = 'twitter_link';
    const LINKEDIN_LINK = 'linkedin_link';
    const YOUTUBE_LINK = 'youtube_link';
	const GOOGLE_RECAPTCHA_API_KEY = 'google_recaptcha_api_key';
	const GOOGLE_RECAPTCHA_SECRET = 'google_recaptcha_secret';
	const CREDIT_APPLICATION_REDIRECT_PATH = 'credit_application_redirect_path';
    const ONLINE_CREDIT_APPLICATION_REDIRECT_PATH = 'online_credit_application_redirect_path';
    const JOB_APPLICATION_REDIRECT_PATH = 'job_application_redirect_path';
	const CONTACT_APPLICATION_REDIRECT_PATH = 'contact_application_redirect_path';
    const LONG_APPLICATIONS_EMAIL = 'long_applications_email';
    const FAILED_LONG_APPLICATIONS_EMAIL = 'failed_long_applications_email';
}