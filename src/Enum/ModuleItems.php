<?php


namespace App\Enum;


class ModuleItems extends Enum
{
    const HEADER = 'Header';
    const FOOTER = 'Footer';
    const CREDIT_CALCULATOR = 'Credit Calculator';
    const OFFICE_MAP = 'Office map';
    const SLIDER = 'Slider';
    const BECOME_CREDIT_CONSULTANT = 'Become credit consultant';
    const WHY_YOU_SHOULD_TRUST_US = 'Why you should trust us';
    const ABOUT_US_MISSION_AND_VISION = 'About Us mission and vision';
    const ABOUT_US_WHO_ARE_WE = 'About Us who are we';
    const CONTENT = 'Content';
    const LATEST_PROMOTIONS = 'Latest promotions';
    const HEADER_LANDING_PAGES = 'header_landing_pages';
    const FOOTER_LANDING_PAGES = 'footer_landing_pages';
	const LATEST_PROMOTIONS_SLIDER = 'Latest promotions slider';
	const ICREDIT_AS_EMPLOYER = 'iCredit as Employer';
}