<?php


namespace App\Enum;


class FamilyStatuses extends Enum
{
    const WIDOWED = 'Widowed';
    const MARRIED = 'Married';
	const SINGLE = 'a9ade86e-d138-444b-81c0-6bc3d8beeba1';
    const DIVORCED = 'Divorced';
    const DOMESTIC_PARTNERSHIP = 'Domestic partnership';
}