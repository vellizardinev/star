<?php


namespace App\Enum;


class ActivityAreas extends Enum
{
    const LUMBERING_COAL_PRODUCTION_ETC = 'Lumbering, coal production, etc.';
    const IT = 'IT';
    const MUNICIPAL_SERVICES = 'Municipal services';
    const LIGHT_INDUSTRY = 'Light industry';
    const MACHINERY_CONSTRUCTION = 'Machinery construction';
    const UNEMPLOYED = 'Unemployed';
    const PUBLIC_SECTOR = 'Public sector';
	const OTHER_SERVICES = 'df5f77b5-9d65-4afd-a944-8c90f39ec59d';
    const POLICE_MILITARY = 'Police/Military';
    const PRODUCTION = 'Productio';
    const PROFESSIONAL_SERVICES = 'Professional services';
    const AGRICULTURE = 'Agriculture';
    const CONSTRUCTION = 'Construction';
    const HEAVY_INDUSTRY = 'Heavy industry';
    const TRANSPORT = 'Transport';
    const TRADING = 'Trading';
}