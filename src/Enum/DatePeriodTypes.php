<?php

namespace App\Enum;


class DatePeriodTypes extends Enum
{
    const WEEK = 'monday this week';
    const MONTH = 'first day of this month';
    const YEAR = 'first day of January this year';
}