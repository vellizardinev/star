<?php


namespace App\Enum;


class NomenclatureTypes extends Enum
{
    const FAMILY_STATUS = 'family_status';
    const INFORMATION_SOURCES = 'information_sources';
    const HOME_TYPES = 'home_types';
    const CREDIT_PURPOSES = 'credit_purposes';
    const EDUCATION_TYPES = 'education_types';
    const EMPLOYMENT_TYPES = 'employment_types';
    const POSITION_TYPES = 'position_types';
    const ACTIVITY_AREAS = 'activity_areas';
    const PROPERTY_TYPES = 'property_types';
    const METHODS_RECEIVING_SALARY = 'methods_receiving_salary';
    const METHODS_TO_RECEIVE_CREDIT = 'methods_to_receive_credit';
    const PARTNER_TYPE = 'partner_type';
    const FAMILY_SIZES = 'family_sizes';
    const INCOME_SOURCES = 'income_sources';
    const EXPENSE_TYPES = 'expense_types';
    const AGREEMENT_TYPES = 'agreement_types';
    const IMAGE_STATUSES = 'image_statuses';
}