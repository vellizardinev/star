<?php


namespace App\Settings;


use App\Enum\Settings;
use Exception;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

class SettingsValidation
{
    protected $rules = [];

    /**
     * SettingsValidation constructor.
     */
    public function __construct()
    {
        $this->initRules();
    }

    /**
     * @param $setting
     * @return mixed
     * @throws Exception
     */
    public function get($setting)
    {
        if (!array_key_exists($setting, $this->rules)) {
            throw new Exception("Unknown setting: $setting");
        }

        return $this->rules[$setting];
    }

    protected function initRules()
    {
        $this->rules = [
            Settings::PAGINATION_PER_PAGE => [new GreaterThan(0), new LessThan(50)],
            Settings::ALLOWED_MIMES => [new NotBlank(), new LessThanOrEqual(255)],
            Settings::MAX_UPLOAD_SIZE => [new NotBlank(), new LessThanOrEqual(255) ],
            Settings::DATE_TIME_FORMAT => [new NotBlank(), new LessThanOrEqual(255)],
            Settings::DATE_FORMAT => [new NotBlank(), new LessThanOrEqual(255)],
            Settings::DEFAULT_FROM_EMAIL => [new Email()],
            Settings::SEND_JOB_APPLICATIONS_TO => [new Email()],
            Settings::SEND_CONTACT_MESSAGES_TO => [new Email()],
            Settings::SEND_CREDIT_APPLICATIONS_TO => [new Email()],
            Settings::SEND_POLL_ENTRY_TO => [new Email()],
            Settings::ZENDESK_API_KEY => [new NotBlank(), new Length(['max' => 64])],
	        Settings::FACEBOOK_LINK => [new NotBlank(), new Url()],
	        Settings::TWITTER_LINK => [new NotBlank(), new Url()],
	        Settings::YOUTUBE_LINK => [new NotBlank(), new Url()],
	        Settings::LINKEDIN_LINK => [new NotBlank(), new Url()],
	        Settings::GOOGLE_RECAPTCHA_API_KEY => [new NotBlank(), new Length(['max' => 64])],
	        Settings::GOOGLE_RECAPTCHA_SECRET => [new NotBlank(), new Length(['max' => 64])],
	        Settings::CREDIT_APPLICATION_REDIRECT_PATH => [new NotBlank(), new Url()],
	        Settings::ONLINE_CREDIT_APPLICATION_REDIRECT_PATH => [new NotBlank(), new Url()],
	        Settings::JOB_APPLICATION_REDIRECT_PATH => [new NotBlank(), new Url()],
	        Settings::CONTACT_APPLICATION_REDIRECT_PATH => [new NotBlank(), new Url()],
	        Settings::IMAGE_MAX_UPLOAD_SIZE => [new NotBlank(), new LessThanOrEqual(255) ],
            Settings::LONG_APPLICATIONS_EMAIL => [new NotBlank(), new Email() ],
            Settings::FAILED_LONG_APPLICATIONS_EMAIL => [new NotBlank(), new Email() ],
        ];
    }
}