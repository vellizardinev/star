<?php

namespace App\EventListener;

use App\Entity\File;
use App\File\FileManager;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Exception;

class FileEventListener
{
    /**
     * @var FileManager
     */
    private $fileManager;

    private $enabled = true;

    /**
     * FileEventListener constructor.
     * @param FileManager $fileManager
     */
    public function __construct(FileManager $fileManager)
    {
        $this->fileManager = $fileManager;
    }

    /**
     * @param LifecycleEventArgs $args
     * @throws Exception
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        if ($this->enabled === false) {
            return;
        }

        $entity = $args->getObject();

        if (!$entity instanceof File) {
            return;
        }

        $this->fileManager->delete($entity);
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }
}


