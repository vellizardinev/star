<?php

namespace App\EventListener;

use App\Entity\Page;
use App\Entity\Setting;
use App\Enum\Pages;
use App\Enum\Settings;
use App\Service\UserDefinedPageRouting;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * Class UserDefinedPageEventListener
 * @package App\EventListener
 */
class UserDefinedPageEventListener
{
    /**
     * @var UserDefinedPageRouting
     */
    private $userDefinedPageRouting;

    /**
     * UserDefinedPageEventListener constructor.
     * @param UserDefinedPageRouting $userDefinedPageRouting
     */
    public function __construct(UserDefinedPageRouting $userDefinedPageRouting)
    {
        $this->userDefinedPageRouting = $userDefinedPageRouting;
    }

    /**
     * @param Page $page
     * @param LifecycleEventArgs $args
     */
    public function prePersist(Page $page, LifecycleEventArgs $args)
    {
        if ($page->getName() === Pages::TERMS_AND_CONDITIONS) {
            $this->setTermsRouteCache($page->getPath());
        }
    }

    /**
     * @param Page $page
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(Page $page, LifecycleEventArgs $args)
    {
        if ($page->getName() === Pages::TERMS_AND_CONDITIONS) {
            $this->setTermsRouteCache($page->getPath());
        }
    }

    public function postUpdate(Page $page, LifecycleEventArgs $args)
    {
        if ($page->getName() === Pages::CREDIT_THANK_YOU) {
            $this->updateRedirectPathSetting(Settings::CREDIT_APPLICATION_REDIRECT_PATH, $page, $args->getObjectManager());
        }

        if ($page->getName() === Pages::CONTACT_THANK_YOU) {
            $this->updateRedirectPathSetting(Settings::CONTACT_APPLICATION_REDIRECT_PATH, $page, $args->getObjectManager());
        }

        if ($page->getName() === Pages::JOB_THANK_YOU) {
            $this->updateRedirectPathSetting(Settings::JOB_APPLICATION_REDIRECT_PATH, $page, $args->getObjectManager());
        }
    }

    private function setTermsRouteCache(string $route)
    {
        $cache = new FilesystemAdapter();
        $termsRoute = $cache->getItem('routes.terms');

        $termsRoute->set($route);
        $cache->save($termsRoute);
    }

    private function updateRedirectPathSetting(string $setting, Page $page, ObjectManager $manager)
    {
        $redirectPathSetting = $manager->getRepository(Setting::class)->findOneBy(['name' => $setting]);
        $redirectPathSetting->setValue($this->userDefinedPageRouting->generateUrl($page));

        $manager->flush();
    }
}


