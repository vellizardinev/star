<?php

namespace App\EventListener;

use App\Entity\CreditShortApplication;
use Doctrine\Persistence\Event\LifecycleEventArgs;

/**
 * Class CreditApplicationEventListener
 * @package App\EventListener
 */
class CreditApplicationEventListener
{
	/**
	 * @param CreditShortApplication $creditApplication
	 * @param LifecycleEventArgs $args
	 */
	public function prePersist(CreditShortApplication $creditApplication, LifecycleEventArgs $args)
    {
		$this->cleanUserData($creditApplication);
    }

	/**
	 * @param CreditShortApplication $creditApplication
	 * @param LifecycleEventArgs $args
	 */
	public function preUpdate(CreditShortApplication $creditApplication, LifecycleEventArgs $args)
	{
		$this->cleanUserData($creditApplication);
	}

	/**
	 * @param CreditShortApplication $creditApplication
	 */
	private function cleanUserData(CreditShortApplication $creditApplication): void {
		$creditApplication
			->setFirstName(null)
			->setLastName(null)
			->setTelephone(null)
			->setBirthdayDate(null);
	}
}


