<?php

namespace App\EventListener;

use App\Entity\FailedOnlineApplicationRequest;
use App\Entity\Setting;
use App\Enum\Settings;
use App\Mail\FailedOnlineApplication;
use App\Mail\MailService;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class FailedOnlineApplicationRequestListener
{
    private $mailService;

    private $manager;

    public function __construct(MailService $mailService, EntityManagerInterface $manager)
    {
        $this->mailService = $mailService;
        $this->manager = $manager;
    }

    /**
     * @param FailedOnlineApplicationRequest $request
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function postUpdate(FailedOnlineApplicationRequest $request)
    {
        if (!$request instanceof FailedOnlineApplicationRequest) {
            return;
        }

        if ($request->getSuccess() === true) {
            $application = $request->getApplication();
            $responseData = json_decode($request->getLastResponse(), true);

            $application->setErpId($responseData['Result']);
            $application->setIsSentToErp(true);

            $this->manager->flush();

            return;
        }

        if ($request->isFailed()) {
            $email = $this->manager->getRepository(Setting::class)
                ->findOneBy(['name' => Settings::FAILED_LONG_APPLICATIONS_EMAIL])->getValue();

            $this->mailService
                ->to($email)
                ->send(new FailedOnlineApplication($request));
        }
    }
}


