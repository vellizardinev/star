<?php

namespace App\EventListener;

use App\Entity\CreditApplicationChangelog;
use App\Entity\CreditShortApplication;
use App\Entity\User;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Exception;
use Symfony\Component\Security\Core\Security;

class CreditApplicationChangelogEventListener
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @param LifecycleEventArgs $args
     * @throws Exception
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof CreditShortApplication) {
            return;
        }

        $manager = $args->getObjectManager();

        $creditApplicationChanges = $manager->getUnitOfWork()->getEntityChangeSet($entity);
	   
	    unset($creditApplicationChanges['updatedAt'], $creditApplicationChanges['createdAt']);

        if ($creditApplicationChanges) {
	        $creditApplicationLog = new CreditApplicationChangelog();
	        $manager->persist($creditApplicationLog);
	        $creditApplicationLog->setCreditApplication($entity);
	        $entity->addCreditApplicationChangelog($creditApplicationLog);

            $user = $this->getUser($manager);

	        $creditApplicationLog->setUser($user);

	        $changesData = [];

            foreach ($creditApplicationChanges as $key => $change) {
                $changesData[] = [
                    ucfirst($key),
                    $change[0],
                    $change[1],
                ];
            }

            $creditApplicationLog->setChangeData($changesData);
        }

        $manager->flush();
    }

    private function getUser($manager): User
    {
        $user = $this->security->getUser();

        if ($user === null || !($user instanceof User)) {
            $user = $manager->getRepository(User::class)->find(1);
        }

        return $user;
    }

}
