<?php


namespace App\Service;


use App\Entity\File;
use App\Entity\Image;
use App\File\FileManager;
use Doctrine\ORM\EntityManagerInterface;

class FileUrlMigration
{
    const IMAGE_PATTERN = '/(<img.*src=")(.+\/Include\/Documents\/[[originalName]].+)(")/i';

    private $filePatterns = [];

    private $imagePatterns = [];

    private $fileReplacements = [];

    private $imageReplacements = [];

    private $manager;

    private $fileManager;

    private $siteBaseUrl;

    public function __construct(EntityManagerInterface $manager, FileManager $fileManager, string $siteBaseUrl)
    {
        $this->manager = $manager;
        $this->fileManager = $fileManager;
        $this->siteBaseUrl = $siteBaseUrl;
    }

    public function updateUrls(string $content): string
    {
        $this->initForImages();

        $result = preg_replace($this->filePatterns, $this->fileReplacements, $content);
        $result = preg_replace($this->imagePatterns, $this->imageReplacements, $result);

        return $result;
    }

    public function loadFileDataFromCSV(string $filePath)
    {
        $file = fopen($filePath, 'r');
        $oldUrls = [];
        $newUrls = [];
        $fileNames = [];

        while (($line = fgetcsv($file, 0, ';')) !== false) {
            if ($line[0] === 'oldUrl') {
                continue;
            }

            list($oldUrls[], $fileNames[], $newUrls[]) = $line;
        }

        fclose($file);

        $newUrls = [];

        foreach ($fileNames as $file) {
            $fileName = $this->extractFileName($file);
            $fileObject = $this->manager->getRepository(File::class)->findOneBy(['originalName' => $fileName]);

            if (!$fileObject) {
                $newUrls[] = sprintf('%s/uploads/files/%s.%s', $this->siteBaseUrl, 'Правила - Новий Рік 2019', 'pdf');
            } else {
                $newUrls[] = sprintf('%s/uploads/files/%s', $this->siteBaseUrl, $fileObject->getName());
            }
        }

        $baseUrl = $this->siteBaseUrl;

        $oldUrls = array_map(
            function ($item) use ($baseUrl) {
                $result = str_replace('/', '\/', $item);

                return sprintf('/%s/i', $result);
            }, $oldUrls);


        $this->filePatterns = $oldUrls;
        $this->fileReplacements = $newUrls;
    }

    private function initForImages()
    {
        $images = $this->manager->getRepository(Image::class)->findAll();

        foreach ($images as $image) {
            $originalName = str_replace(' ', '%20', $image->getOriginalName());

            $this->imagePatterns[] = str_replace(
                '[[originalName]]',
                $originalName . '.' . $image->getMime(),
                self::IMAGE_PATTERN
            );

            $this->imageReplacements[] = sprintf(
                "%s%s%s",
                '${1}',
                $this->fileManager->publicImagePath($image),
                '${3}'
            );
        }
    }

    private function extractFileName(string $fileName)
    {
        $parts = explode('.', $fileName);

        array_pop($parts);

        return implode('.', $parts);
    }
}