<?php


namespace App\Service;


use App\Entity\File;
use App\File\FileManager;
use Exception;

class ImageManipulation
{
    const PNG = 'png';
    const JPEG = 'jpeg';

    private $fileManager;

    public function __construct(FileManager $fileManager)
    {
        $this->fileManager = $fileManager;
    }

    /**
     * @param File $file
     * @throws Exception
     */
    public function toGreyscale(File $file)
    {
        if ($this->determineImageType($file) === self::JPEG) {
            $path = $this->fileManager->systemFilePath($file);
            $image = imagecreatefromjpeg($path);
            imagefilter($image, IMG_FILTER_GRAYSCALE);
            imagejpeg($image, $path);
            imagedestroy($image);

            return;
        }

        if ($this->determineImageType($file) === self::PNG) {
            $path = $this->fileManager->systemFilePath($file);
            $image = imagecreatefrompng($path);
            imagefilter($image, IMG_FILTER_GRAYSCALE);
            imagepng($image, $path);
            imagedestroy($image);

            return;
        }

        throw new Exception('Unsupported image type. Supported image types are .jpeg and .png.');
    }

    public function compressToSize(File $file, int $sizeInBytes): string
    {
        if ($this->determineImageType($file) === self::JPEG) {
            return $this->compressJpeg($file, $sizeInBytes);
        }

        if ($this->determineImageType($file) === self::PNG) {
            return $this->compressPng($file, $sizeInBytes);
        }

        throw new Exception('Unsupported image type. Supported image types are .jpeg and .png.');
    }

    private function compressJpeg(File $file, int $sizeInBytes): string
    {
        $originalFile = $this->fileManager->systemFilePath($file);;
        $resizedFile = tempnam(sys_get_temp_dir(), 'resized_image');

        $originalImage = imagecreatefromjpeg($originalFile);

        $imageQuality = 100;

        do {
            $tempStream = fopen('php://temp', 'w+');
            imagejpeg($originalImage, $tempStream, $imageQuality);
            rewind($tempStream);
            $fstat = fstat($tempStream);
            fclose($tempStream);

            $fileSize = $fstat['size'];
            $imageQuality = $imageQuality - 10;
        } while (($fileSize > $sizeInBytes) && ($imageQuality >= 0));

        imagejpeg($originalImage, $resizedFile, $imageQuality + 10);

        return $resizedFile;
    }

    private function compressPng(File $file, int $sizeInBytes): string
    {
        $originalFile = $this->fileManager->systemFilePath($file);;
        $resizedFile = tempnam(sys_get_temp_dir(), 'resized_image');

        $originalImage = imagecreatefrompng($originalFile);

        $imageQuality = 9;

        do {
            $tempStream = fopen('php://temp', 'w+');
            imagepng($originalImage, $tempStream, $imageQuality);
            rewind($tempStream);
            $fstat = fstat($tempStream);
            fclose($tempStream);

            $fileSize = $fstat['size'];
            $imageQuality--;
        } while (($fileSize > $sizeInBytes) && ($imageQuality >= 0));

        imagepng($originalImage, $resizedFile, $imageQuality + 1);

        return $resizedFile;
    }

    /**
     * @param File $file
     * @return string
     * @throws Exception
     */
    private function determineImageType(File $file): string
    {
        $mime = $file->getMime();

        if ($mime === 'jpg' || $mime === 'jpeg') {
            return self::JPEG;
        }

        if ($mime === 'png') {
            return self::PNG;
        }

        throw new Exception('Unsupported image type. Supported image types are .jpeg and .png.');
    }
}