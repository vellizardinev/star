<?php


namespace App\Service;


use App\Entity\OnlineApplication\OnlineApplication;
use Symfony\Component\Routing\RouterInterface;

class OnlineApplicationSteps
{
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function routeForUncompletedStepBefore(OnlineApplication $application, string $step): string
    {
        $uncompletedStep = $application->uncompletedStepBefore($step);

        switch ($uncompletedStep) {
            case 'personalInfo':
                return $this->router->generate('online_application.personal_info', ['hash' => $application->getHash()]);
            case 'address':
                return $this->router->generate('online_application.address', ['hash' => $application->getHash()]);
            case 'additionalInfo':
                return $this->router->generate('online_application.additional_info', ['hash' => $application->getHash()]);
            case 'contactPerson':
                return $this->router->generate('online_application.contact_person', ['hash' => $application->getHash()]);
            case 'confirmation':
                return $this->router->generate('online_application.confirmation', ['hash' => $application->getHash()]);
            default :
                return $this->router->generate('online_application.credit_details', ['slug' => 'icredit']);
        }
    }
}
