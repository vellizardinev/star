<?php


namespace App\Service;

use App\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class UserDefinedPageRouting
{
    /**
     * @var string
     */
    private $siteBaseUrl;
    /**
     * @var EntityManagerInterface
     */
    private $manager;


    /**
     * UserDefinedPageRoute constructor.
     * @param string $siteBaseUrl
     * @param EntityManagerInterface $manager
     */
    public function __construct(string $siteBaseUrl, EntityManagerInterface $manager)
    {

        $this->siteBaseUrl = $siteBaseUrl;
        $this->manager = $manager;
    }

    public function generateUrl(Page $page, string $fragment = null)
    {
        $url = $this->siteBaseUrl . '/' . $page->getPath();

        if ($fragment !== null) {
            $url .= "#{$fragment}";
        }

        return $url;
    }

    /**
     * @param string $pageName
     * @return string
     * @throws Exception
     */
    public function generateUrlByName(string $pageName)
    {
        $page = $this->manager->getRepository(Page::class)->findOneBy(['internalName' => $pageName]);

        if (!$page) {
            throw new Exception(sprintf('Could not find page named %s', $pageName));
        }

        return $this->generateUrl($page);
    }
}