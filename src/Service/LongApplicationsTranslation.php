<?php


namespace App\Service;


use App\Entity\OnlineApplication\AdditionalInfo;
use App\Entity\OnlineApplication\Address;
use App\Entity\OnlineApplication\Confirmation;
use App\Entity\OnlineApplication\ContactPerson;
use App\Entity\OnlineApplication\CreditDetails;
use App\Entity\OnlineApplication\Expenses;
use App\Entity\OnlineApplication\PersonalInfo;
use App\Entity\OnlineApplication\Property;
use App\Entity\OnlineApplication\WorkDetails;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use Symfony\Component\Yaml\Yaml;

class LongApplicationsTranslation
{
    private $locales;

    private $projectDir;

    public function __construct(string $locales, string $projectDir)
    {
        $this->locales = explode('|', $locales);
        $this->projectDir = $projectDir;
    }

    /**
     * @throws ReflectionException
     */
    public function update()
    {

        $mappedFields = $this->extractFieldNames();

        // Update translations for locales
        foreach ($this->locales as $locale) {
            $this->updateForLocale($locale, $mappedFields);
        }
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    private function extractFieldNames(): array
    {
        $fields = array_merge(
            $this->extractFieldNamesForClass(CreditDetails::class),
            $this->extractFieldNamesForClass(PersonalInfo::class),
            $this->extractFieldNamesForClass(Address::class),
            $this->extractFieldNamesForClass(WorkDetails::class),
            $this->extractFieldNamesForClass(Expenses::class),
            $this->extractFieldNamesForClass(Property::class),
            $this->extractFieldNamesForClass(AdditionalInfo::class),
            $this->extractFieldNamesForClass(ContactPerson::class),
            $this->extractFieldNamesForClass(Confirmation::class)
        );

        sort($fields);

        return $fields;
    }

    /**
     * @param string $fqcn
     * @return array
     * @throws ReflectionException
     */
    private function extractFieldNamesForClass(string $fqcn): array
    {
        $reflection = new ReflectionClass($fqcn);
        $fields = $reflection->getProperties(ReflectionProperty::IS_PRIVATE);
        $mappedFields = [];

        foreach ($fields as $field) {
            $mappedFields[] = $field->getName();
        }

        return $mappedFields;
    }

    private function updateForLocale(string $locale, array $fields)
    {
        $translations = $this->getTranslations($locale);

        // Iterate over fields
        foreach ($fields as $field) {
            // If field is missing in translation files, add it
            if (!array_key_exists($field, $translations)) {
                $translations[$field] = null;
            }
        }

        $keysToDelete = [];
        // Iterate over translation entries
        foreach ($translations as $key => $value) {
            // If a translation entry is missing in fields, delete it from translations
            if (array_search($key, $fields) === false) {
                $keysToDelete[] = $key;
            }
        }

        foreach ($keysToDelete as $key) {
            unset($translations[$key]);
        }

        // Write updated translation files back to file
        $yaml = Yaml::dump($translations);

        file_put_contents($this->pathForLocale($locale), $yaml);
    }

    private function getTranslations(string $locale): array
    {
        $path = $this->pathForLocale($locale);

        $translations = Yaml::parseFile($path);

        if ($translations) {
            return $translations;
        } else {
            return [];
        }
    }

    private function pathForLocale(string $locale): string
    {
        return $this->projectDir . DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR . 'long_application.' . $locale . '.yml';
    }
}