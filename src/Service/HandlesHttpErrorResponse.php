<?php

namespace App\Service;


use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

trait HandlesHttpErrorResponse
{
    /**
     * @param string $url
     * @param ResponseInterface $response
     * @param LoggerInterface $logger
     * @return bool
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function handleResponse(string $url, ResponseInterface $response, LoggerInterface $logger): bool
    {
        if ($response->getStatusCode() >= 300) {
            $logger->error(sprintf(
                'HttpClient request to URL \'%s\' returned error response with status code %s and content: %s',
                $url,
                $response->getStatusCode(),
                $response->getContent(false)
            ));

            return false;
        } else {
            $logger->info(sprintf(
                'HttpClient request to URL \'%s\' returned OK response with status code %s and content: %s',
                $url,
                $response->getStatusCode(),
                $response->getContent(false)
            ));

            return true;
        }
    }
}