<?php


namespace App\Service;


class RandomString
{
    public static function generate(int $length = 32): string
    {
        $permittedChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle($permittedChars), 0, $length <= 62 ? $length : 62);
    }
}