<?php

namespace App\Service;

use App\Service\MobileApi\Response\CreateEasyPayAppResponse;
use App\Service\MobileApi\Response\GenerateEasyPayOrderResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EasyPay
{
    use HandlesHttpErrorResponse;

    const CREATE_APP_URL = '/api/system/createApp';
    const CREATE_ORDER_URL = '/api/merchant/createOrder';
    const TOKENIZATION_URL = 'https://cdn.easypay.ua/partners/card-tokenization/tokenization';

    private $httpClient;

    private $easyPayUrl;

    private $easyPayPartnerKey;

    private $easyPayServiceKey;

    private $easyPaySecretKey;

    private $customersLog;

    private $router;

    private $urlHelper;

    public function __construct(
        HttpClientInterface $httpClient,
        string $easyPayUrl,
        string $easyPayPartnerKey,
        string $easyPayServiceKey,
        string $easyPaySecretKey,
        LoggerInterface $customersLog,
        RouterInterface $router,
        UrlHelper $urlHelper
    ) {
        $this->httpClient = $httpClient;
        $this->easyPayUrl = $easyPayUrl;
        $this->easyPayPartnerKey = $easyPayPartnerKey;
        $this->easyPayServiceKey = $easyPayServiceKey;
        $this->easyPaySecretKey = $easyPaySecretKey;
        $this->customersLog = $customersLog;
        $this->router = $router;
        $this->urlHelper = $urlHelper;
    }

    /**
     * @param string $contract
     * @param float $amount
     * @param bool $isEarlyRepayment
     * @return GenerateEasyPayOrderResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function generatePaymentUrl(string $contract, float $amount, bool $isEarlyRepayment = false): GenerateEasyPayOrderResponse
    {
        $createAppResponse = $this->httpClient->request(
            'POST',
            $this->getCreateAppUrl(),
            [
                'verify_peer' => false,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'PartnerKey' => $this->easyPayPartnerKey,
                    'Locale' => 'ua',
                ],
            ]
        );

        if (!$this->handleResponse($this->getCreateAppUrl(), $createAppResponse, $this->customersLog)) {
            return new GenerateEasyPayOrderResponse([]);
        }

        $createAppResponse = new CreateEasyPayAppResponse($createAppResponse->toArray());

        if ($createAppResponse->isSuccess()) {
            $appId = $createAppResponse->getAppId();
            $pageId = $createAppResponse->getPageId();

            if ($isEarlyRepayment) {
                $orderId = sprintf(
                    '1-%s-%s',
                    $contract,
                    RandomString::generate(6)
                );
            } else {
                $orderId = sprintf(
                    '%s-%s',
                    $contract,
                    RandomString::generate(6)
                );
            }

            $description = sprintf(
                'Payment for loan from iCredit Ukraine with contract number %s',
                $contract
            );

            $successUrl = $this->router->generate('pay_online.success', [], RouterInterface::ABSOLUTE_PATH);
            $successUrl = $this->urlHelper->getAbsoluteUrl($successUrl);

            $failedUrl = $this->router->generate('pay_online.failed', [], RouterInterface::ABSOLUTE_PATH);
            $failedUrl = $this->urlHelper->getAbsoluteUrl($failedUrl);

            $notifyUrl = $this->router->generate('api.easy_pay.confirm_payment', [], RouterInterface::ABSOLUTE_PATH);
            $notifyUrl = $this->urlHelper->getAbsoluteUrl($notifyUrl);

            $data = [
                'order' => [
                    'serviceKey' => $this->easyPayServiceKey,
                    'orderId' => $orderId,
                    'description' => $description,
                    'amount' => $amount,
                    'additionalItems' => [
                        'Merchant.UrlNotify' => $notifyUrl,
                    ],
                ],
                'urls' => [
                    'success' => $successUrl,
                    'failed' => $failedUrl,
                ],
            ];

            $sign = $this->calculateSign(json_encode($data, JSON_PRETTY_PRINT)); //dd($appId, $pageId, $sign);

            $generateOrderResponse = $this->httpClient->request(
                'POST',
                $this->getCreateOrderUrl(),
                [
                    'verify_peer' => false,
                    'body' => json_encode($data, JSON_PRETTY_PRINT),
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'PartnerKey' => $this->easyPayPartnerKey,
                        'Locale' => 'ua',
                        'AppId' => $appId,
                        'PageId' => $pageId,
                        'Sign' => $sign,
                    ],
                ]
            );

// dd($generateOrderResponse->getContent(false));

            if (!$this->handleResponse($this->getCreateOrderUrl(), $generateOrderResponse, $this->customersLog)) {
                return new GenerateEasyPayOrderResponse([]);
            }

            return new GenerateEasyPayOrderResponse($generateOrderResponse->toArray());
        }

        return new GenerateEasyPayOrderResponse([]);
    }

    /**
     * @param string $kycSessionId
     * @return string
     */
    public function generateKycUrl(string $kycSessionId): string
    {
        $timestamp = time();
        $sign = base64_encode(hash('sha256', ($this->easyPaySecretKey . $this->easyPayPartnerKey . $timestamp), true));

        $parameters = [
            'PartnerKey' => $this->easyPayPartnerKey,
            'sign' => $sign,
            'TimeStamp' => $timestamp,
            'Phone' => $kycSessionId,
        ];

        return sprintf(
            '%s?%s',
            self::TOKENIZATION_URL,
            http_build_query($parameters)
        );
    }

    private function getCreateAppUrl(): string
    {
        return $this->easyPayUrl . self::CREATE_APP_URL;
    }

    private function getCreateOrderUrl(): string
    {
        return $this->easyPayUrl . self::CREATE_ORDER_URL;
    }

    private function calculateSign(string $requestBody): string
    {
        return base64_encode(hash('sha256', ($this->easyPaySecretKey . $requestBody), true));
    }
}