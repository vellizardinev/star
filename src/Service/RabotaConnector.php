<?php


namespace App\Service;


use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RabotaConnector
{
    const VACANCY_LIST_URL = 'https://employer-api.rabota.ua/vacancy/list';
    const VACANCY_DETAILS_URL = 'https://employer-api.rabota.ua/vacancy/get/';
    const CITY_LIST_URL = 'https://employer-api.rabota.ua/values/citylist';
    const LANGUAGE_LIST_URL = 'https://employer-api.rabota.ua/values/languagelist';
    const LANGUAGE_SKILLS_URL = 'https://employer-api.rabota.ua/values/languageskilllist';
    const LOGIN_URL = 'https://auth-api.rabota.ua/Login';
    const VACANCY_CITY_LIST_URL = 'https://employer-api.rabota.ua/vacancy/citylist';

    const AUTH_TOKEN_CACHE_KEY = 'rabota_auth_token';
    const CITIES_CACHE_KEY = 'rabota_cities';

    private $manager;

    private $cache;

    private $rabotaEmail;

    private $rabotaPassword;

    private $httpClient;

    public function __construct(
        EntityManagerInterface $manager,
        AdapterInterface $cache,
        HttpClientInterface $httpClient,
        string $rabotaEmail,
        string $rabotaPassword
    ) {
        $this->manager = $manager;
        $this->cache = $cache;
        $this->rabotaEmail = $rabotaEmail;
        $this->rabotaPassword = $rabotaPassword;
        $this->httpClient = $httpClient;
    }

    /**
     * @param int $page
     * @param int|null $cityId
     * @return array|mixed
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function vacancies(int $page = 0, ?int $cityId = null)
    {
        if ($cityId) {
            $data = ['vacancyStateId' => 'Publicated', 'page' => $page, 'cityId' => $cityId];
        } else {
            $data = ['vacancyStateId' => 'Publicated', 'page' => $page];
        }

        $response = $this->httpClient->request(
            'POST',
            self::VACANCY_LIST_URL,
            [
                'json' => $data,
                'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
            ]
        );

        if ($response->getStatusCode() == 401) {
            $this->login();

            $response = $this->httpClient->request(
                'POST',
                self::VACANCY_LIST_URL,
                [
                    'json' => $data,
                    'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
                ]
            );
        }

        $results = json_decode($response->getContent(), true);

        $total = $results['total'];

        $vacancies = array_map(
            function ($vacancy) {
                return [
                    'id' => $vacancy['vacancyId'],
                    'deadline' => Carbon::createFromTimeString($this->extractKey('activeToDate', $vacancy)),
                    'title' => $this->extractKey('vacancyName', $vacancy),
                    'city' => $this->cityName($this->extractKey('cityId', $vacancy), 'ua'),
                    'cityId' => $this->extractKey('cityId', $vacancy),
                ];
            },
            $results['vacancies']
        );

        $vacancies = $this->prepareForPagination($vacancies, $total, $page);

        return ['vacancies' => $vacancies, 'total' => $total];
    }

    /**
     * @param string $vacancyId
     * @return array|mixed
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function vacancyDetails(string $vacancyId)
    {
        $response = $this->httpClient->request(
            'POST',
            self::VACANCY_DETAILS_URL . $vacancyId,
            [
                'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
            ]
        );

        if ($response->getStatusCode() == 401) {
            $this->login();

            $response = $this->httpClient->request(
                'POST',
                self::VACANCY_LIST_URL . $vacancyId,
                [
                    'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
                ]
            );
        }

        $results = json_decode($response->getContent(), true);

        $results = [
            'id' => $results['vacancyId'],
            'title' => $this->extractKey('vacancyName', $results),
            'deadline' => Carbon::createFromTimeString($this->extractKey('activeToDate', $results)),
            'description' => $this->extractKey('description', $results),
            'salary' => $this->extractKey('salary', $results),
            'currency' => $this->extractKey('currencySign', $results),
            'contactPerson' => $this->extractKey('contactPerson', $results),
            'contactEmail' => $this->extractKey('contactEmail', $results),
            'contactPhone' => $this->extractKey('contactPhone', $results),
            'cityId' => $this->extractKey('cityId', $results),
            'city' => $this->cityName($this->extractKey('cityId', $results), 'ua'),
            'languages' => $this->extractLanguages($this->extractKey('languages', $results)),
        ];

        return $results;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function cities(): array
    {
        $item = $this->cache->getItem(self::CITIES_CACHE_KEY);

        if ($item->isHit()) {
            return $item->get();
        }

        $cities = $this->loadCities();

        $item->set($cities);
        $item->expiresAt(Carbon::now()->addHours(2));
        $this->cache->save($item);

        return $cities;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function vacancyCities(): array
    {
        $response = $this->httpClient->request(
            'GET',
            self::VACANCY_CITY_LIST_URL,
            [
                'json' => ["sortOrder" => 2],
                'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
            ]
        );

        if ($response->getStatusCode() == 401) {
            $this->login();

            $response = $this->httpClient->request(
                'GET',
                self::VACANCY_CITY_LIST_URL,
                [
                    'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
                ]
            );
        }

        $results = json_decode($response->getContent(), true);
        $results = array_map(
            function ($city) {
                return [
                    'id' => $city['id'],
                    'ru' => $city['name'],
                    'ua' => $city['nameUkr'],
                    'en' => $this->extractKey('nameEng', $city),
                ];
            },
            $results
        );

        return $results;
    }

    /**
     * @return array|mixed
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function languages()
    {
        $response = $this->httpClient->request(
            'GET',
            self::LANGUAGE_LIST_URL,
            [
                'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
            ]
        );

        if ($response->getStatusCode() == 401) {
            $this->login();

            $response = $this->httpClient->request(
                'GET',
                self::LANGUAGE_LIST_URL,
                [
                    'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
                ]
            );
        }

        $results = json_decode($response->getContent(), true);

        $results = array_map(
            function ($language) {
                return [
                    'id' => $language['id'],
                    'ru' => $language['name'],
                    'ua' => $language['nameUkr'],
                    'en' => $language['nameEng'],
                ];
            },
            $results
        );

        return $results;
    }

    /**
     * @return array|mixed
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function languageSkills()
    {
        $response = $this->httpClient->request(
            'GET',
            self::LANGUAGE_SKILLS_URL,
            [
                'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
            ]
        );

        if ($response->getStatusCode() == 401) {
            $this->login();

            $response = $this->httpClient->request(
                'GET',
                self::LANGUAGE_SKILLS_URL,
                [
                    'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
                ]
            );
        }

        $results = json_decode($response->getContent(), true);

        $results = array_map(
            function ($skill) {
                return [
                    'id' => $skill['id'],
                    'ru' => $skill['name'],
                    'ua' => $skill['nameUkr'],
                    'en' => $skill['nameEng'],
                ];
            },
            $results
        );

        return $results;
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function authToken(): string
    {
        $item = $this->cache->getItem(self::AUTH_TOKEN_CACHE_KEY);

        if ($item->isHit()) {
            return $item->get();
        }

        return $this->login();
    }

    /**
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function login(): string
    {
        $data = [
            'username' => $this->rabotaEmail,
            'password' => $this->rabotaPassword,
            'remember' => true,
        ];

        $response = $this->httpClient->request(
            'POST',
            self::LOGIN_URL,
            [
                'json' => $data,
            ]
        );

        return trim($response->getContent(), "\"");
    }

    /**
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function loadCities(): array
    {
        $response = $this->httpClient->request(
            'GET',
            self::CITY_LIST_URL,
            [
                'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
            ]
        );

        if ($response->getStatusCode() == 401) {
            $this->login();

            $response = $this->httpClient->request(
                'GET',
                self::CITY_LIST_URL,
                [
                    'headers' => ['Authorization' => 'Bearer ' . $this->authToken()],
                ]
            );
        }

        $results = json_decode($response->getContent(), true);
        $results = array_map(
            function ($city) {
                return [
                    'id' => $city['id'],
                    'ru' => $city['name'],
                    'ua' => $city['nameUkr'],
                    'en' => $this->extractKey('nameEng', $city),
                ];
            },
            $results
        );

        return $results;
    }

    public function applicationUrl(string $vacancyId)
    {
        return sprintf('https://rabota.ua/company810307/vacancy%s?mode=apply#apply', $vacancyId);
    }

    /**
     * @param array $data
     * @param string $locale
     * @return string
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function extractLanguages(array $data, string $locale = 'ua'): string
    {
        $result = [];

        $languages = $this->languages();
        $skills = $this->languageSkills();

        foreach ($data as $item) {
            $languageId = $item['languageId'];
            $skillId = $item['skillId'];

            $language = $this->findElement($languageId, $languages);
            $languageName = $language[$locale];

            $skill = $this->findElement($skillId, $skills);
            $skillName = $skill[$locale];

            $result[] = sprintf('%s (%s)', $languageName, $skillName);
        }

        $result = implode(', ', $result);

        return $result;
    }

    private function extractKey(string $key, array $data)
    {
        if (array_key_exists($key, $data)) {
            return $data[$key];
        }

        return null;
    }

    /**
     * @param string $cityId
     * @param string $locale
     * @return string|null
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function cityName(string $cityId, string $locale = 'ua'): ?string
    {
        $cities = $this->cities();

        $city = $this->findElement($cityId, $cities);

        if (!$city) {
            $cities = $this->loadCities();
            $city = $this->findElement($cityId, $cities);
        }

        return $city[$locale];
    }
    
    private function findElement(int $elementId, array $data): ?array
    {
        foreach ($data as $item) {
            if ($item['id'] === $elementId) {
                return $item;
            }
        }

        return null;
    }

    private function prepareForPagination(array $vacancies, int $total, int $page = 0)
    {
        $result = array_fill(0, $total, []);

        $startPosition = $page * 20;
        $count = count($vacancies);

        array_splice($result, $startPosition, $count, $vacancies);

        return $result;
    }
}