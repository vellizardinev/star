<?php


namespace App\Service;


use App\Entity\RedirectRule;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class RedirectRulesService
{
    /**
     * @var string
     */
    private $projectDir;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * RedirectRuleService constructor.
     * @param string $projectDir
     * @param EntityManagerInterface $manager
     */
    public function __construct(string $projectDir, EntityManagerInterface $manager)
    {
        $this->projectDir = $projectDir;
        $this->manager = $manager;
    }

    /**
     * @throws Exception
     */
    public function applyRules()
    {
        $htaccess = $this->projectDir . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . '.htaccess';

        $lines = file($htaccess);

        $rules = $this->manager->getRepository(RedirectRule::class)->findBy([], ['oldUrl' => 'desc']);

        $startLine = $this->lineIndex($lines, 'start of custom redirect rules');
        $endLine = $this->lineIndex($lines, 'end of custom redirect rules');

        if (!$startLine || !$endLine) {
            throw new Exception("Could not find section with custom Redirect Rules.");
        }

        $length = $endLine - $startLine - 1;

        $newLines = [];

        foreach ($rules as $rule) {
            $old = ltrim($rule->getOldUrl(), '/');
            $new = ltrim($rule->getNewUrl(), '/');
            $newLines[] = "    Redirect 301 /$old /$new\r\n";
        }

        array_splice($lines, $startLine + 1, $length, $newLines);

        file_put_contents($htaccess, implode('', $lines));
    }

    private function lineIndex(array $haystack, string $needle): ?int
    {
        for ($i = 0; $i < count($haystack); $i++) {
            if (strpos($haystack[$i], $needle) !== false) {
                return $i;
            }
        }

        return null;
    }
}