<?php


namespace App\Service;


use App\Entity\HttpRequest;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HttpRequestService
{
    private $manager;

    private $httpClient;

    private $retriedHttpRequestsLog;

    public function __construct(EntityManagerInterface $manager, HttpClientInterface $httpClient, LoggerInterface $retriedHttpRequestsLog)
    {
        $this->manager = $manager;
        $this->httpClient = $httpClient;
        $this->retriedHttpRequestsLog = $retriedHttpRequestsLog;
    }

    /**
     * @param HttpRequest $httpRequest
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function send(HttpRequest $httpRequest): void
    {
        if ($httpRequest->getCompleted() === true) {
            return;
        }

        $response = $this->httpClient->request(
            $httpRequest->getMethod(),
            $httpRequest->getUrl(),
            $httpRequest->getOptions()
        );

        $httpRequest->addAttempt();
        $httpRequest->setLastAttemptedOn(Carbon::now());
        $httpRequest->setLastResponseCode($response->getStatusCode());
        $httpRequest->setLastResponse(json_encode($response->getContent(false)));

        if ($response->getStatusCode() >= 300) {
            $httpRequest->setSuccess(false);
        } else {
            $httpRequest->setSuccess(true);
        }

        $this->manager->flush();

        $this->retriedHttpRequestsLog->info(
            sprintf(
                'Retried request to: "%s". Result: %s. Response code: %s. Response content: %s',
                $httpRequest->getUrl(),
                $httpRequest->getSuccess() === true ? 'success' : 'fail',
                $httpRequest->getLastResponseCode(),
                $httpRequest->getLastResponse()
            )
        );
    }
}