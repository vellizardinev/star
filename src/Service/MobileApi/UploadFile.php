<?php


namespace App\Service\MobileApi;


use App\Entity\Document;
use App\Enum\ApiEndpoints;
use App\File\FileManager;
use App\Service\ImageManipulation;
use App\Service\MobileApi\Response\GetIdCardsResponse;
use CURLFile;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class UploadFile
{
    use CreatesAuthenticationHash;

    const UPLOAD_FILE = 'UploadFile';

    const FILE_TYPE_UKNOWN = '240';

    const FILE_TYPE_PASSPORT_FIRST_PAGE = '100760';
    const FILE_TYPE_PASSPORT_SECOND_PAGE = '100761';
    const FILE_TYPE_PASSPORT_PHOTO_AT_25 = '100762';
    const FILE_TYPE_PASSPORT_PASTED_PHOTO_AT_25 = '100763';
    const FILE_TYPE_PASSPORT_PHOTO_AT_45 = '100764';
    const FILE_TYPE_PASSPORT_PASTED_PHOTO_AT_45 = '100765';
    const FILE_TYPE_PASSPORT_SELFIE_WITH_PASSPORT = '100766';
    const FILE_TYPE_PASSPORT_PROOF_OF_RESIDENCE = '100768';
    const FILE_TYPE_ID_CARD_REGISTRATION_APPENDIX = '100771';
    const FILE_TYPE_ID_CARD_SELFIE = '100775';
    const FILE_TYPE_ID_CARD_PHOTO = '100769';
    const FILE_TYPE_ID_CARD_VALIDITY = '100770';
    const FILE_TYPE_PENSION_CERTIFICATE_FIRST_SPREAD = '100772';
    const FILE_TYPE_PENSION_CERTIFICATE_VALIDITY_RECORD = '100773';
    const FILE_TYPE_PENSION_CERTIFICATE_PHOTO_AND_VALIDITY_SPREAD = '100774';
    const FILE_TYPE_INDIVIDUAL_TAX_NUMBER = '100776';
    const FILE_TYPE_SELFIE = '100783';

    private $httpClient;

    private $mobileApiUrl;

    private $creditApplicationsLog;

    private $manager;

    private $fileManager;

    private $imageManipulation;

    public function __construct(
        string $mobileApiUrl,
        string $mobileApiUsername,
        string $mobileApiPassword,
        LoggerInterface $creditApplicationsLog,
        EntityManagerInterface $manager,
        FileManager $fileManager,
        ImageManipulation $imageManipulation
    ) {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
        $this->creditApplicationsLog = $creditApplicationsLog;
        $this->manager = $manager;
        $this->fileManager = $fileManager;
        $this->imageManipulation = $imageManipulation;
    }

    public function curlUpload(Document $document, string $erpId, string $type = self::FILE_TYPE_UKNOWN): bool
    {
        $curlFile = new CURLFile(
            $this->fileManager->systemFilePath($document),
            'image/' . $document->getMime(),
            'test_name'
        );

        $parameters = [
            "filename" => $erpId,
            "extension" => $document->getMime(),
            "type" => $type,
            "file" => $curlFile,
        ];

//        dd(json_encode($parameters, JSON_PRETTY_PRINT));

        $curlSession = curl_init();

        curl_setopt($curlSession, CURLOPT_URL, $this->getUploadUrl());

        curl_setopt($curlSession, CURLOPT_FRESH_CONNECT, true);

        curl_setopt($curlSession, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));

        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curlSession, CURLOPT_POST, true);

        curl_setopt($curlSession, CURLOPT_POSTFIELDS, $parameters);

        // Download the given URL, and return output
        $response = curl_exec($curlSession);

        if (curl_errno($curlSession)) {
            $errorText = curl_error($curlSession);

            $this->creditApplicationsLog->error(
                sprintf(
                    'Failed to upload file ID: %s related with online application ERP ID: %s. Error message: %s',
                    $document->getId(),
                    $erpId,
                    $errorText
                )
            );

            return false;
        }

        $responseData = json_decode($response);

        if ($responseData === null || (isset($responseData->RequestResult) && ($responseData->RequestResult !== 0 || $responseData->Result !== 1))) {
            $this->creditApplicationsLog->error(
                sprintf(
                    'Failed to upload file ID: %s related with online application ERP ID: %s. Response: %s',
                    $document->getId(),
                    $erpId,
                    $response
                )
            );

            return false;
        }

        $document->setIsSentToErp(true);
        $this->manager->flush();

        curl_close($curlSession);

        $this->creditApplicationsLog->info(
            sprintf(
                'Sent document with ID: %s to API. Parent application Erp ID: %s. Response=%s',
                $document->getId(),
                $erpId,
                $response
            )
        );

        return true;
    }

    public function uploadAsBase64String(Document $document, string $erpId, string $type = self::FILE_TYPE_UKNOWN): bool
    {
        if ($type === self::FILE_TYPE_SELFIE) {
            $compressedFilePath = $this->imageManipulation->compressToSize($document, 1363148);
            $file = base64_encode(file_get_contents($compressedFilePath));
            unlink($compressedFilePath);
        } else {
            $file = base64_encode(file_get_contents($this->fileManager->systemFilePath($document)));
        }

        $data = [
            'file' =>
                [
                    "CreditId" => $erpId,
                    "Extension" => $document->getMime(),
                    "Type" => $type,
                    "Content" => $file,
                ]
        ];

        $response = $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::UPLOAD_FILE_AS_BASE_64_STRING), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json'],
        ]);

        $this->creditApplicationsLog->info(
            sprintf(
                'Sent document with ID: %s to API. Parent application Erp ID: %s. Response=%s',
                $document->getId(),
                $erpId,
                $response->getContent(false)
            )
        );

        if ($response->getStatusCode() < 400) {
            $responseData = $response->toArray(false);

            if ($responseData['Result'] === 1) {
                $document->setIsSentToErp(true);
                $this->manager->flush();

                return true;
            }
        }

        return false;
    }

    /**
     * @param string $creditId
     * @return GetIdCardsResponse
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function fileStatusForCredit(string $creditId): GetIdCardsResponse
    {
        $data = [
            "creditID" => $creditId,
        ];
//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::GET_IDCARDS_BY_CREDIT_ID),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->creditApplicationsLog->info(
            sprintf(
                'Called GetIDCardsByCreditID for credit ID: %s. Response: %s',
                $creditId,
                $response->getContent()
            )
        );

//        if (!empty($response->toArray()['Result'])) {
//            dd($response->toArray());
//        }

        return new GetIdCardsResponse($response->toArray());
    }

    private function getUploadUrl(): string
    {
        return $this->mobileApiUrl . self::UPLOAD_FILE;
    }

    /**
     * @param string $action
     * @return string|null
     */
    private function getEndpoint(string $action)
    {
        return $this->mobileApiUrl . $action;
    }
}