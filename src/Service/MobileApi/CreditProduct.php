<?php


namespace App\Service\MobileApi;


use App\Entity\CreditData;

class CreditProduct
{
    public $creditPeriod;

    public $creditPeriodId;

    public $productId;

    public $productName;

    public $amount;

    public $periods;

    public $payment;

    public $description;

    public $total;

    public function __construct(array $data)
    {
        $this->creditPeriod = $data['CreditPeriod'];
        $this->creditPeriodId = $data['CreditPeriodID'];
        $this->productId = $data['ProductID'];
        $this->productName = $data['ProductName'];
        $this->amount = $data['SumFrom'];
        $this->periods = $data['CreditPeriod'];
        $this->payment = $data['PaymentSmall'];
        $this->description = $data['Description'];
        $this->total = $data['PaymentSmall'] * $data['CreditPeriod'];
    }

    /**
     * @param array $creditProductsData
     * @return CreditProduct[]
     */
    public static function collection(array $creditProductsData): array
    {
        $result = [];

        foreach ($creditProductsData as $data) {
            $result[] = new self($data);
        }

        return $result;
    }

    public function creditData(): CreditData
    {
        return (new CreditData)
            ->setAmount($this->amount)
            ->setPeriods($this->periods)
            ->setPayment($this->payment)
            ->setPaymentWithPenalty($this->payment)
            ->setTotal($this->total)
            ->setTotalWithPenalty($this->total)
            ->setInterestRateFirstPeriod($this->payment)
            ->setInterestRateSecondPeriod($this->payment)
            ->setAprc($this->payment)
            ->setCreditPeriodId($this->creditPeriodId)
            ->setProductId($this->productId);
    }
}