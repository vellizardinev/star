<?php


namespace App\Service\MobileApi;


use App\Entity\Credit;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class UpdateCreditData
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param CreditProduct[] $data
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function update(array $data): void
    {
        $highestId = $this->manager
            ->createQueryBuilder()
            ->select('Max(credit_data.id)')
            ->from('App\\Entity\\CreditData', 'credit_data')
            ->getQuery()
            ->getSingleScalarResult();

        $count = 1;

        foreach ($data as $creditProduct) {
            $creditData = $creditProduct->creditData();

            $creditName = $creditProduct->description === 'Money Shop Weekly' ? 'iWeek' : $creditProduct->description;

            $credit = $this->manager->getRepository(Credit::class)->findOneBy(['title' => $creditName]);

            if (null === $credit) {
                continue;
            }

            $creditData->setCredit($credit)->setPaymentSchedule($credit->getPaymentSchedule());

            $this->manager->persist($creditData);

            if ($count % 100 === 0) {
                $this->manager->flush();
            }

            $count++;
        }

        $this->manager->flush();

        // Delete old credit data entries
        if ($highestId !== null) {
            $q = $this->manager->createQuery('delete from App\\Entity\\CreditData credit_data where credit_data.id <= :highestId');
            $q->setParameter('highestId', $highestId);
            $q->execute();
        }
    }
}