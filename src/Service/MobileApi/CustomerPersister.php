<?php


namespace App\Service\MobileApi;


use App\Entity\Customer;
use Symfony\Component\HttpFoundation\RequestStack;

class CustomerPersister
{
    const CUSTOMER_SESSION_KEY = 'logged_customer';

    private $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getMasterRequest();
    }

    public function get(): Customer
    {
        $serializedCustomer = $this->request->getSession()->get(self::CUSTOMER_SESSION_KEY, null);

        if ($serializedCustomer === null) {
            return new Customer();
        } else {
            return unserialize($serializedCustomer);
        }
    }

    public function save(Customer $customer)
    {
        $serializedCustomer = serialize($customer);
        $this->request->getSession()->set(self::CUSTOMER_SESSION_KEY, $serializedCustomer);
    }
}