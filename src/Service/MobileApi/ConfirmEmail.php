<?php


namespace App\Service\MobileApi;


use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ConfirmEmail
{
    use CreatesAuthenticationHash;

    const CHANNEL = 'F93C4B69-EA31-461F-B533-8411459AD1CD';

    const CLIENT_VERSION = '1.0';
    const PLATFORM_TYPE = 1;

    const ENDPOINT = 'EmailConfirmation';

    private $httpClient;

    private $mobileApiUrl;

    private $manager;

    private $logger;

    public function __construct(
        EntityManagerInterface $manager,
        string $mobileApiUrl,
        string $mobileApiUsername,
        string $mobileApiPassword,
        LoggerInterface $creditApplicationsLog
    ) {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
        $this->manager = $manager;
        $this->logger = $creditApplicationsLog;
    }

    /**
     * @param string $confirmationId
     * @return bool
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function checkEmailConfirmation(string $confirmationId): bool
    {
        $data = array(
            "ConfirmationID" => strtolower($confirmationId),
            "Timestamp" => time(),
        );

        $hash = $this->generateHash($data);

        $parameters = [
            "Hash" => $hash,
            "ClientVersion" => self::CLIENT_VERSION,
            "PlatformType" => self::PLATFORM_TYPE,
        ];

        $data = array_merge($data, $parameters);
        $data = ['confirm' => $data];

//         dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->logger->info(
            sprintf(
                'Called EmailConfirmation with ConfirmationId: %s. Response: %s',
                $confirmationId,
                $response->getContent()
            )
        );

        $responseData = $response->toArray();

        return $responseData['Result'] ===  true;

    }

    private function getEndpoint(): string
    {
        return $this->mobileApiUrl . self::ENDPOINT;
    }
}
