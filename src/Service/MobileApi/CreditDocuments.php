<?php


namespace App\Service\MobileApi;


use App\Entity\CreditDocument;
use App\Enum\ApiEndpoints;
use App\Enum\CreditDocumentType;
use App\File\FileManager;
use App\Service\MobileApi\DataTransferObject\DocumentType;
use App\Service\MobileApi\Response\GetInitialDocumentTypesResponse;
use App\Service\MobileApi\Response\GetRegularDocumentTypesResponse;
use App\Service\MobileApi\Response\SignDocumentsResponse;
use App\Service\MobileApi\Response\StoredDocumentsListResponse;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\FileExistsException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class CreditDocuments
{
    const CHANNEL = 'F93C4B69-EA31-461F-B533-8411459AD1CD';

    const DOCUMENT_TYPE_SEF = 105;
    const DOCUMENT_TYPE_OFFER = 1672;
    const DOCUMENT_TYPE_OFFER_NAME = 'Oferta.pdf';

    private $httpClient;

    private $mobileApiUrl;

    private $manager;

    private $fileManager;

    private $logger;

    private $useDummyCreditDocuments;

    public function __construct(EntityManagerInterface $manager, FileManager $fileManager, string $mobileApiUrl, string $mobileApiUsername, string $mobileApiPassword, LoggerInterface $creditApplicationsLog, bool $useDummyCreditDocuments)
    {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
        $this->manager = $manager;
        $this->fileManager = $fileManager;
        $this->logger = $creditApplicationsLog;
        $this->useDummyCreditDocuments = $useDummyCreditDocuments;
    }

    /**
     * @param string $erpId
     * @return CreditDocument[]
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws FileExistsException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getInitialDocuments(string $erpId): array
    {
        // First get the document types from API
        $initialDocumentTypes = $this->getInitialDocumentTypes()->getDocumentTypes();
        $documents = [];

        // Then download and persist them
        foreach ($initialDocumentTypes as $documentType) {
            $documents[] = $this->getSingleDocument($erpId, $documentType);
        }

        return $documents;
    }

    /**
     * @param string $erpId
     * @return CreditDocument[]
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws FileExistsException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getInitialDocumentsOnApproval(string $erpId): array
    {
        // First get the document types from API
        $initialDocumentTypes = $this->getInitialDocumentTypesOnApproval()->getDocumentTypes();
        $documents = [];

        // Then download and persist them
        foreach ($initialDocumentTypes as $documentType) {
            $documents[] = $this->getSingleDocument($erpId, $documentType);
        }

        return $documents;
    }

    /**
     * @param string $erpId
     * @param string $code
     * @param string $ipAddress
     * @return SignDocumentsResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function signInitialDocuments(string $erpId, string $code, string $ipAddress): SignDocumentsResponse
    {
        $data = [
            'channel' => self::CHANNEL,
            'creditID' => $erpId,
            'smsCode' => $code,
            'ipAddress' => $ipAddress,
        ];

//         dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::SIGN_INITIAL_DOCUMENTS), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $this->logger->info(
            sprintf(
                'Called SignInitialDocuments for Credit %s with Code: %s. Response: %s',
                $erpId,
                $code,
                $response->getContent()
            )
        );

        $responseData = $response->toArray();

        return new SignDocumentsResponse($responseData);
    }

    /**
     * @param string $erpId
     * @return array|mixed
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws FileExistsException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getCreditOffer(string $erpId)
    {
        // First check to see if we have already downloaded the offer for this credit application
        $offerArray = $this->manager
            ->getRepository(CreditDocument::class)
            ->forCreditApplication($erpId, CreditDocumentType::CREDIT_OFFER);

        if (!empty($offerArray)) {
            return array_pop($offerArray);
        }

        $documentType = new DocumentType(self::DOCUMENT_TYPE_OFFER, self::DOCUMENT_TYPE_OFFER_NAME);

        return $this->getSingleDocument($erpId, $documentType, CreditDocumentType::CREDIT_OFFER);
    }

    /**
     * @param string $erpId
     * @param $code
     * @param string|null $ipAddress
     * @return SignDocumentsResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function acceptCreditOffer(string $erpId): bool
    {
        $data = [
            'channel' => self::CHANNEL,
            'creditID' => $erpId,
        ];

//         dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::CONFIRM_OFFER), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $this->logger->info(
            sprintf(
                'Called ConfirmOffer for Credit with ID: %s. Response: %s',
                $erpId,
                $response->getContent()
            )
        );

        $responseData = $response->toArray();

        return $responseData['Result'];
    }

    /**
     * @param string $erpId
     * @return array|mixed
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws FileExistsException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getRegularDocuments(string $erpId)
    {
        // First check to see of we have already downloaded the regular documents for this credit application
        $regularDocuments = $this->manager
            ->getRepository(CreditDocument::class)
            ->forCreditApplication($erpId, CreditDocumentType::REGULAR);

        if (!empty($regularDocuments)) {
            return $regularDocuments;
        }

        // If not, first get the document types from API
        $regularDocumentTypes = $this->getRegularDocumentTypes($erpId)->getDocumentTypes();
        $documents = [];

        // Then download and persist them
        foreach ($regularDocumentTypes as $documentType) {
            $documents[] = $this->getSingleDocument($erpId, $documentType, CreditDocumentType::REGULAR);
        }

        return $documents;
    }

    /**
     * @param string $erpId
     * @param $code
     * @param string|null $ipAddress
     * @return SignDocumentsResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function signRegularDocuments(string $erpId, $code, ?string $ipAddress): SignDocumentsResponse
    {
        $data = [
            'channel' => self::CHANNEL,
            'creditID' => $erpId,
            'smsCode' => $code,
            'ipAddress' => $ipAddress,
        ];

//         dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::SIGN_REGULAR_DOCUMENTS), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $this->logger->info(
            sprintf(
                'Called SignRegularDocuments for Credit %s with Code: %s. Response: %s',
                $erpId,
                $code,
                $response->getContent()
            )
        );

        $responseData = $response->toArray();

        return new SignDocumentsResponse($responseData);
    }
    
    public function storedDocumentsList(string $erpId): StoredDocumentsListResponse
    {
        $data = [
            'channel' => self::CHANNEL,
            'creditID' => $erpId,
        ];

        $response = $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::GET_CREDIT_STORED_DOCUMENTS), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $responseData = $response->toArray();

//        dd($responseData);

        return new StoredDocumentsListResponse($responseData);
    }

    public function getStoredDocument(string $documentId): ResponseInterface
    {
        $data = [
            'channel' => self::CHANNEL,
            'docID' => $documentId,
        ];

//         dd(json_encode($data, JSON_PRETTY_PRINT));

        return $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::GET_STORED_DOCUMENT), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);
    }

    /**
     * @return GetInitialDocumentTypesResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getInitialDocumentTypes(): GetInitialDocumentTypesResponse
    {
        $data = ['channel' => self::CHANNEL];

        $response = $this->httpClient->request('GET', $this->getEndpoint(ApiEndpoints::GET_INITIAL_DOCUMENT_TYPES), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $responseData = $response->toArray();

        return new GetInitialDocumentTypesResponse($responseData);
    }

    /**
     * @return GetInitialDocumentTypesResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getInitialDocumentTypesOnApproval(): GetInitialDocumentTypesResponse
    {
        $data = ['channel' => self::CHANNEL];

        $response = $this->httpClient->request('GET', $this->getEndpoint(ApiEndpoints::GET_INITIAL_DOCUMENT_TYPES_ON_APPROVAL), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $responseData = $response->toArray();

        return new GetInitialDocumentTypesResponse($responseData);
    }

    /**
     * @param string $erpId
     * @return GetRegularDocumentTypesResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getRegularDocumentTypes(string $erpId): GetRegularDocumentTypesResponse
    {
        $data = [
            'channel' => self::CHANNEL,
            'creditID' => $erpId,
        ];

        $response = $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::GET_REGULAR_DOCUMENT_TYPES), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

//        dd($response->getContent());

        $responseData = $response->toArray();

        return new GetRegularDocumentTypesResponse($responseData);
    }

    /**
     * @param string $action
     * @return string|null
     */
    private function getEndpoint(string $action)
    {
        return $this->mobileApiUrl . $action;
    }

    /**
     * @param string $erpId
     * @param DocumentType $documentType
     * @param string $creditDocumentType
     * @return CreditDocument
     * @throws ClientExceptionInterface
     * @throws FileExistsException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getSingleDocument(string $erpId, DocumentType $documentType, string $creditDocumentType = CreditDocumentType::INITIAL): CreditDocument
    {
        $data = [
            'channel' => self::CHANNEL,
            'creditID' => $erpId,
            'docType' => $documentType->getKey(),
        ];

//         dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::GET_SINGLE_DOCUMENT), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        if ($this->useDummyCreditDocuments === false) {
            $content = $response->getContent();
            $extension = $documentType->fileExtension();
        } else {
            $content = 'This is a dummy credit document for testing purposes only.';
            $extension = 'txt';
        }

        $response->cancel();

        $fileName = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $documentType->fileNameNoExtension() . '_' . $erpId . '.' . $extension;
        file_put_contents($fileName, $content);
        $uploadedFile = new UploadedFile($fileName, $fileName);

        $creditDocument = $this->fileManager->uploadCreditDocument($uploadedFile);
        $creditDocument->setOriginalName($documentType->fileNameNoExtension());
        $creditDocument->setErpId($erpId);
        $creditDocument->setType($creditDocumentType);
        $creditDocument->setErpKey($documentType->getKey());

        $this->manager->persist($creditDocument);
        $this->manager->flush();

        unlink($fileName);

        return $creditDocument;
    }
}
