<?php


namespace App\Service\MobileApi;


use App\Entity\Credit;
use App\Entity\CreditData;
use App\Entity\CreditShortApplication;
use App\Entity\Customer;
use App\Enum\ApiEndpoints;
use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SavePPZShort
{
    use CreatesAuthenticationHash;
    use ConvertsDatesToString;

    const CHANNEL = 'F93C4B69-EA31-461F-B533-8411459AD1CD';
    const CLIENT_VERSION = '1.0';
    const PLATFORM_TYPE = 1;

    const NOT_AVAILABLE = 'not_available';

    private $httpClient;

    private $mobileApiUrl;

    private $creditApplicationsLog;

    private $manager;

    private $translator;

    public function __construct(string $mobileApiUrl, string $mobileApiUsername, string $mobileApiPassword, LoggerInterface $creditApplicationsLog, EntityManagerInterface $manager, TranslatorInterface $translator)
    {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
        $this->creditApplicationsLog = $creditApplicationsLog;
        $this->manager = $manager;
        $this->translator = $translator;
    }

    /**
     * @param CreditShortApplication $application
     * @param Customer|null $customer
     * @return bool
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function saveShortApplication(CreditShortApplication $application, ?Customer $customer = null): bool
    {
        $data = array(
            'FirstName' => $customer !== null ? $customer->fullName() : $application->getName(),
            'LastName' => self::NOT_AVAILABLE,
            'MobilePhone' => $application->getTelephone(),
            'Timestamp' => time(),
        );

        $data['Hash'] = $this->generateHash($data);

        $creditData = $this->getCreditData($application->getCredit(), $application->getAmount());

        $parameters = [
            'SecondName' => self::NOT_AVAILABLE,
            'EGN' => is_null($application->getPersonalIdentificationNumber()) ? '' : $application->getPersonalIdentificationNumber(),
            'Email' => is_null($application->getEmail()) ? '' : $application->getEmail(),
            'MobilePhone' => '0' . $application->getTelephone(),
            'CreditSum' => is_null($application->getAmount()) ? 0 : $application->getAmount(),
            'SubSource' => $this->getSource($application),
            'BirthDate' => $this->dateToString($this->calculateBirthDate($application->getPersonalIdentificationNumber())),
            'ForRefinance' => $application->getIsRefinance(),
            'Address' => $application->getCity(),
            'Channel' => self::CHANNEL,
            'ClientVersion' => self::CLIENT_VERSION,
            'PlatformType' => self::PLATFORM_TYPE,
            'RequestResult' => 0
        ];

        if ($creditData) {
            $parameters['CreditProduct'] = $creditData->getProductId();
            $parameters['CreditPeriod'] = $creditData->getCreditPeriodId();
            $parameters['CreditPeriodValue'] = $creditData->getPeriods();
        }

        $data = array_merge($parameters, $data);
        $data = ['call' => $data];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::SAVE_PPZ_SHORT), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        if ($response->getStatusCode() >= 500) {
            $content = $response->getContent(false);
        } else {
            $content = json_encode($response->toArray(false), JSON_PRETTY_PRINT);
        }

        $application->setJsonData(json_encode($data, JSON_PRETTY_PRINT));
        $application->setApiResponse($content);

        $this->manager->flush();

        if ($response->getStatusCode() >= 300) {
            $this->creditApplicationsLog->error(sprintf('Failed to send Short PPZ to API. Response=%s', $response->getContent(false)));

            return false;
        }

        $responseData = $response->toArray();

        if ($responseData['RequestResult'] !== 0 || $responseData['Result'] !== true) {
            $this->creditApplicationsLog->error(sprintf('Failed to send Short PPZ to API. Response=%s', $response->getContent(false)));

            return false;
        }

        $this->creditApplicationsLog->info(sprintf('Sent Short PPZ to API. Response=%s', $response->getContent()));

        return true;
    }

    /**
     * @param string $action
     * @return string
     */
    private function getEndpoint(string $action)
    {
        return $this->mobileApiUrl . $action;
    }

    private function getCreditData(?Credit $credit, ?float $amount): ?CreditData
    {
        if (!$credit || !$amount) {
            return null;
        }

        return $this->manager
            ->getRepository(CreditData::class)
            ->findOneBy([
                'credit' => $credit,
                'amount' => $amount,
            ]);
    }

    private function getSource(CreditShortApplication $object)
    {
        if ($object->getAffiliate() !== null) {
            return $this->translator->trans('affiliate', ['%name%' => $object->getAffiliate()->getName()], 'call_flow');
        }

        if ($object->getLanding() !== null) {
            return $this->translator->trans('landing_page', ['%name%' => $object->getLanding()->getName()], 'call_flow');
        }

        return $this->translator->trans('website', [], 'call_flow');
    }

    private function calculateBirthDate(?string $pin): ?DateTimeInterface
    {
        if (null === $pin) {
            return null;
        }

        $days = intval(substr($pin, 0, 5));

        return Carbon::create(1900, 1, 1)->addDays($days);
    }
}
