<?php


namespace App\Service\MobileApi;


use App\Entity\Customer;
use App\Entity\OnlineApplication\OnlineApplication;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AllowCustomerToApplyOnline
{
    private $manager;

    private $creditsService;

    private $allowPPZApplicationService;

    public function __construct(EntityManagerInterface $manager, CustomerCredits $creditsService, AllowPPZApplication $allowPPZApplicationService)
    {
        $this->manager = $manager;
        $this->creditsService = $creditsService;
        $this->allowPPZApplicationService = $allowPPZApplicationService;
    }

    /**
     * @param Customer $customer
     * @return bool
     * @throws ReflectionException
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function isDenied(Customer $customer): bool
    {
        // Deny if there are applications in the ERP
        $response = $this->creditsService->getCredits($customer);

        $creditApplications = $response->getCreditApplications();

        if ($creditApplications->count() > 0) {
            return true;
        }

        // Deny if there are credit applications in our DB
        $localCreditApplications = $this->manager
            ->getRepository(OnlineApplication::class)
            ->forEmail($customer->getEmail());

        if (count($localCreditApplications) > 0) {
            return true;
        }

        // Deny if ERP does not allow for some other reason
        if ($this->allowPPZApplicationService->isDenied($customer)) {
            return true;
        }

        return false;
    }
}