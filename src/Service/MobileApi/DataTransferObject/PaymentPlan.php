<?php


namespace App\Service\MobileApi\DataTransferObject;


use Doctrine\Common\Collections\ArrayCollection;

class PaymentPlan
{
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return ArrayCollection|PaymentPlanItem[]
     */
    public function getItems(): ArrayCollection
    {
        return $this->items;
    }

    public function setItems(array $data): self
    {
        foreach ($data as $item) {
            $paymentPlanItem = (new PaymentPlanItem)
                ->setPayment($item['PMT'])
                ->setDeadline($item['CurrentDate'])
                ->setPaid($item['PaidSum'])
                ->setDaysDelay($item['CurrentDelay'])
                ->setIndex($item['CurrentNumber'])
            ;

            $this->items->add($paymentPlanItem);
        }

        return $this;
    }
}