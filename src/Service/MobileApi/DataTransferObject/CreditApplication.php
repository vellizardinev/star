<?php


namespace App\Service\MobileApi\DataTransferObject;


use App\Entity\OnlineApplication\OnlineApplication;
use App\Enum\ActiveCreditStatus;
use Carbon\Carbon;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class CreditApplication
{
    private $amount;

    private $applicationDate;

    private $status;

    private $creditId;

    private $applicationFileStatuses;

    private $daysDelayForSigning;

    private $isOnlineChannel = false;

    /**
     * CreditApplication constructor.
     */
    public function __construct()
    {
        $this->applicationFileStatuses = new ArrayCollection();
    }

    public static function fromOnlineApplication(OnlineApplication $onlineApplication)
    {
        return (new self)
            ->setAmount($onlineApplication->getCreditDetails()->getAmount())
            ->setCreditId($onlineApplication->getErpId())
            ->setStatus(intval(ActiveCreditStatus::PENDING_CHANGES))
            ->setApplicationDate($onlineApplication->getCreatedAt())
            ;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getApplicationDate(): Carbon
    {
        return $this->applicationDate;
    }

    public function setApplicationDateFromString(?string $applicationDate): self
    {
        $this->applicationDate = $this->extractDate($applicationDate);

        return $this;
    }

    public function setApplicationDate(DateTimeInterface $date): self
    {
        $this->applicationDate = Carbon::instance($date);

        return $this;
    }

    public function getStatus(): ?int
    {
        if ($this->status === intval(ActiveCreditStatus::PENDING_SIGNING_INITIAL_DOCUMENTS) &&
            $this->isOnlineChannel() === false) {
            return intval(ActiveCreditStatus::APPROVED);
        }

        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreditId(): ?string
    {
        return $this->creditId;
    }

    public function setCreditId(?string $creditId): self
    {
        $this->creditId = $creditId;

        return $this;
    }

    public function setApplicationFileStatuses(ArrayCollection $statuses): self
    {
        $this->applicationFileStatuses = $statuses;

        return $this;
    }

    public function getApplicationFileStatuses(): Collection
    {
        return $this->applicationFileStatuses;
    }

    public function hasRejectedFiles(): bool
    {
        foreach ($this->applicationFileStatuses as $applicationFileStatus) {
            /** @var ApplicationFileStatus $applicationFileStatus */
            if ($applicationFileStatus->isRejected()) {
                return true;
            }
        }

        return false;
    }

    public function isPendingSigningInitialDocs(): bool
    {
        return $this->getStatus() === intval(ActiveCreditStatus::PENDING_SIGNING_INITIAL_DOCUMENTS) &&
            $this->isOnlineChannel() === true
            ;
    }

    public function isPendingSigningInitialDocsOnApproval(): bool
    {
        return $this->getStatus() === intval(ActiveCreditStatus::PENDING_SIGNING_INITIAL_DOCS_ON_APPROVAL) &&
            $this->isOnlineChannel() === true
            ;
    }

    public function isPendingOfferConfirmation(): bool
    {
        return $this->getStatus() === intval(ActiveCreditStatus::PENDING_CONFIRM_OFFER) &&
            $this->isOnlineChannel() === true
            ;
    }

    public function isApproved(): bool
    {
        return $this->getStatus() === intval(ActiveCreditStatus::APPROVED) &&
            $this->isOnlineChannel() === true
            ;
    }

    public function showCreditDocuments(): bool
    {
        if ($this->isOnlineChannel() === false) {
            return false;
        }

        if ($this->getStatus() === intval(ActiveCreditStatus::PENDING_CHANGES) ||
            $this->getStatus() === intval(ActiveCreditStatus::REJECTED) ||
            $this->getStatus() === intval(ActiveCreditStatus::CORRECTION_NEEDED)
        ) {
            return true;
        }

        return false;
    }

    public function isWaitingForReview(): bool
    {
        return $this->getStatus() === intval(ActiveCreditStatus::PENDING_CHANGES) &&
            $this->isOnlineChannel() === true
            ;
    }

    public function isPendingSigningRegularDocs(): bool
    {
        return $this->getStatus() === intval(ActiveCreditStatus::PENDING_SIGNING_REGULAR_DOCS) &&
            $this->isOnlineChannel() === true
            ;
    }

    public function needsCorrection(): bool
    {
        return $this->getStatus() === intval(ActiveCreditStatus::CORRECTION_NEEDED);
    }

    private function extractDate(?string $dateString): ?DateTimeInterface
    {
        if ($dateString === null) {
            return null;
        }

        preg_match('/([\-]?\d+)(\d{3})([\+\-]\d{4})/', $dateString, $matches);

        $timestamp = $matches[1] . $matches[3];

        return Carbon::createFromTimestamp($timestamp);
    }

    public function getDaysDelayForSigning(): ?int
    {
        return $this->daysDelayForSigning;
    }

    public function setDaysDelayForSigning(?int $daysDelayForSigning): self
    {
        $this->daysDelayForSigning = $daysDelayForSigning;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOnlineChannel(): bool
    {
        return $this->isOnlineChannel;
    }

    /**
     * @param bool $isOnlineChannel
     * @return CreditApplication
     */
    public function setIsOnlineChannel(bool $isOnlineChannel): self
    {
        $this->isOnlineChannel = $isOnlineChannel;

        return $this;
    }
}