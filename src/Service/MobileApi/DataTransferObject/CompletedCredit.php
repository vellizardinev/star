<?php


namespace App\Service\MobileApi\DataTransferObject;


use Carbon\Carbon;
use DateTimeInterface;

class CompletedCredit
{
    private $contractCode;

    private $amount;

    private $dateCompleted;

    private $creditId;

    public function getContractCode(): string
    {
        return $this->contractCode;
    }

    public function setContractCode(string $contractCode): self
    {
        $this->contractCode = $contractCode;

        return $this;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDateCompleted(): Carbon
    {
        return $this->dateCompleted;
    }

    public function setDateCompleted(?string $dateCompleted): self
    {
        $this->dateCompleted = $this->extractDate($dateCompleted);

        return $this;
    }

    private function extractDate(?string $dateString): ?DateTimeInterface
    {
        if ($dateString === null) {
            return null;
        }

        preg_match('/([\-]?\d+)(\d{3})([\+\-]\d{4})/', $dateString, $matches);

        $timestamp = $matches[1] . $matches[3];

        return Carbon::createFromTimestamp($timestamp);
    }

    public function getCreditId(): string
    {
        return $this->creditId;
    }

    public function setCreditId(string $creditId)
    {
        $this->creditId = $creditId;

        return $this;
    }
}