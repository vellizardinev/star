<?php


namespace App\Service\MobileApi\DataTransferObject;


use App\Enum\PaymentPlanItemStatus;
use Carbon\Carbon;
use DateTimeInterface;

class PaymentPlanItem
{
    private $deadline;

    private $payment;

    private $paid;

    private $daysDelay;

    private $index;

    public function getDeadline(): Carbon
    {
        return $this->deadline;
    }

    public function setDeadline(?string $deadline): self
    {
        $this->deadline = $this->extractDate($deadline);

        return $this;
    }

    public function getPayment(): ?float
    {
        return $this->payment;
    }

    public function setPayment(?float $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getPaid(): ?float
    {
        return $this->paid;
    }

    public function setPaid(?float $paid): self
    {
        $this->paid = $paid;

        return $this;
    }

    public function getStatus(): string
    {
        if ($this->getDeadline()->greaterThan(Carbon::now())) {
            return PaymentPlanItemStatus::NOT_REACHED_DEADLINE;
        } else {
            if ($this->getPaid() === 0.0) {
                return PaymentPlanItemStatus::NOT_PAID;
            } else {
                if ($this->getPaid() < $this->getPayment()) {
                    return PaymentPlanItemStatus::PARTIALLY_PAID;
                } else {
                    if ($this->getPaid() >= $this->getPayment()) {
                        return PaymentPlanItemStatus::PAID;
                    }
                }
            }
        }

        return PaymentPlanItemStatus::NOT_REACHED_DEADLINE;
    }

    public function getDaysDelay(): ?int
    {
        return $this->daysDelay;
    }

    public function setDaysDelay(?int $daysDelay): self
    {
        $this->daysDelay = $daysDelay;

        return $this;
    }

    public function getIndex(): ?int
    {
        return $this->index;
    }

    public function setIndex(?int $index): self
    {
        $this->index = $index;

        return $this;
    }

    private function extractDate(?string $dateString): ?DateTimeInterface
    {
        if ($dateString === null) {
            return null;
        }

        preg_match('/([\-]?\d+)(\d{3})([\+\-]\d{4})/', $dateString, $matches);

        $timestamp = $matches[1] . $matches[3];

        return Carbon::createFromTimestamp($timestamp);
    }
}