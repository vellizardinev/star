<?php


namespace App\Service\MobileApi\DataTransferObject;


class ActiveCredit
{
    private $contractCode;

    private $amount;

    private $payment;

    private $employee;

    private $employeeTelephone;

    private $iban;

    private $paymentPlan;

    private $paymentUrl;

    private $fines;

    private $creditId;

    private $isOnlineChannel;

    public function getContractCode(): string
    {
        return $this->contractCode;
    }

    public function setContractCode(string $contractCode): self
    {
        $this->contractCode = $contractCode;

        return $this;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPayment(): float
    {
        return $this->payment;
    }

    public function setPayment(float $payment): ActiveCredit
    {
        $this->payment = $payment;

        return $this;
    }

    public function getEmployee(): ?string
    {
        return $this->employee;
    }

    public function setEmployee(?string $employee): self
    {
        $this->employee = $employee;

        return $this;
    }

    public function getEmployeeTelephone(): ?string
    {
        return $this->employeeTelephone;
    }

    public function setEmployeeTelephone(?string $employeeTelephone): self
    {
        $this->employeeTelephone = $employeeTelephone;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getPaymentPlan(): ?PaymentPlan
    {
        return $this->paymentPlan;
    }

    public function setPaymentPlan(array $paymentPlanData): self
    {
        $this->paymentPlan = new PaymentPlan;
        $this->paymentPlan->setItems($paymentPlanData);

        return $this;
    }

    public function getPaymentUrl(): ?string
    {
        return $this->paymentUrl;
    }

    public function setPaymentUrl(?string $paymentUrl): self
    {
        $this->paymentUrl = $paymentUrl;

        return $this;
    }

    public function getFines()
    {
        return $this->fines;
    }

    public function setFines($fines): self
    {
        $this->fines = $fines;

        return $this;
    }

    public function getCreditId(): string
    {
        return $this->creditId;
    }

    public function setCreditId(string $creditId): self
    {
        $this->creditId = $creditId;

        return $this;
    }

    public function getIsOnlineChannel(): ?bool
    {
        return $this->isOnlineChannel;
    }

    public function setIsOnlineChannel(bool $isOnlineChannel): self
    {
        $this->isOnlineChannel = $isOnlineChannel;

        return $this;
    }
}