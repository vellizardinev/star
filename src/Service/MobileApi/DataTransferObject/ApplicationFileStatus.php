<?php


namespace App\Service\MobileApi\DataTransferObject;


use App\Enum\CreditApplicationFileStatus;

class ApplicationFileStatus
{
    private $fileType;

    private $status;

    private $fileName;

    public function getFileType(): string
    {
        return $this->fileType;
    }

    public function setFileType(?string $fileType): self
    {
        $this->fileType = $fileType;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(?string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function isRejected(): bool
    {
        if ($this->status === CreditApplicationFileStatus::REJECTED) {
            return true;
        }

        return false;
    }
}