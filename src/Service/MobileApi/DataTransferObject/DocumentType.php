<?php


namespace App\Service\MobileApi\DataTransferObject;


class DocumentType
{
    private $key;

    private $name;

    public function __construct(int $key, string $name)
    {
        $this->key = $key;
        $this->name = $name;
    }

    public function getKey(): int
    {
        return $this->key;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function fileNameNoExtension(): ?string
    {
        if (null === $this->name) {
            return null;
        }

        $parts = pathinfo($this->name);

        return $parts['filename'];
    }

    public function fileNameLowercase(): ?string
    {
        if (null === $this->name) {
            return null;
        }

        $parts = pathinfo($this->name);

        return strtolower($parts['filename']);
    }

    public function fileExtension()
    {
        if (null === $this->name) {
            return null;
        }

        $parts = pathinfo($this->name);

        return $parts['extension'];
    }
}