<?php


namespace App\Service\MobileApi;


trait CreatesAuthenticationHash
{
    public function generateHash(array $parameters): string
    {
        $textToHash = $this->generateAuthenticationString($parameters);

        return hash("sha256", $textToHash, false);
    }

    public function generateBaseDataHash(int $timestamp): string
    {
        $timestampRemainder = $timestamp;
        $timestampSum = 0;

        while (intval($timestampRemainder) != 0) {
            $timestampSum += ($timestampRemainder % 10);
            $timestampRemainder /= 10;
        }

        $dataToHash = [
            (float) '1.0',
            $timestamp,
            $timestampSum,
        ];

        $textToHash = $this->generateAuthenticationString($dataToHash);

        return hash("sha256", $textToHash, false);
    }

    private function generateAuthenticationString(array $parameters): string
    {
        $result = '';

        foreach ($parameters as $key => $item) {

            if ($item == null) {
                continue;
            }

            if (is_string($item) && !empty($item)) {
                mb_internal_encoding('UTF-8');
                $result .= (mb_strlen($item)) . $item;
            } else {
                $result .= (string)$item;
            }
        }

        return $result;
    }
}