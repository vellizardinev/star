<?php


namespace App\Service\MobileApi;


use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CreditProducts
{
    const GET_CREDIT_PRODUCTS = 'GetCreditProducts';

    const CHANNEL = 'F93C4B69-EA31-461F-B533-8411459AD1CD';

    private $httpClient;

    private $mobileApiUrl;

    public function __construct(string $mobileApiUrl, string $mobileApiUsername, string $mobileApiPassword)
    {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
    }

    /**
     * @return CreditProduct[]
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function creditProducts(): array
    {
        $data = ['channel' => self::CHANNEL];

        $response = $this->httpClient->request('POST', $this->creditProductsUrl(), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $responseData = json_decode($response->getContent(), true);
        $creditProductsData = $responseData['CreditProducts'];

        return CreditProduct::collection($creditProductsData);
    }

    private function creditProductsUrl(): string
    {
        return $this->mobileApiUrl . self::GET_CREDIT_PRODUCTS;
    }
}