<?php


namespace App\Service\MobileApi;


use Carbon\Carbon;
use DateTimeInterface;

trait ConvertsDatesToString
{
    /**
     * @param DateTimeInterface|null $date
     * @return string
     */
    public function dateToString(?DateTimeInterface $date): ?string
    {
        if ($date === null) {
            return null;
        }

        $timestamp = $date->getTimestamp();

        $timezone = timezone_open('Europe/Kiev');
        $dateTimeGMT = clone $date;
        $dateTimeGMT->setTimeZone(timezone_open("Etc/GMT"));
        $dateTimeGMT->setTimestamp($timestamp);

        // Get offset in hours
        $GMTSecondsOffset = timezone_offset_get($timezone, $dateTimeGMT);
        $GMTHourOffset = $GMTSecondsOffset / 3600;
        $GMTHourOffset = sprintf("%02d", $GMTHourOffset);

        // Apply offset to timestamp
        if (0 > $GMTHourOffset) {
            $sign = "-";
        } else {
            $sign = "+";
        }

        return '/Date(' . $timestamp . '000' . $sign . $GMTHourOffset . '00)/';
    }

    public function stringToDate(string $dateString): DateTimeInterface
    {
        preg_match('/([\-]?\d+)(\d{3})([\+\-]\d{4})/', $dateString, $matches);

        $timestamp = $matches[1];

        return Carbon::createFromTimestamp($timestamp);
    }
}
