<?php


namespace App\Service\MobileApi;


use App\Entity\OnlineApplication\OnlineApplication;
use App\Enum\ApiEndpoints;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class KycService
{
    use CreatesAuthenticationHash;

    const CHANNEL = 'F93C4B69-EA31-461F-B533-8411459AD1CD';
    const CLIENT_VERSION = '1.0';
    const PLATFORM_TYPE = 1;

    private $httpClient;

    private $mobileApiUrl;

    private $manager;

    private $logger;

    public function __construct(EntityManagerInterface $manager, string $mobileApiUrl, string $mobileApiUsername, string $mobileApiPassword, LoggerInterface $creditApplicationsLog)
    {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
        $this->manager = $manager;
        $this->logger = $creditApplicationsLog;
    }

    /**
     * @param OnlineApplication $application
     * @return bool
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function generateSessionId(OnlineApplication $application): bool
    {
        $timestamp = time();

        $data = [
            'ClientVersion' => self::CLIENT_VERSION,
            'Timestamp' => $timestamp,
            'Hash' => $this->generateBaseDataHash($timestamp),
            'PlatformType' => self::PLATFORM_TYPE,
            'RequestResult' => 0,
        ];

        $data = [
            'creditID' => $application->getErpId(),
            'data' => $data
        ];

//         dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::GENERATE_KYC_SESSION_ID), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $this->logger->info(
            sprintf(
                'Called GenerateKycSessionID for Credit with GUID: %s. Response: %s',
                $application->getErpId(),
                $response->getContent(false)
            )
        );

        $responseData = $response->toArray();

        if ($responseData['RequestResult'] === 0 &&
            empty($responseData['Result']) === false
        ) {
            $application->setKycSessionId($responseData['Result']);
            $this->manager->flush();

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $kycSessionId
     * @param string $externalToken
     * @param string $phoneNumber
     * @return bool
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function saveKycData(string $kycSessionId, string $externalToken, string $phoneNumber): bool
    {
        $timestamp = time();
        $data = [
            'ExternalToken' => $externalToken,
            'KycSessionID' => $kycSessionId,
            'PhoneNumber' => $phoneNumber,
            'ClientVersion' => self::CLIENT_VERSION,
            'Timestamp' => $timestamp,
            'Hash' => $this->generateBaseDataHash($timestamp),
            'PlatformType' => self::PLATFORM_TYPE,
            'RequestResult' => 0,
        ];
        $data = ['data' => $data];
//         dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request('POST', $this->getEndpoint(ApiEndpoints::SAVE_KYC_DATA), [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $this->logger->info(
            sprintf(
                'Called SaveKYCData for Credit with VerificationId: %s. Response: %s',
                $kycSessionId,
                $response->getContent(false)
            )
        );

        $responseData = $response->toArray();

        if ($responseData['RequestResult'] === 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $action
     * @return string|null
     */
    private function getEndpoint(string $action)
    {
        return $this->mobileApiUrl . $action;
    }
}
