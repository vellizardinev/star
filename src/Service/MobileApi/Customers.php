<?php


namespace App\Service\MobileApi;


use App\Entity\Customer;
use App\Enum\ApiEndpoints;
use App\Service\MobileApi\Response\ActivateAccountResponse;
use App\Service\MobileApi\Response\ChangePasswordResponse;
use App\Service\MobileApi\Response\CreateAccountResponse;
use App\Service\MobileApi\Response\ForgotPasswordResponse;
use App\Service\MobileApi\Response\LoginStepOneResponse;
use App\Service\MobileApi\Response\LoginStepTwoResponse;
use App\Service\MobileApi\Response\LogoutResponse;
use App\Service\MobileApi\Response\ResendAccountConfirmationEmailResponse;
use App\Service\MobileApi\Response\ResendVerificationCodeResponse;
use App\Service\MobileApi\Response\UpdateAccountResponse;
use App\Service\MobileApi\Response\UserPaymentInstrumentsResponse;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Customers
{
    use CreatesAuthenticationHash;

    const CLIENT_VERSION = '1.0';
    const PLATFORM_TYPE = 1;

    private $httpClient;

    private $mobileApiUrl;

    private $customersLog;

    public function __construct(string $mobileApiUrl, string $mobileApiUsername, string $mobileApiPassword, LoggerInterface $customersLog)
    {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
        $this->customersLog = $customersLog;
    }

    /**
     * @param string $credentialUsedForLogin
     * @param string $password
     * @return LoginStepOneResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function loginStepOne(string $credentialUsedForLogin, string $password): LoginStepOneResponse
    {
        $hashableData = [
            "EGN" => $this->looksLikePIN($credentialUsedForLogin) ? $credentialUsedForLogin : null,
            "Password" => $password,
            "Email" => $this->looksLikeEmail($credentialUsedForLogin) ? $credentialUsedForLogin : null,
            "Timestamp" => time(),
        ];

        $hash = $this->generateHash($hashableData);

        $data = [
            "ClientVersion" => self::CLIENT_VERSION,
            "PlatformType" => self::PLATFORM_TYPE,
            "RequestResult" => 0,
            "EGN" => $this->looksLikePIN($credentialUsedForLogin) ? $credentialUsedForLogin : null,
            "Email" => $this->looksLikeEmail($credentialUsedForLogin) ? $credentialUsedForLogin : null,
            "FirstName" => "",
            "GCMAuthorizationToken" => "",
            "HasUserAccount" => false,
            "IsEmailActive" => false,
            "IsPhoneActive" => false,
            "LastName" => "",
            "MiddleName" => "",
            "MobilePhoneNumber" => $this->looksLikePhone($credentialUsedForLogin) ? $credentialUsedForLogin : null,
            "Hash" => $hash,
        ];

        $data = array_merge($hashableData, $data);
        $data = ['login' => $data];

//        dd(json_encode($data, JSON_PRETTY_PRINT), $credentialUsedForLogin);

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::LOGIN),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

// dd($response->toArray());

        $this->customersLog->info(
            sprintf(
                'Called Login for email/PIN/Phone: %s. Response: %s',
                $credentialUsedForLogin,
                $response->getContent()
            )
        );

        return new LoginStepOneResponse($response->toArray());
    }

    /**
     * @param string $credentialUsedForLogin
     * @param string $oneTimePassword
     * @return LoginStepTwoResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function loginStepTwo(string $credentialUsedForLogin, string $oneTimePassword): LoginStepTwoResponse
    {
        $timestamp = time();

        $hashableData = [
            "EGN" => $this->looksLikePIN($credentialUsedForLogin) ? $credentialUsedForLogin : null,
            "Password" => null,
            "Email" => $this->looksLikeEmail($credentialUsedForLogin) ? $credentialUsedForLogin : null,
            "Timestamp" => $timestamp,
        ];

        $hash = $this->generateHash($hashableData);

        $data = [];

        if ($this->looksLikePIN($credentialUsedForLogin)) {
            $data['EGN'] = $credentialUsedForLogin;
        }

        if ($this->looksLikePhone($credentialUsedForLogin)) {
            $data['MobilePhoneNumber'] = $credentialUsedForLogin;
        }

        if ($this->looksLikeEmail($credentialUsedForLogin)) {
            $data['Email'] = $credentialUsedForLogin;
        }

        $data = array_merge($data, [
            "ClientVersion" => self::CLIENT_VERSION,
            "Hash" => $hash,
            "PlatformType" => self::PLATFORM_TYPE,
            "Timestamp" => $timestamp,
        ]);

        $data = ['login' => $data, 'otp' => $oneTimePassword];

//        dd($data, $credentialUsedForLogin);

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::LOGIN_WITH_ONE_TIME_PASSWORD),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

//        dd(json_encode($hashableData, JSON_PRETTY_PRINT), json_encode($data, JSON_PRETTY_PRINT), $response->toArray());

        $this->customersLog->info(
            sprintf(
                'Called LoginWithOneTimePassword for email/PIN/Phone: %s. Response: %s',
                $credentialUsedForLogin,
                $response->getContent()
            )
        );

        return new LoginStepTwoResponse($response->toArray());
    }

    /**
     * @param Customer $customer
     * @return CreateAccountResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function createAccount(Customer $customer): CreateAccountResponse
    {
        $hashableData = [
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "Password" => $customer->getPassword(),
            "Email" => $customer->getEmail(),
            "Timestamp" => time(),
        ];

        $hash = $this->generateHash($hashableData);

        $requestData = [
            "Hash" => $hash,
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "RequestResult" => 0,
            "FirstName" => $customer->getFirstName(),
            "GCMAuthorizationToken" => "",
            "HasUserAccount" => false,
            "IsEmailActive" => false,
            "IsPhoneActive" => false,
            "LastName" => $customer->getLastName(),
            "MiddleName" => $customer->getMiddleName(),
            "MobilePhoneNumber" => $customer->telephoneWithLeadingZero(),
            "ClientVersion" => self::CLIENT_VERSION,
            "PlatformType" => self::PLATFORM_TYPE,
        ];

        $requestData = [
            'newUser' => array_merge($requestData, $hashableData),
        ];

//        dd(json_encode($requestData, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::CREATE_USER),
            [
                'verify_peer' => false,
                'json' => $requestData,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called CreateUserAccount for Customer: %s. Response: %s',
                $customer,
                $response->getContent()
            )
        );

        return new CreateAccountResponse($response->toArray());
    }

    /**
     * @param Customer $customer
     * @param string $code
     * @return ActivateAccountResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function activateAccount(Customer $customer, string $code): ActivateAccountResponse
    {
        $hashableData = array(
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "Email" => $customer->getEmail(),
            "VerificationCode" => $code,
            "Timestamp" => time(),
        );

        $hash = $this->generateHash($hashableData);

        $data = [
            "Hash" => $hash,
            "RequestResult" => 0,
            "ClientVersion" => self::CLIENT_VERSION,
            "PlatformType" => self::PLATFORM_TYPE,
        ];

        $data = array_merge($data, $hashableData);
        $data = ['activation' => $data];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::ACTIVATE_USER_ACCOUNT),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called ActivateUserAccount for Customer %. Response: %s',
                $customer,
                $response->getContent()
            )
        );

        return new ActivateAccountResponse($response->toArray());
    }

    /**
     * @param Customer $customer
     * @return LogoutResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function logout(Customer $customer): LogoutResponse
    {
        $hashableData = array(
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "Password" => null,
            "Email" => $customer->getEmail(),
            "Timestamp" => time(),
        );


        $hash = $this->generateHash($hashableData);

        $data = [
            "Hash" => $hash,
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "FirstName" => $customer->getFirstName(),
            "GCMAuthorizationToken" => $customer->getToken(),
            "LastName" => $customer->getLastName(),
            "MiddleName" => $customer->getMiddleName(),
            "MobilePhoneNumber" => $customer->getTelephone(),
            "HasUserAccount" => $customer->getHasUserAccount(),
            "IsEmailActive" => $customer->getIsEmailActive(),
            "IsPhoneActive" => $customer->getIsPhoneActive(),
            "ClientVersion" => self::CLIENT_VERSION,
            "PlatformType" => self::PLATFORM_TYPE,
        ];

        $data = array_merge($hashableData, $data);
        $data = ['loggedUser' => $data];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::LOGOUT),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called Logout for Customer %s. Response: %s',
                $customer,
                $response->getContent()
            )
        );

        return new LogoutResponse($response->toArray());
    }

    /**
     * @param Customer $customer
     * @return UpdateAccountResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function updateAccount(Customer $customer): UpdateAccountResponse
    {
        $hashedData = [
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "Password" => $customer->getPassword(),
            "Email" => $customer->getEmail(),
            "Timestamp" => time(),
        ];

        $hash = $this->generateHash($hashedData);

        $data = [
            "Hash" => $hash,
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "RequestResult" => 0,
            "FirstName" => $customer->getFirstName(),
            "GCMAuthorizationToken" => "",
            "HasUserAccount" => $customer->getHasUserAccount(),
            "IsEmailActive" => $customer->getIsEmailActive(),
            "IsPhoneActive" => $customer->getIsPhoneActive(),
            "LastName" => $customer->getLastName(),
            "MiddleName" => $customer->getMiddleName(),
            "MobilePhoneNumber" => $customer->telephoneWithLeadingZero(),
            "ClientVersion" => self::CLIENT_VERSION,
            "PlatformType" => self::PLATFORM_TYPE,
        ];

        $data = array_merge($data, $hashedData);
        $data = ['updateUser' => $data];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::UPDATE_USER),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called UpdateUser for Customer %s. Response: %s',
                $customer,
                $response->getContent()
            )
        );

        return new UpdateAccountResponse($response->toArray());
    }

    /**
     * @param Customer $customer
     * @param string $oldPassword
     * @param string $newPassword
     * @return ChangePasswordResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function changePassword(Customer $customer, string $oldPassword, string $newPassword): ChangePasswordResponse
    {
        $customerData = $this->customerData($customer);

        $customerData['Password'] = $oldPassword;

        $data['NewPassword'] = $newPassword;
        $data['Timestamp'] = time();
        $data['Hash'] = $this->generateHash($data);
        $data['PlatformType'] = self::PLATFORM_TYPE;
        $data['ClientVersion'] = self::CLIENT_VERSION;

        $customerData = ['User' => $customerData];
        $data = array_merge($customerData, $data);
        $data = ['passwordChange' => $data];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::CHANGE_ACCOUNT_PASSWORD),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called ChangeAccountPassword for Customer %s. Response: %s',
                $customer,
                $response->getContent()
            )
        );

        return new ChangePasswordResponse($response->toArray());
    }

    /**
     * @param Customer $customer
     * @return ResendAccountConfirmationEmailResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function resendConfirmationEmail(Customer $customer): ResendAccountConfirmationEmailResponse
    {
        $hashableData = [
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "Password" => $customer->getPassword(),
            "Email" => $customer->getEmail(),
            "Timestamp" => time(),
        ];

        $hash = $this->generateHash($hashableData);

        $data = [
            "Hash" => $hash,
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "FirstName" => $customer->getFirstName(),
            "GCMAuthorizationToken" => "",
            "HasUserAccount" => false,
            "IsEmailActive" => false,
            "IsPhoneActive" => false,
            "LastName" => $customer->getLastName(),
            "MiddleName" => $customer->getMiddleName(),
            "MobilePhoneNumber" => $customer->getTelephone(),
            "ClientVersion" => self::CLIENT_VERSION,
            "PlatformType" => self::PLATFORM_TYPE,
        ];

        $data = array_merge($data, $hashableData);
        $data = ['account' => $data];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::RESEND_ACCOUNT_CONFIRMATION_EMAIL),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called ResendAccountConfirmationEmail for Customer %s. Response: %s',
                $customer,
                $response->getContent()
            )
        );

        return new ResendAccountConfirmationEmailResponse($response->toArray());
    }

    /**
     * @param string $recoveryEmail
     * @return ForgotPasswordResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function forgotPassword(string $recoveryEmail): ForgotPasswordResponse
    {
        $hashableData = [
            "RecoveryEmail" => $recoveryEmail,
            "Timestamp" => time(),
        ];

        $hashableData['Hash'] = $this->generateHash($hashableData);

        $baseData = [
            'PlatformType' => self::PLATFORM_TYPE,
            'ClientVersion' => self::CLIENT_VERSION,
        ];

        $data = array_merge($hashableData, $baseData);
        $data = ['forgottenPassword' => $data];

//        dd(json_encode($requestData, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::FORGOTTEN_PASSWORD),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called ForgottenPassword for recovery email: %s. Response: %s',
                $recoveryEmail,
                $response->getContent()
            )
        );

        return new ForgotPasswordResponse($response->toArray());
    }

    /**
     * @param Customer $customer
     * @return ResendVerificationCodeResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function resendVerificationCode(Customer $customer): ResendVerificationCodeResponse
    {
        $hashableData = [
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "Password" => $customer->getPassword(),
            "Email" => $customer->getEmail(),
            "Timestamp" => time(),
        ];

        $hash = $this->generateHash($hashableData);

        $data = [
            "Hash" => $hash,
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "RequestResult" => 0,
            "FirstName" => $customer->getFirstName(),
            "GCMAuthorizationToken" => "",
            "HasUserAccount" => false,
            "IsEmailActive" => false,
            "IsPhoneActive" => false,
            "LastName" => $customer->getLastName(),
            "MiddleName" => $customer->getMiddleName(),
            "MobilePhoneNumber" => $customer->getTelephone(),
            "ClientVersion" => self::CLIENT_VERSION,
            "PlatformType" => self::PLATFORM_TYPE,
        ];

        $data = array_merge($data, $hashableData);
        $data = ['user' => $data];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::RESEND_VERIFICATION_SMS),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called GeneratePesonalCode for Customer %s. Response: %s',
                $customer,
                $response->getContent()
            )
        );

        return new ResendVerificationCodeResponse($response->toArray());
    }

    /**
     * @param string $verificationId
     * @param string $newPassword
     * @return ChangePasswordResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function changeForgottenPassword(string $verificationId, string $newPassword): ChangePasswordResponse
    {
        $hashableData = [
            "VerificationID" => $verificationId,
            "NewPassword" => $newPassword,
            "Timestamp" => time(),
        ];

        $hash = $this->generateHash($hashableData);

        $parameters = [
            "Hash" => $hash,
            "ConfirmNewPassword" => $newPassword,
            "ClientVersion" => self::CLIENT_VERSION,
            "PlatformType" => self::PLATFORM_TYPE
        ];

        $data = array_merge($hashableData, $parameters);
        $data = ['pass' => $data];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::CHANGE_FORGOTTEN_PASSWORD),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called ChangeForgottenPassword for verificationId %s. Response: %s',
                $verificationId,
                $response->getContent()
            )
        );

        return new ChangePasswordResponse($response->toArray());
    }

    /**
     * @param Customer $customer
     * @return UserPaymentInstrumentsResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getPaymentInstruments(Customer $customer): UserPaymentInstrumentsResponse
    {
        $hashableData = array(
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "Password" => null,
            "Email" => $customer->getEmail(),
            "Timestamp" => time(),
        );

        $hash = $this->generateHash($hashableData);

        $data = [
            "Hash" => $hash,
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "FirstName" => $customer->getFirstName(),
            "GCMAuthorizationToken" => $customer->getToken(),
            "LastName" => $customer->getLastName(),
            "MiddleName" => 'not available',
            "MobilePhoneNumber" => $customer->getTelephone(),
            "HasUserAccount" => $customer->getHasUserAccount(),
            "IsEmailActive" => $customer->getIsEmailActive(),
            "IsPhoneActive" => $customer->getIsPhoneActive(),
            "ClientVersion" => self::CLIENT_VERSION,
            "PlatformType" => self::PLATFORM_TYPE,
        ];

        $data = array_merge($hashableData, $data);
        $data = ['loggedUser' => $data];

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::GET_USER_PAYMENT_INSTRUMENTS),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called GetUserPaymentInstruments for Customer %s. Response: %s',
                $customer,
                $response->getContent(false)
            )
        );

        return new UserPaymentInstrumentsResponse($response->toArray());
    }

    public function IsEmailConfirmed(Customer $customer): bool
    {
        $data = $this->customerData($customer);

        $data = ['account' => $data];

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::CHECK_IS_USER_ACCOUNT_EMAIL_CONFIRMED),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called CheckIsUserAccountEmailConfirmed for Customer %s. Response: %s',
                $customer,
                $response->getContent(false)
            )
        );

        $responseArray = $response->toArray();

        return $responseArray['RequestResult'] === 0 && $responseArray['Result'] === true;
    }

    /**
     * @param string $action
     * @return string|null
     */
    private function getEndpoint(string $action)
    {
        return $this->mobileApiUrl . $action;
    }

    private function customerData(Customer $customer): array
    {
        $hashableData = [
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "Password" => null,
            "Email" => $customer->getEmail(),
            "Timestamp" => time(),
        ];

        $hash = $this->generateHash($hashableData);
        $hashableData["Hash"] = $hash;

        $data = [
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "FirstName" => $customer->getFirstName(),
            "GCMAuthorizationToken" => $customer->getToken(),
            "LastName" => $customer->getLastName(),
            "MiddleName" => $customer->getMiddleName(),
            "MobilePhoneNumber" => $customer->getTelephone(),
            "HasUserAccount" => $customer->getHasUserAccount(),
            "IsEmailActive" => $customer->getIsEmailActive(),
            "IsPhoneActive" => $customer->getIsPhoneActive(),
            'PlatformType' => self::PLATFORM_TYPE,
            'ClientVersion' => self::CLIENT_VERSION,
        ];

        $data = array_merge($hashableData, $data);

        return $data;
    }

    private function looksLikeEmail(string $value): bool
    {
        return strpos($value, '@') !== false;
    }

    private function looksLikePhone(string $value): bool
    {
        return preg_match('/\d{9}/', $value) === 1;
    }

    private function looksLikePIN(string $value): bool
    {
        return preg_match('/[1-9]{1}\d{9}/', $value) === 1;
    }
}