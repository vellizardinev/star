<?php


namespace App\Service\MobileApi;


use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BasicAuthHttpClientFactory
{
    public static function create(string $username, string $password): HttpClientInterface
    {
        return HttpClient::create(
            [
                'auth_basic' => [$username, $password],
                'extra' => [
                    'curl' => [
                        CURLOPT_SSLVERSION => CURL_SSLVERSION_MAX_TLSv1_2,
                    ]
                ]
            ]
        );
    }
}
