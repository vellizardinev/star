<?php


namespace App\Service\MobileApi;


use App\Entity\Customer;
use App\Enum\ApiEndpoints;
use App\Service\MobileApi\Response\GetCreditsResponse;
use App\Service\MobileApi\Response\GetRefinanceSumResponse;
use App\Service\MobileApi\Response\GetRepaymentSumResponse;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CustomerCredits
{
    use CreatesAuthenticationHash;

    const CLIENT_VERSION = '1.0';
    const PLATFORM_TYPE = 1;

    private $httpClient;

    private $mobileApiUrl;

    private $customersLog;

    public function __construct(string $mobileApiUrl, string $mobileApiUsername, string $mobileApiPassword, LoggerInterface $customersLog)
    {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
        $this->customersLog = $customersLog;
    }

    /**
     * @param Customer $customer
     * @return GetCreditsResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getCredits(Customer $customer): GetCreditsResponse
    {

        $data = $this->customerData($customer);
        $data = ['loggedUser' => $data];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::GET_CUSTOMER_CREDITS),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called GetClientCredits for email/PIN: %s. Response: %s',
                $customer->getEmail(),
                $response->getContent()
            )
        );
//dd($response->toArray());
        return new GetCreditsResponse($response->toArray());
    }

    /**
     * @param string $contractCode
     * @return GetRefinanceSumResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function calculateRefinance(string $contractCode): GetRefinanceSumResponse
    {
        $data = [
            "codeContract" => $contractCode
        ];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::GET_REFINANCE_SUM),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called GetRefinanceSum for Contract#: %s. Response: %s',
                $contractCode,
                $response->getContent()
            )
        );

        return new GetRefinanceSumResponse($response->toArray());
    }

    /**
     * @param string $contractCode
     * @return GetRepaymentSumResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function calculateRepayment(string $contractCode): GetRepaymentSumResponse
    {
        $data = [
            "codeContract" => $contractCode
        ];

//        dd(json_encode($data, JSON_PRETTY_PRINT));

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::GET_REPAYMENT_SUM),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called GetRepaymentSum for Contract#: %s. Response: %s',
                $contractCode,
                $response->getContent()
            )
        );

        return new GetRepaymentSumResponse($response->toArray());
    }

    /**
     * @param string $action
     * @return string|null
     */
    private function getEndpoint(string $action)
    {
        return $this->mobileApiUrl . $action;
    }

    private function customerData(Customer $customer): array
    {
        $hashableData = [
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "Password" => null,
            "Email" => $customer->getEmail(),
            "Timestamp" => time(),
        ];

        $hash = $this->generateHash($hashableData);
        $hashableData["Hash"] = $hash;

        $data = [
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "FirstName" => $customer->getFirstName(),
            "GCMAuthorizationToken" => $customer->getToken(),
            "LastName" => $customer->getLastName(),
            "MiddleName" => 'not available',
            "MobilePhoneNumber" => $customer->getTelephone(),
            "HasUserAccount" => $customer->getHasUserAccount(),
            "IsEmailActive" => $customer->getIsEmailActive(),
            "IsPhoneActive" => $customer->getIsPhoneActive(),
            'PlatformType' => self::PLATFORM_TYPE,
            'ClientVersion' => self::CLIENT_VERSION,
        ];

        $data = array_merge($hashableData, $data);

        return $data;
    }
}