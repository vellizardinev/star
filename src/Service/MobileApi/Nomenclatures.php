<?php


namespace App\Service\MobileApi;


use App\Entity\Nomenclature;
use App\Enum\NomenclatureTypes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Nomenclatures
{
    use CreatesAuthenticationHash;

    const GET_NOMENCLATURES = 'GetNomenclatures';

    const FAMILY_STATUS_PARENT_ID = '4f4f34fc-1d49-4300-a409-2d7a72efb539';
    const INFORMATION_SOURCES_PARENT_ID = '07db48cd-2808-408b-83cc-a95eac474373';
    const EDUCATION_TYPES_PARENT_ID = '87446639-ac77-487d-92f2-4d5ef180dccd';
    const HOME_TYPES_PARENT_ID = '90d725ba-4476-435b-a3e9-747de95da5e1';
    const CREDIT_PURPOSES_PARENT_ID = '0c9a31a1-6c21-4bae-a6e3-e92eef78ee8c';
    const EMPLOYMENT_TYPES_PARENT_ID = 'b7559c48-f998-452c-acdb-378d0162f8b2';
    const POSITION_TYPES_PARENT_ID = '4ee5f496-935e-4614-8963-31e953f62943';
    const ACTIVITY_AREAS_PARENT_ID = 'bc232b99-c1f3-4634-9981-fda90b1e2262';
    const PROPERTY_TYPES_PARENT_ID = '18695066-a2fd-4312-9db3-0288ba99f77b';
    const METHODS_TO_RECEIVE_CREDIT_PARENT_ID = 'fc1e5194-fe4c-4052-88f6-a8c960af891b';
    const PARTNER_TYPES_PARENT_ID = '81db708d-f044-4939-9ec9-8a9034d05b5c';
    const FAMILY_SIZES_PARENT_ID = 'd699532c-972f-4057-91eb-7d68ff9c67ca';
    const INCOME_SOURCES_PARENT_ID = '584dd0c9-be89-4bbc-a2a8-570148de6ffc';
    const EXPENSE_TYPES_PARENT_ID = 'a9385056-eb9e-4b81-bcf5-0b0dcb045e5a';
    const AGREEMENT_TYPES_PARENT_ID = '076f6efb-3a94-426f-a071-5ccdb1c1a8c0';
    const IMAGE_STATUSES_PARENT_ID = '42b9a503-e3a9-49aa-8765-79658dcad81c';

    private $httpClient;

    private $mobileApiUrl;

    private $manager;

    public function __construct(string $mobileApiUrl, string $mobileApiUsername, string $mobileApiPassword, EntityManagerInterface $manager)
    {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
        $this->manager = $manager;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function update()
    {
        $this->updateFamilyStatuses();
        $this->updateInformationSources();
        $this->updateEducationTypes();
        $this->updateHomeTypes();
        $this->updateCreditPurposes();
        $this->updateEmploymentTypes();
        $this->updatePositionTypes();
        $this->updateActivityAreas();
        $this->updatePropertyTypes();
        $this->updateMethodsToReceiveCredit();
        $this->updateMethodsToReceiveSalary();
        $this->updatePartnerTypes();
        $this->updateFamilySizes();
        $this->updateIncomeSources();
        $this->updateExpenseTypes();
        $this->updateAgreementTypes();
        $this->updateImageStatuses();
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateFamilyStatuses()
    {
        $this->updateNomenclature(NomenclatureTypes::FAMILY_STATUS, self::FAMILY_STATUS_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateInformationSources()
    {
        $this->updateNomenclature(NomenclatureTypes::INFORMATION_SOURCES, self::INFORMATION_SOURCES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateEducationTypes()
    {
        $this->updateNomenclature(NomenclatureTypes::EDUCATION_TYPES, self::EDUCATION_TYPES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateHomeTypes()
    {
        $this->updateNomenclature(NomenclatureTypes::HOME_TYPES, self::HOME_TYPES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateCreditPurposes()
    {
        $this->updateNomenclature(NomenclatureTypes::CREDIT_PURPOSES, self::CREDIT_PURPOSES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateEmploymentTypes()
    {
        $this->updateNomenclature(NomenclatureTypes::EMPLOYMENT_TYPES, self::EMPLOYMENT_TYPES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updatePositionTypes()
    {
        $this->updateNomenclature(NomenclatureTypes::POSITION_TYPES, self::POSITION_TYPES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateActivityAreas()
    {
        $this->updateNomenclature(NomenclatureTypes::ACTIVITY_AREAS, self::ACTIVITY_AREAS_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updatePropertyTypes()
    {
        $this->updateNomenclature(NomenclatureTypes::PROPERTY_TYPES, self::PROPERTY_TYPES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateMethodsToReceiveCredit()
    {
        $this->updateNomenclature(NomenclatureTypes::METHODS_TO_RECEIVE_CREDIT, self::METHODS_TO_RECEIVE_CREDIT_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateMethodsToReceiveSalary()
    {
        $this->updateNomenclature(NomenclatureTypes::METHODS_RECEIVING_SALARY, self::METHODS_TO_RECEIVE_CREDIT_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updatePartnerTypes()
    {
        $this->updateNomenclature(NomenclatureTypes::PARTNER_TYPE, self::PARTNER_TYPES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateFamilySizes()
    {
        $this->updateNomenclature(NomenclatureTypes::FAMILY_SIZES, self::FAMILY_SIZES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateIncomeSources()
    {
        $this->updateNomenclature(NomenclatureTypes::INCOME_SOURCES, self::INCOME_SOURCES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateExpenseTypes()
    {
        $this->updateNomenclature(NomenclatureTypes::EXPENSE_TYPES, self::EXPENSE_TYPES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateAgreementTypes()
    {
        $this->updateNomenclature(NomenclatureTypes::AGREEMENT_TYPES, self::AGREEMENT_TYPES_PARENT_ID);
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateImageStatuses()
    {
        $this->updateNomenclature(NomenclatureTypes::IMAGE_STATUSES, self::IMAGE_STATUSES_PARENT_ID);
    }

    private function getNomenclaturesUrl(): string
    {
        return $this->mobileApiUrl . self::GET_NOMENCLATURES;
    }

    /**
     * @param string $nomenclatureType
     * @param string $parentId
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateNomenclature(string $nomenclatureType, string $parentId)
    {
        $timestamp = time();

        $data = [
            'data' => [
                'ParentID' => $parentId,
                'ClientVersion' => '1.0',
                'Hash' => $this->generateHash([$parentId, $timestamp]),
                'PlatformType' => '1',
                'Timestamp' => $timestamp,
            ],
        ];

        $response = $this->httpClient->request(
            'POST',
            $this->getNomenclaturesUrl(),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $responseData = json_decode($response->getContent(), true);
        $nomenclatures = $responseData['Nomenclature'];

        // Delete nomenclatures in our DB that no longer exist
        $this->delete($nomenclatures, $nomenclatureType);

        foreach ($nomenclatures as $data) {
            if ($this->manager->getRepository(Nomenclature::class)->exists($data['key'], $nomenclatureType)) {
                continue;
            }

            $entry = new Nomenclature($data['key'], $data['value'], $nomenclatureType);
            $this->manager->persist($entry);

            $entry->setVisible(NomenclaturesConfiguration::visible(sprintf('%s-%s', $nomenclatureType, $entry->getGuid())));

            if ($text = NomenclaturesConfiguration::text(sprintf('%s-%s', $nomenclatureType, $entry->getGuid()))) {
                $entry->setText($text);
            }
        }

        $this->manager->flush();
    }

    private function delete(array $items, string $nomenclatureType)
    {
        $guids = array_map(
            function ($item) {
                return $item['key'];
            }, $items);

        $q = $this->manager->createQuery('delete from App\\Entity\\Nomenclature nomenclature where nomenclature.type = :type and nomenclature.guid in (:guids)');
        $q->setParameter('guids', $guids);
        $q->setParameter('type', $nomenclatureType);
        $q->execute();
    }
}