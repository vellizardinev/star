<?php


namespace App\Service\MobileApi;


use App\Entity\Customer;
use App\Enum\ApiEndpoints;
use App\Service\MobileApi\Response\CreditProposalResponse;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class GetCreditProposal
{
    use CreatesAuthenticationHash;
    use ConvertsDatesToString;

    const CHANNEL = 'F93C4B69-EA31-461F-B533-8411459AD1CD';
    const CLIENT_VERSION = '1.0';
    const PLATFORM_TYPE = 1;

    private $httpClient;

    private $mobileApiUrl;

    private $customersLog;

    private $manager;

    public function __construct(string $mobileApiUrl, string $mobileApiUsername, string $mobileApiPassword, LoggerInterface $customersLog, EntityManagerInterface $manager)
    {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
        $this->customersLog = $customersLog;
        $this->manager = $manager;
    }

    /**
     * @param Customer $customer
     * @return array
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function getCreditProposal(Customer $customer): CreditProposalResponse
    {
        $data = $this->customerData($customer);
        $data = [
            'data' => $data,
            'channel' => self::CHANNEL,
        ];

        $response = $this->httpClient->request(
            'POST',
            $this->getEndpoint(ApiEndpoints::GET_CREDIT_PROPOSAL),
            [
                'verify_peer' => false,
                'json' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );

        $this->customersLog->info(
            sprintf(
                'Called GetCreditProposal for email/PIN: %s. Response: %s',
                $customer->getEmail(),
                $response->getContent()
            )
        );

        return new CreditProposalResponse($response->toArray());
    }

    /**
     * @param string $action
     * @return string
     */
    private function getEndpoint(string $action)
    {
        return $this->mobileApiUrl . $action;
    }

    private function customerData(Customer $customer): array
    {
        $hashableData = [
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "Password" => null,
            "Email" => $customer->getEmail(),
            "Timestamp" => time(),
        ];

        $hash = $this->generateHash($hashableData);
        $hashableData["Hash"] = $hash;

        $data = [
            "EGN" => $customer->getPersonalIdentificationNumber(),
            "FirstName" => $customer->getFirstName(),
            "GCMAuthorizationToken" => $customer->getToken(),
            "LastName" => $customer->getLastName(),
            "MiddleName" => 'not available',
            "MobilePhoneNumber" => $customer->getTelephone(),
            "HasUserAccount" => $customer->getHasUserAccount(),
            "IsEmailActive" => $customer->getIsEmailActive(),
            "IsPhoneActive" => $customer->getIsPhoneActive(),
            'PlatformType' => self::PLATFORM_TYPE,
            'ClientVersion' => self::CLIENT_VERSION,
        ];

        $data = array_merge($hashableData, $data);

        return $data;
    }
}
