<?php


namespace App\Service\MobileApi;


use App\Entity\FailedOnlineApplicationRequest;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Enum\MethodsReceivingSalary;
use App\Enum\MethodsToReceiveCredit;
use App\Enum\PropertyType;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SaveProposalFullData
{
    use CreatesAuthenticationHash;

    const SAVE_PPZ_FULL = 'SaveProposalFullData';

    const CHANNEL = 'F93C4B69-EA31-461F-B533-8411459AD1CD';

    private $httpClient;

    private $mobileApiUrl;

    private $creditApplicationsLog;

    private $manager;

    public function __construct(string $mobileApiUrl, string $mobileApiUsername, string $mobileApiPassword, LoggerInterface $creditApplicationsLog, EntityManagerInterface $manager)
    {
        $this->httpClient = BasicAuthHttpClientFactory::create($mobileApiUsername, $mobileApiPassword);
        $this->mobileApiUrl = $mobileApiUrl;
        $this->creditApplicationsLog = $creditApplicationsLog;
        $this->manager = $manager;
    }

    /**
     * @param OnlineApplication $application
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function saveProposal(OnlineApplication $application)
    {
        $data = $this->mapData($application);

//        $json_string = json_encode($data, JSON_PRETTY_PRINT);print_r($json_string);die;

        $requestOptions = [
            'verify_peer' => false,
            'json' => $data,
            'headers' => ['Content-Type' => 'application/json']
        ];

        $response = $this->httpClient->request('POST', $this->getUrl(), $requestOptions);

        $responseData = json_decode($response->getContent(), true);

        $this->creditApplicationsLog->info(sprintf('Sent online application to API. Response=%s', $response->getContent()));

        $application->setApiResponse($response->getContent());
        $application->setJsonData(json_encode($data, JSON_PRETTY_PRINT));

        if ($responseData['RequestResult'] === 0 && !empty($responseData['Result'])) {
            $application->setErpId($responseData['Result']);
            $application->setIsSentToErp(true);
        } else {
            // Make sure the application is retried three more times
            $httpRequest = (new FailedOnlineApplicationRequest)
                ->setApplication($application)
                ->setUrl($this->getUrl())
                ->setMethod('POST')
                ->setOptions($requestOptions)
                ->setIntervalInSeconds(5 * 60)
                ->setMultiplier(0)
                ->setMaxAttempts(3)
            ;

            $this->manager->persist($httpRequest);
        }

        $this->manager->flush();
    }

    private function getUrl(): string
    {
        return $this->mobileApiUrl . self::SAVE_PPZ_FULL;
    }

    private function mapData(OnlineApplication $application): array
    {
        $timestamp = time();

        $hash = $this->generateHash([
            $application->getPersonalInfo()->getFirstName(),
            $application->getPersonalInfo()->getMiddleName(),
            $application->getPersonalInfo()->getLastName(),
            $application->getPersonalInfo()->getPersonalIdentificationNumber(),
            $timestamp
        ]);

        return [
            'loggedUser' => [
                "Email" => $application->getPersonalInfo()->getEmail(),
            ], // matched
            'proposalDTO' => [
                'Hash' => $hash,
                'Timestamp' => $timestamp,
                "ClientVersion" => "1.0",
                'Channel' => self::CHANNEL, // matched
                'ProposalType' => 0,
                'KYCId' => $this->getKycId($application), // matched
                'IsClientDisabled' => false,
                'HowOftenDoesTheClientReceiveSalary' => -1,
                'ClientPassportAddress' => [
                    'PostCode' => $application->getAddress()->getPermanentAddressPostcode(), // matched
                    'District' => $application->getAddress()->getPermanentAddressNeighbourhood(), // matched
                    'Area' => $application->getAddress()->getPermanentAddressArea(), // new
                    'Municipality' => $application->getAddress()->getPermanentAddressMunicipality(), // new
                    'City' => $application->getAddress()->getPermanentAddressCity(), // matched
                    'BlockNumber' => $application->getAddress()->getPermanentAddressBlockNumber(), // matched
                    'Street' => $application->getAddress()->getPermanentAddressStreet(), // matched
                    'Entrance' => $application->getAddress()->getPermanentAddressEntrance(), // matched
                    'Floor' => $application->getAddress()->getPermanentAddressFloor(), // matched
                    'Apartment' => $application->getAddress()->getPermanentAddressApartment(), // matched
                ],
                'ClientCurrentAddress' => [
                    'City' => $application->getAddress()->getCurrentAddressCity(), // matched
                    'PostCode' => $application->getAddress()->getCurrentAddressPostcode(), // matched
	                'Area' => $application->getAddress()->getCurrentAddressArea(), // new
	                'Municipality' => $application->getAddress()->getCurrentAddressMunicipality(), // new
                    'District' => $application->getAddress()->getCurrentAddressNeighbourhood(), // matched
                    'BlockNumber' => $application->getAddress()->getCurrentAddressBlockNumber(), // matched
                    'Street' => $application->getAddress()->getCurrentAddressStreet(), // matched
                    'Entrance' => $application->getAddress()->getCurrentAddressEntrance(), // matched
                    'Floor' => $application->getAddress()->getCurrentAddressFloor(), // matched
                    'Apartment' => $application->getAddress()->getCurrentAddressApartment(), // matched
                ],
                'ClientIBANs' => $this->iban($application), // matched
                'WorkAddress' => [
                    'PostCode' => $application->getWorkDetails()->getWorkAddressPostcode(), // matched
                    'District' => $application->getWorkDetails()->getWorkAddressNeighbourhood(), // updated
	                'Area' => $application->getWorkDetails()->getWorkAddressArea(), // new
	                'Municipality' => $application->getWorkDetails()->getWorkAddressMunicipality(), // new
                    'City' => $application->getWorkDetails()->getWorkAddressCity(), // matched
                    'BlockNumber' => $application->getWorkDetails()->getWorkAddressBlockNumber(), // matched
                    'Street' => $application->getWorkDetails()->getWorkAddressStreet(), // matched
                    'Entrance' => $application->getWorkDetails()->getWorkAddressEntrance(), // updated
                    'Floor' => $application->getWorkDetails()->getWorkAddressFloor(), // updated
                    'Apartment' => $application->getWorkDetails()->getWorkAddressApartment(), // matched
                ],

                'MobilePhone' => $this->telephoneWithLeadingZero($application->getPersonalInfo()->getMobileTelephone()), // matched
                'Email' => $application->getPersonalInfo()->getEmail(), // matched
                'EGN' => $application->getPersonalInfo()->getPersonalIdentificationNumber(), // matched
                'CreditSum' => $application->getCreditDetails()->getAmount(), // matched
                'CreditProduct' => $application->getCreditDetails()->getCreditProductId(), // matched
                'CreditPeriod' => $application->getCreditDetails()->getCreditPeriodId(), // matched
                'CreditPeriodValue' => $application->getCreditDetails()->getPeriod(), // matched
                'CreditPayment' => $application->getCreditDetails()->getInstalmentAmount(), // matched
                'FirstName' => $application->getPersonalInfo()->getFirstName(), // matched
                'SecondName' => $application->getPersonalInfo()->getMiddleName(), // matched
                'LastName' => $application->getPersonalInfo()->getLastName(), // matched
                'IDCardNumber' => $application->getPersonalInfo()->getIdCardNumber(), // matched
                'IDCardIssueDate' => $this->dateToString($application->getPersonalInfo()->getIdCardIssuedAt()), // matched
                'IDCardIssuedBy' => $application->getPersonalInfo()->getIdCardIssuedBy(), // matched
                'PhoneForRemoteDeals' => $this->telephoneWithLeadingZero($application->getPersonalInfo()->getTelephoneForNewContracts()), // matched
                'Workplace' => $application->getWorkDetails()->getCompanyName(), // updated
                'WorkPhone' => $this->telephoneWithLeadingZero($application->getWorkDetails()->getCompanyTelephone()), // updated
                'WorkingPeriod' => $application->getWorkDetails()->getWorkExperienceCurrentEmployer(), // updated
                'WorkingPeriodTotal' => $application->getWorkDetails()->getWorkExperienceTotal(), // matched
                'ClientSallary' => $application->getAdditionalInfo()->getMonthlyIncome(), // matched
                'IncomeOther' => $application->getAdditionalInfo()->getOtherIncome(), // updated
                'SalaryReceive' => $this->mapMethodReceivingSalary($application->getWorkDetails()->getHowSalaryIsReceived()), // matched
                'WaterAndPowerExpense' => $application->getIncomesAndExpenses()->getHouseholdExpensesElectricityAndWater(), // matched
                'InternetAndTVExpense' => $application->getIncomesAndExpenses()->getHouseholdExpensesInternetAndTv(), // matched
                'PhoneGSMExpense' => $application->getIncomesAndExpenses()->getHouseholdExpensesTelephone(), // matched
                'HomeType' => $application->getAdditionalInfo()->getTypeOfHome(), // matched
                'IsClientInDisadvantage' => $application->getPersonalInfo()->getDisadvantaged(), // matched
                'WorkAreaOfExpertise' => $application->getAdditionalInfo()->getIndustry(), // matched
                'WorkContract' => $application->getWorkDetails()->getEmploymentType(), // matched
                'Education' => $application->getAdditionalInfo()->getEducation(), // matched
                'CreditPurpose' => $application->getAdditionalInfo()->getPurposeOfCredit(), // matched
//                '$wantsDiscretion' => $application->getWantsDiscretion(), // not in API
                'FamilyCount' => $application->getAdditionalInfo()->getHouseholdSize(), // matched
                'FamilyStatus' => $application->getAdditionalInfo()->getFamilyStatus(), // matched
                'AvailableReceiveMethods' => [$application->getCreditDetails()->getHowShouldMoneyBeReceived()], // matched
//                '$isPoliticalPerson' => $application->getIsPoliticalPerson(), // not in API
//                '$isRelatedToPoliticalPerson' => $application->getIsRelatedToPoliticalPerson(), // not in API
//                '$countryOfBirth' => $application->getCountryOfBirth(), // not in API
//                '$nationality' => $application->getNationality(), // not in API
//                '$countryOfResidence' => $application->getCountryOfBirth(), // not in API
                'WorkPosition' => $application->getWorkDetails()->getCompanyPosition(), // updated
                'GetSalaryOnDayOfMonth' => $application->getWorkDetails()->getDayForReceivingSalary(), // matched
                'ClientExpensesRent' => $application->getIncomesAndExpenses()->getHouseholdExpensesRent(), // matched
                'OutcomePmtTo1' => $application->getIncomesAndExpenses()->getHouseholdExpensesOtherCredits(), // updated
                'ExpensesLoan' => $application->getIncomesAndExpenses()->getHouseholdExpensesOtherCreditsInstalmentAmount(), // matched
                'OtherOutcome' => $application->getIncomesAndExpenses()->getHouseholdExpensesOther(), // updated
                'DateOfEndingTheFixedTermContract' => null, // work contract is not in application form
                'DateOfContractConclusion' => null, // work contract is not in application form
                'LearnedAboutTheCompanyFromGuid' => $application->getAdditionalInfo()->getFoundOutAboutTheCompanyFrom(), // matched

                'PayBy' => $application->getCreditDetails()->getHowShouldMoneyBeReceived(), // matched
                'PayByArgument' => $application->getCreditDetails()->getIban(), // matched
                'LivingOnThisAddressFromDate' => $this->dateToString($application->getAddress()->getLivingOnAddressFromDate()), // matched
                'Bonusses' => $application->getAdditionalInfo()->getBonusIncome(), // updated
                'SocialIncome' => $application->getAdditionalInfo()->getSocialIncome(), // updated
                'IncomeRent' => $application->getAdditionalInfo()->getRentIncome(), // updated
                'CivilContractIncome' => $application->getAdditionalInfo()->getCivilContractIncome(), // updated
                'PensionIncome' => $application->getAdditionalInfo()->getPension(), // updated
//                '$paymentHour' => $application->getPaymentHour(), // not in API
                'TotalMonthlyIncome' => $application->getAdditionalInfo()->getIncomesTotal(), // updated
                'TotalMonthlyExpense' => $application->getIncomesAndExpenses()->getExpensesTotal(), // matched

                'PartnerPhone' => $this->telephoneWithLeadingZero($application->getContactPerson()->getContactPersonMobileTelephone()), // new
                'PartnerType' => $application->getContactPerson()->getContactPersonRelationship(), // new
                'PartnerFirstName' => $application->getContactPerson()->getContactPersonFirstName(), // new
                'PartnerMiddleName' => $application->getContactPerson()->getContactPersonMiddleName(), // new
                'PartnerLastName' => $application->getContactPerson()->getContactPersonLastName(), // new
                'PartnerDateOfBirth' => $this->dateToString($application->getContactPerson()->getContactPersonDateOfBirth()), // new

	            'ClientExpensesFoodAndClothes' => $application->getIncomesAndExpenses()->getHouseholdExpensesFoodAndClothes(), //new
	            'ClientExpensesKids' => $application->getIncomesAndExpenses()->getHouseholdExpensesKids(), //new
	            'ClientExpensesHeating' => $application->getIncomesAndExpenses()->getHouseholdExpensesHeating(), //new
	            'ClientExpensesTransport' => $application->getIncomesAndExpenses()->getHouseholdExpensesTransport(), //new
	            'ClientExpensesDrinksAndCigarettes' => $application->getIncomesAndExpenses()->getHouseholdExpensesDrinksCigarettes(), //new

                'HomeTypeID' => $this->hasProperty($application), // matched
//                '$hasCar' => $application->getHasCar(), // not in API
                'CarNumber' => $application->getProperty()->getNumberOfCars(), // matched

                'Agreements' => $this->agreements($application), // matched
                'IsRefinans' => $application->getIsRefinance() ?? false, // matched
                'PEP' => $application->getAdditionalInfo()->getIAmPep(), // matched
                'MilitaryStatus' => $application->getAdditionalInfo()->getIAmMilitaryPerson(), // matched
            ]
        ];
    }

    private function mapMethodReceivingSalary(string $method): ?int
    {
        if ($method === MethodsReceivingSalary::CASH) {
            return 0;
        }

        if ($method === MethodsReceivingSalary::BANK_ACCOUNT) {
            return 1;
        }

        if ($method === MethodsReceivingSalary::WITHOUT_INCOME) {
            return null;
        }

        return null;
    }

    private function agreements(OnlineApplication $application): array
    {
        $agreements = [];

	    if ($application->getConfirmation()->getAgreedToProcessingOfPersonalData()) {
		    $agreements[] = 'adbdfdd0-d871-4ed4-bd37-482eaabae854'; // Contact person agreed to give personal data (Потвърждение, че клиентът има съгласието на лицето, посочено като лице за контакт, да предостави данните му и ние да ги обработим задължително)
	    }

        if ($application->getCreditDetails()->getHowShouldMoneyBeReceived() === MethodsToReceiveCredit::BANK_ACCOUNT) {
            $agreements[] = '3dbd9ec6-d1d4-4afc-b9af-61df92712e7b'; // Потвърждение за номера на банковата карта
        }

        return $agreements;
    }

    private function hasProperty(OnlineApplication $application): ?string
    {
        if ($application->getProperty()->getHasProperty() === PropertyType::I_DONT_OWN_A_PROPERTY) {
            return '00000000-0000-0000-0000-000000000000';
        } else {
            return $application->getProperty()->getHasProperty();
        }
    }

    /**
     * @param DateTimeInterface|null $date
     * @return string
     */
    private function dateToString(?DateTimeInterface $date): ?string
    {
        if ($date === null) {
            return null;
        }

        $timestamp = $date->getTimestamp();

        $timezone = timezone_open('Europe/Sofia');
        $dateTimeGMT = clone $date;
        $dateTimeGMT->setTimeZone(timezone_open("Etc/GMT"));
        $dateTimeGMT->setTimestamp($timestamp);

        // Get offset in hours
        $GMTSecondsOffset = timezone_offset_get($timezone, $dateTimeGMT);
        $GMTHourOffset = $GMTSecondsOffset / 3600;
        $GMTHourOffset = sprintf("%02d", $GMTHourOffset);

        // Apply offset to timestamp
        if (0 > $GMTHourOffset) {
            $sign = "-";
        } else {
            $sign = "+";
        }

        return '/Date(' . $timestamp . '000' . $sign . $GMTHourOffset . '00)/';
    }

    private function iban(OnlineApplication $application): ?array
    {
        if ($application->getCreditDetails()->getIban()) {
            return [$application->getCreditDetails()->getIban()];
        }

        return null;
    }

    private function telephoneWithLeadingZero(string $telephone): string
    {
        return '0' . $telephone;
    }

    private function getKycId(OnlineApplication $application): ?string
    {
        if ($application->getCreditDetails()->getSelectedPaymentInstrument() !== null) {
            return $application->getCreditDetails()->getSelectedPaymentInstrument();
        }

        return $application->getKycSessionId();
    }
}