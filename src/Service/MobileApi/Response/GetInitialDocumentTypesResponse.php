<?php


namespace App\Service\MobileApi\Response;


use App\Service\MobileApi\DataTransferObject\DocumentType;

class GetInitialDocumentTypesResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0;

        parent::__construct($data);
    }

    /**
     * @return DocumentType[]
     */
    public function getDocumentTypes(): array
    {
        $result = [];

        foreach ($this->data['Result'] as $item) {
            $result[] = new DocumentType($item['key'], $item['value']);
        }

        return $result;
    }
}