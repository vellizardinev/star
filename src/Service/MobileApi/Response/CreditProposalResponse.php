<?php


namespace App\Service\MobileApi\Response;


class CreditProposalResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0 && !empty($data['ClientPassportAddress']);

        parent::__construct($data);
    }

    public function getData(): array
    {
        return $this->data;
    }
}