<?php


namespace App\Service\MobileApi\Response;


class UpdateAccountResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['Result']['Success'] === true;

        parent::__construct($data);
    }

    public function hasErrors(): bool
    {
        return !empty($this->data['Result']['Errors']);
    }

    public function getErrors(): array
    {
        if (array_key_exists('Errors', $this->data['Result'])) {
            return $this->data['Result']['Errors'];
        }

        return [];
    }
}