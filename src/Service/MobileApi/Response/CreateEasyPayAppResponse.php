<?php


namespace App\Service\MobileApi\Response;


class CreateEasyPayAppResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = !empty($data) && $data['error'] === null && !empty($data['appId'] && !empty($data['pageId']));

        parent::__construct($data);
    }

    public function getAppId(): ?string
    {
        if ($this->isSuccess() === true) {
            return $this->data['appId'];
        }

        return null;
    }

    public function getPageId(): ?string
    {
        if ($this->isSuccess() === true) {
            return $this->data['pageId'];
        }

        return null;
    }
}