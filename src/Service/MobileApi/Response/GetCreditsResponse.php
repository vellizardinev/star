<?php


namespace App\Service\MobileApi\Response;


use App\Service\MobileApi\DataTransferObject\ActiveCredit;
use App\Service\MobileApi\DataTransferObject\CompletedCredit;
use App\Service\MobileApi\DataTransferObject\CreditApplication;
use Doctrine\Common\Collections\ArrayCollection;

class GetCreditsResponse extends BaseResponse
{
    private $activeCredits;

    private $lastCompletedCredit;

    private $creditApplications;

    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0;

        $this->activeCredits = new ArrayCollection();
        $this->creditApplications = new ArrayCollection();

        if (is_array($data['ActiveCredits'])) {
            $this->createActiveCredits($data['ActiveCredits']);
            $this->createCreditApplications($data['ActiveCredits']);
        }

        if (is_array($data['ProposalCredits'])) {
            $this->createActiveCredits($data['ProposalCredits']);
            $this->createCreditApplications($data['ProposalCredits']);
        }

        if ($data['LastCompletedCredit'] !== null) {
            $this->lastCompletedCredit = (new CompletedCredit())
                ->setContractCode($data['LastCompletedCredit']['CodeContract'])
                ->setAmount($data['LastCompletedCredit']['Amount'])
                ->setDateCompleted($data['LastCompletedCredit']['CompleteDate'])
                ->setCreditId($data['LastCompletedCredit']['CreditID'])
            ;
        }

        parent::__construct($data);
    }

    /**
     * @return ArrayCollection|ActiveCredit[]
     */
    public function getActiveCredits(): ArrayCollection
    {
        return $this->activeCredits;
    }

    public function getLastCompletedCredit(): ?CompletedCredit
    {
        return $this->lastCompletedCredit;
    }

    /**
     * @return ArrayCollection|CreditApplication[]
     */
    public function getCreditApplications(): ArrayCollection
    {
        return $this->creditApplications;
    }

    public function creditApplication(string $erpId): ?CreditApplication
    {
        foreach ($this->getCreditApplications()->toArray() as $application) {
            /** @var $application CreditApplication */
            if ($application->getCreditId() === $erpId) {
                return $application;
            }
        }

        return null;
    }

    public function getCreditIds(): array
    {
        $result = [];

        foreach ($this->creditApplications as $creditApplication) {
            /** @var $creditApplication CreditApplication */
            $result[] = $creditApplication->getCreditId();
        }

        foreach ($this->activeCredits as $activeCredit) {
            /** @var $activeCredit ActiveCredit */
            $result[] = $activeCredit->getCreditId();
        }

        return $result;
    }
    
    private function createActiveCredits(array $data): void
    {
        foreach ($data as $creditData) {
            if (!$this->isActiveCredit($creditData)) {
                continue;
            }

            $credit = (new ActiveCredit)
                ->setContractCode($creditData['CodeContract'])
                ->setAmount($creditData['Amount'])
                ->setPayment($creditData['LastActiveCreditDueAmount'])
                ->setEmployee($creditData['LastCreditConsultantName'])
                ->setEmployeeTelephone($creditData['LastCreditConsultantMobilePhone'])
                ->setIban($creditData['LastCreditVirtualIBAN'])
                ->setPaymentPlan($creditData['LastActiveCreditRepaymentPlan'])
                ->setFines($creditData['LastActiveCreditFines'])
                ->setCreditId($creditData['CreditID'])
                ->setIsOnlineChannel($creditData['IsOnlineChannel'])
            ;

            $this->activeCredits->add($credit);
        }
    }
    
    private function createCreditApplications(array $data): void 
    {
        foreach ($data as $creditData) {
            if (!$this->isCreditApplication($creditData)) {
                continue;
            }

            $application = (new CreditApplication())
                ->setCreditId($creditData['CreditID'])
                ->setAmount($creditData['Amount'])
                ->setApplicationDateFromString($creditData['ApplicationDate'])
                ->setStatus($creditData['State'])
                ->setDaysDelayForSigning($creditData['DaysDelayForSigning'])
                ->setIsOnlineChannel($creditData['IsOnlineChannel'])
            ;

            $this->creditApplications->add($application);
        }
    }

    private function isCreditApplication(array $creditData): bool 
    {
        if ($creditData['State'] === 0 ||
            $creditData['State'] === 2 ||
            $creditData['State'] === 11 ||
            $creditData['State'] === 12 ||
            $creditData['State'] === 13 ||
            $creditData['State'] === 14 ||
            $creditData['State'] === 15
        ) {
            return true;
        }
        
        return false;
    }

    private function isActiveCredit(array $creditData): bool
    {
        if ($creditData['State'] === 1 ||
            $creditData['State'] === 7 ||
            $creditData['State'] === 8
        ) {
            return true;
        }

        return false;
    }

    public function getBonusPoints(): int
    {
        if (empty($this->data['ClientBonusPoints'])) {
            return 0;
        }

        $points = 0;

        foreach ($this->data['ClientBonusPoints'] as $contracts) {
            $points += (int) $contracts['Points'];
        }

        return $points;
    }
}