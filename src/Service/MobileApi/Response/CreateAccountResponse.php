<?php


namespace App\Service\MobileApi\Response;


use App\Enum\CreateUserAccountErrors;

class CreateAccountResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['Result']['Success'] === true;

        parent::__construct($data);
    }

    public function getValidationError(): ?string
    {
        if ($this->isSuccess() === true) {
            return null;
        }

        if ($this->data['Result'] === 1) {
            return CreateUserAccountErrors::ERROR_EGN_EXISTS;
        }

        if ($this->data['Result'] === 3) {
            return CreateUserAccountErrors::ERROR_EMAIL_EXISTS;
        }

        if ($this->data['Result'] === 4) {
            return CreateUserAccountErrors::ERROR_INVALID_PASSWORD;
        }
        if ($this->data['Result'] === 5) {
            return CreateUserAccountErrors::ERROR_INVALID_EGN;
        }

        if ($this->data['Result'] === 6) {
            return CreateUserAccountErrors::ERROR_INVALID_TELEPHONE;
        }

        if ($this->data['Result'] === 7) {
            return CreateUserAccountErrors::USER_ACCOUNT_DOES_NOT_EXIST;
        }


        return CreateUserAccountErrors::ERROR_UNKNOWN;
    }

    public function hasErrors(): bool
    {
        return !empty($this->data['Result']['Errors']);
    }

    public function getErrors(): array
    {
        if (array_key_exists('Errors', $this->data['Result'])) {
            return $this->data['Result']['Errors'];
        }

        return [];
    }
}