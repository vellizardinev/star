<?php


namespace App\Service\MobileApi\Response;


class GetRefinanceSumResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0;

        parent::__construct($data);
    }

    public function getRefinance()
    {
        return $this->data['Result'];
    }
}