<?php


namespace App\Service\MobileApi\Response;


abstract class BaseResponse
{
    protected $isSuccess = true;

    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }
}