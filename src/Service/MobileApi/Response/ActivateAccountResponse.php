<?php


namespace App\Service\MobileApi\Response;


class ActivateAccountResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0 && $data['Result'] !== false;

        parent::__construct($data);
    }
}