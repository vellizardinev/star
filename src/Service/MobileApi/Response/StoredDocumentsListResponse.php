<?php


namespace App\Service\MobileApi\Response;


use App\Service\MobileApi\DataTransferObject\StoredDocument;

class StoredDocumentsListResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0;

        parent::__construct($data);
    }

    /**
     * @return StoredDocument[]
     */
    public function getStoredDocuments(): array
    {
        $result = [];

        foreach ($this->data['Result'] as $item) {
            $result[] = new StoredDocument($item['value'], $item['key']);
        }

        usort($result,
            function (StoredDocument $document1, StoredDocument $document2) {
                return strcmp($document1->getName(), $document2->getName()) > 0;
            });

        return $result;
    }
}