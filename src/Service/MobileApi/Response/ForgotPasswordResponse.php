<?php


namespace App\Service\MobileApi\Response;


class ForgotPasswordResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0 && $data['Result'] === true;

        parent::__construct($data);
    }
}