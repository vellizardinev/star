<?php


namespace App\Service\MobileApi\Response;


class SignDocumentsResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0 && $data['Result'] === 0;

        parent::__construct($data);
    }

    public function isCodeWrong(): bool
    {
        return $this->data['Result'] === 16;
    }

    public function hasCodeExpired(): bool
    {
        return $this->data['Result'] === 17;
    }

    public function hasAlreadySigned(): bool
    {
        return $this->data['Result'] === 15;
    }
}