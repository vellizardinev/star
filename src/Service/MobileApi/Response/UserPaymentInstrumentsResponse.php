<?php


namespace App\Service\MobileApi\Response;



use App\Entity\OnlineApplication\UserPaymentInstrument;

class UserPaymentInstrumentsResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0;

        parent::__construct($data);
    }

    public function getUserPaymentInstruments(): array
    {
        $result = [];
        $result[] = new UserPaymentInstrument(UserPaymentInstrument::NEW_CARD, UserPaymentInstrument::NEW_CARD);

        if (is_array($this->data['Result'])) {
            foreach ($this->data['Result'] as $dataItem) {
                $result[] = new UserPaymentInstrument($dataItem['InstrumentInfo'], $dataItem['KycSessionID']);
            }
        }

        return $result;
    }
}
