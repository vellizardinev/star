<?php


namespace App\Service\MobileApi\Response;


class GetRepaymentSumResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0;

        parent::__construct($data);
    }

    public function getRepayment()
    {
        return $this->data['Result'];
    }
}