<?php


namespace App\Service\MobileApi\Response;


class ChangePasswordResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0 && (bool) $data['Result'] === true;

        parent::__construct($data);
    }
}