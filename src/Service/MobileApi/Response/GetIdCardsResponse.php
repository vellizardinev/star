<?php


namespace App\Service\MobileApi\Response;


use App\Service\MobileApi\DataTransferObject\ApplicationFileStatus;
use Doctrine\Common\Collections\ArrayCollection;

class GetIdCardsResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0;

        parent::__construct($data);
    }

    /**
     * @return ArrayCollection|ApplicationFileStatus[]
     */
    public function getApplicationFileStatuses(): ArrayCollection
    {
        $statuses = new ArrayCollection();

        foreach ($this->data['Result'] as $data) {
            $status = (new ApplicationFileStatus)
                ->setStatus($data['Status'])
                ->setFileName($data['FileName'])
                ->setFileType($data['Type'])
            ;

            $statuses->add($status);
        }

        return $statuses;
    }
}