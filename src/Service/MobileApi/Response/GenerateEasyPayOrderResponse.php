<?php


namespace App\Service\MobileApi\Response;


class GenerateEasyPayOrderResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = !empty($data) && $data['error'] === null && !empty($data['forwardUrl']);

        parent::__construct($data);
    }

    public function getUrl(): ?string
    {
        if ($this->isSuccess() === true) {
            return $this->data['forwardUrl'];
        }

        return null;
    }
}