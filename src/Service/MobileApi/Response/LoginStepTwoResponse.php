<?php


namespace App\Service\MobileApi\Response;


use App\Entity\Customer;

class LoginStepTwoResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['RequestResult'] === 0 && $data['HasUserAccount'] === true;

        parent::__construct($data);
    }

    public function getCustomer(): ?Customer
    {
        if ($this->isSuccess() === false) {
            return null;
        }

        return (new Customer)
            ->setFirstName($this->data['FirstName'])
            ->setMiddleName($this->data['MiddleName'])
            ->setLastName($this->data['LastName'])
            ->setPersonalIdentificationNumber($this->data['EGN'])
            ->setTelephone(ltrim($this->data['MobilePhoneNumber'], '0'))
            ->setEmail($this->data['Email'])
            ->setToken($this->data['GCMAuthorizationToken'])
            ->setIsEmailActive($this->data['IsEmailActive'])
            ->setIsPhoneActive($this->data['IsPhoneActive'])
            ->setHasUserAccount($this->data['HasUserAccount'])
            ->setPassword($this->data['Password']);
    }
}