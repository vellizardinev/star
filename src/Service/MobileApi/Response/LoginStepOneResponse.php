<?php


namespace App\Service\MobileApi\Response;


class LoginStepOneResponse extends BaseResponse
{
    public function __construct(array $data)
    {
        $this->isSuccess = $data['Status'] === 0 && $data['HasUserAccount'] === true;

        parent::__construct($data);
    }
}