<?php


namespace App\Service;


use App\Entity\CareersPartner;
use App\Entity\Credit;
use App\Entity\Department;
use App\Entity\Faq;
use App\Entity\Gift;
use App\Entity\JobOpening;
use App\Entity\LoyaltyPartner;
use App\Entity\News;
use App\Entity\Office;
use App\Entity\Poll;
use App\Entity\Promotion;
use Doctrine\ORM\EntityManagerInterface;

class EntityTranslator
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function translateNews(News $news): News
    {
        $repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');
        $title = $news->getTitle();
        $introduction = $news->getIntroduction();
        $content = $news->getContent();

        $repository
            ->translate($news, 'title', 'ru', $title)
            ->translate($news, 'introduction', 'ru', $introduction)
            ->translate($news, 'content', 'ru', $content)
            ->translate($news, 'title', 'en', $title)
            ->translate($news, 'introduction', 'en', $introduction)
            ->translate($news, 'content', 'en', $content);

        $this->manager->persist($news);
        $this->manager->flush();

        return $news;
    }

    public function translatePromotion(Promotion $promotion): Promotion
    {
        $repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');
        $title = $promotion->getTitle();
        $description = $promotion->getDescription();

        $repository
            ->translate($promotion, 'title', 'ru', $title)
            ->translate($promotion, 'description', 'ru', $description)
            ->translate($promotion, 'title', 'en', $title)
            ->translate($promotion, 'description', 'en', $description);

        $this->manager->persist($promotion);
        $this->manager->flush();

        return $promotion;
    }

    public function translateFaq(Faq $faq): Faq
    {
        $repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');
        $question = $faq->getQuestion();
        $answer = $faq->getAnswer();

        $repository
            ->translate($faq, 'question', 'ru', $question)
            ->translate($faq, 'answer', 'ru', $answer)
            ->translate($faq, 'question', 'en', $question)
            ->translate($faq, 'answer', 'en', $answer);

        $this->manager->persist($faq);
        $this->manager->flush();

        return $faq;
    }

	public function translateGift(Gift $gift): Gift
	{
		$repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');
		$name = $gift->getName();

		$repository
			->translate($gift, 'name', 'ru', $name)
			->translate($gift, 'name', 'en', $name);

		$this->manager->persist($gift);
		$this->manager->flush();

		return $gift;
	}

	public function translateOffice(Office $office): Office
	{
		$repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');
		$name = $office->getName();
		$address = $office->getAddress();
		$workingTime = $office->getWorkingTime();

		$repository
			->translate($office, 'name', 'ru', $name)
			->translate($office, 'address', 'ru', $address)
			->translate($office, 'workingTime', 'ru', $workingTime)
			->translate($office, 'name', 'en', $name)
			->translate($office, 'address', 'en', $address)
			->translate($office, 'workingTime', 'en', $workingTime);

		$this->manager->persist($office);
		$this->manager->flush();

		return $office;
	}

    public function translateCredit(Credit $credit): Credit
    {
        $repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');

        $title = $credit->getTitle();
        $shortDescription = $credit->getShortDescription();
        $fullDescription = $credit->getFullDescription();
        $parametersDescription = $credit->getParametersDescription();
        $requirements = $credit->getRequirements();

        $repository
            ->translate($credit, 'title', 'ru', $title)
            ->translate($credit, 'shortDescription', 'ru', $shortDescription)
            ->translate($credit, 'fullDescription', 'ru', $fullDescription)
            ->translate($credit, 'parametersDescription', 'ru', $parametersDescription)
            ->translate($credit, 'requirements', 'ru', $requirements)
            ->translate($credit, 'title', 'en', $title)
            ->translate($credit, 'shortDescription', 'en', $shortDescription)
            ->translate($credit, 'fullDescription', 'en', $fullDescription)
            ->translate($credit, 'parametersDescription', 'en', $parametersDescription)
            ->translate($credit, 'requirements', 'en', $requirements);

        $this->manager->persist($credit);
        $this->manager->flush();

        return $credit;
    }

    public function translatePoll(Poll $poll): Poll
    {
        $repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');
        $title = $poll->getTitle();
        $description = $poll->getDescription();

        $repository
            ->translate($poll, 'title', 'ru', $title)
            ->translate($poll, 'description', 'ru', $description)
            ->translate($poll, 'title', 'en', $title)
            ->translate($poll, 'description', 'en', $description);

        $this->manager->persist($poll);
        $this->manager->flush();

        return $poll;
    }

	public function translateDepartment(Department $department): Department
	{
		$repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');
		$name = $department->getName();
		$director = $department->getDirector();
		$description = $department->getDescription();

		$repository
			->translate($department, 'name', 'ru', $name)
			->translate($department, 'director', 'ru', $director)
			->translate($department, 'description', 'ru', $description)
			->translate($department, 'name', 'en', $name)
			->translate($department, 'director', 'en', $director)
			->translate($department, 'description', 'en', $description);

		$this->manager->persist($department);
		$this->manager->flush();

		return $department;
	}

	public function translateJobOpening(JobOpening $jobOpening): JobOpening
	{
		$repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');
		$position = $jobOpening->getPosition();
		$description = $jobOpening->getDescription();
		$reference = $jobOpening->getReference();
		$slug = $jobOpening->getSlug();

		$repository
			->translate($jobOpening, 'position', 'ru', $position)
			->translate($jobOpening, 'description', 'ru', $description)
			->translate($jobOpening, 'reference', 'ru', $reference)
			->translate($jobOpening, 'slug', 'ru', $slug)
			->translate($jobOpening, 'position', 'en', $position)
			->translate($jobOpening, 'description', 'en', $description)
			->translate($jobOpening, 'reference', 'en', $reference)
			->translate($jobOpening, 'slug', 'en', $slug)
		;

		$this->manager->persist($jobOpening);
		$this->manager->flush();

		return $jobOpening;
	}

	public function translateLoyaltyPartner(LoyaltyPartner $loyaltyPartner): LoyaltyPartner
	{
		$repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');
		$name = $loyaltyPartner->getName();
		$description = $loyaltyPartner->getDescription();

		$repository
			->translate($loyaltyPartner, 'name', 'ru', $name)
			->translate($loyaltyPartner, 'description', 'ru', $description)
			->translate($loyaltyPartner, 'name', 'en', $name)
			->translate($loyaltyPartner, 'description', 'en', $description);

		$this->manager->persist($loyaltyPartner);
		$this->manager->flush();

		return $loyaltyPartner;
	}

    public function translateCareerPartner(CareersPartner $careersPartner): CareersPartner
    {
        $repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');
        $name = $careersPartner->getName();
        $description = $careersPartner->getDescription();

        $repository
            ->translate($careersPartner, 'name', 'ru', $name)
            ->translate($careersPartner, 'description', 'ru', $description)
            ->translate($careersPartner, 'name', 'en', $name)
            ->translate($careersPartner, 'description', 'en', $description);

        $this->manager->persist($careersPartner);
        $this->manager->flush();

        return $careersPartner;
    }
}