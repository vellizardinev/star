<?php

namespace App\DataFixtures;

use App\Factory\MailFactory;
use App\Factory\NewsFactory;
use Doctrine\Persistence\ObjectManager;

class MailFixtures extends BaseFixtures
{
    /**
     * @var NewsFactory
     */
    private $mailFactory;

    /**
     * @param MailFactory $mailFactory
     */
    public function __construct(MailFactory $mailFactory)
    {
        $this->mailFactory = $mailFactory;
    }

    protected function loadData(ObjectManager $manager)
    {
    }
}
