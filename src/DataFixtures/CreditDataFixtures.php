<?php

namespace App\DataFixtures;

use App\Entity\Credit;
use App\Entity\CreditData;
use App\Factory\CreditFactory;
use App\Service\MobileApi\CreditProducts;
use App\Service\MobileApi\UpdateCreditData;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CreditDataFixtures extends BaseFixtures implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @var CreditFactory
     */
    private $creditFactory;
    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
	/**
	 * @var CreditProducts
	 */
	private $creditProductsService;

    private $updateCreditDataService;

    public function __construct(CreditFactory $creditFactory, EntityManagerInterface $entityManager, CreditProducts $creditProductsService, UpdateCreditData $updateCreditDataService)
    {
        $this->creditFactory = $creditFactory;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        $this->entityManager = $entityManager;
        $this->creditProductsService = $creditProductsService;
        $this->updateCreditDataService = $updateCreditDataService;
    }

    /**
     * @param ObjectManager $manager
     * @throws DBALException
     */
    protected function loadData(ObjectManager $manager)
    {
        // Option A) Insert SQL dump. Use this when speed is needed, e.g. - when running tests
//        $this->loadFromDump();

        // Option B) Parse contents of csv files. Use this when the base data in the csv files has been updated and the DB needs to have the fresh data as well.
        // If this  is the case, the SQL dump should also be updated.
//        $this->loadDataFromDir('iCredit');
//        $this->loadDataFromDir('iMonth');
//        $this->loadDataFromDir('Pensioner');
//        $this->loadDataFromDir('BiMonth');
//        $this->loadDataFromDir('iWeek');

	    // Option C) Load credit data from SmartIT API
	    $this->loadDataFromApi();
    }

    protected function loadDataFromDir(string $directory)
    {
        $credit = $this->manager->getRepository(Credit::class)->findOneBy(['title' => $directory]);

        $fileNames = array_slice(scandir(__DIR__ . '/credit_data' . DIRECTORY_SEPARATOR . $directory), 2);

        $this->loadAmountsAndPeriods(
            __DIR__ . '/credit_data' . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $fileNames[0],
            $credit
        );

        foreach ($fileNames as $fileName) {
            $filePath = __DIR__ . '/credit_data' . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $fileName;
            $this->loadDataFromFile($fileName, $filePath, $credit);
        }
    }

    protected function loadAmountsAndPeriods(string $filePath, Credit $credit)
    {
        $handle = fopen($filePath, "r");

        $periods = (fgetcsv($handle, 0, ';'));
        array_shift($periods);

        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            foreach ($periods as $period) {
                $creditData = (new CreditData)
                    ->setPeriods($period)
                    ->setAmount($data[0])
                    ->setCredit($credit)
                    ->setPaymentSchedule($credit->getPaymentSchedule());
                $this->manager->persist($creditData);
            }
        }

        fclose($handle);

        $this->manager->flush();
    }

    protected function loadDataFromFile(string $fileName, string $filePath, Credit $credit)
    {
        $parts = explode('-', $fileName);
        $property = array_shift($parts);

        $handle = fopen($filePath, "r");

        $periods = fgetcsv($handle, 0, ';');
        array_shift($periods);

        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            $index = 1;

            foreach ($periods as $period) {
                $creditData = $this->manager->getRepository(CreditData::class)->findOneBy(
                    [
                        'credit' => $credit,
                        'amount' => $data[0],
                        'periods' => $period,
                    ]
                );

                $this->propertyAccessor->setValue(
                    $creditData,
                    $property,
                    $this->normalizeValue($data[$index])
                );

                $index++;
            }
        }

        $this->manager->flush();

        fclose($handle);
    }

    protected function normalizeValue(string $value)
    {
        $value = str_replace(',', '.', $value);
        $value = ((float)$value);

        if ($value === 0.00) {
            $value = null;
        }

        return $value;
    }

    /**
     * @throws DBALException
     */
    protected function loadFromDump()
    {
        $finder = new Finder();
        $finder->in(__DIR__ . '/credit_data');
        $finder->name('credit_data.sql');

        foreach( $finder as $file ){
            $content = $file->getContents();

            $stmt = $this->entityManager->getConnection()->prepare($content);
            $stmt->execute();
        }
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
	private function loadDataFromApi()
	{
		$creditProducts = $this->creditProductsService->creditProducts();

		$this->updateCreditDataService->update($creditProducts);
	}

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [CreditFixtures::class];
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['credit_data'];
    }
}
