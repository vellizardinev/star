<?php

namespace App\DataFixtures;

use App\File\FileManager;
use DirectoryIterator;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use League\Flysystem\FileExistsException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileFixtures extends BaseFixtures implements FixtureGroupInterface
{
    /**
     * @var FileManager
     */
    private $fileManager;
    /**
     * @var string
     */
    private $projectDir;

    /**
     * @param FileManager $fileManager
     * @param string $projectDir
     */
    public function __construct(FileManager $fileManager, string $projectDir)
    {
        $this->fileManager = $fileManager;
        $this->projectDir = $projectDir;
    }

    /**
     * @param ObjectManager $manager
     * @throws FileExistsException
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->clearOldFiles(
            $this->projectDir . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'files'
        );
        $this->clearOldFiles(
            $this->projectDir . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images'
        );

        $this->loadImages(__DIR__ . DIRECTORY_SEPARATOR . 'images');
        $this->loadDocuments(__DIR__ . DIRECTORY_SEPARATOR . 'files');
    }

    /**
     * @param string $path
     * @throws FileExistsException
     */
    protected function loadImages(string $path)
    {
        setlocale(LC_CTYPE, 'uk_UA.utf8');

        foreach (new DirectoryIterator($path) as $fileInfo) {
            if($fileInfo->isDot()) continue;
            $uploadedFile = new UploadedFile($fileInfo->getRealPath(), $fileInfo->getBasename());

            $file = $this->fileManager->uploadImage($uploadedFile);
            $this->manager->persist($file);
            $this->manager->flush();
        }
    }

    /**
     * @param string $path
     * @throws FileExistsException
     */
    protected function loadDocuments(string $path)
    {
        setlocale(LC_CTYPE, 'uk_UA.utf8');

        foreach (new DirectoryIterator($path) as $fileInfo) {
            if($fileInfo->isDot()) continue;

            $uploadedFile = new UploadedFile($fileInfo->getRealPath(), $fileInfo->getBasename());

            $file = $this->fileManager->uploadFile($uploadedFile);
            $this->manager->persist($file);
            $this->manager->flush();
        }
    }

    /**
     * @param string $path
     */
    protected function clearOldFiles(string $path)
    {
        $finder = new Finder();
        $finder->in($path);
        $finder->name('/.*/');

        foreach ($finder as $file) {
            unlink($file->getRealPath());
        }
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['files', 'module_items'];
    }
}
