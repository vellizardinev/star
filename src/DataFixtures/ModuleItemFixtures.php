<?php

namespace App\DataFixtures;

use App\Entity\Credit;
use App\Entity\Module;
use App\Entity\ModuleItem;
use App\Entity\Page;
use App\Enum\MapTypes;
use App\Enum\ModuleItems;
use App\Enum\ModuleWidths;
use App\Enum\Pages;
use App\Factory\ModuleItemFactory;
use App\Module\Parameter\BecomeCreditConsultant;
use App\Module\Parameter\Content;
use App\Module\Parameter\CreditCalculator;
use App\Module\Parameter\ExpandableContent;
use App\Module\Parameter\Footer;
use App\Module\Parameter\Header;
use App\Module\Parameter\iCreditAsEmployer;
use App\Module\Parameter\ItemsCount;
use App\Module\Parameter\Link;
use App\Module\Parameter\OfficeMap;
use App\Module\Parameter\Slide;
use App\Module\Parameter\Slider;
use App\Module\Parameter\WhyYouShouldTrustUs;
use App\Service\FileUrlMigration;
use App\Service\UserDefinedPageRouting;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use ReflectionException;
use Symfony\Component\Routing\RouterInterface;

class ModuleItemFixtures extends BaseFixtures implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @var ModuleItemFactory
     */
    private $moduleItemFactory;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var UserDefinedPageRouting
     */
    private $userDefinedPageRouting;
    /**
     * @var string
     */
    private $locales;
    /**
     * @var FileUrlMigration
     */
    private $fileUrlMigration;

    /**
     * @param ModuleItemFactory $moduleItemFactory
     * @param RouterInterface $router
     * @param UserDefinedPageRouting $userDefinedPageRouting
     * @param string $locales
     * @param FileUrlMigration $fileUrlMigration
     */
    public function __construct(
        ModuleItemFactory $moduleItemFactory,
        RouterInterface $router,
        UserDefinedPageRouting $userDefinedPageRouting,
        string $locales,
        FileUrlMigration $fileUrlMigration
    ) {
        $this->moduleItemFactory = $moduleItemFactory;
        $this->router = $router;
        $this->userDefinedPageRouting = $userDefinedPageRouting;
        $this->locales = explode('|', $locales);
        $this->fileUrlMigration = $fileUrlMigration;
    }

    public function getDependencies()
    {
        return array(
            PageFixtures::class,
            ModuleFixtures::class,
            FileFixtures::class
        );
    }

    /**
     * @param ObjectManager $manager
     * @throws ReflectionException
     */
    protected function loadData(ObjectManager $manager)
    {
        /** @var Page $termsAndConditionsPage */
        $termsAndConditionsPage = $manager->getRepository(Page::class)->findOneBy(
            ['name' => Pages::TERMS_AND_CONDITIONS]
        );

        /** @var Page $iCreditAsEmployerPage */
        $iCreditAsEmployerPage = $manager->getRepository(Page::class)->findOneBy(
            ['name' => Pages::ICREDIT_AS_EMPLOYER]
        );

        /** @var Page $motivationalProgrammePage */
        $motivationalProgrammePage = $manager->getRepository(Page::class)->findOneBy(
            ['name' => Pages::MOTIVATION_PROGRAMME]
        );

        /** @var Page $loyaltyProgramPage */
//        $loyaltyProgramPage = $manager->getRepository(Page::class)->findOneBy(
//            ['name' => Pages::LOYALTY_PROGRAM]
//        );

        // Header
        /** @var ModuleItem $headerModuleItem */
        $headerModuleItem = $this->moduleItemFactory->create(['name' => ModuleItems::HEADER]);
        $headerData = new Header;

        $headerData->addTopLink(new Link('News', $this->router->generate('news.index.ua')));
        $headerData->addTopLink(new Link('Careers', $this->router->generate('careers.index.ua')));
        $headerData->addTopLink(new Link('Contact', $this->router->generate('contact.show.ua')));
        $headerData->addTopLink(new Link('Questions', $this->router->generate('faqs.index.ua')));

        $headerData->addBottomLink(new Link('Credits', $this->router->generate('credits.index.ua')));
        $headerData->addBottomLink(new Link('Promotions', $this->router->generate('promotions.index.ua')));
        $headerData->addBottomLink(new Link('Loyalty Programme', $this->router->generate('loyalty_program.index.ua')));
        $headerData->addBottomLink(new Link('Social Activity', $this->router->generate('social_activity.index.ua')));
        $headerData->addBottomLink(new Link('', ''));

        $headerModuleItem->setData($this->createData($headerData));
        $manager->flush();

        // Footer
        /** @var ModuleItem $footerModuleItem */
        $footerModuleItem = $this->moduleItemFactory->create(['name' => ModuleItems::FOOTER]);
        $footerData = new Footer;

        $footerData->addFirstColumnLink(new Link('All about us', $this->router->generate('static_pages.about_us.ua')));

        $footerData->addFirstColumnLink(
            new Link(
                'Personal data protection', $this->userDefinedPageRouting->generateUrl(
                $termsAndConditionsPage,
                'declaratie-confidentialitate-date-candidati'
            )
            )
        );
        $footerData->addFirstColumnLink(
            new Link(
                'Terms and conditions', $this->userDefinedPageRouting->generateUrl(
                $termsAndConditionsPage
            )
            )
        );
        $footerData->addFirstColumnLink(
            new Link('Frequently asked questions', $this->router->generate('faqs.index.ua'))
        );
        $footerData->addFirstColumnLink(new Link('Contacts', $this->router->generate('contact.show.ua')));
        $footerData->addFirstColumnLink(
            new Link('Employees Login', 'https://easyweb.icredit.ua/MemberShipLogin.aspx?ReturnUrl=%2f')
        );
        $footerData->addFirstColumnLink(new Link('', '#'));
        $footerData->addFirstColumnLink(new Link('', '#'));
        $footerData->addFirstColumnLink(new Link('', '#'));
        $footerData->addFirstColumnLink(new Link('', '#'));

        $credits = $this->manager->getRepository(Credit::class)->findBy([], [], 10);
        foreach ($credits as $credit) {
            $footerData->addSecondColumnLink(
                new Link(
                    $credit->getTitle(),
                    $this->router->generate('credits.show', ['slug' => $credit->getSlug(), '_locale' => 'ua'])
                )
            );
        }

        $footerData->addThirdColumnLink(new Link('Job Openings', $this->router->generate('career.all.ua')));
        $footerData->addThirdColumnLink(new Link('Apply for a job', $this->router->generate('career.apply_for_job.ua')));
        $footerData->addThirdColumnLink(
            new Link('Become a credit consultant', $this->router->generate('career.become_credit_consultant.ua'))
        );
        $footerData->addThirdColumnLink(
            new Link(
                'iCredit as an employer', $this->userDefinedPageRouting->generateUrl(
                $iCreditAsEmployerPage
            )
            )
        );

        $footerModuleItem->setData($this->createData($footerData));
        $manager->flush();

        // CreditCalculator
        /** @var ModuleItem $creditCalculatorModuleItem */
        $creditCalculatorModuleItem = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::CREDIT_CALCULATOR,
                'description' => ModuleItems::CREDIT_CALCULATOR . ' [Home page]',
            ]
        );
        $creditCalculatorData = new CreditCalculator;
        $creditCalculatorData->width = ModuleWidths::STANDARD;
        $creditCalculatorData->backgroundColor = '#f6f6f6';

        $creditCalculatorModuleItem->setData($this->createData($creditCalculatorData));
        $manager->flush();

        $this->loadTermsAndConditionsModules($manager, $termsAndConditionsPage);
        $this->loadICreditAsEmployerModules($manager, $iCreditAsEmployerPage);
        $this->loadMotivationalProgrammeModules($manager, $motivationalProgrammePage);
        $this->loadContentForThankYouPages($manager);
        $this->loadContentForMoneyShopPage($manager);
        $this->loadContentForVeroCashPage($manager);
        $this->loadContentForFamilyFinancesPage($manager);
        $this->loadContentForFinancialCulturePage($manager);
        $this->loadContentForFinancialDictionaryPage($manager);

        // OfficeMap
        /** @var ModuleItem $officeMapModuleItem */
        $officeMapModuleItem = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::OFFICE_MAP,
                'description' => ModuleItems::OFFICE_MAP . ' [Home page]',
            ]
        );
        $officeData = new OfficeMap();
        $officeData->setType(MapTypes::getValue('ROADMAP'));
        $officeData->width = ModuleWidths::FULL;

        $officeMapModuleItem->setData($this->createData($officeData));
        $manager->flush();
        $this->loadSliderForCredits($manager);

        // BecomeCreditConsultant
        /** @var ModuleItem $becomeCreditConsultantModuleItem */
        $becomeCreditConsultantModuleItem = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::BECOME_CREDIT_CONSULTANT,
                'description' => ModuleItems::BECOME_CREDIT_CONSULTANT . ' [Home page]',
            ]
        );

        $content = <<<TAG
<h4>To become a Credit Consultant it is enough to have good communication skills and be ambitious. There are no
        restrictions on gender, age, education nor experience.</h4>
    <ul>
        <li>You become part of an international company that gives you opportunities for development</li>
        <li>You get to enjoy a remuneration system that guarantees you high starting salary</li>
        <li>You get professional paid-by-the-company trainings</li>
    </ul>
TAG;

        $becomeCreditConsultant = new BecomeCreditConsultant();
        $becomeCreditConsultant->headline = 'Become credit consultant';
        $becomeCreditConsultant->content = $content;

        $becomeCreditConsultantModuleItem->setData($this->createData($becomeCreditConsultant));

        $this->loadWhyYouShouldTrustUs();
        $this->loadContentForBecomeCreditConsultant();
        $this->loadContentForAboutUs();
        $this->loadContentForEasyBonus();
        $this->loadContentForApplyForJob();
        $this->loadLatestPromotionsSlider($manager);
        $this->loadICreditAsEmployer();
        $this->loadContentForCustomerFeedbackPage($manager);
        $this->loadContentForOfficesList();
        $this->loadContentForCreditApplicationPage();

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    protected function loadSliderForCredits(ObjectManager $manager): void
    {
        /** @var ModuleItem $sliderNoduleItem */
        $sliderNoduleItem = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::SLIDER,
                'description' => ModuleItems::SLIDER . ' [Credits page]',
            ]
        );

        $sliderData = new Slider();
        $sliderData->textColor = 'white';
        $sliderData->backgroundImage = 'https://www.easycredit.bg/assets/uploads/credits-header-image.jpg';
        $slideArray = [];

        $slide = new Slide();
        $slide->setHeadline('iCredit');
        $slide->setDescription(
            'Один з найшвидших з існуючих в Україні кредитів готівкою для фізичних осіб і СПД. Послуга зручна, проста і прозора.'
        );
        $slideArray[] = $slide;

        $slide = new Slide();
        $slide->setHeadline('BiMonth');
        $slide->setDescription(
            'Продукт BiMonth спеціально створений для тих, кому зручно вносити платежі по кредиту 2 рази на місяць.'
        );
        $slideArray[] = $slide;

        $slide = new Slide();
        $slide->setHeadline('iMonth');
        $slide->setDescription(
            'Швидкі кредити, які погашаються рівними щомісячними внесками.'
        );
        $slideArray[] = $slide;

        $sliderData->setSlides($slideArray);
        $sliderNoduleItem->setData($this->createData($sliderData));

        $manager->flush();
    }

    protected function loadWhyYouShouldTrustUs(): void
    {
        /** @var ModuleItem $whyYouShouldTrustUsModuleItem */
        $whyYouShouldTrustUsModuleItem = $this->moduleItemFactory->create(['name' => ModuleItems::WHY_YOU_SHOULD_TRUST_US]);
        $reasonArray = [
            ['3500', 'credit consultants'],
            ['75%', 'of the Ukrainians know the iCredit brand'],
            ['78%', 'customers would recommend the iCredit brand'],
            ['1 000 000', 'credits in Europe'],
        ];
        $whyYouShouldTrustUs = new WhyYouShouldTrustUs();
        $whyYouShouldTrustUs->backgroundColor = '#ffffff';

        foreach ($whyYouShouldTrustUs->getReasons() as $key => $reason) {
            $reason->number = $reasonArray[$key][0];
            $reason->statement = $reasonArray[$key][1];
        }

        $whyYouShouldTrustUsModuleItem->setData($this->createData($whyYouShouldTrustUs));
    }

    protected function loadContentForBecomeCreditConsultant()
    {
        /** @var ModuleItem $contentBecomeCreditConsultant */
        $contentBecomeCreditConsultant = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::CONTENT,
                'description' => ModuleItems::CONTENT . ' [Become Credit Consultant]',
            ]
        );

        $contentData = new Content();
        $contentData->backgroundColor = '#ffffff';
        $contentData->content = file_get_contents(__DIR__ . '/page_data/become_credit_consultant.txt');

        $contentBecomeCreditConsultant->setData($this->createData($contentData));
    }

    protected function loadContentForAboutUs()
    {
        // Mission and vision
        /** @var ModuleItem $contentMissionAndVision */
        $contentMissionAndVision = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::CONTENT,
                'description' => ModuleItems::CONTENT . ' [About Us: Mission and vision]',
            ]
        );

        $contentData = new Content();
        $contentData->content = <<<TXT
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis enim lobortis scelerisque fermentum dui faucibus in ornare. Senectus et netus et malesuada. Egestas sed tempus urna et pharetra pharetra massa massa. Mauris vitae ultricies leo integer malesuada. Enim sed faucibus turpis in eu mi bibendum neque. Amet consectetur adipiscing elit duis tristique sollicitudin. Suspendisse in est ante in. Nunc sed blandit libero volutpat. Aliquam etiam erat velit scelerisque in.
TXT;

        $contentMissionAndVision->setData($this->createData($contentData));

        // Who are we
        /** @var ModuleItem $contentMissionAndVision */
        $contentWhoAreWe = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::CONTENT,
                'description' => ModuleItems::CONTENT . ' [About Us: Who are we]',
            ]
        );

        $contentData = new Content();
        $contentData->content = file_get_contents(__DIR__ . '/page_data/who_are_we.txt');

        $contentWhoAreWe->setData($this->createData($contentData));
    }

    /**
     * @param ObjectManager $manager
     * @param Page $termsAndConditionsPage
     */
    protected function loadTermsAndConditionsModules(ObjectManager $manager, Page $termsAndConditionsPage): void
    {
        $this->fileUrlMigration->loadFileDataFromCSV(__DIR__ . '/document_links/document_links.csv');

        // Address
        $title = 'Address';
        $content = file_get_contents(__DIR__ . '/page_data/legal_information/address.txt');
        $content = $this->fileUrlMigration->updateUrls($content);
        $this->addExpandableContent($manager, $termsAndConditionsPage, $title, $content);

        // Valid license
        $title = 'Valid license';
        $content = file_get_contents(__DIR__ . '/page_data/legal_information/valid_license.txt');
        $content = $this->fileUrlMigration->updateUrls($content);
        $this->addExpandableContent($manager, $termsAndConditionsPage, $title, $content);

        // Certificate of registration of a financial Institution
        $title = 'Certificate of registration of a financial Institution';
        $content = file_get_contents(__DIR__ . '/page_data/legal_information/certificate_of_registration.txt');
        $content = $this->fileUrlMigration->updateUrls($content);
        $this->addExpandableContent($manager, $termsAndConditionsPage, $title, $content);

        // Additional information
        $title = 'Additional information';
        $content = file_get_contents(__DIR__ . '/page_data/legal_information/additional_information.txt');
        $content = $this->fileUrlMigration->updateUrls($content);
        $this->addExpandableContent($manager, $termsAndConditionsPage, $title, $content);

        // Financial statements disclosure
        $title = 'Financial statements disclosure';
        $content = file_get_contents(__DIR__ . '/page_data/legal_information/financial_statements_disclosure.txt');
        $content = $this->fileUrlMigration->updateUrls($content);
        $this->addExpandableContent($manager, $termsAndConditionsPage, $title, $content);

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    protected function loadLatestPromotionsSlider(ObjectManager $manager): void
    {
        /** @var ModuleItem $latestPromotionsModuleItem */
        $latestPromotionsModuleItem = $this->moduleItemFactory->create(['name' => ModuleItems::LATEST_PROMOTIONS_SLIDER]);
        $latestPromotionsModuleItem->setDescription(ModuleItems::LATEST_PROMOTIONS_SLIDER . '[Home page]');
        $promotionsCount = new ItemsCount();
        $promotionsCount->itemsCount = 5;
        $promotionsCount->width = ModuleWidths::STANDARD;
        $promotionsCount->backgroundColor = '#f6f6f6';

        $latestPromotionsModuleItem->setData($this->createData($promotionsCount));

        $manager->flush();
    }

    protected function loadICreditAsEmployerModules(ObjectManager $manager, Page $page)
    {
        $contentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Content']);

        $contentData = new Content();
        $contentData->content = file_get_contents(__DIR__ . '/page_data/icredit_as_an_employer.html');

        $moduleItem = new ModuleItem();
        $manager->persist($moduleItem);
        $moduleItem->setModule($contentModule);
        $moduleItem->setPage($page);
        $moduleItem->setData($this->createData($contentData));
    }

    protected function loadMotivationalProgrammeModules(ObjectManager $manager, Page $page)
    {
        $contentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Content']);

        $contentData = new Content();
        $contentData->content = file_get_contents(__DIR__ . '/page_data/motivational_programme.txt');

        $moduleItem = new ModuleItem();
        $manager->persist($moduleItem);
        $moduleItem->setModule($contentModule);
        $moduleItem->setPage($page);
        $moduleItem->setData($this->createData($contentData));
    }

    /**
     * @param ObjectManager $manager
     * @param Page $termsAndConditionsPage
     * @param string $title
     * @param string $content
     */
    protected function addExpandableContent(
        ObjectManager $manager,
        Page $termsAndConditionsPage,
        string $title,
        string $content
    ): void {
        $expandableContentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Expandable content']);

        $expandableContentData = new ExpandableContent();

        $expandableContentData->content = $content;
        $expandableContentData->title = $title;

        $moduleItem = new ModuleItem();
        $manager->persist($moduleItem);
        $moduleItem->setModule($expandableContentModule);
        $moduleItem->setPage($termsAndConditionsPage);
        $moduleItem->setData($this->createData($expandableContentData));
    }

    protected function loadContentForEasyBonus()
    {
        /** @var ModuleItem $contentItem */
        $contentItem = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::CONTENT,
                'description' => ModuleItems::CONTENT . ' [Easy bonus]',
            ]
        );

        $contentData = new Content();
        $contentData->content = file_get_contents(__DIR__ . '/page_data/ibonus.html');

        $contentItem->setData($this->createData($contentData));
    }

    protected function loadContentForApplyForJob()
    {
        /** @var ModuleItem $contentItem */
        $contentItem = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::CONTENT,
                'description' => ModuleItems::CONTENT . ' [Apply for a job]',
            ]
        );

        $contentData = new Content();
        $contentData->content = file_get_contents(__DIR__ . '/page_data/apply_for_job.txt');

        $contentItem->setData($this->createData($contentData));
    }

    protected function loadContentForThankYouPages(ObjectManager $manager)
    {
        $contentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Content']);

        $contentData = new Content();
        $contentData->content = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';

        $pageNames = [
            Pages::getValue('CREDIT_THANK_YOU'),
            Pages::CREDIT_LONG_THANK_YOU,
            Pages::getValue('JOB_THANK_YOU'),
            Pages::getValue('CONTACT_THANK_YOU'),
        ];

        $thankYouPages = $manager->getRepository(Page::class)->findBy(
            ['name' => $pageNames]
        );

        foreach ($thankYouPages as $thankYouPage) {
            $moduleItem = new ModuleItem();
            $manager->persist($moduleItem);
            $moduleItem->setModule($contentModule);
            $moduleItem->setPage($thankYouPage);
            $moduleItem->setData($this->createData($contentData));
        }
    }

    protected function loadICreditAsEmployer(): void
    {
        /** @var ModuleItem $moduleItem */
        $moduleItem = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::ICREDIT_AS_EMPLOYER,
                'description' => ModuleItems::ICREDIT_AS_EMPLOYER . ' [Careers page]',
            ]
        );
        $reasonArray = [
            ['More than', '1 mln', 'credits given'],
            ['More than', '2500', 'credit consultants'],
            ['More than', '70%', 'of the Ukrainian population knows the brand iCredit'],
            ['More than', '180', 'offices in Ukraine'],
        ];
        $parameters = new iCreditAsEmployer();
        $parameters->backgroundImage = 'https://www.easycredit.bg/assets/uploads/careers-supervisor-background.jpg';

        foreach ($parameters->getReasons() as $key => $reason) {
            $reason->prefix = $reasonArray[$key][0];
            $reason->number = $reasonArray[$key][1];
            $reason->statement = $reasonArray[$key][2];
        }

        $moduleItem->setData($this->createData($parameters));
    }

    protected function loadContentForMoneyShopPage(ObjectManager $manager)
    {
        $contentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Content']);

        $contentData = new Content();
        $contentData->content = file_get_contents(__DIR__ . '/page_data/money_shop.txt');

        $page = $manager->getRepository(Page::class)->findOneBy(['name' => Pages::MONEY_SHOP]);

        $moduleItem = new ModuleItem();
        $manager->persist($moduleItem);
        $moduleItem->setModule($contentModule);
        $moduleItem->setPage($page);
        $moduleItem->setData($this->createData($contentData));
    }

    protected function loadContentForVeroCashPage(ObjectManager $manager)
    {
        $contentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Content']);

        $contentData = new Content();
        $contentData->content = file_get_contents(__DIR__ . '/page_data/vero_cash.txt');

        $page = $manager->getRepository(Page::class)->findOneBy(['name' => Pages::VERO_CASH]);

        $moduleItem = new ModuleItem();
        $manager->persist($moduleItem);
        $moduleItem->setModule($contentModule);
        $moduleItem->setPage($page);
        $moduleItem->setData($this->createData($contentData));
    }

    protected function loadContentForCustomerFeedbackPage(ObjectManager $manager)
    {
        $contentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Content']);

        $contentData = new Content();
        $contentData->content = file_get_contents(__DIR__ . '/page_data/customer_feedback.txt');

        $page = $manager->getRepository(Page::class)->findOneBy(['name' => Pages::CUSTOMER_FEEDBACK]);

        $moduleItem = new ModuleItem();
        $manager->persist($moduleItem);
        $moduleItem->setModule($contentModule);
        $moduleItem->setPage($page);
        $moduleItem->setData($this->createData($contentData));

        $manager->flush();
    }

	protected function loadContentForFamilyFinancesPage(ObjectManager $manager)
	{
		$contentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Content']);

		$contentData = new Content();
		$contentData->content = file_get_contents(__DIR__ . '/page_data/family_finances.txt');

		$page = $manager->getRepository(Page::class)->findOneBy(['name' => Pages::FAMILY_FINANCES]);

		$moduleItem = new ModuleItem();
		$manager->persist($moduleItem);
		$moduleItem->setModule($contentModule);
		$moduleItem->setPage($page);
		$moduleItem->setData($this->createData($contentData));
	}

    protected function loadContentForFinancialCulturePage(ObjectManager $manager)
    {
        $contentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Content']);

        $contentData = new Content();
        $contentData->content = file_get_contents(__DIR__ . '/page_data/financial_culture.html');

        $page = $manager->getRepository(Page::class)->findOneBy(['name' => Pages::FINANCIAL_CULTURE]);

        $moduleItem = new ModuleItem();
        $manager->persist($moduleItem);
        $moduleItem->setModule($contentModule);
        $moduleItem->setPage($page);
        $moduleItem->setData($this->createData($contentData));
    }

    protected function loadContentForFinancialDictionaryPage(ObjectManager $manager)
    {
        $contentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Content']);

        $contentData = new Content();
        $contentData->content = file_get_contents(__DIR__ . '/page_data/financial_dictionary.html');

        $page = $manager->getRepository(Page::class)->findOneBy(['name' => Pages::FINANCIAL_DICTIONARY]);

        $moduleItem = new ModuleItem();
        $manager->persist($moduleItem);
        $moduleItem->setModule($contentModule);
        $moduleItem->setPage($page);
        $moduleItem->setData($this->createData($contentData));
    }

	protected function loadContentForOfficesList()
	{
		/** @var ModuleItem $contentItem */
		$contentItem = $this->moduleItemFactory->create(
			[
				'name' => ModuleItems::CONTENT,
				'description' => ModuleItems::CONTENT . ' [Offices list]',
			]
		);

		$contentData = new Content();
		$contentData->content = file_get_contents(__DIR__ . '/page_data/offices_list.html');

		$contentItem->setData($this->createData($contentData));
	}

    protected function loadContentForCreditApplicationPage()
    {
        /** @var ModuleItem $contentItem */
        $contentItem = $this->moduleItemFactory->create(
            [
                'name' => ModuleItems::CONTENT,
                'description' => ModuleItems::CONTENT . ' [Credit application page]',
            ]
        );

        $contentData = new Content();

        $contentItem->setData($this->createData($contentData));
    }

    protected function createData($parametersObject)
    {
        $result = [];

        foreach ($this->locales as $locale) {
            $result[$locale] = clone $parametersObject;
        }

        return serialize($result);
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['module_items'];
    }
}
