<p>
    Чи траплялося з вами таке, що наприкінці місяця вам не вистачало грошей? Скажете: “З ким не бувало!”
</p>
<p>
    А чи пробували ви вести сімейний бюджет?
</p>
<p>
    Ми, представники Компанії iCredit, віримо, що контроль витрат за допомогою сімейного бюджету — дуже
    дуже простий вихід з ситуації. Ось декілька правил, котрі допоможуть вам в цьому.
</p>
<p>
    В першу чергу вкажіть місячні доходи усіх членів сім”ї (зарплати, додаткові доходи, оренда та ін.)
</p>
<p>
    Після цього опишіть найважливіші витрати за місяць, яких неможливо уникнути, наприклад:
</p>
<p>
    -   оренда, вода, електроенергія, опалення — сума майже завжди одна й та сама;
    <br>
    -   телефони, кабельне телебачення, інтернет — ці витрати також суттєво не змінюютьсяпротягом місяця;
    <br>
    -   продукти харчування — запишіть середню суму витрат на харчі для всієї сім”ї;
    <br>
    -   транспорт — громадський та особистий. Якщо є особистий транспорт, добре, якби ви завжди враховували кошти на ремонт;
    <br>
    -   діти — опишіть витрати на проїзд, харчування, кишенькові витрати, навчання та ін.
</p>
<p>
    Далі ряд витрат, що не є життєво-важливими, але нам доводиться їх враховувати:
</p>
<p>
    -   ремонт та підтримка житла — заздалегідь передбачений ремонт, зміна меблів;
    <br>
    -   інші — витрати на речі, взуття, догляд за собою, побутову хімію, розваги, ліки та ін.
    <br>
    І що виходить після підведення риски? Можете підрахувати баланс вашого сімейного бюджету і таким чином,
    контролювати свої витрати так, щоб вам спокійно вистачало грошей від зарплатні до зарплатні.
</p>
<p>
    Наша вам порада: якщо ви раптом вирішите взяти швидкий кредит, то вам необхідно оцінити розмір внеску, котрий ви зможете собі дозволити.
</p>
<p>
    Подивіться на свій сімейний бюджет і якщо остаточна сума (Доходи-Витрати) більше внеску по кредиту,
    тоді у вас не буде проблем з його виплатою. Приблизний сімейний бюджет можна побачити нижче.
</p>

<table border="1">
    <colgroup><col width="246"></colgroup>
    <colgroup><col width="64"></colgroup>
    <tbody>
        <tr>
            <td colspan="2">
                <p align="center"><strong>Дохід:</strong></p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Зарплата</p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <p>Інші надходження</p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <p align="right"><strong>ВСЬОГО:</strong></p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p align="center"><strong>Витрати:</strong></p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Оплата комунальних послуг</p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <p>Телефон, телебачення, інтернет</p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <p>Продукти харчування</p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <p>Транспорт</p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <p>Витрати на дітей</p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <p>Ремонт та підтримка житла</p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <p>Інші витрати</p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <p align="right"><strong>ВСЬОГО:</strong></p>
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <p align="center"><strong>Залишок суми</strong></p>
                <p align="center"><strong>(дохід - витрати)</strong></p>
            </td>
            <td>

            </td>
        </tr>
    </tbody>
</table>