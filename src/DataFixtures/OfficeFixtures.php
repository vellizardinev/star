<?php

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Office;
use App\Factory\CityFactory;
use App\Factory\OfficeFactory;
use App\Repository\CityRepository;
use App\Service\EntityTranslator;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class OfficeFixtures extends BaseFixtures implements FixtureGroupInterface
{
    private $officeFactory;
    /**
     * @var CityFactory
     */
    private $cityFactory;
    /**
     * @var CityRepository
     */
    private $cityRepository;
	/**
	 * @var EntityTranslator
	 */
	private $entityTranslator;

    /**
     * OfficeFixtures constructor.
     * @param OfficeFactory $officeFactory
     * @param CityFactory $cityFactory
     * @param CityRepository $cityRepository
     * @param EntityTranslator $entityTranslator
     */
    public function __construct(OfficeFactory $officeFactory, CityFactory $cityFactory, CityRepository $cityRepository, EntityTranslator $entityTranslator)
    {
        $this->officeFactory = $officeFactory;
        $this->cityFactory = $cityFactory;
        $this->cityRepository = $cityRepository;
        $this->entityTranslator = $entityTranslator;
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->loadCitiesFromList($this->loadCitiesFromCSV());

        $this->loadICreditOfficesFromCSV();

        $this->loadCentralOffice($manager);
    }

    protected function loadICreditOfficesFromCSV()
    {
	    $handle = fopen(__DIR__ . '/offices/i_credit.csv', "r");

	    while (($data = fgetcsv($handle, 0, ';')) !== false) {
		    if ($data[0] === 'Name') {
			    continue;
		    }

		    $office = $this->officeFactory->create(
			    [
				    'name' => trim($data[0]),
				    'city' => $this->cityRepository->findOneBy(['name' => trim($data[1])]),
				    'address' => trim($data[3]),
				    'telephone' => trim($data[2]),
				    'latitude' => doubleval(explode(',', $data[4])[0]),
				    'longitude' => doubleval(explode(',', $data[4])[1]),
			    ]
		    );

		    $this->entityTranslator->translateOffice($office);

	    }

	    fclose($handle);
    }

    protected function loadCitiesFromCSV(): array
    {
        $cities = [];

        $handle = fopen(__DIR__ . '/offices/i_credit.csv', "r");

        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            if ($data[0] === 'Name') {
                continue;
            }

            $cities[] = trim($data[1]);
        }

        fclose($handle);

        $cities = array_unique($cities);

        return $cities;
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['offices'];
    }

    protected function loadCitiesFromList(array $cities = [])
    {
        $defaultNames = ['Коростень', 'Стрий', 'Харків'];

        $fullList = array_merge($cities, $defaultNames);
        $fullList = array_unique($fullList);

        foreach ($fullList as $city) {
            $this->cityFactory->create(['name' => $city]);
        }
    }

    protected function loadCentralOffice(ObjectManager $manager)
    {
        $central = (new Office)
            ->setName('Central Office')
            ->setShowOnMap(false)
            ->setCity($manager->getRepository(City::class)->findOneBy(['name' => 'Київ']))
            ->setLatitude('50.4985282')
            ->setLongitude('30.520622')
            ->setAddress('м. Київ, Оболонський район, вул. Оболонська набережна 15, корпус 4')
	        ->setTelephone('067 322 22 66')
            ->setWorkingTime('пн-пт: з 9:30 до 18:30');

        $manager->persist($central);
        $manager->flush();
    }
}
