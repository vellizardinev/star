<?php

namespace App\DataFixtures;

use App\Factory\GiftFactory;
use App\Service\EntityTranslator;
use Doctrine\Persistence\ObjectManager;

class GiftFixtures extends BaseFixtures
{
    /**
     * @var GiftFactory
     */
    private $giftFactory;

    /**
     * @var int
     */
    private $loadSeedData;

	/**
	 * @var EntityTranslator
	 */
	private $entityTranslator;

    /**
     * @param GiftFactory $giftFactory
     * @param int $loadSeedData
     * @param EntityTranslator $entityTranslator
     */
    public function __construct(GiftFactory $giftFactory, int $loadSeedData, EntityTranslator $entityTranslator)
    {
        $this->giftFactory = $giftFactory;
        $this->loadSeedData = $loadSeedData;
        $this->entityTranslator = $entityTranslator;
    }

    protected function loadData(ObjectManager $manager)
    {
        if ($this->loadSeedData === 1) {
            $gifts = $this->giftFactory->create([], 30);

            foreach ($gifts as $gift){
	            $this->entityTranslator->translateGift($gift);
            }
        }
    }
}
