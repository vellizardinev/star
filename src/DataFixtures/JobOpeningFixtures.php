<?php

namespace App\DataFixtures;

use App\Factory\JobOpeningFactory;
use App\Service\EntityTranslator;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class JobOpeningFixtures extends BaseFixtures implements DependentFixtureInterface
{
    private $factory;

    /**
     * @var int
     */
    private $loadSeedData;

	/**
	 * @var EntityTranslator
	 */
	private $entityTranslator;

    /**
     * DepartmentFixtures constructor.
     * @param JobOpeningFactory $factory
     * @param int $loadSeedData
     * @param EntityTranslator $entityTranslator
     */
    public function __construct(JobOpeningFactory $factory, int $loadSeedData, EntityTranslator $entityTranslator)
    {
        $this->factory = $factory;
        $this->loadSeedData = $loadSeedData;
        $this->entityTranslator = $entityTranslator;
    }

    protected function loadData(ObjectManager $manager)
    {
        if ($this->loadSeedData === 1) {
            $jobOpenings = $this->factory->create([], 23);

	        foreach ($jobOpenings as $jobOpening){
		        $this->entityTranslator->translateJobOpening($jobOpening);
	        }
        }
    }

    public function getDependencies()
    {
        return array(
            OfficeFixtures::class,
            DepartmentFixtures::class,
            OfficeFixtures::class,
        );
    }
}
