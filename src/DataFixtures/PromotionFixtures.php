<?php

namespace App\DataFixtures;

use App\Factory\SiriusPromotionFactory;
use Doctrine\Persistence\ObjectManager;

class PromotionFixtures extends BaseFixtures
{
    /**
     * @var SiriusPromotionFactory
     */
    private $promotionFactory;

    private $loadSeedData;

    /**
     * @param SiriusPromotionFactory $promotionFactory
     * @param integer $loadSeedData
     */
    public function __construct(SiriusPromotionFactory $promotionFactory, int $loadSeedData)
    {
        $this->promotionFactory = $promotionFactory;
        $this->loadSeedData = $loadSeedData;
    }

    protected function loadData(ObjectManager $manager)
    {
        if ($this->loadSeedData === 1) {
            $this->promotionFactory->create([], 10);
        }
    }
}
