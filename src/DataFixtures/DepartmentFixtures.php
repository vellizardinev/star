<?php

namespace App\DataFixtures;

use App\Factory\SiriusDepartmentFactory;
use App\Service\EntityTranslator;
use Doctrine\Persistence\ObjectManager;

class DepartmentFixtures extends BaseFixtures
{
    private $factory;

    /**
     * @var int
     */
    private $loadSeedData;

	/**
	 * @var EntityTranslator
	 */
	private $entityTranslator;

    /**
     * DepartmentFixtures constructor.
     * @param SiriusDepartmentFactory $factory
     * @param int $loadSeedData
     * @param EntityTranslator $entityTranslator
     */
    public function __construct(SiriusDepartmentFactory $factory, int $loadSeedData, EntityTranslator $entityTranslator)
    {
        $this->factory = $factory;
        $this->loadSeedData = $loadSeedData;
        $this->entityTranslator = $entityTranslator;
    }

    protected function loadData(ObjectManager $manager)
    {
        if ($this->loadSeedData === 1) {
            $departments = $this->factory->create([], 5);

	        foreach ($departments as $department){
		        $this->entityTranslator->translateDepartment($department);
	        }
        }
    }
}
