<?php

namespace App\DataFixtures;

use App\Enum\NewsCategories;
use App\Factory\NewsFactory;
use App\Factory\RedirectRuleFactory;
use App\Service\EntityTranslator;
use App\Service\FileUrlMigration;
use Carbon\Carbon;
use DateTimeZone;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Finder;

class NewsFixtures extends BaseFixtures implements FixtureGroupInterface, DependentFixtureInterface
{
    /**
     * @var NewsFactory
     */
    private $newsFactory;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var RedirectRuleFactory
     */
    private $redirectRuleFactory;

    /**
     * @var int
     */
    private $loadSeedData;
    /**
     * @var FileUrlMigration
     */
    private $fileUrlMigration;
	/**
	 * @var int
	 */
	private $loadTestNews;
    /**
     * @var EntityTranslator
     */
    private $entityTranslator;

    /**
     * @param NewsFactory $newsFactory
     * @param EntityManagerInterface $entityManager
     * @param RedirectRuleFactory $redirectRuleFactory
     * @param int $loadSeedData
     * @param int $loadTestNews
     * @param FileUrlMigration $fileUrlMigration
     * @param EntityTranslator $entityTranslator
     */
    public function __construct(
        NewsFactory $newsFactory,
        EntityManagerInterface $entityManager,
        RedirectRuleFactory $redirectRuleFactory,
        int $loadSeedData,
        int $loadTestNews,
        FileUrlMigration $fileUrlMigration,
        EntityTranslator $entityTranslator
    ) {
        $this->newsFactory = $newsFactory;
        $this->entityManager = $entityManager;
        $this->redirectRuleFactory = $redirectRuleFactory;
        $this->loadSeedData = $loadSeedData;
        $this->loadTestNews = $loadTestNews;
        $this->fileUrlMigration = $fileUrlMigration;
        $this->entityTranslator = $entityTranslator;
    }

    /**
     * @param ObjectManager $manager
     * @throws DBALException
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->loadFromCsv();

//        if ($this->loadTestNews === 1) {
//            $this->loadFromCsv();
//        } else{
//	        $this->loadFromDump();
//        }
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['news'];
    }

    private function loadFromCsv()
    {
        $handle = fopen(__DIR__ . '/news/news_data.csv', "r");

        while (($data = fgetcsv($handle, 0, ';')) !== false) {

            if ($data[0] === 'id') {
                continue;
            }

            $startDate = trim($data[6]) === 'NULL' ? null : Carbon::parse(
                trim($data[6]),
                new DateTimeZone('Europe/Kiev')
            );

            $endDate = trim($data[7]) === 'NULL' ? null : Carbon::parse(
                trim($data[7]),
                new DateTimeZone('Europe/Kiev')
            );

	        $createdDate = trim($data[11]) === 'NULL' ? Carbon::now() : Carbon::parse(
		        trim($data[11]),
		        new DateTimeZone('Europe/Kiev')
	        );

	        $updatedDate = trim($data[13]) === 'NULL' ? Carbon::now() : Carbon::parse(
		        trim($data[13]),
		        new DateTimeZone('Europe/Kiev')
	        );

	        $newsContent = trim($data[2]);

	        $this->fileUrlMigration->loadFileDataFromCSV(__DIR__ . '/document_links/document_links.csv');

	        $newsContent = $this->fileUrlMigration->updateUrls($newsContent);

            $news = $this->newsFactory->make(
                [
                    'title' => trim($data[1]),
                    'introduction' => trim($data[4]),
                    'content' => $newsContent,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'createdAt' => $createdDate,
                    'updatedAt' => $updatedDate,
                    'category' => NewsCategories::NEWS,
                    'metaKeywords' => mb_substr(trim($data[5]), 0, 190),
                ]
            );

            $this->manager->persist($news);
            $this->manager->flush();

            $this->entityTranslator->translateNews($news);

            $this->redirectRuleFactory->create(
                [
                    'oldUrl' => "News/News/{$data[0]}",
                    'newUrl' => "/novyny/{$news->getSlug()}",
                ]
            );
        }

        fclose($handle);
    }

    private function getContent(string $id)
    {
        $path = __DIR__ . "/news/content/$id/ContentHTML.txt";

        $content = file_get_contents($path);

        return $content;
    }

    /**
     * @throws DBALException
     */
    protected function loadFromDump()
    {
        $finder = new Finder();
        $finder->in(__DIR__ . '/news');
        $finder->name('news_data.sql');

        foreach ($finder as $file) {
            $content = $file->getContents();

            $stmt = $this->entityManager->getConnection()->prepare($content);
            $stmt->execute();
        }
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [FileFixtures::class];
    }
}
