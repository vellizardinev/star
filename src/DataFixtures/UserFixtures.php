<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Enum\Roles;
use App\Factory\SiriusUserFactory;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixtures
{
    private $passwordEncoder;

    private $userFactory;

    private $adminPassword;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, SiriusUserFactory $userFactory, string $adminPassword)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->userFactory = $userFactory;
        $this->adminPassword = $adminPassword;
    }

    protected function loadData(ObjectManager $manager)
    {
        /** @var User $admin */
        $admin = $this->userFactory->create([
            'email' => 'admin@siriussoftware.bg',
            'password' => $this->passwordEncoder->encodePassword(new User(), $this->adminPassword),
        ]);

        $admin->setRoles([Roles::ADMINISTRATOR]);

        /** @var User $testUser */
        $testUser = $this->userFactory->create([
            'email' => 'test@siriussoftware.bg',
        ]);

        /** @var User $marketingUser */
        $marketingUser = $this->userFactory->create([
            'email' => 'editor-marketing-ua@easy-asset-management.bg',
        ]);
        $marketingUser->setRoles([Roles::ROLE_DASHBOARD, Roles::ROLE_PAGES, Roles::ROLE_MODULES, Roles::ROLE_MANAGED_MODULES]);

	    $creditApplicationUser = $this->userFactory->create([
		    'email' => 'credit-application-operator@easy-asset-management.bg',
	    ]);
	    $creditApplicationUser->setRoles([Roles::ROLE_TELEPHONE_APPLICATIONS, Roles::ROLE_ONLINE_APPLICATIONS]);

        $manager->persist($admin);
        $manager->persist($testUser);
        $manager->persist($marketingUser);
        $manager->persist($creditApplicationUser);
        $manager->flush();
    }
}
