<?php

namespace App\DataFixtures;

use App\Entity\Permission\Group;
use App\Entity\User;
use App\Enum\PermissionGroups;
use App\Factory\PermissionFactory;
use App\Factory\PermissionGroupFactory;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PermissionsFixtures extends BaseFixtures implements DependentFixtureInterface
{
    /**
     * @var PermissionGroupFactory
     */
    private $permissionGroupFactory;
    /**
     * @var PermissionFactory
     */
    private $permissionFactory;

    /**
     * PermissionsFixtures constructor.
     * @param PermissionGroupFactory $permissionGroupFactory
     * @param PermissionFactory $permissionFactory
     */
    public function __construct(PermissionGroupFactory $permissionGroupFactory, PermissionFactory $permissionFactory)
    {
        $this->permissionGroupFactory = $permissionGroupFactory;
        $this->permissionFactory = $permissionFactory;
    }

    protected function loadData(ObjectManager $manager)
    {
        /** @var Group $permissionsGroup */
        $permissionsGroup = $this->permissionGroupFactory->create(['name' => PermissionGroups::PERMISSION_ADMIN]);

        $permissionCreate = $this->permissionFactory->create(['domain' => 'permissions', 'action' => 'create']);
        $permissionEdit = $this->permissionFactory->create(['domain' => 'permissions', 'action' => 'edit']);
        $permissionView = $this->permissionFactory->create(['domain' => 'permissions', 'action' => 'view']);
        $permissionDelete = $this->permissionFactory->create(['domain' => 'permissions', 'action' => 'delete']);

        $permissionsGroup->addPermission($permissionCreate);
        $permissionsGroup->addPermission($permissionEdit);
        $permissionsGroup->addPermission($permissionView);
        $permissionsGroup->addPermission($permissionDelete);

        $user = $this->manager->getRepository(User::class)->findOneBy(['email' => 'admin@siriussoftware.bg']);

        $permissionsGroup->addUser($user);
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [UserFixtures::class];
    }
}
