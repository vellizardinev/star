<?php

namespace App\DataFixtures;

use App\Factory\PollFactory;
use Doctrine\Persistence\ObjectManager;

class PollFixtures extends BaseFixtures
{
    /**
     * @var PollFactory
     */
    private $factory;

    /**
     * @var int
     */
    private $loadSeedData;

    /**
     * @param PollFactory $factory
     * @param int $loadSeedData
     */
    public function __construct(PollFactory $factory, int $loadSeedData)
    {
        $this->factory = $factory;
        $this->loadSeedData = $loadSeedData;
    }

    protected function loadData(ObjectManager $manager)
    {
        if ($this->loadSeedData === 1) {
            $this->factory->create([], 5);
        }
    }
}
