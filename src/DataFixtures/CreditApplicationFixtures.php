<?php

namespace App\DataFixtures;

use App\Entity\CreditShortApplication;
use App\Entity\LandingPage;
use App\Factory\AffiliateFactory;
use App\Factory\CreditApplicationFactory;
use Carbon\Carbon;
use DateTimeZone;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CreditApplicationFixtures extends BaseFixtures implements DependentFixtureInterface
{
    private $factory;

    private $loadSeedData;

    private $affiliateFactory;

    public function __construct(CreditApplicationFactory $factory, int $loadSeedData, AffiliateFactory $affiliateFactory)
    {
        $this->factory = $factory;
        $this->loadSeedData = $loadSeedData;
        $this->affiliateFactory = $affiliateFactory;
    }

    protected function loadData(ObjectManager $manager)
    {
        if ($this->loadSeedData !== 1) {
            return;
        }

        $affiliate = $this->affiliateFactory->create(['name' => 'Test partner']);

        $handle = fopen(__DIR__ . '/credit_application_data/credit_application_data.csv', "r");

        while (($data = fgetcsv($handle, 0, ';')) !== false) {

            if ($data[0] === 'phone') {
                continue;
            }

            $preferredCallDate = trim($data[12]) === 'NULL' ? Carbon::now() : Carbon::parse(
                trim($data[12]),
                new DateTimeZone('Europe/Sofia')
            );

            $birthdayDate = trim($data[6]) === 'NULL' ? Carbon::now() : Carbon::parse(
                trim($data[6]),
                new DateTimeZone('Europe/Sofia')
            );

            $createdDate = trim($data[11]) === 'NULL' ? Carbon::now() : Carbon::parse(
                trim($data[11]),
                new DateTimeZone('Europe/Sofia')
            );

            $landing = $manager->getRepository(LandingPage::class)->findOneBy([]);

            /** @var CreditShortApplication $creditApplication */
            $creditApplication = $this->factory->make(
                [
                    'name' => $data[3],
                    'email' => $data[10],
                    'city' => $data[8],
                    'telephone' => $data[0],
                    'amount' => $data[9],
                    'level' => $data[2],
                    'landing' => $data[0] == '380883033328' ? $landing : null,
                    'affiliate' => $data[0] == '380897337007' ? $affiliate : null,
                    'birthdayDate' => $birthdayDate,
                    'personalIdentificationNumber' => $data[7],
                    'preferredCallDate' => $preferredCallDate,
                ]
            );

            $this->manager->persist($creditApplication);
            $this->manager->flush();
            $creditApplication->setCreatedAt($createdDate);
        }

        $manager->flush();

        fclose($handle);
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return [CreditFixtures::class, OfficeFixtures::class, PageFixtures::class];
    }
}
