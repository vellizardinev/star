<?php

namespace App\DataFixtures;

use App\Entity\Page;
use App\Enum\Pages;
use App\Enum\Settings;
use App\Factory\SettingFactory;
use App\Service\UserDefinedPageRouting;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SettingsFixtures extends BaseFixtures implements DependentFixtureInterface
{
    /**
     * @var SettingFactory
     */
    private $settingFactory;
	/**
	 * @var UserDefinedPageRouting
	 */
	private $userDefinedPageRouting;
    /**
     * @var string|null
     */
    private $captchaKey;
    /**
     * @var string|null
     */
    private $captchaSecret;

    /**
     * @param SettingFactory $settingFactory
     * @param UserDefinedPageRouting $userDefinedPageRouting
     * @param string|null $captchaKey
     * @param string|null $captchaSecret
     */
    public function __construct(SettingFactory $settingFactory, UserDefinedPageRouting $userDefinedPageRouting, ?string $captchaKey, ?string $captchaSecret)
    {
        $this->settingFactory = $settingFactory;
        $this->userDefinedPageRouting = $userDefinedPageRouting;
        $this->captchaKey = $captchaKey;
        $this->captchaSecret = $captchaSecret;
    }

	public function getDependencies()
	{
		return array(
			PageFixtures::class
		);
	}

    protected function loadData(ObjectManager $manager)
    {
        $this->settingFactory->create([
            'name' => Settings::PAGINATION_PER_PAGE,
            'uiLabel' => 'Pagination: Items per page',
            'value' => 10,
        ]);

        $this->settingFactory->create([
            'name' => Settings::DATE_FORMAT,
            'uiLabel' => 'Date / Time: Date format',
            'value' => 'Y-m-d',
        ]);

        $this->settingFactory->create([
            'name' => Settings::DATE_TIME_FORMAT,
            'uiLabel' => 'Date / Time: Date / Time format',
            'value' => 'Y-m-d H:i',
        ]);

        $this->settingFactory->create([
            'name' => Settings::MAX_UPLOAD_SIZE,
            'uiLabel' => 'Files: Max file upload size',
            'value' => '20M',
        ]);

	    $this->settingFactory->create([
		    'name' => Settings::IMAGE_MAX_UPLOAD_SIZE,
		    'uiLabel' => 'Images: Max file upload size',
		    'value' => '5M',
	    ]);

        $this->settingFactory->create([
            'name' => Settings::ALLOWED_MIMES,
            'uiLabel' => 'Files: Allowed file types',
            'value' => 'image/jpeg, image/png',
        ]);

        $this->settingFactory->create([
            'name' => Settings::DEFAULT_FROM_EMAIL,
            'uiLabel' => 'Default email to be used in the "from" field',
            'value' => 'office@icredit.ua',
        ]);

        $this->settingFactory->create([
            'name' => Settings::SEND_CONTACT_MESSAGES_TO,
            'uiLabel' => 'Email address to send contact messages to',
            'value' => 'office@icredit.ua',
        ]);

	    $this->settingFactory->create([
		    'name' => Settings::SEND_JOB_APPLICATIONS_TO,
		    'uiLabel' => 'Email address to send job applications to',
		    'value' => 'hr@icredit.ua',
	    ]);

	    $this->settingFactory->create([
		    'name' => Settings::SEND_CREDIT_APPLICATIONS_TO,
		    'uiLabel' => 'Email address to send credit applications to',
		    'value' => 'office@icredit.ua',
	    ]);

        $this->settingFactory->create([
            'name' => Settings::SEND_POLL_ENTRY_TO,
            'uiLabel' => 'Email address to send poll entries to',
            'value' => 'marketing@icredit.ua',
        ]);

	    $this->settingFactory->create([
		    'name' => Settings::ZENDESK_API_KEY,
		    'uiLabel' => 'Zendesk API key',
		    'value' => '1c24dc9a-41de-4b30-a046-04e5478c834c',
	    ]);

	    $this->settingFactory->create([
		    'name' => Settings::FACEBOOK_LINK,
		    'uiLabel' => 'Facebook link',
		    'value' => ' https://www.facebook.com/icredit.ua/',
	    ]);

	    $this->settingFactory->create([
		    'name' => Settings::GOOGLE_RECAPTCHA_API_KEY,
		    'uiLabel' => 'Google recaptcha API key',
		    'value' => !empty($this->captchaKey) ? $this->captchaKey : '6Letz8UUAAAAAFUj7cKZ0JaFz-cXSQQn88E3rqyl',
	    ]);

	    $this->settingFactory->create([
		    'name' => Settings::GOOGLE_RECAPTCHA_SECRET,
		    'uiLabel' => 'Google recaptcha secret',
		    'value' => !empty($this->captchaSecret) ? $this->captchaSecret : '6Letz8UUAAAAAGNRQoBnQ-pWB7vgJTbe4O93o_3M',
	    ]);

        $this->settingFactory->create([
            'name' => Settings::LONG_APPLICATIONS_EMAIL,
            'uiLabel' => 'Email to send long credit applications to',
            'value' => 'online@icredit.ua',
        ]);

        $this->settingFactory->create([
            'name' => Settings::FAILED_LONG_APPLICATIONS_EMAIL,
            'uiLabel' => 'Email for sending online credit applications that failed to be sent to SmartIT API.',
            'value' => 'cd@icredit.ua',
        ]);

	   $this->loadThankYouPageSettings();
    }

    private function loadThankYouPageSettings()
    {
    	$creditThankYouPage = $this->manager->getRepository(Page::class)->findOneBy(['name' => Pages::CREDIT_THANK_YOU]);
	    $this->settingFactory->create([
		    'name' => Settings::CREDIT_APPLICATION_REDIRECT_PATH,
		    'uiLabel' => 'Redirect path after credit application',
		    'value' => $this->userDefinedPageRouting->generateUrl($creditThankYouPage),
	    ]);

        $onlineCreditThankYouPage = $this->manager->getRepository(Page::class)->findOneBy(['name' => Pages::CREDIT_LONG_THANK_YOU]);
        $this->settingFactory->create([
            'name' => Settings::ONLINE_CREDIT_APPLICATION_REDIRECT_PATH,
            'uiLabel' => 'Redirect path after online credit application',
            'value' => $this->userDefinedPageRouting->generateUrl($onlineCreditThankYouPage),
        ]);

	    $jobThankYouPage = $this->manager->getRepository(Page::class)->findOneBy(['name' => Pages::JOB_THANK_YOU]);
	    $this->settingFactory->create([
		    'name' => Settings::JOB_APPLICATION_REDIRECT_PATH,
		    'uiLabel' => 'Redirect path after job application',
		    'value' =>$this->userDefinedPageRouting->generateUrl($jobThankYouPage),
	    ]);

	    $contactThankYouPage = $this->manager->getRepository(Page::class)->findOneBy(['name' => Pages::CONTACT_THANK_YOU]);
	    $this->settingFactory->create([
		    'name' => Settings::CONTACT_APPLICATION_REDIRECT_PATH,
		    'uiLabel' => 'Redirect path after contact application',
		    'value' => $this->userDefinedPageRouting->generateUrl($contactThankYouPage),
	    ]);
    }
}
