<?php

namespace App\DataFixtures;

use App\Entity\Module;
use App\Enum\MapTypes;
use App\Factory\ModuleFactory;
use App\Factory\NewsFactory;
use App\Module\Parameter\BecomeCreditConsultant;
use App\Module\Parameter\Content;
use App\Module\Parameter\CreditApplicationForm;
use App\Module\Parameter\ExpandableContent;
use App\Module\Parameter\FooterLandingPage;
use App\Module\Parameter\HeaderLandingPage;
use App\Module\Parameter\ItemsCount;
use App\Module\Parameter\Link;
use App\Module\Parameter\OfficeMap;
use App\Module\Parameter\Slider;
use App\Module\Parameter\WhyYouShouldTrustUs;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use ReflectionException;

class ModuleFixtures extends BaseFixtures implements FixtureGroupInterface
{
    /**
     * @var NewsFactory
     */
    private $moduleFactory;
    /**
     * @var string
     */
    private $locales;

    /**
     * @param ModuleFactory $moduleFactory
     * @param string $locales
     */
    public function __construct(ModuleFactory $moduleFactory, string $locales)
    {
        $this->moduleFactory = $moduleFactory;
        $this->locales = explode('|', $locales);
    }

    /**
     * @param ObjectManager $manager
     * @throws ReflectionException
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->loadLatestPromotions();
        $this->loadContentModule();
        $this->loadOfficeMap();
        $this->loadExpandableContent();
        $this->loadSlider();
        $this->loadBecomeCreditConsultant();
        $this->loadWhyShouldYouTrustUs();
        $this->loadLatestPromotionsSlider();
        $this->loadLatestNews();
        $this->loadHeaderLandingPage();
        $this->loadFooterLandingPage();
        $this->loadCreditApplicationForm();

        $manager->flush();
    }

    protected function loadLatestNews()
    {
        /** @var Module $latestNewsModule */
        $latestNewsModule = $this->moduleFactory->create(['name' => 'Latest news']);
        $latestNewsModule->setData($this->createData(new ItemsCount()));
    }

    protected function loadHeaderLandingPage()
    {
        /** @var Module $headerLandingPage */
        $headerLandingPage = $this->moduleFactory->create(['name' => 'Header landing page']);
        $headerData = new HeaderLandingPage();

        $headerData->addLink(new Link('Link 1', '#'));
        $headerData->addLink(new Link('Link 2', '#'));
        $headerData->addLink(new Link('Link 3', '#'));

        $headerLandingPage->setData($this->createData($headerData));
    }

    protected function loadFooterLandingPage()
    {
        /** @var Module $footerLandingPage */
        $footerLandingPage = $this->moduleFactory->create(['name' => 'Footer landing page']);

        $text = <<<TXT
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio pellentesque diam volutpat commodo sed egestas. Risus viverra adipiscing at in tellus integer. Nisl condimentum id venenatis a condimentum vitae sapien pellentesque habitant. Turpis cursus in hac habitasse platea dictumst quisque. Dui faucibus in ornare quam viverra orci. Nisl nisi scelerisque eu ultrices vitae auctor eu. Pellentesque pulvinar pellentesque habitant morbi. Lacus sed viverra tellus in. Sit amet est placerat in. Purus semper eget duis at tellus at urna condimentum. Et netus et malesuada fames ac turpis egestas integer eget. Aliquam id diam maecenas ultricies mi eget mauris pharetra. Sapien pellentesque habitant morbi tristique senectus et netus et.
TXT;

        $footerData = new FooterLandingPage($text);

        $footerLandingPage->setData($this->createData($footerData));
    }

    protected function loadCreditApplicationForm()
    {
        /** @var Module $creditApplicationFrom */
        $creditApplicationFrom = $this->moduleFactory->create(['name' => 'Credit application form']);
        $data = new CreditApplicationForm();

        $creditApplicationFrom->setData($this->createData($data));
    }

    protected function loadLatestPromotions()
    {
        /** @var Module $latestPromotionsModule */
        $latestPromotionsModule = $this->moduleFactory->create(['name' => 'Latest promotions']);
        $latestPromotionsModule->setData($this->createData(new ItemsCount()));
    }

    protected function loadContentModule()
    {
        $contentModule = $this->moduleFactory->create(['name' => 'Content']);
        $contentModule->setData($this->createData(new Content()));
    }

    /**
     * @throws ReflectionException
     */
    protected function loadOfficeMap()
    {
        /** @var Module $officeMapModule */
        $officeMapModule = $this->moduleFactory->create(['name' => 'Office map']);
        $data = new OfficeMap();
        $data->setType(MapTypes::getValue('ROADMAP'));
        $officeMapModule->setData($this->createData($data));
    }

    protected function loadExpandableContent()
    {
        $expandableContentModule = $this->moduleFactory->create(['name' => 'Expandable content']);
        $expandableContentModule->setData($this->createData(new ExpandableContent()));
    }

    protected function loadSlider()
    {
        /** @var Module $sliderModule */
        $sliderModule = $this->moduleFactory->create(['name' => 'Slider']);
        $sliderModule->setData($this->createData(new Slider()));
    }

    protected function loadBecomeCreditConsultant()
    {
        /** @var Module $becomeCreditConsultantModule */
        $becomeCreditConsultantModule = $this->moduleFactory->create(['name' => 'Become credit consultant']);
        $becomeCreditConsultantModule->setData($this->createData(new BecomeCreditConsultant()));
    }

    protected function loadWhyShouldYouTrustUs()
    {
        /** @var Module $whyYouShouldTrustUsModule */
        $whyYouShouldTrustUsModule = $this->moduleFactory->create(['name' => 'Why you should trust us']);
        $whyYouShouldTrustUsModule->setData($this->createData(new WhyYouShouldTrustUs()));
    }

    protected function loadLatestPromotionsSlider()
    {
        /** @var Module $latestPromotionsSliderModule */
        $latestPromotionsSliderModule = $this->moduleFactory->create(['name' => 'Latest promotions slider']);
        $latestPromotionsSliderModule->setData($this->createData(new ItemsCount()));
    }

    protected function createData($parametersObject)
    {
        $result = [];

        foreach ($this->locales as $locale) {
            $result[$locale] = clone $parametersObject;
        }

        return serialize($result);
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['module_items'];
    }
}
