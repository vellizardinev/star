<?php

namespace App\DataFixtures;

use App\Factory\RedirectRuleFactory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

class RedirectFixtures extends BaseFixtures
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var RedirectRuleFactory
     */
    private $redirectRuleFactory;
    /**
     * @var string
     */
    private $siteBaseUrl;

    /**
     * @param EntityManagerInterface $entityManager
     * @param RedirectRuleFactory $redirectRuleFactory
     * @param string $siteBaseUrl
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        RedirectRuleFactory $redirectRuleFactory,
        string $siteBaseUrl
    ) {
        $this->entityManager = $entityManager;
        $this->redirectRuleFactory = $redirectRuleFactory;
        $this->siteBaseUrl = $siteBaseUrl;
    }

    /**
     * @param ObjectManager $manager
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->loadFromCsv();
    }

    private function loadFromCsv()
    {
        $handle = fopen(__DIR__ . '/redirects/redirects.csv', "r");

        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            if ($data[0] === 'Link') {
                continue;
            }

            $host1 = 'http://icredit.ua';
            $host2 = 'https://icredit.ua';
            $host3 = 'https://www.icredit.ua';
            $host4 = 'http://www.icredit.ua';

            $old = str_replace($host1, '', $data[0]);
            $old = str_replace($host2, '', $old);
            $old = str_replace($host3, '', $old);
            $old = str_replace($host4, '', $old);
            $old = ltrim($old, '/');

            $new = str_replace($host1, '', $data[1]);
            $new = str_replace($host2, '', $new);
            $new = str_replace($host3, '', $new);
            $new = str_replace($host4, '', $new);
//            $new = ltrim($new, '/');
//            $new = $this->siteBaseUrl . (empty($new) ? '' : '/' . $new);

            $this->redirectRuleFactory->create(
                [
                    'oldUrl' => $old,
                    'newUrl' => $new,
                ]
            );
        }

        fclose($handle);
    }
}
