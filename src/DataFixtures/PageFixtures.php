<?php

namespace App\DataFixtures;

use App\Entity\LandingPage;
use App\Entity\Page;
use App\Enum\Pages;
use App\LandingPage\LandingPageManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class PageFixtures extends BaseFixtures implements FixtureGroupInterface
{
    private $landingPageManager;

    public function __construct(LandingPageManager $landingPageManager)
    {
        $this->landingPageManager = $landingPageManager;
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->loadICreditAsEmployer($manager);
        $this->loadMotivationProgram($manager);
        $this->loadFinancialCulture($manager);
        $this->loadFinancialDictionary($manager);
        $this->loadTermsAndConditionsPage($manager);
	    $this->loadThankYouPages($manager);
	    $this->loadMoneyShopPage($manager);
	    $this->loadVeroCashPage($manager);
	    $this->loadCustomerFeedbacksPage($manager);
	    $this->loadFamilyFinancesPage($manager);

	    $this->createLandingPage($manager, 'Test landing page', 'test-landing-page');

		$manager->flush();
    }

    private function loadTermsAndConditionsPage(ObjectManager $manager)
    {
        $this->createPage($manager, Pages::TERMS_AND_CONDITIONS, 'yurydychna-informatsiia', true);
    }

	private function loadThankYouPages(ObjectManager $manager)
	{
        $this->createPage($manager, Pages::CREDIT_THANK_YOU, 'kredyt-dyakuyu');

        $this->createPage($manager, Pages::JOB_THANK_YOU, 'robotu-dyakuyu');

        $this->createPage($manager, Pages::CONTACT_THANK_YOU, 'kontakty-dyakuyu');

        $this->createPage($manager, Pages::CREDIT_LONG_THANK_YOU, 'online-credit-application-thank-you');
	}

	private function loadMoneyShopPage(ObjectManager $manager)
	{
        $this->createPage($manager, Pages::MONEY_SHOP, 'money-shop', true);
	}

	private function loadVeroCashPage(ObjectManager $manager)
	{
        $this->createPage($manager, Pages::VERO_CASH, 'vero-cash', true);
	}

	private function loadCustomerFeedbacksPage(ObjectManager $manager)
	{
        $this->createPage($manager, Pages::CUSTOMER_FEEDBACK, 'vidhuky-kliientiv', true);
	}

    protected function loadICreditAsEmployer(ObjectManager $manager): void
    {
        $this->createPage($manager, Pages::ICREDIT_AS_EMPLOYER, 'icredit-yak-robotodavets');
    }

    protected function loadMotivationProgram(ObjectManager $manager): void
    {
        $motivationProgrammePage = $this->createPage($manager, Pages::MOTIVATION_PROGRAMME, 'motyvatsiini-prohramy', true);
        $motivationProgrammePage->setMetaTitle('Работа в iCredit');
        $motivationProgrammePage->setMetaDescription('Работа в iCredit');
        $motivationProgrammePage->setMetaKeywords(
            'работа, изи кредит, нужна работа, карьера, icredit, ищу работу, кредитный консультант, агент, менеджер, региональный представитель, администратор, менеджер по выдаче кредитов'
        );
        $motivationProgrammePage->setOgTitle('Работа в iCredit');
        $motivationProgrammePage->setOgDescription('Работа в iCredit');
    }

    protected function loadFinancialCulture(ObjectManager $manager): void
    {
        $this->createPage($manager, Pages::FINANCIAL_CULTURE, 'finansova-kultura', true);
    }

    protected function loadFinancialDictionary(ObjectManager $manager): void
    {
        $this->createPage($manager, Pages::FINANCIAL_DICTIONARY, 'finansovyi-slovnyk', true);
    }

	protected function loadFamilyFinancesPage(ObjectManager $manager): void
	{
		$this->createPage($manager, Pages::FAMILY_FINANCES, 'simeini-finansy', true);
	}

    protected function createPage(ObjectManager $manager, string $name, string $path, bool $includeInSiteMap = false): Page
    {
        $repository = $manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');

        $page = (new Page)
            ->setName($name)
            ->setInternalName($name)
            ->setPath($path)
            ->setIncludeInSiteMap($includeInSiteMap);

        $repository
            ->translate($page, 'name', 'ru', $name)
            ->translate($page, 'path', 'ru', $path)
            ->translate($page, 'name', 'en', $name)
            ->translate($page, 'path', 'en', $path)
        ;

        $manager->persist($page);

        return $page;
    }

    protected function createLandingPage(ObjectManager $manager, string $name, string $path, bool $includeInSiteMap = false): Page
    {
        $repository = $manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');

        $page = (new LandingPage())
            ->setName($name)
            ->setInternalName($name)
            ->setPath($path)
            ->setIncludeInSiteMap($includeInSiteMap);

        $this->landingPageManager->createHeader($page);
        $this->landingPageManager->createFooter($page);

        $repository
            ->translate($page, 'name', 'ru', $name)
            ->translate($page, 'path', 'ru', $path)
            ->translate($page, 'name', 'en', $name)
            ->translate($page, 'path', 'en', $path)
        ;

        $manager->persist($page);

        return $page;
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['module_items'];
    }
}
