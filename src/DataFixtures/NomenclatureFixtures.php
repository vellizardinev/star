<?php

namespace App\DataFixtures;

use App\Service\MobileApi\Nomenclatures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class NomenclatureFixtures extends BaseFixtures
{
    private $nomenclaturesService;

    public function __construct(Nomenclatures $nomenclaturesService)
    {
        $this->nomenclaturesService = $nomenclaturesService;
    }

    /**
     * @param ObjectManager $manager
     * @throws NonUniqueResultException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->nomenclaturesService->update();
    }
}
