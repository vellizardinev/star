<?php

namespace App\DataFixtures;

use App\Factory\CareersPartnerFactory;
use App\Factory\LoyaltyPartnerFactory;
use App\Service\EntityTranslator;
use Doctrine\Persistence\ObjectManager;

class PartnerFixtures extends BaseFixtures
{
    /**
     * @var LoyaltyPartnerFactory
     */
    private $loyaltyPartnerFactory;

    /**
     * @var CareersPartnerFactory
     */
    private $careersPartnerFactory;

    private $loadSeedData;

	/**
	 * @var EntityTranslator
	 */
	private $entityTranslator;

    /**
     * @param LoyaltyPartnerFactory $loyaltyPartnerFactory
     * @param CareersPartnerFactory $careersPartnerFactory
     * @param int $loadSeedData
     * @param EntityTranslator $entityTranslator
     */
    public function __construct(
        LoyaltyPartnerFactory $loyaltyPartnerFactory,
        CareersPartnerFactory $careersPartnerFactory,
        int $loadSeedData,
        EntityTranslator $entityTranslator
    ) {
        $this->loyaltyPartnerFactory = $loyaltyPartnerFactory;
        $this->careersPartnerFactory = $careersPartnerFactory;
        $this->loadSeedData = $loadSeedData;
        $this->entityTranslator = $entityTranslator;
    }

    protected function loadData(ObjectManager $manager)
    {
	    // Partners will not be available at launch of the site
	    return;

	    if ($this->loadSeedData !== 1) {
            return;
        }

        $loyaltyPartners = $this->loyaltyPartnerFactory->create([], 10);

	    foreach ($loyaltyPartners as $careerPartner){
		    $this->entityTranslator->translateLoyaltyPartner($careerPartner);
	    }

        $careerPartners = $this->careersPartnerFactory->create([], 10);

        foreach ($careerPartners as $careerPartner){
            $this->entityTranslator->translateCareerPartner($careerPartner);
        }

	    $this->createEkoPartner();
    }

    private function createEkoPartner()
    {
	    $loyaltyPartner = $this->loyaltyPartnerFactory->create(
		    [
			    'name' => 'eko',
			    'description' => '<p style="text-align: center;"><span style="font-size: 10pt;">Отстъпки от цените на гориво:<br>
<span class="clever-link" data-link="http://www.eko.bg/">www.eko.bg</span>

</span></p><div class="table-wrapper"><table cellpadding="3" cellspacing="0" height="109" style="border-collapse: collapse;" width="336">
	<tbody>
		<tr>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;"><span style="font-size: 10pt;">&nbsp;Гориво</span></td>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;">&nbsp;<span style="font-size: 10pt;">раб.дни</span></td>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;">&nbsp;<span style="font-size: 10pt;">уикенд</span></td>
		</tr>
		<tr>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;"><span style="font-size: 10pt;">&nbsp;A95H</span></td>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;">&nbsp;<span style="font-size: 10pt;">0.05 лв.</span></td>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;"><span style="font-size: 10pt;">&nbsp;0.07 </span><span style="font-size: 10pt;">лв.</span></td>
		</tr>
		<tr>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;">&nbsp;<span style="font-size: 10pt;">Дизел иконом.</span></td>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;">&nbsp;<span style="font-size: 10pt;">0.05 </span><span style="font-size: 10pt;">лв.</span></td>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;">&nbsp;<span style="font-size: 10pt;">0.07</span><span style="font-size: 10pt;"> лв.</span></td>
		</tr>
		<tr>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;">&nbsp;<span style="font-size: 10pt;">Газ</span></td>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;">&nbsp;<span style="font-size: 10pt;">0.03 </span><span style="font-size: 10pt;">лв.</span></td>
			<td style="width: 33.3333%; border-color: rgb(0, 0, 0); border-width: 1px; border-style: solid; text-align: center;">&nbsp;<span style="font-size: 10pt;">0.03</span><span style="font-size: 10pt;"> лв.</span></td>
		</tr>
	</tbody>
</table></div>

<p style="text-align: center;"></p>',
			    'discount' => 'До 7 ст./л.',
		    ]
	    );

	    $this->entityTranslator->translateLoyaltyPartner($loyaltyPartner);
    }
}
