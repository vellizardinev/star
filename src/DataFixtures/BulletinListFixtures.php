<?php

namespace App\DataFixtures;

use App\Factory\BulletinListFactory;
use Doctrine\Persistence\ObjectManager;

class BulletinListFixtures extends BaseFixtures
{
    private $factory;

    /**
     * @var int
     */
    private $loadSeedData;

    /**
     * BulletinListFixtures constructor.
     * @param BulletinListFactory $factory
     * @param int $loadSeedData
     */
    public function __construct(BulletinListFactory $factory, int $loadSeedData)
    {
        $this->factory = $factory;
        $this->loadSeedData = $loadSeedData;
    }

    protected function loadData(ObjectManager $manager)
    {
        if ($this->loadSeedData === 1) {
            $this->factory->create([], 20);
        }
    }
}
