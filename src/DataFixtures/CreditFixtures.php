<?php

namespace App\DataFixtures;

use App\Enum\CreditType;
use App\Enum\PaymentSchedule;
use App\Factory\CreditFactory;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class CreditFixtures extends BaseFixtures implements FixtureGroupInterface
{
    /**
     * @var CreditFactory
     */
    private $creditFactory;

    /**
     * @param CreditFactory $creditFactory
     */
    public function __construct(CreditFactory $creditFactory)
    {
        $this->creditFactory = $creditFactory;
    }

    protected function loadData(ObjectManager $manager)
    {
        $requirements = <<<TAG
<ul><li>Потрібен паспорт та ІПН</li><li>Від 2 000 до 50 000 грн.</li><li>Від 15 до 36 внески</li><li>Погашення: Раз на тиждень</li></ul>
TAG;
        $this->creditFactory->create([
            'type' => CreditType::PERSONAL,
            'title' => 'iCredit',
            'shortDescription' => 'Один з найшвидших з існуючих в Україні кредитів готівкою для фізичних осіб і СПД.',
            'fullDescription' => file_get_contents(__DIR__ . '/credit_descriptions/i_credit.txt'),
            'paymentSchedule' => PaymentSchedule::WEEKLY,
            'requirements' => $requirements,
            'slug' => 'icredit',
            'maxAmount' => 50000,
            'parametersDescription' => $this->faker->sentences(3, true),
            'allowOnlineApplication' => true,
            'defaultAmount' => 2000.00
        ]);

	    $requirements = <<<TAG
<ul><li>Потрібен паспорт та ІПН</li><li>Від 2 000 до 50 000 грн.</li><li>Від 6 до 28 внески</li><li>Погашення: Два рази на місяць</li></ul>
TAG;
        $this->creditFactory->create([
            'type' => CreditType::PERSONAL,
            'title' => 'BiMonth',
            'shortDescription' => 'Продукт BiMonth спеціально створений для тих, кому зручно вносити платежі по кредиту 2 рази на місяць.',
            'fullDescription' => file_get_contents(__DIR__ . '/credit_descriptions/bi_month.txt'),
            'paymentSchedule' => PaymentSchedule::BIWEEKLY,
            'requirements' => $requirements,
            'slug' => 'bi-month',
            'maxAmount' => 50000,
            'parametersDescription' => $this->faker->sentences(3, true),
            'allowOnlineApplication' => true,
            'defaultAmount' => 2000.00
        ]);

        $requirements = <<<TAG
<ul><li>Потрібен паспорт та ІПН</li><li>Від 2 000 до 50 000 грн.</li><li>Від 3 до 14 внески</li><li>Погашення: Раз на місяць</li></ul>
TAG;
        $this->creditFactory->create([
            'type' => CreditType::PERSONAL,
            'title' => 'iMonth',
            'shortDescription' => 'Швидкі кредити, які погашаються рівними щомісячними внесками.',
            'fullDescription' => file_get_contents(__DIR__ . '/credit_descriptions/i_month.txt'),
            'paymentSchedule' => PaymentSchedule::MONTHLY,
            'requirements' => $requirements,
            'slug' => 'i-month',
            'maxAmount' => 50000,
            'parametersDescription' => $this->faker->sentences(3, true),
            'allowOnlineApplication' => true,
            'defaultAmount' => 2000.00
        ]);

	    $requirements = <<<TAG
<ul><li>Потрібне пенсійне посвідчення</li><li>Від 1 000 до 15 000 грн.</li><li>Від 4 до 15 внески</li><li>Погашення: Раз на місяць</li></ul>
TAG;
	    $this->creditFactory->create([
		    'type' => CreditType::PERSONAL,
		    'title' => 'Pensioner',
		    'shortDescription' => 'Describe credit pensioner in several sentences',
		    'fullDescription' => file_get_contents(__DIR__ . '/credit_descriptions/pensioner.txt'),
		    'paymentSchedule' => PaymentSchedule::MONTHLY,
		    'requirements' => $requirements,
		    'slug' => 'pensioner',
		    'maxAmount' => 15000,
		    'parametersDescription' => $this->faker->sentences(3, true),
		    'allowOnlineApplication' => true,
		    'defaultAmount' => 1000.00,
		    'pensioner' => true
	    ]);

        $requirements = <<<TAG
<ul><li>Потрібен паспорт та ІПН</li><li>Від 2 000 до 5 000 грн.</li><li>Від 8 до 12 внески</li><li>Погашення: Раз на тиждень</li></ul>
TAG;
        $this->creditFactory->create([
            'type' => CreditType::PERSONAL,
            'title' => 'iWeek',
            'shortDescription' => 'Один з найшвидших з існуючих в Україні кредитів готівкою для фізичних осіб і СПД.',
            'fullDescription' => file_get_contents(__DIR__ . '/credit_descriptions/i_week.txt'),
            'paymentSchedule' => PaymentSchedule::WEEKLY,
            'requirements' => $requirements,
            'slug' => 'i-week',
            'maxAmount' => 5000,
            'parametersDescription' => $this->faker->sentences(3, true),
            'allowOnlineApplication' => true,
            'defaultAmount' => 2000.00
        ]);
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['credit_data'];
    }
}
