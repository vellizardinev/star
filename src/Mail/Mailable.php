<?php


namespace App\Mail;


abstract class Mailable
{
    abstract function type(): string;

    abstract function context(): array;
}