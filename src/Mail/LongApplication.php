<?php


namespace App\Mail;


class LongApplication extends Mailable
{
    /**
     * @var array
     */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    function type(): string
    {
        return 'long_application';
    }

    function context(): array
    {
        return ['data' => $this->data];
    }
}