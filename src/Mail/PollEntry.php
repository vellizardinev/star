<?php


namespace App\Mail;


use App\Entity\Poll;

class PollEntry extends Mailable
{
    /**
     * @var array
     */
    private $data;
    /**
     * @var Poll
     */
    private $poll;

    /**
     * PollEntry constructor.
     * @param Poll $poll
     * @param array $data
     */
    public function __construct(Poll $poll, array $data)
    {

        $this->data = $data;
        $this->poll = $poll;
    }

    function type(): string
    {
        return 'poll_entry';
    }

    function context(): array
    {
        $processed = [];

        foreach ($this->data as $question => $questionData) {
            if (!array_key_exists('answer', $questionData)) {
                continue;
            }

            $item = ['question' => $question];

            foreach ($questionData['answer'] as $key => $answer) {
                if (array_key_exists('comment', $questionData)) {
                    if (array_key_exists($key, $questionData['comment'])) {
                        $comment = $questionData['comment'][$key];
                    }
                }

                $item['answers'][] = ['answer' => $answer, 'comment' => $comment ?? 'n/a'];
            }

            $processed[] = $item;
        }


        return [
            'data' => $processed,
            'pollName' => $this->poll->getTitle(),
        ];
    }
}