<?php


namespace App\Mail;


use App\Entity\JobApplication as JobApplicationRequest;
use App\Entity\JobOpening;
use Symfony\Contracts\Translation\TranslatorInterface;

class JobApplication extends Mailable
{
    /**
     * @var JobApplicationRequest
     */
    private $jobApplication;

    /**
     * @var JobOpening
     */
    private $jobOpening;

    /**
     * JobApplication constructor.
     * @param JobApplicationRequest $jobApplication
     * @param JobOpening $jobOpening
     */
    public function __construct(JobApplicationRequest $jobApplication, JobOpening $jobOpening)
    {
        $this->jobApplication = $jobApplication;
        $this->jobOpening = $jobOpening;
    }

    function type(): string
    {
        return 'job_application';
    }

    function context(): array
    {
        return [
            'city' => $this->jobApplication->getCity() ?? 'n/a',
            'name' => $this->jobApplication->getFullName(),
            'telephone' => $this->jobApplication->getTelephone(),
            'email' => $this->jobApplication->getEmail(),
            'comment' => $this->jobApplication->getComment() ?? 'n/a',
            'cv' => $this->jobApplication->getCv() !== null ? true : false,
            'position' => $this->jobOpening->getPosition() ?? 'n/a',
            'reference' => $this->jobOpening->getReference() ?? 'n/a',
            'department' => $this->jobOpening->getDepartment()?$this->jobOpening->getDepartment()->getName() : 'n/a',
            'office' => $this->jobOpening->getOffice()?$this->jobOpening->getOffice()->getName() : 'n/a',
            'officeCity' => $this->jobOpening->getOffice()?$this->jobOpening->getOffice()->getCity()->getName() : 'n/a',
        ];
    }
}