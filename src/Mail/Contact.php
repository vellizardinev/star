<?php


namespace App\Mail;


use App\Entity\Contact as ContactMessage;
use App\Enum\ContactAbout;
use App\Enum\FeedbackStrategy;
use Symfony\Contracts\Translation\TranslatorInterface;

class Contact extends Mailable
{
    /**
     * @var ContactMessage
     */
    private $contact;

	/**
	 * @var TranslatorInterface
	 */
	private $translator;

    /**
     * Contact constructor.
     * @param ContactMessage $contact
     * @param TranslatorInterface $translator
     */
    public function __construct(ContactMessage $contact, TranslatorInterface $translator)
    {
        $this->contact = $contact;
        $this->translator = $translator;
    }

    function type(): string
    {
        return 'contact';
    }

    function context(): array
    {
        return [
            'inRelationWith' => $this->translateContactAboutValue($this->contact->getAbout()),
            'firstName' => $this->contact->getFirstName(),
            'lastName' => $this->contact->getLastName(),
            'email' => $this->contact->getEmail(),
            'telephone' => $this->contact->getTelephone(),
            'address' => $this->contact->getAddress() ?? 'n/a',
            'preferredFeedbackMethod' => $this->translateFeedbackStrategyValue($this->contact->getFeedbackStrategy()),
            'comment' => $this->contact->getComment() ?? 'n/a',
        ];
    }

    private function translateContactAboutValue($value)
    {
		$translationKey = ContactAbout::getTranslationKey($value);
    	return $this->translator->trans($translationKey, [], 'form');
    }

	private function translateFeedbackStrategyValue($value)
	{
		if($value){
			$translationKey = FeedbackStrategy::getTranslationKey($value);
			return $this->translator->trans($translationKey, [], 'form');
		}

		return 'n/a';
	}
}