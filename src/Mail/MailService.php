<?php

namespace App\Mail;

use App\Entity\Mail;
use App\Entity\Setting;
use App\Enum\Settings;
use Doctrine\ORM\EntityManagerInterface;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Swift_Attachment;

class MailService
{
    protected $to = [];

    protected $subject;

    protected $cc = [];

    protected $bcc = [];

    protected $from;

    protected $locale = 'ua';

    protected $attachments = [];

    protected $manager;

    protected $mailer;

    private $twig;

    private $loadMailTemplatesFromDatabase;

    public function __construct(
        EntityManagerInterface $manager,
        RequestStack $requestStack,
        Swift_Mailer $mailer,
        Environment $twig,
        bool $loadMailTemplatesFromDatabase
    )
    {
        $this->manager = $manager;
        $this->from = $manager->getRepository(Setting::class)
            ->findOneBy(['name' => Settings::DEFAULT_FROM_EMAIL])
            ->getValue();

        if ($requestStack->getMasterRequest()) {
            $this->locale = $requestStack->getMasterRequest()->getLocale();
        }

        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->loadMailTemplatesFromDatabase = $loadMailTemplatesFromDatabase;
    }

    public function subject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function to(string $address): self
    {
        $this->to = [$address];

        return $this;
    }

    public function toMany(array $addresses = []): self
    {
        $this->to = $addresses;

        return $this;
    }

    public function cc(string $address): self
    {
        $this->cc = [$address];

        return $this;
    }

    public function ccMany(array $addresses = []): self
    {
        $this->cc = $addresses;

        return $this;
    }

    public function bcc(string $address): self
    {
        $this->bcc = [$address];

        return $this;
    }

    public function bccMany(array $addresses = []): self
    {
        $this->bcc = $addresses;

        return $this;
    }

    public function from(string $address): self
    {
        $this->from = $address;

        return $this;
    }

    public function locale(string $locale = 'bg'): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function attachments(array $attachments): self
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * @param Mailable $mailable
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function send(Mailable $mailable)
    {
        $mailObject = $this->manager
            ->getRepository(Mail::class)
            ->findOneBy(['type' => $mailable->type(), 'locale' => $this->locale]);

        if ($this->loadMailTemplatesFromDatabase) {
            $template = $this->twig->createTemplate($mailObject->getContent());
            $body = $this->twig->render($template, $mailable->context());
        } else {
            $body = $this->twig->render(
                sprintf('mail/%s/%s.html.twig', $this->locale, $mailable->type()),
                $mailable->context()
            );

            $subject = $mailable->type();
        }

        $email = (new Swift_Message)
            ->setTo($this->to)
            ->setFrom($this->from)
            ->setCc($this->cc)
            ->setBcc($this->bcc)
            ->setSubject($subject ?? $mailObject->getSubject())
            ->setBody($body, 'text/html');

        if (!empty($this->attachments)) {
            foreach ($this->attachments as $attachment) {
                if($attachment instanceof UploadedFile) {
                    $email->attach(Swift_Attachment::fromPath($attachment)->setFilename($attachment->getClientOriginalName()));
                }else{
                    $email->attach(Swift_Attachment::fromPath($attachment));
                }
            }
        }

        $this->mailer->send($email);
    }
}