<?php


namespace App\Mail;


use App\Entity\OnlineApplication\OnlineApplication;

class UnfinishedApplicationReminder extends Mailable
{
    private $onlineApplication;

    private $applicationUrl;


    public function __construct(OnlineApplication $onlineApplication, string $applicationUrl)
    {
        $this->onlineApplication = $onlineApplication;
        $this->applicationUrl = $applicationUrl;
    }

    function type(): string
    {
        return 'unfinished_application_reminder';
    }

    function context(): array
    {
        return [
            'userName' => $this->onlineApplication->getPersonalInfo()->fullName(),
            'applicationUrl' => $this->applicationUrl,
        ];
    }
}