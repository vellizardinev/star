<?php


namespace App\Mail;


use App\Entity\CreditShortApplication;

class CreditApplication extends Mailable
{
    /**
     * @var CreditShortApplication $creditApplication
     */
    private $creditApplication;

    /**
     * CreditApplication constructor.
     * @param CreditShortApplication $creditApplication
     */
    public function __construct(CreditShortApplication $creditApplication)
    {
       $this->creditApplication = $creditApplication;
    }

    function type(): string
    {
        return 'credit_application';
    }

    function context(): array
    {
	    $creditApplicationData = [
		    'name' => $this->creditApplication->getName(),
		    'telephone' => $this->creditApplication->getTelephone(),
		    'birthdayDate' => $this->creditApplication->getBirthdayDate(),
            'amount' => $this->creditApplication->getAmount() ?? 'n/a',
            'landingPage' => $this->creditApplication->getLanding() ?? 'n/a',
            'partner' => $this->creditApplication->getAffiliate() ?? 'n/a',
	    ];

    	if ($this->creditApplication->getCredit())
	    {
		    $credit = $this->creditApplication->getCredit();
		    $creditData = [
			    'creditTitle' => $credit->getTitle(),
			    'creditFullDescription' => $credit->getFullDescription(),
			    'creditShortDescription' => $credit->getShortDescription(),
			    'creditMaxAmount' => $credit->getMaxAmount(),
			    'creditRequirements' => $credit->getRequirements(),
			    'creditPaymentSchedule' => $credit->getPaymentSchedule(),
			    'creditParametersDescription' => $credit->getParametersDescription()
		    ];

		    $creditApplicationData = array_merge($creditApplicationData, $creditData);
	    }

        return $creditApplicationData;
    }
}