<?php


namespace App\Mail;


use App\Entity\FailedOnlineApplicationRequest;

class FailedOnlineApplication extends Mailable
{
    private $httpRequest;

    public function __construct(FailedOnlineApplicationRequest $httpRequest)
    {
        $this->httpRequest = $httpRequest;
    }

    function type(): string
    {
        return 'failed_online_application';
    }

    function context(): array
    {
        $application = $this->httpRequest->getApplication();
        return [
            'email' => $application->getPersonalInfo()->getEmail(),
            'customerName' => $application->getPersonalInfo()->getFirstName() . ' ' . $application->getPersonalInfo()->getLastName(),
            'amount' => $application->getCreditDetails()->getAmount(),
            'submittedOn' => $application->getCreatedAt(),
        ];
    }
}