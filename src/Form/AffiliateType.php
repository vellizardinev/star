<?php

namespace App\Form;

use App\Entity\Affiliate;
use App\Form\CustomFields\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AffiliateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'label.name',
            ])
            ->add('contactInfo', CKEditorType::class, [
                'label' => 'label.contact_info',
                'label_attr' => ['class' => 'required'],
            ])
            ->add('customCSS', TextareaType::class,[
                'label' => 'label.custom_css',
                'attr' => ['rows' => 10],
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Affiliate::class,
            'translation_domain' => 'form'
        ]);
    }
}
