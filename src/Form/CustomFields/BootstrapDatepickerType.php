<?php


namespace App\Form\CustomFields;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BootstrapDatepickerType extends AbstractType
{
    public function getParent()
    {
        return DateType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'widget' => 'single_text',
            'html5' => false,
            'format' => 'dd/MM/yyyy',
            'attr' => [
            	'autocomplete' => 'off',
                'data-provide' => 'datepicker',
                'data-date-autoclose' => true,
                'data-date-format' => 'dd/mm/yyyy',
	            'data-date-orientation' => 'bottom',
            ],
        ]);
    }
}