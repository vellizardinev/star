<?php

namespace App\Form\CustomFields;

use App\Entity\Setting;
use App\Enum\Settings;
use App\Validator\Constraints\CheckedCaptcha;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CaptchaType extends AbstractType
{
	/**
	 * @var EntityManagerInterface
	 */
	private $manager;

	/**
	 * CaptchaType constructor.
	 * @param EntityManagerInterface $entityManager
	 */
	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->manager = $entityManager;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefault('mapped', false);
		$resolver->setDefault('constraints', [new CheckedCaptcha()]);
		$resolver->setDefault('attr', [
			'data-controller' => 'captcha',
			'data-api-key' => $this->manager->getRepository(Setting::class)->get(Settings::GOOGLE_RECAPTCHA_API_KEY)->getValue()
		]);
	}

	public function getParent()
	{
		return HiddenType::class;
	}

	public function getBlockPrefix()
	{
		return 'recaptcha';
	}
}