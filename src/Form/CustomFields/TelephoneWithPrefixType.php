<?php


namespace App\Form\CustomFields;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TelephoneWithPrefixType extends AbstractType
{
    const DEFAULT_PREFIX = '+380';

    public function getParent()
    {
        return TelType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'prefix' => self::DEFAULT_PREFIX,
                'style' => 'height: inherit;',
            ],
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $attributes = $view->vars['attr'];

        if (!isset($attributes['prefix'])) {
            $attributes['prefix'] = self::DEFAULT_PREFIX;
        }

        if (array_key_exists('style', $attributes)) {
            $attributes['style'] .= ' height: inherit;';
        } else {
            $attributes['style'] = 'height: inherit;';
        }

        $view->vars['attr'] = $attributes;
    }
}