<?php


namespace App\Form\CustomFields;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class BootstrapFileUploadType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getParent()
    {
        return FileType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'data-controller' => 'bootstrap-file-upload',
                'data-action' => 'change->bootstrap-file-upload#change',
                'data-browse-button' => $this->translator->trans('button.browse', [], 'form')
            ],
        ]);
    }
}