<?php


namespace App\Form\CustomFields;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

class CKEditorType extends AbstractType
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * CKEditorType constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function getParent()
    {
        return TextareaType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'data-controller' => 'ckeditor',
                'data-upload-url' => $this->router->generate('admin.ckeditor.upload'),
            ],
            'required' => false,
        ]);
    }
}