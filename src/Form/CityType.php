<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\JobOpening;
use App\Enum\NewsCategories;
use App\Repository\CityRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cities', EntityType::class,[
            	'placeholder' => 'placeholder.all',
            	'class' => City::class,
                'choices' => $options['city_choices'],
                'query_builder' => function (CityRepository $cityRepository) {
                    return $cityRepository->citiesUsedInJobOpening();
                },
	            'choice_label' => 'name',
	            'label' => 'cities',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'translation_domain' => 'form',
	        'city_choices' => null
        ]);
    }
}
