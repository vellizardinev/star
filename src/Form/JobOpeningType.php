<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Department;
use App\Entity\JobOpening;
use App\Entity\Office;
use App\Form\CustomFields\BootstrapDatepickerType;
use App\Form\CustomFields\CKEditorType;
use App\Repository\OfficeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobOpeningType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', TextType::class, [
                'label' => 'label.position',
            ])
            ->add('description', CKEditorType::class, [
                'label' => 'label.description',
	            'label_attr' => ['class' => 'required']
            ])
            ->add('deadline', BootstrapDatepickerType::class, [
                'label' => 'label.deadline',
            ])
            ->add('reference', TextType::class, [
                'label' => 'label.reference',
            ])
            ->add('city', EntityType::class, [
                'label' => 'label.city',
                'class' => City::class,
                'choice_label' => 'name'
            ])
            ->add('office', EntityType::class, [
                'label' => 'label.office',
                'class' => Office::class,
                'query_builder' => function (OfficeRepository $officeRepository) {
                    return $officeRepository->all();
                },
                'choice_label' => 'name'
            ])
            ->add('department', EntityType::class, [
                'label' => 'label.department',
                'class' => Department::class,
                'choice_label' => 'name'
            ])
	        ->add('showImage', CheckboxType::class, [
		        'label' => 'label.show_image',
		        'required' => false,
	        ])
        ;

	    if ($options['show_slug']) {
		    $builder
			    ->add('slug', TextType::class, [
				    'label' => 'label.slug',
			    ]);
	    }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => JobOpening::class,
            'translation_domain' => 'form',
	        'show_slug' => true
        ]);
    }
}
