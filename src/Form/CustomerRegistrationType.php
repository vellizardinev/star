<?php

namespace App\Form;

use App\Entity\Customer;
use App\Enum\CreateUserAccountErrors;
use App\Form\CustomFields\CaptchaType;
use App\Validator\Constraints\ApiPassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CustomerRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, ['label' => 'label.first_name'])
            ->add('middleName', TextType::class, ['label' => 'label.middle_name'])
            ->add('lastName', TextType::class, ['label' => 'label.last_name'])
            ->add('personalIdentificationNumber', TextType::class, ['label' => 'label.personal_identification_number'])
            ->add('email', EmailType::class, ['label' => 'label.email'])
	        ->add(
		        'telephone',
		        TextType::class,
		        [
			        'label' => 'label.mobile_telephone',
			        'attr' => [
				        'pattern' => '\d+',
			        ],
		        ]
	        )
            ->add('plainPassword', RepeatedType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'type' => PasswordType::class,
                'first_options'  => ['label' => 'label.password'],
                'second_options' => ['label' => 'label.repeat_password'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'password_not_blank',
                    ]),
                    new ApiPassword(['message' => 'error_invalid_password']),
                ],
            ])
	        ->add('captcha', CaptchaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
            'translation_domain' => 'form',
        ]);
    }
}
