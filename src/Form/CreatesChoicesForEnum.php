<?php


namespace App\Form;


trait CreatesChoicesForEnum
{
    private function choicesFromEnum(array $translationKeys): array
    {
        $choices = [];
        $flippedKeys = array_flip($translationKeys);
        $choices = array_merge($choices, $flippedKeys);

        return $choices;
    }
}