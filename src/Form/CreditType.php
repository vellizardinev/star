<?php

namespace App\Form;

use App\Entity\Credit;
use App\Enum\PaymentSchedule;
use App\Form\CustomFields\CKEditorType;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [];
        $categories = array_flip(PaymentSchedule::toTranslationKeys());
        $choices = array_merge($choices, $categories);

        $types = [];
        $values = array_flip(\App\Enum\CreditType::toTranslationKeys());
        $types = array_merge($types, $values);

        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'label.type',
                'choices' => $types,
            ])
            ->add('title', TextType::class, [
                'label' => 'label.title',
            ])
            ->add('shortDescription', CKEditorType::class, [
                'label' => 'label.short_description',
	            'required' => false,
            ])
            ->add('fullDescription', CKEditorType::class, [
                'label' => 'label.full_description',
                'required' => false,
            ])
            ->add('maxAmount', NumberType::class, [
                'label' => 'label.max_amount',
            ])
            ->add('parametersDescription', CKEditorType::class, [
                'label' => 'label.parameters_description',
                'required' => false,
            ])
            ->add('requirements', CKEditorType::class, [
                'label' => 'label.requirements',
	            'label_attr' => ['class' => 'required']
            ])
            ->add('paymentSchedule', ChoiceType::class, [
                'label' => 'label.payment_schedule',
                'choices' => $choices,
            ])
            ->add('allowOnlineApplication', CheckboxType::class, [
                'label' => 'label.allow_online_application',
                'required' => false,
            ])
        ;

	    if ($options['show_slug']) {
		    $builder
			    ->add('slug', TextType::class, [
				    'label' => 'label.slug',
			    ]);
	    }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Credit::class,
            'translation_domain' => 'form',
	        'show_slug' => true
        ]);
    }
}
