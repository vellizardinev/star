<?php

namespace App\Form;

use App\Entity\Department;
use App\Form\CustomFields\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepartmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'label.name',
            ])
            ->add('director', TextType::class, [
                'label' => 'label.director',
            ])
	        ->add('description', CKEditorType::class, [
		        'label' => 'label.description',
	        ])
	        ->add('visible', CheckboxType::class, [
		        'label' => 'label.visible',
		        'required' => false
	        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Department::class,
            'translation_domain' => 'form'
        ]);
    }
}
