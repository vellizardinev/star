<?php

namespace App\Form;

use App\Enum\ItemsPerPage;
use App\Enum\Sort;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SortType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('itemsPerPage', ChoiceType::class,[
	            'label' => 'label.items_per_page',
	            'choices' => ItemsPerPage::toValueArray(),
	            'attr' => [
	            	'data-controller' => 'query',
		            'data-query-parameter'=> 'limit'
	            ],
	            'data' => $options['limit']
            ])
	        ->add('sortBy', ChoiceType::class,[
	        	'label' => 'label.sort_by',
		        'choices' => array_flip(Sort::toTranslationKeys()),
		        'attr' => [
			        'data-controller' => 'query',
			        'data-query-parameter'=> 'sort_by'
		        ],
		        'data' => $options['sort_by']
	        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'translation_domain' => 'form',
	        'limit' => null,
	        'sort_by' => Sort::getValue('POSITION_ASC'),
        ]);
    }
}
