<?php

namespace App\Form;

use App\Entity\Permission\Group;
use App\Entity\Permission\Permission;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PermissionGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'label.name'
            ])
            ->add('permissions', EntityType::class, [
                'expanded' => true,
                'multiple' => true,
                'class' => Permission::class,
                'choice_label' => 'fullName',
                'label' => 'label.permissions',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Group::class,
            'translation_domain' => 'form'
        ]);
    }
}
