<?php

namespace App\Form;

use App\Entity\Profile;
use App\Form\CustomFields\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, ['label' => 'label.first_name'])
            ->add('lastName', TextType::class, ['label' => 'label.last_name']);

        if($options['add_captcha'])
        {
	        $builder->add('captcha', CaptchaType::class);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Profile::class,
            'translation_domain' => 'form',
	        'add_captcha' => false
        ]);
    }
}
