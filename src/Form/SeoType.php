<?php

namespace App\Form;

use App\Entity\Image;
use App\Entity\News;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('metaTitle', TextType::class, [
                'label' => 'label.meta_title',
                'required' => false,
            ])
            ->add('metaDescription', TextareaType::class, [
                'label' => 'label.meta_description',
                'required' => false,
            ])
            ->add('metaKeywords', TextType::class, [
                'label' => 'label.meta_keywords',
                'required' => false,
            ])
            ->add('ogTitle', TextType::class, [
                'label' => 'label.og_title',
                'required' => false,
            ])
            ->add('ogDescription', TextareaType::class, [
                'label' => 'label.og_description',
                'required' => false,
            ])
            ->add('ogImage', EntityType::class, [
                'class' => Image::class,
                'choice_label' => 'originalName',
                'label' => 'label.og_image',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
            'translation_domain' => 'form',
        ]);
    }
}
