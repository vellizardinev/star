<?php

namespace App\Form;

use App\Entity\LoyaltyPartner;
use App\Form\CustomFields\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'label.name',
            ])
            ->add('description', CKEditorType::class, [
	            'label' => 'label.description',
	            'label_attr' => ['class' => 'required']
            ])
	        ->add('discount', TextType::class, [
		        'label' => 'label.discount'
	        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LoyaltyPartner::class,
            'translation_domain' => 'form'
        ]);
    }
}
