<?php

namespace App\Form;

use App\Entity\CreditLongApplication;
use App\Enum\ActivityAreas;
use App\Enum\BooleanTypes;
use App\Enum\CreditPurposes;
use App\Enum\EducationTypes;
use App\Enum\EmploymentTypes;
use App\Enum\FamilyStatuses;
use App\Enum\FrequencyOfReceivingSalary;
use App\Enum\HomeTypes;
use App\Enum\InformationSources;
use App\Enum\MethodsReceivingSalary;
use App\Enum\PassportTypes;
use App\Enum\PensionCertificateTypes;
use App\Enum\PositionTypes;
use App\Enum\PropertyType;
use App\Enum\RelationshipTypes;
use App\Form\CustomFields\BootstrapDatepickerType;
use App\Form\CustomFields\CaptchaType;
use Carbon\Carbon;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class CreditLongApplicationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // Credit details
            ->add(
                'amount',
                HiddenType::class,
                ['attr' => ['data-controller' => 'credit-parameters-field', 'data-property-name' => 'amount']]
            )
            ->add(
                'product',
                HiddenType::class,
                ['attr' => ['data-controller' => 'credit-parameters-field', 'data-property-name' => 'product']]
            )
            ->add(
                'period',
                HiddenType::class,
                ['attr' => ['data-controller' => 'credit-parameters-field', 'data-property-name' => 'period']]
            )
            ->add(
                'instalmentAmount',
                HiddenType::class,
                ['attr' => ['data-controller' => 'credit-parameters-field', 'data-property-name' => 'payment']]
            )
//            ->add(
//                'howShouldMoneyBeReceived',
//                ChoiceType::class,
//                [
//                    'choices' => $this->choicesFromEnum(MethodsToReceiveCredit::toTranslationKeys()),
//                    'placeholder' => 'select_an_option',
//                    'label' => 'label.how_should_money_be_received',
//                    'attr' => ['data-controller' => 'master-field', 'data-property-name' => 'receiveCreditMethod'],
//                ]
//            )
//            ->add(
//                'iban',
//                TextType::class,
//                [
//                    'required' => false,
//                    'label' => 'label.iban',
//                    'label_attr' => ['class' => 'required'],
//                    'row_attr' => ['data-controller' => 'iban', 'data-property-name' => 'receiveCreditMethod'],
//                ]
//            )
//            ->add('bankCardExpirationDateMonth', ChoiceType::class, [
//                'required' => false,
//                'mapped' => true,
//                'label' => 'label.bank_card_expiration_date',
//                'expanded' => false,
//                'placeholder' => 'month',
//                'choices' => $this->months(),
//                'label_attr' => ['class' => 'required'],
//            ])
//            ->add('bankCardExpirationDateYear', ChoiceType::class, [
//                'required' => false,
//                'mapped' => true,
//                'label' => 'label.bank_card_expiration_date',
//                'expanded' => false,
//                'placeholder' => 'year',
//                'choices' => $this->years(null, Carbon::today()->year + 5),
//                'label_attr' => ['class' => 'required'],
//            ])

            // Personal info
            ->add('firstName', TextType::class, ['label' => 'label.first_name', 'help' => 'help.only_letters_and_dash_allowed'])
            ->add('middleName', TextType::class, ['label' => 'label.middle_name', 'help' => 'help.only_letters_and_dash_allowed'])
            ->add('lastName', TextType::class, ['label' => 'label.last_name', 'help' => 'help.only_letters_and_dash_allowed'])
            ->add(
                'personalIdentificationNumber',
                TextType::class,
                [
                    'label' => 'label.personal_identification_number', 'help' => 'help.pin',
                    'attr' => ['data-controller' => 'pin', 'data-action' => 'change->pin#update']
                ]
            )
            ->add('idCardNumber', TextType::class, ['label' => 'label.id_card_number'])
            ->add('idCardIssuedAt', BootstrapDatepickerType::class, ['label' => 'label.id_card_issued_at'])
            ->add('idCardIssuedBy', TextType::class, ['label' => 'label.id_card_issued_by'])

            // Files
            ->add('passportType', ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(PassportTypes::toTranslationKeys()),
                    'label' => 'label.passport_type',
                    'placeholder' => 'select_an_option',
                    'attr' => ['data-controller' => 'master-field', 'data-property-name' => 'passportType'],
                ]
            )
            ->add(
                'oldStylePassportFirstPageUploaded',
                FileType::class,
                [
                    'label' => 'label.old_style_passport_first_page',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePassportSecondPageUploaded',
                FileType::class,
                [
                    'label' => 'label.old_style_passport_second_page',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePassportPhotoAt25Uploaded',
                FileType::class,
                [
                    'label' => 'label.old_style_passport_photo_at_25',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePassportPastedPhotoAt25Uploaded',
                FileType::class,
                [
                    'label' => 'label.old_style_passport_pasted_photo_at_25',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePassportPhotoAt45Uploaded',
                FileType::class,
                [
                    'label' => 'label.old_style_passport_photo_at_45_uploaded',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePassportPastedPhotoAt45Uploaded',
                FileType::class,
                [
                    'label' => 'label.old_style_passport_pasted_photo_at_45',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePassportProofOfResidenceUploaded',
                FileType::class,
                [
                    'label' => 'label.old_style_passport_proof_of_residence',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePassportSelfieWithPassportUploaded',
                FileType::class,
                [
                    'label' => 'label.old_style_passport_selfie_with_passport',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['application/pdf', 'application/x-pdf', 'image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'newStylePassportPhotoPageUploaded',
                FileType::class,
                [
                    'label' => 'label.new_style_passport_photo_page',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['application/pdf', 'application/x-pdf', 'image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'newStylePassportValidityPageUploaded',
                FileType::class,
                [
                    'label' => 'label.new_style_passport_validity_page',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'newStylePassportRegistrationAppendixUploaded',
                FileType::class,
                [
                    'label' => 'label.new_style_passport_registration_appendix',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'newStylePassportSelfieWithIdCardUploaded',
                FileType::class,
                [
                    'label' => 'label.new_style_passport_selfie_with_id_card',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['application/pdf', 'application/x-pdf', 'image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add('pensionCertificateType', ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(PensionCertificateTypes::toTranslationKeys()),
                    'label' => 'label.pensioner_certificate_type',
                    'label_attr' => ['data-controller' => 'pensioner-documents'],
                    'placeholder' => 'select_an_option',
                    'required' => false,
                    'attr' => ['data-controller' => 'master-field', 'data-property-name' => 'pensionCertificateType'],
                ]
            )
            ->add(
                'oldStylePensionCertificateFirstSpreadUploaded',
                FileType::class,
                [
                    'label' => 'label.old_style_pension_certificate_first_spread',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePensionCertificateValidityRecordUploaded',
                FileType::class,
                [
                    'label' => 'label.old_style_pension_certificate_validity_record',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'newStylePensionCertificatePhotoAndValiditySpreadUploaded',
                FileType::class,
                [
                    'label' => 'label.new_style_pension_certificate_photo_and_validity_spread',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '5M'])],
                    'attr' => ['data-controller' => 'bootstrap-file-upload', 'data-action' => 'change->bootstrap-file-upload#change'],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )


            // Permanent address
            ->add('permanentAddressArea', TextType::class, ['label' => 'label.permanent_address_area'])
            ->add('permanentAddressMunicipality', TextType::class, ['label' => 'label.permanent_address_municipality'])
            ->add('permanentAddressCity', TextType::class, ['label' => 'label.permanent_address_city'])
            ->add(
                'permanentAddressPostCode',
                TextType::class,
                ['required' => false, 'label' => 'label.permanent_address_post_code']
            )
            ->add(
                'permanentAddressNeighbourhood',
                TextType::class,
                ['required' => false, 'label' => 'label.permanent_address_neighbourhood']
            )
            ->add('permanentAddressStreet', TextType::class, ['label' => 'label.permanent_address_street'])
            ->add('permanentAddressBlockNumber', NumberType::class, ['label' => 'label.permanent_address_block_number'])
            ->add(
                'permanentAddressEntrance',
                TextType::class,
                ['required' => false, 'label' => 'label.permanent_address_entrance']
            )
            ->add(
                'permanentAddressFloor',
                NumberType::class,
                ['required' => false, 'label' => 'label.permanent_address_floor']
            )
            ->add('permanentAddressApartment', TextType::class, ['label' => 'label.permanent_address_apartment'])


            // Current address
            ->add(
                'currentAddressSameAsPermanentAddress',
                ChoiceType::class,
                [
                    'label' => 'label.current_address_same_as_permanent_address',
                    'expanded' => true,
                    'multiple' => false,
                    'attr' => ['class' => 'mt-1', 'data-controller' => 'current-address'],
                    'label_attr' => ['class' => 'font-weight-normal p-0'],
                    'choices' => $this->choicesFromEnum(BooleanTypes::toTranslationKeys()),
                ]
            )
            ->add('currentAddressArea', TextType::class, [
                'required' => false,
                'label' => 'label.current_address_area',
                'label_attr' => ['class' => 'required'],
            ])
            ->add(
                'currentAddressMunicipality',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'label.current_address_municipality',
                    'label_attr' => ['class' => 'required'],
                ]
            )
            ->add('currentAddressCity', TextType::class, [
                'required' => false,
                'label' => 'label.current_address_city',
                'label_attr' => ['class' => 'required'],
            ])
            ->add(
                'currentAddressPostCode',
                TextType::class,
                ['required' => false, 'label' => 'label.current_address_post_code']
            )
            ->add(
                'currentAddressNeighbourhood',
                TextType::class,
                ['required' => false, 'label' => 'label.current_address_neighbourhood']
            )
            ->add(
                'currentAddressStreet',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'label.current_address_street',
                    'label_attr' => ['class' => 'required'],
                ]
            )
            ->add(
                'currentAddressBlockNumber',
                NumberType::class,
                [
                    'required' => false,
                    'label' => 'label.current_address_block_number',
                    'label_attr' => ['class' => 'required'],
                ]
            )
            ->add(
                'currentAddressEntrance',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'label.current_address_entrance',
                ]
            )
            ->add(
                'currentAddressFloor',
                NumberType::class,
                ['required' => false, 'label' => 'label.current_address_floor']
            )
            ->add(
                'currentAddressApartment',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'label.current_address_apartment',
                    'label_attr' => ['class' => 'required'],
                ]
            )
            ->add(
                'livingOnAddressFromDate',
                BootstrapDatepickerType::class,
                ['required' => true, 'label' => 'label.current_address_living_on_address_from_date']
            )


            // Work details
            ->add('companyName', TextType::class, ['label' => 'label.company_name'])
            ->add('companyPosition', TextType::class, ['label' => 'label.job_position'])
            ->add('workAddressArea', TextType::class, ['required' => true, 'label' => 'label.work_address_area'])
            ->add(
                'workAddressMunicipality',
                TextType::class,
                ['required' => true, 'label' => 'label.work_address_municipality']
            )
            ->add('workAddressCity', TextType::class, ['required' => true, 'label' => 'label.work_address_city'])
            ->add(
                'workAddressPostCode',
                TextType::class,
                ['required' => false, 'label' => 'label.work_address_post_code']
            )
            ->add(
                'workAddressNeighbourhood',
                TextType::class,
                ['required' => false, 'label' => 'label.work_address_neighbourhood']
            )
            ->add('workAddressStreet', TextType::class, ['label' => 'label.work_address_street'])
            ->add(
                'workAddressBlockNumber',
                NumberType::class,
                ['required' => true, 'label' => 'label.work_address_block_number']
            )
            ->add(
                'workAddressEntrance',
                TextType::class,
                ['required' => false, 'label' => 'label.work_address_entrance']
            )
            ->add('workAddressFloor', NumberType::class, ['required' => false, 'label' => 'label.work_address_floor'])
            ->add(
                'workAddressApartment',
                TextType::class,
                ['required' => false, 'label' => 'label.work_address_apartment']
            )
            ->add(
                'workExperienceTotal',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.work_experience_total']
            )
            ->add(
                'workExperienceCurrentEmployer',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.work_experience_current_employer']
            )
            ->add(
                'howSalaryIsReceived',
                ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(MethodsReceivingSalary::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.how_salary_is_received',
                    'attr' => ['data-controller' => 'master-field', 'data-property-name' => 'howSalaryIsReceived'],
                ]
            )
            ->add(
                'frequencyOfReceivingSalary',
                ChoiceType::class,
                [
                    'required' => false,
                    'choices' => $this->choicesFromEnum(FrequencyOfReceivingSalary::toTranslationKeys()),
                    'label' => 'label.frequency_of_receiving_salary',
                    'label_attr' => ['class' => 'required'],
                    'placeholder' => 'select_an_option',
                ]
            )
            ->add(
                'dayForReceivingSalary',
                NumberType::class,
                [
                    'html5' => true,
                    'scale' => 0,
                    'required' => false,
                    'label' => 'label.day_for_receiving_salary',
                    'label_attr' => ['class' => 'required'],
                ]
            )

            // Contact person info
            ->add('contactPersonFirstName', TextType::class, ['label' => 'label.first_name', 'help' => 'help.only_letters_and_dash_allowed'])
            ->add('contactPersonMiddleName', TextType::class, ['label' => 'label.middle_name', 'help' => 'help.only_letters_and_dash_allowed'])
            ->add('contactPersonLastName', TextType::class, ['label' => 'label.last_name', 'help' => 'help.only_letters_and_dash_allowed'])
            ->add(
                'contactPersonDateOfBirth',
                BootstrapDatepickerType::class,
                ['label' => 'label.contact_person_date_of_birth']
            )
            ->add(
                'contactPersonMobileTelephone',
                TelType::class,
                [
                    'label' => 'label.mobile_telephone',
                    'attr' => [
                        'style' => 'height: inherit;',
                        'pattern' => '\d+',
                    ],
                    'help' => 'help.phone_number',
                ]
            )
            ->add(
                'contactPersonRelationship',
                ChoiceType::class,
                [
                    'label' => 'label.contact_person_relationship',
                    'choices' => $this->choicesFromEnum(RelationshipTypes::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                ]
            )

            // Contacts
            ->add(
                'mobileTelephone',
                TelType::class,
                [
                    'label' => 'label.mobile_telephone',
                    'help' => 'help.phone_number',
                    'attr' => [
                        'style' => 'height: inherit;',
                        'pattern' => '\d+',
                    ],
                ]
            )
            ->add(
                'companyTelephone',
                TelType::class,
                [
                    'label' => 'label.company_telephone',
                    'help' => 'help.phone_number',
                    'attr' => [
                        'style' => 'height: inherit;',
                        'pattern' => '\d+',
                    ],
                ]
            )
            ->add('email', EmailType::class, ['label' => 'label.email'])


            // Expenses
            ->add(
                'householdExpensesFoodAndClothes',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_food_and_clothes']
            )
            ->add(
                'householdExpensesKids',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_kids']
            )
            ->add(
                'householdExpensesElectricityAndWater',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_electricity_and_water']
            )
            ->add(
                'householdExpensesHeating',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_heating']
            )
            ->add(
                'householdExpensesInternetAndTv',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_internet_and_tv']
            )
            ->add(
                'householdExpensesTelephone',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_telephone']
            )
            ->add(
                'householdExpensesRent',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_rent']
            )
            ->add(
                'householdExpensesTransport',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_transport']
            )
            ->add(
                'householdExpensesDrinksCigarettes',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_drinks_cigarettes']
            )
            ->add(
                'householdExpensesOtherCredits',
                TextType::class,
                ['label' => 'label.household_expenses_other_credits']
            )
            ->add(
                'householdExpensesOtherCreditsInstalmentAmount',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_other_credits_instalment_amount']
            )
            ->add(
                'householdExpensesOther',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_expenses_other']
            )


            // Incomes
            ->add(
                'monthlyIncome',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.monthly_income']
            )
            ->add('otherIncome', NumberType::class, ['html5' => true, 'scale' => 0, 'label' => 'label.other_income'])


            // Property info
            ->add(
                'hasProperty',
                ChoiceType::class,
                [
                    'label' => 'label.has_property',
                    'choices' => $this->choicesFromEnum(PropertyType::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                ]
            )
            ->add(
                'hasCar',
                ChoiceType::class,
                [
                    'label' => 'label.has_car',
                    'choices' => $this->choicesFromEnum(BooleanTypes::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                    'attr' => ['data-controller' => 'master-field', 'data-property-name' => 'hasCar'],
                ]
            )
            ->add(
                'numberOfCars',
                NumberType::class,
                [
                    'required' => false,
                    'label' => 'label.number_of_cars',
                    'html5' => true, 'scale' => 0,
                    'label_attr' => ['class' => 'required'],
                ]
            )
            ->add(
                'carYearOfManufacture',
                ChoiceType::class,
                [
                    'required' => false,
                    'translation_domain' => false,
                    'choice_translation_domain' => false,
                    'label' => 'label.car_year_of_manufacture',
                    'choices' => $this->years(1980, Carbon::today()->year),
                    'label_attr' => ['class' => 'required'],
                ]
            )


            // Additional info
            ->add(
                'industry',
                ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(ActivityAreas::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.industry',
                ]
            )
            ->add(
                'employmentType',
                ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(EmploymentTypes::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.employment_type',
                    'attr' => ['data-controller' => 'master-field', 'data-property-name' => 'employmentType'],
                ]
            )
            ->add(
                'expirationDateOfEmploymentContract',
                BootstrapDatepickerType::class,
                [
                    'required' => false,
                    'label' => 'label.expiration_date_of_employment_contract',
                    'label_attr' => ['class' => 'required'],
                ]
            )
            ->add(
                'typeOfWork',
                ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(PositionTypes::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.type_of_work',
                ]
            )
            ->add(
                'education',
                ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(EducationTypes::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.education',
                ]
            )
            ->add(
                'purposeOfCredit',
                ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(CreditPurposes::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.purpose_of_credit',
                ]
            )
            ->add(
                'typeOfHome',
                ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(HomeTypes::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.type_of_home',
                ]
            )
            ->add(
                'householdSize',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.household_size']
            )
            ->add(
                'familyStatus',
                ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(FamilyStatuses::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.family_status',
                ]
            )
            ->add(
                'foundOutAboutTheCompanyFrom',
                ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(InformationSources::toTranslationKeys()),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.found_out_about_the_company_from',
                ]
            )

            // Captcha
            ->add('captcha', CaptchaType::class, ['mapped' => false])

            ->add('consent', ChoiceType::class, [
                'mapped' => false,
                'expanded' => true,
                'required' => true,
                'label' => false,
                'attr' => ['class' => 'mt-1'],
                'label_attr' => ['class' => 'font-weight-normal p-0'],
            ])
        ;
    }

    private function choicesFromEnum(array $translationKeys): array
    {
        $choices = [];
        $flippedKeys = array_flip($translationKeys);
        $choices = array_merge($choices, $flippedKeys);

        return $choices;
    }

    private function years(?int $startYear = null, ?int $endYear = null): array
    {
        if (!$startYear) {
            $startYear = Carbon::today()->year;
        }

        if (!$endYear) {
            $endYear = Carbon::today()->year;
        }

        $keys = range($startYear, $endYear);

        return array_combine($keys, $keys);
    }

    private function months(): array
    {
        $keys = range(1, 12);

        return array_combine($keys, $keys);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => CreditLongApplication::class,
                'translation_domain' => 'form_long',
            ]
        );
    }
}
