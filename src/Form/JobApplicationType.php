<?php

namespace App\Form;

use App\Entity\JobApplication;
use App\Form\CustomFields\BootstrapFileUploadType;
use App\Form\CustomFields\CaptchaType;
use App\Form\CustomFields\TelephoneWithPrefixType;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobApplicationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', TextType::class, [
                'label' => 'label.city',
	            'required' => false
            ])
            ->add('fullName', TextType::class, [
                'label' => 'label.required.full_name',
            ])
            ->add('email', EmailType::class, [
                'label' => 'label.required.email',
            ])
            ->add('telephone', TelephoneWithPrefixType::class, [
                'label' => 'label.required.telephone',
	            'attr' => [
		            'pattern' => '\d+'
	            ],
	            'help' => 'help.only_numbers_are_allowed'
            ])
            ->add('cv', BootstrapFileUploadType::class, [
                'label' => 'label.required.upload_cv',
                'required' => false,
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'label.comment',
                'required' => false,
            ])
	        ->add('captcha', CaptchaType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => JobApplication::class,
            'translation_domain' => 'form',
        ]);
    }
}
