<?php

namespace App\Form;

use App\Entity\News;
use App\Form\CustomFields\BootstrapDatepickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VisibilityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', BootstrapDatepickerType::class, [
                'label' => 'label.start_date',
                'required' => false,
            ])
            ->add('endDate', BootstrapDatepickerType::class, [
                'label' => 'label.end_date',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
            'translation_domain' => 'form',
        ]);
    }
}
