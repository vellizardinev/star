<?php

namespace App\Form;

use App\Entity\Page;
use App\Enum\PageTypes;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [];
        $types = array_flip(PageTypes::toTranslationKeys());
        $choices = array_merge($choices, $types);

        $builder
            ->add('name', TextType::class, [
                'label' => 'label.name',
            ])
        ;

        if ($options['show_type']) {
            $builder->add('type', ChoiceType::class, [
                'expanded' => false,
                'multiple' => false,
                'label' => 'label.page_type',
                'mapped' => false,
                'choices' => $choices,
	            'data' => $options['show_type']
            ]);
        }

	    if ($options['show_path']) {
		    $builder->add('path', TextType::class, [
			    'label' => 'label.path',
		    ]);
	    }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
            'translation_domain' => 'form',
	        'show_type' => false,
	        'show_path' => true
        ]);
    }
}
