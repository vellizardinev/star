<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Office;
use App\Enum\OfficeTypes;
use App\Form\CustomFields\CKEditorType;
use App\Repository\CityRepository;
use App\Repository\OfficeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfficeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'label.name',
            ])
            ->add('address', CKEditorType::class, [
                'label' => 'label.address',
	            'label_attr' => ['class' => 'required']
            ])
            ->add('telephone', TextType::class, [
                'label' => 'label.telephone',
                'required' => false,
            ])
            ->add('latitude', NumberType::class, [
                'label' => 'label.latitude',
                'scale' => 7,
            ])
            ->add('longitude', NumberType::class, [
                'label' => 'label.longitude',
                'scale' => 7,
            ])
            ->add('city', EntityType::class, [
                'label' => 'label.city',
                'class' => City::class,
                'choice_label' => 'name',
	            'query_builder' => function (CityRepository $cityRepository) {
		            return $cityRepository->all();
	            },
            ])
	        ->add('workingTime', TextType::class, [
		        'label' => 'label.working_time',
	        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Office::class,
            'translation_domain' => 'form',
        ]);
    }
}
