<?php

namespace App\Form;

use App\Entity\LandingPage;
use App\Entity\Promotion;
use App\Form\CustomFields\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PromotionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'label.title',
            ])
            ->add('description', CKEditorType::class, [
                'label' => 'label.description',
	            'label_attr' => ['class' => 'required']
            ])
            ->add('featured', CheckboxType::class, [
                'label' => 'label.featured',
                'required' => false,
            ])
            ->add('showImage', CheckboxType::class, [
                'label' => 'label.show_image',
                'required' => false,
            ])
            ->add('landingPage', EntityType::class, [
                'class' => LandingPage::class,
                'label' => 'label.landing_page',
                'required' => false,
                'placeholder' => 'placeholder.select_landing_page'
            ])
        ;

	    if ($options['show_slug']) {
		    $builder
			    ->add('slug', TextType::class, [
				    'label' => 'label.slug',
			    ]);
	    }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Promotion::class,
            'translation_domain' => 'form',
	        'show_slug' => true
        ]);
    }
}
