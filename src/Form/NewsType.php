<?php

namespace App\Form;

use App\Entity\News;
use App\Enum\NewsCategories;
use App\Form\CustomFields\CKEditorType;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [];
        $categories = array_flip(NewsCategories::toTranslationKeys());
        $choices = array_merge($choices, $categories);

        $builder
            ->add('title', TextType::class, [
                'label' => 'label.title',
            ])
	        ->add('introduction', TextType::class, [
		        'label' => 'label.introduction',
		        'required' => false
	        ])
            ->add('category', ChoiceType::class, [
                'choices' => $choices,
                'label' => 'label.category',
            ])
            ->add('content', CKEditorType::class, [
                'label' => 'label.content',
            ])
            ->add('featured', CheckboxType::class, [
                'label' => 'label.featured',
                'required' => false,
            ])
	        ->add('showImage', CheckboxType::class, [
		        'label' => 'label.show_image',
		        'required' => false,
	        ])
        ;

	    if ($options['show_slug']) {
		    $builder
			    ->add('slug', TextType::class, [
				    'label' => 'label.slug',
			    ]);
	    }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
	        'translation_domain' => 'form',
	        'show_slug' => true
        ]);
    }
}
