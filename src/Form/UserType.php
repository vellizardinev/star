<?php

namespace App\Form;

use App\Entity\Permission\Group;
use App\Entity\User;
use App\Enum\Roles;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class UserType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('email', TextType::class, [
				'label' => 'label.email',
			])
			->add('active', CheckboxType::class, [
				'label' => 'label.active',
			])
			->add('password', UserPasswordType::class, [
				'mapped' => false,
				'label' => false
			])
			->add('profile', ProfileType::class, [
				'required' => false,
				'label' => false
			]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => User::class,
			'translation_domain' => 'form',
		]);
	}
}
