<?php

namespace App\Form;

use App\Entity\Message;
use App\Entity\MessageRecipient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Email;

class MessageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $builder
		    ->add('subject', TextType::class, [
			    'label' => 'label.subject',
		    ])
		    ->add('content', TextareaType::class, [
			    'label' => 'label.message',
		    ])
		    ->add('visible', CheckboxType::class, [
			    'label' => 'label.visible',
			    'required' => false
		    ])
            ->add('toAllCustomers', CheckboxType::class, [
                'label' => 'label.to_all_customers',
                'required' => false
            ])
            ->add('recipientEmails', TextareaType::class, [
                'mapped' => false,
                'data' => $this->extractEmailsString($options['data']),
                'help' => 'help.recipients',
                'required' => false,
                'label' => 'label.recipients',
                'constraints' => [
                    new All(['constraints' => new Email()])
                ]
            ])
        ;

        $builder->get('recipientEmails')
            ->addModelTransformer(new CallbackTransformer(
                function ($emailsAsArray) {
                    return implode(', ', $emailsAsArray);
                },
                function ($emailsAsString) {
                    $emails = explode(',', $emailsAsString);

                    $emails = array_map(
                        function ($email) {
                            return trim($email);
                        }, $emails);

                    return $emails;
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Message::class,
	        'translation_domain' => 'form',
        ]);
    }

    private function extractEmailsString(Message $data): array
    {
        return $data->getRecipients()->map(
            function (MessageRecipient $item) {
                return $item->getEmail();
            })->toArray();
    }
}
