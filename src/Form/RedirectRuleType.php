<?php

namespace App\Form;

use App\Entity\RedirectRule;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RedirectRuleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldUrl', TextType::class, [
                'label' => 'label.old_url',
            ])
            ->add('newUrl', TextType::class, [
                'label' => 'label.new_url',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RedirectRule::class,
            'translation_domain' => 'form'
        ]);
    }
}
