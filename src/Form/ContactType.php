<?php

namespace App\Form;

use App\Entity\Contact;
use App\Enum\ContactAbout;
use App\Enum\FeedbackStrategy;
use App\Form\CustomFields\CaptchaType;
use App\Form\CustomFields\TelephoneWithPrefixType;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $aboutChoices = [];
        $categories = array_flip(ContactAbout::toTranslationKeys());
        $aboutChoices = array_merge($aboutChoices, $categories);

        $feedbackChoices = [];
        $categories = array_flip(FeedbackStrategy::toTranslationKeys());
        $feedbackChoices = array_merge($feedbackChoices, $categories);

        $builder
            ->add('about', ChoiceType::class, [
                'label' => 'label.related_with',
                'choices' => $aboutChoices,
                'placeholder' => 'placeholder.please_select_an_option'
            ])
            ->add('firstName', TextType::class, [
                'label' => 'label.first_name',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'label.last_name',
            ])
            ->add('email', EmailType::class, [
                'label' => 'label.email',
            ])
            ->add('telephone', TelephoneWithPrefixType::class, [
                'label' => 'label.telephone',
	            'attr' => [
		            'pattern' => '\d+'
	            ],
	            'help' => 'help.only_numbers_are_allowed'
            ])
            ->add('address', TextareaType::class, [
                'label' => 'label.address',
                'required' => false,
            ])
            ->add('feedbackStrategy', ChoiceType::class, [
                'label' => 'label.feedback_strategy',
                'choices' => $feedbackChoices,
                'required' => false,
                'placeholder' => 'placeholder.please_select_an_option',
            ])
	        ->add('captcha', CaptchaType::class)
            ->add('comment', TextareaType::class, [
                'label' => 'label.comment',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'translation_domain' => 'form',
        ]);
    }
}
