<?php

namespace App\Form\OnlineApplication;

use App\Entity\OnlineApplication\OnlineApplication;
use App\Form\CreatesChoicesForEnum;
use App\Form\CustomFields\CaptchaType;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

class PersonalInfoDocumentsType extends AbstractType
{
    use CreatesChoicesForEnum;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var OnlineApplication $data */
        $data = $builder->getData();
        $credit = $data->getCreditDetails()->getProduct();
        $isPensioner = $credit === 'Pensioner';

        $age = $data->getUploadedDocuments()->age();

        $builder
            ->add('personalInfo', PersonalInfoType::class,
                [
                    'label' => false,
                    'constraints' => [new Valid()],
                ])
            ->add('uploadedDocuments', UploadedDocumentsType::class,
            [
                'label' => false,
                'is_pensioner' => $isPensioner,
                'age' => $age,
                'constraints' => [new Valid()],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => OnlineApplication::class,
                'translation_domain' => 'form_long',
            ]
        );
    }
}
