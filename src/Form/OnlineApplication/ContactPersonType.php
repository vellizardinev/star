<?php

namespace App\Form\OnlineApplication;

use App\Entity\OnlineApplication\ContactPerson;
use App\Enum\NomenclatureTypes;
use App\Enum\RelationshipTypes;
use App\Form\CreatesChoicesForEnum;
use App\Form\CustomFields\BootstrapDatepickerType;
use App\Form\CustomFields\CaptchaType;
use App\Repository\NomenclatureRepository;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactPersonType extends AbstractType
{
	private $nomenclatureRepository;

	public function __construct(NomenclatureRepository $nomenclatureRepository)
	{
		$this->nomenclatureRepository = $nomenclatureRepository;
	}

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contactPersonFirstName', TextType::class, ['label' => 'label.first_name', 'help' => 'help.only_letters_and_dash_allowed'])
            ->add('contactPersonMiddleName', TextType::class, ['label' => 'label.middle_name', 'help' => 'help.only_letters_and_dash_allowed'])
            ->add('contactPersonLastName', TextType::class, ['label' => 'label.last_name', 'help' => 'help.only_letters_and_dash_allowed'])
            ->add(
                'contactPersonDateOfBirth',
                BootstrapDatepickerType::class,
                ['label' => 'label.contact_person_date_of_birth']
            )
            ->add(
                'contactPersonMobileTelephone',
                TelType::class,
                [
                    'label' => 'label.mobile_telephone',
                    'attr' => [
                        'style' => 'height: inherit;',
                        'pattern' => '\d+',
                    ],
                    'help' => 'help.phone_number',
                ]
            )
            ->add(
                'contactPersonRelationship',
                ChoiceType::class,
                [
                    'label' => 'label.contact_person_relationship',
	                'choices' => $this->nomenclatureRepository->choices(NomenclatureTypes::PARTNER_TYPE),
                    'placeholder' => 'select_an_option',
                ]
            )
        ;

        $builder->get('contactPersonMobileTelephone')
            ->addModelTransformer(new CallbackTransformer(
                function ($telephoneWithPrefix) {
                    if (substr($telephoneWithPrefix, 0, 4 ) === "+380") {
                        return substr($telephoneWithPrefix, 4);
                    }

                    return $telephoneWithPrefix;
                },
                function ($telephone) {
                    return $telephone;
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => ContactPerson::class,
                'translation_domain' => 'form_long',
            ]
        );
    }
}
