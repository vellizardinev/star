<?php

namespace App\Form\OnlineApplication;

use App\Entity\OnlineApplication\AdditionalInfo;
use App\Enum\NomenclatureTypes;
use App\Form\CustomFields\CaptchaType;
use App\Repository\NomenclatureRepository;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdditionalInfoType extends AbstractType
{
	private $nomenclatureRepository;

	public function __construct(NomenclatureRepository $nomenclatureRepository)
	{
		$this->nomenclatureRepository = $nomenclatureRepository;
	}

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'purposeOfCredit',
                ChoiceType::class,
                [
	                'choices' => $this->nomenclatureRepository->choices(NomenclatureTypes::CREDIT_PURPOSES),
	                'placeholder' => 'select_an_option',
                    'label' => 'label.purpose_of_credit',
                ]
            )
	        ->add(
		        'householdSize',
		        ChoiceType::class,
		        [
			        'placeholder' => 'select_an_option',
			        'choices' => $this->nomenclatureRepository->familySizes(),
			        'label' => 'label.household_size',
		        ]
	        )
            ->add(
                'familyStatus',
                ChoiceType::class,
                [
	                'choices' => $this->nomenclatureRepository->choices(NomenclatureTypes::FAMILY_STATUS),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.family_status',
                ]
            )
            ->add(
                'foundOutAboutTheCompanyFrom',
                ChoiceType::class,
                [
	                'choices' => $this->nomenclatureRepository->choices(NomenclatureTypes::INFORMATION_SOURCES),
	                'placeholder' => 'select_an_option',
                    'label' => 'label.found_out_about_the_company_from',
                ]
            )
            ->add(
                'monthlyIncome',
                NumberType::class,
                ['html5' => true, 'scale' => 0, 'label' => 'label.monthly_income']
            )
            ->add('otherIncome', NumberType::class, ['html5' => true, 'scale' => 0, 'label' => 'label.other_income'])
            ->add(
                'iAmPep',
                CheckboxType::class,
                [
                    'label' => 'label.i_am_pep',
                    'required' => false,
                    'attr' => ['class' => 'mt-1'],
                    'label_attr' => ['class' => 'checkbox-custom'],
                ]
            )
            ->add(
                'iAmMilitaryPerson',
                CheckboxType::class,
                [
                    'label' => 'label.i_am_military_person',
                    'required' => false,
                    'attr' => ['class' => 'mt-1'],
                    'label_attr' => ['class' => 'checkbox-custom'],
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => AdditionalInfo::class,
                'translation_domain' => 'form_long',
            ]
        );
    }
}
