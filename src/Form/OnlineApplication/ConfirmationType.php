<?php

namespace App\Form\OnlineApplication;

use App\Entity\OnlineApplication\Confirmation;
use App\Form\CustomFields\CaptchaType;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfirmationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Confirmation::class,
                'translation_domain' => 'form_long',
            ]
        );
    }
}
