<?php

namespace App\Form\OnlineApplication;

use App\Entity\OnlineApplication\UploadedDocuments;
use App\Enum\PassportTypes;
use App\Enum\PensionCertificateTypes;
use App\Form\CreatesChoicesForEnum;
use App\Form\CustomFields\BootstrapFileUploadType;
use App\Validator\MinimumUploadSize;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class UploadedDocumentsType extends AbstractType
{
    use CreatesChoicesForEnum;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('passportType', ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(PassportTypes::toTranslationKeys()),
                    'label' => 'label.passport_type',
                    'placeholder' => 'select_an_option',
                    'attr' => ['data-controller' => 'master-field', 'data-property-name' => 'passportType'],
                ]
            )
            ->add(
                'individualTaxNumberUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.individual_tax_number',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M'])],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'selfieUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.selfie',
                    'required' => false,
                    'constraints' => [new File(['mimeTypes' => ['image/jpeg'], 'mimeTypesMessage' => 'online_application.allowed_file_types_jpeg_only', 'maxSize' => '8M'])],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.selfie',
                ]
            )
            ->add(
                'oldStylePassportFirstPageUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.old_style_passport_first_page',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize()
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePassportSecondPageUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.old_style_passport_second_page',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize(),
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePassportProofOfResidenceUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.old_style_passport_proof_of_residence',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize(),
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePassportSelfieWithPassportUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.old_style_passport_selfie_with_passport',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['application/pdf', 'application/x-pdf', 'image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize(),
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'newStylePassportPhotoPageUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.new_style_passport_photo_page',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['application/pdf', 'application/x-pdf', 'image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize(),
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'newStylePassportValidityPageUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.new_style_passport_validity_page',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize(),
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'newStylePassportRegistrationAppendixUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.new_style_passport_registration_appendix',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize(),
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'newStylePassportSelfieWithIdCardUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.new_style_passport_selfie_with_id_card',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['application/pdf', 'application/x-pdf', 'image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize(),
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add('pensionCertificateType', ChoiceType::class,
                [
                    'choices' => $this->choicesFromEnum(PensionCertificateTypes::toTranslationKeys()),
                    'label' => 'label.pensioner_certificate_type',
                    'placeholder' => 'select_an_option',
                    'required' => $options['is_pensioner'],
                    'attr' => ['data-controller' => 'master-field', 'data-property-name' => 'pensionCertificateType'],
                    'constraints' => $options['is_pensioner'] === true ? [new NotBlank()] : [],
                ]
            )
            ->add(
                'oldStylePensionCertificateFirstSpreadUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.old_style_pension_certificate_first_spread',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize(),
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'oldStylePensionCertificateValidityRecordUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.old_style_pension_certificate_validity_record',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize(),
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
            ->add(
                'newStylePensionCertificatePhotoAndValiditySpreadUploaded',
                BootstrapFileUploadType::class,
                [
                    'label' => 'label.new_style_pension_certificate_photo_and_validity_spread',
                    'required' => false,
                    'constraints' => [
                        new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                        new MinimumUploadSize(),
                    ],
                    'label_attr' => ['class' => 'required'],
                    'help' => 'help.allowed_file_types',
                ]
            )
        ;

        if ($options['age'] >= 25) {
            $builder
                ->add(
                    'oldStylePassportPhotoAt25Uploaded',
                    BootstrapFileUploadType::class,
                    [
                        'label' => 'label.old_style_passport_photo_at_25',
                        'required' => false,
                        'constraints' => [
                            new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                            new MinimumUploadSize(),
                        ],
                        'label_attr' => ['class' => 'required'],
                        'help' => 'help.allowed_file_types',
                    ]
                )
                ->add(
                    'oldStylePassportPastedPhotoAt25Uploaded',
                    BootstrapFileUploadType::class,
                    [
                        'label' => 'label.old_style_passport_pasted_photo_at_25',
                        'required' => false,
                        'constraints' => [
                            new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                            new MinimumUploadSize(),
                        ],
                        'label_attr' => ['class' => 'required'],
                        'help' => 'help.allowed_file_types',
                    ]
                );
        }

        if ($options['age'] >= 45) {
            $builder
                ->add(
                    'oldStylePassportPhotoAt45Uploaded',
                    BootstrapFileUploadType::class,
                    [
                        'label' => 'label.old_style_passport_photo_at_45_uploaded',
                        'required' => false,
                        'constraints' => [
                            new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                            new MinimumUploadSize(),
                        ],
                        'label_attr' => ['class' => 'required'],
                        'help' => 'help.allowed_file_types',
                    ]
                )
                ->add(
                    'oldStylePassportPastedPhotoAt45Uploaded',
                    BootstrapFileUploadType::class,
                    [
                        'label' => 'label.old_style_passport_pasted_photo_at_45',
                        'required' => false,
                        'constraints' => [
                            new File(['mimeTypes' => ['image/jpeg', 'image/png'], 'mimeTypesMessage' => 'online_application.allowed_file_types', 'maxSize' => '8M']),
                            new MinimumUploadSize(),
                        ],
                        'label_attr' => ['class' => 'required'],
                        'help' => 'help.allowed_file_types',
                    ]
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => UploadedDocuments::class,
                'translation_domain' => 'form_long',
                'is_pensioner' => false,
                'age' => 18,
            ]
        );
    }
}
