<?php

namespace App\Form\OnlineApplication;

use App\Entity\OnlineApplication\PersonalInfo;
use App\Form\CreatesChoicesForEnum;
use App\Form\CustomFields\BootstrapDatepickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonalInfoType extends AbstractType
{
    use CreatesChoicesForEnum;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idCardNumber', TextType::class, ['label' => 'label.id_card_number'])
            ->add('idCardIssuedAt', BootstrapDatepickerType::class, ['label' => 'label.id_card_issued_at'])
            ->add('idCardIssuedBy', TextType::class, ['label' => 'label.id_card_issued_by'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => PersonalInfo::class,
                'translation_domain' => 'form_long',
            ]
        );
    }
}
