<?php

namespace App\Form\OnlineApplication;

use App\Entity\OnlineApplication\Address;
use App\Enum\BooleanTypes;
use App\Form\CreatesChoicesForEnum;
use App\Form\CustomFields\CaptchaType;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    use CreatesChoicesForEnum;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('permanentAddressArea', TextType::class, ['label' => 'label.permanent_address_area'])
            ->add('permanentAddressMunicipality', TextType::class, ['label' => 'label.permanent_address_municipality'])
            ->add('permanentAddressCity', TextType::class, ['label' => 'label.permanent_address_city'])
            ->add(
                'permanentAddressPostCode',
                TextType::class,
                ['required' => false, 'label' => 'label.permanent_address_post_code']
            )
            ->add('permanentAddressStreet', TextType::class, ['label' => 'label.permanent_address_street'])
            ->add('permanentAddressBlockNumber', NumberType::class, ['label' => 'label.permanent_address_block_number'])
            ->add('permanentAddressApartment', TextType::class, ['label' => 'label.permanent_address_apartment'])

            ->add(
                'currentAddressSameAsPermanentAddress',
                ChoiceType::class,
                [
                    'label' => 'label.current_address_same_as_permanent_address',
                    'expanded' => true,
                    'multiple' => false,
                    'attr' => ['class' => 'mt-1', 'data-controller' => 'current-address'],
                    'choice_attr' => ['class' => ''],
                    'label_attr' => ['class' => 'radio-custom choice-type-label'],
                    'choices' => $this->choicesFromEnum(BooleanTypes::toTranslationKeys()),
                ]
            )
            ->add('currentAddressArea', TextType::class, [
                'required' => false,
                'label' => 'label.current_address_area',
                'label_attr' => ['class' => 'required'],
            ])
            ->add(
                'currentAddressMunicipality',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'label.current_address_municipality',
                    'label_attr' => ['class' => 'required'],
                ]
            )
            ->add('currentAddressCity', TextType::class, [
                'required' => false,
                'label' => 'label.current_address_city',
                'label_attr' => ['class' => 'required'],
            ])
            ->add(
                'currentAddressPostCode',
                TextType::class,
                ['required' => false, 'label' => 'label.current_address_post_code']
            )
            ->add(
                'currentAddressStreet',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'label.current_address_street',
                    'label_attr' => ['class' => 'required'],
                ]
            )
            ->add(
                'currentAddressBlockNumber',
                NumberType::class,
                [
                    'required' => false,
                    'label' => 'label.current_address_block_number',
                    'label_attr' => ['class' => 'required'],
                ]
            )
            ->add(
                'currentAddressApartment',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'label.current_address_apartment',
                    'label_attr' => ['class' => 'required'],
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Address::class,
                'translation_domain' => 'form_long',
            ]
        );
    }
}
