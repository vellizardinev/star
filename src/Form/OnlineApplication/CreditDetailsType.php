<?php

namespace App\Form\OnlineApplication;

use App\Entity\OnlineApplication\CreditDetails;
use App\Entity\OnlineApplication\UserPaymentInstrument;
use App\Enum\NomenclatureTypes;
use App\Repository\NomenclatureRepository;
use Carbon\Carbon;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreditDetailsType extends AbstractType
{
    private $nomenclatureRepository;

    public function __construct(NomenclatureRepository $nomenclatureRepository)
    {
        $this->nomenclatureRepository = $nomenclatureRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // Credit details
            ->add(
                'amount',
                HiddenType::class,
                ['attr' => ['data-controller' => 'credit-parameters-field', 'data-property-name' => 'amount']]
            )
            ->add(
                'product',
                HiddenType::class,
                ['attr' => ['data-controller' => 'credit-parameters-field', 'data-property-name' => 'product']]
            )
            ->add(
                'period',
                HiddenType::class,
                ['attr' => ['data-controller' => 'credit-parameters-field', 'data-property-name' => 'period']]
            )
            ->add(
                'instalmentAmount',
                HiddenType::class,
                ['attr' => ['data-controller' => 'credit-parameters-field', 'data-property-name' => 'payment']]
            )
            ->add(
                'creditPeriodId',
                HiddenType::class,
                ['attr' => ['data-controller' => 'credit-parameters-field', 'data-property-name' => 'creditPeriodId']]
            )
            ->add(
                'creditProductId',
                HiddenType::class,
                ['attr' => ['data-controller' => 'credit-parameters-field', 'data-property-name' => 'creditProductId']]
            )
            ->add(
                'howShouldMoneyBeReceived',
                ChoiceType::class,
                [
                    'choices' => $this->nomenclatureRepository->choices(NomenclatureTypes::METHODS_TO_RECEIVE_CREDIT),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.how_should_money_be_received',
                    'attr' => ['data-controller' => 'master-field', 'data-property-name' => 'receiveCreditMethod'],
                ]
            )
        ;

        if (count($options['availablePaymentInstruments']) > 0) {
            $builder->add('selectedPaymentInstrument', ChoiceType::class, [
                'required' => false,
                'label' => 'label.available_payment_instruments',
                'label_attr' => ['class' => 'required'],
                'placeholder' => 'select_payment_instrument_used_previously',
                'choices' => $this->createAvailablePaymentInstrumentChoices($options['availablePaymentInstruments']),
                'attr' => ['data-controller' => 'available-payment-instruments', 'data-property-name' => 'receiveCreditMethod'],
            ]);
        }
    }

    private function years(?int $startYear = null, ?int $endYear = null): array
    {
        if (!$startYear) {
            $startYear = Carbon::today()->year;
        }

        if (!$endYear) {
            $endYear = Carbon::today()->year;
        }

        $keys = range($startYear, $endYear);

        return array_combine($keys, $keys);
    }

    private function months(): array
    {
        $keys = range(1, 12);

        return array_combine($keys, $keys);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => CreditDetails::class,
                'translation_domain' => 'form_long',
                'availablePaymentInstruments' => [],
            ]
        );
    }

    /**
     * @param UserPaymentInstrument[] $availableInstruments
     * @return array
     */
    private function createAvailablePaymentInstrumentChoices(array $availableInstruments): array
    {
        $choices = [];

        foreach ($availableInstruments as $instrument) {
            $choices[$instrument->getInstrumentInfo()] = $instrument->getKycSessionId();
        }

        return $choices;
    }
}
