<?php

namespace App\Form\OnlineApplication;

use App\Entity\OnlineApplication\WorkDetails;
use App\Enum\NomenclatureTypes;
use App\Form\CustomFields\CaptchaType;
use App\Repository\NomenclatureRepository;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkDetailsType extends AbstractType
{
	private $nomenclatureRepository;

	public function __construct(NomenclatureRepository $nomenclatureRepository)
	{
		$this->nomenclatureRepository = $nomenclatureRepository;
	}

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', TextType::class, ['label' => 'label.company_name'])
            ->add('companyPosition', TextType::class, ['label' => 'label.job_position'])
            ->add(
                'companyTelephone',
                TelType::class,
                [
                    'label' => 'label.company_telephone',
                    'help' => 'help.phone_number',
                    'attr' => [
                        'style' => 'height: inherit;',
                        'pattern' => '\d+',
                    ],
                ]
            )
            ->add(
                'employmentType',
                ChoiceType::class,
                [
	                'choices' => $this->nomenclatureRepository->choices(NomenclatureTypes::EMPLOYMENT_TYPES),
                    'placeholder' => 'select_an_option',
                    'label' => 'label.employment_type',
                    'attr' => ['data-controller' => 'master-field', 'data-property-name' => 'employmentType'],
                ]
            )
        ;

        $builder->get('companyTelephone')
            ->addModelTransformer(new CallbackTransformer(
                function ($telephoneWithPrefix) {
                    if (substr($telephoneWithPrefix, 0, 4 ) === "+380") {
                        return substr($telephoneWithPrefix, 4);
                    }

                    return $telephoneWithPrefix;
                },
                function ($telephone) {
                    return $telephone;
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => WorkDetails::class,
                'translation_domain' => 'form_long',
            ]
        );
    }
}
