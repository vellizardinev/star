<?php

namespace App\Form\OnlineApplication;

use App\Entity\OnlineApplication\OnlineApplication;
use App\Form\CustomFields\CaptchaType;
use App\Repository\NomenclatureRepository;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

class WorkDetailsAdditionalInfoType extends AbstractType
{
	private $nomenclatureRepository;

	public function __construct(NomenclatureRepository $nomenclatureRepository)
	{
		$this->nomenclatureRepository = $nomenclatureRepository;
	}

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('workDetails', WorkDetailsType::class, ['label' => false, 'constraints' => [new Valid()]])
            ->add('additionalInfo', AdditionalInfoType::class, ['label' => false, 'constraints' => [new Valid()]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => OnlineApplication::class,
                'translation_domain' => 'form_long',
            ]
        );
    }
}
