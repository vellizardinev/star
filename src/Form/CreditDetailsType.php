<?php

namespace App\Form;

use App\Entity\Credit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreditDetailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('title', TextType::class, [
		        'label' => 'label.title',
		        'disabled' => true
	        ])
	        ->add('shortDescription', TextareaType::class, [
		        'label' => 'label.short_description',
		        'disabled' => true
	        ])
	        ->add('fullDescription', TextareaType::class, [
		        'label' => 'label.full_description',
		        'disabled' => true
	        ])
	        ->add('maxAmount', NumberType::class, [
		        'label' => 'label.max_amount',
		        'disabled' => true
	        ])
	        ->add('parametersDescription', TextareaType::class, [
		        'label' => 'label.parameters_description',
		        'disabled' => true
	        ])
	        ->add('requirements', TextareaType::class, [
		        'label' => 'label.requirements',
		        'disabled' => true
	        ])
	        ->add('paymentSchedule', TextType::class, [
		        'label' => 'label.payment_schedule',
		        'disabled' => true
	        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Credit::class,
            'translation_domain' => 'form',
        ]);
    }
}
