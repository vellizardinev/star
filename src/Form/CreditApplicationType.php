<?php

namespace App\Form;

use App\Entity\CreditShortApplication;
use App\Form\CustomFields\CaptchaType;
use App\Form\CustomFields\TelephoneWithPrefixType;
use App\Repository\CityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreditApplicationType extends AbstractType
{
    private $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'label.credit_application.name',
                    'label_attr' => ['class' =>  'pb-0'],
                ]
            )
            ->add(
                'telephone',
                TelephoneWithPrefixType::class,
                [
                    'label' => 'label.telephone',
                    'attr' => [
                        'pattern' => '\d+',
                    ],
                    'label_attr' => ['class' =>  'pb-0'],
                    'help' => 'help.only_numbers_are_allowed',
                ]
            )
            ->add('city', ChoiceType::class, [
                'placeholder' => 'please_select_your_city',
                'required' => true,
                'label' => 'label.city',
                'choices' => $this->getCities(),
                'multiple' => false,
                'expanded' => false,
            ])

            ->add(
                'amount',
                HiddenType::class,
                [
                    'required' => false,
                ]
            )
            ->add('captcha', CaptchaType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => CreditShortApplication::class,
                'translation_domain' => 'form',
            ]
        );
    }

    private function getCities(): array
    {
        $cities = $this->cityRepository->findBy([], ['name' => 'ASC']);

        $names = [];

        foreach ($cities as $city) {
            $names[] = $city->getName();
        }

        return array_combine($names, $names);
    }

}
