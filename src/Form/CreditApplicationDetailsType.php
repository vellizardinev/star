<?php

namespace App\Form;

use App\Entity\CreditShortApplication;
use App\Enum\CreditApplicationStatuses;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreditApplicationDetailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [];
        $statuses = array_flip(CreditApplicationStatuses::toTranslationKeys());
        $choices = array_merge($choices, $statuses);

        $builder
	        ->add('name', TextType::class, [
		        'label' => 'label.name',
	        ])
	        ->add('telephone', TextType::class, [
		        'label' => 'label.telephone',
	        ])
//	        ->add('birthdayDate', DateType::class, [
//		        'label' => 'label.birthday_date',
//		        'widget' => 'single_text',
//		        'disabled' => true
//	        ])
            ->add('amount', TextType::class, [
	            'label' => 'label.amount',
            ])
            ->add('status', ChoiceType::class, [
                'choices' => $choices,
                'label' => 'label.status'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CreditShortApplication::class,
            'translation_domain' => 'form',
        ]);
    }
}
