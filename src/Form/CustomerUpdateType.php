<?php

namespace App\Form;

use App\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

class CustomerUpdateType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('firstName', TextType::class, ['label' => 'label.first_name'])
			->add('middleName', TextType::class, ['label' => 'label.middle_name'])
			->add('lastName', TextType::class, ['label' => 'label.last_name'])
			->add(
				'personalIdentificationNumber',
				TextType::class,
				[
					'label' => 'label.personal_identification_number',
					'disabled' => true,
				]
			)
			->add('email', EmailType::class, ['label' => 'label.email'])
			->add(
				'telephone',
				TextType::class,
				[
					'label' => 'label.mobile_telephone',
					'attr' => [
						'pattern' => '\d+',
					],
				]
			);

		/** @var Customer $data */
		$data = $builder->getData();

		if ($data->getIsPhoneActive() === false) {
			$builder->add(
				'smsCode',
				TextType::class,
				[
					'mapped' => false,
					'required' => false,
					'label' => 'label.code',
					'constraints' => [
						new Regex(['pattern' => '/^[a-zA-Z\d]{8}$/']),
					],
				]
			);
		}
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			[
				'data_class' => Customer::class,
				'translation_domain' => 'form',
			]
		);
	}
}
