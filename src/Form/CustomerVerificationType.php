<?php

namespace App\Form;

use App\Form\CustomFields\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class CustomerVerificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', TextType::class, [
                'label' => 'label.code',
	            'attr' => ['placeholder' => 'placeholder.code'],
                'constraints' => [
                    new NotBlank(),
	                new Regex(['pattern' => '/^[\d\w]{8,8}+$/', 'message' => 'invalid_sms_code'])
                ]
            ])
            ->add('captcha', CaptchaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'form',
        ]);
    }
}
