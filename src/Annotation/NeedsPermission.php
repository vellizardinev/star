<?php

namespace App\Annotation;

use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class NeedsPermission
 * @Annotation
 * @Target({"CLASS", "METHOD"})
 * @Attributes({@Attribute("permission", type="string")})”
 */
class NeedsPermission
{
    /**
     * @var string
     */
    public $permission;
}