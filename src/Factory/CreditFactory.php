<?php

namespace App\Factory;

use App\Entity\Credit;
use App\Enum\PaymentSchedule;
use ReflectionException;

class CreditFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'title' => $this->faker->words(3, true),
                'shortDescription' => $this->faker->sentence,
                'fullDescription' => $this->faker->paragraphs(3, true),
                'requirements' => $this->faker->sentence,
                'paymentSchedule' => PaymentSchedule::getRandomValue(),
                'maxAmount' => $this->faker->numberBetween(1, 1000000),
	            'parametersDescription' => $this->faker->words(3, true),
	            'allowOnlineApplication' => true,
	            'slug' => $this->faker->slug
            ];
    }

    public function model()
    {
        return Credit::class;
    }
}