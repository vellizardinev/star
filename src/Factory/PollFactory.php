<?php

namespace App\Factory;

use App\Entity\Poll;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;

class PollFactory extends BaseFactory
{
    /**
     * @var PollQuestionFactory
     */
    private $questionFactory;

    /**
     * PollFactory constructor.
     * @param PollQuestionFactory $questionFactory
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(PollQuestionFactory $questionFactory, EntityManagerInterface $entityManager)
    {
        $this->questionFactory = $questionFactory;
        $this->entityManager = $entityManager;

        parent::__construct($this->entityManager);
    }

    /**
     * @return array
     */
    public function data()
    {
        return
            [
                'title' => $this->faker->sentence,
                'description' => $this->faker->paragraphs(1, true),
                'questions' => $this->questionFactory->make([], 5),
                'endDate' => Carbon::today()->subDays(10),
            ];
    }

    public function model()
    {
        return Poll::class;
    }
}