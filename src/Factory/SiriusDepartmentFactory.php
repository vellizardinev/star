<?php

namespace App\Factory;

use App\Entity\Department;
use ReflectionException;

class SiriusDepartmentFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'name' => $this->faker->words(2, true),
                'director' => $this->faker->name,
                'description' => $this->faker->paragraphs(2, true),
            ];
    }

    public function model()
    {
        return Department::class;
    }
}