<?php

namespace App\Factory;

use App\Entity\City;
use App\Entity\Office;
use App\Enum\OfficeTypes;

class OfficeFactory extends BaseFactory
{
    /**
     * @return array
     */
    public function data()
    {
        return
            [
                'name' => $this->faker->words(2, true),
                'city' => $this->getRandomEntity(City::class),
                'address' => $this->faker->address,
                'telephone' => $this->faker->phoneNumber,
                'latitude' => $this->faker->latitude,
                'longitude' => $this->faker->longitude,
				'workingTime' => "Понеділок (пн) - П'ятниця (пт): 9:30 - 18:30",
	            'type' => OfficeTypes::ICREDIT
            ];
    }

    public function model()
    {
        return Office::class;
    }
}