<?php


namespace App\Factory;


use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

abstract class BaseFactory implements ModelFactory
{
    /**
     * @var Generator
     */
    protected $faker;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var PropertyAccessorInterface
     */
    protected $propertyAccessor;

    /**
     * BaseFactory constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->faker = Factory::create();
        $this->entityManager = $entityManager;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * Creates and instance of a model class, populates it with attributes and persists to DB
     * @param array $attributes
     * @param null $count
     * @return mixed
     */
    public function create(array $attributes = [], $count = null)
    {
        $modelClass = $this->model();

        if ($count) {
            $objects = [];

            for ($i = 0; $i < $count; $i++) {
                $modelAttributes = $this->data();
                $modelAttributes = array_merge($modelAttributes, $attributes);

                $object = $this->buildModel(new $modelClass, $modelAttributes);

                $this->entityManager->persist($object);

                $objects[]= $object;
            }

            $this->entityManager->flush();

            return $objects;
        }

        $modelAttributes = $this->data();
        $modelAttributes = array_merge($modelAttributes, $attributes);

        $object = $this->buildModel(new $modelClass, $modelAttributes);

        $this->entityManager->persist($object);
        $this->entityManager->flush();

        return $object;
    }

    /**
     * Creates and instance of a model class, populates it with attributes
     * @param array $attributes
     * @param null $count
     * @return mixed
     */
    public function make(array $attributes = [], $count = null)
    {
        $modelClass = $this->model();

        if ($count) {
            $objects = [];

            for ($i = 0; $i < $count; $i++) {
                $modelAttributes = $this->data();
                $modelAttributes = array_merge($modelAttributes, $attributes);

                $object = $this->buildModel(new $modelClass, $modelAttributes);

                $objects[]= $object;
            }

            return $objects;
        }

        $modelAttributes = $this->data();
        $modelAttributes = array_merge($modelAttributes, $attributes);

        $object = $this->buildModel(new $modelClass, $modelAttributes);

        return $object;
    }

    public function createFromArray(array $attributes = [])
    {
        $modelClass = $this->model();
        $modelObject = new $modelClass;

        foreach ($attributes as $attributeName => $attributeValue) {
            if ($this->propertyAccessor->isWritable($modelObject, $attributeName)) {
                $this->propertyAccessor->setValue($modelObject, $attributeName, $attributeValue);
            }
        }

        $this->entityManager->persist($modelObject);
        $this->entityManager->flush();

        return $modelObject;
    }

    public function makeFromArray(array $attributes = [])
    {
        $modelClass = $this->model();
        $modelObject = new $modelClass;

        foreach ($attributes as $attributeName => $attributeValue) {
            if ($this->propertyAccessor->isWritable($modelObject, $attributeName)) {
                $this->propertyAccessor->setValue($modelObject, $attributeName, $attributeValue);
            }
        }

        return $modelObject;
    }

    protected function buildModel($model, array $attributes)
    {
        foreach ($attributes as $attributeName => $attributeValue) {
            $this->propertyAccessor->setValue($model, $attributeName, $attributeValue);
        }

        return $model;
    }

    protected function getRandomEntity($class)
    {
        $iDs = $this->entityManager->createQuery("select entity.id from $class entity")->getScalarResult();
        $iDs = array_map('current', $iDs);

        $entity = $this->entityManager->getRepository($class)->find($iDs[rand(0, count($iDs) - 1)]);

        return $entity;
    }
}