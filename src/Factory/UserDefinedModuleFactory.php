<?php

namespace App\Factory;

use App\Entity\UserDefinedModule;

class UserDefinedModuleFactory extends BaseFactory
{
    public function data()
    {
        return
            [
                'name' => $this->faker->word,
                'content' => $this->faker->paragraphs(3, true),
            ];
    }

    public function model()
    {
        return UserDefinedModule::class;
    }
}