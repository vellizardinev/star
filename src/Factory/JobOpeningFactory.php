<?php

namespace App\Factory;

use App\Entity\City;
use App\Entity\Department;
use App\Entity\JobOpening;
use App\Entity\Office;
use Carbon\Carbon;

class JobOpeningFactory extends BaseFactory
{
    /**
     * @return array
     */
    public function data()
    {
        return
            [
                'position' => $this->faker->words(3, true),
                'description' => $this->faker->paragraphs(2, true),
                'reference' => $this->faker->randomNumber(6),
                'department' => $this->getRandomEntity(Department::class),
                'office' => $this->getRandomEntity(Office::class),
                'city' => $this->getRandomEntity(City::class),
                'deadline' => Carbon::now()->addDays(30),
                'slug' => $this->faker->slug,
            ];
    }

    public function model()
    {
        return JobOpening::class;
    }
}