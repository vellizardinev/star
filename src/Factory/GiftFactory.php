<?php

namespace App\Factory;

use App\Entity\Gift;
use ReflectionException;

class GiftFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'name' => $this->faker->words(2, true),
                'code' => $this->faker->randomNumber(3),
                'price' => $this->faker->randomNumber(3),
            ];
    }

    public function model()
    {
        return Gift::class;
    }
}