<?php

namespace App\Factory;

use App\Entity\User;
use App\Enum\Roles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SiriusUserFactory extends BaseFactory
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;

        parent::__construct($entityManager);
    }

    public function data()
    {
        return
            [
                'email' => $this->faker->email,
                'password' => $this->passwordEncoder->encodePassword(new User(), '123456789'),
                'roles' => [Roles::USER],
                'active' => true,
            ];
    }

    public function model()
    {
        return User::class;
    }
}