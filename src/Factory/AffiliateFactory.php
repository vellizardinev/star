<?php

namespace App\Factory;

use App\Entity\Affiliate;

class AffiliateFactory extends BaseFactory
{
    /**
     * @return array
     */
    public function data()
    {
        return
            [
                'name' => $this->faker->company,
	            'contactInfo' => $this->faker->sentences(3, true)
            ];
    }

    public function model()
    {
        return Affiliate::class;
    }
}