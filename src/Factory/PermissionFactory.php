<?php

namespace App\Factory;

use App\Entity\Permission\Permission;

class PermissionFactory extends BaseFactory
{
    public function data()
    {
        return
            [
                'domain' => $this->faker->word,
                'action' => $this->faker->word,
            ];
    }

    public function model()
    {
        return Permission::class;
    }
}