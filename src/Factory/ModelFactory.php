<?php


namespace App\Factory;


interface ModelFactory
{
    public function data();

    public function model();

    public function create(array $attributes = [], $count = null);

    public function make(array $attributes = [], $count = null);
}