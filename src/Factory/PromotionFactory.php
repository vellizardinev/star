<?php

namespace App\Factory;

use App\Entity\Promotion;
use App\Repository\PromotionRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Promotion|Proxy createOne(array $attributes = [])
 * @method static Promotion[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Promotion|Proxy find($criteria)
 * @method static Promotion|Proxy findOrCreate(array $attributes)
 * @method static Promotion|Proxy first(string $sortedField = 'id')
 * @method static Promotion|Proxy last(string $sortedField = 'id')
 * @method static Promotion|Proxy random(array $attributes = [])
 * @method static Promotion|Proxy randomOrCreate(array $attributes = [])
 * @method static Promotion[]|Proxy[] all()
 * @method static Promotion[]|Proxy[] findBy(array $attributes)
 * @method static Promotion[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Promotion[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static PromotionRepository|RepositoryProxy repository()
 * @method Promotion|Proxy create($attributes = [])
 */
final class PromotionFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return
            [
                'title' => self::faker()->sentence,
                'description' => self::faker()->paragraphs(3, true),
                'featured' => false,
                'slug' => self::faker()->slug,
            ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this// ->afterInstantiate(function(Promotion $promotion) {})
            ;
    }

    protected static function getClass(): string
    {
        return Promotion::class;
    }
}
