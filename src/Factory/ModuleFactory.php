<?php

namespace App\Factory;

use App\Entity\Module;

class ModuleFactory extends BaseFactory
{
    public function data()
    {
        return
            [
                'name' => $this->faker->word,
            ];
    }

    public function model()
    {
        return Module::class;
    }
}