<?php

namespace App\Factory;

use App\Entity\ModuleItem;

class ModuleItemFactory extends BaseFactory
{
    public function data()
    {
        return
            [
                'name' => $this->faker->word,
            ];
    }

    public function model()
    {
        return ModuleItem::class;
    }
}