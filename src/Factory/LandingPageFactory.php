<?php

namespace App\Factory;

use App\Entity\LandingPage;

class LandingPageFactory extends BaseFactory
{
    public function data()
    {
        return
            [
                'name' => $this->faker->word,
                'path' => $this->faker->slug,
            ];
    }

    public function model()
    {
        return LandingPage::class;
    }
}