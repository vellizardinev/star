<?php

namespace App\Factory;

use App\Entity\PollAnswer;

class PollAnswerFactory extends BaseFactory
{
    /**
     * @return array
     */
    public function data()
    {
        $hasComment = $this->faker->boolean;

        return
            [
                'text' => $this->faker->sentence,
                'hasComment' => $hasComment,
                'commentHelpText' => $hasComment === true ? 'Please, explain here...' : null,
            ];
    }

    public function model()
    {
        return PollAnswer::class;
    }
}