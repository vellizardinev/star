<?php

namespace App\Factory;

use App\Entity\CareersPartner;
use ReflectionException;

class CareersPartnerFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'name' => $this->faker->company,
                'description' => $this->faker->paragraphs(3, true),
                'discount' => $this->faker->randomElement(['-5%', '-10%', '-20%', '-30%']),
            ];
    }

    public function model()
    {
        return CareersPartner::class;
    }
}