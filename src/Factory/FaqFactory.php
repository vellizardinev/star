<?php

namespace App\Factory;

use App\Entity\Faq;
use App\Repository\FaqRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Faq|Proxy createOne(array $attributes = [])
 * @method static Faq[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Faq|Proxy find($criteria)
 * @method static Faq|Proxy findOrCreate(array $attributes)
 * @method static Faq|Proxy first(string $sortedField = 'id')
 * @method static Faq|Proxy last(string $sortedField = 'id')
 * @method static Faq|Proxy random(array $attributes = [])
 * @method static Faq|Proxy randomOrCreate(array $attributes = [])
 * @method static Faq[]|Proxy[] all()
 * @method static Faq[]|Proxy[] findBy(array $attributes)
 * @method static Faq[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Faq[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static FaqRepository|RepositoryProxy repository()
 * @method Faq|Proxy create($attributes = [])
 */
final class FaqFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            'question' => self::faker()->sentence,
            'answer' => self::faker()->paragraph,
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            // ->afterInstantiate(function(Faq $faq) {})
        ;
    }

    protected static function getClass(): string
    {
        return Faq::class;
    }
}
