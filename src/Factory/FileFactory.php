<?php

namespace App\Factory;

use App\Entity\File;

class FileFactory extends BaseFactory
{
    public function data()
    {
        return
            [
                'name' => $this->faker->word,
                'mime' => 'jpeg',
                'size' => $this->faker->numberBetween(1000, 2000),
                'originalName' => $this->faker->word,
            ];
    }

    public function model()
    {
        return File::class;
    }
}