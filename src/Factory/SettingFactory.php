<?php

namespace App\Factory;

use App\Entity\Setting;

class SettingFactory extends BaseFactory
{
    public function data()
    {
        return
            [
                'name' => $this->faker->word,
                'uiLabel' => $this->faker->word,
                'value' => $this->faker->word,
                'readonly' => false,
            ];
    }

    public function model()
    {
        return Setting::class;
    }
}