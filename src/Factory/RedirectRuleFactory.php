<?php

namespace App\Factory;

use App\Entity\City;
use App\Entity\RedirectRule;
use ReflectionException;

class RedirectRuleFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'oldUrl' => $this->faker->word,
                'newUrl' => $this->faker->word,
            ];
    }

    public function model()
    {
        return RedirectRule::class;
    }
}