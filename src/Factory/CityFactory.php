<?php

namespace App\Factory;

use App\Entity\City;
use ReflectionException;

class CityFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'name' => $this->faker->city,
            ];
    }

    public function model()
    {
        return City::class;
    }
}