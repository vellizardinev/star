<?php

namespace App\Factory;

use App\Entity\Promotion;
use ReflectionException;

class SiriusPromotionFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'title' => $this->faker->sentence,
                'description' => $this->faker->paragraphs(3, true),
                'featured' => false,
	            'slug' => $this->faker->slug,
            ];
    }

    public function model()
    {
        return Promotion::class;
    }
}