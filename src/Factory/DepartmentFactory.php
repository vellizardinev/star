<?php

namespace App\Factory;

use App\Entity\Department;
use App\Repository\DepartmentRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Department|Proxy createOne(array $attributes = [])
 * @method static Department[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Department|Proxy find($criteria)
 * @method static Department|Proxy findOrCreate(array $attributes)
 * @method static Department|Proxy first(string $sortedField = 'id')
 * @method static Department|Proxy last(string $sortedField = 'id')
 * @method static Department|Proxy random(array $attributes = [])
 * @method static Department|Proxy randomOrCreate(array $attributes = [])
 * @method static Department[]|Proxy[] all()
 * @method static Department[]|Proxy[] findBy(array $attributes)
 * @method static Department[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Department[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static DepartmentRepository|RepositoryProxy repository()
 * @method Department|Proxy create($attributes = [])
 */
final class DepartmentFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return
            [
                'name' => self::faker()->words(2, true),
                'director' => self::faker()->name,
                'description' => self::faker()->paragraphs(2, true),
            ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            // ->afterInstantiate(function(Department $department) {})
        ;
    }

    protected static function getClass(): string
    {
        return Department::class;
    }
}
