<?php

namespace App\Factory;

use App\Entity\PollQuestion;
use App\Enum\PollQuestionType;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionException;

class PollQuestionFactory extends BaseFactory
{
    /**
     * @var PollAnswerFactory
     */
    private $answerFactory;

    /**
     * PollQuestionFactory constructor.
     * @param PollAnswerFactory $answerFactory
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(PollAnswerFactory $answerFactory, EntityManagerInterface $entityManager)
    {
        $this->answerFactory = $answerFactory;
        $this->entityManager = $entityManager;
        parent::__construct($entityManager);
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'text' => $this->faker->sentence,
                'type' => $this->faker->randomElement(PollQuestionType::toValueArray()),
                'answers' => $this->answerFactory->make([], 5),
            ];
    }

    public function model()
    {
        return PollQuestion::class;
    }
}