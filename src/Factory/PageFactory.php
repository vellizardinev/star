<?php

namespace App\Factory;

use App\Entity\Page;

class PageFactory extends BaseFactory
{
    public function data()
    {
        return
            [
                'name' => $this->faker->word,
                'path' => $this->faker->slug,
            ];
    }

    public function model()
    {
        return Page::class;
    }
}