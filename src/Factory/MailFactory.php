<?php


namespace App\Factory;


use App\Entity\Mail;

class MailFactory extends BaseFactory
{
    private $locales= ['en', 'bg'];

    public function data()
    {
        return [
            'type' => $this->faker->word,
            'subject' => $this->faker->sentence,
            'content' => $this->faker->paragraphs(2, true),
            'locale' => $this->faker->randomElement($this->locales),
        ];
    }

    public function model()
    {
        return Mail::class;
    }
}