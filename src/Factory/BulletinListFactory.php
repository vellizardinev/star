<?php

namespace App\Factory;

use App\Entity\BulletinList;
use ReflectionException;

class BulletinListFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'subscriberEmail' => $this->faker->email,
            ];
    }

    public function model()
    {
        return BulletinList::class;
    }
}