<?php

namespace App\Factory;

use App\Entity\Permission\Group;

class PermissionGroupFactory extends BaseFactory
{
    public function data()
    {
        return
            [
                'name' => $this->faker->word,
            ];
    }

    public function model()
    {
        return Group::class;
    }
}