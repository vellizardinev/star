<?php

namespace App\Factory;

use App\Entity\City;
use App\Entity\Credit;
use App\Entity\CreditShortApplication;
use App\Enum\CreditApplicationStatuses;
use Carbon\Carbon;
use ReflectionException;

class CreditApplicationFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'name' => $this->faker->name,
                'email' => $this->faker->email,
                'credit' => $this->getRandomEntity(Credit::class),
                'city' => $this->getRandomEntity(City::class)->getName(),
                'telephone' => '380' . $this->faker->randomNumber(6),
	            'amount' => $this->faker->randomFloat(2, 0, 12000),
	            'status' => CreditApplicationStatuses::UNPROCESSED,
                'level' => 'Seles_Partner',
                'landing' => null,
                'affiliate' => null,
                'birthdayDate' => Carbon::today()->subYears(rand(10, 30)),
                'personalIdentificationNumber' => rand(1000, 9999),
                'preferredCallDate' => Carbon::today()->addDays(rand(1, 5)),
            ];
    }

    public function model()
    {
        return CreditShortApplication::class;
    }
}