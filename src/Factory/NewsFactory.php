<?php

namespace App\Factory;

use App\Entity\News;
use App\Enum\NewsCategories;
use ReflectionException;

class NewsFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'title' => $this->faker->sentence,
                'introduction' => $this->faker->sentences(2, true),
                'content' => $this->faker->paragraphs(5, true),
                'category' => NewsCategories::NEWS
            ];
    }

    public function model()
    {
        return News::class;
    }
}