<?php

namespace App\Factory;

use App\Entity\Faq;
use ReflectionException;

class SiriusFaqFactory extends BaseFactory
{
    /**
     * @return array
     * @throws ReflectionException
     */
    public function data()
    {
        return
            [
                'question' => $this->faker->sentence,
                'answer' => $this->faker->paragraphs(3, true),
            ];
    }

    public function model()
    {
        return Faq::class;
    }
}