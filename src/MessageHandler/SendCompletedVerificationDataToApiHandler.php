<?php


namespace App\MessageHandler;


use App\Message\SendCompletedVerificationDataToApi;
use App\Service\MobileApi\KycService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SendCompletedVerificationDataToApiHandler implements MessageHandlerInterface
{
    private $kycService;

    public function __construct(KycService $kycService)
    {
        $this->kycService = $kycService;
    }

    public function __invoke(SendCompletedVerificationDataToApi $command)
    {
        $this->kycService->saveKycData(
            $command->getKycSessionId(),
            $command->getExternalToken(),
            $command->getPhone()
        );
    }
}
