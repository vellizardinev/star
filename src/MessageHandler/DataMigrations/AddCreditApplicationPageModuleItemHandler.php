<?php

namespace App\MessageHandler\DataMigrations;

use App\Entity\ModuleItem;
use App\Enum\ModuleItems;
use App\Factory\ModuleItemFactory;
use App\Message\DataMigrations\AddCreditApplicationPageModuleItem;
use App\Module\Parameter\Content;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddCreditApplicationPageModuleItemHandler implements MessageHandlerInterface
{
    private $moduleItemFactory;

    private $locales;

    public function __construct(ModuleItemFactory $moduleItemFactory, string $locales)
    {
        $this->moduleItemFactory = $moduleItemFactory;
	    $this->locales = explode('|', $locales);
    }

    public function __invoke(AddCreditApplicationPageModuleItem $message)
    {
	    /** @var ModuleItem $contentItem */
	    $contentItem = $this->moduleItemFactory->create(
		    [
			    'name' => ModuleItems::CONTENT,
			    'description' => ModuleItems::CONTENT . ' [Credit application page]',
		    ]
	    );

	    $contentData = new Content();

	    $contentItem->setData($this->createData($contentData));
    }

	protected function createData($parametersObject): string
    {
		$result = [];

		foreach ($this->locales as $locale) {
			$result[$locale] = clone $parametersObject;
		}

		return serialize($result);
	}
}
