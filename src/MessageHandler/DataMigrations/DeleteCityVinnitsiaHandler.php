<?php

namespace App\MessageHandler\DataMigrations;

use App\Entity\City;
use App\Entity\JobOpening;
use App\Entity\Office;
use App\Message\DataMigrations\DeleteCityVinnitsia;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteCityVinnitsiaHandler implements MessageHandlerInterface
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function __invoke(DeleteCityVinnitsia $message)
    {
        $nameToRemove = 'Вінница';

        if ($city = $this->manager->getRepository(City::class)->findOneBy(['name' => $nameToRemove])) {
            // Update offices related with the city
            $offices = $this->manager->getRepository(Office::class)->officesForCity($nameToRemove);

            if (!empty($offices)) {
                $correctCity = $this->manager->getRepository(City::class)->findOneBy(['name' => 'Вінниця']);

                foreach ($offices as $jobApplication) {
                    $jobApplication->setCity($correctCity);
                }

                $this->manager->flush();
            }

            // Update job openings related with the city
            $jobOpenings = $this->manager->getRepository(JobOpening::class)->jobOpeningForCity($nameToRemove);

            if (!empty($jobOpenings)) {
                $correctCity = $this->manager->getRepository(City::class)->findOneBy(['name' => 'Вінниця']);

                foreach ($jobOpenings as $jobApplication) {
                    $jobApplication->setCity($correctCity);
                }

                $this->manager->flush();
            }

            $this->manager->remove($city);
            $this->manager->flush();
        }
    }
}
