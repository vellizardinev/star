<?php

namespace App\MessageHandler\DataMigrations;

use App\Factory\CityFactory;
use App\Message\DataMigrations\UpdateCities;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateCitiesHandler implements MessageHandlerInterface
{
    private $cityFactory;

    public function __construct(CityFactory $cityFactory)
    {
        $this->cityFactory = $cityFactory;
    }

    public function __invoke(UpdateCities $message)
    {
        $this->cityFactory->create([
            'name' => 'Коростень',
        ]);
	    $this->cityFactory->create([
		    'name' => 'Стрий',
	    ]);
	    $this->cityFactory->create([
		    'name' => 'Харків',
	    ]);
    }
}
