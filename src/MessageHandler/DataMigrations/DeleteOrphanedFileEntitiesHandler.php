<?php

namespace App\MessageHandler\DataMigrations;

use App\Entity\CreditLongApplication;
use App\Entity\File;
use App\File\FileManager;
use App\Message\DataMigrations\DeleteOrphanedFileEntities;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteOrphanedFileEntitiesHandler implements MessageHandlerInterface
{
    private $manager;

    private $fileManager;

    private $logger;

    public function __construct(EntityManagerInterface $manager, FileManager $fileManager, LoggerInterface $logger)
    {
        $this->manager = $manager;
        $this->fileManager = $fileManager;
        $this->logger = $logger;
    }

    /**
     * @param DeleteOrphanedFileEntities $message
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function __invoke(DeleteOrphanedFileEntities $message)
    {
        $files = $this->manager->getRepository(File::class)->findAll();
        $toDelete = [];

        foreach ($files as $file) {
            $path = $this->fileManager->systemFilePath($file);

            if (!file_exists($path)) {
                if (!$this->manager->getRepository(CreditLongApplication::class)->fileIsAssociatedWithApplication(
                    $file
                )) {
                    $toDelete[] = $file;
                }
            }
        }

        foreach ($toDelete as $file) {
            $this->logger->info(
                sprintf(
                    'Deleting orphaned file entity with ID: %s and Name: %s',
                    $file->getId(),
                    $file->getOriginalName()
                )
            );

            $this->manager->remove($file);
        }

        $this->manager->flush();
    }
}
