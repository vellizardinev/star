<?php

namespace App\MessageHandler\DataMigrations;

use App\Entity\Mail;
use App\Message\DataMigrations\UpdateLongFormMails;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateLongFormMailsHandler implements MessageHandlerInterface
{
    private $manager;

    private $projectDir;

    private $locales = [];

    public function __construct(EntityManagerInterface $manager, string $projectDir, string $locales)
    {
        $this->manager = $manager;
        $this->projectDir = $projectDir;
        $this->locales = explode('|', $locales);
    }

    public function __invoke(UpdateLongFormMails $message)
    {
        foreach ($this->locales as $locale) {
            $dir = $this->projectDir . "/templates/mail/$locale/long_application.html.twig";
            $text = file_get_contents($dir);

            $mailObject = $this->manager
                ->getRepository(Mail::class)
                ->findOneBy(['type' => 'long_application', 'locale' => $locale]);

            $mailObject->setContent($text);
            $this->manager->flush();
        }
    }
}
