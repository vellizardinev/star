<?php

namespace App\MessageHandler\DataMigrations;

use App\Entity\ModuleItem;
use App\Enum\ModuleItems;
use App\Factory\ModuleItemFactory;
use App\Message\DataMigrations\AddOfficeListModuleItem;
use App\Module\Parameter\Content;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddOfficeListModuleItemHandler implements MessageHandlerInterface
{
    private $moduleItemFactory;

    private $locales;

    public function __construct(ModuleItemFactory $moduleItemFactory, string $locales)
    {
        $this->moduleItemFactory = $moduleItemFactory;
	    $this->locales = explode('|', $locales);
    }

    public function __invoke(AddOfficeListModuleItem $message)
    {
	    /** @var ModuleItem $contentItem */
	    $contentItem = $this->moduleItemFactory->create(
		    [
			    'name' => ModuleItems::CONTENT,
			    'description' => ModuleItems::CONTENT . ' [Offices list]',
		    ]
	    );

	    $contentData = new Content();
	    $contentData->content = file_get_contents(__DIR__ . '/page_data/offices_list.html');

	    $contentItem->setData($this->createData($contentData));
    }

	protected function createData($parametersObject)
	{
		$result = [];

		foreach ($this->locales as $locale) {
			$result[$locale] = clone $parametersObject;
		}

		return serialize($result);
	}
}
