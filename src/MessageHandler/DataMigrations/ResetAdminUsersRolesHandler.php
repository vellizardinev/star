<?php

namespace App\MessageHandler\DataMigrations;

use App\Entity\User;
use App\Enum\Roles;
use App\Message\DataMigrations\ResetAdminUsersRoles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ResetAdminUsersRolesHandler implements MessageHandlerInterface
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function __invoke(ResetAdminUsersRoles $message)
    {
        $users = $this->manager->getRepository(User::class)->findAll();

        foreach ($users as $user) {
            /** @var User $user */
            if ($user->hasRole(Roles::ADMINISTRATOR)) {
                $user->setRoles([Roles::ADMINISTRATOR]);
                continue;
            }

            $user->setRoles([Roles::USER]);
        }

        $this->manager->flush();
    }
}
