<?php

namespace App\MessageHandler\DataMigrations;

use App\Enum\CreditType;
use App\Enum\PaymentSchedule;
use App\Factory\CreditFactory;
use App\Message\DataMigrations\AddIWeekCredit;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddIWeekCreditHandler implements MessageHandlerInterface
{
    private $creditFactory;

    public function __construct(CreditFactory $creditFactory)
    {
        $this->creditFactory = $creditFactory;
    }

    public function __invoke(AddIWeekCredit $message)
    {
        $requirements = <<<TAG
<ul><li>Потрібен паспорт та ІПН</li><li>Від 2 000 до 5 000 грн.</li><li>Від 8 до 12 внески</li><li>Погашення: Раз на тиждень</li></ul>
TAG;
        $this->creditFactory->create([
            'type' => CreditType::PERSONAL,
            'title' => 'iWeek',
            'shortDescription' => 'Один з найшвидших з існуючих в Україні кредитів готівкою для фізичних осіб і СПД.',
            'fullDescription' => file_get_contents(__DIR__ . '/credit_descriptions/i_week.txt'),
            'paymentSchedule' => PaymentSchedule::WEEKLY,
            'requirements' => $requirements,
            'slug' => 'iweek',
            'maxAmount' => 5000,
            'parametersDescription' => $requirements,
            'allowOnlineApplication' => true,
            'defaultAmount' => 2000.00
        ]);
    }
}
