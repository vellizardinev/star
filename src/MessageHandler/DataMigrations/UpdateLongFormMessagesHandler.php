<?php

namespace App\MessageHandler\DataMigrations;

use App\Message\DataMigrations\UpdateLongFormMessages;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateLongFormMessagesHandler implements MessageHandlerInterface
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param UpdateLongFormMessages $message
     * @throws DBALException
     */
    public function __invoke(UpdateLongFormMessages $message)
    {
        $connection = $this->manager->getConnection();
        $sql = 'select file_id from credit_long_application_files';
        $statement = $connection->prepare($sql);
        $statement->execute();

        $fileIDs = $statement->fetchAll();
        $fileIDs = array_map(
            function ($item) {
                return intval($item['file_id']);
            },
            $fileIDs
        );

        $sql = 'update file set dtype=\'document\' where id in (?)';
        $connection->executeQuery($sql, [$fileIDs], [Connection::PARAM_INT_ARRAY]);
    }
}
