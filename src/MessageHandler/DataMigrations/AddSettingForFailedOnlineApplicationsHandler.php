<?php

namespace App\MessageHandler\DataMigrations;

use App\Enum\Settings;
use App\Factory\SettingFactory;
use App\Message\DataMigrations\AddSettingForFailedOnlineApplications;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddSettingForFailedOnlineApplicationsHandler implements MessageHandlerInterface
{
    private $factory;

    public function __construct(SettingFactory $factory)
    {
        $this->factory = $factory;
    }

    public function __invoke(AddSettingForFailedOnlineApplications $message)
    {
        $this->factory->create([
            'name' => Settings::FAILED_LONG_APPLICATIONS_EMAIL,
            'uiLabel' => 'Email for sending online credit applications that failed to be sent to SmartIT API.',
            'value' => 'cd@icredit.ua',
        ]);
    }
}
