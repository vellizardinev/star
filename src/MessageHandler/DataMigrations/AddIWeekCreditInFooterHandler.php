<?php

namespace App\MessageHandler\DataMigrations;

use App\Entity\Credit;
use App\Entity\ModuleItem;
use App\Enum\ModuleItems;
use App\Message\DataMigrations\AddIWeekCreditInFooter;
use App\Module\Parameter\Footer;
use App\Module\Parameter\Link;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Routing\RouterInterface;

final class AddIWeekCreditInFooterHandler implements MessageHandlerInterface
{
    private $manager;

	private $router;

    public function __construct(EntityManagerInterface $manager, RouterInterface $router)
    {
        $this->manager = $manager;
        $this->router = $router;
    }

    public function __invoke(AddIWeekCreditInFooter $message)
    {
	    $credit = $this->manager->getRepository(Credit::class)->findOneBy(['title' => 'iWeek']);
	    $footerModuleItem = $this->manager->getRepository(ModuleItem::class)->findOneBy(['name' => ModuleItems::FOOTER]);

	    /** @var Footer $footerData */
	    $footerData = unserialize($footerModuleItem->getData());

	    $updatedFooterData = [];

	    $iWeekLink = new Link(
		    $credit->getTitle(),
		    $this->router->generate('credits.show', ['slug' => $credit->getSlug(), '_locale' => 'ua'])
	    );

	    foreach ($footerData as $locale => $data){
		    $updatedFooterData[$locale] = $data;
		    $updatedFooterData[$locale]->addSecondColumnLink($iWeekLink);
	    }

	    $footerModuleItem->setData(serialize($updatedFooterData));

	    $this->manager->flush();
    }
}
