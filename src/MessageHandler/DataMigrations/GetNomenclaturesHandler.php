<?php

namespace App\MessageHandler\DataMigrations;

use App\Message\DataMigrations\DeleteThankYouPage;
use App\Message\DataMigrations\GetNomenclatures;
use App\Service\MobileApi\Nomenclatures;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

final class GetNomenclaturesHandler implements MessageHandlerInterface
{
    private $nomenclatures;

    public function __construct(Nomenclatures $nomenclatures)
    {
        $this->nomenclatures = $nomenclatures;
    }

    /**
     * @param GetNomenclatures $message
     * @throws ClientExceptionInterface
     * @throws NonUniqueResultException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(GetNomenclatures $message)
    {
        $this->nomenclatures->update();
    }
}
