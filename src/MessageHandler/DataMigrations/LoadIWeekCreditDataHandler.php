<?php

namespace App\MessageHandler\DataMigrations;

use App\Entity\Credit;
use App\Entity\CreditData;
use App\Message\DataMigrations\LoadIWeekCreditData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

final class LoadIWeekCreditDataHandler implements MessageHandlerInterface
{
    private $manager;

    private $propertyAccessor;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    public function __invoke(LoadIWeekCreditData $message)
    {
        $this->loadDataFromDir('iWeek');
    }

    private function loadDataFromDir(string $directory)
    {
        $credit = $this->manager->getRepository(Credit::class)->findOneBy(['title' => $directory]);

        $fileNames = array_slice(scandir(__DIR__ . '/credit_data' . DIRECTORY_SEPARATOR . $directory), 2);

        $this->loadAmountsAndPeriods(
            __DIR__ . '/credit_data' . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $fileNames[0],
            $credit
        );

        foreach ($fileNames as $fileName) {
            $filePath = __DIR__ . '/credit_data' . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $fileName;
            $this->loadDataFromFile($fileName, $filePath, $credit);
        }
    }

    private function loadAmountsAndPeriods(string $filePath, Credit $credit)
    {
        $handle = fopen($filePath, "r");

        $periods = (fgetcsv($handle, 0, ';'));
        array_shift($periods);

        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            foreach ($periods as $period) {
                $creditData = (new CreditData)
                    ->setPeriods($period)
                    ->setAmount($data[0])
                    ->setCredit($credit)
                    ->setPaymentSchedule($credit->getPaymentSchedule());
                $this->manager->persist($creditData);
            }
        }

        fclose($handle);

        $this->manager->flush();
    }

    private function loadDataFromFile(string $fileName, string $filePath, Credit $credit)
    {
        $parts = explode('-', $fileName);
        $property = array_shift($parts);

        $handle = fopen($filePath, "r");

        $periods = fgetcsv($handle, 0, ';');
        array_shift($periods);

        while (($data = fgetcsv($handle, 0, ';')) !== false) {
            $index = 1;

            foreach ($periods as $period) {
                $creditData = $this->manager->getRepository(CreditData::class)->findOneBy(
                    [
                        'credit' => $credit,
                        'amount' => $data[0],
                        'periods' => $period,
                    ]
                );

                $this->propertyAccessor->setValue(
                    $creditData,
                    $property,
                    $this->normalizeValue($data[$index])
                );

                $index++;
            }
        }

        $this->manager->flush();

        fclose($handle);
    }

    private function normalizeValue(string $value)
    {
        $value = str_replace(',', '.', $value);
        $value = ((float)$value);

        if ($value === 0.00) {
            $value = null;
        }

        return $value;
    }
}
