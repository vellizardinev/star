<?php

namespace App\MessageHandler\DataMigrations;

use App\Entity\User;
use App\Message\DataMigrations\UpdateAdminPassword;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class UpdateAdminPasswordHandler implements MessageHandlerInterface
{
    private $manager;

    private $userPasswordEncoder;

    private $adminPassword;

    public function __construct(EntityManagerInterface $manager, UserPasswordEncoderInterface $userPasswordEncoder, string $adminPassword)
    {
        $this->manager = $manager;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->adminPassword = $adminPassword;
    }

    public function __invoke(UpdateAdminPassword $message)
    {
        if ($admin = $this->manager->getRepository(User::class)->findOneBy(['email' => 'admin@siriussoftware.bg'])) {
            $admin->setPassword($this->userPasswordEncoder->encodePassword(new User(), $this->adminPassword),);

            $this->manager->flush();
        }
    }
}
