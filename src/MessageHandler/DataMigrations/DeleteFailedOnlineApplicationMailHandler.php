<?php

namespace App\MessageHandler\DataMigrations;

use App\Entity\Mail;
use App\Message\DataMigrations\DeleteFailedOnlineApplicationMail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteFailedOnlineApplicationMailHandler implements MessageHandlerInterface
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function __invoke(DeleteFailedOnlineApplicationMail $message)
    {
        $templates = $this->manager
            ->getRepository(Mail::class)
            ->findBy([
                'type' => 'failed_online_application'
            ]);

        foreach ($templates as $template) {
            $this->manager->remove($template);
        }

        $this->manager->flush();
    }
}
