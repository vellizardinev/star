<?php

namespace App\MessageHandler\DataMigrations;

use App\Message\DataMigrations\GetCreditDataFromApi;
use App\Service\MobileApi\CreditProducts;
use App\Service\MobileApi\UpdateCreditData;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

final class GetCreditDataFromApiHandler implements MessageHandlerInterface
{
    private $creditProductsService;

    private $manager;

    private $updateCreditDataService;

    public function __construct(CreditProducts $creditProductsService, EntityManagerInterface $manager, UpdateCreditData $updateCreditDataService)
    {
        $this->creditProductsService = $creditProductsService;
        $this->manager = $manager;
        $this->updateCreditDataService = $updateCreditDataService;
    }

    /**
     * @param GetCreditDataFromApi $message
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function __invoke(GetCreditDataFromApi $message)
    {
        $creditProducts = $this->creditProductsService->creditProducts();
        $this->updateCreditDataService->update($creditProducts);
    }
}
