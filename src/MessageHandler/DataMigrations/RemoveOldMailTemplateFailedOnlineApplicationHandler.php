<?php


namespace App\MessageHandler\DataMigrations;


use App\Entity\Mail;
use App\Message\DataMigrations\RemoveOldMailTemplateFailedOnlineApplication;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class RemoveOldMailTemplateFailedOnlineApplicationHandler implements MessageHandlerInterface
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function __invoke(RemoveOldMailTemplateFailedOnlineApplication $message)
    {
        $templates = $this->manager
            ->getRepository(Mail::class)
            ->findBy([
                'type' => 'failed_online_application'
            ]);

        foreach ($templates as $template) {
            $this->manager->remove($template);
        }

        $this->manager->flush();
    }
}