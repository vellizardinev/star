<?php


namespace App\MessageHandler;


use App\Entity\JobOpening;
use App\Entity\Office;
use App\Message\DeleteOffice;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class DeleteOfficeHandler implements MessageHandlerInterface
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function __invoke(DeleteOffice $command)
    {
        $office = $this->manager->getRepository(Office::class)->find($command->getOfficeId());

        $jobOpenings = $this->manager
            ->getRepository(JobOpening::class)
            ->findBy(['office' => $office]);

        foreach ($jobOpenings as $jobOpening) {
            $this->manager->remove($jobOpening);
        }

        $this->manager->remove($office);
        $this->manager->flush();
    }
}