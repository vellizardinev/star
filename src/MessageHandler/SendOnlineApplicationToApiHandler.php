<?php


namespace App\MessageHandler;


use App\Entity\OnlineApplication\OnlineApplication;
use App\Message\SendOnlineApplicationToApi;
use App\Service\MobileApi\SaveProposalFullData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SendOnlineApplicationToApiHandler implements MessageHandlerInterface
{
    private $manager;

    private $saveProposalService;

    public function __construct(EntityManagerInterface $manager, SaveProposalFullData $saveProposalService)
    {
        $this->manager = $manager;
        $this->saveProposalService = $saveProposalService;
    }

    /**
     * @param SendOnlineApplicationToApi $command
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(SendOnlineApplicationToApi $command)
    {
        $application = $this->manager
            ->getRepository(OnlineApplication::class)
            ->find($command->getApplicationId());

        $this->saveProposalService->saveProposal($application);
    }
}