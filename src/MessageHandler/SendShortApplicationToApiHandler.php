<?php


namespace App\MessageHandler;


use App\Entity\CreditShortApplication;
use App\Message\SendShortApplicationToApi;
use App\Service\MobileApi\SavePPZShort;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SendShortApplicationToApiHandler implements MessageHandlerInterface
{
    private $manager;

    private $savePPZShortService;

    public function __construct(EntityManagerInterface $manager, SavePPZShort $savePPZShortService)
    {
        $this->manager = $manager;
        $this->savePPZShortService = $savePPZShortService;
    }

    /**
     * @param SendShortApplicationToApi $command
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(SendShortApplicationToApi $command)
    {
        $application = $this->manager
            ->getRepository(CreditShortApplication::class)
            ->find($command->getApplicationId());

        $success = $this->savePPZShortService->saveShortApplication($application);

        if ($success) {
            $application->setSentToErp(true);
            $this->manager->flush();
        }
    }
}