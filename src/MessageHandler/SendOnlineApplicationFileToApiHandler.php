<?php


namespace App\MessageHandler;


use App\Entity\Document;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Message\SendOnlineApplicationFileToApi;
use App\Service\MobileApi\UploadFile;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SendOnlineApplicationFileToApiHandler implements MessageHandlerInterface
{
    private $manager;

    private $uploadFileService;

    public function __construct(EntityManagerInterface $manager, UploadFile $uploadFileService)
    {
        $this->manager = $manager;
        $this->uploadFileService = $uploadFileService;
    }

    /**
     * @param SendOnlineApplicationFileToApi $message
     */
    public function __invoke(SendOnlineApplicationFileToApi $message)
    {
        $document = $this->manager->getRepository(Document::class)->find($message->getDocumentId());
        $application = $this->manager->getRepository(OnlineApplication::class)->find($message->getApplicationId());
        $type = $message->getType();

        /** @var $application OnlineApplication */
        $this->uploadFileService->uploadAsBase64String($document, $application->getErpId(), $type);
    }
}