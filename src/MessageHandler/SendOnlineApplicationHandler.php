<?php


namespace App\MessageHandler;


use App\Entity\OnlineApplication\OnlineApplication;
use App\Entity\Setting;
use App\Enum\Settings;
use App\File\FileManager;
use App\Mail\LongApplication;
use App\Mail\MailService;
use App\Message\SendOnlineApplication;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendOnlineApplicationHandler implements MessageHandlerInterface
{
    private $manager;

    private $translator;

    private $mailService;

    private $fileManager;

    public function __construct(EntityManagerInterface $manager, TranslatorInterface $translator, MailService $mailService, FileManager $fileManager)
    {
        $this->manager = $manager;
        $this->translator = $translator;
        $this->mailService = $mailService;
        $this->fileManager = $fileManager;
    }

    /**
     * @param SendOnlineApplication $command
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function __invoke(SendOnlineApplication $command)
    {
        $application = $this->manager
            ->getRepository(OnlineApplication::class)
            ->find($command->getApplicationId());

        $array = $application->toArray();

        $data = [];

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (array_key_exists('name', $value)) {
                    $translatedKey = $this->translator->trans($key, [], 'long_application');
                    $data[$translatedKey] = $value['name'];
                    continue;
                }

                continue;
            }

            $translatedKey = $this->translator->trans($key, [], 'long_application');

            if (is_bool($value)) {
                $data[$translatedKey] = $this->normalizeBoolean($value);
            } else {
                $data[$translatedKey] = $value;
            }
        }

        ksort($data);

        $to = $this->manager
            ->getRepository(Setting::class)
            ->get(Settings::LONG_APPLICATIONS_EMAIL)
            ->getValue();

        $filePaths = $this->getAttachments($application, $this->fileManager);

        $this->mailService
            ->to($to)
            ->locale('ua')
            ->attachments($filePaths)
            ->send(new LongApplication($data));
    }
    
    private function normalizeBoolean(bool $value): string
    {
        if ($value) {
            return $this->translator->trans('value.true', [], 'text');
        } else {
            return $this->translator->trans('value.false', [], 'text');
        }
    }

    private function getAttachments(OnlineApplication $application, FileManager $fileManager): array
    {
        $result = [];

        if ($file = $application->getUploadedDocuments()->getIndividualTaxNumber()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getOldStylePassportFirstPage()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getOldStylePassportSecondPage()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getOldStylePassportPhotoAt25()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getOldStylePassportPastedPhotoAt25()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getOldStylePassportPhotoAt45()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getOldStylePassportPastedPhotoAt45()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getOldStylePassportProofOfResidence()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getOldStylePassportSelfieWithPassport()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getNewStylePassportPhotoPage()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getNewStylePassportValidityPage()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getNewStylePassportRegistrationAppendix()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getNewStylePassportSelfieWithIdCard()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getOldStylePensionCertificateFirstSpread()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getOldStylePensionCertificateValidityRecord()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getUploadedDocuments()->getNewStylePensionCertificatePhotoAndValiditySpread()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        return $result;
    }
}