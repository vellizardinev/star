<?php


namespace App\MessageHandler;


use App\Message\ResetFeaturedPromotions;
use App\Repository\PromotionRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ResetFeaturedPromotionsHandler implements MessageHandlerInterface
{
    /**
     * @var PromotionRepository
     */
    private $repository;

    /**
     * ResetFeaturedNewsHandler constructor.
     * @param PromotionRepository $repository
     */
    public function __construct(PromotionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(ResetFeaturedPromotions $resetFeaturedPromotions)
    {
        $newFeatured = $this->repository->find($resetFeaturedPromotions->getPromotionId());

        $this->repository->resetFeatured($newFeatured);
    }
}