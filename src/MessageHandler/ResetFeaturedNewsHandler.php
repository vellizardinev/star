<?php


namespace App\MessageHandler;


use App\Message\ResetFeaturedNews;
use App\Repository\NewsRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ResetFeaturedNewsHandler implements MessageHandlerInterface
{
    /**
     * @var NewsRepository
     */
    private $repository;

    /**
     * ResetFeaturedNewsHandler constructor.
     * @param NewsRepository $repository
     */
    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(ResetFeaturedNews $resetFeaturedNews)
    {
        $newFeatured = $this->repository->find($resetFeaturedNews->getNewsId());

        $this->repository->resetFeatured($newFeatured);
    }
}