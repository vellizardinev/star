<?php


namespace App\MessageHandler;


use App\Entity\CreditLongApplication;
use App\Entity\Setting;
use App\Enum\Settings;
use App\File\FileManager;
use App\Mail\LongApplication;
use App\Mail\MailService;
use App\Message\SendLongCreditApplication;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class SendLongCreditApplicationHandler implements MessageHandlerInterface
{
    private $manager;

    private $serializer;

    private $translator;

    private $mailService;

    private $fileManager;

    public function __construct(EntityManagerInterface $manager, SerializerInterface $serializer, TranslatorInterface $translator, MailService $mailService, FileManager $fileManager)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
        $this->translator = $translator;
        $this->mailService = $mailService;
        $this->fileManager = $fileManager;
    }

    /**
     * @param SendLongCreditApplication $command
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __invoke(SendLongCreditApplication $command)
    {
        $application = $this->manager
            ->getRepository(CreditLongApplication::class)
            ->find($command->getApplicationId());

        $array = json_decode($this->serializer->serialize($application, 'json'), true);

        $data = [];

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (array_key_exists('name', $value)) {
                    $translatedKey = $this->translator->trans($key, [], 'long_application');
                    $data[$translatedKey] = $value['name'];
                    continue;
                }

                continue;
            }

            $translatedKey = $this->translator->trans($key, [], 'long_application');

            if (is_bool($value)) {
                $data[$translatedKey] = $this->normalizeBoolean($value);
            } else {
                $data[$translatedKey] = $value;
            }
        }

        ksort($data);

        $to = $this->manager
            ->getRepository(Setting::class)
            ->get(Settings::LONG_APPLICATIONS_EMAIL)
            ->getValue();

        $filePaths = $this->getAttachments($application, $this->fileManager);

        $this->mailService
            ->to($to)
            ->locale('ua')
            ->attachments($filePaths)
            ->send(new LongApplication($data));
    }
    
    private function normalizeBoolean(bool $value): string
    {
        if ($value) {
            return $this->translator->trans('value.true', [], 'text');
        } else {
            return $this->translator->trans('value.false', [], 'text');
        }
    }

    private function getAttachments(CreditLongApplication $application, FileManager $fileManager): array
    {
        $result = [];

        if ($file = $application->getOldStylePassportFirstPage()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getOldStylePassportSecondPage()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getOldStylePassportPhotoAt25()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getOldStylePassportPastedPhotoAt25()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getOldStylePassportPhotoAt45()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getOldStylePassportPastedPhotoAt45()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getOldStylePassportProofOfResidence()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getOldStylePassportSelfieWithPassport()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getNewStylePassportPhotoPage()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getNewStylePassportValidityPage()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getNewStylePassportRegistrationAppendix()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getNewStylePassportSelfieWithIdCard()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getOldStylePensionCertificateFirstSpread()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getOldStylePensionCertificateValidityRecord()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        if ($file = $application->getNewStylePensionCertificatePhotoAndValiditySpread()) {
            $result[] = $fileManager->systemFilePath($file);
        }

        return $result;
    }
}