<?php

namespace App\Repository;

use App\Entity\News;
use App\Entity\Promotion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method Promotion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Promotion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Promotion[]    findAll()
 * @method Promotion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromotionRepository extends ServiceEntityRepository
{
    use FiltersByVisibility;
    use AddsTranslatableHints;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        parent::__construct($registry, Promotion::class);
    }

    public function filtered(array $filters = [], ?bool $onlyVisible = false, ?bool $excludeFeatured = false): Query
    {
        $builder = $this->createQueryBuilder('promotion')
            ->orderBy('promotion.createdAt', 'DESC');

        if ($onlyVisible) $this->visible($builder);

        if ($excludeFeatured) {
            $builder->andWhere('promotion.featured = false');
        }

        if (!empty($filters['title'])) {
            $builder->andWhere('promotion.title LIKE :title')
                ->setParameter('title', "%{$filters['title']}%");
        }

        return $builder->getQuery();
    }

    /**
     * @param Promotion $promotion
     * @param bool $previous
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getSibling(Promotion $promotion, $previous = true)
	{
		$builder = $this->createQueryBuilder('promotion');

		$this->visible($builder);

		if ($previous) {
			$builder
				->orderBy('promotion.id', 'DESC')
				->andWhere('promotion.id < :id');
		} else {
			$builder->andWhere('promotion.id > :id');
		}

		$builder->setParameter('id', $promotion->getId());
		$builder->setMaxResults(1);

		return $builder->getQuery()->getOneOrNullResult();
	}

	public function search($term): array
	{
        $builder = $this->createQueryBuilder('promotion');

        $this->visible($builder);

		if (!empty($term)) {
			$builder
				->orderBy('promotion.createdAt', 'DESC')
				->andWhere('promotion.title like :term or promotion.description like :term')
				->setParameter('term', "%{$term}%");

			return $builder->getQuery()->getResult();
		}

		return [];
	}

	public function latest(int $count)
	{
		$builder = $this->createQueryBuilder('promotion')
			->orderBy('promotion.createdAt', 'DESC')
			->setMaxResults((int) $count);

		$this->visible($builder);

		return $builder->getQuery()->getResult();
	}

    /**
     * @return Promotion|null
     * @throws NonUniqueResultException
     */
    public function featured(): ?Promotion
    {
        $builder = $this->createQueryBuilder('promotion')
            ->orderBy('promotion.createdAt', 'DESC')
            ->where('promotion.featured = true')
            ->setMaxResults(1);

        $this->visible($builder);

        return $builder->getQuery()->getOneOrNullResult();
    }

    public function resetFeatured(Promotion $promotion)
    {
        $query = $this->getEntityManager()
            ->createQuery("UPDATE App\Entity\Promotion promotion SET promotion.featured = false where promotion != :newFeaturedPromo")
            ->setParameter('newFeaturedPromo', $promotion);

        $query->execute();
    }

    /**
     * @param string $slug
     * @return News|null
     * @throws NonUniqueResultException
     */
    public function findBySlug(string $slug): ?Promotion
    {
        $builder = $this->createQueryBuilder('promotion')
            ->where('promotion.slug = :slug')
            ->setParameter('slug', $slug)
            ->setMaxResults(1);

        $query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());

        return $query->getOneOrNullResult();
    }
}
