<?php

namespace App\Repository\Scheduler;

use App\Entity\Scheduler\Schedule;
use DateTime;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Schedule|null find($id, $lockMode = null, $lockVersion = null)
 * @method Schedule|null findOneBy(array $criteria, array $orderBy = null)
 * @method Schedule[]    findAll()
 * @method Schedule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScheduleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Schedule::class);
    }

    public function lastExecutedAt(string $name): ?DateTimeInterface
    {
        $builder = $this->createQueryBuilder('schedule')
            ->andWhere('schedule.name = :name')
            ->orderBy('schedule.executedAt', 'DESC')
            ->setMaxResults(1)
            ->setParameter('name', $name);

        /** @var Schedule $entry */
        $entry = $builder->getQuery()->getOneOrNullResult();

        if (!$entry) {
            return null;
        }

        return $entry->getExecutedAt();
    }

    public function executedBefore(DateTime $date): array
    {
        $builder = $this->createQueryBuilder('schedule')
            ->where('schedule.executedAt <= :date')
            ->setParameter('date', $date);

        return $builder->getQuery()->getResult();
    }
}
