<?php

namespace App\Repository;

use App\Entity\Mail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Mail|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mail|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mail[]    findAll()
 * @method Mail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mail::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('mail')
            ->orderBy('mail.type', 'asc');

        if (!empty($filters['type'])) {
            $builder->andWhere('mail.type LIKE :type')
                ->setParameter('type', "%{$filters['type']}%");
        }

        if (!empty($filters['subject'])) {
            $builder->andWhere('mail.subject LIKE :subject')
                ->setParameter('subject', "%{$filters['subject']}%");
        }

        if (!empty($filters['content'])) {
            $builder->andWhere('mail.content LIKE :content')
                ->setParameter('content', "%{$filters['content']}%");
        }

        if (!empty($filters['locale'])) {
            $builder->andWhere('mail.locale = :locale')
                ->setParameter('locale', $filters['locale']);
        }

        return $builder->getQuery();
    }
}
