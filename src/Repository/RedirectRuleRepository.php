<?php

namespace App\Repository;

use App\Entity\RedirectRule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method RedirectRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method RedirectRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method RedirectRule[]    findAll()
 * @method RedirectRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RedirectRuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RedirectRule::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('redirect_rule')
            ->orderBy('redirect_rule.createdAt', 'DESC');

        if (!empty($filters['url'])) {
            $builder->andWhere('redirect_rule.oldUrl LIKE :url or redirect_rule.newUrl LIKE :url')
                ->setParameter('url', "%{$filters['url']}%");
        }

        return $builder->getQuery();
    }
}
