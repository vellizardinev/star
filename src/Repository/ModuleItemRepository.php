<?php

namespace App\Repository;

use App\Entity\ModuleItem;
use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ModuleItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModuleItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModuleItem[]    findAll()
 * @method ModuleItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModuleItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModuleItem::class);
    }

    public function namedItems()
    {
        $builder = $this->createQueryBuilder('module_item')
            ->andWhere('module_item.name is not null')
            ->orderBy('module_item.name', 'ASC');

        return $builder->getQuery()->getResult();
    }

    public function findOneByNameOrDescription(string $keyword)
    {
        $builder = $this->createQueryBuilder('module_item')
            ->andWhere('module_item.name is not null')
            ->andWhere('module_item.name = :keyword or module_item.description = :keyword')
            ->orderBy('module_item.name', 'ASC')
            ->setMaxResults(1)
            ->setParameter('keyword', $keyword);

        return $builder->getQuery()->getOneOrNullResult();
    }

    public function findForPage(Page $page)
    {
        $builder = $this->createQueryBuilder('module_item')
            ->leftJoin('module_item.module', 'module')
            ->andWhere('module_item.page = :page')
            ->andWhere('module.name != :header')
            ->andWhere('module.name != :footer')
            ->orderBy('module_item.positionIndex', 'ASC')
            ->setParameter('page', $page)
            ->setParameter('header', 'Header landing page')
            ->setParameter('footer', 'Footer landing page');

        return $builder->getQuery()->getResult();
    }
}
