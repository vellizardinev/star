<?php


namespace App\Repository;


use Carbon\Carbon;
use Doctrine\ORM\QueryBuilder;

trait FiltersByVisibility
{
    protected function visible(QueryBuilder $builder)
    {
        $alias = $builder->getRootAliases()[0];
        $startDate = sprintf('%s.startDate', $alias);
        $endDate = sprintf('%s.endDate', $alias);
        $statementStartDate = sprintf('%s is null or %s <= :now', $startDate, $startDate);
        $statementEndDate = sprintf('%s is null or %s >= :now', $endDate, $endDate);

        $builder
            ->andWhere($statementStartDate)
            ->andWhere($statementEndDate)
            ->setParameter('now', Carbon::today());

        return $builder;
    }
}