<?php

namespace App\Repository;

use App\Entity\Poll;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Poll|null find($id, $lockMode = null, $lockVersion = null)
 * @method Poll|null findOneBy(array $criteria, array $orderBy = null)
 * @method Poll[]    findAll()
 * @method Poll[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PollRepository extends ServiceEntityRepository
{
    use FiltersByVisibility;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Poll::class);
    }

    public function filtered(array $filters = [], $onlyVisible = false): Query
    {
        $builder = $this->createQueryBuilder('poll')
            ->orderBy('poll.createdAt', 'DESC');

        if ($onlyVisible) $this->visible($builder);

        if (!empty($filters['title'])) {
            $builder->andWhere('poll.title LIKE :title')
                ->setParameter('title', "%{$filters['title']}%");
        }

        return $builder->getQuery();
    }
}
