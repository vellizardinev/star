<?php

namespace App\Repository;

use App\Entity\LandingPage;
use App\Entity\Page;
use App\Enum\PageTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends ServiceEntityRepository
{
    use FiltersByVisibility;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    public function filtered(array $filters = [], $onlyVisible = false): Query
    {
        $builder = $this->createQueryBuilder('page')
            ->orderBy('page.name', 'asc');

        if ($onlyVisible) $this->visible($builder);

        if (!empty($filters['name'])) {
            $builder->andWhere('page.name LIKE :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

        if (!empty($filters['type']) && $filters['type'] === PageTypes::PAGE) {
            $builder
	            ->andWhere('page not instance of :type')
                ->setParameter('type', $this->getEntityManager()->getClassMetadata(LandingPage::class));
        }

        if (!empty($filters['type']) && $filters['type'] === PageTypes::LANDING_PAGE) {
            $builder->andWhere('page instance of :type')
                ->setParameter('type', $this->getEntityManager()->getClassMetadata(LandingPage::class));
        }

        return $builder->getQuery();
    }

    /**
     * @return Page[]
     */
    public function forSiteMap(): array
    {
        $builder = $this->createQueryBuilder('page')
            ->orderBy('page.name', 'asc');

        $this->visible($builder);

        $builder->andWhere('page.includeInSiteMap = :include')->setParameter('include', true);

        return $builder->getQuery()->getResult();
    }
}
