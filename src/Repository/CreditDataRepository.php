<?php

namespace App\Repository;

use App\Entity\Credit;
use App\Entity\CreditData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CreditData|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreditData|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreditData[]    findAll()
 * @method CreditData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreditDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CreditData::class);
    }

    /**
     * @param array $filters
     * @return mixed
     */
    public function get(array $filters = [])
    {
        $builder = $this->createQueryBuilder('credit_data')
            ->leftJoin('credit_data.credit', 'credit')
            ->andWhere('credit_data.paymentWithPenalty is not null')
            ->orderBy('credit_data.paymentWithPenalty', 'ASC');

        if (!empty($filters['amount'])) {
            $builder->andWhere('credit_data.amount = :amount')
                ->setParameter('amount', $filters['amount']);
        }

        if (!empty($filters['payment'])) {
            $builder->andWhere('credit_data.paymentWithPenalty = :payment')
                ->setParameter('payment', $filters['payment']);
        }

        if (!empty($filters['periods'])) {
            $builder->andWhere('credit_data.periods = :periods')
                ->setParameter('periods', $filters['periods']);
        }

        if (!empty($filters['pensioner']) && $filters['pensioner'] === 'yes') {
            $creditPensioner = $this->getEntityManager()
                ->getRepository(Credit::class)
                ->findOneBy(['pensioner' => true]);

            $filters['creditId'] = $creditPensioner->getId();
        }

        if (!empty($filters['pensioner']) && $filters['pensioner'] === 'no') {
            $creditPensioner = $this->getEntityManager()
                ->getRepository(Credit::class)
                ->findOneBy(['pensioner' => true]);

            $filters['creditIdToExclude'] = $creditPensioner->getId();
        }

        if (!empty($filters['paymentSchedule'])) {
            $builder->andWhere('credit_data.paymentSchedule = :paymentSchedule')
                ->setParameter('paymentSchedule', $filters['paymentSchedule']);
        }

        if (!empty($filters['type'])) {
            $builder
                ->andWhere('credit.type = :type')
                ->setParameter('type', $filters['type']);
        }

        if (!empty($filters['credit'])) {
            $builder
                ->andWhere('credit.slug = :slug')
                ->setParameter('slug', $filters['credit']);
        }

        if (!empty($filters['creditIdToExclude'])) {
            $builder
                ->andWhere('credit.id != :creditIdToExclude')
                ->setParameter('creditIdToExclude', $filters['creditIdToExclude']);
        }

//        return $builder->getQuery()->getResult();
        return $builder->getQuery()->getResult()[0];
    }

    public function getAmountSteps(array $credits): array
    {
        $iDs = array_map(
            function (Credit $credit) {
                return $credit->getId();
            }, $credits);

        $builder = $this->createQueryBuilder('credit_data')
            ->select('credit_data.amount')
            ->distinct()
            ->andWhere('credit_data.credit in (:credits)')
            ->orderBy('credit_data.amount', 'ASC')
            ->setParameter('credits', $iDs);

        $result = $builder->getQuery()->getArrayResult();

        $result = array_map(
            function ($element) {
                return $element['amount'];
            }, $result);

        return $result;
    }

    public function getPaymentSteps(array $credits, $amount): array
    {
        $iDs = array_map(
            function (Credit $credit) {
                return $credit->getId();
            }, $credits);

        $builder = $this->createQueryBuilder('credit_data')
            ->select('credit_data.paymentWithPenalty')
            ->distinct()
            ->andWhere('credit_data.credit in (:credits)')
            ->andWhere('credit_data.payment is not null')
            ->andWhere('credit_data.amount = :amount')
            ->orderBy('credit_data.paymentWithPenalty', 'ASC')
            ->setParameter('amount', $amount)
            ->setParameter('credits', $iDs);

        $result = $builder->getQuery()->getArrayResult();

        $result = array_map(
            function ($data) {
                return $data['paymentWithPenalty'];
            }, $result);

        return $result;
    }

    /**
     * @param Credit $credit
     * @return CreditData|null
     */
    public function getDefaults(Credit $credit): CreditData
    {
        $builder = $this->createQueryBuilder('credit_data')
            ->andWhere('credit_data.credit = :credit')

            ->andWhere('credit_data.payment is not null')
            ->setParameter('credit', $credit)
            ->orderBy('credit_data.payment', 'ASC');

        if ($credit->getDefaultAmount() !== null) {
            $builder ->andWhere('credit_data.amount >= :amount')->setParameter('amount', $credit->getDefaultAmount());
        }

        $results = $builder->getQuery()->getResult();

        /** @var CreditData $creditData */
        $creditData = $results[0];

        return $creditData;
    }

    public function exists(Credit $credit, float $amount, float $payment, int $period): bool
    {
        $builder = $this->createQueryBuilder('credit_data')
            ->andWhere('credit_data.credit = :credit')
            ->andWhere('credit_data.amount = :amount')
            ->andWhere('credit_data.payment = :payment')
            ->andWhere('credit_data.periods = :period')
            ->setParameter('credit', $credit)
            ->setParameter('amount', $amount)
            ->setParameter('payment', $payment)
            ->setParameter('period', $period);

        $results = $builder->getQuery()->getResult();

        return count($results) > 0;
    }
}
