<?php

namespace App\Repository;

use App\Entity\Gift;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method Gift|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gift|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gift[]    findAll()
 * @method Gift[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GiftRepository extends ServiceEntityRepository
{
	use AddsTranslatableHints;

	private $requestStack;

    public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
	    $this->requestStack = $requestStack;
        parent::__construct($registry, Gift::class);
    }

	public function filtered(array $filters = []): Query
	{
		$builder = $this->createQueryBuilder('gift');

		if (!empty($filters['name'])) {
			$builder->andWhere('gift.name LIKE :name')
				->setParameter('name', "%{$filters['name']}%");
		}

		if (!empty($filters['code'])) {
			$builder->andWhere('gift.code LIKE :code')
				->setParameter('code', "%{$filters['code']}%");
		}

		if (!empty($filters['orderBy'])) {
			$sortingParameters = explode(':', $filters['orderBy']);
			$builder
				->addSelect("ABS(gift.$sortingParameters[0]) as HIDDEN field_int_value")
				->orderBy('field_int_value', strtoupper($sortingParameters[1]));
		}else{
			$builder
				->orderBy('gift.price', 'ASC');
		}

		return $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());
	}

	public function search($term): ?array
	{
		if (!empty($term)) {
			$builder = $this->createQueryBuilder('gift')
				->orderBy('gift.positionIndex', 'asc')
				->andWhere('gift.name LIKE :term')
				->orWhere('gift.code LIKE :term')
				->setParameter('term', "%{$term}%");

			$query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());
			return $query->getResult();
		}

		return null;
	}
}
