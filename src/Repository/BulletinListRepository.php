<?php

namespace App\Repository;

use App\Entity\BulletinList;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BulletinList|null find($id, $lockMode = null, $lockVersion = null)
 * @method BulletinList|null findOneBy(array $criteria, array $orderBy = null)
 * @method BulletinList[]    findAll()
 * @method BulletinList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BulletinListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BulletinList::class);
    }

	public function filtered(array $filters = []): Query
	{
		$builder = $this->createQueryBuilder('bulletin')
			->orderBy('bulletin.createdAt', 'desc');

		if (!empty($filters['email'])) {
			$builder->andWhere('bulletin.subscriberEmail LIKE :email')
				->setParameter('email', "%{$filters['email']}%");
		}

		if (!empty($filters['start_date'])) {
			$startDate = Carbon::createFromFormat('d/m/Y', $filters['start_date'])->setTime(0, 0);

			$builder->andWhere('bulletin.createdAt >= :start_date')
				->setParameter('start_date', $startDate);
		}

		if (!empty($filters['end_date'])) {
			$endDate = Carbon::createFromFormat('d/m/Y', $filters['end_date'])->setTime(24, 0);

			$builder->andWhere('bulletin.createdAt < :end_date')
				->setParameter('end_date', $endDate);
		}

		return $builder->getQuery();
	}
}
