<?php

namespace App\Repository\Permission;

use App\Entity\Permission\Group;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Group|null find($id, $lockMode = null, $lockVersion = null)
 * @method Group|null findOneBy(array $criteria, array $orderBy = null)
 * @method Group[]    findAll()
 * @method Group[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Group::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('permission_group')
            ->orderBy('permission_group.name', 'ASC');

        if (!empty($filters['name'])) {
            $builder->andWhere('permission_group.name LIKE :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

        return $builder->getQuery();
    }
}
