<?php

namespace App\Repository\Permission;

use App\Entity\Permission\Permission;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Permission|null find($id, $lockMode = null, $lockVersion = null)
 * @method Permission|null findOneBy(array $criteria, array $orderBy = null)
 * @method Permission[]    findAll()
 * @method Permission[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PermissionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Permission::class);
    }

    /**
     * @param string $domain
     * @param string $action
     * @param User $user
     * @return bool
     * @throws NonUniqueResultException
     */
    public function checkPermissionForUser(string $domain, string $action, User $user)
    {
        $builder = $this->createQueryBuilder('permission')
            ->select('count(permission.id)')
            ->join('permission.groups', 'groups')
            ->join('groups.users', 'users')
            ->andWhere('permission.domain = :domain')
            ->andWhere('permission.action = :action')
            ->andWhere('users = :user')
            ->setParameter('domain', $domain)
            ->setParameter('action', $action)
            ->setParameter('user', $user);

        return $builder->getQuery()
                ->useQueryCache(true)
                ->useResultCache(true)
                ->getSingleScalarResult() > 0;
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('permission')
            ->orderBy('permission.domain', 'ASC')
            ->addOrderBy('permission.action', 'ASC');

        if (!empty($filters['domain'])) {
            $builder->andWhere('permission.domain LIKE :domain')
                ->setParameter('domain', "%{$filters['domain']}%");
        }

        if (!empty($filters['action'])) {
            $builder->andWhere('permission.action LIKE :action')
                ->setParameter('action', "%{$filters['action']}%");
        }

        return $builder->getQuery();
    }
}
