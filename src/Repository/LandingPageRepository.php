<?php

namespace App\Repository;

use App\Entity\LandingPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LandingPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method LandingPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method LandingPage[]    findAll()
 * @method LandingPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LandingPageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LandingPage::class);
    }
}
