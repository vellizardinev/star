<?php

namespace App\Repository;

use App\Entity\Setting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Setting|null find($id, $lockMode = null, $lockVersion = null)
 * @method Setting|null findOneBy(array $criteria, array $orderBy = null)
 * @method Setting[]    findAll()
 * @method Setting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Setting::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('setting')
            ->orderBy('setting.name', 'asc');

        if (!empty($filters['name'])) {
            $builder->andWhere('setting.uiLabel LIKE :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

        return $builder->getQuery();
    }

    /**
     * @param $setting
     * @return mixed
     */
    public function get($setting)
    {
        try {
            return $this->createQueryBuilder('setting')
                ->where('setting.name = :setting')
                ->setParameter('setting', $setting)
                ->setMaxResults(1)
                ->getQuery()
                ->useQueryCache(true)
                ->useResultCache(true)
                ->getOneOrNullResult();
        } catch(Exception $exception) {
            return '';
        }
    }
}
