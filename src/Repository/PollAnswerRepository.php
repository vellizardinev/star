<?php

namespace App\Repository;

use App\Entity\PollAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PollAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method PollAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method PollAnswer[]    findAll()
 * @method PollAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PollAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PollAnswer::class);
    }
}
