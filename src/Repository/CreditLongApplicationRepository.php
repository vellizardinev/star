<?php

namespace App\Repository;

use App\Entity\CreditLongApplication;
use App\Entity\File;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;

/**
 * @method CreditLongApplication|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreditLongApplication|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreditLongApplication[]    findAll()
 * @method CreditLongApplication[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreditLongApplicationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CreditLongApplication::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('creditApplication')
            ->orderBy('creditApplication.id', 'desc');

        if (!empty($filters['name'])) {
            $builder->andWhere('creditApplication.firstName LIKE :name or creditApplication.middleName LIKE :name or creditApplication.lastName LIKE :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

        if (!empty($filters['telephone'])) {
            $builder->andWhere('creditApplication.mobileTelephone LIKE :telephone or creditApplication.companyTelephone LIKE :telephone or creditApplication.additionalPhone LIKE :telephone')
                ->setParameter('telephone', "%{$filters['telephone']}%");
        }

        if (!empty($filters['start_date'])) {
            $startDate = Carbon::createFromFormat('d/m/Y', $filters['start_date'])->setTime(0, 0);

            $builder->andWhere('creditApplication.createdAt >= :start_date')
                ->setParameter('start_date', $startDate);
        }

        if (!empty($filters['end_date'])) {
            $endDate = Carbon::createFromFormat('d/m/Y', $filters['end_date'])->setTime(24, 0);

            $builder->andWhere('creditApplication.createdAt < :end_date')
                ->setParameter('end_date', $endDate);
        }

        return $builder->getQuery();
    }

    /**
     * @param int $days
     * @return CreditLongApplication[]
     */
    public function olderThan(int $days = 3): array
    {
        $referenceDate = Carbon::now()->subDays($days);

        $builder = $this->createQueryBuilder('credit_long_application')
            ->andWhere('credit_long_application.createdAt <= :date')
            ->setParameter('date', $referenceDate);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param File $file
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function fileIsAssociatedWithApplication(File $file): bool
    {
        $builder = $this->createQueryBuilder('credit_long_application')
            ->select('count(credit_long_application.id)')
            ->andWhere(':file member of credit_long_application.files')
            ->setMaxResults(1)
            ->setParameter('file', $file);

        return $builder->getQuery()->getSingleScalarResult() > 0;
    }
}
