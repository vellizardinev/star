<?php

namespace App\Repository;

use App\Entity\JobOpening;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method JobOpening|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobOpening|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobOpening[]    findAll()
 * @method JobOpening[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobOpeningRepository extends ServiceEntityRepository
{
    use FiltersByVisibility;
	use AddsTranslatableHints;

	private $requestStack;

    public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
	    $this->requestStack = $requestStack;
        parent::__construct($registry, JobOpening::class);
    }

    public function filtered(array $filters = [], $onlyVisible = false): Query
    {
        $builder = $this->createQueryBuilder('job_opening')
            ->leftJoin('job_opening.office', 'office')
            ->leftJoin('job_opening.department', 'department')
            ->leftJoin('job_opening.city', 'city')
            ->orderBy('job_opening.createdAt', 'desc');

        if ($onlyVisible) $this->visible($builder);

        if (!empty($filters['position'])) {
            $builder->andWhere('lower(job_opening.position) LIKE :position')
                ->setParameter('position', strtolower("%{$filters['position']}%"));
        }

        if (!empty($filters['description'])) {
            $builder->andWhere('lower(job_opening.description) LIKE :description')
                ->setParameter('description', strtolower("%{$filters['description']}%"));
        }

        if (!empty($filters['office']) && $filters['office'] !== 'any') {
            $builder->andWhere('office.id = :officeId')
                ->setParameter('officeId', $filters['office']);
        }

        if (!empty($filters['department']) && $filters['department'] !== 'any') {
            $builder->andWhere('department.id = :departmentId')
                ->setParameter('departmentId', $filters['department']);
        }

        if (!empty($filters['city']) && $filters['city'] !== 'any') {
            $builder->andWhere('city.id = :cityId')
                ->setParameter('cityId', $filters['city']);
        }

	    return $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());
    }

	public function search($term): ?array
	{
		if (!empty($term)) {
			$builder = $this->createQueryBuilder('job_opening')
				->orderBy('job_opening.createdAt', 'desc')
				->andWhere('job_opening.position LIKE :term')
				->orWhere('job_opening.description LIKE :term')
				->setParameter('term', "%{$term}%");

			$query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());
			return $query->getResult();
		}

		return [];
	}

    public function jobOpeningForCity(string $city): array
    {
        $builder = $this->createQueryBuilder('job_opening')
            ->leftJoin('job_opening.city', 'city')
            ->andWhere('city.name = :city')
            ->setParameter('city', $city)
        ;

        return $builder->getQuery()->getResult();
    }
}
