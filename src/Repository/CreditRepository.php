<?php

namespace App\Repository;

use App\Entity\Credit;
use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method Credit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Credit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Credit[]    findAll()
 * @method Credit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreditRepository extends ServiceEntityRepository
{
    use AddsTranslatableHints;

    private $requestStack;

    public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        parent::__construct($registry, Credit::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('credit')
            ->orderBy('credit.positionIndex', 'asc');

        if (!empty($filters['title'])) {
            $builder->andWhere('credit.title LIKE :title')
                ->setParameter('title', "%{$filters['title']}%");
        }

        return $builder->getQuery();
    }

	public function search($term): ?array
	{
		if (!empty($term)) {
			$builder = $this->createQueryBuilder('credit')
				->orderBy('credit.title', 'asc')
				->andWhere('(credit.title LIKE :term or credit.fullDescription like :term or credit.shortDescription like :term or credit.requirements like :term)')
				->setParameter('term', "%{$term}%");

			return $builder->getQuery()->getResult();
		}

		return [];
	}

    public function getCreditByParameters(array $parameters = []): array
    {
        $builder = $this->createQueryBuilder('credit');

        if (!empty($parameters['credit'])) {
            return $builder
                ->andWhere('credit.slug = :slug')
                ->setParameter('slug', $parameters['credit'])
                ->getQuery()
                ->getResult();
        }

        if (!empty($parameters['pensioner']) && $parameters['pensioner'] === 'yes') {
            return $builder
                ->andWhere('credit.pensioner = true')
                ->getQuery()
                ->getResult();
        }

        if (!empty($parameters['paymentSchedule'])) {
            $builder
                ->andWhere('credit.pensioner = false')
                ->andWhere('credit.paymentSchedule = :paymentSchedule')
                ->setParameter('paymentSchedule', $parameters['paymentSchedule']);
        }

        if (!empty($parameters['type'])) {
            $builder
                ->andWhere('credit.pensioner = false')
                ->andWhere('credit.type = :type')
                ->setParameter('type', $parameters['type']);
        }

        return $builder->getQuery()->getResult();
    }

    /**
     * @param string|null $slug
     * @return News|null
     * @throws NonUniqueResultException
     */
    public function findBySlug(?string $slug): ?Credit
    {
        $builder = $this->createQueryBuilder('credit')
            ->where('credit.slug = :slug')
            ->setParameter('slug', $slug)
            ->setMaxResults(1);

        $query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());

        return $query->getOneOrNullResult();
    }
}
