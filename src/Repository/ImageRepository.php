<?php

namespace App\Repository;

use App\Entity\Image;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Image|null find($id, $lockMode = null, $lockVersion = null)
 * @method Image|null findOneBy(array $criteria, array $orderBy = null)
 * @method Image[]    findAll()
 * @method Image[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Image::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('image')
            ->orderBy('image.createdAt', 'desc');

        if (!empty($filters['name'])) {
            $builder->andWhere('image.name LIKE :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

        return $builder->getQuery();
    }

	public function getGalleryImages()
	{
		$builder = $this->createQueryBuilder('image')
			->orderBy('image.createdAt', 'desc');

		return $builder->getQuery()->getResult();
	}

}
