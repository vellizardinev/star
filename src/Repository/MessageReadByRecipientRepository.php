<?php

namespace App\Repository;

use App\Entity\MessageReadByRecipient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MessageReadByRecipient|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageReadByRecipient|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageReadByRecipient[]    findAll()
 * @method MessageReadByRecipient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageReadByRecipientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MessageReadByRecipient::class);
    }
}
