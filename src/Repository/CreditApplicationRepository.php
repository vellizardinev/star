<?php

namespace App\Repository;

use App\Entity\CreditApplicationChangelog;
use App\Entity\CreditShortApplication;
use App\Enum\CreditApplicationSources;
use App\Enum\DatePeriodTypes;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CreditShortApplication|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreditShortApplication|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreditShortApplication[]    findAll()
 * @method CreditShortApplication[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreditApplicationRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, CreditShortApplication::class);
	}

	public function filtered(array $filters = []): Query
	{
		$builder = $this->createQueryBuilder('creditApplication')
			->orderBy('creditApplication.id', 'desc');

		if (!empty($filters['name'])) {
			$builder->andWhere('creditApplication.name LIKE :name')
				->setParameter('name', "%{$filters['name']}%");
		}

		if (!empty($filters['telephone'])) {
			$builder->andWhere('creditApplication.telephone LIKE :telephone')
				->setParameter('telephone', "%{$filters['telephone']}%");
		}

		if (!empty($filters['source']) && $filters['source'] !== 'any') {
			$source = $filters['source'];
			if ($source !== CreditApplicationSources::WEBSITE) {
				$builder->andWhere("creditApplication.{$source} is not null");
			} else {
				$builder
					->andWhere('creditApplication.affiliate is null')
					->andWhere('creditApplication.landing is null');
			}
		}

		if (!empty($filters['start_date'])) {
			$startDate = Carbon::createFromFormat('d/m/Y', $filters['start_date'])->setTime(0, 0);

			$builder->andWhere('creditApplication.createdAt >= :start_date')
				->setParameter('start_date', $startDate);
		}

		if (!empty($filters['end_date'])) {
			$endDate = Carbon::createFromFormat('d/m/Y', $filters['end_date'])->setTime(24, 0);

			$builder->andWhere('creditApplication.createdAt < :end_date')
				->setParameter('end_date', $endDate);
		}

		if (array_key_exists('erp_status', $filters) && $filters['erp_status'] !== 'any') {
			$isSent = (bool)$filters['erp_status'];

			if ($isSent) {
				$builder->andWhere('creditApplication.sentToErp = true');
			} else {
				$builder->andWhere('creditApplication.sentToErp = false or creditApplication.sentToErp is null');
			}
		}

		return $builder->getQuery();
	}

	public function countByPeriod(string $period): int
	{
		if (!DatePeriodTypes::hasValue($period)) {
			return 0;
		}

		$date = date("Y-m-d", strtotime($period));

		$query = $this->createQueryBuilder('creditApplication')
			->select('count(creditApplication.id)')
			->where('creditApplication.createdAt >= :date')
			->setParameter('date', $date)
			->getQuery()
			->useQueryCache(true)
			->useResultCache(true, 3600);

		return $query->getSingleScalarResult();
	}

	public function withIdGreaterThan(?int $id)
	{
		$builder = $this->createQueryBuilder('credit_application')
			->orderBy('credit_application.id', 'ASC');

		if ($id) {
			$builder->andWhere('credit_application.id > :id')->setParameter('id', $id);
		}

		return $builder->getQuery()->getResult();
	}

	public function getExportData(array $filters = []): array
	{
		$qb2 = $this->_em->createQueryBuilder()
			->select('MAX(changelog.createdAt) maxDate')
			->from(CreditApplicationChangelog::class, 'changelog')
			->where('changelog.creditApplication = creditApplication');

		$builder = $this->createQueryBuilder('creditApplication');
		$builder
			->orderBy('creditApplication.id', 'desc')
			->select(
				'creditApplication.name',
				'creditApplication.telephone',
				'creditApplication.amount',
				'creditApplication.status',
				'creditApplication.createdAt',
				'a.name as affiliate',
				'l.name as landing',
				'user.email as editor'
			)
			->leftJoin('creditApplication.landing', 'l')
			->leftJoin('creditApplication.affiliate', 'a')
			->leftJoin('creditApplication.creditApplicationChangelog', 'c', 'WITH', $builder->expr()->eq('c.createdAt', '(' . $qb2->getDQL() . ')'))
			->leftJoin('c.user', 'user');

		if (!empty($filters['name'])) {
			$builder->andWhere('creditApplication.name LIKE :name')
				->setParameter('name', "%{$filters['name']}%");
		}

		if (!empty($filters['telephone'])) {
			$builder->andWhere('creditApplication.telephone LIKE :telephone')
				->setParameter('telephone', "%{$filters['telephone']}%");
		}

		if (!empty($filters['source']) && $filters['source'] !== 'any') {
			$source = $filters['source'];
			if ($source !== CreditApplicationSources::WEBSITE) {
				$builder->andWhere("creditApplication.{$source} is not null");
			} else {
				$builder
					->andWhere('creditApplication.affiliate is null')
					->andWhere('creditApplication.landing is null');
			}
		}

		if (!empty($filters['start_date'])) {
			$startDate = Carbon::createFromFormat('d/m/Y', $filters['start_date'])->setTime(0, 0);

			$builder->andWhere('creditApplication.createdAt >= :start_date')
				->setParameter('start_date', $startDate);
		}

		if (!empty($filters['end_date'])) {
			$endDate = Carbon::createFromFormat('d/m/Y', $filters['end_date'])->setTime(24, 0);

			$builder->andWhere('creditApplication.createdAt < :end_date')
				->setParameter('end_date', $endDate);
		}

		if (!empty($filters['erp_status'])) {
			$isSent = $filters['erp_status'];
			if ($isSent) {
				$builder->andWhere('creditApplication.sentToErp = true');
			} else {
				$builder->andWhere('creditApplication.sentToErp = false or creditApplication.sentToErp is null');
			}
		}
		return $builder->getQuery()->getArrayResult();
	}
}
