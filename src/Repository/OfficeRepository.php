<?php

namespace App\Repository;

use App\Entity\Office;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method Office|null find($id, $lockMode = null, $lockVersion = null)
 * @method Office|null findOneBy(array $criteria, array $orderBy = null)
 * @method Office[]    findAll()
 * @method Office[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfficeRepository extends ServiceEntityRepository
{
	use AddsTranslatableHints;

	private $requestStack;

    public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
	    $this->requestStack = $requestStack;
        parent::__construct($registry, Office::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('office')
            ->leftJoin('office.city', 'city')
            ->orderBy('office.name', 'ASC');

        if (!empty($filters['name'])) {
            $builder->andWhere('lower(office.name) LIKE :name')
                ->setParameter('name', strtolower("%{$filters['name']}%"));
        }

        if (!empty($filters['city']) && $filters['city'] !== 'any') {
            $builder->andWhere('city.id = :cityId')
                ->setParameter('cityId', $filters['city']);
        }

        return $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());
    }

	public function search($term): array
	{
		if (!empty($term)) {
			$builder = $this->createQueryBuilder('office')
				->orderBy('office.name', 'desc')
				->leftJoin('office.city', 'city')
				->andWhere('office.name LIKE :term')
				->orWhere('office.telephone LIKE :term')
				->orWhere('office.address LIKE :term')
				->orWhere('city.name LIKE :term')
				->setParameter('term', "%{$term}%");

			$query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());
			
			return $query->getResult();
		}

		return [];
	}

	public function findAllOffices(): array
	{
		$builder = $this->createQueryBuilder('office')
			->select('office, city')
			->orderBy('office.name', 'desc')
			->where('office.name != :centralOfficeName')
			->setParameter('centralOfficeName', 'Central Office')
			->leftJoin('office.city', 'city');

		$query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());

		return $query->getResult();
	}

	public function all(): QueryBuilder
    {
        $builder = $this->createQueryBuilder('office')
            ->orderBy('office.name', 'asc');

        return $builder;
    }

	public function officesForMap()
    {
        $builder = $this->createQueryBuilder('office')
            ->select('office, city')
            ->where('office.showOnMap = true')
            ->orderBy('office.name', 'desc')
            ->leftJoin('office.city', 'city');

	    $query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());

		return $builder->getQuery()->getResult();
	}

	public function citiesWithOffices()
    {
        $builder = $this->createQueryBuilder('office')
            ->join('office.city', 'city')
            ->select('city.id')
            ->distinct();

        $result = $builder->getQuery()->getResult();

        $result = array_map(
            function ($item) {
                return $item['id'];
            }, $result);

        return $result;
    }

    public function officesForCity(string $city): array
    {
        $builder = $this->createQueryBuilder('office')
            ->leftJoin('office.city', 'city')
            ->andWhere('city.name = :city')
            ->setParameter('city', $city)
        ;

        return $builder->getQuery()->getResult();
    }
}
