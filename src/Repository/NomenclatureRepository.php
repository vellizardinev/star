<?php

namespace App\Repository;

use App\Entity\Nomenclature;
use App\Enum\NomenclatureTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method Nomenclature|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nomenclature|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nomenclature[]    findAll()
 * @method Nomenclature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NomenclatureRepository extends ServiceEntityRepository
{
    use AddsTranslatableHints;

    private $requestStack;

    public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
        parent::__construct($registry, Nomenclature::class);
        $this->requestStack = $requestStack;
    }

    /**
     * @param string $guid
     * @param string $type
     * @return bool
     * @throws NonUniqueResultException
     */
    public function exists(string $guid, string $type): bool
    {
        $builder = $this->createQueryBuilder('nomenclature')
            ->select('count(nomenclature.id)')
            ->andWhere('nomenclature.guid = :guid')
            ->andWhere('nomenclature.type = :type')
            ->setParameter('guid', $guid)
            ->setParameter('type', $type)
            ->setMaxResults(1);

        return $builder->getQuery()->getSingleScalarResult() > 0;
    }

    public function choices(string $type, ?string $orderBy = null, ?string $orderDirection = 'asc'): array
    {
        $builder = $this->createQueryBuilder('nomenclature')
            ->select('nomenclature.guid, nomenclature.text')
            ->andWhere('nomenclature.type = :type')
            ->andWhere('nomenclature.visible = :visible')
            ->orderBy('nomenclature.text', 'ASC')
            ->setParameter('type', $type)
            ->setParameter('visible', true);

        if ($orderBy) {
            $builder->orderBy("nomenclature.$orderBy", $orderDirection);
        }

        $query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());

        $results = $query->getArrayResult();

        $choices = [];

        foreach ($results as $item) {
            $choices[$item['text']] = $item['guid'];
        }

        return $choices;
    }

    public function familySizes(): array
    {
        $choices = $this->choices(NomenclatureTypes::FAMILY_SIZES);

        $choices = array_flip($choices);

        $choices = array_map(
            function ($item) {
                return intval($item);
            }, $choices);

        asort($choices);
        $choices = array_flip($choices);

        return $choices;
    }

    public function propertyTypes(): array
    {
        $choices = $this->choices(NomenclatureTypes::PROPERTY_TYPES);

        $choices = ['Nie' => 'nie'] + $choices;

        return $choices;
    }

    public function howSalaryIsReceived(): array
    {
        $choices = $this->choices(NomenclatureTypes::METHODS_RECEIVING_SALARY);

        $choices = ['Bez dochodów' => 'Without income'] + $choices;

        return $choices;
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('nomenclature')
            ->orderBy('nomenclature.type', 'asc');

        if (!empty($filters['text'])) {
            $builder->andWhere('nomenclature.text LIKE :text')
                ->setParameter('text', "%{$filters['text']}%");
        }


        if (!empty($filters['type'])  && $filters['type'] !== 'any') {
            $builder->andWhere('nomenclature.type LIKE :type')
                ->setParameter('type', "%{$filters['type']}%");
        }

        return $builder->getQuery();
    }
}
