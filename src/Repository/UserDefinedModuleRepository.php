<?php

namespace App\Repository;

use App\Entity\UserDefinedModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserDefinedModule|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserDefinedModule|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserDefinedModule[]    findAll()
 * @method UserDefinedModule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserDefinedModuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserDefinedModule::class);
    }
}
