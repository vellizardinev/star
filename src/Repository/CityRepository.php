<?php

namespace App\Repository;

use App\Entity\City;
use App\Entity\Office;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

	public function citiesUsedInJobOpening(): QueryBuilder
	{
		$now = new DateTime('now');
		$now = $now->format('Y-m-d');

		$builder = $this->createQueryBuilder('city')
			->distinct()
			->leftJoin(
				'App:JobOpening',
				'job_opening',
				'WITH',
				'job_opening.city is not null'
			)
			->join('job_opening.city', 'jobCity')
			->andWhere('city = jobCity')
			->andWhere('job_opening.startDate is null or job_opening.startDate <= :now')
			->andWhere('job_opening.endDate is null or job_opening.endDate >= :now')
			->setParameter('now', $now)
			->orderBy('city.name', 'asc');

		return $builder;
	}

	public function citiesWithOffices()
    {
        $cityIds = $this->getEntityManager()->getRepository(Office::class)->citiesWithOffices();

        $builder = $this->createQueryBuilder('city')
            ->where('city.id in (:cityIds)')
            ->setParameter('cityIds', $cityIds)
            ->orderBy('city.name', 'ASC');

        return $builder->getQuery()->getResult();
    }

	public function all(): QueryBuilder
	{
		$builder = $this->createQueryBuilder('city')
			->orderBy('city.name', 'asc');

		return $builder;
	}
}
