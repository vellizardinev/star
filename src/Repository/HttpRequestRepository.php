<?php

namespace App\Repository;

use App\Entity\HttpRequest;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HttpRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method HttpRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method HttpRequest[]    findAll()
 * @method HttpRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HttpRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HttpRequest::class);
    }

    public function requestsToRetry(): array
    {
        $builder = $this->createQueryBuilder('http_request')
            ->andWhere('http_request.attempts < http_request.maxAttempts')
            ->andWhere('http_request.success = false')
            ->andWhere('http_request.nextAttemptOn is null or http_request.nextAttemptOn <= :now')
            ->setParameter('now', Carbon::now())
        ;

        return $builder->getQuery()->getResult();
    }
}
