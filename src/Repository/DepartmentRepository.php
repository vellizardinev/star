<?php

namespace App\Repository;

use App\Entity\Department;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method Department|null find($id, $lockMode = null, $lockVersion = null)
 * @method Department|null findOneBy(array $criteria, array $orderBy = null)
 * @method Department[]    findAll()
 * @method Department[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepartmentRepository extends ServiceEntityRepository
{
	use AddsTranslatableHints;

	private $requestStack;

	public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
	    $this->requestStack = $requestStack;
	    parent::__construct($registry, Department::class);
    }

	public function filtered(array $filters = []): Query
	{
		$builder = $this->createQueryBuilder('department')
			->orderBy('department.positionIndex', 'asc');

		if (!empty($filters['name'])) {
			$builder->andWhere('department.name LIKE :name')
				->setParameter('name', "%{$filters['name']}%");
		}

		if (!empty($filters['director'])) {
			$builder->andWhere('department.director LIKE :director')
				->setParameter('director', "%{$filters['director']}%");
		}

		if (!empty($filters['description'])) {
			$builder->andWhere('department.description LIKE :description')
				->setParameter('description', "%{$filters['description']}%");
		}

		if (array_key_exists('visible', $filters) && $filters['visible'] !== 'any') {
			$builder->andWhere('department.visible = :visible')
				->setParameter('visible', (bool)$filters['visible']);
		}

		return $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());
	}

	public function search($term): ?array
	{
		if (!empty($term)) {
			$builder = $this->createQueryBuilder('department')
				->orderBy('department.positionIndex', 'asc')
				->andWhere('department.visible = true')
				->andWhere('department.name LIKE :term or department.description LIKE :term or department.director LIKE :term')
				->setParameter('term', "%{$term}%");

			$query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());

			return $query->getResult();
		}

		return [];
	}

	public function findVisible(): ?array
	{
		$builder = $this->createQueryBuilder('department')
			->orderBy('department.positionIndex', 'asc')
			->andWhere('department.visible = true');

		$query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());

		return $query->getResult();
	}
}
