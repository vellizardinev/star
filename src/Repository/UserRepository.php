<?php

namespace App\Repository;

use App\Entity\User;
use App\Enum\Roles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('user')
            ->select('user, profile')
            ->leftJoin('user.profile', 'profile')
            ->orderBy('user.createdAt', 'DESC');

        if (!empty($filters['name'])) {
            $builder
                ->andWhere('profile.firstName LIKE :name or profile.lastName like :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

        if (!empty($filters['email'])) {
            $builder->andWhere('user.email LIKE :email')
                ->setParameter('email', "%{$filters['email']}%");
        }

        if (!empty($filters['role']) && $filters['role'] !== Roles::USER) {
            $builder->andWhere('user.roles like :role')
                ->setParameter('role', "%{$filters['role']}%");
        }

        if (array_key_exists('status', $filters) && $filters['status'] !== 'any') {
            $builder->andWhere('user.active = :status')
                ->setParameter('status', (bool)$filters['status']);
        }

        return $builder->getQuery();
    }
}
