<?php

namespace App\Repository;

use App\Entity\CareersPartner;
use App\Entity\Image;
use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CareersPartnerRepository extends ServiceEntityRepository
{
    use FiltersByVisibility;
    use AddsTranslatableHints;

    private $requestStack;

    public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        parent::__construct($registry, CareersPartner::class);
    }

    public function filtered(array $filters = [], $onlyVisible = false): Query
    {
        $builder = $this->createQueryBuilder('partner')
            ->orderBy('partner.positionIndex', 'asc');

        if ($onlyVisible) $this->visible($builder);

        if (!empty($filters['name'])) {
            $builder->andWhere('partner.name LIKE :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

	    if (!empty($filters['description'])) {
		    $builder->andWhere('partner.description LIKE :description')
			    ->setParameter('description', "%{$filters['description']}%");
	    }

	    if (!empty($filters['discount'])) {
		    $builder->andWhere('partner.discount LIKE :discount')
			    ->setParameter('discount', "%{$filters['discount']}%");
	    }

        return $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());
    }

    /**
     * @param Image $image
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findByImage(Image $image)
    {
        $builder = $this->createQueryBuilder('partner')
            ->where(':image member of partner.images')
            ->setParameter('image', $image)
            ->setMaxResults(1);

        return $builder->getQuery()->getOneOrNullResult();
    }
}
