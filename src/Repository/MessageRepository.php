<?php

namespace App\Repository;

use App\DataTransferObject\MessageProxy;
use App\Entity\Customer;
use App\Entity\Message;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('message')
            ->orderBy('message.id', 'desc');

        if (!empty($filters['subject'])) {
            $builder->andWhere('message.subject LIKE :subject')
                ->setParameter('subject', "%{$filters['subject']}%");
        }

        if (!empty($filters['content'])) {
            $builder->andWhere('message.content LIKE :content')
                ->setParameter('content', "%{$filters['content']}%");
        }

        if (!empty($filters['created_by'])) {
            $builder
                ->join('message.createdBy', 'createdBy')
                ->leftJoin('createdBy.profile', 'profile')
                ->andWhere('profile.firstName LIKE :name or profile.lastName like :name or createdBy.email like :name')
                ->setParameter('name', "%{$filters['created_by']}%");
        }

        if (array_key_exists('visible', $filters) && $filters['visible'] !== 'any') {
            $builder->andWhere('message.visible = :visible')
                ->setParameter('visible', (bool)$filters['visible']);
        }

        return $builder->getQuery();
    }

    public function unreadMessagesCountForCustomer(Customer $customer): int
    {
        $subBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('count(message_read_by_recipient.id)')
            ->from('App\\Entity\\MessageReadByRecipient', 'message_read_by_recipient')
            ->andWhere('message = message_read_by_recipient.message')
            ->andWhere('message_read_by_recipient.email = :email')
        ;

        $builder = $this->createQueryBuilder('message')
            ->select('message.id')
            ->orderBy('message.id','DESC')
            ->leftJoin('message.recipients', 'recipients')
            ->leftJoin('message.messageReadByRecipients', 'message_read_by_recipients')
            ->andWhere('message.visible = true')
            ->andWhere('message.toAllCustomers = true or recipients.email = :email')
            ->andWhere('0 = (' . $subBuilder->getQuery()->getDQL() . ')')
            ->setParameter('email', $customer->getEmail())
            ->distinct()
        ;

        return count($builder->getQuery()->getResult());
    }

    /**
     * @param \App\Entity\Customer $customer
     * @return MessageProxy[]
     */
    public function messagesForCustomer(Customer $customer): array
    {
        $messages = array_merge(
            $this->unreadMessagesForCustomer($customer),
            $this->readMessagesForCustomer($customer)
        );

        usort($messages,
            function (MessageProxy $proxy1, MessageProxy $proxy2) {
                return Carbon::instance($proxy1->getMessage()->getCreatedAt())->lessThanOrEqualTo($proxy2->getMessage()->getCreatedAt());
            });

        return $messages;
    }

    /**
     * @param Customer $customer
     * @return MessageProxy[]
     */
    private function unreadMessagesForCustomer(Customer $customer): array
    {
        $subBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('count(message_read_by_recipient.id)')
            ->from('App\\Entity\\MessageReadByRecipient', 'message_read_by_recipient')
            ->andWhere('message = message_read_by_recipient.message')
            ->andWhere('message_read_by_recipient.email = :email')
        ;

        $builder = $this->createQueryBuilder('message')
            ->orderBy('message.id','DESC')
            ->leftJoin('message.recipients', 'recipients')
            ->leftJoin('message.messageReadByRecipients', 'message_read_by_recipients')
            ->andWhere('message.visible = true')
            ->andWhere('message.toAllCustomers = true or recipients.email = :email')
            ->andWhere('0 = (' . $subBuilder->getQuery()->getDQL() . ')')
            ->setParameter('email', $customer->getEmail())
            ->distinct()
        ;

        $messages = $builder->getQuery()->getResult();
        $results = [];

        foreach ($messages as $message) {
            $results[] = new MessageProxy($message, false);
        }

        return $results;
    }

    /**
     * @param Customer $customer
     * @return MessageProxy[]
     */
    private function readMessagesForCustomer(Customer $customer): array
    {
        $subBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('count(message_read_by_recipient.id)')
            ->from('App\\Entity\\MessageReadByRecipient', 'message_read_by_recipient')
            ->andWhere('message = message_read_by_recipient.message')
            ->andWhere('message_read_by_recipient.email = :email')
        ;

        $builder = $this->createQueryBuilder('message')
            ->orderBy('message.id','DESC')
            ->leftJoin('message.recipients', 'recipients')
            ->leftJoin('message.messageReadByRecipients', 'message_read_by_recipients')
            ->andWhere('message.visible = true')
            ->andWhere('message.toAllCustomers = true or recipients.email = :email')
            ->andWhere('0 < (' . $subBuilder->getQuery()->getDQL() . ')')
            ->setParameter('email', $customer->getEmail())
            ->distinct()
        ;

        $messages = $builder->getQuery()->getResult();
        $results = [];

        foreach ($messages as $message) {
            $results[] = new MessageProxy($message, true);
        }

        return $results;
    }
}
