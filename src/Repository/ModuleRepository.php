<?php

namespace App\Repository;

use App\Entity\Module;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Module|null find($id, $lockMode = null, $lockVersion = null)
 * @method Module|null findOneBy(array $criteria, array $orderBy = null)
 * @method Module[]    findAll()
 * @method Module[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Module::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('module')
            ->orderBy('module.name', 'asc')
            ->andWhere('module.name != :headerLandingPage')
            ->andWhere('module.name != :footerLandingPage')
            ->setParameter('headerLandingPage', 'Header landing page')
            ->setParameter('footerLandingPage', 'Footer landing page');

        if (!empty($filters['name'])) {
            $builder->andWhere('module.name LIKE :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

        return $builder->getQuery();
    }
}
