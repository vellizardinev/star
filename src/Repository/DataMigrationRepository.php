<?php

namespace App\Repository;

use App\Entity\DataMigration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DataMigration|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataMigration|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataMigration[]    findAll()
 * @method DataMigration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataMigrationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataMigration::class);
    }

    public function hasRun(string $migrationName): bool
    {
        $builder = $this->createQueryBuilder('data_migration')
            ->andWhere('data_migration.name = :name')
            ->setParameter('name', $migrationName);

        return count($builder->getQuery()->getResult()) > 0;
    }
}
