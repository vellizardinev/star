<?php

namespace App\Repository;

use App\Entity\Affiliate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Affiliate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Affiliate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Affiliate[]    findAll()
 * @method Affiliate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffiliateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Affiliate::class);
    }

	public function filtered(array $filters = []): Query
	{
		$builder = $this->createQueryBuilder('affiliate');

		if (!empty($filters['name'])) {
			$builder->andWhere('affiliate.name LIKE :name')
				->setParameter('name', "%{$filters['name']}%");
		}

		if (!empty($filters['contact_info'])) {
			$builder->andWhere('affiliate.contactInfo LIKE :contactInfo')
				->setParameter('contactInfo', "%{$filters['contact_info']}%");
		}

		return $builder->getQuery();
	}
}
