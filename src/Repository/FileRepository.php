<?php

namespace App\Repository;

use App\Entity\File;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method File|null find($id, $lockMode = null, $lockVersion = null)
 * @method File|null findOneBy(array $criteria, array $orderBy = null)
 * @method File[]    findAll()
 * @method File[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, File::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('file')
            ->andWhere('file not instance of App\\Entity\\Document')
            ->andWhere('file not instance of App\\Entity\\CreditDocument')
            ->orderBy('file.createdAt', 'desc');

        if (!empty($filters['name'])) {
            $builder->andWhere('file.name LIKE :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

        return $builder->getQuery();
    }
}
