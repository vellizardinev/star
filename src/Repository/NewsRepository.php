<?php

namespace App\Repository;

use App\Entity\Image;
use App\Entity\News;
use App\Enum\NewsCategories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    use FiltersByVisibility;
    use AddsTranslatableHints;

    private $requestStack;

    public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        parent::__construct($registry, News::class);
    }

    public function filtered(array $filters = [], ?bool $onlyVisible = false, ?bool $excludeFeatured = false): Query
    {
        $builder = $this->createQueryBuilder('news')
            ->orderBy('news.createdAt', 'DESC');

        if ($onlyVisible) {
            $this->visible($builder);
        }

        if ($excludeFeatured) {
            if (array_key_exists('category', $filters) && $filters['category'] === 'any') {
                $builder->andWhere("(news.featured = false or news.category != 'news')");
            }

            if (array_key_exists('category', $filters) && $filters['category'] === NewsCategories::NEWS) {
                $builder->andWhere("(news.featured = false or news.category != 'news')");
            }

            if (array_key_exists('category', $filters) && $filters['category'] === NewsCategories::VIDEO) {
                $builder->andWhere("(news.featured = false or news.category != 'video')");
            }

//            if (array_key_exists('category', $filters) && $filters['category'] === NewsCategories::ITIMES) {
//                $builder->andWhere("(news.featured = false or news.category != 'itimes')");
//            }

            if (!array_key_exists('category', $filters)) {
                $builder->andWhere("(news.featured = false or news.category != 'news')");
            }
        }

        if (!empty($filters['title'])) {
            $builder->andWhere('news.title LIKE :title')
                ->setParameter('title', "%{$filters['title']}%");
        }

        if (array_key_exists('category', $filters) && $filters['category'] !== 'any') {
            $builder->andWhere('news.category = :category')
                ->setParameter('category', $filters['category']);
        }

        return $builder->getQuery();
    }

    /**
     * @param Image $image
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findByImage(Image $image)
    {
        $builder = $this->createQueryBuilder('news')
            ->where(':image member of news.images')
            ->setParameter('image', $image)
            ->setMaxResults(1);

        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $slug
     * @return News|null
     * @throws NonUniqueResultException
     */
    public function findBySlug(string $slug): ?News
    {
        $builder = $this->createQueryBuilder('news')
            ->where('news.slug = :slug')
            ->setParameter('slug', $slug)
            ->setMaxResults(1);

        $query = $this->addTranslatableHints($builder, $this->requestStack->getMasterRequest()->getLocale());

        return $query->getOneOrNullResult();
    }

    public function latest(int $count)
    {
        $builder = $this->createQueryBuilder('news')
            ->orderBy('news.createdAt', 'DESC')
            ->setMaxResults((int)$count);

        $this->visible($builder);

        return $builder->getQuery()->getResult();
    }

    public function search($term): ?array
    {
        if (!empty($term)) {
            $builder = $this->createQueryBuilder('news')
	            ->orderBy('news.createdAt', 'DESC')
	            ->andWhere('news.title like :term or news.content like :term or news.introduction like :term')
	            ->setParameter('term', "%{$term}%");

	        $this->visible($builder);

            return $builder->getQuery()->getResult();
        }

        return [];
    }

    /**
     * @param News $news
     * @param bool $previous
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getSibling(News $news, $previous = true)
    {
        $builder = $this->createQueryBuilder('news');
        if ($previous) {
            $builder
                ->orderBy('news.id', 'DESC')
                ->andWhere('news.id < :id');
        } else {
            $builder->andWhere('news.id > :id');
        }
        $builder->setParameter('id', $news->getId());
        $builder->setMaxResults(1);

	    $this->visible($builder);

        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $category
     * @return News|null
     * @throws NonUniqueResultException
     */
    public function featured(string $category = NewsCategories::NEWS): ?News
    {
        if ($category === 'any') {
            $category = NewsCategories::NEWS;
        }

        $builder = $this->createQueryBuilder('news')
            ->orderBy('news.createdAt', 'DESC')
            ->where('news.featured = true')
            ->andWhere('news.category = :category')
            ->setParameter('category', $category)
            ->setMaxResults(1);

        $this->visible($builder);

        return $builder->getQuery()->getOneOrNullResult();
    }

    public function resetFeatured(News $newFeatured)
    {
        $category = $newFeatured->getCategory();

        $query = $this->getEntityManager()
            ->createQuery(
                "UPDATE App\Entity\News news SET news.featured = false where news != :newFeatured and news.category = :category"
            )
            ->setParameter('newFeatured', $newFeatured)
            ->setParameter('category', $category);

        $query->execute();
    }
}
