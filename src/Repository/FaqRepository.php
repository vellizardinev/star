<?php

namespace App\Repository;

use App\Entity\Faq;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Faq|null find($id, $lockMode = null, $lockVersion = null)
 * @method Faq|null findOneBy(array $criteria, array $orderBy = null)
 * @method Faq[]    findAll()
 * @method Faq[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FaqRepository extends ServiceEntityRepository
{
    use FiltersByVisibility;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Faq::class);
    }

    public function filtered(array $filters = [], $onlyVisible = false): Query
    {
        $builder = $this->createQueryBuilder('faq')
            ->orderBy('faq.positionIndex', 'asc');

        if ($onlyVisible) $this->visible($builder);

        if (!empty($filters['question'])) {
            $builder->andWhere('faq.question LIKE :question')
                ->setParameter('question', "%{$filters['question']}%");
        }


        if (!empty($filters['answer'])) {
            $builder->andWhere('faq.answer LIKE :answer')
                ->setParameter('answer', "%{$filters['answer']}%");
        }

        return $builder->getQuery();
    }

    public function search($term): ?array
    {
        if (!empty($term)) {
            $builder = $this->createQueryBuilder('faq')
                ->orderBy('faq.positionIndex', 'asc')
                ->andWhere('faq.question LIKE :term or faq.answer LIKE :term')
                ->setParameter('term', "%{$term}%");

            return $builder->getQuery()->getResult();
        }

        return [];
    }
}
