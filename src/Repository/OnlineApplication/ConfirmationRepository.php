<?php

namespace App\Repository\OnlineApplication;

use App\Entity\OnlineApplication\Confirmation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Confirmation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Confirmation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Confirmation[]    findAll()
 * @method Confirmation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfirmationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Confirmation::class);
    }
}
