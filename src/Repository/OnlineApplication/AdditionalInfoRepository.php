<?php

namespace App\Repository\OnlineApplication;

use App\Entity\OnlineApplication\AdditionalInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdditionalInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdditionalInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdditionalInfo[]    findAll()
 * @method AdditionalInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdditionalInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdditionalInfo::class);
    }
}
