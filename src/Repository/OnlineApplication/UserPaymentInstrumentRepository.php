<?php

namespace App\Repository\OnlineApplication;

use App\Entity\OnlineApplication\UserPaymentInstrument;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserPaymentInstrument|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPaymentInstrument|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPaymentInstrument[]    findAll()
 * @method UserPaymentInstrument[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPaymentInstrumentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserPaymentInstrument::class);
    }
}
