<?php

namespace App\Repository\OnlineApplication;

use App\Entity\OnlineApplication\WorkDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WorkDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkDetails[]    findAll()
 * @method WorkDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkDetailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorkDetails::class);
    }
}
