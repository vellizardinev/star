<?php

namespace App\Repository\OnlineApplication;

use App\Entity\OnlineApplication\CreditDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CreditDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreditDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreditDetails[]    findAll()
 * @method CreditDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreditDetailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CreditDetails::class);
    }
}
