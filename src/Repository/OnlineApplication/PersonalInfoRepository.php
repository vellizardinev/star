<?php

namespace App\Repository\OnlineApplication;

use App\Entity\OnlineApplication\PersonalInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PersonalInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonalInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonalInfo[]    findAll()
 * @method PersonalInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonalInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonalInfo::class);
    }
}
