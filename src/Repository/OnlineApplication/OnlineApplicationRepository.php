<?php

namespace App\Repository\OnlineApplication;

use App\Entity\OnlineApplication\OnlineApplication;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OnlineApplication|null find($id, $lockMode = null, $lockVersion = null)
 * @method OnlineApplication|null findOneBy(array $criteria, array $orderBy = null)
 * @method OnlineApplication[]    findAll()
 * @method OnlineApplication[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OnlineApplicationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OnlineApplication::class);
    }

    public function filtered(array $filters = []): Query
    {
        $builder = $this->createQueryBuilder('creditApplication')
            ->leftJoin('creditApplication.personalInfo', 'personal_info')
            ->leftJoin('creditApplication.workDetails', 'work_details')
            ->orderBy('creditApplication.id', 'desc')
            ->andWhere('creditApplication.isCompleted = true');

        if (!empty($filters['name'])) {
            $builder->andWhere('personal_info.firstName LIKE :name or personal_info.middleName LIKE :name or personal_info.lastName LIKE :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

        if (!empty($filters['telephone'])) {
            $builder->andWhere('personal_info.mobileTelephone LIKE :telephone or work_details.companyTelephone LIKE :telephone')
                ->setParameter('telephone', "%{$filters['telephone']}%");
        }

        if (!empty($filters['start_date'])) {
            $startDate = Carbon::createFromFormat('d/m/Y', $filters['start_date'])->setTime(0, 0);

            $builder->andWhere('creditApplication.createdAt >= :start_date')
                ->setParameter('start_date', $startDate);
        }

        if (!empty($filters['end_date'])) {
            $endDate = Carbon::createFromFormat('d/m/Y', $filters['end_date'])->setTime(24, 0);

            $builder->andWhere('creditApplication.createdAt < :end_date')
                ->setParameter('end_date', $endDate);
        }

        if (isset($filters['erp_status']) && is_numeric($filters['erp_status'])) {

            $builder->andWhere('creditApplication.isSentToErp = :erp_status')
                ->setParameter('erp_status', "{$filters['erp_status']}");
        }

        return $builder->getQuery();
    }

    /**
     * @param int $days
     * @param bool $includeCompleted
     * @param int $maxResults
     * @return OnlineApplication[]
     */
    public function olderThan(int $days = 3, bool $includeCompleted = true, int $maxResults = 50): array
    {
        $referenceDate = Carbon::now()->subDays($days);

        $builder = $this->createQueryBuilder('online_application')
            ->orderBy('online_application.id', 'asc')
            ->andWhere('online_application.createdAt <= :date')
            ->setMaxResults($maxResults)
            ->setParameter('date', $referenceDate)
        ;

        if ($includeCompleted === false) {
            $builder
                ->andWhere('online_application.isCompleted = false');
        }

        return $builder->getQuery()->getResult();
    }

    /**
     * @param int $days
     * @param bool $includeCompleted
     * @param int $maxResults
     * @return OnlineApplication[]
     */
    public function completedWithFilesOlderThan(int $days = 3, int $maxResults = 50): array
    {
        $referenceDate = Carbon::now()->subDays($days);

        $builder = $this->createQueryBuilder('online_application')
            ->innerJoin('online_application.uploadedDocuments', 'uploaded_documents')
            ->orderBy('online_application.id', 'asc')
            ->andWhere('online_application.createdAt <= :date')
            ->andWhere('online_application.isCompleted = true')
            ->andWhere('(uploaded_documents.oldStylePassportFirstPage is not null or uploaded_documents.newStylePassportPhotoPage is not null)')
            ->setMaxResults($maxResults)
            ->setParameter('date', $referenceDate)
        ;

        return $builder->getQuery()->getResult();
    }

    /**
     * @param string $email
     * @return OnlineApplication[]
     */
    public function forEmail(string $email): array
    {
        $tenMinutesAgo = Carbon::now()->subMinutes(10);

        $builder = $this->createQueryBuilder('online_application')
            ->leftJoin('online_application.personalInfo', 'personal_info')
            ->andWhere('online_application.isCompleted = true')
            ->andWhere('personal_info.email = :email')
            ->andWhere('online_application.createdAt > :tenMinutesAgo')
            ->setParameter('email', $email)
            ->setParameter('tenMinutesAgo', $tenMinutesAgo)
        ;

        return $builder->getQuery()->getResult();
    }

    public function getExportData(array $filters = []): array
    {
        $builder = $this
            ->createQueryBuilder('online_application')
            ->orderBy('online_application.createdAt', 'ASC')
            ->leftJoin('online_application.personalInfo', 'personal_info')
            ->andWhere('online_application.isCompleted = true')
        ;

        if (!empty($filters['name'])) {
            $builder->andWhere('personal_info.firstName LIKE :name or personal_info.lastName like :name')
                ->setParameter('name', "%{$filters['name']}%");
        }

        if (!empty($filters['telephone'])) {
            $builder->andWhere('personal_info.mobileTelephone LIKE :telephone')
                ->setParameter('telephone', "%{$filters['telephone']}%");
        }

        if (!empty($filters['start_date'])) {
            $startDate = Carbon::createFromFormat('d/m/Y', $filters['start_date'])->setTime(0, 0);

            $builder->andWhere('online_application.createdAt >= :start_date')
                ->setParameter('start_date', $startDate);
        }

        if (!empty($filters['end_date'])) {
            $endDate = Carbon::createFromFormat('d/m/Y', $filters['end_date'])->setTime(24, 0);

            $builder->andWhere('online_application.createdAt < :end_date')
                ->setParameter('end_date', $endDate);
        }

        return $builder->getQuery()->getResult();
    }

    public function withConfirmationIdGreaterThan(?int $confirmationId)
    {
        $builder = $this->createQueryBuilder('online_application')
            ->andWhere('online_application.isCompleted = true')
            ->join('online_application.confirmation', 'confirmation')
            ->orderBy('online_application.id', 'ASC');

        if ($confirmationId) {
            $builder->andWhere('confirmation.id > :confirmationId')->setParameter('confirmationId', $confirmationId);
        }

        return $builder->getQuery()->getResult();
    }

    /**
     * @param int $days
     * @param bool $includeCompleted
     * @param int $maxResults
     * @param bool $checkForCompletedApplications
     * @return OnlineApplication[]
     */
    public function unfinishedFromYesterday(int $maxResults = 15, bool $checkForCompletedApplications = true): array
    {
        $startDate = Carbon::now()->subDay()->startOfDay();
        $endDate = Carbon::now()->subDay()->endOfDay();

        $builder = $this->createQueryBuilder('online_application')
            ->join('online_application.personalInfo', 'personal_info')
            ->andWhere('personal_info is not null')
            ->andWhere('online_application.createdAt between :startDate and :endDate')
            ->andWhere('online_application.isCompleted = false')
            ->andWhere('online_application.hasSentReminderEmail = false')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->setMaxResults($maxResults)
            ->addOrderBy('online_application.id', 'asc');

        if ($checkForCompletedApplications) {
            $subBuilder = $this->getEntityManager()
                ->createQueryBuilder()
                ->select('count(completed_application)')
                ->from('App\\Entity\\OnlineApplication\\OnlineApplication', 'completed_application')
                ->join('completed_application.personalInfo', 'completed_personal_info')
                ->andWhere('completed_application.isCompleted = true')
                ->andWhere('completed_personal_info.personalIdentificationNumber = personal_info.personalIdentificationNumber')
                ->andWhere('completed_application.createdAt > online_application.createdAt');

            $builder
                ->andWhere('0 = (' . $subBuilder->getQuery()->getDQL() . ')')
            ;
        }

        return $builder->getQuery()->getResult();
    }
}
