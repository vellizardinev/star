<?php

namespace App\Repository;

use App\Entity\CreditDocument;
use App\Enum\CreditDocumentType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CreditDocument|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreditDocument|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreditDocument[]    findAll()
 * @method CreditDocument[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreditDocumentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CreditDocument::class);
    }

    public function forCreditApplication(string $erpId, $type = CreditDocumentType::INITIAL): array
    {
        $builder = $this->createQueryBuilder('credit_document')
            ->andWhere('credit_document.erpId = :erpId')
            ->setParameter('erpId', $erpId)
            ;

        if ($type !== CreditDocumentType::ALL) {
            $builder
                ->andWhere('credit_document.type = :type')
                ->setParameter('type', $type)
                ;
        }

        return $builder->getQuery()->getResult();
    }

    public function hasCreditDocuments(?string $erpId = null): bool
    {
        if (null === $erpId) {
            return false;
        }

        $builder = $this->createQueryBuilder('credit_document')
            ->select('count(credit_document.id)')
            ->andWhere('credit_document.erpId = :erpId')
            ->setParameter('erpId', $erpId)
            ->setMaxResults(1)
        ;

        return $builder->getQuery()->getSingleScalarResult() > 0;
    }
}
