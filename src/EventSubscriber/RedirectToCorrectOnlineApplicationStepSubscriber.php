<?php


namespace App\EventSubscriber;


use App\Annotation\RedirectToCorrectOnlineApplicationStep;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Service\OnlineApplicationSteps;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

class RedirectToCorrectOnlineApplicationStepSubscriber implements EventSubscriberInterface
{
    protected $annotationReader;

    private $manager;

    private $router;

    private $onlineApplicationSteps;

    public function __construct(
        Reader $annotationReader,
        EntityManagerInterface $manager,
        RouterInterface $router,
        OnlineApplicationSteps $onlineApplicationSteps
    ) {
        $this->annotationReader = $annotationReader;
        $this->manager = $manager;
        $this->router = $router;
        $this->onlineApplicationSteps = $onlineApplicationSteps;
    }

    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return [
            KernelEvents::CONTROLLER => [
                ['checkStep', 10],
            ],
        ];
    }

    /**
     * @param $event
     * @throws ReflectionException
     */
    public function checkStep(ControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        $action = new ReflectionMethod($controller[0], $controller[1]);
        $class = new ReflectionClass($controller[0]);

        $annotation = $this
            ->annotationReader
            ->getMethodAnnotation($action, RedirectToCorrectOnlineApplicationStep::class);

        if (!$annotation) {
            $annotation = $this
                ->annotationReader
                ->getClassAnnotation($class, RedirectToCorrectOnlineApplicationStep::class);
        }

        if (!($annotation instanceof RedirectToCorrectOnlineApplicationStep)) {
            return;
        }

        $hash = $event->getRequest()->attributes->get('hash');

        if ($hash === null) {
            return;
        }

        $application = $this->manager->getRepository(OnlineApplication::class)->findOneBy(['hash' => $hash]);

        if (!$application) {
            $event->setController(
                function () {
                    return new RedirectResponse($this->router->generate('static_pages.home_page'));
                }
            );

            return;
        }

        if ($application->getIsCompleted()) {
            $event->setController(
                function () {
                    return new RedirectResponse($this->router->generate('static_pages.home_page'));
                }
            );

            return;
        }

        $routeForNextStep = $this->onlineApplicationSteps->routeForUncompletedStepBefore($application, $event->getController()[1]);

        if ($routeForNextStep !== $event->getRequest()->getPathInfo()) {
            $event->setController(
                function () use ($routeForNextStep) {
                    return new RedirectResponse($routeForNextStep);
                }
            );
        }
    }
}
