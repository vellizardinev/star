<?php


namespace App\EventSubscriber;


use App\Annotation\NeedsActiveCustomerAccount;
use App\Entity\Customer;
use Doctrine\Common\Annotations\Reader;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class NeedsActiveCustomerAccountSubscriber implements EventSubscriberInterface
{
    protected $annotationReader;

    private $router;

    private $token;

    private $request;

    public function __construct(
        Reader $annotationReader,
        RouterInterface $router,
        TokenStorageInterface $token,
        RequestStack $requestStack
    ) {
        $this->annotationReader = $annotationReader;
        $this->router = $router;
        $this->token = $token;
        $this->request = $requestStack->getMasterRequest();
    }

    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return [
            KernelEvents::CONTROLLER => [
                ['checkAccount', 10],
            ],
        ];
    }

    /**
     * @param $event
     * @throws ReflectionException
     */
    public function checkAccount(ControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        $action = new ReflectionMethod($controller[0], $controller[1]);
        $class = new ReflectionClass($controller[0]);

        $annotation = $this
            ->annotationReader
            ->getMethodAnnotation($action, NeedsActiveCustomerAccount::class);

        if (!$annotation) {
            $annotation = $this
                ->annotationReader
                ->getClassAnnotation($class, NeedsActiveCustomerAccount::class);
        }

        if (!($annotation instanceof NeedsActiveCustomerAccount)) {
            return;
        }

        $customer = $this->token->getToken()->getUser();

        if ($customer === null || !$customer instanceof Customer) {
            $this->request->getSession()->set('_security.main.target_path', $this->request->getUri());

            $event->setController(
                function () {
                    return new RedirectResponse($this->router->generate('customer_login.login'));
                }
            );

            return;
        }

        if ($customer->isActive() === false) {
            $event->setController(
                function () {
                    return new RedirectResponse($this->router->generate('customer_registration.verification'));
                }
            );
        }
    }
}
