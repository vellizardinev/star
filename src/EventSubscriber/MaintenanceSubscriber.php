<?php


namespace App\EventSubscriber;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MaintenanceSubscriber implements EventSubscriberInterface
{
    private $maintenanceMode;

    private $twig;

    public function __construct(int $maintenanceMode, Environment $twig)
    {
        $this->maintenanceMode = (bool) $maintenanceMode;
        $this->twig = $twig;
    }

    /**
     * @param RequestEvent $event
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if ($this->maintenanceMode === false) {
            return;
        }

        $html = $this->twig->render('maintenance_page.html.twig');
        $response = new Response($html);

        $event->setResponse($response);
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['onKernelRequest', 10],
            ],
        ];
    }
}