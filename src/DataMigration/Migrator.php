<?php


namespace App\DataMigration;


use App\Entity\DataMigration as DataMigrationEntity;
use App\Message\DataMigrations\AddCreditApplicationPageModuleItem;
use App\Message\DataMigrations\AddIWeekCredit;
use App\Message\DataMigrations\AddOfficeListModuleItem;
use App\Message\DataMigrations\AddSettingForFailedOnlineApplications;
use App\Message\DataMigrations\DeleteCityVinnitsia;
use App\Message\DataMigrations\DeleteFailedOnlineApplicationMail;
use App\Message\DataMigrations\DeleteOrphanedFileEntities;
use App\Message\DataMigrations\GetCreditDataFromApi;
use App\Message\DataMigrations\GetNomenclatures;
use App\Message\DataMigrations\LoadIWeekCreditData;
use App\Message\DataMigrations\RemoveOldMailTemplateFailedOnlineApplication;
use App\Message\DataMigrations\ResetAdminUsersRoles;
use App\Message\DataMigrations\UpdateAdminPassword;
use App\Message\DataMigrations\UpdateCities;
use App\Message\DataMigrations\AddIWeekCreditInFooter;
use App\Message\DataMigrations\UpdateLongFormMails;
use App\Message\DataMigrations\UpdateLongFormMessages;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

class Migrator
{
    private $migrations = [];

    private $manager;

    private $messageBus;

    public function __construct(EntityManagerInterface $manager, MessageBusInterface $messageBus)
    {
        $this->register();
        $this->manager = $manager;
        $this->messageBus = $messageBus;
    }

    public function addData(SymfonyStyle $io)
    {
        foreach ($this->migrations as $migration) {
            $hasRun = $this->manager
                ->getRepository(DataMigrationEntity::class)
                ->hasRun($migration);

            if ($hasRun) {
            	$io->warning(sprintf('%s is already migrated!', $migration));
                continue;
            }

            $this->messageBus->dispatch(new $migration());

            $migrationEntry = new DataMigrationEntity($migration);
            $this->manager->persist($migrationEntry);

	        $io->success(sprintf('%s is migrated successfully.', $migration));
        }

        $this->manager->flush();
    }
    public function addAll(): int
    {
        $count = 0;

        foreach ($this->migrations as $migration) {
            $hasRun = $this->manager
                ->getRepository(DataMigrationEntity::class)
                ->hasRun($migration);

            if ($hasRun) {
                continue;
            }

            $migrationEntry = new DataMigrationEntity($migration);
            $this->manager->persist($migrationEntry);
            $this->manager->flush();
            $count++;
        }

        return $count;
    }


    private function register()
    {
        $this->migrations = [
            // Add migration message classes here using their FQCN
            UpdateLongFormMessages::class,
            DeleteOrphanedFileEntities::class,
            AddIWeekCredit::class,
            LoadIWeekCreditData::class,
            UpdateLongFormMails::class,
            AddOfficeListModuleItem::class,
            AddIWeekCreditInFooter::class,
            UpdateCities::class,
            GetNomenclatures::class,
            GetCreditDataFromApi::class,
            AddSettingForFailedOnlineApplications::class,
            RemoveOldMailTemplateFailedOnlineApplication::class,
            DeleteFailedOnlineApplicationMail::class,
            DeleteCityVinnitsia::class,
            UpdateAdminPassword::class,
            ResetAdminUsersRoles::class,
            AddCreditApplicationPageModuleItem::class,
        ];
    }
}