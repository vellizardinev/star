<?php

namespace App\Twig;

use App\Entity\Credit;
use App\Entity\CreditData;
use App\Entity\CreditDocument;
use App\Entity\Customer;
use App\Entity\File;
use App\Entity\Image;
use App\Entity\Message;
use App\Entity\Nomenclature;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Entity\Page;
use App\Entity\Setting;
use App\Enum\ActiveCreditStatus;
use App\Enum\CreditApplicationFileStatus;
use App\Enum\CreditApplicationFileTypes;
use App\Enum\CreditPurposes;
use App\Enum\EmploymentTypes;
use App\Enum\FamilyStatuses;
use App\Enum\InformationSources;
use App\Enum\Pages;
use App\Enum\RelationshipTypes;
use App\Enum\Settings;
use App\File\FileManager;
use App\Security\PermissionsService;
use App\Service\MobileApi\DataTransferObject\CreditApplication;
use App\Service\RandomString;
use Carbon\Carbon;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class UtilitiesExtension extends AbstractExtension implements ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $locales;
    private $uploadsBaseUrl;

    /**
     * UtilitiesExtension constructor.
     * @param ContainerInterface $container
     * @param $locales
     * @param $uploadsBaseUrl
     */
    public function __construct(ContainerInterface $container, $locales, $uploadsBaseUrl)
    {
        $this->container = $container;
        $this->locales = $locales;
        $this->uploadsBaseUrl = $uploadsBaseUrl;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('formatted_date', [$this, 'formattedDate']),
            new TwigFilter('formatted_date_time', [$this, 'formattedDateTime']),
            new TwigFilter('limit_chars', [$this, 'limitCharacters']),
            new TwigFilter('class_name', [$this, 'getClassName']),
            new TwigFilter('relative_image_path', [$this, 'relativeImagePath']),
            new TwigFilter('conditional_date_time', [$this, 'conditionalFormattedDateTime']),
            new TwigFilter('currency_symbol', [$this, 'currencySymbol']),
            new TwigFilter('employment_types', [$this, 'employmentTypeTranslation']),
            new TwigFilter('relationship_types', [$this, 'relationshipTypeTranslation']),
            new TwigFilter('credit_purposes', [$this, 'creditPurposeTranslation']),
            new TwigFilter('family_statuses', [$this, 'familyStatusTranslation']),
            new TwigFilter('information_sources', [$this, 'informationSourcesTranslation']),
            new TwigFilter('active_credit_status', [$this, 'activeCreditStatusTranslation']),
            new TwigFilter('credit_application_file_status', [$this, 'creditApplicationFileStatusTranslation']),
            new TwigFilter('credit_application_file_type', [$this, 'creditApplicationFileTypesTranslation']),
            new TwigFilter('nomenclature_item', [$this, 'nomenclatureItem']),
            new TwigFilter('set_locale', [$this, 'setLocale']),
            new TwigFilter('add_country_code', [$this, 'addCountryCode']),
            new TwigFilter('boolean_to_string', [$this, 'booleanToString']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('uploaded_file_asset', [$this, 'getUploadedFilePath']),
            new TwigFunction('uploaded_image_asset', [$this, 'getUploadedImagePath']),
            new TwigFunction('uploaded_credit_document_asset', [$this, 'getUploadedCreditDocumentPath']),
            new TwigFunction('has_permission', [$this, 'hasPermission']),
            new TwigFunction('app_locales', [$this, 'locales']),
            new TwigFunction('user_defined_page_path', [$this, 'userDefinedPagePath']),
	        new TwigFunction('get_setting', [$this, 'getSetting']),
	        new TwigFunction('protection_of_personal_data_page_path', [$this, 'protectionOfPersonalDataPage']),
	        new TwigFunction('protection_of_applicant_personal_data_page_path', [$this, 'protectionOfApplicantsPersonalDataPage']),
	        new TwigFunction('random_string', [$this, 'randomString']),
	        new TwigFunction('default_credit_slug', [$this, 'iCreditSlug']),
            new TwigFunction('credit_defaults', [$this, 'creditDefaults']),
	        new TwigFunction('get_css_class_for_step', [$this, 'onlineApplicationStepCssClass']),
	        new TwigFunction('has_credit_documents', [$this, 'hasCreditDocuments']),
            new TwigFunction('unread_messages_count', [$this, 'unreadMessagesForCustomer']),
        ];
    }

    public function formattedDate(?DateTime $value): string
    {
        if (!$value) {
            return '';
        }

        /** @var Setting $setting */
        $setting = $this->container->get(EntityManagerInterface::class)->getRepository(Setting::class)->get(
            Settings::DATE_FORMAT
        );

        return $value->format($setting->getValue());
    }

    public function formattedDateTime(?DateTime $value): string
    {
        if (!$value) {
            return '';
        }

        /** @var Setting $setting */
        $setting = $this->container->get(EntityManagerInterface::class)->getRepository(Setting::class)->get(
            Settings::DATE_TIME_FORMAT
        );

        return $value->format($setting->getValue());
    }

    public function getUploadedFilePath(File $file)
    {
        return $this->container->get(FileManager::class)->publicFilePath($file);
    }

    public function getUploadedImagePath(Image $image)
    {
        return $this->container->get(FileManager::class)->publicImagePath($image);
    }

    /**
     * @param $permission
     * @return bool
     */
    public function hasPermission($permission)
    {
        return $this->container->get(PermissionsService::class)->check($permission);
    }

    /**
     * Returns an array of service types required by such instances, optionally keyed by the service names used internally.
     *
     * For mandatory dependencies:
     *
     *  * ['logger' => 'Psr\Log\LoggerInterface'] means the objects use the "logger" name
     *    internally to fetch a service which must implement Psr\Log\LoggerInterface.
     *  * ['loggers' => 'Psr\Log\LoggerInterface[]'] means the objects use the "loggers" name
     *    internally to fetch an iterable of Psr\Log\LoggerInterface instances.
     *  * ['Psr\Log\LoggerInterface'] is a shortcut for
     *  * ['Psr\Log\LoggerInterface' => 'Psr\Log\LoggerInterface']
     *
     * otherwise:
     *
     *  * ['logger' => '?Psr\Log\LoggerInterface'] denotes an optional dependency
     *  * ['loggers' => '?Psr\Log\LoggerInterface[]'] denotes an optional iterable dependency
     *  * ['?Psr\Log\LoggerInterface'] is a shortcut for
     *  * ['Psr\Log\LoggerInterface' => '?Psr\Log\LoggerInterface']
     *
     * @return array The required service types, optionally keyed by service names
     */
    public static function getSubscribedServices()
    {
        return [
            FileManager::class,
            EntityManagerInterface::class,
            PermissionsService::class,
            RequestStack::class,
            TranslatorInterface::class,
        ];
    }

    public function locales()
    {
        return explode('|', $this->locales);
    }

    public function limitCharacters(?string $text, int $limit)
    {
        if (!$text) {
            return '';
        }

        $content = strip_tags($text);

        if (mb_strlen($content) > $limit) {
            return mb_substr($content, 0, $limit) . '...';
        } else {
            return $content;
        }
    }

    public function userDefinedPagePath(Page $page)
    {
        $locale = $this->container->get(RequestStack::class)->getMasterRequest()->getLocale();

        if ($locale === 'ua') {
            $prefix = '';
        } else {
            $prefix = "/$locale";
        }

	    return sprintf(
		    '%s/%s',
		    $prefix,
		    $page->getPath()
	    );

    }

	public function protectionOfPersonalDataPage()
	{
		$gdprPage = $this->getGDPRPage();

		return sprintf(
			'/%s/%s#%s',
			$this->container->get(RequestStack::class)->getMasterRequest()->getLocale(),
			$gdprPage,
			'protectia-datelor'
		);
	}

	public function protectionOfApplicantsPersonalDataPage()
	{
		$gdprPage = $this->getGDPRPage();

		return sprintf(
			'/%s/%s#%s',
			$this->container->get(RequestStack::class)->getMasterRequest()->getLocale(),
			$gdprPage,
			'declaratie-confidentialitate-date-candidati'
		);
	}

	private function getGDPRPage()
	{
		$cache = new FilesystemAdapter();

		$gdpr = $cache->getItem('routes.gdpr');

		if (!$gdpr->isHit()) {
			$page = $this->container->get(EntityManagerInterface::class)->getRepository(Page::class)->findOneBy(['name' => Pages::TERMS_AND_CONDITIONS]);
			if($page)
			{
				$gdpr->set($page->getPath());
			}
		}

		return $gdpr->get();
	}

	/**
	 * @param $object
	 * @return string
	 * @throws ReflectionException
	 */
	public function getClassName($object)
	{
		return (new ReflectionClass($object))->getShortName();
	}

	public function getSetting(string $settingName)
	{
		$setting = $this->container->get(EntityManagerInterface::class)->getRepository(Setting::class)->get(
			$settingName
		);

		return $setting->getValue();
	}

	public function randomString(): string
    {
        return RandomString::generate(4);
    }

    public function relativeImagePath(string $imageUrl)
    {
        return str_replace($this->uploadsBaseUrl, '', $imageUrl);
    }

    public function iCreditSlug(): string
    {
        $defaultCredit = $this->container->get(EntityManagerInterface::class)->getRepository(Credit::class)->findOneBy([]);

        return $defaultCredit->getSlug();
    }

    public function creditDefaults(string $slug = 'icredit'): CreditData
    {
        $credit = $this->container
            ->get(EntityManagerInterface::class)
            ->getRepository(Credit::class)
            ->findOneBy(['slug' => $slug]);

        if (!$credit) {
            $credit = $this->container
                ->get(EntityManagerInterface::class)
                ->getRepository(Credit::class)
                ->findOneBy([]);
        }

        return $this->container
            ->get(EntityManagerInterface::class)
            ->getRepository(CreditData::class)
            ->getDefaults($credit);
    }

    public function conditionalFormattedDateTime($value)
    {
        if ($value instanceof DateTimeInterface) {
            return $value->format('d.m.Y H:i');
        }

        if (is_numeric($value)) {
            return $value;
        }

        try {
            $date = Carbon::createFromTimeString($value);

            if ($date) {
                return $date->format('d.m.Y H:i');
            } else {
                return $value;
            }
        } catch (Exception $exception) {
            return $value;
        }
    }

    public function currencySymbol(string $value): string
    {
        return $value . ' UAH';
    }

    /**
     * @param string $value
     * @return string
     * @throws ReflectionException
     */
    public function employmentTypeTranslation(string $value): string
    {
        if (EmploymentTypes::hasValue($value)) {
            return EmploymentTypes::getTranslationKey($value);
        } else {
            return $value;
        }
    }

    /**
     * @param string $value
     * @return string
     * @throws ReflectionException
     */
    public function relationshipTypeTranslation(string $value): string
    {
        if (RelationshipTypes::hasValue($value)) {
            return RelationshipTypes::getTranslationKey($value);
        } else {
            return $value;
        }
    }

    /**
     * @param string $value
     * @return string
     * @throws ReflectionException
     */
    public function creditPurposeTranslation(string $value): string
    {
        if (CreditPurposes::hasValue($value)) {
            return CreditPurposes::getTranslationKey($value);
        } else {
            return $value;
        }
    }

    /**
     * @param string $value
     * @return string
     * @throws ReflectionException
     */
    public function familyStatusTranslation(string $value): string
    {
        if (FamilyStatuses::hasValue($value)) {
            return FamilyStatuses::getTranslationKey($value);
        } else {
            return $value;
        }
    }

    /**
     * @param string $value
     * @return string
     * @throws ReflectionException
     */
    public function informationSourcesTranslation(string $value): string
    {
        if (InformationSources::hasValue($value)) {
            return InformationSources::getTranslationKey($value);
        } else {
            return $value;
        }
    }

    /**
     * @param string $value
     * @return string
     * @throws ReflectionException
     */
    public function activeCreditStatusTranslation(string $value): string
    {
        if (ActiveCreditStatus::hasValue($value)) {
            return ActiveCreditStatus::getTranslationKey($value);
        } else {
            return $value;
        }
    }

    /**
     * @param string $value
     * @return string
     * @throws ReflectionException
     */
    public function creditApplicationFileStatusTranslation(string $value): string
    {
        if (CreditApplicationFileStatus::hasValue($value)) {
            return CreditApplicationFileStatus::getTranslationKey($value);
        } else {
            return $value;
        }
    }

    /**
     * @param string $value
     * @return string
     * @throws ReflectionException
     */
    public function creditApplicationFileTypesTranslation(string $value): string
    {
        if (CreditApplicationFileTypes::hasValue($value)) {
            return CreditApplicationFileTypes::getTranslationKey($value);
        } else {
            return $value;
        }
    }

	public function onlineApplicationStepCssClass(int $currentStep, int $step, ?OnlineApplication $application = null): string
	{
		$class = 'circle ';

		if ($currentStep === $step) {
			$class .= ' circle-current';
		}

		switch ($currentStep) {
			case 1:
				if ($application) {
					$class .= ' circle-clickable';

					if ($currentStep !== $step) {
						$class .= ' circle-completed';
					}
				}
				break;
			case 2:
				if ($application && $application->getCreditDetails() !== null) {
					$class .= ' circle-clickable';

					if ($currentStep !== $step && $application->getPersonalInfo() !== null) {
						$class .= ' circle-completed';
					}
				}
				break;
			case 3:
				if ($application && $application->getPersonalInfo() !== null && $application->getPersonalInfo()->getId() !== null) {
					$class .= ' circle-clickable';

					if ($currentStep !== $step && $application->getAddress() !== null) {
						$class .= ' circle-completed';
					}
				}
				break;
			case 4:
				if ($application && $application->getContactPerson() !== null) {
					$class .= ' circle-clickable';

					if ($currentStep !== $step && $application->getAddress() !== null) {
						$class .= ' circle-completed';
					}
				}
				break;
			case 5:
				if ($application && $application->getAdditionalInfo() !== null) {
					$class .= ' circle-clickable';
				}

                if ($currentStep !== $step && $application->getAdditionalInfo() !== null) {
                    $class .= ' circle-completed';
                }

				break;
			case 6:
				if ($application && $application->getConfirmation() !== null) {
					$class .= ' circle-clickable';
				}
				break;
			default :
				break;
		}

		return $class;
	}

    public function nomenclatureItem(string $guid): string
    {
        $nomenclatureObject = $this->container
            ->get(EntityManagerInterface::class)
            ->getRepository(Nomenclature::class)
            ->findOneBy(['guid' => $guid]);

        if (!$nomenclatureObject) {
            return $guid;
        }

        /** @var $nomenclatureObject Nomenclature */
        return $nomenclatureObject->getText();
    }

    public function setLocale($translatableEntity, string $locale): void
    {
        if (method_exists($translatableEntity, 'setTranslatableLocale')) {
            $translatableEntity->setTranslatableLocale($locale);
            $this->container
                ->get(EntityManagerInterface::class)
                ->refresh($translatableEntity);
        }
    }

    public function getUploadedCreditDocumentPath(CreditDocument $creditDocument)
    {
        return $this->container->get(FileManager::class)->publicCreditDocumentPath($creditDocument);
    }

    public function addCountryCode(string $telephone): string
    {
        return '+380' . $telephone;
    }

    public function hasCreditDocuments(CreditApplication $creditApplication): bool
    {
        return $this->container
            ->get(EntityManagerInterface::class)
            ->getRepository(CreditDocument::class)
            ->hasCreditDocuments($creditApplication->getCreditId());
    }

    public function unreadMessagesForCustomer(Customer $customer): int
    {
        return $this->container
            ->get(EntityManagerInterface::class)
            ->getRepository(Message::class)
            ->unreadMessagesCountForCustomer($customer);
    }

    public function booleanToString(bool $value): string
    {
        $translator = $this->container
            ->get(TranslatorInterface::class);

        if ($value) {
            return $translator->trans('booleantypes.yes', [], 'form_long');
        } else {
            return $translator->trans('booleantypes.false', [], 'form_long');
        }
    }
}
