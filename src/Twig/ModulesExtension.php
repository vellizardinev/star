<?php

namespace App\Twig;

use App\Entity\ModuleItem;
use App\Entity\UserDefinedModule;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Container\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ModulesExtension extends AbstractExtension
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * UtilitiesExtension constructor.
     * @param ContainerInterface $container
     * @param EntityManagerInterface $manager
     */
    public function __construct(ContainerInterface $container, EntityManagerInterface $manager)
    {
        $this->container = $container;
        $this->manager = $manager;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('render_module', [$this, 'renderModule'], ['is_safe' => ['html']]),
            new TwigFunction('render_named_module', [$this, 'renderNamedModuleItem'], ['is_safe' => ['html']]),
        ];
    }
    
	/**
	 * @param ModuleItem $moduleItem
	 * @return string|null
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	public function renderModule(ModuleItem $moduleItem)
    {
        if ($moduleItem->getModule() instanceof UserDefinedModule) {
            /** @var $module UserDefinedModule */
            $module = $moduleItem->getModule();

            return $module->getContent();
        }

        $rendererClass = $this->extractRendererClass($moduleItem);

	    if ($this->container->has($rendererClass))
	    {
		    return $this->container->get($rendererClass)->render($moduleItem);
	    }

	    return $this->container->get( 'App\\Module\\Renderer\\DefaultRenderer')->render($moduleItem);
    }

    /**
     * @param string $name
     * @param array|null $props
     * @return string|null
     * @throws Exception
     */
    public function renderNamedModuleItem(string $name, ?array $props = [])
    {
        /** @var ModuleItem $moduleItem */
        $moduleItem = $this->manager
            ->getRepository(ModuleItem::class)
            ->findOneByNameOrDescription($name);

        if (!$moduleItem) {
            throw new Exception(sprintf('Could not find module item named %s.', $name));
        }

        $rendererClass = $this->extractRendererClass($moduleItem);

        if ($this->container->has($rendererClass))
        {
            return $this->container->get($rendererClass)->render($moduleItem, $props);
        }

        return $this->container->get( 'App\\Module\\Renderer\\DefaultRenderer')->render($moduleItem, $props);
    }

    private function extractRendererClass(ModuleItem $moduleItem)
    {
        $name = $moduleItem->getModule() !== null ? $moduleItem->getModule()->getName() : $moduleItem->getName();

        $rendererClass = preg_split("/[\s,]+/", $name);

        $rendererClass = array_map(
            function ($item) {
                return ucfirst($item);
            },
            $rendererClass
        );

        $rendererClass = 'App\\Module\\Renderer\\' . implode('', $rendererClass);

        return $rendererClass;
    }
}
