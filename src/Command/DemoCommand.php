<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DemoCommand extends Command
{
    protected static $defaultName = 'demo:command';

    protected function configure()
    {
        $this
            ->setDescription('Empty command for demonstration and testing purposes');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->success("Executing the Demo Command.");

        return self::SUCCESS;
    }
}
