<?php

namespace App\Command;

use App\Entity\Credit;
use App\Service\MobileApi\CreditProducts;
use App\Service\MobileApi\UpdateCreditData;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class UpdateCreditDataCommand extends Command
{
    protected static $defaultName = 'applications:update-credit-data';

    private $manager;

    private $creditProductsService;

    private $updateCreditDataService;

    public function __construct(EntityManagerInterface $manager, CreditProducts $creditProducts, UpdateCreditData $updateCreditDataService)
    {
        $this->manager = $manager;
        $this->creditProductsService = $creditProducts;
        parent::__construct();
        $this->updateCreditDataService = $updateCreditDataService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Updates credit data from SmartIT API.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $creditProducts = $this->creditProductsService->creditProducts();

        if (count($creditProducts) === 0) {
            $output->writeln('<error>The API did not return credit data.</error>');

            return;
        }

        $this->updateCreditDataService->update($creditProducts);

        return self::SUCCESS;
    }
}
