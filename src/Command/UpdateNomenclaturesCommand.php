<?php

namespace App\Command;

use App\Service\MobileApi\Nomenclatures;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class UpdateNomenclaturesCommand extends Command
{
    protected static $defaultName = 'applications:update-nomenclatures';

    private $nomenclaturesService;

    public function __construct(Nomenclatures $nomenclaturesService)
    {
        $this->nomenclaturesService = $nomenclaturesService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Updates nomenclatures data from SmartIT API.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws NonUniqueResultException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->nomenclaturesService->update();

        return self::SUCCESS;
    }
}
