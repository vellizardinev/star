<?php

namespace App\Command;

use App\Schedule\Scheduler;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RunScheduleCommand extends Command
{
    protected static $defaultName = 'schedule:run';

    /**
     * @var Scheduler
     */
    private $scheduler;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * RunScheduleCommand constructor.
     * @param Scheduler $scheduler
     * @param LoggerInterface $logger
     */
    public function __construct(Scheduler $scheduler, LoggerInterface $logger)
    {
        parent::__construct();
        $this->scheduler = $scheduler;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this->setDescription('Runs the scheduler, which in turn executes scheduled commands');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        pcntl_signal(SIGTERM, [$this, 'stopCommand']);
        pcntl_signal(SIGINT, [$this, 'stopCommand']);
        pcntl_async_signals(true);

        $io = new SymfonyStyle($input, $output);

        try {
            $this->scheduler->run();
            $io->success("The scheduler completed successfully.");
            $this->logger->info("The scheduler completed successfully.");
        } catch (Exception $e) {
            $this->logger->error(sprintf("There was a problem with the scheduler. It finished with error: %s", $e->getMessage()));
        }

        return self::SUCCESS;
    }

    protected function stopCommand()
    {
        $this->scheduler->end();
        die();
    }
}
