<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MaintenanceCommand extends Command
{
    protected static $defaultName = 'sirius:maintenance';

    private $projectDir;

    public function __construct(string $projectDir)
    {
        $this->projectDir = $projectDir;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Turns "on" or "off" the application\'s maintenance mode')
            ->addArgument('mode', InputArgument::REQUIRED, 'The value of maintenance mode - either "on" or "off".')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $maintenanceMode = $input->getArgument('mode') === 'on' ? 1 : 0;

        $this->changeMode($maintenanceMode, '.env');
        $this->changeMode($maintenanceMode, '.env.local');

        return self::SUCCESS;
    }

    private function changeMode(int $mode, string $fileName): void
    {
        $envFilePath = $this->projectDir . DIRECTORY_SEPARATOR . $fileName;

        if (file_exists($envFilePath) === false) {
            return;
        }

        $command = 'sed -i -E "s/^MAINTENANCE_MODE=.*$/MAINTENANCE_MODE=' . $mode . '/g" ' . $envFilePath;

        shell_exec($command);

        // Remove temp file
        $path = $this->projectDir . DIRECTORY_SEPARATOR . $fileName . '-E';

        if (file_exists($path)) {
            $command = 'rm ' . $path;
            shell_exec($command);
        }
    }
}
