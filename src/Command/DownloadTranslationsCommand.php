<?php

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DownloadTranslationsCommand extends Command
{
    protected static $defaultName = 'sirius:translation:download';

    private $projectDir;

    private $httpClient;

    private $logger;

    private $siriusTranslationsAuthToken;

    private $siriusTranslationsUrl;

    public function __construct(HttpClientInterface $httpClient, string $projectDir, LoggerInterface $logger, string $siriusTranslationsAuthToken, string $siriusTranslationsUrl)
    {
        $this->projectDir = $projectDir;
        $this->httpClient = $httpClient;
        $this->logger = $logger;
        $this->siriusTranslationsAuthToken = $siriusTranslationsAuthToken;
        $this->siriusTranslationsUrl = $siriusTranslationsUrl;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Uploads local translations inside yaml files to remote Sirius Translations app.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws DecodingExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $requestData = [
            'verify_peer' => false,
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Auth-Token' => $this->siriusTranslationsAuthToken
            ],
        ];

        $response = $this->httpClient->request(
            'GET',
            $this->siriusTranslationsUrl,
            $requestData
        );

        if ($response->getStatusCode() >= 300) {
            $this->logger->error(sprintf(
                'Download translations failed with status code: %s. Response: %s.',
                $response->getStatusCode(),
                $response->getContent(false)
            ));

            $io->error(sprintf(
                'Download translations failed with status code: %s.',
                $response->getStatusCode()
            ));

            return self::SUCCESS;
        }

        $translationsData = $response->toArray()['payload']; //dd($translationsData);

        // Iterate over locales
        foreach ($translationsData as $locale => $domainsData) {
            // Iterate over domains
            foreach ($domainsData as $domain => $translationsForLocaleAndDomain) {
                // Get translations key => value
                $fileContents = [];

                foreach ($translationsForLocaleAndDomain as $translationMessage) {
                    $fileContents[$translationMessage['key']] = $translationMessage['value'];
                }

                // Construct file path
                $path = $this->projectDir .
                    DIRECTORY_SEPARATOR .
                    'translations' .
                    DIRECTORY_SEPARATOR .
                    "$domain.$locale.yml"
                ;

                // Write array translations to file
                $yaml = Yaml::dump($fileContents);

                file_put_contents($path, $yaml);
            }
        }

        $io->success('Download translations completed successfully.');

        return 0;
    }
}
