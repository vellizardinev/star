<?php

namespace App\Command;

use App\Repository\HttpRequestRepository;
use App\Service\HttpRequestService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class RetryHttpRequestsCommand extends Command
{
    protected static $defaultName = 'http-requests:retry';

    private $httpRequestService;

    private $httpRequestRepository;

    public function __construct(HttpRequestService $httpRequestService, HttpRequestRepository $httpRequestRepository)
    {
        $this->httpRequestService = $httpRequestService;

        parent::__construct();
        $this->httpRequestRepository = $httpRequestRepository;
    }

    protected function configure()
    {
        $this->setDescription('Retries Http Requests that have failed the first time.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $requestsToRetry = $this->httpRequestRepository
            ->requestsToRetry();

        foreach ($requestsToRetry as $request) {
            $this->httpRequestService->send($request);
        }

        return self::SUCCESS;
    }
}
