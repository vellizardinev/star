<?php

namespace App\Command;

use App\Schedule\Scheduler;
use Carbon\Carbon;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ScheduleWorkCommand extends Command
{
    protected static $defaultName = 'schedule:work';

    private $scheduler;

    public function __construct(Scheduler $scheduler)
    {
        $this->scheduler = $scheduler;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Use for testing the Scheduler. Mimics the running of a cron job every minute that runs the Scheduler.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        pcntl_signal(SIGTERM, [$this, 'stopCommand']);
        pcntl_signal(SIGINT, [$this, 'stopCommand']);
        pcntl_async_signals(true);

        $io = new SymfonyStyle($input, $output);

        $io->text('Schedule worker started successfully.');

        while (true) {
            if (Carbon::now()->second === 0) {
                try {
                    $this->scheduler->run();
                    $io->success("The scheduler completed successfully.");
                } catch (Exception $e) {
                }
            }

            sleep(1);
        }
    }

    protected function stopCommand()
    {
        $this->scheduler->end();
        die();
    }
}
