<?php

namespace App\Command;

use App\Entity\FailedOnlineApplicationRequest;
use App\Entity\File;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Entity\OnlineApplication\UploadedDocuments;
use App\File\FileManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteOnlineApplicationsCommand extends Command
{
    protected static $defaultName = 'icredit:online-applications:delete';

    private $manager;

    private $fileManager;

    public function __construct(EntityManagerInterface $manager, FileManager $fileManager)
    {
        $this->manager = $manager;
        parent::__construct();
        $this->fileManager = $fileManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Delete online credit applications that are older than 3 days.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $applicationsToDelete = $this->manager
            ->getRepository(OnlineApplication::class)
            ->olderThan(3);

        foreach ($applicationsToDelete as $application) {
            if ($httpRequest = $this->manager
                ->getRepository(FailedOnlineApplicationRequest::class)
                ->findOneBy(['application' => $application])) {
                $this->manager->remove($httpRequest);
            }

            if ($uploadedDocuments = $application->getUploadedDocuments()) {
                $this->deleteFilesForOnlineApplication($uploadedDocuments, $this->fileManager);
            }

            $this->manager->remove($application);
            $this->manager->flush();
        }

        return self::SUCCESS;
    }

    /**
     * @param UploadedDocuments $uploadedDocuments
     * @param FileManager $fileManager
     * @throws Exception
     */
    private function deleteFilesForOnlineApplication(UploadedDocuments $uploadedDocuments, FileManager $fileManager)
    {
        $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportFirstPage(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportSecondPage(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportPhotoAt25(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportPastedPhotoAt25(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportPhotoAt45(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportPastedPhotoAt45(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportProofOfResidence(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getOldStylePassportSelfieWithPassport(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getNewStylePassportPhotoPage(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getNewStylePassportValidityPage(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getNewStylePassportRegistrationAppendix(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getNewStylePassportSelfieWithIdCard(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getOldStylePensionCertificateFirstSpread(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getOldStylePensionCertificateValidityRecord(), $fileManager);

        $this->deleteFileIfExists($uploadedDocuments->getNewStylePensionCertificatePhotoAndValiditySpread(), $fileManager);
    }

    /**
     * @param File|null $file
     * @param FileManager $fileManager
     * @throws Exception
     */
    private function deleteFileIfExists(?File $file, FileManager $fileManager)
    {
        if ($file) {
            $this->manager->remove($file);
        }
    }
}
