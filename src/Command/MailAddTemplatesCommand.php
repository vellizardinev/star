<?php

namespace App\Command;

use App\Entity\Mail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MailAddTemplatesCommand extends Command
{
    protected static $defaultName = 'mail:add-templates';

    protected $addedTypes = [];

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var string
     */
    private $projectDir;

    /**
     * @var array
     */
    private $locales;

    /**
     * MailLoadTemplatesCommand constructor.
     * @param EntityManagerInterface $manager
     * @param string $projectDir
     * @param string $locales
     */
    public function __construct(EntityManagerInterface $manager, string $projectDir, string $locales)
    {
        $this->manager = $manager;

        parent::__construct();
        $this->projectDir = $projectDir;
        $this->locales = explode('|', $locales);
    }

    protected function configure()
    {
        $this
            ->setDescription('Loads new email templates into the database (not to have been loaded before).');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        foreach ($this->locales as $locale) {
            $dir = $this->projectDir . "/templates/mail/$locale";
            $this->loadTemplatesFromDirectory($dir, $locale, $io);
        }

        return self::SUCCESS;
    }

    /**
     * @param $dir
     * @param $locale
     * @param SymfonyStyle $io
     */
    private function loadTemplatesFromDirectory($dir, $locale, SymfonyStyle $io)
    {
        $files = array_diff(scandir($dir), ['..', '.']);

        foreach ($files as $file) {
            $text = file_get_contents($dir . DIRECTORY_SEPARATOR . $file);

            $parts = explode('.', $file);
            $type = $parts[0];
            $parts = $parts[0];
            $parts = explode('_', $parts);
            $subject = ucfirst(implode(' ', $parts));

            preg_match_all("/{{ *\\w+(\\|\\w+\\(\\'\\w+\\'\\))? *}}/", $text, $placeholders);

            if (count($placeholders) > 0) {
                $placeholders = $placeholders[0];
            } else {
                $placeholders = [];
            }

            /** @var Mail $mail */
            $mail = $this->manager->getRepository(Mail::class)->findOneBy(['type' => $type, 'locale' => $locale]);

            if (!$mail)  {
                $mail = new Mail;
                $mail
                    ->setLocale($locale)
                    ->setType($type)
                    ->setSubject($subject)
                    ->setContent($text)
                    ->setPlaceholders($placeholders);

                $this->manager->persist($mail);
                $this->manager->flush();

                $this->addedTypes[] = $type;

                $io->success("Added mail with type '$type' for locale $locale.");
            } else if ($mail && false !== array_search($type, $this->addedTypes)) {
                $mail->setLocale($locale);
                $mail
                    ->setSubject($subject)
                    ->setContent($text);

                $this->manager->persist($mail);
                $this->manager->flush();

                $io->success("Added mail with name '$type' for locale $locale.");
            }
        }
    }
}
