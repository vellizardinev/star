<?php

namespace App\Command;

use App\Entity\OnlineApplication\OnlineApplication;
use App\Entity\OnlineApplication\UploadedDocuments;
use App\EventListener\FileEventListener;
use App\File\FileManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteImagesForCompletedOnlineApplicationsCommand extends Command
{
    protected static $defaultName = 'icredit:online-applications:delete-images-for-completed-applications';

    private $manager;

    private $fileManager;

    public function __construct(EntityManagerInterface $manager, FileManager $fileManager)
    {
        $this->manager = $manager;
        $this->fileManager = $fileManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Delete files for completed online credit applications that are older than 7 days.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->disableFileEventListener();

        $completedApplicationsWithFiles = $this->manager
            ->getRepository(OnlineApplication::class)
            ->completedWithFilesOlderThan(7, 20);

        foreach ($completedApplicationsWithFiles as $completedApplication) {
            if ($uploadedDocuments = $completedApplication->getUploadedDocuments()) {
                $this->deleteFilesForOnlineApplication($uploadedDocuments, $output);
            }
        }

        $this->manager->flush();

        $output->writeln(sprintf('Deleted images for %s completed online applications.', count($completedApplicationsWithFiles)));

        return self::SUCCESS;
    }

    /**
     * @param UploadedDocuments $uploadedDocuments
     * @param FileManager $output
     * @throws Exception
     */
    private function deleteFilesForOnlineApplication(UploadedDocuments $uploadedDocuments, OutputInterface $output)
    {
        $application = $uploadedDocuments->getOnlineApplication();

        if ($file = $uploadedDocuments->getIndividualTaxNumber()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setIndividualTaxNumber(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getSelfie()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setSelfie(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getOldStylePassportFirstPage()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePassportFirstPage(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getOldStylePassportFirstPage()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePassportFirstPage(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getOldStylePassportSecondPage()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePassportSecondPage(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getOldStylePassportPhotoAt25()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePassportPhotoAt25(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getOldStylePassportPastedPhotoAt25()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePassportPastedPhotoAt25(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getOldStylePassportPhotoAt45()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePassportPhotoAt45(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getOldStylePassportPastedPhotoAt45()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePassportPastedPhotoAt45(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getOldStylePassportProofOfResidence()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePassportProofOfResidence(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getOldStylePassportSelfieWithPassport()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePassportSelfieWithPassport(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getNewStylePassportPhotoPage()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setNewStylePassportPhotoPage(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getNewStylePassportValidityPage()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setNewStylePassportValidityPage(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getNewStylePassportRegistrationAppendix()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setNewStylePassportRegistrationAppendix(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getNewStylePassportSelfieWithIdCard()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setNewStylePassportSelfieWithIdCard(null);
            $this->manager->remove($file);
        }

        if ($file = $uploadedDocuments->getOldStylePensionCertificateFirstSpread()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePensionCertificateFirstSpread(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getOldStylePensionCertificateValidityRecord()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setOldStylePensionCertificateValidityRecord(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }

        if ($file = $uploadedDocuments->getNewStylePensionCertificatePhotoAndValiditySpread()) {
            $output->writeln(sprintf(
                'Deleting file with ID# %s for online application with ID# %s, created at: %s',
                $file->getId(),
                $application->getId(),
                $application->getCreatedAt()->format('d-m-Y H:i')
            ));
            $uploadedDocuments->setNewStylePensionCertificatePhotoAndValiditySpread(null);
            $this->manager->remove($file);
            $this->fileManager->delete($file);
        }
    }

    private function disableFileEventListener()
    {
        $listeners = $this->manager->getEventManager()->getListeners('postRemove');

        foreach ($listeners as $key => $listener) {
            if ($listener instanceof FileEventListener) {
                $listener->setEnabled(false);
                break;
            }
        }
    }
}
