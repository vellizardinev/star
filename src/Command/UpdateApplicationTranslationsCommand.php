<?php

namespace App\Command;

use App\Service\LongApplicationsTranslation;
use ReflectionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateApplicationTranslationsCommand extends Command
{
    protected static $defaultName = 'applications:update-translation';

    private $longApplicationsTranslation;

    public function __construct(LongApplicationsTranslation $longApplicationsTranslation)
    {
        $this->longApplicationsTranslation = $longApplicationsTranslation;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Update translation files for long credit applications.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->longApplicationsTranslation->update();

        return self::SUCCESS;
    }
}
