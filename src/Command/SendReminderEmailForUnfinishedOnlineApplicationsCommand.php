<?php

namespace App\Command;

use App\Entity\OnlineApplication\OnlineApplication;
use App\Mail\MailService;
use App\Mail\UnfinishedApplicationReminder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;

class SendReminderEmailForUnfinishedOnlineApplicationsCommand extends Command
{
    protected static $defaultName = 'icredit:online-applications:send-reminder-email';

    private $manager;

    private $mailService;

    private $router;

    public function __construct(EntityManagerInterface $manager, MailService $mailService, RouterInterface $router)
    {
        $this->manager = $manager;
        $this->mailService = $mailService;
        $this->router = $router;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Delete online credit applications that are older than 3 days.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $unfinishedApplications = $this->manager
            ->getRepository(OnlineApplication::class)
            ->unfinishedFromYesterday(30);

        foreach ($unfinishedApplications as $unfinishedApplication) {
            /** @var OnlineApplication $unfinishedApplication */
            $this->mailService
                ->to($unfinishedApplication->getPersonalInfo()->getEmail())
                ->send(new UnfinishedApplicationReminder(
                $unfinishedApplication,
                $this->router->generate('online_application.confirmation', ['hash' => $unfinishedApplication->getHash()], RouterInterface::ABSOLUTE_URL)
            ));

            $unfinishedApplication->setHasSentReminderEmail(true);
        }

        $this->manager->flush();

        $output->writeln(sprintf('Sent reminder emails for %s unfinished online applications.', count($unfinishedApplications)));

        return self::SUCCESS;
    }
}
