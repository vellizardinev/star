<?php

namespace App\Command;

use App\Enum\Enum;
use App\Service\LongApplicationsTranslation;
use ReflectionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;

class GenerateTranslationsForEnumCommand extends Command
{
    protected static $defaultName = 'enum:translations';

    private $locales = [];

    private $projectDir;

    public function __construct(string $locales, string $projectDir)
    {
        $this->locales = explode('|', $locales);
        $this->projectDir = $projectDir;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate translation entries for a given Enum class.')
            ->addArgument('enum', InputArgument::REQUIRED, 'The Enum class name (can be lowercase).')
            ->addArgument('domain', InputArgument::REQUIRED, 'The translation domain to update.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $enumClass = $input->getArgument('enum');
        $domain = $input->getArgument('domain');

        foreach ($this->locales as $locale) {
            $translations = $this->getTranslations($locale, $domain);

            $enumKeys = $this->translationKeys($enumClass);

            if (null === $enumKeys) {
                $io = new SymfonyStyle($input, $output);
                $io->getErrorStyle()->error('The provided enum class does not exist.');
                return;
            }

            foreach ($enumKeys as $key) {
                if (!array_key_exists($key, $translations)) {
                    $translations[$key] = null;
                }
            }

            $yaml = Yaml::dump($translations);

            file_put_contents($this->pathForLocale($locale, $domain), $yaml);

            return self::SUCCESS;
        }
    }

    private function getTranslations(string $locale, string $domain): array
    {
        $path = $this->pathForLocale($locale, $domain);

        $translations = Yaml::parseFile($path);

        if ($translations) {
            return $translations;
        } else {
            return [];
        }
    }

    private function pathForLocale(string $locale, string $domain): string
    {
        return $this->projectDir . DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR . $domain . '.' . $locale . '.yml';
    }

    /**
     * @param string $enumClass
     * @return array|null
     */
    private function translationKeys(string $enumClass): ?array
    {
        $fcqn = sprintf('App\\Enum\\%s',
            ucfirst($enumClass)
        );

        if (!class_exists($fcqn)) {
            return null;
        }

        /** @var Enum $enum */
        return $fcqn::toTranslationKeys();
    }
}
