<?php

namespace App\Command;

use App\Factory\CreditApplicationFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateTestCreditApplicationsCommand extends Command
{
    protected static $defaultName = 'credit-applications:create';
    /**
     * @var CreditApplicationFactory
     */
    private $factory;

    /**
     * CreateTestCreditApplicationsCommand constructor.
     * @param CreditApplicationFactory $factory
     */
    public function __construct(CreditApplicationFactory $factory)
    {
        $this->factory = $factory;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Generates fake credit applications for testing');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->factory->create([], 10);

        $io->success("Created 10 credit applications.");

        return self::SUCCESS;
    }
}
