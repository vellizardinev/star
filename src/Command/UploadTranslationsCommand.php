<?php

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UploadTranslationsCommand extends Command
{
    protected static $defaultName = 'sirius:translation:upload';

    private $projectDir;

    private $httpClient;

    private $logger;

    private $siriusTranslationsAuthToken;

    private $siriusTranslationsUrl;

    public function __construct(HttpClientInterface $httpClient, string $projectDir, LoggerInterface $logger, string $siriusTranslationsAuthToken, string $siriusTranslationsUrl)
    {
        $this->projectDir = $projectDir;
        $this->httpClient = $httpClient;
        $this->logger = $logger;
        $this->siriusTranslationsAuthToken = $siriusTranslationsAuthToken;
        $this->siriusTranslationsUrl = $siriusTranslationsUrl;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Uploads local translations inside yaml files to remote Sirius Translations app.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $path = $this->projectDir . DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR;
        $finder = new Finder();
        $finder->in($path);
        $finder->name('/.*\.[yml|yaml]/');

        $translationData = [];

        $existingTranslations = array_flip($this->existingTranslations());

        foreach ($finder as $file) {
            $locale = $this->extractLocale($file->getFilename());
            $domain = $this->extractDomain($file->getFilename());

            $translations = Yaml::parseFile($file->getRealPath());
            foreach ($translations as $key => $value) {
                $concatenatedTranslation = $locale . $domain . $key;

                if (array_key_exists($concatenatedTranslation, $existingTranslations) === false) {
                    $translationData[] = [
                        'locale' => $locale,
                        'domain' => $domain,
                        'translationKey' => $key,
                        'translationValue' => $value
                    ];
                }
            }
        }

        $requestData = [
            'verify_peer' => false,
            'json' => $translationData,
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Auth-Token' => $this->siriusTranslationsAuthToken
            ],
        ];

        $response = $this->httpClient->request(
            'POST',
            $this->siriusTranslationsUrl,
            $requestData
        );

        if ($response->getStatusCode() >= 300) {
            $this->logger->error(sprintf(
                'Upload translations failed with status code: %s. Response: %s.',
                $response->getStatusCode(),
                $response->getContent(false)
            ));

            $io->error(sprintf(
                'Upload translations failed with status code: %s.',
                $response->getStatusCode()
            ));

            return 1;
        }

        $io->success(sprintf(
            'Upload translations completed successfully. Translations sent to remote: %s.',
            count($translationData)
        ));

        return self::SUCCESS;
    }

    private function extractDomain(string $fileName): string
    {
        $parts = explode('.', $fileName);

        return $parts[0];
    }

    private function extractLocale(string $fileName): string
    {
        $parts = explode('.', $fileName);

        return $parts[1];
    }

    /**
     * @return array
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws DecodingExceptionInterface
     */
    private function existingTranslations(): array
    {
        $requestData = [
            'verify_peer' => false,
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Auth-Token' => $this->siriusTranslationsAuthToken
            ],
        ];

        $response = $this->httpClient->request(
            'GET',
            $this->siriusTranslationsUrl . '/existing-translations',
            $requestData
        );

        return $response->toArray()['payload'];
    }
}
