<?php

namespace App\Command;

use App\DataMigration\Migrator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateDataCommand extends Command
{
    protected static $defaultName = 'data:update';

    private $migrator;

    public function __construct(Migrator $migrator)
    {
        $this->migrator = $migrator;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Updates data running data migrations.')
            ->addOption('add-all', null, InputOption::VALUE_OPTIONAL, 'Mark all data migrations as executed.', false)
        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('add-all') !== false) {
            $count = $this->migrator->addAll();
            $output->writeln(sprintf('Added %s data migrations.', $count));

            return self::SUCCESS;
        }

        $this->migrator->addData(new SymfonyStyle($input, $output));

        return self::SUCCESS;
    }
}
