<?php


namespace App\SiteMap;


class SiteMapItem
{
    /** @var string */
    public $path;

    /** @var string */
    public $locale = 'ua';

    /** @var null|string  */
    public $name = null;
}