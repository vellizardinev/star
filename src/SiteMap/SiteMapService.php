<?php


namespace App\SiteMap;


use App\Annotation\SiteMap;
use App\Entity\Page;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SiteMapService
{
    const CACHE_KEY = 'site_map_cache_key';

    private $router;

    private $translator;

    private $manager;
    /**
     * @var AdapterInterface
     */
    private $cache;

    public function __construct(RouterInterface $router, TranslatorInterface $translator, EntityManagerInterface $manager, AdapterInterface $cache)
    {
        $this->router = $router;
        $this->translator = $translator;
        $this->manager = $manager;
        $this->cache = $cache;
    }

    /**
     * @return SiteMapCollection
     * @throws AnnotationException
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    public function routesForSiteMap(): SiteMapCollection
    {
        $controllerRoutes = $this->getControllerRoutes();
        $customPageRoutes = $this->getCustomPageRoutes();

        return $controllerRoutes->mergeWith($customPageRoutes);
    }

    /**
     * @return SiteMapCollection
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    private function getControllerRoutes(): SiteMapCollection
    {
        $cacheItem = $this->cache->getItem(self::CACHE_KEY);

        if ($cacheItem->isHit()) {
            return $cacheItem->get();
        }

        $siteMapCollection = new SiteMapCollection;

        $annotationReader = new AnnotationReader();

        $actionCache = [];

        foreach ($this->router->getRouteCollection()->all() as $route) {
            $defaults = $route->getDefaults();

            if (isset($defaults['_controller'])) {
                list($controller, $action) = explode('::', $defaults['_controller']);

                if (!class_exists($controller)) {
                    continue;
                }

                $reflectionMethod = new ReflectionMethod($controller, $action);

                $siteMapAnnotation = $annotationReader->getMethodAnnotation($reflectionMethod, SiteMap::class);

                if ($siteMapAnnotation) {
                    $item = new SiteMapItem();
                    $item->locale = $this->valueForKey('_locale', $defaults);
                    $item->path = $route->getPath();
                    $item->name = $this->translator->trans($this->valueForKey('_canonical_route', $defaults), [], 'site_map');

                    $siteMapCollection->addItem($item);
                }
            }
        }

        $cacheItem->set($siteMapCollection);
        $this->cache->save($cacheItem);

        return $siteMapCollection;
    }

    private function getCustomPageRoutes(): SiteMapCollection
    {
        $collection = new SiteMapCollection();

        $pages = $this->manager->getRepository(Page::class)->forSiteMap();
        $this->createItemsForPages($pages, $collection);

        return $collection;
    }

    private function valueForKey(string $key, array $data): ?string
    {
        if (array_key_exists($key, $data)) {
            return $data[$key];
        }

        return null;
    }

    /**
     * @param Page[]
     * @param SiteMapCollection $collection
     * @return void
     */
    private function createItemsForPages(array $pages, SiteMapCollection $collection)
    {
        $repository = $this->manager->getRepository('Gedmo\\Translatable\\Entity\\Translation');

        foreach ($pages as $page) {
            $translations = $repository->findTranslations($page);

            foreach ($translations as $locale => $data) {
                $item = new SiteMapItem();
                $item->locale = $locale;
                $item->path = $data['path'];
                $item->name = $data['name'];

                $collection->addItem($item);
            }
        }
    }
}