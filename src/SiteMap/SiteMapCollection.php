<?php


namespace App\SiteMap;


class SiteMapCollection
{
    private $items = [];

    public function addItem(SiteMapItem $item)
    {
        $this->items[] = $item;
    }

    /**
     * @return SiteMapItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param string|null $locale
     * @return SiteMapItem[]
     */
    public function itemsForLocale(?string $locale = 'ua'): array
    {
        $results = [];

        foreach ($this->items as $item) {
            /** @var $item SiteMapItem */
            if ($item->locale === $locale) {
                $results[] = $item;
            }
        }

        usort($results, [$this, 'sortItems']);

        return $results;
    }

    public function mergeWith(SiteMapCollection $collection): self
    {
        $this->items = array_merge($this->items, $collection->getItems());

        return $this;
    }

    /**
     * @param SiteMapItem $a
     * @param SiteMapItem $b
     * @return int
     */
    private function sortItems(SiteMapItem $a, SiteMapItem $b): int
    {
        if ($a->name === $b->name) {
            return  0;
        }

        if ($a->name > $b->name) {
            return 1;
        }

        return -1;
    }
}