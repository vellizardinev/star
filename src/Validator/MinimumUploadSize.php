<?php


namespace App\Validator;


use function get_class;

use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class MinimumUploadSize extends Constraint

{
    public $message = 'minimum_upload_size_message';

    public $sizeInBytes;

    public function __construct(?int $sizeInBytes = 1000)
    {
        $this->sizeInBytes = $sizeInBytes;

        parent::__construct();
    }

    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }
}
