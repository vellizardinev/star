<?php

namespace App\Validator;


use LogicException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MinimumUploadSizeValidator extends ConstraintValidator
{
    /** @var Constraint $constraint */
    private $constraint;

    public function validate($value, Constraint $constraint)
    {
        $this->constraint = $constraint;
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (!$value instanceof UploadedFile) {
            throw new LogicException('MinimumUploadSize constaint is applicable only to objects of type UploadedFile.');
        }

        if ($this->isBelowLimit($this->constraint->sizeInBytes, $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%sizeInBytes%', $constraint->sizeInBytes)
                ->addViolation();
        }
    }

    private function isBelowLimit(int $limitInBytes, UploadedFile $file): bool
    {
        if ($file->getSize() <= $limitInBytes) {
            return true;
        } else {
            return false;
        }
    }
}
