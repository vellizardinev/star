<?php

namespace App\Validator\Constraints;

use App\Entity\Setting;
use App\Enum\Settings;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CheckedCaptchaValidator extends ConstraintValidator
{
	/**
	 * @var EntityManagerInterface
	 */
	private $manager;

	/**
	 * CaptchaType constructor.
	 * @param EntityManagerInterface $entityManager
	 */
	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->manager = $entityManager;
	}

    public function validate($recaptcha, Constraint $constraint)
    {
    	$captchaSecret = $this->manager->getRepository(Setting::class)->get(Settings::GOOGLE_RECAPTCHA_SECRET)->getValue();
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            "secret" => $captchaSecret, "response" => $recaptcha));
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response);

        if (!$data->success || $data->score < 0.5) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
