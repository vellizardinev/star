<?php

namespace App\Validator\Constraints;

use App\Enum\CreateUserAccountErrors;
use function get_class;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ApiPassword extends Constraint
{
    public $message = CreateUserAccountErrors::ERROR_INVALID_PASSWORD;

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}