<?php

namespace App\Validator\Constraints;


use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Twig\Source;

class TotalUploadSizeValidator extends ConstraintValidator
{
    /** @var Constraint $constraint */
    private $constraint;


    public function __construct()
    {

    }

    public function validate($value, Constraint $constraint)
    {
        $this->constraint = $constraint;
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        $expectedType = sprintf('array of %s', UploadedFile::class);

        if (!is_array($value)) {
            throw new UnexpectedTypeException($value, $expectedType);
        }

        foreach ($value as $item) {
            if (!$item instanceof UploadedFile) {
                throw new UnexpectedTypeException($value, $expectedType);
            }
        }

        if ($this->isAboveLimit($this->constraint->sizeMB, $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%sizeMB%', $constraint->sizeMB)
                ->addViolation();
        }
    }

    private function isAboveLimit(int $limitInMB, array $files): bool
    {
        $totalToUpload = 0;
        $limitInBytes = $limitInMB * 1000000;

        foreach ($files as $file) {
            /** @var $file UploadedFile */
            $totalToUpload += $file->getSize();
        }

        if ($limitInBytes < $totalToUpload) {
            return true;
        } else {
            return false;
        }
    }
}