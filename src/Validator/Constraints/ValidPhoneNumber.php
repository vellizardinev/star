<?php

namespace App\Validator\Constraints;

use function get_class;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidPhoneNumber extends Constraint
{
    public $message = 'invalid_phone_number';

    public $regionCode;

    /**
     * ValidPhoneNumber constructor.
     * @param array $parameters
     */
    public function __construct($parameters)
    {
        $this->regionCode = $parameters['regionCode'];

        parent::__construct();

    }

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}