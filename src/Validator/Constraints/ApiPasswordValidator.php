<?php

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ApiPasswordValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        // Check if password is between 8 and 13 symbols
        if (strlen($value) < 8 || strlen($value) > 13) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();

            return;
        }

        // Check password contains only the required symbols
        if (preg_match("/^[a-zA-Z0-9\!\(\^£\$\%&\*\(\)\}\{\@\#\~\?\>\<\>\,\|\=\_\+\¬\-\]\.\)]+$/", $value) === 0) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();

            return;
        }

        // Check password contains at least one uppercase symbol
        if (preg_match("/[A-Z]+/", $value) === 0) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();

            return;
        }

        // Check password contains at least one lowercase symbol
        if (preg_match("/[a-z]+/", $value) === 0) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();

            return;
        }

        // Check password contains at least one number
        if (preg_match("/[0-9]+/", $value) === 0) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();

            return;
        }

        // Check password contains at least one special symbol
        if (preg_match("/[\!\(\^£$\%&\*\(\)\}\{\@\#\~\?\>\<\>\,\|\=\_\+\¬\-\]\.\)]+/", $value) === 0) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();

            return;
        }
    }
}