<?php

namespace App\Validator\Constraints;

use App\Entity\RedirectRule;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidPagePathValidator extends ConstraintValidator
{
	private $manager;

	/** @var Constraint $constraint */
	private $constraint;

	/**
	 * ValidPagePathValidator constructor.
	 * @param EntityManagerInterface $entityManager
	 */
	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->manager = $entityManager;
	}

    public function validate($value, Constraint $constraint)
    {
        $this->constraint = $constraint;

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

	    $rule = $this->manager->getRepository(RedirectRule::class)->findOneBy(['oldUrl' => $value]);
	    if ($rule) {
		    $this->buildCustomViolation($value, 'path_is_used_as_redirect_rule');

		    return;
	    }
    }

    private function buildCustomViolation($value, $message = null)
    {
        $message = $message ?: $this->constraint->message;
        $this->context->buildViolation($message)
            ->setParameter('%path%', $value)
            ->addViolation();
    }
}