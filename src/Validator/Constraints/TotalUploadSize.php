<?php

namespace App\Validator\Constraints;

use function get_class;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TotalUploadSize extends Constraint
{
    public $message = 'max_total_upload_size_message';

    public $sizeMB;

    public function __construct(?int $sizeMB = 10)
    {
        $this->sizeMB = $sizeMB;
        parent::__construct();
    }

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}