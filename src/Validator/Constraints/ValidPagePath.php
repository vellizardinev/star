<?php

namespace App\Validator\Constraints;

use function get_class;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidPagePath extends Constraint
{
    public $message = 'invalid_page_path';

	public function validatedBy()
	{
		return get_class($this).'Validator';
	}
}