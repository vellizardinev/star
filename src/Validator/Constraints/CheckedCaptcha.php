<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class CheckedCaptcha extends Constraint
{
    public $message = 'the_captcha_is_not_valid_message';
}
