<?php

namespace App\Validator\Constraints;

use function get_class;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidTwig extends Constraint
{
    public $message = 'invalid_twig_template';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}