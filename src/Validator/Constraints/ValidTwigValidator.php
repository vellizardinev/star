<?php

namespace App\Validator\Constraints;


use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Twig\Environment;
use Twig\Source;

class ValidTwigValidator extends ConstraintValidator
{
    /** @var Constraint $constraint */
    private $constraint;
    /**
     * @var Environment
     */
    private $twig;

    /**
     * ValidTwigValidator constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function validate($value, Constraint $constraint)
    {
        $this->constraint = $constraint;
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        try {
            $this->twig->tokenize(new Source($value, 'test'));
        } catch (Exception $e) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}