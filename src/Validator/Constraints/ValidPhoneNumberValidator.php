<?php

namespace App\Validator\Constraints;


use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberType;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use libphonenumber\PhoneNumberUtil;

class ValidPhoneNumberValidator extends ConstraintValidator
{

    /** @var Constraint $constraint */
    private $constraint;

    public function validate($value, Constraint $constraint)
    {
        $this->constraint = $constraint;
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $phoneNumberUtil = PhoneNumberUtil::getInstance();

        $region = $constraint->regionCode;

        try {
            $phoneNumberObject = $phoneNumberUtil->parse($value, $region);
        } catch (NumberParseException $e) {
            $this->buildCustomViolation($value, $e->getMessage());

            return;
        }

        if (!$phoneNumberUtil->isValidNumberForRegion($phoneNumberObject, $region) || $phoneNumberUtil->getNumberType(
                $phoneNumberObject
            ) != PhoneNumberType::MOBILE) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%phone%', $value)
                ->setParameter('%region%', $region)
                ->atPath('phone')
                ->addViolation();
        }
    }

    private function buildCustomViolation($value, $message = null, $region = null)
    {
        $message = $message ?: $this->constraint->message;
        $this->context->buildViolation($message)
            ->setParameter('%phone%', $value)
            ->setParameter('%region%', $region)
            ->atPath('phone')
            ->addViolation();
    }
}