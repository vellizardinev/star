<?php


namespace App\Response;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

class ResponseService
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * ResponseService constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function successResponse($data): JsonResponse
    {
        $json = $this->serializer->serialize(
            [
                'success' => true,
                'payload' => $data,
            ],
            'json',
            array_merge(
                [
                    'json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS,
                ],
                []
            )
        );

        return new JsonResponse($json, 200, [], true);
    }
}