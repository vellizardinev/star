<?php


namespace App\Security;


use App\Entity\Customer;
use App\Service\MobileApi\Customers;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;

class CustomerLogoutHandler implements LogoutHandlerInterface
{
    private $customersService;

    public function __construct(Customers $customersService)
    {
        $this->customersService = $customersService;
    }

    /**
     * @inheritDoc
     */
    public function logout(Request $request, Response $response, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof Customer) {
            return;
        }

        $this->customersService->logout($user);
    }
}