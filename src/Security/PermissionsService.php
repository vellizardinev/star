<?php

namespace App\Security;


use App\Entity\Permission\Permission;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class PermissionsService
{
    protected $manager;

    protected $tokenStorage;

    private $cache;

    /**
     * PermissionsService constructor.
     * @param EntityManagerInterface $manager
     * @param TokenStorageInterface $tokenStorage
     * @param AdapterInterface $cache
     */
    public function __construct(EntityManagerInterface $manager, TokenStorageInterface $tokenStorage, AdapterInterface $cache)
    {
        $this->manager = $manager;
        $this->tokenStorage = $tokenStorage;
        $this->cache = $cache;
    }

    /**
     * @param $permission string
     * @return bool
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function check($permission)
    {
        $parts = explode('.', $permission);

        if (count($parts) !== 2) {
            throw new Exception("Invalid permission string supplied: '$permission'");
        }

        list($domain, $action) = $parts;

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        // First, check the cache if the user has the required permission
        $cacheKey = "permissions_{$user->getId()}_$permission";
        $item = $this->cache->getItem($cacheKey);

        if ($item->isHit()) {
            return $item->get();
        }

        // Then, check the database if the user has a group that has the permission
        $isAllowed = $this->manager->getRepository(Permission::class)
            ->checkPermissionForUser($domain, $action, $user);

        // And save the value in the cache
        $item->set($isAllowed);
        $item->expiresAfter(1000 * 60 * 10);
        $this->cache->save($item);

        return $isAllowed;
    }
}