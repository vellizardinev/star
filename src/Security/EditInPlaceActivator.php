<?php


namespace App\Security;


use App\Entity\User;
use App\Enum\Roles;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Translation\Bundle\EditInPlace\ActivatorInterface;
use function is_object;

class EditInPlaceActivator implements ActivatorInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function checkRequest(Request $request = null): bool
    {
        if ($token = $this->tokenStorage->getToken()) {
            if (is_object($user = $token->getUser())) {
                /** @var $user User */
                return $user->hasRole(Roles::EDITOR_MARKETING_UA);
            }
        }

        return false;
    }
}