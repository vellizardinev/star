<?php


namespace App\LandingPage;


use App\Entity\LandingPage;
use App\Entity\Module;
use App\Entity\ModuleItem;
use App\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;

class LandingPageManager
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * LandingPageManager constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function createLandingPage(Page $page): LandingPage
    {
        $landingPage = (new LandingPage)
            ->setName($page->getName());

        /** @var $landingPage LandingPage */
        $this->createHeader($landingPage);
        $this->createFooter($landingPage);

        $this->manager->persist($landingPage);
        $this->manager->flush();

        return $landingPage;
    }

    public function createHeader(LandingPage $landingPage)
    {
        $module = $this->manager->getRepository(Module::class)->findOneBy(['name' => 'Header landing page']);

        $moduleItem = (new ModuleItem)
            ->setModule($module)
            ->setPage($landingPage)
            ->setData($module->getData());

        $landingPage->setHeader($moduleItem);
    }

    public function createFooter(LandingPage $landingPage)
    {
        $module = $this->manager->getRepository(Module::class)->findOneBy(['name' => 'Footer landing page']);

        $moduleItem = (new ModuleItem)
            ->setModule($module)
            ->setPage($landingPage)
            ->setData($module->getData());

        $landingPage->setFooter($moduleItem);
    }
}