<?php


namespace App\Breadcrumbs;


class CustomerProfileConfirmCredit extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new CustomerProfile());
    }

    function text(): string
    {
        return $this->translator->trans('customer_profile.confirm_credit', [], 'breadcrumbs');
    }

    function url(): string
    {
        return '';
    }
}