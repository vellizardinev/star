<?php


namespace App\Breadcrumbs;


class CustomerProfileMyCredits extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new CustomerProfile());
    }

    function text(): string
    {
        return $this->translator->trans('customer_profile.my_credits', [], 'breadcrumbs');
    }

    function url(): string
    {
        return $this->router->generate('customer_profile.my_credits');
    }
}