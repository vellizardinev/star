<?php


namespace App\Breadcrumbs;


class VerificationSuccess extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new Home());
    }

    function text(): string
    {
        return $this->translator->trans('verification_success', [], 'breadcrumbs');
    }

    function url(): string
    {
        return $this->router->generate('verification.success');
    }
}
