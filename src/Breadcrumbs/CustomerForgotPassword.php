<?php


namespace App\Breadcrumbs;


class CustomerForgotPassword extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new Home());
    }

    function text(): string
    {
        return $this->translator->trans('customer_login.forgot_password', [], 'breadcrumbs');
    }

    function url(): string
    {
        return $this->router->generate('customer_login.forgot_password');
    }
}