<?php


namespace App\Breadcrumbs;


class CustomerProfileAcceptCreditOffer extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new CustomerProfileMyCredits());
    }

    function text(): string
    {
        return $this->translator->trans('customer_profile.my_credits.accept_credit_offer', [], 'breadcrumbs');
    }

    function url(): string
    {
        return '';
    }
}