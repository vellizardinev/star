<?php


namespace App\Breadcrumbs;


class CustomerLoginStepTwo extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new Home());
    }

    function text(): string
    {
        return $this->translator->trans('customer_login.step_two', [], 'breadcrumbs');
    }

    function url(): string
    {
        return $this->router->generate('customer_login.step_two');
    }
}