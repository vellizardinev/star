<?php


namespace App\Breadcrumbs;


use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class BreadcrumbsService
{
    private $router;

    private $translator;

    private $whiteOctoberBreadcrumbs;

    public function __construct(TranslatorInterface $translator, RouterInterface $router, Breadcrumbs $whiteOctoberBreadcrumbs)
    {
        $this->router = $router;
        $this->translator = $translator;
        $this->whiteOctoberBreadcrumbs = $whiteOctoberBreadcrumbs;
    }

    public function create(Breadcrumb $breadcrumb)
    {
        $trail = [];

        $trail[] = $breadcrumb;

        $currentBreadcrumb = $breadcrumb;

        while ($parent = $currentBreadcrumb->getParent()) {
            $trail[] = $parent;
            $currentBreadcrumb = $parent;
        }

        $trail = array_reverse($trail);

        foreach ($trail as $breadcrumb) {
            $breadcrumb->setTranslator($this->translator);
            $breadcrumb->setRouter($this->router);

            $this->whiteOctoberBreadcrumbs->addItem($breadcrumb->text(), $breadcrumb->url());
        }
    }
}