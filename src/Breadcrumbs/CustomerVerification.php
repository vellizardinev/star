<?php


namespace App\Breadcrumbs;


class CustomerVerification extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new Home());
    }

    function text(): string
    {
        return $this->translator->trans('customer_registration.verification', [], 'breadcrumbs');
    }

    function url(): string
    {
        return $this->router->generate('customer_registration.verification');
    }
}