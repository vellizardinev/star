<?php


namespace App\Breadcrumbs;


class CustomerChangePassword extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new Home());
    }

    function text(): string
    {
        return $this->translator->trans('customer_login.change_password', [], 'breadcrumbs');
    }

    function url(): string
    {
        return '';
    }
}
