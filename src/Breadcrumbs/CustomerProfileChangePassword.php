<?php


namespace App\Breadcrumbs;


class CustomerProfileChangePassword extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new CustomerProfile());
    }

    function text(): string
    {
        return $this->translator->trans('customer_profile.change_password', [], 'breadcrumbs');
    }

    function url(): string
    {
        return $this->router->generate('customer_profile.change_password');
    }
}