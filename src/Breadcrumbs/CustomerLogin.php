<?php


namespace App\Breadcrumbs;


class CustomerLogin extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new Home());
    }

    function text(): string
    {
        return $this->translator->trans('customer_login.login', [], 'breadcrumbs');
    }

    function url(): string
    {
        return $this->router->generate('customer_login.login');
    }
}