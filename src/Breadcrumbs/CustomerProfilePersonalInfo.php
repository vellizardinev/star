<?php


namespace App\Breadcrumbs;


class CustomerProfilePersonalInfo extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new CustomerProfile());
    }

    function text(): string
    {
        return $this->translator->trans('customer_profile.personal_info', [], 'breadcrumbs');
    }

    function url(): string
    {
        return $this->router->generate('customer_profile.personal_info');
    }
}