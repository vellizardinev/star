<?php


namespace App\Breadcrumbs;


class CustomerProfileSignRegularDocuments extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new CustomerProfileMyCredits());
    }

    function text(): string
    {
        return $this->translator->trans('customer_profile.my_credits.sign_credit_documents', [], 'breadcrumbs');
    }

    function url(): string
    {
        return '';
    }
}