<?php


namespace App\Breadcrumbs;


class CustomerProfile extends Breadcrumb
{
    public function __construct()
    {
        parent::__construct(new Home());
    }

    function text(): string
    {
        return $this->translator->trans('customer_profile', [], 'breadcrumbs');
    }

    function url(): string
    {
        return $this->router->generate('customer_profile.my_credits');
    }
}