<?php


namespace App\Breadcrumbs;


use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class Breadcrumb
{
    /** @var TranslatorInterface */
    protected $translator;

    /** @var RouterInterface */
    protected $router;

    /** @var null|Breadcrumb */
    protected $parent;

    protected $object;

    public function __construct($parent = null, $object = null)
    {
        $this->parent = $parent;
        $this->object = $object;
    }

    abstract function text(): string;

    abstract function url(): string;

    function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    function setRouter(RouterInterface $router)
    {
        $this->router = $router;
    }

    function getParent(): ?Breadcrumb
    {
        return $this->parent;
    }
}