<?php


namespace App\Breadcrumbs;


class Home extends Breadcrumb
{
    function text(): string
    {
        return $this->translator->trans('home', [], 'breadcrumbs');
    }

    function url(): string
    {
        return $this->router->generate('static_pages.home_page');
    }
}