<?php


namespace App\Message;


class ResetFeaturedPromotions
{
    /**
     * @var int
     */
    private $promotionId;

    /**
     * ResetFeaturedNews constructor.
     * @param int $promotionId
     */
    public function __construct(int $promotionId)
    {
        $this->promotionId = $promotionId;
    }

    /**
     * @return int
     */
    public function getPromotionId(): int
    {
        return $this->promotionId;
    }
}