<?php


namespace App\Message;


class SendOnlineApplication
{
    private $applicationId;

    public function __construct(int $applicationId)
    {
        $this->applicationId = $applicationId;
    }

    public function getApplicationId(): int
    {
        return $this->applicationId;
    }
}