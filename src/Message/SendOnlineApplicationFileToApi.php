<?php


namespace App\Message;


class SendOnlineApplicationFileToApi
{
    private $documentId;

    private $applicationId;

    private $type;

    public function __construct(int $documentId, int $applicationId, int $type)
    {
        $this->documentId = $documentId;
        $this->applicationId = $applicationId;
        $this->type = $type;
    }

    public function getDocumentId(): int
    {
        return $this->documentId;
    }

    public function getApplicationId(): int
    {
        return $this->applicationId;
    }

    public function getType(): int
    {
        return $this->type;
    }
}