<?php


namespace App\Message;


class DeleteOffice
{
    private $officeId;

    public function __construct(int $officeId)
    {
        $this->officeId = $officeId;
    }

    public function getOfficeId(): int
    {
        return $this->officeId;
    }
}