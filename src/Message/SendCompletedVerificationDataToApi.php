<?php


namespace App\Message;


class SendCompletedVerificationDataToApi
{
    private $kycSessionId;

    private $externalToken;
    /**
     * @var string
     */
    private $phone;

    public function __construct(string $kycSessionId, string $externalToken, string $phone)
    {
        $this->kycSessionId = $kycSessionId;
        $this->externalToken = $externalToken;
        $this->phone = $phone;
    }

    public function getKycSessionId(): string
    {
        return $this->kycSessionId;
    }

    public function getExternalToken(): string
    {
        return $this->externalToken;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
