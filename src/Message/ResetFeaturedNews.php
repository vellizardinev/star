<?php


namespace App\Message;


class ResetFeaturedNews
{
    /**
     * @var int
     */
    private $newsId;

    /**
     * ResetFeaturedNews constructor.
     * @param int $newsId
     */
    public function __construct(int $newsId)
    {
        $this->newsId = $newsId;
    }

    /**
     * @return int
     */
    public function getNewsId(): int
    {
        return $this->newsId;
    }
}