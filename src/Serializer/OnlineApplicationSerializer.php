<?php

namespace App\Serializer;

use App\Entity\OnlineApplication\Confirmation;
use App\Entity\OnlineApplication\OnlineApplication;
use App\Entity\OnlineApplication\PersonalInfo;
use Carbon\Carbon;
use DateTimeInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class OnlineApplicationSerializer implements NormalizerInterface
{
    private $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($onlineApplication, $format = null, array $context = [])
    {
        /** @var OnlineApplication $onlineApplication */

        /** @var PersonalInfo $personalInfo */
        $personalInfo = $onlineApplication->getPersonalInfo();

        /** @var Confirmation $confirmation */
        $confirmation = $onlineApplication->getConfirmation();

        return [
            'id' => $confirmation->getId(),
            'onlineApplicationId' => $onlineApplication->getId(),
            'phone' => $this->normalizePhone($personalInfo->getMobileTelephone()),
            'level' => 'Online',
            'name' => $personalInfo->fullName(),
            'telephone' => $this->normalizePhone($personalInfo->getMobileTelephone()),
            'personalIdentificationNumber' => $personalInfo->getPersonalIdentificationNumber(),
            'amount' => $onlineApplication->getCreditDetails()->getAmount(),
            'dateOfApplication' => $this->getDate($onlineApplication->getCreatedAt()),
            'dateOfConfirmation' => $this->getDate($confirmation->getCreatedAt()),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof OnlineApplication;
    }

    private function getDate(?DateTimeInterface $date)
    {
        if ($date) {
            return Carbon::instance($date)->toIso8601String();
        } else {
            return null;
        }
    }

    private function normalizePhone(string $telephone)
    {
        if (strlen($telephone) === 13 && substr($telephone, 0, 4 ) === "+380") {
            return substr($telephone, 1);
        }

        return $telephone;
    }
}
