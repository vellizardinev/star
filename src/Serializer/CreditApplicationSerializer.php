<?php

namespace App\Serializer;

use App\Entity\CreditShortApplication;
use Carbon\Carbon;
use DateTimeInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Contracts\Translation\TranslatorInterface;

class CreditApplicationSerializer implements NormalizerInterface
{
    private $normalizer;

    private $translator;

    public function __construct(ObjectNormalizer $normalizer, TranslatorInterface $translator)
    {
        $this->normalizer = $normalizer;
        $this->translator = $translator;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        /** @var CreditShortApplication $object */
        return [
            'phone' => $object->getTelephone(),
            'id' => $object->getId(),
            'level' => 'Seles_Partner',
            'name' => $object->getName(),
            'telephone' => $object->getTelephone(),
            'source' => $this->getSource($object),
            'dateOfBirth' => $this->getDate($object->getBirthdayDate()),
            'personalIdentificationNumber' => $object->getPersonalIdentificationNumber(),
            'city' => $object->getCity(),
            'amount' => $object->getAmount(),
            'email' => $object->getEmail(),
            'dateOfApplication' => $this->getDate($object->getCreatedAt()),
            'preferredCallDate' => $this->getDate($object->getPreferredCallDate()),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof CreditShortApplication;
    }

    private function getSource(CreditShortApplication $object)
    {
        if ($object->getAffiliate() !== null) {
            return $this->translator->trans('affiliate', ['%name%' => $object->getAffiliate()->getName()], 'call_flow');
        }

        if ($object->getLanding() !== null) {
            return $this->translator->trans('landing_page', ['%name%' => $object->getLanding()->getName()], 'call_flow');
        }

        return $this->translator->trans('website', [], 'call_flow');
    }

    private function getDate(?DateTimeInterface $date)
    {
        if ($date) {
            return Carbon::instance($date)->toIso8601String();
        } else {
            return null;
        }
    }
}
