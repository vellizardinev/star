<?php

namespace App\Serializer;

use App\Entity\Poll;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PollSerializer implements NormalizerInterface
{
    private $normalizer;

    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($poll, $format = null, array $context = [])
    {
        if (array_key_exists('intention', $context)) {
            $intention = $context['intention'];

            if ($intention === 'poll_participation') {
                return $this->pollParticipation($poll);
            }
        }

        return $this->normalizer->normalize($poll, $format, $context);
    }

    public function pollParticipation(Poll $poll): array
    {
        return [
            'id' => $poll->getId(),
            'title' => $poll->getTitle(),
            'sdfg' => $poll->getTitle(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return false;
    }
}
