<?php

namespace App\Serializer;

use App\Entity\CreditData;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreditDataSerializer implements NormalizerInterface
{
    private $normalizer;

    /**
     * ImageSerializer constructor.
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($poll, $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($poll, $format, $context);

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof CreditData;
    }
}
