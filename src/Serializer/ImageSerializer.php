<?php

namespace App\Serializer;

use App\Entity\Image;
use App\File\FileManager;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ImageSerializer implements NormalizerInterface
{
    private $normalizer;
    /**
     * @var FileManager
     */
    private $fileManager;
    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * ImageSerializer constructor.
     * @param FileManager $fileManager
     * @param ObjectNormalizer $normalizer
     * @param CacheManager $cacheManager
     */
    public function __construct(FileManager $fileManager, ObjectNormalizer $normalizer, CacheManager $cacheManager)
    {
        $this->normalizer = $normalizer;
        $this->fileManager = $fileManager;
        $this->cacheManager = $cacheManager;
    }

    public function normalize($image, $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($image, $format, $context);

        $data['publicPath'] = $this->fileManager->publicImagePath($image);

        $data['cachedPath'] = $this->cacheManager->getBrowserPath($image->getFilePath(), 'index_thumbnail');

        return $data;
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Image;
    }
}
