<?php

namespace App\Exception;

use App\Enum\Enum;
use Exception;

class InvalidEnumMemberException extends Exception
{
    /**
     * Create an InvalidEnumMemberException.
     *
     * @param mixed $invalidValue
     * @param Enum $enum
     */
    public function __construct($invalidValue, Enum $enum)
    {
        $enumValues = implode(', ', $enum::getValues());

        parent::__construct("Value {$invalidValue} doesn't exist in [{$enumValues}]");
    }
}