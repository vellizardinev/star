<?php

namespace App\Tests\Admin;

use App\Enum\Roles;
use App\Factory\FaqFactory;
use App\Factory\UserFactory;
use App\Tests\BaseWebTestCaseTest;

class FaqsTest extends BaseWebTestCaseTest
{
    public function testFaqsList(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/faqs');

        $this->assertResponseIsSuccessful();
    }

    public function testCreateFaq(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/faqs/create');

        $this->assertResponseIsSuccessful();
    }

    public function testUpdateFaq(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/faqs/1/update');

        $this->assertResponseIsSuccessful();
    }

    public function test_role_access()
    {
        $client = static::createClient();

        // given we have a user with wrong role
        $wrongUser = UserFactory::createOne(['roles' => [Roles::ROLE_NEWS]]);

        // when we try to login as this user
        $this->loginUser($client, $wrongUser->getEmail());

        // response should be access denied
        $client->request('GET', '/admin/faqs');
        $this->assertResponseStatusCodeSame(403);

        // but when we have a user with the correct role
        $messagesOperator = UserFactory::createOne(['roles' => [Roles::ROLE_FAQS]]);
        $this->loginUser($client, $messagesOperator->getEmail());

        // the response is 200 OK
        $client->request('GET', '/admin/faqs');
        $this->assertResponseIsSuccessful();
    }

    public function test_faq_is_created()
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        // Given we are at the create page
        $crawler = $client->request('GET', '/admin/faqs/create');

        // And we fill-in the form and submit it
        $form = $this->getForm('create', $crawler);
        $client->submit($form, [
            $this->formField('question', $form) => 'Test question',
            $this->formField('answer', $form) => 'Test answer',
        ]);

        // The response should be 301 redirect
        $this->assertResponseRedirects();

        // And there should be the new FAQ in the DB
        $question = FaqFactory::repository()->last()->object();
        $this->assertSame($question->getQuestion(), 'Test question');
    }
}
