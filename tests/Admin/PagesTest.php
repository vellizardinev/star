<?php

namespace App\Tests\Admin;

use App\Enum\Roles;
use App\Factory\UserFactory;
use App\Tests\BaseWebTestCaseTest;

class PagesTest extends BaseWebTestCaseTest
{
    public function testPagesList(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/pages');

        $this->assertResponseIsSuccessful();
    }

    public function testCreatePage(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/pages/create');

        $this->assertResponseIsSuccessful();
    }

    public function testUpdatePage(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/pages/1/update');

        $this->assertResponseIsSuccessful();
    }

    public function testManageContent(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/pages/1/manage-content');

        $this->assertResponseIsSuccessful();
    }

    public function test_role_access()
    {
        $client = static::createClient();

        // given we have a user with wrong role
        $wrongUser = UserFactory::createOne(['roles' => [Roles::ROLE_MODULES]]);

        // when we try to login as this user
        $this->loginUser($client, $wrongUser->getEmail());

        // response should be access denied
        $client->request('GET', '/admin/pages');
        $this->assertResponseStatusCodeSame(403);

        // but when we have a user with the correct role
        $messagesOperator = UserFactory::createOne(['roles' => [Roles::ROLE_PAGES]]);
        $this->loginUser($client, $messagesOperator->getEmail());

        // the response is 200 OK
        $client->request('GET', '/admin/pages');
        $this->assertResponseIsSuccessful();
    }
}
