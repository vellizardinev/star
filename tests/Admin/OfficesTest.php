<?php

namespace App\Tests\Admin;

use App\Enum\Roles;
use App\Factory\UserFactory;
use App\Tests\BaseWebTestCaseTest;

class OfficesTest extends BaseWebTestCaseTest
{
    public function testOfficesList(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/offices');

        $this->assertResponseIsSuccessful();
    }

    public function testCreateOffice(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/offices/create');

        $this->assertResponseIsSuccessful();
    }

    public function testUpdateOffice(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/offices/1/update');

        $this->assertResponseIsSuccessful();
    }

    public function test_role_access()
    {
        $client = static::createClient();

        // given we have a user with wrong role
        $wrongUser = UserFactory::createOne(['roles' => [Roles::ROLE_DEPARTMENTS]]);

        // when we try to login as this user
        $this->loginUser($client, $wrongUser->getEmail());

        // response should be access denied
        $client->request('GET', '/admin/offices');
        $this->assertResponseStatusCodeSame(403);

        // but when we have a user with the correct role
        $messagesOperator = UserFactory::createOne(['roles' => [Roles::ROLE_OFFICES]]);
        $this->loginUser($client, $messagesOperator->getEmail());

        // the response is 200 OK
        $client->request('GET', '/admin/offices');
        $this->assertResponseIsSuccessful();
    }
}
