<?php

namespace App\Tests\Admin;

use App\Enum\Roles;
use App\Factory\DepartmentFactory;
use App\Factory\UserFactory;
use App\Tests\BaseWebTestCaseTest;

class DepartmentsTest extends BaseWebTestCaseTest
{
    public function testDepartmentsList(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/departments');

        $this->assertResponseIsSuccessful();
    }

    public function testCreateDepartment(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/departments/create');

        $this->assertResponseIsSuccessful();
    }

    public function testUpdateDepartment(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/departments/1/update');

        $this->assertResponseIsSuccessful();
    }

    public function test_role_access()
    {
        $client = static::createClient();

        // given we have a user with wrong role
        $wrongUser = UserFactory::createOne(['roles' => [Roles::ROLE_PAGES]]);

        // when we try to login as this user
        $this->loginUser($client, $wrongUser->getEmail());

        // response should be access denied
        $client->request('GET', '/admin/departments');
        $this->assertResponseStatusCodeSame(403);

        // but when we have a user with the correct role
        $messagesOperator = UserFactory::createOne(['roles' => [Roles::ROLE_DEPARTMENTS]]);
        $this->loginUser($client, $messagesOperator->getEmail());

        // the response is 200 OK
        $client->request('GET', '/admin/departments');
        $this->assertResponseIsSuccessful();
    }

    public function test_department_is_created()
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        // Given we are at the create page
        $crawler = $client->request('GET', '/admin/departments/create');

        // And we fill-in the form and submit it
        $form = $this->getForm('create', $crawler);
        $client->submit($form, [
            $this->formField('name', $form) => 'IT',
            $this->formField('director', $form) => 'Big Boss',
        ]);

        // The response should be 301 redirect
        $this->assertResponseRedirects();

        // And there should be the new Department in the DB
        $department = DepartmentFactory::repository()->last()->object();
        $this->assertSame($department->getDirector(), 'Big Boss');
    }
}
