<?php

namespace App\Tests\Admin;

use App\Enum\Roles;
use App\Factory\UserFactory;
use App\Tests\BaseWebTestCaseTest;

class UsersTest extends BaseWebTestCaseTest
{
    public function testUsersList(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/users');

        $this->assertResponseIsSuccessful();
    }

    public function testCreateUser(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/users/create');

        $this->assertResponseIsSuccessful();
    }

    public function testManageRoles(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/users/1/manage-roles');

        $this->assertResponseIsSuccessful();
    }

    public function testManageGroups(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/users/1/manage-groups');

        $this->assertResponseIsSuccessful();
    }

    public function testChangePassword(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/users/change-password/1');

        $this->assertResponseIsSuccessful();
    }

    public function test_role_access()
    {
        $client = static::createClient();

        // given we have a user with wrong role
        $wrongUser = UserFactory::createOne(['roles' => [Roles::ROLE_PAGES]]);

        // when we try to login as this user
        $this->loginUser($client, $wrongUser->getEmail());

        // response should be access denied
        $client->request('GET', '/admin/users');
        $this->assertResponseStatusCodeSame(403);

        // but when we have a user with the correct role
        $messagesOperator = UserFactory::createOne(['roles' => [Roles::ADMINISTRATOR]]);
        $this->loginUser($client, $messagesOperator->getEmail());

        // the response is 200 OK
        $client->request('GET', '/admin/users');
        $this->assertResponseIsSuccessful();
    }
}
