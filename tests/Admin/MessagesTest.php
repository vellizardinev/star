<?php

namespace App\Tests\Admin;

use App\Enum\Roles;
use App\Factory\UserFactory;
use App\Tests\BaseWebTestCaseTest;

class MessagesTest extends BaseWebTestCaseTest
{
    public function testMessagesList(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/messages');

        $this->assertResponseIsSuccessful();
    }

    public function testCreateMessage(): void
    {
        $client = static::createClient();

        $this->loginAsAdmin($client);

        $client->request('GET', '/admin/messages/create');

        $this->assertResponseIsSuccessful();
    }

    public function test_messages_section_is_visible_only_for_users_with_the_role_messages_operator()
    {
        $client = static::createClient();

        // given we have a user with marketing role
        $marketingOperator = UserFactory::createOne(['roles' => [Roles::ROLE_NEWS]]);

        // when we try to login as this user
        $this->loginUser($client, $marketingOperator->getEmail());

        // response should be access denied
        $client->request('GET', '/admin/messages');
        $this->assertResponseStatusCodeSame(403);

        // but when we have a user with the correct role
        $messagesOperator = UserFactory::createOne(['roles' => [Roles::ROLE_MESSAGES_OPERATOR]]);
        $this->loginUser($client, $messagesOperator->getEmail());

        // the response is 200 OK
        $client->request('GET', '/admin/messages');
        $this->assertResponseIsSuccessful();

        // and when messages operator visits another page
        $client->request('GET', '/admin/news');

        // response should be access denied
        $this->assertResponseStatusCodeSame(403);
    }
}
