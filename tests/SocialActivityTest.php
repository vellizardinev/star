<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SocialActivityTest extends WebTestCase
{
    public function testIndexPage()
    {
        $client = static::createClient();
        $client->request('GET', '/sotsialna-aktyvnist');

        $this->assertResponseIsSuccessful();
    }
}
