<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactTest extends WebTestCase
{
    public function testContactsPage()
    {
        $client = static::createClient();
        $client->request('GET', '/kontakty');

        $this->assertResponseIsSuccessful();
    }
}
