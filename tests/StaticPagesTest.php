<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class StaticPagesTest extends WebTestCase
{
    public function testHomePage()
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
    }

    public function testAboutUs()
    {
        $client = static::createClient();
        $client->request('GET', '/pro-nas');

        $this->assertResponseIsSuccessful();
    }
}
