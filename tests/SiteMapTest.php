<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SiteMapTest extends WebTestCase
{
    public function testSiteMapPage()
    {
        $client = static::createClient();
        $client->request('GET', '/site-map');

        $this->assertResponseIsSuccessful();
    }
}
