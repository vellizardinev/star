<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreditsTest extends WebTestCase
{
    public function testIndexPage()
    {
        $client = static::createClient();
        $client->request('GET', '/kredyty');

        $this->assertResponseIsSuccessful();
    }

    public function testApplyPage()
    {
        $client = static::createClient();
        $client->request('GET', '/kredyty/pryiniaty');

        $this->assertResponseIsSuccessful();
    }

}
