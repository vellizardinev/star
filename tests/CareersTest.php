<?php

namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CareersTest extends WebTestCase
{
    public function testBecomeCreditConsultantPage()
    {
        $client = static::createClient();
        $client->request('GET', '/kariera/staty-kredytnym-konsultantom');

        $this->assertResponseIsSuccessful();
    }
}
