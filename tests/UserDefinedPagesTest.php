<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserDefinedPagesTest extends WebTestCase
{
    public function testFinancialDictionaryPage()
    {
        $client = static::createClient();
        $client->request('GET', '/finansovyi-slovnyk');

        $this->assertResponseIsSuccessful();
    }

    public function testFamilyFinancesPage()
    {
        $client = static::createClient();
        $client->request('GET', '/simeini-finansy');

        $this->assertResponseIsSuccessful();
    }

    public function testTermsAndConditionsPage()
    {
        $client = static::createClient();
        $client->request('GET', '/yurydychna-informatsiia');

        $this->assertResponseIsSuccessful();
    }

    public function testICreditAsAnEmployerPage()
    {
        $client = static::createClient();
        $client->request('GET', '/icredit-yak-robotodavets');

        $this->assertResponseIsSuccessful();
    }
}
