<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NewsTest extends WebTestCase
{
    public function testIndexPage()
    {
        $client = static::createClient();
        $client->request('GET', '/novyny');

        $this->assertResponseIsSuccessful();
    }

    public function testShowPage()
    {
        $client = static::createClient();
        $client->request('GET', '/novyny/kredity-na-remont');

        $this->assertResponseIsSuccessful();
    }
}
