<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistrationTest extends WebTestCase
{
    public function testRegistrationPage()
    {
        $client = static::createClient();
        $client->request('GET', '/customer/registration');

        $this->assertResponseIsSuccessful();
    }
}
