<?php

namespace App\Tests;

use App\Factory\PromotionFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PromotionsTest extends WebTestCase
{
    public function testIndexPage()
    {
        $client = static::createClient();
        $client->request('GET', '/aktsii');

        $this->assertResponseIsSuccessful();
    }

    public function testShowPromotion()
    {
        $client = static::createClient();
        $promotion = PromotionFactory::createOne(['slug' => 'test']);
        $client->request('GET', '/aktsii/' . $promotion->getSlug());
        $this->assertResponseIsSuccessful();
    }
}
