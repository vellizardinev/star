<?php

namespace App\Tests;

use App\Factory\UserFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\DomCrawler\Form;
use Zenstruck\Foundry\Test\Factories;

abstract class BaseWebTestCaseTest extends WebTestCase
{
    use Factories;

    public function loginAsAdmin($client): void
    {
        $client->loginUser(UserFactory::repository()->findOneBy(['email' => 'admin@siriussoftware.bg'])->object());
    }

    public function loginUser($client, string $email)
    {
        $client->loginUser(UserFactory::repository()->findOneBy(['email' => $email])->object());
    }

    public function getForm(string $name, Crawler $crawler): ?Form
    {
        $button = $crawler->selectButton($name);
        return $button->form();
    }

    public function formField(string $field, Form $form): string
    {
        return sprintf('%s[%s]', $form->getName(), $field);
    }
}
