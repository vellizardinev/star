<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoyaltyProgramTest extends WebTestCase
{
    public function testIndexPage()
    {
        $client = static::createClient();
        $client->request('GET', '/prohrama-loialnosti');

        $this->assertResponseIsSuccessful();
    }

    public function testGiftsPage()
    {
        $client = static::createClient();
        $client->request('GET', '/prohrama-loialnosti/podarunky');

        $this->assertResponseIsSuccessful();
    }

    public function testIBonusPage()
    {
        $client = static::createClient();
        $client->request('GET', '/prohrama-loialnosti/i-bonus');

        $this->assertResponseIsSuccessful();
    }
}
