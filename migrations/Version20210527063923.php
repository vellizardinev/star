<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210527063923 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
//        // this up() migration is auto-generated, please modify it to your needs
//        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
//
//        $this->addSql('ALTER TABLE uploaded_documents ADD individual_tax_number_id INT DEFAULT NULL');
//        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77675089220 FOREIGN KEY (individual_tax_number_id) REFERENCES file (id) ON DELETE SET NULL');
//        $this->addSql('CREATE UNIQUE INDEX UNIQ_252AB77675089220 ON uploaded_documents (individual_tax_number_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
//        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
//
//        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB77675089220');
//        $this->addSql('DROP INDEX UNIQ_252AB77675089220 ON uploaded_documents');
//        $this->addSql('ALTER TABLE uploaded_documents DROP individual_tax_number_id');
    }
}
