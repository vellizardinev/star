<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Module;
use App\Entity\ModuleItem;
use App\Entity\Page;
use App\Enum\Pages;
use App\Module\Parameter\Content;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200409131316 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getDescription() : string
    {
        return 'Add module items to thank you page for all locales.';
    }

    /**
     * @param Schema $schema
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function up(Schema $schema) : void
    {
        $manager = $this->container->get('doctrine.orm.default_entity_manager');

        if ($page = $manager->getRepository(Page::class)
            ->findOneBy(['internalName' => Pages::CREDIT_LONG_THANK_YOU])) {

            /** @var $page Page */
            foreach ($page->getModuleItems() as $moduleItem) {
                $manager->remove($moduleItem);
            }

            $manager->flush();

            $contentModule = $manager->getRepository(Module::class)->findOneBy(['name' => 'Content']);

            $contentData = new Content();
            $contentData->content = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';

            $moduleItem = new ModuleItem();
            $manager->persist($moduleItem);
            $moduleItem->setModule($contentModule);
            $moduleItem->setPage($page);
            $moduleItem->setData($this->createData($contentData));
            $page->addModuleItem($moduleItem);

            $manager->persist($moduleItem);
            $manager->flush();
        }

    }

    public function down(Schema $schema) : void
    {
    }

    protected function createData($parametersObject)
    {
        $result = [];
        $locales = ['en', 'ua', 'ru'];

        foreach ($locales as $locale) {
            $result[$locale] = clone $parametersObject;
        }

        return serialize($result);
    }
}
