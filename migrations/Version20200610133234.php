<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200610133234 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Set null on delete';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB77626CC1D54');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776710923B3');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776A98CE8F4');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776FE49D613');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB77626ACD8CE');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB77631E34F15');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB7763BC44D36');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB7764AAC1356');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB7765C5CBD24');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB77674238712');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776748DF2BF');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB7767F8B96E1');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776A72EF0FF');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776BC8F1156');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776F84473F0');
        $this->addSql('DROP INDEX UNIQ_252AB77626CC1D54 ON uploaded_documents');
        $this->addSql('DROP INDEX UNIQ_252AB776A98CE8F4 ON uploaded_documents');
        $this->addSql('DROP INDEX UNIQ_252AB776FE49D613 ON uploaded_documents');
        $this->addSql('DROP INDEX UNIQ_252AB776710923B3 ON uploaded_documents');
        $this->addSql('ALTER TABLE uploaded_documents ADD old_style_passport_photo_at_25_id INT DEFAULT NULL, ADD old_style_passport_pasted_photo_at_25_id INT DEFAULT NULL, ADD old_style_passport_photo_at_45_id INT DEFAULT NULL, ADD old_style_passport_pasted_photo_at_45_id INT DEFAULT NULL, DROP old_style_passport_photo_at25_id, DROP old_style_passport_pasted_photo_at25_id, DROP old_style_passport_photo_at45_id, DROP old_style_passport_pasted_photo_at45_id, CHANGE old_style_passport_first_page_id old_style_passport_first_page_id INT DEFAULT NULL, CHANGE old_style_passport_second_page_id old_style_passport_second_page_id INT DEFAULT NULL, CHANGE old_style_passport_proof_of_residence_id old_style_passport_proof_of_residence_id INT DEFAULT NULL, CHANGE old_style_passport_selfie_with_passport_id old_style_passport_selfie_with_passport_id INT DEFAULT NULL, CHANGE new_style_passport_photo_page_id new_style_passport_photo_page_id INT DEFAULT NULL, CHANGE new_style_passport_validity_page_id new_style_passport_validity_page_id INT DEFAULT NULL, CHANGE new_style_passport_registration_appendix_id new_style_passport_registration_appendix_id INT DEFAULT NULL, CHANGE new_style_passport_selfie_with_id_card_id new_style_passport_selfie_with_id_card_id INT DEFAULT NULL, CHANGE old_style_pension_certificate_first_spread_id old_style_pension_certificate_first_spread_id INT DEFAULT NULL, CHANGE old_style_pension_certificate_validity_record_id old_style_pension_certificate_validity_record_id INT DEFAULT NULL, CHANGE new_style_pension_certificate_photo_and_validity_spread_id new_style_pension_certificate_photo_and_validity_spread_id INT DEFAULT NULL, CHANGE pension_certificate_type pension_certificate_type VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776E53DC0BD FOREIGN KEY (old_style_passport_photo_at_25_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776D5D9145 FOREIGN KEY (old_style_passport_pasted_photo_at_25_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7766A7D351D FOREIGN KEY (old_style_passport_photo_at_45_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776821D64E5 FOREIGN KEY (old_style_passport_pasted_photo_at_45_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77626ACD8CE FOREIGN KEY (old_style_passport_first_page_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77631E34F15 FOREIGN KEY (new_style_pension_certificate_photo_and_validity_spread_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7763BC44D36 FOREIGN KEY (old_style_pension_certificate_first_spread_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7764AAC1356 FOREIGN KEY (new_style_passport_selfie_with_id_card_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7765C5CBD24 FOREIGN KEY (old_style_pension_certificate_validity_record_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77674238712 FOREIGN KEY (new_style_passport_registration_appendix_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776748DF2BF FOREIGN KEY (old_style_passport_second_page_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7767F8B96E1 FOREIGN KEY (new_style_passport_validity_page_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776A72EF0FF FOREIGN KEY (old_style_passport_proof_of_residence_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776BC8F1156 FOREIGN KEY (new_style_passport_photo_page_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776F84473F0 FOREIGN KEY (old_style_passport_selfie_with_passport_id) REFERENCES file (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_252AB776E53DC0BD ON uploaded_documents (old_style_passport_photo_at_25_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_252AB776D5D9145 ON uploaded_documents (old_style_passport_pasted_photo_at_25_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_252AB7766A7D351D ON uploaded_documents (old_style_passport_photo_at_45_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_252AB776821D64E5 ON uploaded_documents (old_style_passport_pasted_photo_at_45_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776E53DC0BD');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776D5D9145');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB7766A7D351D');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776821D64E5');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB77626ACD8CE');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776748DF2BF');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776A72EF0FF');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776F84473F0');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB776BC8F1156');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB7767F8B96E1');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB77674238712');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB7764AAC1356');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB7763BC44D36');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB7765C5CBD24');
        $this->addSql('ALTER TABLE uploaded_documents DROP FOREIGN KEY FK_252AB77631E34F15');
        $this->addSql('DROP INDEX UNIQ_252AB776E53DC0BD ON uploaded_documents');
        $this->addSql('DROP INDEX UNIQ_252AB776D5D9145 ON uploaded_documents');
        $this->addSql('DROP INDEX UNIQ_252AB7766A7D351D ON uploaded_documents');
        $this->addSql('DROP INDEX UNIQ_252AB776821D64E5 ON uploaded_documents');
        $this->addSql('ALTER TABLE uploaded_documents ADD old_style_passport_photo_at25_id INT DEFAULT NULL, ADD old_style_passport_pasted_photo_at25_id INT DEFAULT NULL, ADD old_style_passport_photo_at45_id INT DEFAULT NULL, ADD old_style_passport_pasted_photo_at45_id INT DEFAULT NULL, DROP old_style_passport_photo_at_25_id, DROP old_style_passport_pasted_photo_at_25_id, DROP old_style_passport_photo_at_45_id, DROP old_style_passport_pasted_photo_at_45_id, CHANGE old_style_passport_first_page_id old_style_passport_first_page_id INT DEFAULT NULL, CHANGE old_style_passport_second_page_id old_style_passport_second_page_id INT DEFAULT NULL, CHANGE old_style_passport_proof_of_residence_id old_style_passport_proof_of_residence_id INT DEFAULT NULL, CHANGE old_style_passport_selfie_with_passport_id old_style_passport_selfie_with_passport_id INT DEFAULT NULL, CHANGE new_style_passport_photo_page_id new_style_passport_photo_page_id INT DEFAULT NULL, CHANGE new_style_passport_validity_page_id new_style_passport_validity_page_id INT DEFAULT NULL, CHANGE new_style_passport_registration_appendix_id new_style_passport_registration_appendix_id INT DEFAULT NULL, CHANGE new_style_passport_selfie_with_id_card_id new_style_passport_selfie_with_id_card_id INT DEFAULT NULL, CHANGE old_style_pension_certificate_first_spread_id old_style_pension_certificate_first_spread_id INT DEFAULT NULL, CHANGE old_style_pension_certificate_validity_record_id old_style_pension_certificate_validity_record_id INT DEFAULT NULL, CHANGE new_style_pension_certificate_photo_and_validity_spread_id new_style_pension_certificate_photo_and_validity_spread_id INT DEFAULT NULL, CHANGE pension_certificate_type pension_certificate_type VARCHAR(255) CHARACTER SET utf8 DEFAULT \'NULL\' COLLATE `utf8_unicode_ci`');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77626CC1D54 FOREIGN KEY (old_style_passport_photo_at45_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776710923B3 FOREIGN KEY (old_style_passport_pasted_photo_at25_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776A98CE8F4 FOREIGN KEY (old_style_passport_photo_at25_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776FE49D613 FOREIGN KEY (old_style_passport_pasted_photo_at45_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77626ACD8CE FOREIGN KEY (old_style_passport_first_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776748DF2BF FOREIGN KEY (old_style_passport_second_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776A72EF0FF FOREIGN KEY (old_style_passport_proof_of_residence_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776F84473F0 FOREIGN KEY (old_style_passport_selfie_with_passport_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776BC8F1156 FOREIGN KEY (new_style_passport_photo_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7767F8B96E1 FOREIGN KEY (new_style_passport_validity_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77674238712 FOREIGN KEY (new_style_passport_registration_appendix_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7764AAC1356 FOREIGN KEY (new_style_passport_selfie_with_id_card_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7763BC44D36 FOREIGN KEY (old_style_pension_certificate_first_spread_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7765C5CBD24 FOREIGN KEY (old_style_pension_certificate_validity_record_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77631E34F15 FOREIGN KEY (new_style_pension_certificate_photo_and_validity_spread_id) REFERENCES file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_252AB77626CC1D54 ON uploaded_documents (old_style_passport_photo_at45_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_252AB776A98CE8F4 ON uploaded_documents (old_style_passport_photo_at25_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_252AB776FE49D613 ON uploaded_documents (old_style_passport_pasted_photo_at45_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_252AB776710923B3 ON uploaded_documents (old_style_passport_pasted_photo_at25_id)');
    }
}
