<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200612122103 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Creates nomenclature table and add new fields';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE nomenclature (id INT AUTO_INCREMENT NOT NULL, guid VARCHAR(255) NOT NULL, text VARCHAR(255) NOT NULL, visible TINYINT(1) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE file ADD is_sent_to_erp TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE online_application ADD erp_id VARCHAR(255) DEFAULT NULL, ADD is_sent_to_erp TINYINT(1) NOT NULL, ADD api_response LONGTEXT DEFAULT NULL, ADD json_data LONGTEXT DEFAULT NULL, CHANGE credit_details_id credit_details_id INT DEFAULT NULL, CHANGE personal_info_id personal_info_id INT DEFAULT NULL, CHANGE uploaded_documents_id uploaded_documents_id INT DEFAULT NULL, CHANGE address_id address_id INT DEFAULT NULL, CHANGE work_details_id work_details_id INT DEFAULT NULL, CHANGE incomes_and_expenses_id incomes_and_expenses_id INT DEFAULT NULL, CHANGE property_id property_id INT DEFAULT NULL, CHANGE additional_info_id additional_info_id INT DEFAULT NULL, CHANGE contact_person_id contact_person_id INT DEFAULT NULL, CHANGE confirmation_id confirmation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE credit_data ADD credit_period_id VARCHAR(255) DEFAULT NULL, ADD product_id VARCHAR(255) DEFAULT NULL, CHANGE amount amount NUMERIC(10, 2) DEFAULT NULL, CHANGE periods periods INT DEFAULT NULL, CHANGE payment payment NUMERIC(10, 2) DEFAULT NULL, CHANGE payment_with_penalty payment_with_penalty NUMERIC(10, 2) DEFAULT NULL, CHANGE total total NUMERIC(10, 2) DEFAULT NULL, CHANGE total_with_penalty total_with_penalty NUMERIC(10, 2) DEFAULT NULL, CHANGE interest_rate_first_period interest_rate_first_period NUMERIC(10, 4) DEFAULT NULL, CHANGE interest_rate_second_period interest_rate_second_period NUMERIC(10, 4) DEFAULT NULL, CHANGE aprc aprc NUMERIC(10, 4) DEFAULT NULL, CHANGE payment_schedule payment_schedule VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE credit_details ADD credit_product_id VARCHAR(255) NOT NULL, ADD credit_period_id VARCHAR(255) NOT NULL, CHANGE iban iban VARCHAR(255) DEFAULT NULL, CHANGE bank_card_expiration_date bank_card_expiration_date DATE DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE nomenclature');
        $this->addSql('ALTER TABLE credit_data DROP credit_period_id, DROP product_id, CHANGE amount amount NUMERIC(10, 2) DEFAULT \'NULL\', CHANGE periods periods INT DEFAULT NULL, CHANGE payment payment NUMERIC(10, 2) DEFAULT \'NULL\', CHANGE payment_with_penalty payment_with_penalty NUMERIC(10, 2) DEFAULT \'NULL\', CHANGE total total NUMERIC(10, 2) DEFAULT \'NULL\', CHANGE total_with_penalty total_with_penalty NUMERIC(10, 2) DEFAULT \'NULL\', CHANGE interest_rate_first_period interest_rate_first_period NUMERIC(10, 4) DEFAULT \'NULL\', CHANGE interest_rate_second_period interest_rate_second_period NUMERIC(10, 4) DEFAULT \'NULL\', CHANGE aprc aprc NUMERIC(10, 4) DEFAULT \'NULL\', CHANGE payment_schedule payment_schedule VARCHAR(255) CHARACTER SET utf8 DEFAULT \'NULL\' COLLATE `utf8_unicode_ci`');
        $this->addSql('ALTER TABLE credit_details DROP credit_product_id, DROP credit_period_id, CHANGE iban iban VARCHAR(255) CHARACTER SET utf8 DEFAULT \'NULL\' COLLATE `utf8_unicode_ci`, CHANGE bank_card_expiration_date bank_card_expiration_date DATE DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE file DROP is_sent_to_erp');
        $this->addSql('ALTER TABLE online_application DROP erp_id, DROP is_sent_to_erp, DROP api_response, DROP json_data, CHANGE credit_details_id credit_details_id INT DEFAULT NULL, CHANGE personal_info_id personal_info_id INT DEFAULT NULL, CHANGE uploaded_documents_id uploaded_documents_id INT DEFAULT NULL, CHANGE address_id address_id INT DEFAULT NULL, CHANGE work_details_id work_details_id INT DEFAULT NULL, CHANGE incomes_and_expenses_id incomes_and_expenses_id INT DEFAULT NULL, CHANGE property_id property_id INT DEFAULT NULL, CHANGE additional_info_id additional_info_id INT DEFAULT NULL, CHANGE contact_person_id contact_person_id INT DEFAULT NULL, CHANGE confirmation_id confirmation_id INT DEFAULT NULL');
    }
}
