<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Setting;
use App\Enum\Settings;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200401102413 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getDescription() : string
    {
        return 'Add setting for email where long credit applications are sent.';
    }

    /**
     * @param Schema $schema
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $manager = $this->container->get('doctrine.orm.default_entity_manager');

        $setting = new Setting();
        $setting->setName(Settings::LONG_APPLICATIONS_EMAIL);
        $setting->setUiLabel('Email to send long credit applications to');
        $setting->setValue('online@icredit.ua');

        $manager->persist($setting);
        $manager->flush();
    }

    /**
     * @param Schema $schema
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function down(Schema $schema) : void
    {
        $manager = $this->container->get('doctrine.orm.default_entity_manager');
        $setting = $manager
            ->getRepository(Setting::class)
            ->findOneBy(['name' => Settings::LONG_APPLICATIONS_EMAIL]);

        if ($setting) {
            $manager->remove($setting);
            $manager->flush();
        }
    }
}
