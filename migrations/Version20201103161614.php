<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201103161614 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE http_request (id INT AUTO_INCREMENT NOT NULL, application_id INT DEFAULT NULL, url LONGTEXT NOT NULL, method VARCHAR(255) NOT NULL, options LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', success TINYINT(1) NOT NULL, attempts INT NOT NULL, last_attempted_on DATETIME DEFAULT NULL, interval_in_seconds INT NOT NULL, multiplier INT NOT NULL, last_response LONGTEXT DEFAULT NULL, max_attempts INT NOT NULL, last_response_code INT DEFAULT NULL, completed TINYINT(1) NOT NULL, next_attempt_on DATETIME DEFAULT NULL, dtype VARCHAR(255) NOT NULL, INDEX IDX_3E8CAA543E030ACD (application_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE http_request ADD CONSTRAINT FK_3E8CAA543E030ACD FOREIGN KEY (application_id) REFERENCES online_application (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE http_request');
    }
}
