<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200403063752 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE credit_long_application (id INT AUTO_INCREMENT NOT NULL, mobile_telephone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, personal_identification_number VARCHAR(255) NOT NULL, amount NUMERIC(10, 2) NOT NULL, product VARCHAR(255) NOT NULL, period INT NOT NULL, instalment_amount NUMERIC(10, 2) NOT NULL, first_name VARCHAR(255) NOT NULL, middle_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, id_card_number VARCHAR(255) NOT NULL, id_card_issued_at DATETIME NOT NULL, id_card_issued_by VARCHAR(255) NOT NULL, current_address_same_as_permanent_address TINYINT(1) NOT NULL, company_name VARCHAR(255) NOT NULL, company_position VARCHAR(255) NOT NULL, company_telephone VARCHAR(255) NOT NULL, work_experience_current_employer INT NOT NULL, work_experience_total INT NOT NULL, monthly_income NUMERIC(10, 2) NOT NULL, other_income NUMERIC(10, 2) NOT NULL, how_salary_is_received VARCHAR(255) NOT NULL, household_expenses_electricity_and_water NUMERIC(10, 2) NOT NULL, household_expenses_internet_and_tv NUMERIC(10, 2) NOT NULL, household_expenses_telephone NUMERIC(10, 2) NOT NULL, type_of_home VARCHAR(255) NOT NULL, disadvantaged TINYINT(1) NOT NULL, industry VARCHAR(255) NOT NULL, employment_type VARCHAR(255) NOT NULL, type_of_work VARCHAR(255) NOT NULL, education VARCHAR(255) NOT NULL, purpose_of_credit VARCHAR(255) NOT NULL, wants_discretion TINYINT(1) NOT NULL, household_size INT NOT NULL, family_status VARCHAR(255) NOT NULL, how_should_money_be_received VARCHAR(255) NOT NULL, is_political_person TINYINT(1) NOT NULL, is_related_to_political_person TINYINT(1) NOT NULL, country_of_birth VARCHAR(255) NOT NULL, nationality VARCHAR(255) NOT NULL, country_of_residence VARCHAR(255) NOT NULL, day_for_receiving_salary INT DEFAULT NULL, frequency_of_receiving_salary VARCHAR(255) DEFAULT NULL, iban VARCHAR(255) DEFAULT NULL, household_expenses_food_and_clothes NUMERIC(10, 2) NOT NULL, household_expenses_kids NUMERIC(10, 2) NOT NULL, household_expenses_rent NUMERIC(10, 2) NOT NULL, household_expenses_transport NUMERIC(10, 2) NOT NULL, household_expenses_drinks_cigarettes NUMERIC(10, 2) NOT NULL, household_expenses_other_credits NUMERIC(10, 2) NOT NULL, household_expenses_other_credits_instalment_amount NUMERIC(10, 2) NOT NULL, household_expenses_heating NUMERIC(10, 2) NOT NULL, household_expenses_other NUMERIC(10, 2) NOT NULL, expiration_date_of_employment_contract DATETIME DEFAULT NULL, found_out_about_the_company_from VARCHAR(255) NOT NULL, payback_credit_amount_method VARCHAR(255) NOT NULL, living_on_address_from_date DATETIME NOT NULL, bonus_income NUMERIC(10, 2) NOT NULL, social_income NUMERIC(10, 2) NOT NULL, rent_income NUMERIC(10, 2) NOT NULL, civil_contract_income NUMERIC(10, 2) NOT NULL, pension NUMERIC(10, 2) NOT NULL, incomes_total NUMERIC(10, 2) NOT NULL, expenses_total NUMERIC(10, 2) NOT NULL, current_address_area LONGTEXT NOT NULL, current_address_municipality LONGTEXT NOT NULL, current_address_city LONGTEXT NOT NULL, current_address_post_code LONGTEXT DEFAULT NULL, current_address_neighbourhood LONGTEXT DEFAULT NULL, current_address_block_number LONGTEXT NOT NULL, current_address_street LONGTEXT NOT NULL, current_address_entrance LONGTEXT DEFAULT NULL, current_address_floor LONGTEXT DEFAULT NULL, current_address_apartment LONGTEXT NOT NULL, permanent_address_area LONGTEXT NOT NULL, permanent_address_municipality LONGTEXT NOT NULL, permanent_address_city LONGTEXT NOT NULL, permanent_address_post_code LONGTEXT DEFAULT NULL, permanent_address_neighbourhood LONGTEXT DEFAULT NULL, permanent_address_block_number LONGTEXT NOT NULL, permanent_address_street LONGTEXT NOT NULL, permanent_address_entrance LONGTEXT DEFAULT NULL, permanent_address_floor LONGTEXT DEFAULT NULL, permanent_address_apartment LONGTEXT NOT NULL, work_address_area LONGTEXT NOT NULL, work_address_municipality LONGTEXT NOT NULL, work_address_city LONGTEXT NOT NULL, work_address_post_code LONGTEXT DEFAULT NULL, work_address_neighbourhood LONGTEXT DEFAULT NULL, work_address_block_number LONGTEXT NOT NULL, work_address_street LONGTEXT NOT NULL, work_address_entrance LONGTEXT DEFAULT NULL, work_address_floor LONGTEXT DEFAULT NULL, work_address_apartment LONGTEXT NOT NULL, contact_person_first_name VARCHAR(255) NOT NULL, contact_person_middle_name VARCHAR(255) NOT NULL, contact_person_last_name VARCHAR(255) NOT NULL, contact_person_date_of_birth DATETIME NOT NULL, contact_person_mobile_telephone VARCHAR(255) NOT NULL, contact_person_relationship VARCHAR(255) NOT NULL, has_property VARCHAR(255) NOT NULL, has_car TINYINT(1) NOT NULL, number_of_cars INT DEFAULT NULL, car_year_of_manufacture INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE credit_long_application_files (credit_application_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_D2E76F6EC9F772CD (credit_application_id), UNIQUE INDEX UNIQ_D2E76F6E93CB796C (file_id), PRIMARY KEY(credit_application_id, file_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE credit_long_application_files ADD CONSTRAINT FK_D2E76F6EC9F772CD FOREIGN KEY (credit_application_id) REFERENCES credit_long_application (id)');
        $this->addSql('ALTER TABLE credit_long_application_files ADD CONSTRAINT FK_D2E76F6E93CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE credit_long_application_files DROP FOREIGN KEY FK_D2E76F6EC9F772CD');
        $this->addSql('DROP TABLE credit_long_application');
        $this->addSql('DROP TABLE credit_long_application_files');
    }
}
