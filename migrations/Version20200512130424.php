<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200512130424 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE credit_long_application ADD old_style_passport_first_page_id INT DEFAULT NULL, ADD old_style_passport_second_page_id INT DEFAULT NULL, ADD old_style_passport_photo_at25_id INT DEFAULT NULL, ADD old_style_passport_pasted_photo_at25_id INT DEFAULT NULL, ADD old_style_passport_photo_at45_id INT DEFAULT NULL, ADD old_style_passport_pasted_photo_at45_id INT DEFAULT NULL, ADD old_style_passport_proof_of_residence_id INT DEFAULT NULL, ADD old_style_passport_selfie_with_passport_id INT DEFAULT NULL, ADD new_style_passport_photo_page_id INT DEFAULT NULL, ADD new_style_passport_validity_page_id INT DEFAULT NULL, ADD new_style_passport_registration_appendix_id INT DEFAULT NULL, ADD new_style_passport_selfie_with_id_card_id INT DEFAULT NULL, ADD old_style_pension_certificate_first_spread_id INT DEFAULT NULL, ADD old_style_pension_certificate_validity_record_id INT DEFAULT NULL, ADD new_style_pension_certificate_photo_and_validity_spread_id INT DEFAULT NULL, ADD passport_type VARCHAR(255) NOT NULL, ADD pension_certificate_type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F45326126ACD8CE FOREIGN KEY (old_style_passport_first_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F453261748DF2BF FOREIGN KEY (old_style_passport_second_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F453261A98CE8F4 FOREIGN KEY (old_style_passport_photo_at25_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F453261710923B3 FOREIGN KEY (old_style_passport_pasted_photo_at25_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F45326126CC1D54 FOREIGN KEY (old_style_passport_photo_at45_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F453261FE49D613 FOREIGN KEY (old_style_passport_pasted_photo_at45_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F453261A72EF0FF FOREIGN KEY (old_style_passport_proof_of_residence_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F453261F84473F0 FOREIGN KEY (old_style_passport_selfie_with_passport_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F453261BC8F1156 FOREIGN KEY (new_style_passport_photo_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F4532617F8B96E1 FOREIGN KEY (new_style_passport_validity_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F45326174238712 FOREIGN KEY (new_style_passport_registration_appendix_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F4532614AAC1356 FOREIGN KEY (new_style_passport_selfie_with_id_card_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F4532613BC44D36 FOREIGN KEY (old_style_pension_certificate_first_spread_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F4532615C5CBD24 FOREIGN KEY (old_style_pension_certificate_validity_record_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE credit_long_application ADD CONSTRAINT FK_8F45326131E34F15 FOREIGN KEY (new_style_pension_certificate_photo_and_validity_spread_id) REFERENCES file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F45326126ACD8CE ON credit_long_application (old_style_passport_first_page_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F453261748DF2BF ON credit_long_application (old_style_passport_second_page_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F453261A98CE8F4 ON credit_long_application (old_style_passport_photo_at25_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F453261710923B3 ON credit_long_application (old_style_passport_pasted_photo_at25_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F45326126CC1D54 ON credit_long_application (old_style_passport_photo_at45_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F453261FE49D613 ON credit_long_application (old_style_passport_pasted_photo_at45_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F453261A72EF0FF ON credit_long_application (old_style_passport_proof_of_residence_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F453261F84473F0 ON credit_long_application (old_style_passport_selfie_with_passport_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F453261BC8F1156 ON credit_long_application (new_style_passport_photo_page_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F4532617F8B96E1 ON credit_long_application (new_style_passport_validity_page_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F45326174238712 ON credit_long_application (new_style_passport_registration_appendix_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F4532614AAC1356 ON credit_long_application (new_style_passport_selfie_with_id_card_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F4532613BC44D36 ON credit_long_application (old_style_pension_certificate_first_spread_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F4532615C5CBD24 ON credit_long_application (old_style_pension_certificate_validity_record_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8F45326131E34F15 ON credit_long_application (new_style_pension_certificate_photo_and_validity_spread_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F45326126ACD8CE');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F453261748DF2BF');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F453261A98CE8F4');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F453261710923B3');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F45326126CC1D54');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F453261FE49D613');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F453261A72EF0FF');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F453261F84473F0');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F453261BC8F1156');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F4532617F8B96E1');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F45326174238712');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F4532614AAC1356');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F4532613BC44D36');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F4532615C5CBD24');
        $this->addSql('ALTER TABLE credit_long_application DROP FOREIGN KEY FK_8F45326131E34F15');
        $this->addSql('DROP INDEX UNIQ_8F45326126ACD8CE ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F453261748DF2BF ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F453261A98CE8F4 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F453261710923B3 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F45326126CC1D54 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F453261FE49D613 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F453261A72EF0FF ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F453261F84473F0 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F453261BC8F1156 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F4532617F8B96E1 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F45326174238712 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F4532614AAC1356 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F4532613BC44D36 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F4532615C5CBD24 ON credit_long_application');
        $this->addSql('DROP INDEX UNIQ_8F45326131E34F15 ON credit_long_application');
        $this->addSql('ALTER TABLE credit_long_application DROP old_style_passport_first_page_id, DROP old_style_passport_second_page_id, DROP old_style_passport_photo_at25_id, DROP old_style_passport_pasted_photo_at25_id, DROP old_style_passport_photo_at45_id, DROP old_style_passport_pasted_photo_at45_id, DROP old_style_passport_proof_of_residence_id, DROP old_style_passport_selfie_with_passport_id, DROP new_style_passport_photo_page_id, DROP new_style_passport_validity_page_id, DROP new_style_passport_registration_appendix_id, DROP new_style_passport_selfie_with_id_card_id, DROP old_style_pension_certificate_first_spread_id, DROP old_style_pension_certificate_validity_record_id, DROP new_style_pension_certificate_photo_and_validity_spread_id, DROP passport_type, DROP pension_certificate_type');
    }
}
