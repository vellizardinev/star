<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\News;
use App\Entity\RedirectRule;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200226122140 extends AbstractMigration implements ContainerAwareInterface
{
	use ContainerAwareTrait;

    public function getDescription() : string
    {
        return 'Remove duplicate news articles and update redirect rules';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
	    $manager = $this->container->get('doctrine.orm.default_entity_manager');

	    $qb = $manager->createQueryBuilder();
	    $qb
		    ->select('news.id, news.slug')
		    ->from(News::class, 'news')
		    ->innerJoin(News::class, 'joined_news')
		    ->where('news.id > joined_news.id')
		    ->andWhere('news.title = joined_news.title');

	    $duplicateNewsArticle = $qb->getQuery()->getResult();

	    $duplicateNewsArticleIds = array_map(function($value) {
		    return $value['id'];
	    }, $duplicateNewsArticle);

	    $duplicateNewsArticleSlugs = array_map(function($value) {
		    return '/novyny/' . $value['slug'];
	    }, $duplicateNewsArticle);

	    $qb = $manager->createQueryBuilder();
	    $qb
		    ->delete(News::class, 'news')
		    ->where('news.id in (:news_article_ids)')
		    ->setParameter('news_article_ids', $duplicateNewsArticleIds);

	    $qb->getQuery()->execute();

	    $manager->flush();

	    $this->updateRedirectRules($manager, $duplicateNewsArticleSlugs);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    private function updateRedirectRules(EntityManager $manager, array $slugs)
    {
		foreach ($slugs as $slug){
			$redirectRule = $manager->getRepository(RedirectRule::class)->findOneBy(['newUrl' => $slug]);
			if($redirectRule){
				$redirectRule->setNewUrl(substr($slug, 0, -2));
			}
		}

	    $manager->flush();
    }
}
