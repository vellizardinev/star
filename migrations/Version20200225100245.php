<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\CreditData;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200225100245 extends AbstractMigration implements ContainerAwareInterface
{
	use ContainerAwareTrait;

    public function getDescription() : string
    {
        return 'Remove credit data with amount more than 50 000';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
	    $manager = $this->container->get('doctrine.orm.default_entity_manager');

	    $qb = $manager->createQueryBuilder();
	    $qb
		    ->delete(CreditData::class, 'credit_data')
		    ->where('credit_data.amount > :max_amount')
		    ->setParameter('max_amount', '50000');

	    $creditDataToRemove = $qb->getQuery()->execute();
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
