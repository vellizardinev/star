<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200526095343 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE online_application (id INT AUTO_INCREMENT NOT NULL, credit_details_id INT DEFAULT NULL, personal_info_id INT DEFAULT NULL, uploaded_documents_id INT DEFAULT NULL, address_id INT DEFAULT NULL, work_details_id INT DEFAULT NULL, incomes_and_expenses_id INT DEFAULT NULL, property_id INT DEFAULT NULL, additional_info_id INT DEFAULT NULL, contact_person_id INT DEFAULT NULL, confirmation_id INT DEFAULT NULL, is_completed TINYINT(1) NOT NULL, hash VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_3BE81A9A4862BC9C (credit_details_id), UNIQUE INDEX UNIQ_3BE81A9ADEACC8D3 (personal_info_id), UNIQUE INDEX UNIQ_3BE81A9A4602205E (uploaded_documents_id), UNIQUE INDEX UNIQ_3BE81A9AF5B7AF75 (address_id), UNIQUE INDEX UNIQ_3BE81A9AA079AAF1 (work_details_id), UNIQUE INDEX UNIQ_3BE81A9A7575A6C0 (incomes_and_expenses_id), UNIQUE INDEX UNIQ_3BE81A9A549213EC (property_id), UNIQUE INDEX UNIQ_3BE81A9A5C01120C (additional_info_id), UNIQUE INDEX UNIQ_3BE81A9A4F8A983C (contact_person_id), UNIQUE INDEX UNIQ_3BE81A9A6BACE54E (confirmation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_person (id INT AUTO_INCREMENT NOT NULL, contact_person_first_name VARCHAR(255) NOT NULL, contact_person_middle_name VARCHAR(255) NOT NULL, contact_person_last_name VARCHAR(255) NOT NULL, contact_person_date_of_birth DATETIME NOT NULL, contact_person_mobile_telephone VARCHAR(255) NOT NULL, contact_person_relationship VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, permanent_address_area LONGTEXT NOT NULL, permanent_address_municipality LONGTEXT NOT NULL, permanent_address_city LONGTEXT NOT NULL, permanent_address_post_code LONGTEXT DEFAULT NULL, permanent_address_neighbourhood LONGTEXT DEFAULT NULL, permanent_address_block_number LONGTEXT NOT NULL, permanent_address_street LONGTEXT NOT NULL, permanent_address_entrance LONGTEXT DEFAULT NULL, permanent_address_floor LONGTEXT DEFAULT NULL, permanent_address_apartment LONGTEXT NOT NULL, current_address_same_as_permanent_address TINYINT(1) NOT NULL, current_address_area LONGTEXT NOT NULL, current_address_municipality LONGTEXT NOT NULL, current_address_city LONGTEXT NOT NULL, current_address_post_code LONGTEXT DEFAULT NULL, current_address_neighbourhood LONGTEXT DEFAULT NULL, current_address_block_number LONGTEXT NOT NULL, current_address_street LONGTEXT NOT NULL, current_address_entrance LONGTEXT DEFAULT NULL, current_address_floor LONGTEXT DEFAULT NULL, current_address_apartment LONGTEXT NOT NULL, living_on_address_from_date DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE work_details (id INT AUTO_INCREMENT NOT NULL, company_name VARCHAR(255) NOT NULL, company_position VARCHAR(255) NOT NULL, work_address_area LONGTEXT NOT NULL, work_address_municipality LONGTEXT NOT NULL, work_address_city LONGTEXT NOT NULL, work_address_post_code LONGTEXT DEFAULT NULL, work_address_neighbourhood LONGTEXT DEFAULT NULL, work_address_block_number LONGTEXT NOT NULL, work_address_street LONGTEXT NOT NULL, work_address_entrance LONGTEXT DEFAULT NULL, work_address_floor LONGTEXT DEFAULT NULL, work_address_apartment LONGTEXT DEFAULT NULL, work_experience_current_employer INT NOT NULL, work_experience_total INT NOT NULL, how_salary_is_received VARCHAR(255) NOT NULL, day_for_receiving_salary INT DEFAULT NULL, frequency_of_receiving_salary VARCHAR(255) DEFAULT NULL, company_telephone VARCHAR(255) NOT NULL, employment_type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE credit_details (id INT AUTO_INCREMENT NOT NULL, amount NUMERIC(10, 2) NOT NULL, product VARCHAR(255) NOT NULL, period INT NOT NULL, instalment_amount NUMERIC(10, 2) NOT NULL, payback_credit_amount_method VARCHAR(255) NOT NULL, iban VARCHAR(255) DEFAULT NULL, how_should_money_be_received VARCHAR(255) NOT NULL, bank_card_expiration_date DATE DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE confirmation (id INT AUTO_INCREMENT NOT NULL, agreed_to_processing_of_personal_data TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE property (id INT AUTO_INCREMENT NOT NULL, has_property VARCHAR(255) NOT NULL, has_car TINYINT(1) NOT NULL, number_of_cars INT DEFAULT NULL, car_year_of_manufacture INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE additional_info (id INT AUTO_INCREMENT NOT NULL, industry VARCHAR(255) NOT NULL, expiration_date_of_employment_contract DATETIME DEFAULT NULL, type_of_work VARCHAR(255) NOT NULL, education VARCHAR(255) NOT NULL, purpose_of_credit VARCHAR(255) NOT NULL, type_of_home VARCHAR(255) NOT NULL, household_size INT NOT NULL, family_status VARCHAR(255) NOT NULL, found_out_about_the_company_from VARCHAR(255) NOT NULL, monthly_income NUMERIC(10, 2) NOT NULL, other_income NUMERIC(10, 2) NOT NULL, bonus_income NUMERIC(10, 2) NOT NULL, social_income NUMERIC(10, 2) NOT NULL, rent_income NUMERIC(10, 2) NOT NULL, civil_contract_income NUMERIC(10, 2) NOT NULL, pension NUMERIC(10, 2) NOT NULL, incomes_total NUMERIC(10, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personal_info (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, middle_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, id_card_number VARCHAR(255) NOT NULL, id_card_issued_at DATETIME NOT NULL, id_card_issued_by VARCHAR(255) NOT NULL, personal_identification_number VARCHAR(255) NOT NULL, disadvantaged TINYINT(1) NOT NULL, wants_discretion TINYINT(1) NOT NULL, mobile_telephone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, telephone_for_new_contracts VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE expenses (id INT AUTO_INCREMENT NOT NULL, household_expenses_food_and_clothes NUMERIC(10, 2) NOT NULL, household_expenses_kids NUMERIC(10, 2) NOT NULL, household_expenses_rent NUMERIC(10, 2) NOT NULL, household_expenses_transport NUMERIC(10, 2) NOT NULL, household_expenses_drinks_cigarettes NUMERIC(10, 2) NOT NULL, household_expenses_other_credits VARCHAR(255) NOT NULL, household_expenses_other_credits_instalment_amount NUMERIC(10, 2) NOT NULL, household_expenses_heating NUMERIC(10, 2) NOT NULL, household_expenses_electricity_and_water NUMERIC(10, 2) NOT NULL, household_expenses_internet_and_tv NUMERIC(10, 2) NOT NULL, household_expenses_telephone NUMERIC(10, 2) NOT NULL, household_expenses_other NUMERIC(10, 2) NOT NULL, expenses_total NUMERIC(10, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE uploaded_documents (id INT AUTO_INCREMENT NOT NULL, old_style_passport_first_page_id INT DEFAULT NULL, old_style_passport_second_page_id INT DEFAULT NULL, old_style_passport_photo_at25_id INT DEFAULT NULL, old_style_passport_pasted_photo_at25_id INT DEFAULT NULL, old_style_passport_photo_at45_id INT DEFAULT NULL, old_style_passport_pasted_photo_at45_id INT DEFAULT NULL, old_style_passport_proof_of_residence_id INT DEFAULT NULL, old_style_passport_selfie_with_passport_id INT DEFAULT NULL, new_style_passport_photo_page_id INT DEFAULT NULL, new_style_passport_validity_page_id INT DEFAULT NULL, new_style_passport_registration_appendix_id INT DEFAULT NULL, new_style_passport_selfie_with_id_card_id INT DEFAULT NULL, old_style_pension_certificate_first_spread_id INT DEFAULT NULL, old_style_pension_certificate_validity_record_id INT DEFAULT NULL, new_style_pension_certificate_photo_and_validity_spread_id INT DEFAULT NULL, passport_type VARCHAR(255) NOT NULL, pension_certificate_type VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_252AB77626ACD8CE (old_style_passport_first_page_id), UNIQUE INDEX UNIQ_252AB776748DF2BF (old_style_passport_second_page_id), UNIQUE INDEX UNIQ_252AB776A98CE8F4 (old_style_passport_photo_at25_id), UNIQUE INDEX UNIQ_252AB776710923B3 (old_style_passport_pasted_photo_at25_id), UNIQUE INDEX UNIQ_252AB77626CC1D54 (old_style_passport_photo_at45_id), UNIQUE INDEX UNIQ_252AB776FE49D613 (old_style_passport_pasted_photo_at45_id), UNIQUE INDEX UNIQ_252AB776A72EF0FF (old_style_passport_proof_of_residence_id), UNIQUE INDEX UNIQ_252AB776F84473F0 (old_style_passport_selfie_with_passport_id), UNIQUE INDEX UNIQ_252AB776BC8F1156 (new_style_passport_photo_page_id), UNIQUE INDEX UNIQ_252AB7767F8B96E1 (new_style_passport_validity_page_id), UNIQUE INDEX UNIQ_252AB77674238712 (new_style_passport_registration_appendix_id), UNIQUE INDEX UNIQ_252AB7764AAC1356 (new_style_passport_selfie_with_id_card_id), UNIQUE INDEX UNIQ_252AB7763BC44D36 (old_style_pension_certificate_first_spread_id), UNIQUE INDEX UNIQ_252AB7765C5CBD24 (old_style_pension_certificate_validity_record_id), UNIQUE INDEX UNIQ_252AB77631E34F15 (new_style_pension_certificate_photo_and_validity_spread_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE online_application ADD CONSTRAINT FK_3BE81A9A4862BC9C FOREIGN KEY (credit_details_id) REFERENCES credit_details (id)');
        $this->addSql('ALTER TABLE online_application ADD CONSTRAINT FK_3BE81A9ADEACC8D3 FOREIGN KEY (personal_info_id) REFERENCES personal_info (id)');
        $this->addSql('ALTER TABLE online_application ADD CONSTRAINT FK_3BE81A9A4602205E FOREIGN KEY (uploaded_documents_id) REFERENCES uploaded_documents (id)');
        $this->addSql('ALTER TABLE online_application ADD CONSTRAINT FK_3BE81A9AF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE online_application ADD CONSTRAINT FK_3BE81A9AA079AAF1 FOREIGN KEY (work_details_id) REFERENCES work_details (id)');
        $this->addSql('ALTER TABLE online_application ADD CONSTRAINT FK_3BE81A9A7575A6C0 FOREIGN KEY (incomes_and_expenses_id) REFERENCES expenses (id)');
        $this->addSql('ALTER TABLE online_application ADD CONSTRAINT FK_3BE81A9A549213EC FOREIGN KEY (property_id) REFERENCES property (id)');
        $this->addSql('ALTER TABLE online_application ADD CONSTRAINT FK_3BE81A9A5C01120C FOREIGN KEY (additional_info_id) REFERENCES additional_info (id)');
        $this->addSql('ALTER TABLE online_application ADD CONSTRAINT FK_3BE81A9A4F8A983C FOREIGN KEY (contact_person_id) REFERENCES contact_person (id)');
        $this->addSql('ALTER TABLE online_application ADD CONSTRAINT FK_3BE81A9A6BACE54E FOREIGN KEY (confirmation_id) REFERENCES confirmation (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77626ACD8CE FOREIGN KEY (old_style_passport_first_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776748DF2BF FOREIGN KEY (old_style_passport_second_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776A98CE8F4 FOREIGN KEY (old_style_passport_photo_at25_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776710923B3 FOREIGN KEY (old_style_passport_pasted_photo_at25_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77626CC1D54 FOREIGN KEY (old_style_passport_photo_at45_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776FE49D613 FOREIGN KEY (old_style_passport_pasted_photo_at45_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776A72EF0FF FOREIGN KEY (old_style_passport_proof_of_residence_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776F84473F0 FOREIGN KEY (old_style_passport_selfie_with_passport_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB776BC8F1156 FOREIGN KEY (new_style_passport_photo_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7767F8B96E1 FOREIGN KEY (new_style_passport_validity_page_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77674238712 FOREIGN KEY (new_style_passport_registration_appendix_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7764AAC1356 FOREIGN KEY (new_style_passport_selfie_with_id_card_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7763BC44D36 FOREIGN KEY (old_style_pension_certificate_first_spread_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB7765C5CBD24 FOREIGN KEY (old_style_pension_certificate_validity_record_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE uploaded_documents ADD CONSTRAINT FK_252AB77631E34F15 FOREIGN KEY (new_style_pension_certificate_photo_and_validity_spread_id) REFERENCES file (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE online_application DROP FOREIGN KEY FK_3BE81A9A4F8A983C');
        $this->addSql('ALTER TABLE online_application DROP FOREIGN KEY FK_3BE81A9AF5B7AF75');
        $this->addSql('ALTER TABLE online_application DROP FOREIGN KEY FK_3BE81A9AA079AAF1');
        $this->addSql('ALTER TABLE online_application DROP FOREIGN KEY FK_3BE81A9A4862BC9C');
        $this->addSql('ALTER TABLE online_application DROP FOREIGN KEY FK_3BE81A9A6BACE54E');
        $this->addSql('ALTER TABLE online_application DROP FOREIGN KEY FK_3BE81A9A549213EC');
        $this->addSql('ALTER TABLE online_application DROP FOREIGN KEY FK_3BE81A9A5C01120C');
        $this->addSql('ALTER TABLE online_application DROP FOREIGN KEY FK_3BE81A9ADEACC8D3');
        $this->addSql('ALTER TABLE online_application DROP FOREIGN KEY FK_3BE81A9A7575A6C0');
        $this->addSql('ALTER TABLE online_application DROP FOREIGN KEY FK_3BE81A9A4602205E');
        $this->addSql('DROP TABLE online_application');
        $this->addSql('DROP TABLE contact_person');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE work_details');
        $this->addSql('DROP TABLE credit_details');
        $this->addSql('DROP TABLE confirmation');
        $this->addSql('DROP TABLE property');
        $this->addSql('DROP TABLE additional_info');
        $this->addSql('DROP TABLE personal_info');
        $this->addSql('DROP TABLE expenses');
        $this->addSql('DROP TABLE uploaded_documents');
    }
}
