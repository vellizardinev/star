<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\ModuleItem;
use App\Enum\ModuleItems;
use App\Module\Parameter\Header;
use App\Module\Parameter\Link;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200324131308 extends AbstractMigration implements ContainerAwareInterface
{
	use ContainerAwareTrait;

    public function getDescription() : string
    {
	    return 'Add one more link to the bottom row links inside the Header module.';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
	    $manager = $this->container->get('doctrine.orm.default_entity_manager');

	    /** @var ModuleItem $headerModule */
	    $headerModule = $manager->getRepository(ModuleItem::class)->findOneBy(['name' => ModuleItems::HEADER]);

	    /** @var Header $headerData */
	    $headerData = unserialize($headerModule->getData());
	    $result = [];

	    foreach ($headerData as $locale => $data){
		    $result[$locale] = $data;
		    $result[$locale]->addBottomLink(new Link('', ''));
	    }

	    $headerModule->setData(serialize($result));

	    $manager->flush();

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
