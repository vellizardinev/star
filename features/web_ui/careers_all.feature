Feature:
  In order to see career job openings
  as a web user
  I need to be able to see a list of career job openings and collapse a given job opening

  Background:
    Given I have a city named "Sofia"
    And I have a department named "IT"
    And I have an office named "Central"
    And I have a public job opening with the following attributes:
      | description      | deadline  | reference | slug | position  | department| office | city  |
      | Test description | 2019-12-25| 123       | test | Developer |IT         | Central| Sofia |

  Scenario: Job openings' list
    When I am on "/en/career/all"
    Then I should see "City"
    And the response status code should be 200

  Scenario: Job opening details
    When I am on "/en/career/all"
    And I should see "CITY"
    And I should see "DEADLINE"