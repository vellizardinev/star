Feature:
  In order to learn important news about credits
  as a web user
  I need to be able to see a list of recent news articles and read a given news article

  Background:
    Given I am logged in as an admin

  Scenario: News' list
    When I am on "/en/news"
    Then the response status code should be 200

  Scenario: Read news article
    Given I have a news article with the following attributes:
      | title        | slug         | content |
      | Test article | test-article | 7654321 |
    When I am on "/en/news/test-article"
    Then I should see "Test article"
    And I should see "7654321"