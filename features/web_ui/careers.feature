Feature:
  In order to see careers
  as a web user
  I need to be able to see a list of careers and open a given careers page

  Scenario: Careers' list
    When I am on "/en/career"
    Then the response status code should be 200

  Scenario: Apply for credit consultant
    When I am on "/en/career/become-a-credit-consultant"
    And I fill in "City" with "Demo city"
    And I fill in "Full name" with "Demo name"
    And I fill in "Telephone" with "040444334654"
    And I fill in "Email" with "Demo email"
    And I fill in "Comment" with "Demo comment"
    When I press "Send"
    Then the response status code should be 200

#  Scenario: Read internship programs
#    When I am on "/en/programe-de-internship"
#    Then the response status code should be 200
#
#  Scenario: Read easy credit as an employer
#    When I am on "/en/iCredit-ca-angajator"
#    Then the response status code should be 200
#
#  Scenario: Read motivation program
#    When I am on "/en/programe-de-motivatie"
#    Then the response status code should be 200