Feature:
  In order to see frequently asked questions list
  as a web user
  I need to be able to see a list of frequently asked questions and their answers

  Scenario: FAQs' list
    When I am on "/en/questions"
    Then the response status code should be 200

  Scenario: Submit contact form
    When I am on "/en/questions"
    And I fill in "First name" with "Didko"
    And I fill in "Last name" with "Didkov"
    And I fill in "Email" with "test@email.com"
    And I fill in "Telephone" with "0883339999"
    And I press "Send"
    Then I should be on "/en/questions"
    And the response status code should be 200