<?php

use App\Entity\City;
use App\Entity\Department;
use App\Entity\Office;
use App\Factory\AffiliateFactory;
use App\Factory\CityFactory;
use App\Factory\SiriusDepartmentFactory;
use App\Factory\JobOpeningFactory;
use App\Factory\NewsFactory;
use App\Factory\OfficeFactory;
use App\Factory\PageFactory;
use App\Factory\LandingPageFactory;
use App\Factory\UserDefinedModuleFactory;
use App\Factory\LoyaltyPartnerFactory;
use App\Factory\CareersPartnerFactory;
use App\Factory\BulletinListFactory;
use App\Factory\CreditApplicationFactory;
use App\Factory\GiftFactory;
use App\Factory\PollFactory;
use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Exception\ElementNotFoundException;
use Behat\MinkExtension\Context\RawMinkContext;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
class FeatureContext extends RawMinkContext
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @BeforeScenario
     */
    public function copyDatabase()
    {
        chdir('features/data');
        copy('temp_db.sqlite', 'test_db.sqlite');
        chdir('..');
        chdir('..');
    }

    public function manager(): EntityManagerInterface
    {
        return $this->kernel->getContainer()->get('doctrine.orm.default_entity_manager');
    }

    /**
     * @When /^I click "([^"]*)"$/
     * @param $linkText
     * @throws ElementNotFoundException
     */
    public function iClick($linkText)
    {
        $this->page()->clickLink($linkText);
    }

    /**
     * @Given /^I am logged in as an admin$/
     * @throws ElementNotFoundException
     */
    public function iAmLoggedInAsAnAdmin()
    {
        $this->visitPath('/login');
        $this->page()->fillField('Email', 'admin@siriussoftware.bg');
        $this->page()->fillField('Password', '123456');
        $this->page()->pressButton('Login');
    }

    /**
     * @Given I click :linkText in the :rowText row
     * @param $linkText
     * @param $rowText
     */
    public function iClickInTheRow($linkText, $rowText)
    {
        $row = $this->findRowByText($rowText);
        $link = $row->findLink($linkText);

        if ($link) {
            $link->click();
        } else {
            $button = $row->findButton($linkText);

            assert($button !== null, 'Cannot find link or button in row with text '.$linkText);

            $button->press();
        }
    }

    /**
     * @Given I have a news article with the following attributes:
     * @param TableNode $table
     */
    public function iHaveANewsArticleWithTheFollowingAttributes(TableNode $table)
    {
        $newsFactory = $this->kernel->getContainer()->get(NewsFactory::class);

        foreach ($table as $row) {
            $newsFactory->create([
                'title' => $row['title'],
                'content' => $row['content'],
                'slug' => $row['slug'],
                ]);
        }
    }

    /**
     * @Given I have a page with the following attributes:
     * @param TableNode $table
     */
    public function iHaveAPageWithTheFollowingAttributes(TableNode $table)
    {
        $pageFactory = $this->kernel->getContainer()->get(PageFactory::class);

        foreach ($table as $row) {
            $pageFactory->create([
                'name' => $row['name'],
                'path' => $row['path'],
            ]);
        }
    }

	/**
	 * @Given I have a landing page with the following attributes:
	 * @param TableNode $table
	 */
	public function iHaveALandingPageWithTheFollowingAttributes(TableNode $table)
	{
		$pageFactory = $this->kernel->getContainer()->get(LandingPageFactory::class);

		foreach ($table as $row) {
			$pageFactory->create([
				'name' => $row['name'],
				'path' => $row['path'],
			]);
		}
	}

    /**
     * @Given I have a user-defined module with the following attributes:
     * @param TableNode $table
     */
    public function iHaveAUserDefinedModuleWithTheFollowingAttributes(TableNode $table)
    {
        $factory = $this->kernel->getContainer()->get(UserDefinedModuleFactory::class);

        foreach ($table as $row) {
            $factory->create([
                'name' => $row['name'],
                'content' => $row['content'],
            ]);
        }
    }

    /**
     * @Given I have a city named :name
     * @param $name
     */
    public function iHaveACityNamed($name)
    {
        $this->kernel->getContainer()->get(CityFactory::class)->create(['name' => $name]);
    }

    /**
     * @Given I have a department named :name
     * @param $name
     */
    public function iHaveADepartmentNamed($name)
    {
        $this->kernel->getContainer()->get(SiriusDepartmentFactory::class)->create(['name' => $name]);
    }

	/**
	 * @Given I have a department with the following attributes:
	 * @param TableNode $table
	 */
	public function iHaveADepartmentWithTheFollowingAttributes(TableNode $table)
	{
		$departmentFactory = $this->kernel->getContainer()->get(SiriusDepartmentFactory::class);

		foreach ($table as $row) {
			$departmentFactory->create([
				'name' => $row['name'],
				'director' => $row['director'],
				'description' => $row['description'],
			]);
		}
	}

    /**
     * @Given I have an office named :name
     * @param $name
     */
    public function iHaveAnOfficeNamed($name)
    {
        $this->kernel->getContainer()->get(OfficeFactory::class)->create(['name' => $name]);
    }

    /**
     * @Given I have an office named :name located in :city
     * @param $name
     * @param $city
     */
    public function iHaveAnOfficeNamedLocatedIn($name, $city)
    {
        $this->kernel->getContainer()->get(OfficeFactory::class)->create([
            'name' => $name,
            'city' => $this->manager()->getRepository(City::class)->findOneBy(['name' => $city])
        ]);
    }

    /**
     * @Given I have a job opening with the following attributes:
     * @param TableNode $table
     */
    public function iHaveAJobOpeningWithTheFollowingAttributes(TableNode $table)
    {
        $factory = $this->kernel->getContainer()->get(JobOpeningFactory::class);

        foreach ($table as $row) {
            $factory->create([
                'position' => $row['position'],
                'department' => $this->manager()->getRepository(Department::class)->findOneBy(['name' => $row['department']]),
                'office' => $this->manager()->getRepository(Office::class)->findOneBy(['name' => $row['office']]),
                'city' => $this->manager()->getRepository(City::class)->findOneBy(['name' => $row['city']]),
            ]);
        }
    }

	/**
	 * @Given I have a public job opening with the following attributes:
	 * @param TableNode $table
	 */
	public function iHaveAPublicJobOpeningWithTheFollowingAttributes(TableNode $table)
	{
		$factory = $this->kernel->getContainer()->get(JobOpeningFactory::class);

		foreach ($table as $row) {
			$factory->create([
				'position' => $row['position'],
				'department' => $this->manager()->getRepository(Department::class)->findOneBy(['name' => $row['department']]),
				'office' => $this->manager()->getRepository(Office::class)->findOneBy(['name' => $row['office']]),
				'city' => $this->manager()->getRepository(City::class)->findOneBy(['name' => $row['city']]),
				'description' => $row['description'],
				'slug' => $row['slug'],
				'reference' => $row['reference'],
				'deadline' => DateTime::createFromFormat('Y-m-d', $row['deadline'])
			]);
		}
	}

	/**
	 * @Given I have an affiliate named :name
	 * @param $name
	 */
	public function iHaveAnAffiliateNamed($name)
	{
		$this->kernel->getContainer()->get(AffiliateFactory::class)->create(['name' => $name]);
	}

	/**
	 * @Given I have a loyalty partner with the following attributes:
	 * @param TableNode $table
	 */
	public function iHaveALoyaltyParnerWithTheFollowingAttributes(TableNode $table)
	{
		$partnerFactory = $this->kernel->getContainer()->get(LoyaltyPartnerFactory::class);

		foreach ($table as $row) {
			$partnerFactory->create([
				'name' => $row['name'],
				'description' => $row['description'],
				'discount' => $row['discount'],
			]);
		}
	}

	/**
	 * @Given I have a career partner with the following attributes:
	 * @param TableNode $table
	 */
	public function iHaveACareerParnerWithTheFollowingAttributes(TableNode $table)
	{
		$partnerFactory = $this->kernel->getContainer()->get(CareersPartnerFactory::class);

		foreach ($table as $row) {
			$partnerFactory->create([
				'name' => $row['name'],
				'description' => $row['description'],
				'discount' => $row['discount'],
			]);
		}
	}


	/**
	 * @Given I have a subscriber with email :email
	 * @param $email
	 */
	public function iHaveAnSubscriber(string $email)
	{
		$this->kernel->getContainer()->get(BulletinListFactory::class)->create(['subscriberEmail' => $email]);
	}

	/**
	 * @Given I have a gift named :name
	 * @param $name
	 */
	public function iHaveAGiftNamed($name)
	{
		$this->kernel->getContainer()->get(GiftFactory::class)->create(['name' => $name]);
	}

	/**
	 * @Given I have a poll titled :title
	 * @param $title
	 */
	public function iHaveAPollTitled($title)
	{
		$this->kernel->getContainer()->get(PollFactory::class)->create(['title' => $title]);
	}

	/**
	 * @Given I should see :text in table
	 * @param $text
	 * @throws Exception
	 */
	public function iShouldSeeTextInTable($text)
	{
		$row = $this->page()->find('css', sprintf('table tr:contains("%s")', $text));

		if($row === null)
		{
			throw new Exception("\"$text\" is not visible in table");
		}
	}


	/**
	 * @Given I should not see :text in table
	 * @param $text
	 * @throws Exception
	 */
	public function iShouldNotSeeTextInTable($text)
	{
		$row = $this->page()->find('css', sprintf('table tr:contains("%s")', $text));

		if($row)
		{
			throw new Exception("\"$text\" is visible in table");
		}
	}

	/**
	 * @Given I am logged in as an :email :password
	 * @throws ElementNotFoundException
	 */
	public function iAmLoggedInAsAnUser(string $email, string $password)
	{
		$this->visitPath('/en/logout');
		$this->visitPath('/en/login');
		$this->page()->fillField('Email', $email);
		$this->page()->fillField('Password', $password);
		$this->page()->pressButton('Login');
	}

	/**
	 * @Given I have a credit application with the following attributes:
	 * @param TableNode $table
	 */
	public function iHaveACreditApplicationWithTheFollowingAttributes(TableNode $table)
	{
		$creditApplicationFactory = $this->kernel->getContainer()->get(CreditApplicationFactory::class);

		foreach ($table as $row) {
			$creditApplicationFactory->create([
				'name' => $row['name'],
				'telephone' => $row['telephone'],
				'amount' => $row['amount'],
				'status' => $row['status']
			]);
		}
	}

    protected function page()
    {
        return $this->getSession()->getPage();
    }

    protected function findRowByText($rowText)
    {
        $row = $this->page()->find('css', sprintf('table tr:contains("%s")', $rowText));

        assert($row !== null, 'Cannot find a table row with this text.');

        return $row;
    }

    protected function findByCss($selector)
    {
        return $this->page()->find('css', $selector);
    }
}
