<?php

use Behat\Behat\Context\Context;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Defines application features from the specific context.
 */
class ApiFeatureContext implements Context
{
    use KernelDictionary;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @BeforeScenario
     */
    public function copyDatabase()
    {
        chdir('features/data');
        copy('temp_db.sqlite', 'test_db.sqlite');
        chdir('..');
        chdir('..');
    }

    protected function manager(): EntityManagerInterface
    {
        return $this->getContainer()->get('doctrine.orm.default_entity_manager');
    }
}
