Feature:
  In order to manage job openings
  as an admin user
  I need to be able to see and filter a list of all job_openings and update their data

  Background:
    Given I am logged in as an admin

  Scenario: Job Openings' list
    When I am on "/en/admin/jobs"
    Then I should see "Filter"
    And the response status code should be 200

  Scenario: Filter job openings by position
    Given I am on "/en/admin/jobs"
    And I have a city named "Sofia"
    And I have a department named "IT"
    And I have an office named "Central"
    And I have a job opening with the following attributes:
      | position  | department | office  | city  |
      | Developer | IT         | Central | Sofia |
    When I fill in "Position" with "dev"
    And I press "Filter"
    Then I should see "Developer"

  Scenario: Filter job openings by department
    Given I have a city named "Sofia"
    And I have a department named "IT"
    And I have an office named "Central"
    And I have a job opening with the following attributes:
      | position  | department | office  | city  |
      | Developer | IT         | Central | Sofia |
    When I am on "/en/admin/jobs"
    And I select "IT" from "Department"
    And I press "Filter"
    Then I should see "Developer"

  Scenario: Filter job openings by office
    Given I have a city named "Sofia"
    And I have a department named "IT"
    And I have an office named "Central"
    And I have a job opening with the following attributes:
      | position  | department | office  | city  |
      | Developer | IT         | Central | Sofia |
    When I am on "/en/admin/jobs"
    And I select "Central" from "Office"
    And I press "Filter"
    Then I should see "Developer"

  Scenario: Filter job openings by city
    Given I have a city named "Sofia"
    And I have a department named "IT"
    And I have an office named "Central"
    And I have a job opening with the following attributes:
      | position  | department | office  | city  |
      | Developer | IT         | Central | Sofia |
    When I am on "/en/admin/jobs"
    And I select "Sofia" from "City"
    And I press "Filter"
    Then I should see "Developer"

  Scenario: Create job opening
    Given I have a city named "Sofia"
    And I have a department named "IT"
    And I have an office named "Central"
    And I am on "/en/admin/jobs/create"
    And I fill in "Position" with "Developer"
    And I fill in "Reference" with "123"
    And I fill in "Description" with "description"
    And I fill in "Deadline" with "31/08/2019"
    And I select "Sofia" from "City"
    And I select "IT" from "Department"
    And I select "Central" from "Office"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update job opening's content
    Given I have a city named "Sofia"
    And I have a department named "IT"
    And I have an office named "Central"
    And I have a job opening with the following attributes:
      | position  | department | office  | city  |
      | Developer | IT         | Central | Sofia |
    When I am on "/en/admin/jobs/1/update"
    And I fill in "Position" with "Web Developer"
    When I press "Save content"
    Then the response status code should be 200

  Scenario: Update job opening's seo data
    Given I have a city named "Sofia"
    And I have a department named "IT"
    And I have an office named "Central"
    And I have a job opening with the following attributes:
      | position  | department | office  | city  |
      | Developer | IT         | Central | Sofia |
    When I am on "/en/admin/jobs/1/update"
    And I fill in "Meta title" with "Meta Web Developer"
    When I press "Save SEO"
    Then the response status code should be 200

  Scenario: Update job opening's visibility data
    Given I have a city named "Sofia"
    And I have a department named "IT"
    And I have an office named "Central"
    And I have a job opening with the following attributes:
      | position  | department | office  | city  |
      | Developer | IT         | Central | Sofia |
    When I am on "/en/admin/jobs/1/update"
    And I fill in "Start date" with "23/08/2045"
    When I press "Save SEO"
    Then the response status code should be 200
