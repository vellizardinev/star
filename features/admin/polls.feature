Feature:
  In order to be able to ask for the users opinions on various subjects
  as an admin user
  I need to be able to see and filter a list of polls and be able to create/update a poll

  Background:
    Given I am logged in as an admin

  Scenario: Polls' list
    When I am on "/en/admin/polls"
    Then I should see "Filter"
    And the response status code should be 200

  Scenario: Create poll
    Given I am on "/en/admin/polls/create"
    And I fill in "Title" with "demo title"
    And I fill in "Description" with "demo description"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update poll
    Given I have a poll titled "demo poll"
    And I am on "/en/admin/polls/1/update"
    And I fill in "Title" with "demo title"
    And I fill in "Description" with "demo description"
    When I press "Save"
    Then the response status code should be 200