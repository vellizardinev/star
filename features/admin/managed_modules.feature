Feature: 
  In order to see managed modules as an admin user
  I need to be able to see a list of managed modules
  
  Background:
    Given I am logged in as an admin

  Scenario: Managed modules' list
    When I am on "/en/admin/managed-modules"
    Then the response status code should be 200