Feature: 
  In order to manage files
  as an admin user
  I need to be able to see and filter a list of all uploaded files
  
  Background:
    Given I am logged in as an admin

  Scenario: Files' list
    When I am on "/en/admin/files"
    Then I should see "Filter"
    And the response status code should be 200

  Scenario: Filter non-existing name
    Given I am on "/en/admin/files"
    When I fill in "Name" with "abcde"
    And I press "Filter"
    Then I should see "No files found"

  Scenario: Filter files by name
    Given I am on "/en/admin/files"
    When I fill in "Name" with "iTimes8"
    And I press "Filter"
    Then I should see "iTimes8.pdf"
    And I should not see "iTimes9.pdf"