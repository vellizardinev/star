Feature:
  In order to see dashboard
  as an admin user
  I need to be able to see application statistics

  Background:
    Given I am logged in as an admin

  Scenario: Statistics' list
    When I am on "/en/admin/dashboard"
    Then I should see "Statistics"
    And the response status code should be 200

  Scenario: General' list
    When I am on "/en/admin/dashboard"
    Then I should see "General"
    And the response status code should be 200

  Scenario: Products and services' list
    When I am on "/en/admin/dashboard"
    Then I should see "Products and services"
    And the response status code should be 200