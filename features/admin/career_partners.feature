Feature:
  In order to create and manage career partners and discounts
  as an admin user
  I need to be able to see and filter a list of career partners and be able to create/update a partner

  Background:
    Given I am logged in as an admin

  Scenario: Career partners' list
    When I am on "/en/admin/career-partners-and-discounts"
#    Then I should see "Filter"
#    Then I should see "Clear"
#    Then I should see "Name"
#    Then I should see "Description"
#    Then I should see "Discount"
#    And the response status code should be 200
    Then the response status code should be 404

  Scenario: Create career partner
    Given I am on "/en/admin/career-partners-and-discounts/create"
#    And I fill in "Name" with "demo partner"
#    And I fill in "Discount" with "-30%"
#    And I fill in "Description" with "very big description"
#    When I press "Save"
#    Then the response status code should be 200
    Then the response status code should be 404

  Scenario: Update career partner
    Given I have a career partner with the following attributes:
      | name           | description        | discount        |
      | demo partner 1 | demo description 1 | demo discount 1 |
      | demo partner 2 | demo description 2 | demo discount 2 |
    And I am on "/en/admin/career-partners-and-discounts/1/update"
#    And I fill in "Name" with "demo partner"
#    And I fill in "Discount" with "-30%"
#    And I fill in "Description" with "very big description"
#    When I press "Save"
#    Then the response status code should be 200
    Then the response status code should be 404

  Scenario: Filter career partner by name
    Given I have a career partner with the following attributes:
      | name           | description        | discount        |
      | demo partner 1 | demo description 1 | demo discount 1 |
      | demo partner 2 | demo description 2 | demo discount 2 |
    And I am on "/en/admin/career-partners-and-discounts"
#    And  I fill in "Name" with "demo partner 1"
#    And I press "Filter"
#    Then I should see "demo partner 1"
#    And I should not see "demo partner 2"
#    And the response status code should be 200
    Then the response status code should be 404

  Scenario: Filter career partner by description
    Given I have a career partner with the following attributes:
      | name           | description        | discount        |
      | demo partner 1 | demo description 1 | demo discount 1 |
      | demo partner 2 | demo description 2 | demo discount 2 |
    And I am on "/en/admin/career-partners-and-discounts"
#    And  I fill in "Description" with "demo description 1"
#    And I press "Filter"
#    Then I should see "demo description 1"
#    And I should not see "demo description 2"
#    And the response status code should be 200
    Then the response status code should be 404

  Scenario: Filter career partner by discount
    Given I have a career partner with the following attributes:
      | name           | description        | discount        |
      | demo partner 1 | demo description 1 | demo discount 1 |
      | demo partner 2 | demo description 2 | demo discount 2 |
    And I am on "/en/admin/career-partners-and-discounts"
#    And  I fill in "Discount" with "demo discount 1"
#    And I press "Filter"
#    Then I should see "demo discount 1"
#    And I should not see "demo discount 2"
#    And the response status code should be 200
    Then the response status code should be 404