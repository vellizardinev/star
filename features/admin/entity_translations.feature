Feature: 
  In order to manage entity translations
  as an admin user
  I need to be able to see and filter a list of translation items
  
  Background:
    Given I am logged in as an admin

  Scenario: Entity translations' list
    When I am on "/en/admin/entity-translations"
    Then I should see "Filter"
    And the response status code should be 200