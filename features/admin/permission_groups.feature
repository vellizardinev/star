Feature: 
  In order to manage access to the various areas of the application
  as an admin user
  I need to be able to see and filter a list of permission groups, create and update them
  
  Background:
    Given I am logged in as an admin

  Scenario: Permission Groups' list
    When I am on "/en/admin/permission-groups"
    Then I should see "Filter"
    And the response status code should be 200

  Scenario: Filter groups by name
    When I am on "/en/admin/permission-groups"
    And I fill in "Name" with "perm"
    And I press "Filter"
    Then I should see "permission_admin"

  Scenario: Filter groups by name
    When I am on "/en/admin/permission-groups"
    And I fill in "Name" with "abcdefgs"
    And I press "Filter"
    Then I should see "No permission groups found"
    When I click "Clear"
    Then I should see "permission_admin"

  Scenario:
    Given I am on "/en/admin/permission-groups/1/update"
    When I check "permissions.delete"
    And I press "Save"
    Then the response status code should be 200
    And I should be on "/en/admin/permission-groups"

  Scenario: Create group with same name as existing one
    Given I am on "/en/admin/permission-groups/create"
    And I fill in "Name" with "permission_admin"
    When I press "Save"
    Then I should be on "/en/admin/permission-groups/create"
    And I should see "This value is already used"
