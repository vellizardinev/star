Feature: 
  In order to create and manage gifts
  as an admin user
  I need to be able to see and filter a list of gifts and be able to create/update a gift
  
  Background:
    Given I am logged in as an admin

  Scenario: Gifts' list
    When I am on "/en/admin/gifts"
    Then I should see "Filter"
    Then I should see "Clear"
    Then I should see "Name"
    Then I should see "Code"
    And the response status code should be 200
    
  Scenario: Create gift
    Given I am on "/en/admin/gifts/create"
    And I fill in "Name" with "test gift"
    And I fill in "Code" with "test code"
    And I fill in "Price" with "500"
    And I fill in "New" with "1"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update gift
    Given I have a gift named "New gift"
    And I am on "/en/admin/gifts/1/update"
    And I fill in "Name" with "test gift"
    And I fill in "Code" with "test code"
    And I fill in "Price" with "500"
    And I fill in "New" with "1"
    When I press "Save"
    Then the response status code should be 200