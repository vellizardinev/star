Feature: 
  In order to create and manage promotions
  as an admin user
  I need to be able to see and filter a list of promotions and be able to create/update a promotion
  
  Background:
    Given I am logged in as an admin

  Scenario: Promotions' list
    When I am on "/en/admin/promotions"
    Then I should see "Filter"
    And the response status code should be 200
    
  Scenario: Create promotion
    Given I am on "/en/admin/promotions/create"
    And I fill in "Title" with "demo promotion"
    And I fill in "Description" with "nice promo"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update promotion
    Given I am on "/en/admin/promotions/create"
    And I fill in "Title" with "demo promotion"
    And I fill in "Description" with "nice promo"
    When I press "Save"
    When I am on "/en/admin/promotions/1/update"
    Then the response status code should be 200