Feature: 
  In order to manage application settings
  as an admin user
  I need to be able to see and filter a list of all settings
  
  Background:
    Given I am logged in as an admin

  Scenario: Settings' list
    When I am on "/en/admin/settings"
    Then I should see "Filter"
    And I should see "Date / Time"
    And I should see "Date / Time: Date / Time format"
    And the response status code should be 200

  Scenario: Filter settings by name
    When I am on "/en/admin/settings"
    And I fill in "Name" with "Date"
    And I press "Filter"
    Then I should see "Date / Time"
    But I should not see "Pagination"

  Scenario: Filter settings by name
    When I am on "/en/admin/settings"
    And I fill in "Name" with "abcdefgs"
    And I press "Filter"
    Then I should see "No settings found"
    When I click "Clear"
    Then I should see "Date / Time"
    And I should see "Date / Time: Date / Time format"