Feature:
  In order to create and manage custom content on the site
  as an admin user
  I need to be able to see and filter a list of user-defined pages and be able to create/update a page

  Background:
    Given I am logged in as an admin

  Scenario: Pages' list
    When I am on "/en/admin/pages"
    Then I should see "Filter"
    And I should see "Clear"
    And the response status code should be 200

  Scenario: Filter page by name
    Given I have a page with the following attributes:
      | name   | path   |
      | page 1 | page-1 |
      | page 2 | page-2 |
    When I am on "/en/admin/pages"
    And  I fill in "Name" with "page 1"
    And I press "Filter"
    Then the response status code should be 200
    And I should see "page 1"
    And I should not see "page 2"

  Scenario: Filter non-existing name
    Given I have a page with the following attributes:
      | name   | path   |
      | page 1 | page-1 |
      | page 2 | page-2 |
#    And I have a landing page with the following attributes:
#      | name   | path   |
#      | landing page 2 | landing-page-2 |
    When I am on "/en/admin/pages"
    And  I fill in "Name" with "abcde"
    And I press "Filter"
    Then the response status code should be 200
    And I should see "No pages found"

  Scenario: Filter page by page type
    Given I have a page with the following attributes:
      | name   | path   |
      | page 1 | page-1 |
    And I have a landing page with the following attributes:
      | name   | path   |
      | landing page 2 | landing-page-2 |
    When I am on "/en/admin/pages"
    And I select "Landing page" from "type"
    And I press "Filter"
    Then the response status code should be 200
    And I should see "landing page 2"
    And I should not see "page 1"

  Scenario: Filter page by landing page type
    Given I have a page with the following attributes:
      | name   | path   |
      | page 1 | page-1 |
    And I have a landing page with the following attributes:
      | name   | path   |
      | landing page 2 | landing-page-2 |
    When I am on "/en/admin/pages"
    And I select "Page" from "type"
    And I press "Filter"
    Then the response status code should be 200
    And I should not see "landing page 2"
    And I should see "Page"

  Scenario: Click on page path
    Given I am on "/en/admin/pages"
    And I click "contact-thank-you"
    Then I should be on "/en/contact-thank-you"
    And the response status code should be 200

  Scenario: Create custom page
    Given I am on "/en/admin/pages/create"
    And I fill in "Name" with "test page"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Create custom landing page
    Given I am on "/en/admin/pages/create"
    And I fill in "Name" with "test landing page"
    And I select "Landing page" from "Page type"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update custom page
    Given I am on "/en/admin/pages/create"
    And I fill in "Name" with "this is my first test page"
    And I press "Save"
    When I am on "/en/admin/pages/1/update"
    Then the response status code should be 200

  Scenario: Visit custom page
    Given I have a page with the following attributes:
      | name   | path   |
      | page 1 | page-1 |
    When I am on "/en/page-1"
    Then the response status code should be 200

  Scenario: Add module to the page
    Given I have a user-defined module with the following attributes:
      | name     | content             |
      | module 1 | module contents 777 |
    And I have a page with the following attributes:
      | name   | path   |
      | page 1 | page-1 |
    When I am on "/en/admin/pages"
    And I click "Manage content" in the "Contact form submitted" row
    And I click "Add module"
    And I select "module 1" from "Select module"
    And I press "Add module"
    Then the response status code should be 200
    And I should see "module 1"
    When I am on "/en/contact-thank-you"
    Then I should see "module contents 777"
