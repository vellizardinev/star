Feature: 
  In order to create and manage the partners' offices
  as an admin user
  I need to be able to see and filter a list of officess and be able to create/update an office
  
  Background:
    Given I am logged in as an admin

  Scenario: Offices' list
    When I am on "/en/admin/offices"
    Then I should see "Filter"
    And the response status code should be 200

  Scenario: Filter office by name
    Given I have an office named "Test office 777"
    When I am on "/en/admin/offices"
    And I fill in "Name" with "tes"
    When I press "Filter"
    Then I should see "Test office 777"

  Scenario: Filter office by city
    Given I have a city named "Sofia"
    And I have an office named "Test office 777" located in "Sofia"
    When I am on "/en/admin/offices"
    And I select "Sofia" from "City"
    When I press "Filter"
    Then I should see "Test office 777"

  Scenario: Create office
    Given I am on "/en/admin/offices/create"
    And I fill in "Name" with "Test office 777"
    And I fill in "Address" with "test address"
    And I fill in "Telephone" with "0035929712233"
    And I fill in "Latitude" with "12"
    And I fill in "Longitude" with "12"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update office
    Given I have a city named "Sofia"
    And I have an office named "Test office 777" located in "Sofia"
    When I am on "/en/admin/offices/11/update"
    And I fill in "Name" with "Test office 999"
    When I press "Save"
    Then the response status code should be 200