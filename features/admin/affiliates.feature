Feature: 
  In order to create and manage affiliates
  as an admin user
  I need to be able to see and filter a list of affiliates and be able to create/update a affiliate
  
  Background:
    Given I am logged in as an admin

  Scenario: Affiliates' list
    When I am on "/en/admin/affiliates"
    Then I should see "Filter"
    And I should see "Clear"
    And I should see "Name"
    And the response status code should be 200

  Scenario: Filter affiliate by name
    Given I have an affiliate named "Test affiliate 777"
    When I am on "/en/admin/affiliates"
    And I fill in "Name" with "tes"
    When I press "Filter"
    Then I should see "Test affiliate 777"

  Scenario: Filter non-existing name
    Given I have an affiliate named "Test affiliate 777"
    When I am on "/en/admin/affiliates"
    When I fill in "Name" with "abcde"
    And I press "Filter"
    Then I should see "No affiliates found"

  Scenario: Create affiliate
    Given I am on "/en/admin/affiliates/create"
    And I fill in "Name" with "test affiliate name"
    And I fill in "Contact info" with "test contact info"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update affiliate
    Given I have an affiliate named "Test affiliate name"
    And I am on "/en/admin/affiliates/1/update"
    And I fill in "Name" with "update test of affiliate name"
    And I fill in "Contact info" with "update test of contact info"
    When I press "Save"
    Then the response status code should be 200