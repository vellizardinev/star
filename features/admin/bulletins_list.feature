Feature: 
  In order to see list of bulletins
  as an admin user
  I need to be able to see and filter a list of bulletins
  
  Background:
    Given I am logged in as an admin

  Scenario: Bulletins' list
    When I am on "/en/admin/bulletins"
    Then I should see "Filter"
    And I should see "Clear"
    And I should see "Email"
    And I should see "Export to csv"
    And the response status code should be 200

  Scenario: Filter bulletins by email
    Given I have a subscriber with email "test1@email.com"
    And I have a subscriber with email "test2@email.com"
    When I am on "/en/admin/bulletins"
    And I fill in "Email" with "test1@email.com"
    And I press "Filter"
    Then I should see "test1@email.com"
    And I should not see "test2@email.com"
    And the response status code should be 200

  Scenario: Filter non-existing email
    Given I have a subscriber with email "test1@email.com"
    When I am on "/en/admin/bulletins"
    When I fill in "Email" with "abcde"
    And I press "Filter"
    And I should see "No bulletins found"