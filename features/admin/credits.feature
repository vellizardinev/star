Feature: 
  In order to create and manage credits
  as an admin user
  I need to be able to see and filter a list of credits and be able to create/update a credit
  
  Background:
    Given I am logged in as an admin

  Scenario: Credits' list
    When I am on "/en/admin/credits"
    Then I should see "Filter"
    And the response status code should be 200
    
  Scenario: Create credit
    Given I am on "/en/admin/credits/create"
    And I select "Personal" from "Type"
    And I fill in "Title" with "demo news article"
    And I fill in "Short description" with "good credit"
    And I fill in "Full description" with "good credit"
    And I fill in "Requirements" with "only ID card is needed"
    And I select "Weekly" from "Payment schedule"
    And I fill in "Max amount" with "200"
    And I fill in "Parameters description" with "parameters"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update credit
    Given I am on "/en/admin/credits/create"
    And I fill in "Title" with "demo news article"
    And I fill in "Short description" with "good credit"
    And I fill in "Full description" with "good credit"
    And I fill in "Max amount" with "200"
    And I fill in "Parameters description" with "parameters"
    When I press "Save"
    When I am on "/en/admin/credits/1/update"
    Then the response status code should be 200

  Scenario: Filter non-existing title
    Given I am on "/en/admin/credits"
    When I fill in "Title" with "abcde"
    And I press "Filter"
    Then I should see "No credits found"

  Scenario: Filter credits by title
    Given I am on "/en/admin/credits"
    When I fill in "Title" with "iMonth"
    And I press "Filter"
    Then I should see "iMonth" in table
    And I should not see "iCredit" in table