Feature: 
  In order to manage access to the various areas of the application
  as an admin user
  I need to be able to see and filter a list of permissions, create and update them
  
  Background:
    Given I am logged in as an admin

  Scenario: Permissions' list
    When I am on "/en/admin/permissions"
    Then I should see "Filter"
    And the response status code should be 200

  Scenario: Filter permissions by domain
    When I am on "/en/admin/permissions"
    And I fill in "Domain" with "perm"
    And I press "Filter"
    Then I should see 5 "tr" elements

  Scenario: Filter permissions by action
    When I am on "/en/admin/permissions"
    And I fill in "Action" with "edit"
    And I press "Filter"
    Then I should see 2 "tr" element
    When I click "Clear"
    Then I should see 5 "tr" elements

  Scenario: Filter permissions by action
    When I am on "/en/admin/permissions"
    And I fill in "Action" with "abcdefg"
    And I press "Filter"
    Then I should see "No permissions found"

  Scenario: Edit a permission
    Given I am on "/en/admin/permissions/1/update"
    And I fill in "Domain" with "updated_domain"
    And I fill in "Action" with "updated_action"
    When I press "Save"
    Then the response status code should be 200
    And I should see "updated_domain"
    And I should see "updated_action"

  Scenario: Create permission with same domain and action as existing one
    Given I am on "/en/admin/permissions/create"
    And I fill in "Domain" with "permissions"
    And I fill in "Action" with "edit"
    When I press "Save"
    Then I should be on "/en/admin/permissions/create"
    And I should see "This value is already used"