Feature:
  In order to create and manage departments
  as an admin user
  I need to be able to see and filter a list of departments and be able to create/update a department

  Background:
    Given I am logged in as an admin

  Scenario: Departments' list
    When I am on "/en/admin/departments"
    Then I should see "Filter"
    And the response status code should be 200

  Scenario: Create department
    Given I am on "/en/admin/departments/create"

    And I fill in "Name" with "demo department"
    And I fill in "Director" with "demo director"
    And I fill in "Description" with "very big description"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update department
    Given I have a department named "Demo department"
    And I am on "/en/admin/departments/1/update"
    And I fill in "Name" with "demo department"
    And I fill in "Director" with "demo director"
    And I fill in "Description" with "very big description"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Filter non-existing name
    Given I am on "/en/admin/departments"
    When I fill in "Name" with "abcde"
    And I press "Filter"
    Then I should see "No departments found"

  Scenario: Filter departments by name
    Given I have a department with the following attributes:
      | name            | director      |  description    |
      | demo department | demo director | description     |
      | new test depart | new test dir  | very long text  |
    And I am on "/en/admin/departments"
    When I fill in "Name" with "demo department"
    And I press "Filter"
    Then print last response
    And I should see "demo department" in table
    And I should not see "new test depart" in table

  Scenario: Filter non-existing director
    Given I have a department with the following attributes:
      | name            | director      |  description    |
      | demo department | demo director | description     |
      | new test depart | new test dir  | very long text  |
    And I am on "/en/admin/departments"
    When I fill in "Director" with "abcde"
    And I press "Filter"
    Then I should see "No departments found"

  Scenario: Filter departments by director
    Given I have a department with the following attributes:
      | name            | director      |  description    |
      | demo department | demo director | description     |
      | new test depart | new test dir  | very long text  |
    And I am on "/en/admin/departments"
    When I fill in "Director" with "demo director"
    And I press "Filter"
    Then I should see "demo director" in table
    And I should not see "new test dir" in table

  Scenario: Filter non-existing description
    Given I have a department with the following attributes:
      | name            | director      |  description    |
      | demo department | demo director | description     |
      | new test depart | new test dir  | very long text  |
    And I am on "/en/admin/departments"
    When I fill in "Description" with "abcde"
    And I press "Filter"
    Then I should see "No departments found"

  Scenario: Filter departments by description
    Given I have a department with the following attributes:
      | name            | director      |  description    |
      | demo department | demo director | description     |
      | new test depart | new test dir  | very long text  |
    And I am on "/en/admin/departments"
    When I fill in "Description" with "description"
    And I press "Filter"
    Then I should see "description" in table
    And I should not see "very long text" in table