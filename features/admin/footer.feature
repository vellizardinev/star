Feature: 
  In order to create and manage the contents of the footer
  as an admin user
  I need to be able to modify the text and URLs of footer links, and reorder them
  
  Background:
    Given I am logged in as an admin

  Scenario: Configure header
    When I am on "/en/admin/managed-modules/footer/configure"
    Then I should see "Configure module"
    And I should see "Footer"
    And the response status code should be 200