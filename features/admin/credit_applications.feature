Feature: 
  In order to see credit applications
  as an admin user
  I need to be able to see and filter a list of credit applications and be able to view details of a credit application
  
  Background:
    Given I am logged in as an admin

  Scenario: Credit applications' list
    When I am on "/en/admin/credit-applications"
    Then I should see "Filter"
    Then I should see "Clear"
    Then I should see "Name"
    Then I should see "Telephone"
    And the response status code should be 200

  Scenario: Filter credit applications by name
    Given I have a credit application with the following attributes:
      | name          | telephone | amount| status    |
      | Didko Didkov  | 0881234444| 5000  | approved  |
      | Joro Jorov    | 0881231111| 3000  | unapproved|
    When I am on "/en/admin/credit-applications"
    And I fill in "Name" with "Didko Didkov"
    And I press "Filter"
    Then I should see "Didko Didkov"
    And I should not see "Joro Jorov"
    And the response status code should be 200

  Scenario: Filter non-existing name
    Given I have a credit application with the following attributes:
      | name          | telephone | amount| status    |
      | Didko Didkov  | 0881234444| 5000  | approved  |
    When I am on "/en/admin/credit-applications"
    And I fill in "Name" with "Didko Jorov"
    And I press "Filter"
    Then I should see "No credit applications found"
    And the response status code should be 200

  Scenario: Filter credit applications by telephone
    Given I have a credit application with the following attributes:
      | name          | telephone | amount| status    |
      | Didko Didkov  | 0881234444| 5000  | approved  |
      | Joro Jorov    | 0881231111| 3000  | unapproved|
    When I am on "/en/admin/credit-applications"
    And I fill in "Telephone" with "0881234444"
    And I press "Filter"
    Then I should see "Didko Didkov"
    And I should not see "Joro Jorov"
    And the response status code should be 200

  Scenario: Filter non-existing telephone
    Given I have a credit application with the following attributes:
      | name          | telephone | amount| status    |
      | Didko Didkov  | 0881234444| 5000  | approved  |
    When I am on "/en/admin/credit-applications"
    And I fill in "Telephone" with "0893412345"
    And I press "Filter"
    Then I should see "No credit applications found"
    And the response status code should be 200