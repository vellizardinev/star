Feature: 
  In order to create and manage news articles
  as an admin user
  I need to be able to see and filter a list of news and be able to create/update a news article
  
  Background:
    Given I am logged in as an admin

  Scenario: News' list
    When I am on "/en/admin/news"
    Then I should see "Filter"
    Then I should see "Title"
    Then I should see "Clear"
    And the response status code should be 200
    
  Scenario: Create news article
    Given I am on "/en/admin/news/create"
    And I fill in "Title" with "demo news article"
    And I fill in "Content" with "very big news"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update news article
    Given I have a news article with the following attributes:
      | title           | content       |  slug         |
      | news article 1  | demo content  | demo-article  |
    And I am on "/en/admin/news/1/update"
    And I fill in "Title" with "demo news article"
    And I fill in "Content" with "very big news"
    And I fill in "Slug" with "demo-slug"
    When I press "Save"
    When I am on "/en/admin/news/1/update"
    Then the response status code should be 200

  Scenario: Preview news article
    Given I have a news article with the following attributes:
      | title           | content       |  slug         |
      | news article 1  | demo content  | demo-article  |
    And I am on "/en/admin/news"
    And I click "Preview" in the "news article 1" row
    Then I should be on "/en/news/demo-article"
    And the response status code should be 200

  Scenario: Click on news article' title
    Given I have a news article with the following attributes:
      | title           | content       |  slug         |
      | news article 1  | demo content  | demo-article  |
    And I am on "/en/admin/news"
    And I click "news article 1"
    Then I should be on "/en/news/demo-article"
    And the response status code should be 200

  Scenario: Filter news article by title
    Given I have a news article with the following attributes:
      | title           | content         |  slug           |
      | news article 1  | demo content 1  | demo-article-1  |
      | news article 2  | demo content 2  | demo-article-2  |
    And I am on "/en/admin/news"
    And I fill in "Title" with "news article 1"
    And I press "Filter"
    Then I should be on "/en/admin/news?title=news+article+1"
    And the response status code should be 200
    And I should see "news article 1"
    And I should not see "news article 2"

  Scenario: Filter non-existing title
    Given I have a news article with the following attributes:
      | title           | content         |  slug           |
      | news article 1  | demo content 1  | demo-article-1  |
      | news article 2  | demo content 2  | demo-article-2  |
    And I am on "/en/admin/news"
    And I fill in "Title" with "abcde"
    And I press "Filter"
    Then the response status code should be 200
    And I should see "No news found"