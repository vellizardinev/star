Feature: 
  In order to manage emails
  as an admin user
  I need to be able to see and filter a list of all emails
  
  Background:
    Given I am logged in as an admin

  Scenario: Emails' list
    When I am on "/en/admin/mails"
    Then I should see "Filter"
    And the response status code should be 200

  Scenario: Update email
    Given I am on "/en/admin/mails/1/update"
    And I fill in "Subject" with "demo subject"
    And I fill in "Content" with "demo content"
    When I press "Save"
    Then the response status code should be 200
    And I should be on "/en/admin/mails"
    
  Scenario: Filter emails by type
    When I am on "/en/admin/mails"
    And I fill in "Type" with "contact"
    When I press "Filter"
    Then I should see "Contact"
    And I should not see "Job application"
    And I should not see "Poll entry"
    And I should not see "User registration"

  Scenario: Filter non-existing type
    When I am on "/en/admin/mails"
    And I fill in "Type" with "non-existing"
    When I press "Filter"
    Then I should see "No mails found"

  Scenario: Filter emails by subject
    When I am on "/en/admin/mails"
    And I fill in "Subject" with "Credit application"
    When I press "Filter"
    Then I should see "Credit application"
    And I should not see "Contact"
    And I should not see "Job application"
    And I should not see "Poll entry"
    And I should not see "User registration"

  Scenario: Filter non-existing type
    When I am on "/en/admin/mails"
    And I fill in "Subject" with "non-existing"
    When I press "Filter"
    Then I should see "No mails found"

  Scenario: Filter emails by locale
    When I am on "/en/admin/mails"
    And I select "EN" from "locale"
    When I press "Filter"
    Then I should see "en" in table
    And I should not see "ro" in table