Feature: 
  In order to create and manage frequently asked questions
  as an admin user
  I need to be able to see and filter a list of FAQs and be able to create/update a FAQ
  
  Background:
    Given I am logged in as an admin

  Scenario: FAQs' list
    When I am on "/en/admin/faqs"
    Then I should see "Filter"
    And the response status code should be 200

  Scenario: Create faq
    Given I am on "/en/admin/faqs/create"
    And I fill in "Question" with "test question"
    And I fill in "Answer" with "test answer"
    When I press "Save"
    Then the response status code should be 200

  Scenario: Update faq
    Given I am on "/en/admin/faqs/1/update"
    And I fill in "Question" with "test question"
    And I fill in "Answer" with "test answer"
    When I press "Save"
    When I am on "/en/admin/faqs/1/update"
    Then the response status code should be 200

  Scenario: Filter non-existing question
    Given I am on "/en/admin/faqs"
    When I fill in "Question" with "abcde"
    And I press "Filter"
    Then I should see "No faqs found"

  Scenario: Filter faqs by question
    Given I am on "/en/admin/faqs"
    When I fill in "Question" with "Що таке iCredit?"
    And I press "Filter"
    Then I should see "Що таке iCredit?" in table
    And I should not see "Чому мене відвідують вдома для збору платежів по кредиту?" in table

  Scenario: Filter non-existing answer
    Given I am on "/en/admin/faqs"
    When I fill in "Answer" with "abcde"
    And I press "Filter"
    Then I should see "No faqs found"

  Scenario: Filter faqs by answer
    Given I am on "/en/admin/faqs"
    When I fill in "Answer" with "iCredit - це швидкі кредити готівкою в розмірі від 2 000 до 30 000 грн"
    And I press "Filter"
    Then I should see "Що таке iCredit?" in table
    And I should not see "Чому мене відвідують вдома для збору платежів по кредиту?" in table
