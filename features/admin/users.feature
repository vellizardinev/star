Feature: 
  In order to manage users
  as an admin user
  I need to be able to see and filter a list of all registered users, update their roles and profile and
  be able to activate/deactivate a given user
  
  Background:
    Given I am logged in as an admin

  Scenario: Users' list
    When I am on "/en/admin/users"
    Then I should see "Filter"
    And the response status code should be 200

  Scenario: Filter users by email
    Given I am on "/en/admin/users"
    When I fill in "Email" with "admin"
    And I press "Filter"
    Then I should see "admin@"

  Scenario: Filter non-existing email
    Given I am on "/en/admin/users"
    When I fill in "Email" with "abcde"
    And I press "Filter"
    And I should see "No users found"

  Scenario: Filter users by role
    Given I am on "/en/admin/users"
    Then I select "Administrator" from "Role"
    And I press "Filter"
    Then I should see "admin@"
    But I should not see "test@"

  Scenario: Manage roles form
    Given I am on "/en/admin/users/2/manage-roles"
    Then I check "Supporter"
    Then I press "Save"
    Then I select "Supporter" from "Role"
    When I press "Filter"
    Then I should see "test@siriussoftware.bg"

  Scenario: Update profile
    When I am on "/en/admin/users/profile/1"
    And I fill in "First name" with "Gogo"
    And I fill in "Last name" with "Gogov"
    And I press "Save"
    Then the response status code should be 200
    And I should see "Gogo Gogov"

  Scenario: Deactivate user
    When I am on "/en/admin/users"
    And I click "Deactivate" in the "test@siriussoftware.bg" row
    And select "Inactive" from "Status"
    When I press "Filter"
    Then I should see "test@"

  Scenario: Create user
    When I am on "/en/admin/users/create"
    And I fill in "Email" with "test_create@siriussoftware.bg"
    And I fill in "Password" with "12345678"
    And I fill in "Repeat password" with "12345678"
    And I press "Save"
    Then the response status code should be 200
    And I should see "n/a"
    And I should see "test_create@siriussoftware.bg"

  Scenario: Create user with same email as existing one
    When I am on "/en/admin/users/create"
    And I fill in "Email" with "test@siriussoftware.bg"
    And I fill in "Password" with "12345678"
    And I fill in "Repeat password" with "12345678"
    And I press "Save"
    Then I should be on "/en/admin/users/create"
    And I should see "There is already an account with this email"

  Scenario: Create user with profile data
    When I am on "/en/admin/users/create"
    And I fill in "First name" with "Didko"
    And I fill in "Last name" with "Didkov"
    And I fill in "Email" with "test_create@siriussoftware.bg"
    And I fill in "Password" with "12345678"
    And I fill in "Repeat password" with "12345678"
    And I press "Save"
    Then the response status code should be 200
    And I should see "Didko Didkov"
    And I should see "test_create@siriussoftware.bg"

  Scenario: Change user password
    When I am on "/en/admin/users"
    And I click "Change password" in the "test@siriussoftware.bg" row
    Then I should be on "/en/admin/users/change-password/2"
    And I should see "Password"
    And I should see "Repeat password"
    When I fill in "Password" with "qwerty"
    And I fill in "Repeat password" with "qwerty"
    And I press "Save"
    Then I am logged in as an "test@siriussoftware.bg" "qwerty"
    And the response status code should be 200
    And I should be on "/en/"

  Scenario: Failed change of user password
    When I am on "/en/admin/users"
    And I click "Change password" in the "test@siriussoftware.bg" row
    Then I should be on "/en/admin/users/change-password/2"
    And I should see "Password"
    And I should see "Repeat password"
    When I fill in "Password" with "qwerty"
    And I fill in "Repeat password" with "abcdef"
    And I press "Save"
    Then I am logged in as an "test@siriussoftware.bg" "qwerty"
    And the response status code should be 200
    And I should be on "/en/login"