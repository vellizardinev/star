Feature: 
  In order to create and manage custom content on the site
  as an admin user
  I need to be able to see and filter a list of modules and be able to create/update a module
  
  Background:
    Given I am logged in as an admin

  Scenario: Modules' list
    When I am on "/en/admin/modules"
    Then I should see "Filter"
    And the response status code should be 200
    
  Scenario: Create user defined module
    Given I am on "/en/admin/modules/create"
    And I fill in "Name" with "123456"
    And I fill in "Content" with "123456"
    When I press "Save"
    Then I should see "123456"

  Scenario: Update module
    Given I am on "/en/admin/modules/create"
    And I fill in "Name" with "123456"
    And I fill in "Content" with "a test content"
    And I press "Save"
    When I click "Edit" in the "123456" row
    Then the response status code should be 200