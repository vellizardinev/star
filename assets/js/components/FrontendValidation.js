import Vue from 'vue';

Vue.component('frontend-validation', {
    delimiters: ['[[', ']]'],
    template: `
      <div class="form-group">
          <label for="amount" class="required">
            <i class="fa fa-asterisk fa-required" v-if="required"></i>
            <span v-text="label"></span>
            <span class="invalid-feedback d-block" v-for="error in errors" v-if="hasErrors">
                <span class="d-block">
                    <span class="form-error-icon badge badge-danger text-uppercase">
                        ERROR
                    </span>
                    <span class="form-error-message" v-text="error"></span>
                </span>
            </span>
          </label>
          <slot></slot>
      </div>
    `,
    props: {
        property: {
            required: true,
            type: String
        },
        label: {
            required: true,
            type: String
        },
        required: {
            required: false,
            type: Boolean,
            default: true
        },
        entity: {
            required: true,
            type: String
        }
    },
    data() {
        return {
            errors: []
        }
    },
    mounted() {
        this.$refs['input'] = (this.$el.querySelector('input'));

        this.$refs.input.addEventListener('input', this.validate);
    },
    methods: {
        validate() {
            let url = `/frontend-validation/validate`,
                data = {property: this.property, entity: this.entity, value: this.$refs.input.value},
                _self = this;

            $.ajax({
                method: 'POST',
                data: data,
                url: url,
                success() {
                    _self.errors = [];
                    _self.$refs.input.classList.remove('is-invalid');
                },
                error(response) {
                    _self.errors = response.responseJSON.errors;

                    if (_self.errors.length > 0) {
                        _self.$refs.input.classList.add('is-invalid');
                    }
                }
            });
        }
    },
    computed: {
        hasErrors() {
            return this.errors.length > 0;
        }
    }
});