import Vue from 'vue';

Vue.component('zopim-chat', {
    template: '#zopim-chat',
    methods: {
        enableZopim() {
            $zopim.livechat.window.toggle();
        },
    }
});