import Vue from 'vue';

Vue.component('redirect-timer', {
    delimiters: ['[[', ']]'],
    template: '#redirect-timer',
    props: {
        seconds: {
            required: false,
            type: Number,
            default: 30
        },
        redirectUrl: {
            required: true,
            type: String
        }
    },
    data() {
        return {
            elapsedTime: 0,
            timer: null
        }
    },
    mounted() {
        this.startTimer();
    },
    methods: {
        startTimer() {
            this.timer = setInterval(this.tick, 1000)
        },

        tick() {
            this.elapsedTime++;
        }
    },
    computed: {
        remainingTime() {
            return this.seconds - this.elapsedTime;
        }
    },
    watch: {
        remainingTime(value) {
            if (value === 0) {
                clearInterval(this.timer);
                window.location.href = this.redirectUrl;
            }
        }
    }
});