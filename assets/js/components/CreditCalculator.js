import Vue from 'vue';
import Slider from 'ion-rangeslider';
import 'ion-rangeslider/css/ion.rangeSlider.css';

Vue.component('credit-calculator', {
    delimiters: ['[[', ']]'],
    template: '#credit-calculator',
    props: {
        standalone: {
            required: false,
            type: Boolean,
            default: false
        },
        currency: {
            required: false,
            type: String,
            default: 'грн.'
        },
        locale: {
            required: false,
            type: String,
            default: 'ua'
        },
        credit: {
            required: false,
            type: String,
            default: null
        },
        creditAmount: {
            required: false,
            type: String,
            default: null
        },
        creditPayment: {
            required: false,
            type: String,
            default: null
        }
    },
    data() {
        return {
            isRefreshing: false,
            amountSteps: [],
            paymentSteps: [],
            type: 'personal',
            amount: 2000.00,
            payment: 120.00,
            paymentWithPenalty: 120.00,
            periods: 36,
            total: 4320.00,
            totalWithPenalty: 4320.00,
            interestRateFirstPeriod: 3.0599,
            interestRateSecondPeriod: 0.0128,
            aprc:11.3817,
            paymentSchedule: 'weekly',
            pensioner: 'no',
            creditTitle: '',
            creditSlug: '',
            requirements: '',
            default: null,
            creditProductId: '',
            creditPeriodId: '',
            expenses: 2082
        }
    },
    mounted() {
        this.getData(this.defaultParams(), true);

        EventManager.listen('amount-changed', this.amountChanged);
        EventManager.listen('payment-changed', this.paymentChanged);
    },
    methods: {
        defaultParams() {
            if (this.credit) {
                return {
                    credit: this.credit,
                    amount: this.creditAmount,
                    payment: this.creditPayment
                }
            }

            return {
                pensioner: 'no',
                paymentSchedule: 'weekly',
                type: 'personal',
                amount: 2000.00
            }
        },

        getData(params = this.defaultParams(), creditHasChanged = false) {
            let url = '/en/credit-data?' + $.param(params),
                _self = this;

            this.isRefreshing = true;

            $.ajax({
                url: url,
                method: 'GET',
                success: function (response) {
                    _self.amountSteps = response.amountSteps.map(value => parseFloat(value));
                    _self.paymentSteps = response.paymentSteps.map(value => parseFloat(value));

                    _self.paymentSchedule = response.creditData.credit.paymentSchedule;
                    _self.pensioner = response.creditData.credit.pensioner === true ? 'yes' : 'no';
                    _self.type = response.creditData.credit.type;
                    _self.amount = response.creditData.amount;
                    _self.payment = response.creditData.payment;
                    _self.paymentWithPenalty = response.creditData.paymentWithPenalty;
                    _self.periods = response.creditData.periods;
                    _self.total = response.creditData.total;
                    _self.totalWithPenalty = response.creditData.totalWithPenalty;
                    _self.interestRateFirstPeriod = response.creditData.interestRateFirstPeriod;
                    _self.interestRateSecondPeriod = response.creditData.interestRateSecondPeriod;
                    _self.aprc = response.creditData.aprc;
                    _self.creditTitle = response.creditData.credit.title;
                    _self.creditSlug = response.creditData.credit.slug;
                    _self.requirements = response.creditData.credit.requirements;
                    _self.default = response.creditData.credit.defaultAmount;
                    _self.creditProductId = response.creditData.productId;
                    _self.creditPeriodId = response.creditData.creditPeriodId;
                    _self.expenses = response.creditData.totalWithPenalty - response.creditData.amount;

                    _self.isRefreshing = false;

                    if (creditHasChanged) {
                        _self.initAmountSlider();
                    }

                    _self.initPaymentSlider();

                    EventManager.fire('credit-parameters-changed', {
                        product: _self.creditTitle,
                        slug: _self.creditSlug,
                        amount: _self.amount,
                        period: _self.periods,
                        payment: _self.payment,
                        creditProductId: _self.creditProductId,
                        creditPeriodId: _self.creditPeriodId,
                        requirements: _self.requirements
                    });
                },
                error: function (response) {
                    console.log(response);
                }
            });
        },

        amountChanged(value) {
            this.amount = value.toFixed(2);

            let parameters = {
                amount: this.amount,
                pensioner: this.pensioner,
                paymentSchedule: this.paymentSchedule,
                type: this.type
            };

            this.getData(parameters);
        },

        paymentChanged(value) {
            this.paymentWithPenalty = value.toFixed(2);

            let parameters = {
                payment: this.paymentWithPenalty,
                amount: this.amount,
                pensioner: this.pensioner,
                paymentSchedule: this.paymentSchedule,
                type: this.type
            };

            this.getData(parameters);
        },

        paymentScheduleChanged() {
            if (this.paymentSchedule === 'monthly' && this.type === 'personal') {
                this.pensioner = 'no';
            }

            let parameters = {
                paymentSchedule: this.paymentSchedule,
                type: this.type,
                pensioner: this.pensioner,
                amount: this.defaultAmountForPaymentSchedule
            };

            this.getData(parameters, true);
        },

        pensionerChanged() {
            let parameters = {};

            if (this.pensioner === 'yes') {
                this.paymentSchedule = 'monthly';

                parameters = {
                    paymentSchedule: this.paymentSchedule,
                    type: this.type,
                    pensioner: this.pensioner,
                    amount: this.defaultAmountForPaymentSchedule
                };
            } else {
                this.paymentSchedule = 'weekly';

                parameters = {
                    paymentSchedule: this.paymentSchedule,
                    type: this.type,
                    pensioner: this.pensioner,
                    amount: this.defaultAmountForPaymentSchedule
                };
            }

            this.getData(parameters, true);
        },

        changeType(newType) {
            if (this.type === newType) {
                return;
            }

            this.type = newType;

            if (newType === 'business') {
                this.pensioner = 'no';
            }

            this.paymentSchedule = 'weekly';

            let parameters = {
                paymentSchedule: this.paymentSchedule,
                type: this.type,
                pensioner: this.pensioner
            };

            if (newType === 'business') {
                parameters.amount = 1600.00;
                parameters.payment = 92.00;
            }

            this.getData(parameters, true);
        },

        initAmountSlider() {
            let amountSlider = $(this.$refs.amount).data("ionRangeSlider"),
                index = this.amountSteps.findIndex(value => value.toFixed(2) === this.amount);

            if (amountSlider) {
                amountSlider.destroy();
            }

            $(this.$refs.amount).ionRangeSlider({
                skin: 'flat',
                grid: true,
                grid_snap: true,
                values: this.amountSteps,
                from: index,
                postfix: ' ' + this.currency,
                force_edges: false,
                onFinish: function (data) {
                    EventManager.fire('amount-changed', data.from_value);
                }
            });

            amountSlider = $(this.$refs.amount).data("ionRangeSlider");
            amountSlider.update({from: index});
        },

        initPaymentSlider() {
            let paymentSlider = $(this.$refs.payment).data("ionRangeSlider"),
                adjustedPaymentSteps = this.paymentSteps;

            if (paymentSlider) {
                paymentSlider.destroy();
            }

            if (adjustedPaymentSteps.length === 1) {
                adjustedPaymentSteps.push(adjustedPaymentSteps[0]);
            }

            let index = this.paymentSteps.findIndex(value => value.toFixed(2) === this.paymentWithPenalty);

            $(this.$refs.payment).ionRangeSlider({
                skin: 'flat',
                grid: true,
                grid_snap: true,
                values: adjustedPaymentSteps,
                from: index,
                postfix: ' ' + this.currency,
                force_edges: false,
                onFinish: function (data) {
                    EventManager.fire('payment-changed', data.from_value);
                }
            });

            paymentSlider = $(this.$refs.payment).data("ionRangeSlider");
            paymentSlider.update({from: index});
        },

        createOffer() {
            EventManager.fire('create-credit-offer', {
                type: this.type,
                amount: this.amount,
                payment: this.paymentWithPenalty,
                paymentWithPenalty: this.paymentWithPenalty,
                periods: this.periods,
                duration: this.duration,
                total: this.totalWithPenalty,
                totalWithPenalty: this.totalWithPenalty,
                paymentSchedule: this.paymentSchedule,
                interestRateFirstPeriod: this.interestFirstPeriod,
                interestRateSecondPeriod: this.interestSecondPeriod,
                aprc: this.interestAPRC,
                expenses: this.expenses,
                pensioner: this.pensioner,
                title: this.creditTitle,
                slug: this.creditSlug,
                requirements: this.requirements,
            });
        },

        apply() {
            let url = `/get-application-route/${this.creditSlug}/${this.locale}`,
                _self = this;

            $.ajax({
                url: url,
                method: 'GET',
                success: function (response) {
                    window.location.href = `${response}/?amount=${_self.amount}&payment=${_self.payment}&period=${_self.periods}`;
                },
                error: function (response) {
                    console.log(response);
                }
            });
        }
    },
    computed: {
        duration() {
            if (this.paymentSchedule === 'weekly') {
                return Math.ceil(this.periods / 4.33);
            } else if (this.paymentSchedule === 'biweekly') {
                return Math.ceil(this.periods / 2);
            } else {
                return this.periods;
            }
        },

        interestFirstPeriod() {
            return (this.interestRateFirstPeriod * 100).toFixed(2) + '%';
        },

        interestSecondPeriod() {
            return (this.interestRateSecondPeriod * 100).toFixed(2) + '%';
        },

        interestAPRC() {
            return (this.aprc * 100).toFixed(2) + '%';
        },

        defaultAmount() {
            return this.default === null ? this.amount : this.default;
        },
        defaultAmountForPaymentSchedule() {
            if (this.paymentSchedule === 'weekly') {
                return 2000.00;
            }

            if (this.paymentSchedule === 'biweekly') {
                return 2000.00;
            }

            if (this.paymentSchedule === 'monthly') {
                if (this.pensioner === 'no') {
                    return 2000.00;
                } else {
                    return 1000.00;
                }
            }
        },
        containerClass() {
            return {
                'col-md-6': this.standalone === false,
                'col-xl-4': this.standalone === false
            }
        },
        bottomClass() {
            return {
                'standalone': this.standalone === true
            }
        }
    },
    watch: {
        type: function () {
            EventManager.fire('credit-type-changed');
        }
    }
});