import Vue from 'vue';

Vue.component('apply-online-button', {
    name: "ApplyOnlineButton",
    delimiters: ['[[', ']]'],
    template: '#apply-online-button',
    props: {
        checkCustomerIsAllowedUrl: {
            type: String,
            required: true
        },
        redirectUrl: {
            required: true,
            type: String
        }
    },

    data() {
        return {
            checking: false
        }
    },

    computed: {
        isChecking() {
            return this.checking === true;
        }
    },

    methods: {
        checkCustomerIsDenied() {
            let _self = this;

            this.checking = true;

            $.ajax({
                method: 'get',
                url: this.checkCustomerIsAllowedUrl,
                success(response) {
                    _self.checking = false;

                    if (response.isDenied) {
                        $('#' + _self.$refs.modal.$el.getAttribute('id')).modal('show');
                    } else {
                        window.location.replace(_self.redirectUrl);
                    }
                },
                error(error) {
                    _self.checking = false;
                    console.log(error);
                }
            });
        }
    }
});