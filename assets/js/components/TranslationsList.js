import Vue from 'vue';

Vue.component('translations-list', {
    delimiters: ['[[', ']]'],
    template: '#translations-list',
    props: {
        authToken: {
            type: String,
            required: true
        },
        baseUrl: {
            type: String,
            required: false,
            default: ''
        }
    },
    data() {
        return {
            translationItems: [],
            locales: [],
            domains: [],
            selectedLocale: '',
            selectedDomain: '',
            selectedKey: '',
            selectedValue: '',
            defaultLocale: null,
            isLoading: true,
            timeout: null
        }
    },
    async mounted() {
        await this.getLocales();
        await this.getDomains();
        await this.getTranslations();
    },
    methods: {
        debouncedGetTranslations() {
            if (this.timeout) clearTimeout(this.timeout);

            this.timeout = setTimeout(() => {
                this.getTranslations();
            }, 300);
        },

        async getLocales() {
            let url = `${this.baseUrl}/locales`,
                _self = this;

            await $.ajax({
                url: url,
                method: 'GET',
                headers: {
                    'X-Auth-Token': this.authToken
                },
                success: function (response) {
                    _self.locales = response.payload.locales;
                    _self.selectedLocale = _self.locales[0];
                },
                error: function (response) {
                    console.log(response);
                }
            });
        },

        async getDomains() {
            let url = `${this.baseUrl}/domains`,
                _self = this;

            await $.ajax({
                url: url,
                method: 'GET',
                headers: {
                    'X-Auth-Token': this.authToken
                },
                success: function (response) {
                    _self.domains = response.payload.domains;
                    _self.selectedDomain = _self.domains[0];
                },
                error: function (response) {
                    console.log(response);
                }
            });
        },

        async getTranslations() {
            let url = `${this.baseUrl}/list?locale=${this.selectedLocale}&domain=${this.selectedDomain}&key=${this.selectedKey}&value=${this.selectedValue}`,
                _self = this;

            this.isLoading = true;

            $.ajax({
                url: url,
                method: 'GET',
                headers: {
                    'X-Auth-Token': this.authToken
                },
                success: function (response) {
                    _self.translationItems = response.payload.translations;
                    _self.defaultLocale = response.payload.defaultLocale;
                    _self.isLoading = false;
                },
                error: function (response) {
                    console.log(response);
                    _self.isLoading = false;
                }
            });
        },

        clear() {
            this.selectedLocale = this.locales[0];
            this.selectedDomain = this.domains[0];
            this.selectedKey = '';
            this.selectedValue = '';
            this.getTranslations();
        }
    }
});
