import Vue from 'vue';

Vue.component('credit-repayment', {
    delimiters: ['[[', ']]'],
    template: '#credit-repayment',
    props: {
        contract: {
            required: true,
            type: Number
        }
    },
    data() {
        return {
            repaymentSum: null
        }
    },
    methods: {
        getRepaymentSum() {
            let url = `/customer-profile/${this.contract}/calculate-repayment`,
                _self = this;

            $.ajax({
                method: 'GET',
                url: url,
                success(response) {
                    _self.repaymentSum = response.repaymentSum;
                },
                error(error) {
                    flash(error['responseJSON'], 'error');
                }
            });
        },
        redirectForPayment() {
            let url = `/customer-profile/repayment-url`,
                data = {contract: this.contract, amount: this.repaymentSum, isEarlyRepayment: true};

            $.ajax({
                method: 'POST',
                data: data,
                url: url,
                success(response) {
                    window.location.href = response.repaymentUrl;
                },
                error(error) {
                    flash(error['responseJSON'], 'error');
                }
            });
        }
    },
    computed: {
        formattedRepaymentSum() {
            if (this.repaymentSum === null) {
                return '';
            }

            if (this.repaymentSum > 0) {
                return `${this.repaymentSum.toFixed(2)} UAH`;
            }

            return '';
        },
        state() {
            if (this.repaymentSum === null) {
                return 'initial';
            }

            if (this.repaymentSum > 0) {
                return 'pay';
            }

            if (this.repaymentSum === 0) {
                return 'repayment_not_possible';
            }

            if (this.repaymentSum === -1) {
                return 'error';
            }

            return 'initial';
        }
    }
});