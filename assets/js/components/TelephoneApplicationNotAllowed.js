import Vue from 'vue';

Vue.component('telephone-application-not-allowed', {
    name: "TelephoneApplicationNotAllowed",
    delimiters: ['[[', ']]'],
    template: '#telephone-application-not-allowed',
    props: {
        show: {
            type: Boolean,
            required: false,
            default: false
        }
    },
    mounted() {
        if (this.show === true) {
            $('#' + this.$refs.modal.$el.getAttribute('id')).modal('show');
        }
    }
});