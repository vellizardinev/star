import Vue from 'vue';

Vue.component('translation-item', {
    delimiters: ['[[', ']]'],
    template: '#translation-item',
    props: {
        id: {
            type: String,
            required: true
        },
        locale: {
            type: String,
            required: true
        },
        domain: {
            type: String,
            required: true,
        },
        translationKey: {
            type: String,
            required: true
        },
        translationValue: {
            type: String,
            required: false,
            default: null
        },
        defaultLocale: {
            type: String,
            required: false,
            default: null
        },
        defaultTranslationValue: {
            type: String,
            required: false,
            default: null
        },
        baseUrl: {
            type: String,
            required: false,
            default: ''
        },
        authToken: {
            type: String,
            required: true
        }
    },
    data() {
        return {
            translationItems: [],
            state: 'display',
            value: '',
            oldValue: ''
        }
    },
    mounted() {
        this.value = this.oldValue = this.translationValue;
    },
    methods: {
        save() {
            let _self = this,
                url = `${this.baseUrl}/${this.id}/update`;

            _self.state = 'display';
            $.ajax({
                method: 'POST',
                url: url,
                headers: {
                    'X-Auth-Token': this.authToken
                },
                data: {value: this.value},
                success() {
                    _self.oldValue = _self.value;
                    _self.state = 'display';
                    flash('Translation text was updated.', 'success');
                },
                error(error) {
                    _self.state = 'display';
                    _self.value = _self.translationValue;
                    flash(error['responseJSON'].detail, 'error');
                }
            });
        },
        cancel() {
            this.value = this.oldValue;
            this.state = 'display';
        }
    },

    computed: {
        defaultTranslationText() {
            return `${this.defaultLocale}: ${this.defaultTranslationValue}`;
        }
    },

    watch: {
        state: function(newState) {
            if (newState === 'editing') {
                this.$nextTick(() => {
                    this.$refs.editField.focus();
                    this.$refs.editField.select();
                });
            }
        }
    }
});
