import Vue from 'vue';

Vue.component('credit-repayment-amount-form', {
    delimiters: ['[[', ']]'],
    template: '#credit-repayment-amount-form',
    props: {
        initialAmount: {
            required: false,
            type: Number,
            default: 0
        },
        contract: {
            required: true,
            type: String
        }
    },
    data() {
        return {
            amount: 0.00,
            errors: []
        }
    },
    mounted() {
        this.amount = this.initialAmount.toFixed(2);
    },
    methods: {
        submit() {
            let url = `/customer-profile/repayment-url`,
                data = {contract: this.contract, amount: this.amount, isEarlyRepayment: false},
                _self = this;

            $.ajax({
                method: 'POST',
                data: data,
                url: url,
                success(response) {
                    window.location.href = response.repaymentUrl;
                },
                error(response) {
                    _self.errors = response.responseJSON.errors;
                }
            });
        }
    },
    computed: {
        hasError() {
            return this.errors.length > 0;
        }
    }
});