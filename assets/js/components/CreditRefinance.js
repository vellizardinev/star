import Vue from 'vue';

Vue.component('credit-refinance', {
    delimiters: ['[[', ']]'],
    template: '#credit-refinance',
    props: {
        contract: {
            required: true,
            type: Number
        }
    },
    data() {
        return {
            refinanceSum: null
        }
    },
    methods: {
        getRefinanceSum() {
            let url = `/customer-profile/${this.contract}/calculate-refinance`,
                _self = this;

            $.ajax({
                method: 'GET',
                url: url,
                success(response) {
                    _self.refinanceSum = response.refinanceSum;
                },
                error(error) {
                    flash(error['responseJSON'], 'error');
                }
            });
        }
    },
    computed: {
        formattedRefinanceSum() {
            if (this.refinanceSum === null) {
                return '';
            }

            if (this.refinanceSum > 0) {
                return `${this.refinanceSum.toFixed(2)} UAH`;
            }

            return '';
        },
        state() {
            if (this.refinanceSum === null) {
                return 'initial';
            }

            if (this.refinanceSum > 0) {
                return 'refinance';
            }

            if (this.refinanceSum === 0) {
                return 'refinance_not_possible';
            }

            if (this.refinanceSum === -1) {
                return 'error';
            }

            return 'initial';
        }
    }
});