import Vue from 'vue';

Vue.component('inline-boolean-edit', {
    template: `
<div class="form-check">
    <input class="form-check-input" type="checkbox" @change="update" v-model="value">
</div>
`,
    props: {
        url: {
            required: true,
            type: String
        },
        method: {
            required: false,
            type: String,
            default: 'POST'
        },
        settingValue: {
            required: true,
            type: String
        }
    },
    data() {
        return {
            state: 'display',
            value: ''
        }
    },
    mounted() {
        this.value = this.settingValue === '1';
    },
    methods: {
        update() {
            let _self = this;

            $.ajax({
                method: this.method,
                url: this.url,
                data: {value: this.value},
                success() {
                    flash('Updated.', 'success');
                },
                error(error) {
                    _self.value = _self.settingValue === '1';
                    flash(error['responseJSON'].detail, 'error');
                }
            });
        }
    }
});