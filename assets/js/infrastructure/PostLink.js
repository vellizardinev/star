import Vue from 'vue';

Vue.component('post-link', {
    template: `<a href="" :class="linkClass" @click.prevent="postAction"><i :class="iconClass"></i> <slot></slot></a>`,
    props: {
        targetUrl: {
            required: true,
            type: String
        },
        redirectUrl: {
            required: true,
            type: String
        },
        needsConfirmation: {
            required: false,
            type: Boolean,
            default: false
        },
        confirmationMessage: {
            required: false,
            type: String,
            default: 'Please, confirm the action.'
        },
        iconClass: {
            required: false,
            type: String,
            default: ''
        },
        linkClass: {
            required: false,
            type: String,
            default: 'dropdown-item'
        }
    },
    methods: {
        postAction() {
            let _self = this;

            if (this.needsConfirmation) {
                if (confirm(this.confirmationMessage)) {
                    $.ajax({
                        method: 'POST',
                        url: this.targetUrl,
                        success() {
                            window.location.replace(_self.redirectUrl);
                        },
                        error(error) {
                            if (error['responseJSON']['error']) {
                                flash(error['responseJSON']['error'], 'error');
                            }
                        }
                    });
                }
            } else {
                $.ajax({
                    method: 'POST',
                    url: this.targetUrl,
                    success() {
                        window.location.replace(_self.redirectUrl);
                    },
                    error(error) {
                        if (error['responseJSON']['error']) {
                            flash(error['responseJSON']['error'], 'error');
                        }
                    }
                });
            }
        }
    }
});