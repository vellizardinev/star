import Vue from 'vue';

Vue.component('flash', {
    template: '#flash',
    props: {
        'message': {
            required: true,
            type: String
        },
        'level': {
            required: false,
            type: String,
            default: 'success'
        }
    },
    data() {
        return {
            messageClass: 'success',
            show: false,
            hasBeenShown: false
        }
    },
    created() {
        this.messageClass = this.getMessageClass(this.level);

        this.flash();
    },
    methods: {
        flash() {
            if (this.hasBeenShown) {
                return;
            }

            this.messageClass = this.getMessageClass(this.level);
            this.show = true;
            this.hide();
        },

        hide() {
            this.hasBeenShown = true;

            setTimeout(() => {
                this.show = false;
            }, 5000);
        },

        getMessageClass(level) {
            if (level === 'error') {
                return 'danger';
            } else {
                return 'success';
            }
        }
    }
});