import Vue from 'vue';

Vue.component('tab', {
    template: '#tab',
    props: {
        name: { required: true },
        selected: { default: false },
        parent: {
            required: false,
            type: String,
            default: null
        }
    },

    data() {
        return {
            isActive: false,
            isChildActive: false
        };
    },

    computed: {
        href() {
            return '#' + this.name.toLowerCase().replace(/ /g, '-');
        }
    },

    created() {
        EventManager.listen('childTabActive', this.activate);
    },

    mounted() {
        let currentUrl = window.location.href,
            parts = currentUrl.split('#'),
            fragment = null;

        if (parts.length > 1) {
            fragment = decodeURI(parts[parts.length - 1]).toLowerCase().replace(/ /g, '-');

            if (!this.isChildActive) {
                this.isActive = ('#' + fragment) === this.href;
            }

            if (this.isActive === true) {
                EventManager.fire('childTabActive', this.parent);
            }
        } else {
            this.isActive = this.selected;
        }
    },

    methods: {
        activate(parent) {
            if (this.name === parent) {
                this.isChildActive = true;
                this.isActive = true;
            }
        }
    }
});