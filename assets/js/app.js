import '../css/app.scss';

import $ from 'jquery';

import './bootstrap';
import './custom';

import 'bootstrap';

import 'bootstrap-datepicker';
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.css'

import Vue from 'vue';
import './infrastructure/Flash';
import './infrastructure/Flashes';
import './infrastructure/Tab';
import './infrastructure/Tabs';
import './infrastructure/Modal';
import './infrastructure/Carousel';
import './infrastructure/DeleteLink';
import './infrastructure/DeleteModuleItem';
import './infrastructure/PostLink';
import './infrastructure/InlineValueEdit';
import './infrastructure/InlineBooleanEdit';
import './infrastructure/FileUploadButton';
import './infrastructure/FilesList';
import './infrastructure/Slider';
import './components/EntityTranslation';
import './components/EntityTranslationsForm';
import './components/CreditCalculator';
import './components/CreditOffer';
import './components/CreditComparision';
import './components/PollAnswer';
import './components/PollQuestion';
import './components/Poll';
import './components/Gmaps';
import './components/SliderModule';
import './components/PollParticipation';
import './components/ImageGallery';
import './components/CookieDeclaration';
import './components/ZopimChat';
import './components/CreditRepayment';
import './components/CreditRefinance';
import './components/CreditRepaymentAmountForm';
import './components/FrontendValidation';
import './components/ApplyOnlineButton';
import './components/TelephoneApplicationNotAllowed';
import './components/RedirectTimer';
import './components/TranslationItem';
import './components/TranslationsList';

import {Application} from "stimulus"
import {definitionsFromContext} from "stimulus/webpack-helpers"

window.EventManager = new class {
    constructor() {
        this.vue = new Vue();
    }

    fire(event, data = null) {
        this.vue.$emit(event, data);
    }

    listen(event, callback) {
        this.vue.$on(event, callback);
    }
};

window.flash = function (message, level = 'success') {
    window.EventManager.fire('flash', {message, level});
};

const application = Application.start();
const context = require.context("./controllers", true, /\.js$/);
application.load(definitionsFromContext(context));

Vue.options.delimiters = ['[[', ']]'];
Vue.config.ignoredElements = ['x-trans'];

const app = new Vue({
    el: '#app',
    data: {
        showModal: false,
        dynamicComponents: []
    },
    mounted() {
        EventManager.listen('add-component', this.addComponent);
        EventManager.listen('remove-component', this.removeComponent);
    },
    methods: {
        addComponent(data) {
            this.dynamicComponents.push(data);
        },

        removeComponent(componentId) {
            let index = this.dynamicComponents.findIndex(item => item.id === componentId);

            this.dynamicComponents.splice(index, 1);
        },
    }
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

let elements = document.querySelectorAll('input, select, textarea');

elements.forEach(element => {
    element.addEventListener('invalid', function () {
        element.scrollIntoView(false);
    });
});

$(function() {
    $("form").submit(function(event) {
        $(this).find('button[type="submit"]').prop("disabled", true);
    });
});