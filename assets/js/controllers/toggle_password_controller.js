import { Controller } from "stimulus"

export default class extends Controller {
    static targets = [ "passwordElement", "buttonElement" ];

    toggle() {
        if (this.passwordElementTarget.getAttribute('type') === 'password') {
            this.passwordElementTarget.setAttribute('type', 'text');
        } else {
            this.passwordElementTarget.setAttribute('type', 'password');
        }

        this.buttonElementTarget.classList.toggle('fa-eye');
        this.buttonElementTarget.classList.toggle('fa-eye-slash');
    }
}
