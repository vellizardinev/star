import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ['element'];

    scroll() {
        if (this.element.dataset.mobileOnly) {
            let isMobile = window.matchMedia("(max-width: 576px)").matches;

            if (isMobile) {
                this.elementTarget.scrollIntoView({behavior: "smooth", block: "center"});
            }
        } else{
            this.elementTarget.scrollIntoView({behavior: "smooth", block: "center"});
        }
    }
}
