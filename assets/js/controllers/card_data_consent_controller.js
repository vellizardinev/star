import {Controller} from "stimulus";

export default class extends Controller {
    connect(){
        EventManager.listen('master-field-changed', data => {
            if (data.propertyName === this.propertyName) {
                if (data.value === '3e7115f4-eb2e-42b2-8df2-e5c5ed34fad3') { // Bank account
                    this.element.classList.remove('d-none');
                    this.element.querySelector('input').setAttribute('required', 'required');
                } else {
                    this.element.classList.add('d-none');
                    this.element.querySelector('input').removeAttribute('required');
                }
            }
        })
    }

    get propertyName() {
        return this.element.dataset.propertyName;
    }
}