import {Controller} from "stimulus";

export default class extends Controller {
    connect() {
        EventManager.listen('toggle-repayment-plan', data => {
            if (this.contract === data.contract) {
                if (data.isVisible) {
                    this.element.classList.remove('d-none');
                } else {
                    this.element.classList.add('d-none');
                }
            }
        });
    }

    get contract() {
        return this.element.dataset.contract;
    }
}