import {Controller} from "stimulus"

export default class extends Controller {
    toggleBackgroundOpacity() {
        this.element.classList.toggle('bg-white');
    }
}