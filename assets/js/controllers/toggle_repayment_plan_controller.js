import {Controller} from "stimulus";

export default class extends Controller {
    static targets = [ "arrow" ];

    toggle(event) {
        if (event.target.nodeName === 'BUTTON') {
            return;
        }

        this.isVisible = !this.isVisible;

        EventManager.fire('toggle-repayment-plan', {
            contract: this.contract,
            isVisible: this.isVisible
        });
    }

    get isVisible() {
        return this.element.dataset.isVisible === 'true';
    }

    set isVisible(value) {
        this.element.setAttribute('data-is-visible', value);

        if (value === true) {
            this.arrowTarget.classList.remove('fa-angle-right');
            this.arrowTarget.classList.add('fa-angle-down');
        } else {
            this.arrowTarget.classList.remove('fa-angle-down');
            this.arrowTarget.classList.add('fa-angle-right');
        }
    }

    get contract() {
        return this.element.dataset.contract;
    }
}