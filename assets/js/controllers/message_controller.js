import {Controller} from "stimulus"

export default class extends Controller {
    static targets = ['arrow', 'content', 'title'];

    toggleContent() {
        this.arrowTarget.classList.toggle('fa-angle-right');
        this.arrowTarget.classList.toggle('fa-angle-down');

        this.contentTarget.classList.toggle('not-visible');

        if (this.isUnRead) {
            let  _self = this;

            this.titleTarget.classList.remove('text-danger');

            $.ajax({
                method: 'POST',
                url: this.url,
                success() {
                    EventManager.fire('customer-message-was-read');
                },
                error(error) {
                    console.log(error['responseJSON']);
                    _self.titleTarget.classList.add('text-danger');
                }
            });
        }
    }

    get isUnRead() {
        return this.titleTarget.classList.contains('text-danger');
    }

    get url() {
        return this.element.dataset.url;
    }
}
