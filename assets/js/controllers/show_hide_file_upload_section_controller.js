import {Controller} from "stimulus";

export default class extends Controller {
    connect() {
        EventManager.listen('toggle-file-upload-section', data => {
            if (this.creditId === data.creditId) {
                if (data.isVisible) {
                    this.element.classList.remove('d-none');
                } else {
                    this.element.classList.add('d-none');
                }
            }
        });
    }

    get creditId() {
        return this.element.dataset.creditId;
    }
}