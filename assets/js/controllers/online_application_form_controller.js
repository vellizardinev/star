import {Controller} from "stimulus";

export default class extends Controller {
    connect() {
        EventManager.listen('credit-parameters-changed', data => {
            if (window.location.href.indexOf('application=') != -1) {
                return;
            }

            let [url,] = this.initialActon.split('?'),
                [domain, route, credit, creditDetails, isRefinance] = url.split('/');

            let queryParametersObject = {amount: data.amount, period: data.period, payment: data.payment},
                queryString = $.param(queryParametersObject),
                newUrl = `/${route}/${data.slug}/${creditDetails}/${isRefinance}?${queryString}`;

            this.element.setAttribute('action', newUrl);
        })
    }

    get initialActon() {
        return this.element.getAttribute('action');
    }
}