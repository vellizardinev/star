import {Controller} from "stimulus"

export default class extends Controller {
    connect() {
        EventManager.listen('customer-message-was-read', () => {
            this.element.innerHTML = this.count - 1;

            if (this.count === 0) {
                this.element.classList.add('d-none');
            }
        });
    }

    get count() {
        return parseInt(this.element.innerHTML);
    }
}
