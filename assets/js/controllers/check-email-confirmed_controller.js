import { Controller } from 'stimulus';

export default class extends Controller
{
    connect() {
        fetch(this.url, {method: 'POST'});
    }

    get url() {
        return this.element.dataset.url;
    }
}