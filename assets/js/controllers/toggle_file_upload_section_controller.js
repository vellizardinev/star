import {Controller} from "stimulus";

export default class extends Controller {
    static targets = [ "arrow" ]

    connect() {
        $(document).ready(() => {
            this.isVisible = this.element.dataset.isVisible === 'true';

            EventManager.fire('toggle-file-upload-section', {
                creditId: this.creditId,
                isVisible: this.isVisible
            });
        });
    }

    toggle() {
        this.isVisible = !this.isVisible;

        EventManager.fire('toggle-file-upload-section', {
            creditId: this.creditId,
            isVisible: this.isVisible
        });
    }

    get isVisible() {
        return this.element.dataset.isVisible === 'true';
    }

    set isVisible(value) {
        this.element.setAttribute('data-is-visible', value);

        if (value === true) {
            this.arrowTarget.classList.remove('fa-angle-right');
            this.arrowTarget.classList.add('fa-angle-down');
        } else {
            this.arrowTarget.classList.remove('fa-angle-down');
            this.arrowTarget.classList.add('fa-angle-right');
        }
    }

    get creditId() {
        return this.element.dataset.creditId;
    }
}