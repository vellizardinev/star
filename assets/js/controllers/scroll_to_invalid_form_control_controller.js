import { Controller } from "stimulus"

export default class extends Controller {
    connect() {
        let invalidControl = this.element.querySelector('.form-control.is-invalid');

         if (!invalidControl) {
             return;
         }

        invalidControl.scrollIntoView({behavior: "smooth", block: "center"});
    }
}