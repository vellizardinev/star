(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/css/app.scss":
/*!*****************************!*\
  !*** ./assets/css/app.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _css_app_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../css/app.scss */ "./assets/css/app.scss");
/* harmony import */ var _css_app_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_app_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bootstrap */ "./assets/js/bootstrap.js");
/* harmony import */ var _bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _custom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./custom */ "./assets/js/custom.js");
/* harmony import */ var _custom__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_custom__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js");
/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(bootstrap__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! bootstrap-datepicker */ "./node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js");
/* harmony import */ var bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var bootstrap_datepicker_dist_css_bootstrap_datepicker_standalone_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.css */ "./node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.css");
/* harmony import */ var bootstrap_datepicker_dist_css_bootstrap_datepicker_standalone_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(bootstrap_datepicker_dist_css_bootstrap_datepicker_standalone_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var _infrastructure_Flash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./infrastructure/Flash */ "./assets/js/infrastructure/Flash.js");
/* harmony import */ var _infrastructure_Flashes__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./infrastructure/Flashes */ "./assets/js/infrastructure/Flashes.js");
/* harmony import */ var _infrastructure_Tab__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./infrastructure/Tab */ "./assets/js/infrastructure/Tab.js");
/* harmony import */ var _infrastructure_Tabs__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./infrastructure/Tabs */ "./assets/js/infrastructure/Tabs.js");
/* harmony import */ var _infrastructure_Modal__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./infrastructure/Modal */ "./assets/js/infrastructure/Modal.js");
/* harmony import */ var _infrastructure_Carousel__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./infrastructure/Carousel */ "./assets/js/infrastructure/Carousel.js");
/* harmony import */ var _infrastructure_DeleteLink__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./infrastructure/DeleteLink */ "./assets/js/infrastructure/DeleteLink.js");
/* harmony import */ var _infrastructure_DeleteModuleItem__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./infrastructure/DeleteModuleItem */ "./assets/js/infrastructure/DeleteModuleItem.js");
/* harmony import */ var _infrastructure_PostLink__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./infrastructure/PostLink */ "./assets/js/infrastructure/PostLink.js");
/* harmony import */ var _infrastructure_InlineValueEdit__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./infrastructure/InlineValueEdit */ "./assets/js/infrastructure/InlineValueEdit.js");
/* harmony import */ var _infrastructure_InlineBooleanEdit__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./infrastructure/InlineBooleanEdit */ "./assets/js/infrastructure/InlineBooleanEdit.js");
/* harmony import */ var _infrastructure_FileUploadButton__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./infrastructure/FileUploadButton */ "./assets/js/infrastructure/FileUploadButton.js");
/* harmony import */ var _infrastructure_FilesList__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./infrastructure/FilesList */ "./assets/js/infrastructure/FilesList.js");
/* harmony import */ var _infrastructure_Slider__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./infrastructure/Slider */ "./assets/js/infrastructure/Slider.js");
/* harmony import */ var _components_EntityTranslation__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/EntityTranslation */ "./assets/js/components/EntityTranslation.js");
/* harmony import */ var _components_EntityTranslationsForm__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/EntityTranslationsForm */ "./assets/js/components/EntityTranslationsForm.js");
/* harmony import */ var _components_CreditCalculator__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/CreditCalculator */ "./assets/js/components/CreditCalculator.js");
/* harmony import */ var _components_CreditOffer__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/CreditOffer */ "./assets/js/components/CreditOffer.js");
/* harmony import */ var _components_CreditComparision__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./components/CreditComparision */ "./assets/js/components/CreditComparision.js");
/* harmony import */ var _components_PollAnswer__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./components/PollAnswer */ "./assets/js/components/PollAnswer.js");
/* harmony import */ var _components_PollQuestion__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./components/PollQuestion */ "./assets/js/components/PollQuestion.js");
/* harmony import */ var _components_Poll__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./components/Poll */ "./assets/js/components/Poll.js");
/* harmony import */ var _components_Gmaps__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./components/Gmaps */ "./assets/js/components/Gmaps.js");
/* harmony import */ var _components_SliderModule__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./components/SliderModule */ "./assets/js/components/SliderModule.js");
/* harmony import */ var _components_PollParticipation__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./components/PollParticipation */ "./assets/js/components/PollParticipation.js");
/* harmony import */ var _components_ImageGallery__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./components/ImageGallery */ "./assets/js/components/ImageGallery.js");
/* harmony import */ var _components_CookieDeclaration__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./components/CookieDeclaration */ "./assets/js/components/CookieDeclaration.js");
/* harmony import */ var _components_ZopimChat__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./components/ZopimChat */ "./assets/js/components/ZopimChat.js");
/* harmony import */ var _components_CreditRepayment__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./components/CreditRepayment */ "./assets/js/components/CreditRepayment.js");
/* harmony import */ var _components_CreditRefinance__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./components/CreditRefinance */ "./assets/js/components/CreditRefinance.js");
/* harmony import */ var _components_CreditRepaymentAmountForm__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./components/CreditRepaymentAmountForm */ "./assets/js/components/CreditRepaymentAmountForm.js");
/* harmony import */ var _components_FrontendValidation__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./components/FrontendValidation */ "./assets/js/components/FrontendValidation.js");
/* harmony import */ var _components_ApplyOnlineButton__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./components/ApplyOnlineButton */ "./assets/js/components/ApplyOnlineButton.js");
/* harmony import */ var _components_TelephoneApplicationNotAllowed__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./components/TelephoneApplicationNotAllowed */ "./assets/js/components/TelephoneApplicationNotAllowed.js");
/* harmony import */ var _components_RedirectTimer__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./components/RedirectTimer */ "./assets/js/components/RedirectTimer.js");
/* harmony import */ var _components_TranslationItem__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./components/TranslationItem */ "./assets/js/components/TranslationItem.js");
/* harmony import */ var _components_TranslationsList__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./components/TranslationsList */ "./assets/js/components/TranslationsList.js");
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var stimulus_webpack_helpers__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! stimulus/webpack-helpers */ "./node_modules/stimulus/webpack-helpers.js");















































window.EventManager = new class {
  constructor() {
    this.vue = new vue__WEBPACK_IMPORTED_MODULE_7__["default"]();
  }

  fire(event, data = null) {
    this.vue.$emit(event, data);
  }

  listen(event, callback) {
    this.vue.$on(event, callback);
  }

}();

window.flash = function (message, level = 'success') {
  window.EventManager.fire('flash', {
    message,
    level
  });
};

const application = stimulus__WEBPACK_IMPORTED_MODULE_45__["Application"].start();

const context = __webpack_require__("./assets/js/controllers sync recursive \\.js$");

application.load(Object(stimulus_webpack_helpers__WEBPACK_IMPORTED_MODULE_46__["definitionsFromContext"])(context));
vue__WEBPACK_IMPORTED_MODULE_7__["default"].options.delimiters = ['[[', ']]'];
vue__WEBPACK_IMPORTED_MODULE_7__["default"].config.ignoredElements = ['x-trans'];
const app = new vue__WEBPACK_IMPORTED_MODULE_7__["default"]({
  el: '#app',
  data: {
    showModal: false,
    dynamicComponents: []
  },

  mounted() {
    EventManager.listen('add-component', this.addComponent);
    EventManager.listen('remove-component', this.removeComponent);
  },

  methods: {
    addComponent(data) {
      this.dynamicComponents.push(data);
    },

    removeComponent(componentId) {
      let index = this.dynamicComponents.findIndex(item => item.id === componentId);
      this.dynamicComponents.splice(index, 1);
    }

  }
});
jquery__WEBPACK_IMPORTED_MODULE_1___default()(function () {
  jquery__WEBPACK_IMPORTED_MODULE_1___default()('[data-toggle="tooltip"]').tooltip();
});
let elements = document.querySelectorAll('input, select, textarea');
elements.forEach(element => {
  element.addEventListener('invalid', function () {
    element.scrollIntoView(false);
  });
});
jquery__WEBPACK_IMPORTED_MODULE_1___default()(function () {
  jquery__WEBPACK_IMPORTED_MODULE_1___default()("form").submit(function (event) {
    jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).find('button[type="submit"]').prop("disabled", true);
  });
});

/***/ }),

/***/ "./assets/js/bootstrap.js":
/*!********************************!*\
  !*** ./assets/js/bootstrap.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
  $.ajaxSetup({
    beforeSend: function (xhr, settings) {
      xhr.setRequestHeader("X-CSRFToken", token.content);
    }
  });
} else {
  console.error('CSRF token not found.');
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/ApplyOnlineButton.js":
/*!***************************************************!*\
  !*** ./assets/js/components/ApplyOnlineButton.js ***!
  \***************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");


vue__WEBPACK_IMPORTED_MODULE_1__["default"].component('apply-online-button', {
  name: "ApplyOnlineButton",
  delimiters: ['[[', ']]'],
  template: '#apply-online-button',
  props: {
    checkCustomerIsAllowedUrl: {
      type: String,
      required: true
    },
    redirectUrl: {
      required: true,
      type: String
    }
  },

  data() {
    return {
      checking: false
    };
  },

  computed: {
    isChecking() {
      return this.checking === true;
    }

  },
  methods: {
    checkCustomerIsDenied() {
      let _self = this;

      this.checking = true;
      $.ajax({
        method: 'get',
        url: this.checkCustomerIsAllowedUrl,

        success(response) {
          _self.checking = false;

          if (response.isDenied) {
            $('#' + _self.$refs.modal.$el.getAttribute('id')).modal('show');
          } else {
            window.location.replace(_self.redirectUrl);
          }
        },

        error(error) {
          _self.checking = false;
          console.log(error);
        }

      });
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/CookieDeclaration.js":
/*!***************************************************!*\
  !*** ./assets/js/components/CookieDeclaration.js ***!
  \***************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('cookie-declaration', {
  template: `<div id="CookieDeclaration"></div>`,

  mounted() {
    this.loadJs('https://consent.cookiebot.com/e15aaf9f-7788-4a45-96ee-cd51ae692650/cd.js', function () {
      console.log('Loaded cookie declaration script.');
    });
  },

  methods: {
    loadJs(url, callback) {
      $.ajax({
        url: url,
        dataType: 'script',
        success: callback,
        async: true
      });
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/CreditCalculator.js":
/*!**************************************************!*\
  !*** ./assets/js/components/CreditCalculator.js ***!
  \**************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var ion_rangeslider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ion-rangeslider */ "./node_modules/ion-rangeslider/js/ion.rangeSlider.js");
/* harmony import */ var ion_rangeslider__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ion_rangeslider__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ion-rangeslider/css/ion.rangeSlider.css */ "./node_modules/ion-rangeslider/css/ion.rangeSlider.css");
/* harmony import */ var ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_2__);



vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('credit-calculator', {
  delimiters: ['[[', ']]'],
  template: '#credit-calculator',
  props: {
    standalone: {
      required: false,
      type: Boolean,
      default: false
    },
    currency: {
      required: false,
      type: String,
      default: 'грн.'
    },
    locale: {
      required: false,
      type: String,
      default: 'ua'
    },
    credit: {
      required: false,
      type: String,
      default: null
    },
    creditAmount: {
      required: false,
      type: String,
      default: null
    },
    creditPayment: {
      required: false,
      type: String,
      default: null
    }
  },

  data() {
    return {
      isRefreshing: false,
      amountSteps: [],
      paymentSteps: [],
      type: 'personal',
      amount: 2000.00,
      payment: 120.00,
      paymentWithPenalty: 120.00,
      periods: 36,
      total: 4320.00,
      totalWithPenalty: 4320.00,
      interestRateFirstPeriod: 3.0599,
      interestRateSecondPeriod: 0.0128,
      aprc: 11.3817,
      paymentSchedule: 'weekly',
      pensioner: 'no',
      creditTitle: '',
      creditSlug: '',
      requirements: '',
      default: null,
      creditProductId: '',
      creditPeriodId: '',
      expenses: 2082
    };
  },

  mounted() {
    this.getData(this.defaultParams(), true);
    EventManager.listen('amount-changed', this.amountChanged);
    EventManager.listen('payment-changed', this.paymentChanged);
  },

  methods: {
    defaultParams() {
      if (this.credit) {
        return {
          credit: this.credit,
          amount: this.creditAmount,
          payment: this.creditPayment
        };
      }

      return {
        pensioner: 'no',
        paymentSchedule: 'weekly',
        type: 'personal',
        amount: 2000.00
      };
    },

    getData(params = this.defaultParams(), creditHasChanged = false) {
      let url = '/en/credit-data?' + $.param(params),
          _self = this;

      this.isRefreshing = true;
      $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
          _self.amountSteps = response.amountSteps.map(value => parseFloat(value));
          _self.paymentSteps = response.paymentSteps.map(value => parseFloat(value));
          _self.paymentSchedule = response.creditData.credit.paymentSchedule;
          _self.pensioner = response.creditData.credit.pensioner === true ? 'yes' : 'no';
          _self.type = response.creditData.credit.type;
          _self.amount = response.creditData.amount;
          _self.payment = response.creditData.payment;
          _self.paymentWithPenalty = response.creditData.paymentWithPenalty;
          _self.periods = response.creditData.periods;
          _self.total = response.creditData.total;
          _self.totalWithPenalty = response.creditData.totalWithPenalty;
          _self.interestRateFirstPeriod = response.creditData.interestRateFirstPeriod;
          _self.interestRateSecondPeriod = response.creditData.interestRateSecondPeriod;
          _self.aprc = response.creditData.aprc;
          _self.creditTitle = response.creditData.credit.title;
          _self.creditSlug = response.creditData.credit.slug;
          _self.requirements = response.creditData.credit.requirements;
          _self.default = response.creditData.credit.defaultAmount;
          _self.creditProductId = response.creditData.productId;
          _self.creditPeriodId = response.creditData.creditPeriodId;
          _self.expenses = response.creditData.totalWithPenalty - response.creditData.amount;
          _self.isRefreshing = false;

          if (creditHasChanged) {
            _self.initAmountSlider();
          }

          _self.initPaymentSlider();

          EventManager.fire('credit-parameters-changed', {
            product: _self.creditTitle,
            slug: _self.creditSlug,
            amount: _self.amount,
            period: _self.periods,
            payment: _self.payment,
            creditProductId: _self.creditProductId,
            creditPeriodId: _self.creditPeriodId,
            requirements: _self.requirements
          });
        },
        error: function (response) {
          console.log(response);
        }
      });
    },

    amountChanged(value) {
      this.amount = value.toFixed(2);
      let parameters = {
        amount: this.amount,
        pensioner: this.pensioner,
        paymentSchedule: this.paymentSchedule,
        type: this.type
      };
      this.getData(parameters);
    },

    paymentChanged(value) {
      this.paymentWithPenalty = value.toFixed(2);
      let parameters = {
        payment: this.paymentWithPenalty,
        amount: this.amount,
        pensioner: this.pensioner,
        paymentSchedule: this.paymentSchedule,
        type: this.type
      };
      this.getData(parameters);
    },

    paymentScheduleChanged() {
      if (this.paymentSchedule === 'monthly' && this.type === 'personal') {
        this.pensioner = 'no';
      }

      let parameters = {
        paymentSchedule: this.paymentSchedule,
        type: this.type,
        pensioner: this.pensioner,
        amount: this.defaultAmountForPaymentSchedule
      };
      this.getData(parameters, true);
    },

    pensionerChanged() {
      let parameters = {};

      if (this.pensioner === 'yes') {
        this.paymentSchedule = 'monthly';
        parameters = {
          paymentSchedule: this.paymentSchedule,
          type: this.type,
          pensioner: this.pensioner,
          amount: this.defaultAmountForPaymentSchedule
        };
      } else {
        this.paymentSchedule = 'weekly';
        parameters = {
          paymentSchedule: this.paymentSchedule,
          type: this.type,
          pensioner: this.pensioner,
          amount: this.defaultAmountForPaymentSchedule
        };
      }

      this.getData(parameters, true);
    },

    changeType(newType) {
      if (this.type === newType) {
        return;
      }

      this.type = newType;

      if (newType === 'business') {
        this.pensioner = 'no';
      }

      this.paymentSchedule = 'weekly';
      let parameters = {
        paymentSchedule: this.paymentSchedule,
        type: this.type,
        pensioner: this.pensioner
      };

      if (newType === 'business') {
        parameters.amount = 1600.00;
        parameters.payment = 92.00;
      }

      this.getData(parameters, true);
    },

    initAmountSlider() {
      let amountSlider = $(this.$refs.amount).data("ionRangeSlider"),
          index = this.amountSteps.findIndex(value => value.toFixed(2) === this.amount);

      if (amountSlider) {
        amountSlider.destroy();
      }

      $(this.$refs.amount).ionRangeSlider({
        skin: 'flat',
        grid: true,
        grid_snap: true,
        values: this.amountSteps,
        from: index,
        postfix: ' ' + this.currency,
        force_edges: false,
        onFinish: function (data) {
          EventManager.fire('amount-changed', data.from_value);
        }
      });
      amountSlider = $(this.$refs.amount).data("ionRangeSlider");
      amountSlider.update({
        from: index
      });
    },

    initPaymentSlider() {
      let paymentSlider = $(this.$refs.payment).data("ionRangeSlider"),
          adjustedPaymentSteps = this.paymentSteps;

      if (paymentSlider) {
        paymentSlider.destroy();
      }

      if (adjustedPaymentSteps.length === 1) {
        adjustedPaymentSteps.push(adjustedPaymentSteps[0]);
      }

      let index = this.paymentSteps.findIndex(value => value.toFixed(2) === this.paymentWithPenalty);
      $(this.$refs.payment).ionRangeSlider({
        skin: 'flat',
        grid: true,
        grid_snap: true,
        values: adjustedPaymentSteps,
        from: index,
        postfix: ' ' + this.currency,
        force_edges: false,
        onFinish: function (data) {
          EventManager.fire('payment-changed', data.from_value);
        }
      });
      paymentSlider = $(this.$refs.payment).data("ionRangeSlider");
      paymentSlider.update({
        from: index
      });
    },

    createOffer() {
      EventManager.fire('create-credit-offer', {
        type: this.type,
        amount: this.amount,
        payment: this.paymentWithPenalty,
        paymentWithPenalty: this.paymentWithPenalty,
        periods: this.periods,
        duration: this.duration,
        total: this.totalWithPenalty,
        totalWithPenalty: this.totalWithPenalty,
        paymentSchedule: this.paymentSchedule,
        interestRateFirstPeriod: this.interestFirstPeriod,
        interestRateSecondPeriod: this.interestSecondPeriod,
        aprc: this.interestAPRC,
        expenses: this.expenses,
        pensioner: this.pensioner,
        title: this.creditTitle,
        slug: this.creditSlug,
        requirements: this.requirements
      });
    },

    apply() {
      let url = `/get-application-route/${this.creditSlug}/${this.locale}`,
          _self = this;

      $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
          window.location.href = `${response}/?amount=${_self.amount}&payment=${_self.payment}&period=${_self.periods}`;
        },
        error: function (response) {
          console.log(response);
        }
      });
    }

  },
  computed: {
    duration() {
      if (this.paymentSchedule === 'weekly') {
        return Math.ceil(this.periods / 4.33);
      } else if (this.paymentSchedule === 'biweekly') {
        return Math.ceil(this.periods / 2);
      } else {
        return this.periods;
      }
    },

    interestFirstPeriod() {
      return (this.interestRateFirstPeriod * 100).toFixed(2) + '%';
    },

    interestSecondPeriod() {
      return (this.interestRateSecondPeriod * 100).toFixed(2) + '%';
    },

    interestAPRC() {
      return (this.aprc * 100).toFixed(2) + '%';
    },

    defaultAmount() {
      return this.default === null ? this.amount : this.default;
    },

    defaultAmountForPaymentSchedule() {
      if (this.paymentSchedule === 'weekly') {
        return 2000.00;
      }

      if (this.paymentSchedule === 'biweekly') {
        return 2000.00;
      }

      if (this.paymentSchedule === 'monthly') {
        if (this.pensioner === 'no') {
          return 2000.00;
        } else {
          return 1000.00;
        }
      }
    },

    containerClass() {
      return {
        'col-md-6': this.standalone === false,
        'col-xl-4': this.standalone === false
      };
    },

    bottomClass() {
      return {
        'standalone': this.standalone === true
      };
    }

  },
  watch: {
    type: function () {
      EventManager.fire('credit-type-changed');
    }
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/CreditComparision.js":
/*!***************************************************!*\
  !*** ./assets/js/components/CreditComparision.js ***!
  \***************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var ion_rangeslider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ion-rangeslider */ "./node_modules/ion-rangeslider/js/ion.rangeSlider.js");
/* harmony import */ var ion_rangeslider__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ion_rangeslider__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ion-rangeslider/css/ion.rangeSlider.css */ "./node_modules/ion-rangeslider/css/ion.rangeSlider.css");
/* harmony import */ var ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_2__);



vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('credit-comparison', {
  delimiters: ['[[', ']]'],
  template: '#credit-comparison',
  props: {
    paymentSchemeVisible: {
      required: false,
      type: Boolean,
      default: true
    },
    pensionerVisible: {
      required: false,
      type: Boolean,
      default: true
    },
    interestRateVisible: {
      required: false,
      type: Boolean,
      default: true
    }
  },

  data() {
    return {
      offers: []
    };
  },

  mounted() {
    EventManager.listen('create-credit-offer', this.createOffer);
    EventManager.listen('remove-credit-offer', this.removeOffer);
    EventManager.listen('credit-type-changed', this.removeOffer);
  },

  methods: {
    createOffer(data) {
      if (this.offers.length >= 2) {
        return;
      }

      this.offers.push(data);
    },

    removeOffer(index) {
      if (index === null) {
        this.offers = [];
      }

      this.offers.splice(index, 1);
    }

  }
});

/***/ }),

/***/ "./assets/js/components/CreditOffer.js":
/*!*********************************************!*\
  !*** ./assets/js/components/CreditOffer.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var ion_rangeslider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ion-rangeslider */ "./node_modules/ion-rangeslider/js/ion.rangeSlider.js");
/* harmony import */ var ion_rangeslider__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ion_rangeslider__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ion-rangeslider/css/ion.rangeSlider.css */ "./node_modules/ion-rangeslider/css/ion.rangeSlider.css");
/* harmony import */ var ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ion_rangeslider_css_ion_rangeSlider_css__WEBPACK_IMPORTED_MODULE_2__);



vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('credit-offer', {
  delimiters: ['[[', ']]'],
  template: '#credit-offer',
  props: {
    index: {
      required: true,
      type: Number
    },
    currency: {
      required: false,
      type: String,
      default: 'грн.'
    },
    locale: {
      required: false,
      type: String,
      default: 'ua'
    },
    type: {
      required: false,
      type: String,
      default: 'personal'
    },
    amount: {
      required: false,
      type: String,
      default: 0
    },
    payment: {
      required: false,
      type: String,
      default: 0
    },
    paymentWithPenalty: {
      required: false,
      type: String,
      default: 0
    },
    periods: {
      required: false,
      type: Number,
      default: 0
    },
    duration: {
      required: false,
      type: Number,
      default: 0
    },
    total: {
      required: false,
      type: String,
      default: 0
    },
    totalWithPenalty: {
      required: false,
      type: String,
      default: 0
    },
    interestRateFirstPeriod: {
      required: false,
      type: String,
      default: 0
    },
    interestRateSecondPeriod: {
      required: false,
      type: String,
      default: 0
    },
    aprc: {
      required: false,
      type: String,
      default: ''
    },
    expenses: {
      required: false,
      type: Number,
      default: ''
    },
    paymentSchedule: {
      required: false,
      type: String,
      default: 'weekly'
    },
    pensioner: {
      required: false,
      type: String,
      default: 'no'
    },
    title: {
      required: false,
      type: String,
      default: 'iCredit'
    },
    slug: {
      required: false,
      type: String,
      default: 'icredit'
    },
    requirements: {
      required: false,
      type: String,
      default: 'only ID card needed'
    }
  },
  methods: {
    close() {
      EventManager.fire('remove-credit-offer', this.index);
    },

    apply() {
      let url = `/get-application-route/${this.slug}/${this.locale}`,
          _self = this;

      $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
          window.location.href = `${response}/?amount=${_self.amount}&payment=${_self.payment}&period=${_self.periods}`;
        },
        error: function (response) {
          console.log(response);
        }
      });
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/CreditRefinance.js":
/*!*************************************************!*\
  !*** ./assets/js/components/CreditRefinance.js ***!
  \*************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('credit-refinance', {
  delimiters: ['[[', ']]'],
  template: '#credit-refinance',
  props: {
    contract: {
      required: true,
      type: Number
    }
  },

  data() {
    return {
      refinanceSum: null
    };
  },

  methods: {
    getRefinanceSum() {
      let url = `/customer-profile/${this.contract}/calculate-refinance`,
          _self = this;

      $.ajax({
        method: 'GET',
        url: url,

        success(response) {
          _self.refinanceSum = response.refinanceSum;
        },

        error(error) {
          flash(error['responseJSON'], 'error');
        }

      });
    }

  },
  computed: {
    formattedRefinanceSum() {
      if (this.refinanceSum === null) {
        return '';
      }

      if (this.refinanceSum > 0) {
        return `${this.refinanceSum.toFixed(2)} UAH`;
      }

      return '';
    },

    state() {
      if (this.refinanceSum === null) {
        return 'initial';
      }

      if (this.refinanceSum > 0) {
        return 'refinance';
      }

      if (this.refinanceSum === 0) {
        return 'refinance_not_possible';
      }

      if (this.refinanceSum === -1) {
        return 'error';
      }

      return 'initial';
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/CreditRepayment.js":
/*!*************************************************!*\
  !*** ./assets/js/components/CreditRepayment.js ***!
  \*************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('credit-repayment', {
  delimiters: ['[[', ']]'],
  template: '#credit-repayment',
  props: {
    contract: {
      required: true,
      type: Number
    }
  },

  data() {
    return {
      repaymentSum: null
    };
  },

  methods: {
    getRepaymentSum() {
      let url = `/customer-profile/${this.contract}/calculate-repayment`,
          _self = this;

      $.ajax({
        method: 'GET',
        url: url,

        success(response) {
          _self.repaymentSum = response.repaymentSum;
        },

        error(error) {
          flash(error['responseJSON'], 'error');
        }

      });
    },

    redirectForPayment() {
      let url = `/customer-profile/repayment-url`,
          data = {
        contract: this.contract,
        amount: this.repaymentSum,
        isEarlyRepayment: true
      };
      $.ajax({
        method: 'POST',
        data: data,
        url: url,

        success(response) {
          window.location.href = response.repaymentUrl;
        },

        error(error) {
          flash(error['responseJSON'], 'error');
        }

      });
    }

  },
  computed: {
    formattedRepaymentSum() {
      if (this.repaymentSum === null) {
        return '';
      }

      if (this.repaymentSum > 0) {
        return `${this.repaymentSum.toFixed(2)} UAH`;
      }

      return '';
    },

    state() {
      if (this.repaymentSum === null) {
        return 'initial';
      }

      if (this.repaymentSum > 0) {
        return 'pay';
      }

      if (this.repaymentSum === 0) {
        return 'repayment_not_possible';
      }

      if (this.repaymentSum === -1) {
        return 'error';
      }

      return 'initial';
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/CreditRepaymentAmountForm.js":
/*!***********************************************************!*\
  !*** ./assets/js/components/CreditRepaymentAmountForm.js ***!
  \***********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('credit-repayment-amount-form', {
  delimiters: ['[[', ']]'],
  template: '#credit-repayment-amount-form',
  props: {
    initialAmount: {
      required: false,
      type: Number,
      default: 0
    },
    contract: {
      required: true,
      type: String
    }
  },

  data() {
    return {
      amount: 0.00,
      errors: []
    };
  },

  mounted() {
    this.amount = this.initialAmount.toFixed(2);
  },

  methods: {
    submit() {
      let url = `/customer-profile/repayment-url`,
          data = {
        contract: this.contract,
        amount: this.amount,
        isEarlyRepayment: false
      },
          _self = this;

      $.ajax({
        method: 'POST',
        data: data,
        url: url,

        success(response) {
          window.location.href = response.repaymentUrl;
        },

        error(response) {
          _self.errors = response.responseJSON.errors;
        }

      });
    }

  },
  computed: {
    hasError() {
      return this.errors.length > 0;
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/EntityTranslation.js":
/*!***************************************************!*\
  !*** ./assets/js/components/EntityTranslation.js ***!
  \***************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('entity-translation', {
  delimiters: ['[[', ']]'],
  template: `
<div class="form-group">
    <label for="content" class="control-label text-uppercase text-primary font-weight-bolder">[[locale]]</label>
    <span class="fa fa-cloud-upload text-dark" @click="saveContent" style="font-size: 1.5em; cursor: pointer;"></span>
    <textarea name="content" class="form-control" rows="3" v-model="content"></textarea>
</div>
`,
  props: {
    objectClass: {
      required: true,
      type: String
    },
    foreignKey: {
      required: true,
      type: String
    },
    field: {
      required: true,
      type: String
    },
    locale: {
      required: true,
      type: String
    }
  },

  data() {
    return {
      content: 'Loading...'
    };
  },

  mounted() {
    this.getContent();
  },

  methods: {
    getContent() {
      let url = `/admin/entity-translations/get/${encodeURI(this.objectClass)}/${this.foreignKey}/${this.field}/${this.locale}`,
          _self = this;

      $.ajax({
        method: 'GET',
        url: url,

        success(response) {
          _self.content = response.content;
        },

        error(error) {
          console.log(error);
        }

      });
    },

    saveContent() {
      let url = `/admin/entity-translations/${encodeURI(this.objectClass)}/${this.foreignKey}/${this.field}/${this.locale}`,
          _self = this,
          data = {
        content: this.content
      };

      $.ajax({
        method: 'POST',
        url: url,
        data: data,

        success() {
          flash('Translation was updated.', 'success');
        },

        error(error) {
          console.log(error);
          flash('Translation could not be saved.', 'error');
        }

      });
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/EntityTranslationsForm.js":
/*!********************************************************!*\
  !*** ./assets/js/components/EntityTranslationsForm.js ***!
  \********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('entity-translations-form', {
  delimiters: ['[[', ']]'],
  template: `
<div>
    <entity-translation v-for="locale in localesArray" :key="locale" :object-class="objectClass" :field="field" :foreign-key="foreignKey" :locale="locale"></entity-translation>
</div>
`,
  props: {
    objectClass: {
      required: true,
      type: String
    },
    foreignKey: {
      required: true,
      type: String
    },
    field: {
      required: true,
      type: String
    },
    locales: {
      required: true,
      type: String
    }
  },

  data() {
    return {
      localesArray: [],
      translations: {}
    };
  },

  mounted() {
    this.localesArray = this.locales.split('|');
  }

});

/***/ }),

/***/ "./assets/js/components/FrontendValidation.js":
/*!****************************************************!*\
  !*** ./assets/js/components/FrontendValidation.js ***!
  \****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('frontend-validation', {
  delimiters: ['[[', ']]'],
  template: `
      <div class="form-group">
          <label for="amount" class="required">
            <i class="fa fa-asterisk fa-required" v-if="required"></i>
            <span v-text="label"></span>
            <span class="invalid-feedback d-block" v-for="error in errors" v-if="hasErrors">
                <span class="d-block">
                    <span class="form-error-icon badge badge-danger text-uppercase">
                        ERROR
                    </span>
                    <span class="form-error-message" v-text="error"></span>
                </span>
            </span>
          </label>
          <slot></slot>
      </div>
    `,
  props: {
    property: {
      required: true,
      type: String
    },
    label: {
      required: true,
      type: String
    },
    required: {
      required: false,
      type: Boolean,
      default: true
    },
    entity: {
      required: true,
      type: String
    }
  },

  data() {
    return {
      errors: []
    };
  },

  mounted() {
    this.$refs['input'] = this.$el.querySelector('input');
    this.$refs.input.addEventListener('input', this.validate);
  },

  methods: {
    validate() {
      let url = `/frontend-validation/validate`,
          data = {
        property: this.property,
        entity: this.entity,
        value: this.$refs.input.value
      },
          _self = this;

      $.ajax({
        method: 'POST',
        data: data,
        url: url,

        success() {
          _self.errors = [];

          _self.$refs.input.classList.remove('is-invalid');
        },

        error(response) {
          _self.errors = response.responseJSON.errors;

          if (_self.errors.length > 0) {
            _self.$refs.input.classList.add('is-invalid');
          }
        }

      });
    }

  },
  computed: {
    hasErrors() {
      return this.errors.length > 0;
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/Gmaps.js":
/*!***************************************!*\
  !*** ./assets/js/components/Gmaps.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var _utils_gmaps__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/gmaps */ "./assets/js/utils/gmaps.js");
/* harmony import */ var _google_markerclusterer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @google/markerclusterer */ "./node_modules/@google/markerclusterer/src/markerclusterer.js");
/* harmony import */ var _google_markerclusterer__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_google_markerclusterer__WEBPACK_IMPORTED_MODULE_2__);



vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('gmaps', {
  delimiters: ['[[', ']]'],
  template: '#gmaps',
  props: {
    address: {
      required: false,
      type: String,
      default: 'Ukraine'
    },
    zoom: {
      required: false,
      type: Number,
      default: 6
    },
    type: {
      required: false,
      type: String,
      default: null
    },
    offices: {
      required: false,
      type: Array,
      default: null
    },
    cities: {
      required: false,
      type: Array,
      default: null
    },
    marker1: {
      required: true,
      type: String
    },
    marker2: {
      required: true,
      type: String
    },
    officeIcon: {
      required: true,
      type: String
    },
    showMapInitially: {
      type: Boolean,
      required: false,
      default: true
    }
  },

  data() {
    return {
      isOfficeSelected: false,
      map: null,
      geocoder: null,
      selectedCity: this.address,
      selectedOffice: {
        name: null,
        city: null,
        telephone: null,
        workingTime: [],
        googleMapLink: null
      },
      styles: [{
        url: this.marker1,
        width: 40,
        height: 40,
        textColor: '#ffffff',
        textSize: 10
      }, {
        url: this.marker2,
        width: 40,
        height: 40,
        textColor: '#ffffff',
        textSize: 11
      }, {
        url: this.marker2,
        width: 40,
        height: 40,
        textColor: '#ffffff',
        textSize: 12
      }],
      mapStyles: [{
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#0c0b0b"
        }]
      }, {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [{
          "color": "#f2f2f2"
        }]
      }, {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "road",
        "elementType": "all",
        "stylers": [{
          "saturation": -100
        }, {
          "lightness": 45
        }]
      }, {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#090909"
        }]
      }, {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [{
          "visibility": "simplified"
        }]
      }, {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [{
          "visibility": "simplified"
        }]
      }, {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [{
          "visibility": "on"
        }, {
          "color": "#b3c0cb"
        }]
      }, {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#9b7f7f"
        }]
      }, {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [{
          "color": "#fef7f7"
        }]
      }],
      isMapVisible: false
    };
  },

  computed: {
    gmapType: function () {
      if (this.type === 'satellite') {
        return google.maps.MapTypeId.SATELLITE;
      } else if (this.type === 'hybrid') {
        return google.maps.MapTypeId.HYBRID;
      } else if (this.type === 'terrain') {
        return google.maps.MapTypeId.TERRAIN;
      }

      return google.maps.MapTypeId.ROADMAP;
    },

    telephoneText() {
      if (this.selectedOffice.telephone) {
        return this.selectedOffice.telephone;
      } else {
        return 'n/a';
      }
    },

    workingTimeText() {
      if (typeof this.selectedOffice.workingTime !== 'string') {
        return;
      }

      let parts = this.selectedOffice.workingTime.split(',');
      return `${parts[0]}: ${parts[1]} - ${parts[2]}`;
    },

    computeZoom() {
      if (this.selectedCity && this.address !== this.selectedCity) {
        return 13;
      }

      return this.zoom;
    }

  },

  async mounted() {
    this.isMapVisible = this.showMapInitially;

    if (this.isMapVisible) {
      await this.loadMap();
    }
  },

  methods: {
    async loadMap() {
      try {
        const google = await Object(_utils_gmaps__WEBPACK_IMPORTED_MODULE_1__["default"])();
        const map = new google.maps.Map(this.$refs.gmap);
        this.geocoder = new google.maps.Geocoder();
        this.map = map;
        map.setOptions({
          styles: this.mapStyles
        });
        this.geocoder.geocode({
          address: this.address
        }, (results, status) => {
          if (status !== 'OK' || !results[0]) {
            throw new Error(status);
          }

          map.setCenter(results[0].geometry.location);
          map.fitBounds(results[0].geometry.viewport);
          map.setZoom(this.zoom);
          map.mapTypeId = this.gmapType;
          map.gestureHandling = 'cooperative';
        });

        const markerClickHandler = marker => {
          map.setZoom(17);
          map.setCenter(marker.getPosition());
          this.selectOffice(marker);
        };

        const markers = this.offices.map(location => {
          const marker = new google.maps.Marker({ ...location,
            map,
            icon: this.officeIcon
          });
          marker.addListener('click', () => markerClickHandler(marker));
          return marker;
        });
        new _google_markerclusterer__WEBPACK_IMPORTED_MODULE_2___default.a(map, markers, {
          styles: this.styles
        });
      } catch (error) {
        console.error(error);
      }
    },

    filter() {
      this.geocoder.geocode({
        address: this.address + ',' + this.selectedCity
      }, (results, status) => {
        if (status !== 'OK' || !results[0]) {
          throw new Error(status);
        }

        this.map.setCenter(results[0].geometry.location);
        this.map.fitBounds(results[0].geometry.viewport);
        this.map.setZoom(this.computeZoom);
        this.map.mapTypeId = this.gmapType;
        this.map.gestureHandling = 'cooperative';
      });
    },

    selectOffice(office) {
      this.selectedOffice.name = office.title;
      this.selectedOffice.city = office.city;
      this.selectedOffice.telephone = office.telephone;
      this.selectedOffice.address = office.address;
      this.selectedOffice.workingTime = office.workingTime;
      this.isOfficeSelected = true;
      this.selectedOffice.googleMapLink = "https://www.google.com/maps/dir//" + office.getPosition().lat() + "," + office.getPosition().lng();
    },

    deselectOffice() {
      this.isOfficeSelected = false;
    },

    async showMap() {
      this.isMapVisible = true;
      await this.loadMap();
    }

  }
});

/***/ }),

/***/ "./assets/js/components/ImageGallery.js":
/*!**********************************************!*\
  !*** ./assets/js/components/ImageGallery.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('image-gallery', {
  delimiters: ['[[', ']]'],
  template: '#image-gallery',
  props: {
    componentId: {
      type: String,
      required: true
    }
  },

  data() {
    return {
      images: [],
      modalElement: null,
      isLoading: true,
      uploading: false
    };
  },

  mounted() {
    this.modalElement = this.$el.querySelector('#image-gallery-modal');
    $(this.modalElement).on('hidden.bs.modal', this.closeGallery);
    this.openGallery();
  },

  methods: {
    getImages() {
      let url = '/admin/image',
          _self = this;

      $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
          _self.images = JSON.parse(response);
          _self.isLoading = false;
        },
        error: function (response) {
          console.log(response);
        }
      });
    },

    openGallery() {
      $(this.modalElement).modal('show');
      this.getImages();
    },

    closeGallery() {
      $(this.modalElement).modal('hide');
      EventManager.fire('remove-component', this.componentId);
    },

    selectImage(publicPath) {
      EventManager.fire('image-gallery.select-image', {
        path: publicPath,
        componentId: this.componentId
      });
      this.closeGallery();
    },

    selectFile() {
      this.$refs.fileInput.click();
    },

    uploadFile() {
      let file = this.$refs.fileInput.files[0],
          _self = this;

      if (!file) {
        return;
      }

      this.uploading = true;
      let formData = new FormData();
      formData.append("file", file);
      $.ajax({
        method: 'post',
        url: '/admin/files/image/create?flash=0',
        data: formData,
        processData: false,
        contentType: false,

        success(response) {
          _self.uploading = false;

          _self.images.unshift(response);
        },

        error(error) {
          _self.uploading = false;
          flash(error['responseJSON'].detail, 'error');
        }

      });
    }

  },
  computed: {
    isUploading() {
      return this.uploading === true;
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/Poll.js":
/*!**************************************!*\
  !*** ./assets/js/components/Poll.js ***!
  \**************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var sortablejs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sortablejs */ "./node_modules/sortablejs/modular/sortable.esm.js");


vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('poll', {
  delimiters: ['[[', ']]'],
  template: '#poll',
  props: {
    pollId: {
      required: true,
      type: String
    },
    locale: {
      required: false,
      type: String,
      default: 'ua'
    }
  },

  data() {
    return {
      poll: {
        questions: []
      },
      newQuestion: {
        type: 'single_answer'
      },
      isAdding: false,
      sortable: null
    };
  },

  created() {
    this.getData();
  },

  mounted() {
    this.initSortable();
  },

  methods: {
    getCreateData() {
      return {
        text: this.newQuestion.text,
        type: this.newQuestion.type,
        pollId: this.poll.id
      };
    },

    getData() {
      let url = `${this.locale === 'ua' ? '' : '/' + this.locale}/admin/polls/${this.pollId}`,
          _self = this;

      $.ajax({
        method: 'GET',
        url: url,

        success(response) {
          _self.poll = response;
        },

        error(error) {
          console.log(error);
        }

      });
    },

    onQuestionDeleted(id) {
      let index = this.poll.questions.findIndex(item => item.id === id);
      this.poll.questions.splice(index, 1);
    },

    createQuestion() {
      let url = `${this.locale === 'ua' ? '' : '/' + this.locale}/admin/questions/new`,
          data = this.getCreateData(),
          _self = this;

      $.ajax({
        method: 'POST',
        data: data,
        url: url,

        success(response) {
          let question = response['payload'];
          question['answers'] = [];
          _self.isAdding = false;

          _self.poll.questions.push(question);

          _self.initSortable();

          _self.resetQuestion();

          flash(response['message'], 'success');
        },

        error(error) {
          console.log(error);
        }

      });
    },

    resetQuestion() {
      this.newQuestion = {
        text: '',
        type: 'single_answer',
        questions: []
      };
    },

    initSortable() {
      let _self = this;

      this.sortable = sortablejs__WEBPACK_IMPORTED_MODULE_1__["default"].create(this.$refs.questions, {
        ghostClass: 'blue-background-during-drag',
        'handle': '.sort-handle',
        animation: 300,
        onSort: function () {
          let data = [];

          _self.$refs.questions.children.forEach(item => data.push(item.getAttribute('data-id')));

          _self.sortQuestions({
            sortedQuestions: data
          });
        }
      });
    },

    sortQuestions(data) {
      let url = `${this.locale === 'ua' ? '' : '/' + this.locale}/admin/polls/${this.poll.id}/sort-questions`;
      $.ajax({
        method: 'POST',
        data: data,
        url: url,

        success(response) {
          flash(response, 'success');
        },

        error(error) {
          console.log(error);
        }

      });
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/PollAnswer.js":
/*!********************************************!*\
  !*** ./assets/js/components/PollAnswer.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('poll-answer', {
  delimiters: ['[[', ']]'],
  template: '#poll-answer',
  props: {
    answer: {
      required: true,
      type: Object
    },
    locale: {
      required: false,
      type: String,
      default: 'ua'
    }
  },

  data() {
    return {
      isEditing: false,
      deleteState: 'initial'
    };
  },

  methods: {
    getSaveData() {
      return {
        text: this.answer.text,
        hasComment: this.answer.hasComment,
        commentHelpText: this.answer.hasComment ? this.answer.commentHelpText : null
      };
    },

    save() {
      let url = `${this.locale === 'ua' ? '' : '/' + this.locale}/admin/answers/${this.answer.id}/update`,
          _self = this,
          data = this.getSaveData();

      $.ajax({
        method: 'POST',
        data: data,
        url: url,

        success(response) {
          _self.isEditing = false;
          flash(response, 'success');
        },

        error(error) {
          _self.isEditing = false;
          flash(error['responseJSON'], 'error');
        }

      });
    },

    deleteAnswer() {
      let url = `${this.locale === 'ua' ? '' : '/' + this.locale}/admin/answers/${this.answer.id}/delete`,
          _self = this;

      $.ajax({
        method: 'POST',
        url: url,

        success(response) {
          _self.isEditing = false;

          _self.changeDeleteState('initial');

          _self.$emit('answerWasDeleted', _self.answer.id);

          flash(response, 'success');
        },

        error(error) {
          _self.isEditing = false;
          flash(error['responseJSON'], 'error');
        }

      });
    },

    changeDeleteState(newState) {
      this.deleteState = newState;
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/PollParticipation.js":
/*!***************************************************!*\
  !*** ./assets/js/components/PollParticipation.js ***!
  \***************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('poll-participation', {
  delimiters: ['[[', ']]'],
  template: '#poll-participation',
  props: {
    locale: {
      required: false,
      type: String,
      default: 'ua'
    }
  },

  data() {
    return {
      activePolls: {},
      activePollsData: [],
      selectedPoll: null,
      show: true
    };
  },

  mounted() {
    const regex = /\/poll\/\d{1}\/complete/g;

    if (window.location.href.match(regex) !== null || window.location.href.indexOf('/en/admin') >= 0) {
      this.show = false;
      return;
    }

    setTimeout(this.getActivePolls, 3000);
  },

  methods: {
    getActivePolls() {
      let url = `/en/poll/active?locale=${this.locale}`,
          _self = this;

      $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
          _self.activePollsData = response;
          _self.activePolls = response.map(item => item.id);

          _self.selectPoll();
        },
        error: function (response) {
          console.log(response);
        }
      });
    },

    selectPoll() {
      let completedPolls = localStorage.getItem('completed_polls') || '',
          notCompletedPolls;

      if (this.activePolls.length === 0) {
        return;
      }

      if (!completedPolls) {
        this.selectedPoll = this.activePolls[0];
        return;
      }

      completedPolls = completedPolls.split('|');
      notCompletedPolls = this.activePolls.filter(poll => completedPolls.findIndex(item => parseInt(item) === poll) < 0);
      this.selectedPoll = notCompletedPolls.pop();
    },

    declinePoll() {
      if (!this.selectedPoll) {
        return;
      }

      let completedPolls = localStorage.getItem('completed_polls') || '';
      completedPolls = completedPolls.split('|');
      completedPolls.push(this.selectedPoll);
      localStorage.setItem('completed_polls', completedPolls.join('|'));
      this.close();
    },

    showPoll() {
      let pollUrl = this.activePollsData.filter(item => item.id === this.selectedPoll).pop().route;
      window.location.href = pollUrl; // window.location.href = `/en/poll/${this.selectedPoll}/complete`;
    },

    close() {
      $('#poll_participation').modal('hide');
    },

    open() {
      if (this.show) {
        $('#poll_participation').modal('show');
      }
    }

  },
  watch: {
    selectedPoll: function (newValue) {
      if (newValue) {
        this.open();
      }
    }
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/PollQuestion.js":
/*!**********************************************!*\
  !*** ./assets/js/components/PollQuestion.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var sortablejs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sortablejs */ "./node_modules/sortablejs/modular/sortable.esm.js");


vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('poll-question', {
  delimiters: ['[[', ']]'],
  template: '#poll-question',
  props: {
    question: {
      required: true,
      type: Object
    },
    locale: {
      required: false,
      type: String,
      default: 'ua'
    }
  },

  data() {
    return {
      isEditing: false,
      isOpen: false,
      isAdding: false,
      deleteState: 'initial',
      newAnswer: {
        hasComment: false
      },
      sortable: null
    };
  },

  methods: {
    getSaveData() {
      return {
        text: this.question.text,
        type: this.question.type
      };
    },

    getCreateData() {
      return {
        text: this.newAnswer.text,
        hasComment: this.newAnswer.hasComment,
        commentHelpText: this.newAnswer.hasComment ? this.newAnswer.commentHelpText : null,
        questionId: this.question.id
      };
    },

    save() {
      let url = `${this.locale === 'ua' ? '' : '/' + this.locale}/admin/questions/${this.question.id}/update`,
          _self = this,
          data = this.getSaveData();

      $.ajax({
        method: 'POST',
        data: data,
        url: url,

        success(response) {
          _self.isEditing = false;
          flash(response, 'success');
        },

        error(error) {
          _self.isEditing = false;
          flash(error['responseJSON'], 'error');
        }

      });
    },

    toggleOpen() {
      this.isOpen = !this.isOpen;
    },

    deleteQuestion() {
      let url = `${this.locale === 'ua' ? '' : '/' + this.locale}/admin/questions/${this.question.id}/delete`,
          _self = this;

      $.ajax({
        method: 'POST',
        url: url,

        success(response) {
          _self.isEditing = false;

          _self.changeDeleteState('initial');

          _self.$emit('questionWasDeleted', _self.question.id);

          flash(response, 'success');
        },

        error(error) {
          _self.changeDeleteState('initial');

          flash(error['responseJSON'], 'error');
        }

      });
    },

    onAnswerDeleted(id) {
      let index = this.question.answers.findIndex(answer => answer.id === id);
      this.question.answers.splice(index, 1);
    },

    changeDeleteState(newState) {
      this.deleteState = newState;
    },

    createAnswer() {
      let url = `${this.locale === 'ua' ? '' : '/' + this.locale}/admin/answers/new`,
          data = this.getCreateData(),
          _self = this;

      $.ajax({
        method: 'POST',
        data: data,
        url: url,

        success(response) {
          _self.isAdding = false;

          _self.question.answers.push(response['payload']);

          _self.resetAnswer();

          flash(response['message'], 'success');
        },

        error(error) {
          console.log(error);
        }

      });
    },

    resetAnswer() {
      this.newAnswer = {
        text: '',
        hasComment: false,
        commentHelpText: '',
        answers: []
      };
    },

    toggleSorting() {
      this.sorting = !this.sorting;
    },

    sortAnswers(data) {
      let url = `${this.locale === 'ua' ? '' : '/' + this.locale}/admin/questions/${this.question.id}/sort-answers`;
      $.ajax({
        method: 'POST',
        data: data,
        url: url,

        success(response) {
          flash(response, 'success');
        },

        error(error) {
          console.log(error);
        }

      });
    }

  },
  computed: {
    showAnswers() {
      return !this.isEditing && this.isOpen;
    }

  },
  watch: {
    showAnswers: function (newValue) {
      let _self = this;

      if (newValue) {
        this.sortable = sortablejs__WEBPACK_IMPORTED_MODULE_1__["default"].create(this.$refs.answers, {
          ghostClass: 'blue-background-during-drag',
          'handle': '.sort-handle',
          animation: 300,
          onSort: function () {
            let data = [];

            _self.$refs.answers.children.forEach(item => data.push(item.getAttribute('data-id')));

            _self.sortAnswers({
              sortedAnswers: data
            });
          }
        });
      } else {
        this.sortable = null;
      }
    }
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/RedirectTimer.js":
/*!***********************************************!*\
  !*** ./assets/js/components/RedirectTimer.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('redirect-timer', {
  delimiters: ['[[', ']]'],
  template: '#redirect-timer',
  props: {
    seconds: {
      required: false,
      type: Number,
      default: 30
    },
    redirectUrl: {
      required: true,
      type: String
    }
  },

  data() {
    return {
      elapsedTime: 0,
      timer: null
    };
  },

  mounted() {
    this.startTimer();
  },

  methods: {
    startTimer() {
      this.timer = setInterval(this.tick, 1000);
    },

    tick() {
      this.elapsedTime++;
    }

  },
  computed: {
    remainingTime() {
      return this.seconds - this.elapsedTime;
    }

  },
  watch: {
    remainingTime(value) {
      if (value === 0) {
        clearInterval(this.timer);
        window.location.href = this.redirectUrl;
      }
    }

  }
});

/***/ }),

/***/ "./assets/js/components/SliderModule.js":
/*!**********************************************!*\
  !*** ./assets/js/components/SliderModule.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! slick-carousel */ "./node_modules/slick-carousel/slick/slick.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! slick-carousel/slick/slick.css */ "./node_modules/slick-carousel/slick/slick.css");
/* harmony import */ var slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var slick_carousel_slick_slick_theme_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! slick-carousel/slick/slick-theme.css */ "./node_modules/slick-carousel/slick/slick-theme.css");
/* harmony import */ var slick_carousel_slick_slick_theme_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(slick_carousel_slick_slick_theme_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var flickity__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! flickity */ "./node_modules/flickity/js/index.js");
/* harmony import */ var flickity__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(flickity__WEBPACK_IMPORTED_MODULE_4__);





vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('slider-module', {
  template: `<div class="carousel" :style="styles"><slot></slot></div>`,
  props: {
    centerPadding: {
      default: '15px',
      type: String,
      required: false
    },
    textColor: {
      default: 'white',
      required: false,
      type: String
    },
    size: {
      default: 'medium',
      required: false,
      type: String
    }
  },

  mounted() {
    this.createStyle();
    new flickity__WEBPACK_IMPORTED_MODULE_4___default.a(this.$el, {
      items: 1,
      wrapAround: true,
      autoPlay: false,
      pageDots: false,
      contain: true,
      setGallerySize: false
    });
  },

  computed: {
    styles() {
      let styles = {};
      styles.color = this.textColor;

      if (this.size === 'small') {
        styles.height = '200px';
      } else if (this.size === 'medium') {
        styles.height = '400px';
      } else {
        styles.height = '600px';
      }

      return styles;
    }

  },
  methods: {
    createStyle() {
      let styleElement = document.createElement('style');
      let css = document.createTextNode(`
            .flickity-button-icon {
                fill: ${this.textColor};
            }
            .flickity-button:hover {
                  background: none;
                  cursor: pointer;
            }
            `);
      styleElement.append(css);
      document.querySelector('head').append(styleElement);
    }

  }
});

/***/ }),

/***/ "./assets/js/components/TelephoneApplicationNotAllowed.js":
/*!****************************************************************!*\
  !*** ./assets/js/components/TelephoneApplicationNotAllowed.js ***!
  \****************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('telephone-application-not-allowed', {
  name: "TelephoneApplicationNotAllowed",
  delimiters: ['[[', ']]'],
  template: '#telephone-application-not-allowed',
  props: {
    show: {
      type: Boolean,
      required: false,
      default: false
    }
  },

  mounted() {
    if (this.show === true) {
      $('#' + this.$refs.modal.$el.getAttribute('id')).modal('show');
    }
  }

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/TranslationItem.js":
/*!*************************************************!*\
  !*** ./assets/js/components/TranslationItem.js ***!
  \*************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('translation-item', {
  delimiters: ['[[', ']]'],
  template: '#translation-item',
  props: {
    id: {
      type: String,
      required: true
    },
    locale: {
      type: String,
      required: true
    },
    domain: {
      type: String,
      required: true
    },
    translationKey: {
      type: String,
      required: true
    },
    translationValue: {
      type: String,
      required: false,
      default: null
    },
    defaultLocale: {
      type: String,
      required: false,
      default: null
    },
    defaultTranslationValue: {
      type: String,
      required: false,
      default: null
    },
    baseUrl: {
      type: String,
      required: false,
      default: ''
    },
    authToken: {
      type: String,
      required: true
    }
  },

  data() {
    return {
      translationItems: [],
      state: 'display',
      value: '',
      oldValue: ''
    };
  },

  mounted() {
    this.value = this.oldValue = this.translationValue;
  },

  methods: {
    save() {
      let _self = this,
          url = `${this.baseUrl}/${this.id}/update`;

      _self.state = 'display';
      $.ajax({
        method: 'POST',
        url: url,
        headers: {
          'X-Auth-Token': this.authToken
        },
        data: {
          value: this.value
        },

        success() {
          _self.oldValue = _self.value;
          _self.state = 'display';
          flash('Translation text was updated.', 'success');
        },

        error(error) {
          _self.state = 'display';
          _self.value = _self.translationValue;
          flash(error['responseJSON'].detail, 'error');
        }

      });
    },

    cancel() {
      this.value = this.oldValue;
      this.state = 'display';
    }

  },
  computed: {
    defaultTranslationText() {
      return `${this.defaultLocale}: ${this.defaultTranslationValue}`;
    }

  },
  watch: {
    state: function (newState) {
      if (newState === 'editing') {
        this.$nextTick(() => {
          this.$refs.editField.focus();
          this.$refs.editField.select();
        });
      }
    }
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/TranslationsList.js":
/*!**************************************************!*\
  !*** ./assets/js/components/TranslationsList.js ***!
  \**************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('translations-list', {
  delimiters: ['[[', ']]'],
  template: '#translations-list',
  props: {
    authToken: {
      type: String,
      required: true
    },
    baseUrl: {
      type: String,
      required: false,
      default: ''
    }
  },

  data() {
    return {
      translationItems: [],
      locales: [],
      domains: [],
      selectedLocale: '',
      selectedDomain: '',
      selectedKey: '',
      selectedValue: '',
      defaultLocale: null,
      isLoading: true,
      timeout: null
    };
  },

  async mounted() {
    await this.getLocales();
    await this.getDomains();
    await this.getTranslations();
  },

  methods: {
    debouncedGetTranslations() {
      if (this.timeout) clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        this.getTranslations();
      }, 300);
    },

    async getLocales() {
      let url = `${this.baseUrl}/locales`,
          _self = this;

      await $.ajax({
        url: url,
        method: 'GET',
        headers: {
          'X-Auth-Token': this.authToken
        },
        success: function (response) {
          _self.locales = response.payload.locales;
          _self.selectedLocale = _self.locales[0];
        },
        error: function (response) {
          console.log(response);
        }
      });
    },

    async getDomains() {
      let url = `${this.baseUrl}/domains`,
          _self = this;

      await $.ajax({
        url: url,
        method: 'GET',
        headers: {
          'X-Auth-Token': this.authToken
        },
        success: function (response) {
          _self.domains = response.payload.domains;
          _self.selectedDomain = _self.domains[0];
        },
        error: function (response) {
          console.log(response);
        }
      });
    },

    async getTranslations() {
      let url = `${this.baseUrl}/list?locale=${this.selectedLocale}&domain=${this.selectedDomain}&key=${this.selectedKey}&value=${this.selectedValue}`,
          _self = this;

      this.isLoading = true;
      $.ajax({
        url: url,
        method: 'GET',
        headers: {
          'X-Auth-Token': this.authToken
        },
        success: function (response) {
          _self.translationItems = response.payload.translations;
          _self.defaultLocale = response.payload.defaultLocale;
          _self.isLoading = false;
        },
        error: function (response) {
          console.log(response);
          _self.isLoading = false;
        }
      });
    },

    clear() {
      this.selectedLocale = this.locales[0];
      this.selectedDomain = this.domains[0];
      this.selectedKey = '';
      this.selectedValue = '';
      this.getTranslations();
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/components/ZopimChat.js":
/*!*******************************************!*\
  !*** ./assets/js/components/ZopimChat.js ***!
  \*******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('zopim-chat', {
  template: '#zopim-chat',
  methods: {
    enableZopim() {
      $zopim.livechat.window.toggle();
    }

  }
});

/***/ }),

/***/ "./assets/js/controllers sync recursive \\.js$":
/*!******************************************!*\
  !*** ./assets/js/controllers sync \.js$ ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./CKEditor/ApplyButton.js": "./assets/js/controllers/CKEditor/ApplyButton.js",
	"./CKEditor/HorizontalRule.js": "./assets/js/controllers/CKEditor/HorizontalRule.js",
	"./CKEditor/InsertApplyButton.js": "./assets/js/controllers/CKEditor/InsertApplyButton.js",
	"./CKEditor/InsertHorizontalRule.js": "./assets/js/controllers/CKEditor/InsertHorizontalRule.js",
	"./CKEditor/InsertImage.js": "./assets/js/controllers/CKEditor/InsertImage.js",
	"./CKEditor/InsertImageViaURL.js": "./assets/js/controllers/CKEditor/InsertImageViaURL.js",
	"./CKEditor/Widgets.js": "./assets/js/controllers/CKEditor/Widgets.js",
	"./CKEditor/WidgetsUI.js": "./assets/js/controllers/CKEditor/WidgetsUI.js",
	"./CustomUploadAdapter.js": "./assets/js/controllers/CustomUploadAdapter.js",
	"./age_field_controller.js": "./assets/js/controllers/age_field_controller.js",
	"./available-payment-instruments_controller.js": "./assets/js/controllers/available-payment-instruments_controller.js",
	"./background_controller.js": "./assets/js/controllers/background_controller.js",
	"./bank_card_number_controller.js": "./assets/js/controllers/bank_card_number_controller.js",
	"./bootstrap_file_upload_controller.js": "./assets/js/controllers/bootstrap_file_upload_controller.js",
	"./bootstrap_multiple_file_upload_controller.js": "./assets/js/controllers/bootstrap_multiple_file_upload_controller.js",
	"./bulletin_controller.js": "./assets/js/controllers/bulletin_controller.js",
	"./captcha_controller.js": "./assets/js/controllers/captcha_controller.js",
	"./car_controller.js": "./assets/js/controllers/car_controller.js",
	"./card_data_consent_controller.js": "./assets/js/controllers/card_data_consent_controller.js",
	"./check-email-confirmed_controller.js": "./assets/js/controllers/check-email-confirmed_controller.js",
	"./ckeditor_controller.js": "./assets/js/controllers/ckeditor_controller.js",
	"./clipboard_controller.js": "./assets/js/controllers/clipboard_controller.js",
	"./credit_application_form_controller.js": "./assets/js/controllers/credit_application_form_controller.js",
	"./credit_controller.js": "./assets/js/controllers/credit_controller.js",
	"./credit_parameters_element_controller.js": "./assets/js/controllers/credit_parameters_element_controller.js",
	"./credit_parameters_field_controller.js": "./assets/js/controllers/credit_parameters_field_controller.js",
	"./current_address_controller.js": "./assets/js/controllers/current_address_controller.js",
	"./current_address_section_controller.js": "./assets/js/controllers/current_address_section_controller.js",
	"./expandable_controller.js": "./assets/js/controllers/expandable_controller.js",
	"./expiration_date_controller.js": "./assets/js/controllers/expiration_date_controller.js",
	"./faq_controller.js": "./assets/js/controllers/faq_controller.js",
	"./header_module_controller.js": "./assets/js/controllers/header_module_controller.js",
	"./iban_controller.js": "./assets/js/controllers/iban_controller.js",
	"./image_gallery_controller.js": "./assets/js/controllers/image_gallery_controller.js",
	"./job_opening_controller.js": "./assets/js/controllers/job_opening_controller.js",
	"./locale_controller.js": "./assets/js/controllers/locale_controller.js",
	"./master_field_controller.js": "./assets/js/controllers/master_field_controller.js",
	"./message-unread-count_controller.js": "./assets/js/controllers/message-unread-count_controller.js",
	"./message_controller.js": "./assets/js/controllers/message_controller.js",
	"./online_application_form_controller.js": "./assets/js/controllers/online_application_form_controller.js",
	"./page_selector_controller.js": "./assets/js/controllers/page_selector_controller.js",
	"./passport_type_controller.js": "./assets/js/controllers/passport_type_controller.js",
	"./pensioner_documents_controller.js": "./assets/js/controllers/pensioner_documents_controller.js",
	"./phone_number_controller.js": "./assets/js/controllers/phone_number_controller.js",
	"./pickr_controller.js": "./assets/js/controllers/pickr_controller.js",
	"./pin_controller.js": "./assets/js/controllers/pin_controller.js",
	"./poll_controller.js": "./assets/js/controllers/poll_controller.js",
	"./query_controller.js": "./assets/js/controllers/query_controller.js",
	"./salary_controller.js": "./assets/js/controllers/salary_controller.js",
	"./scroll_to_element_controller.js": "./assets/js/controllers/scroll_to_element_controller.js",
	"./scroll_to_invalid_form_control_controller.js": "./assets/js/controllers/scroll_to_invalid_form_control_controller.js",
	"./show_hide_file_upload_section_controller.js": "./assets/js/controllers/show_hide_file_upload_section_controller.js",
	"./show_hide_repayment_plan_controller.js": "./assets/js/controllers/show_hide_repayment_plan_controller.js",
	"./sort_links_controller.js": "./assets/js/controllers/sort_links_controller.js",
	"./sortable_controller.js": "./assets/js/controllers/sortable_controller.js",
	"./toggle_file_upload_section_controller.js": "./assets/js/controllers/toggle_file_upload_section_controller.js",
	"./toggle_password_controller.js": "./assets/js/controllers/toggle_password_controller.js",
	"./toggle_repayment_plan_controller.js": "./assets/js/controllers/toggle_repayment_plan_controller.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./assets/js/controllers sync recursive \\.js$";

/***/ }),

/***/ "./assets/js/controllers/CKEditor/ApplyButton.js":
/*!*******************************************************!*\
  !*** ./assets/js/controllers/CKEditor/ApplyButton.js ***!
  \*******************************************************/
/*! exports provided: ApplyButtonUI, ApplyButtonEditing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplyButtonUI", function() { return ApplyButtonUI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplyButtonEditing", function() { return ApplyButtonEditing; });
/* harmony import */ var _ckeditor_ckeditor5_widget_src_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ckeditor/ckeditor5-widget/src/utils */ "./node_modules/@ckeditor/ckeditor5-widget/src/utils.js");
/* harmony import */ var _ckeditor_ckeditor5_widget_src_widget__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ckeditor/ckeditor5-widget/src/widget */ "./node_modules/@ckeditor/ckeditor5-widget/src/widget.js");
/* harmony import */ var _ckeditor_ckeditor5_ui_src_button_buttonview__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ckeditor/ckeditor5-ui/src/button/buttonview */ "./node_modules/@ckeditor/ckeditor5-ui/src/button/buttonview.js");
/* harmony import */ var _InsertApplyButton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./InsertApplyButton */ "./assets/js/controllers/CKEditor/InsertApplyButton.js");




class ApplyButtonUI {
  constructor(editor) {
    this.editor = editor;
  }

  init() {
    const editor = this.editor; // The "applyButton" button must be registered among the UI components of the editor
    // to be displayed in the toolbar.

    editor.ui.componentFactory.add('applyButton', locale => {
      // The button will be an instance of ButtonView.
      const buttonView = new _ckeditor_ckeditor5_ui_src_button_buttonview__WEBPACK_IMPORTED_MODULE_2__["default"](locale);
      buttonView.set({
        label: 'Apply Button',
        withText: true,
        tooltip: true
      });
      buttonView.on('execute', () => {
        this.editor.execute('insertApplyButton');
      });
      return buttonView;
    });
  }

}
class ApplyButtonEditing {
  constructor(editor) {
    this.editor = editor;
  }

  static get requires() {
    return [_ckeditor_ckeditor5_widget_src_widget__WEBPACK_IMPORTED_MODULE_1__["default"]];
  }

  init() {
    this._defineSchema();

    this._defineConverters();

    this.editor.commands.add('insertApplyButton', new _InsertApplyButton__WEBPACK_IMPORTED_MODULE_3__["default"](this.editor));
  }

  _defineSchema() {
    const schema = this.editor.model.schema;
    schema.register('applyButton', {
      // Behaves like a self-contained object (e.g. an image).
      isObject: true,
      // Allow in places where other blocks are allowed (e.g. directly in the root).
      allowWhere: '$block',
      allowAttributes: ['linkHref', 'buttonText']
    });
  }

  _defineConverters() {
    const conversion = this.editor.conversion; // <applyButton> converters

    conversion.for('upcast').elementToElement({
      view: {
        name: 'div',
        classes: ['applyButton']
      },
      model: (viewElement, modelWriter) => {
        const linkHref = viewElement.getChild(0).getAttribute('href');

        const buttonText = viewElement.getChild(0).getChild(0)._textData;

        return modelWriter.createElement('applyButton', {
          linkHref,
          buttonText
        });
      }
    });
    conversion.for('editingDowncast').elementToElement({
      model: 'applyButton',
      view: (modelItem, viewWriter) => {
        const widgetElement = createApplyButtonView(modelItem, viewWriter); // Enable widget handling on a placeholder element inside the editing view.

        return Object(_ckeditor_ckeditor5_widget_src_utils__WEBPACK_IMPORTED_MODULE_0__["toWidget"])(widgetElement, viewWriter);
      }
    });
    conversion.for('dataDowncast').elementToElement({
      model: 'applyButton',
      view: createApplyButtonView
    }); // Helper method for both downcast converters.

    function createApplyButtonView(modelItem, viewWriter) {
      const linkHref = modelItem.getAttribute('linkHref');
      const buttonText = modelItem.getAttribute('buttonText') ? modelItem.getAttribute('buttonText') : 'Подати заявку'; // Create the container div

      const applyButtonView = viewWriter.createContainerElement('div', {
        class: 'applyButton'
      }); // Create the button

      const theButton = viewWriter.createContainerElement('a', {
        class: 'btn btn-outline-danger',
        href: linkHref
      }); // Insert the button's text.

      const innerText = viewWriter.createText(buttonText);
      viewWriter.insert(viewWriter.createPositionAt(theButton, 0), innerText); // Insert the button

      viewWriter.insert(viewWriter.createPositionAt(applyButtonView, 0), theButton);
      return applyButtonView;
    }
  }

}

/***/ }),

/***/ "./assets/js/controllers/CKEditor/HorizontalRule.js":
/*!**********************************************************!*\
  !*** ./assets/js/controllers/CKEditor/HorizontalRule.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HorizontalRule; });
/* harmony import */ var _ckeditor_ckeditor5_widget_src_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ckeditor/ckeditor5-widget/src/utils */ "./node_modules/@ckeditor/ckeditor5-widget/src/utils.js");
/* harmony import */ var _ckeditor_ckeditor5_ui_src_button_buttonview__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ckeditor/ckeditor5-ui/src/button/buttonview */ "./node_modules/@ckeditor/ckeditor5-ui/src/button/buttonview.js");
/* harmony import */ var _InsertHorizontalRule__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./InsertHorizontalRule */ "./assets/js/controllers/CKEditor/InsertHorizontalRule.js");



class HorizontalRule {
  constructor(editor) {
    this.editor = editor;
  }

  init() {
    const editor = this.editor;
    const model = editor.model;
    const conversion = editor.conversion;
    const schema = model.schema;
    this.editor.commands.add('insertHorizontalRule', new _InsertHorizontalRule__WEBPACK_IMPORTED_MODULE_2__["default"](this.editor)); // Configure new horizontal rule schema

    schema.register('horizontalRule', {
      allowIn: '$root',
      isObject: true,
      isBlock: true
    });
    conversion.for('upcast').elementToElement({
      view: {
        name: 'hr',
        classes: ['horizontalRule']
      },
      model: (viewElement, modelWriter) => {
        return modelWriter.createElement('horizontalRule');
      }
    });
    conversion.for('editingDowncast').elementToElement({
      model: 'horizontalRule',
      view: (modelItem, viewWriter) => {
        const widgetElement = createHorizontalRuleView(modelItem, viewWriter); // Enable widget handling on a placeholder element inside the editing view.

        return Object(_ckeditor_ckeditor5_widget_src_utils__WEBPACK_IMPORTED_MODULE_0__["toWidget"])(widgetElement, viewWriter);
      }
    });
    conversion.for('dataDowncast').elementToElement({
      model: 'horizontalRule',
      view: createHorizontalRuleView
    }); // Helper method for both downcast converters.

    function createHorizontalRuleView(modelItem, viewWriter) {
      return viewWriter.createContainerElement('hr', {
        class: 'horizontalRule'
      });
    }

    editor.ui.componentFactory.add('horizontalRule', locale => {
      const view = new _ckeditor_ckeditor5_ui_src_button_buttonview__WEBPACK_IMPORTED_MODULE_1__["default"](locale);
      view.set({
        label: 'Horizontal Line',
        withText: true,
        tooltip: true
      }); // Callback executed once the button is clicked.

      view.on('execute', () => {
        this.editor.execute('insertHorizontalRule');
      });
      return view;
    });
  }

}

/***/ }),

/***/ "./assets/js/controllers/CKEditor/InsertApplyButton.js":
/*!*************************************************************!*\
  !*** ./assets/js/controllers/CKEditor/InsertApplyButton.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return InsertApplyButton; });
class InsertApplyButton {
  constructor(editor) {
    this.editor = editor;
  }

  execute() {
    const linkHref = prompt('Button URL');
    const buttonText = prompt('Button text', 'Подати заявку');
    this.editor.model.change(writer => {
      const buttonElement = writer.createElement('applyButton', {
        linkHref: linkHref,
        buttonText: buttonText
      }); // Insert the image in the current selection location.

      this.editor.model.insertContent(buttonElement, this.editor.model.document.selection);
    });
  }

  refresh() {}

}

/***/ }),

/***/ "./assets/js/controllers/CKEditor/InsertHorizontalRule.js":
/*!****************************************************************!*\
  !*** ./assets/js/controllers/CKEditor/InsertHorizontalRule.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return InsertHorizontalRule; });
class InsertHorizontalRule {
  constructor(editor) {
    this.editor = editor;
  }

  execute() {
    this.editor.model.change(writer => {
      const horizontalRuleElement = writer.createElement('horizontalRule'); // Insert the hr at the end of the selection

      writer.insert(horizontalRuleElement, this.editor.model.document.selection.focus);
      writer.setSelection(horizontalRuleElement, 'after');
    });
  }

  refresh() {}

}

/***/ }),

/***/ "./assets/js/controllers/CKEditor/InsertImage.js":
/*!*******************************************************!*\
  !*** ./assets/js/controllers/CKEditor/InsertImage.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return InsertImage; });
/* harmony import */ var _ckeditor_ckeditor5_ui_src_button_buttonview__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ckeditor/ckeditor5-ui/src/button/buttonview */ "./node_modules/@ckeditor/ckeditor5-ui/src/button/buttonview.js");
/* harmony import */ var _ckeditor_ckeditor5_core_theme_icons_image_svg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ckeditor/ckeditor5-core/theme/icons/image.svg */ "./node_modules/@ckeditor/ckeditor5-core/theme/icons/image.svg");
/* harmony import */ var _InsertImageViaURL__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./InsertImageViaURL */ "./assets/js/controllers/CKEditor/InsertImageViaURL.js");



class InsertImage {
  constructor(editor) {
    this.editor = editor;
  }

  init() {
    const editor = this.editor;
    this.editor.commands.add('insertImageViaURL', new _InsertImageViaURL__WEBPACK_IMPORTED_MODULE_2__["default"](this.editor));
    editor.ui.componentFactory.add('insertImage', locale => {
      const view = new _ckeditor_ckeditor5_ui_src_button_buttonview__WEBPACK_IMPORTED_MODULE_0__["default"](locale);
      view.set({
        label: 'Insert image',
        icon: _ckeditor_ckeditor5_core_theme_icons_image_svg__WEBPACK_IMPORTED_MODULE_1__["default"],
        tooltip: true
      }); // Callback executed once the image is clicked.

      view.on('execute', () => {
        const imageUrl = prompt('Image URL');
        editor.model.change(writer => {
          const imageElement = writer.createElement('image', {
            src: imageUrl
          }); // Insert the image in the current selection location.

          editor.model.insertContent(imageElement, editor.model.document.selection);
        });
      });
      return view;
    });
  }

}

/***/ }),

/***/ "./assets/js/controllers/CKEditor/InsertImageViaURL.js":
/*!*************************************************************!*\
  !*** ./assets/js/controllers/CKEditor/InsertImageViaURL.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return InsertImageViaURL; });
class InsertImageViaURL {
  constructor(editor) {
    this.editor = editor;
  }

  execute() {
    const url = prompt('Image URL');
    this.editor.model.change(writer => {
      const imageElement = writer.createElement('image', {
        src: url
      }); // Insert the image in the current selection location.

      this.editor.model.insertContent(imageElement, this.editor.model.document.selection);
    });
  }

  refresh() {}

}

/***/ }),

/***/ "./assets/js/controllers/CKEditor/Widgets.js":
/*!***************************************************!*\
  !*** ./assets/js/controllers/CKEditor/Widgets.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Widgets; });
/* harmony import */ var _ApplyButton__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ApplyButton */ "./assets/js/controllers/CKEditor/ApplyButton.js");
/* harmony import */ var _HorizontalRule__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HorizontalRule */ "./assets/js/controllers/CKEditor/HorizontalRule.js");
/* harmony import */ var _WidgetsUI__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WidgetsUI */ "./assets/js/controllers/CKEditor/WidgetsUI.js");



class Widgets {
  constructor(editor) {
    this.editor = editor;
  }

  static get requires() {
    return [_ApplyButton__WEBPACK_IMPORTED_MODULE_0__["ApplyButtonEditing"], _HorizontalRule__WEBPACK_IMPORTED_MODULE_1__["default"], _ApplyButton__WEBPACK_IMPORTED_MODULE_0__["ApplyButtonUI"], _WidgetsUI__WEBPACK_IMPORTED_MODULE_2__["default"]];
  }

}

/***/ }),

/***/ "./assets/js/controllers/CKEditor/WidgetsUI.js":
/*!*****************************************************!*\
  !*** ./assets/js/controllers/CKEditor/WidgetsUI.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return WidgetsUI; });
/* harmony import */ var _ckeditor_ckeditor5_ui_src_dropdown_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ckeditor/ckeditor5-ui/src/dropdown/utils */ "./node_modules/@ckeditor/ckeditor5-ui/src/dropdown/utils.js");
/* harmony import */ var _ckeditor_ckeditor5_utils_src_collection__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ckeditor/ckeditor5-utils/src/collection */ "./node_modules/@ckeditor/ckeditor5-utils/src/collection.js");
/* harmony import */ var _ckeditor_ckeditor5_ui_src_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ckeditor/ckeditor5-ui/src/model */ "./node_modules/@ckeditor/ckeditor5-ui/src/model.js");



class WidgetsUI {
  constructor(editor) {
    this.editor = editor;
  }

  init() {
    const editor = this.editor; // The "placeholder" dropdown must be registered among the UI components of the editor
    // to be displayed in the toolbar.

    editor.ui.componentFactory.add('widgets', locale => {
      const dropdownView = Object(_ckeditor_ckeditor5_ui_src_dropdown_utils__WEBPACK_IMPORTED_MODULE_0__["createDropdown"])(locale); // Populate the list in the dropdown with items.

      Object(_ckeditor_ckeditor5_ui_src_dropdown_utils__WEBPACK_IMPORTED_MODULE_0__["addListToDropdown"])(dropdownView, getDropdownItemsDefinitions());
      dropdownView.buttonView.set({
        label: 'Widgets',
        tooltip: true,
        withText: true
      }); // Execute the command when the dropdown item is clicked (executed).

      dropdownView.on('execute', evt => {
        editor.execute(evt.source.commandParam);
        editor.editing.view.focus();
      });
      return dropdownView;
    });
  }

}

function getDropdownItemsDefinitions() {
  const itemDefinitions = new _ckeditor_ckeditor5_utils_src_collection__WEBPACK_IMPORTED_MODULE_1__["default"]();
  const imageDefinition = {
    type: 'button',
    model: new _ckeditor_ckeditor5_ui_src_model__WEBPACK_IMPORTED_MODULE_2__["default"]({
      commandParam: 'insertImageViaURL',
      label: 'Image via URL',
      withText: true
    })
  }; // Add the item definition to the collection.

  itemDefinitions.add(imageDefinition);
  const applyButtonDefinition = {
    type: 'button',
    model: new _ckeditor_ckeditor5_ui_src_model__WEBPACK_IMPORTED_MODULE_2__["default"]({
      commandParam: 'insertApplyButton',
      label: 'Apply Button',
      withText: true
    })
  }; // Add the item definition to the collection.

  itemDefinitions.add(applyButtonDefinition);
  const horizontalRuleDefinition = {
    type: 'button',
    model: new _ckeditor_ckeditor5_ui_src_model__WEBPACK_IMPORTED_MODULE_2__["default"]({
      commandParam: 'insertHorizontalRule',
      label: 'Horizontal Line',
      withText: true
    })
  }; // Add the item definition to the collection.

  itemDefinitions.add(horizontalRuleDefinition);
  return itemDefinitions;
}

/***/ }),

/***/ "./assets/js/controllers/CustomUploadAdapter.js":
/*!******************************************************!*\
  !*** ./assets/js/controllers/CustomUploadAdapter.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CustomUploadAdapter; });
class CustomUploadAdapter {
  constructor(loader, uploadUrl) {
    // CKEditor 5's FileLoader instance.
    this.loader = loader;
    this.xhr = null;
    this.uploadUrl = uploadUrl;
  } // Starts the upload process.


  upload() {
    let url = this.uploadUrl,
        self = this;
    return this.loader.file.then(uploadedFile => {
      return new Promise((resolve, reject) => {
        const data = new FormData();
        data.append('upload', uploadedFile);
        self.xhr = $.ajax({
          url: url,
          data,
          contentType: false,
          processData: false,
          type: 'POST',
          success: response => {
            resolve({
              default: response.url
            });
          },
          error: response => {
            let data = response.responseJSON;
            reject(data && data.error ? data.error.message : 'Upload failed.');
          }
        });
      });
    });
  } // Aborts the upload process.


  abort() {
    if (this.xhr) {
      this.xhr.abort();
    }
  }

}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/controllers/age_field_controller.js":
/*!*******************************************************!*\
  !*** ./assets/js/controllers/age_field_controller.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('pin-updated', data => {
      if (data.value >= this.ageLimit) {
        this.element.classList.remove('d-none');
      } else {
        this.element.classList.add('d-none');
      }
    });
  }

  get ageLimit() {
    return parseInt(this.element.dataset.ageLimit);
  }

});

/***/ }),

/***/ "./assets/js/controllers/available-payment-instruments_controller.js":
/*!***************************************************************************!*\
  !*** ./assets/js/controllers/available-payment-instruments_controller.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('master-field-changed', data => {
      if (data.propertyName === this.propertyName) {
        if (data.value === '3e7115f4-eb2e-42b2-8df2-e5c5ed34fad3') {
          // Bank account
          this.element.setAttribute('required', 'required');
        } else {
          this.element.removeAttribute('required');
        }
      }
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

});

/***/ }),

/***/ "./assets/js/controllers/background_controller.js":
/*!********************************************************!*\
  !*** ./assets/js/controllers/background_controller.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    this.id = this.element.getAttribute('id');
    this.createStyleTag(this.data.get('image'));
  }

  createStyleTag() {
    let styleElement = document.createElement('style');
    let css = document.createTextNode(`
            ${this.imageCSS}
            ${this.imageMobileCSS}
        `);
    styleElement.append(css);
    document.querySelector('head').append(styleElement);
  }

  get imageCSS() {
    if (this.data.has('image')) {
      return `
            #${this.id} {
                background-image: url(${this.data.get('image')});
                background-size: cover;
                background-position: center center;
                background-repeat: no-repeat;
            }
            `;
    }

    return '';
  }

  get imageMobileCSS() {
    if (this.data.has('imageMobile')) {
      return `
            @media(max-width:576px) {
                #${this.id} {
                    background-image: url(${this.data.get('imageMobile')});
                    background-size: cover;
                    background-position: center center;
                    background-repeat: no-repeat;
                }
            }
            `;
    }

    return '';
  }

});

/***/ }),

/***/ "./assets/js/controllers/bank_card_number_controller.js":
/*!**************************************************************!*\
  !*** ./assets/js/controllers/bank_card_number_controller.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('master-field-changed', data => {
      if (data.propertyName === this.propertyName) {
        if (data.value === '3e7115f4-eb2e-42b2-8df2-e5c5ed34fad3') {
          // Bank account
          this.element.classList.remove('d-none');
        } else {
          this.element.classList.add('d-none');
          this.element.querySelectorAll('select').forEach(element => element.value = '');
        }
      }
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

});

/***/ }),

/***/ "./assets/js/controllers/bootstrap_file_upload_controller.js":
/*!*******************************************************************!*\
  !*** ./assets/js/controllers/bootstrap_file_upload_controller.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    let label = this.element.parentNode.querySelector('.custom-file-label');
    label.setAttribute('data-browse', this.element.getAttribute('data-browse-button'));
  }

  change() {
    let label = this.element.parentNode.querySelector('.custom-file-label');

    if (this.element.files.length > 0) {
      label.innerHTML = "<span>" + this.element.files[0].name + "</span>";
    } else {
      label.innerHTML = "<span></span>";
    }
  }

});

/***/ }),

/***/ "./assets/js/controllers/bootstrap_multiple_file_upload_controller.js":
/*!****************************************************************************!*\
  !*** ./assets/js/controllers/bootstrap_multiple_file_upload_controller.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    let label = this.element.parentNode.querySelector('.custom-file-label');
    label.setAttribute('data-browse', this.element.getAttribute('data-browse-button'));
  }

  change() {
    let label = this.element.parentNode.querySelector('.custom-file-label'),
        fileNames = [];
    this.element.files.forEach(file => {
      fileNames.push(file.name);
    });
    let fileNamesString = fileNames.join(', ');
    label.innerHTML = "<span>" + fileNamesString + "</span>";
  }

});

/***/ }),

/***/ "./assets/js/controllers/bulletin_controller.js":
/*!******************************************************!*\
  !*** ./assets/js/controllers/bulletin_controller.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _class; });
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


class _class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  subscribe(event) {
    let email = this.emailTarget.value,
        url = this.data.get('subscribeUrl'),
        _self = this;

    event.preventDefault();
    $.ajax({
      method: 'POST',
      url: url,
      data: {
        'email': email
      },

      success(response) {
        flash(response, 'success');
        _self.emailTarget.value = '';
      },

      error(error) {
        flash(error['responseJSON'], 'error');
      }

    });
  }

}

_defineProperty(_class, "targets", ['email']);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/controllers/captcha_controller.js":
/*!*****************************************************!*\
  !*** ./assets/js/controllers/captcha_controller.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    let apiKey = this.element.getAttribute('data-api-key');
    let capthaInput = this.element;
    grecaptcha.ready(function () {
      grecaptcha.execute(apiKey, {
        action: 'homepage'
      }).then(function (token) {
        capthaInput.value = token;
      });
    });
  }

});

/***/ }),

/***/ "./assets/js/controllers/car_controller.js":
/*!*************************************************!*\
  !*** ./assets/js/controllers/car_controller.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('master-field-changed', data => {
      if (data.propertyName === this.propertyName) {
        if (data.value === '1') {
          this.element.classList.remove('d-none');
        } else {
          this.element.classList.add('d-none');
        }
      }
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

});

/***/ }),

/***/ "./assets/js/controllers/card_data_consent_controller.js":
/*!***************************************************************!*\
  !*** ./assets/js/controllers/card_data_consent_controller.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('master-field-changed', data => {
      if (data.propertyName === this.propertyName) {
        if (data.value === '3e7115f4-eb2e-42b2-8df2-e5c5ed34fad3') {
          // Bank account
          this.element.classList.remove('d-none');
          this.element.querySelector('input').setAttribute('required', 'required');
        } else {
          this.element.classList.add('d-none');
          this.element.querySelector('input').removeAttribute('required');
        }
      }
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

});

/***/ }),

/***/ "./assets/js/controllers/check-email-confirmed_controller.js":
/*!*******************************************************************!*\
  !*** ./assets/js/controllers/check-email-confirmed_controller.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    fetch(this.url, {
      method: 'POST'
    });
  }

  get url() {
    return this.element.dataset.url;
  }

});

/***/ }),

/***/ "./assets/js/controllers/ckeditor_controller.js":
/*!******************************************************!*\
  !*** ./assets/js/controllers/ckeditor_controller.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var _ckeditor_ckeditor5_editor_classic_src_classiceditor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ckeditor/ckeditor5-editor-classic/src/classiceditor */ "./node_modules/@ckeditor/ckeditor5-editor-classic/src/classiceditor.js");
/* harmony import */ var _ckeditor_ckeditor5_essentials_src_essentials__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ckeditor/ckeditor5-essentials/src/essentials */ "./node_modules/@ckeditor/ckeditor5-essentials/src/essentials.js");
/* harmony import */ var _ckeditor_ckeditor5_heading_src_heading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ckeditor/ckeditor5-heading/src/heading */ "./node_modules/@ckeditor/ckeditor5-heading/src/heading.js");
/* harmony import */ var _ckeditor_ckeditor5_basic_styles_src_bold__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ckeditor/ckeditor5-basic-styles/src/bold */ "./node_modules/@ckeditor/ckeditor5-basic-styles/src/bold.js");
/* harmony import */ var _ckeditor_ckeditor5_basic_styles_src_italic__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ckeditor/ckeditor5-basic-styles/src/italic */ "./node_modules/@ckeditor/ckeditor5-basic-styles/src/italic.js");
/* harmony import */ var _ckeditor_ckeditor5_link_src_link__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ckeditor/ckeditor5-link/src/link */ "./node_modules/@ckeditor/ckeditor5-link/src/link.js");
/* harmony import */ var _ckeditor_ckeditor5_list_src_list__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ckeditor/ckeditor5-list/src/list */ "./node_modules/@ckeditor/ckeditor5-list/src/list.js");
/* harmony import */ var _ckeditor_ckeditor5_image_src_image__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ckeditor/ckeditor5-image/src/image */ "./node_modules/@ckeditor/ckeditor5-image/src/image.js");
/* harmony import */ var _ckeditor_ckeditor5_image_src_imagetoolbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ckeditor/ckeditor5-image/src/imagetoolbar */ "./node_modules/@ckeditor/ckeditor5-image/src/imagetoolbar.js");
/* harmony import */ var _ckeditor_ckeditor5_image_src_imagecaption__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ckeditor/ckeditor5-image/src/imagecaption */ "./node_modules/@ckeditor/ckeditor5-image/src/imagecaption.js");
/* harmony import */ var _ckeditor_ckeditor5_image_src_imagestyle__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ckeditor/ckeditor5-image/src/imagestyle */ "./node_modules/@ckeditor/ckeditor5-image/src/imagestyle.js");
/* harmony import */ var _ckeditor_ckeditor5_image_src_imageresize__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ckeditor/ckeditor5-image/src/imageresize */ "./node_modules/@ckeditor/ckeditor5-image/src/imageresize.js");
/* harmony import */ var _ckeditor_ckeditor5_image_src_imageupload__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ckeditor/ckeditor5-image/src/imageupload */ "./node_modules/@ckeditor/ckeditor5-image/src/imageupload.js");
/* harmony import */ var _ckeditor_ckeditor5_media_embed_src_mediaembed__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ckeditor/ckeditor5-media-embed/src/mediaembed */ "./node_modules/@ckeditor/ckeditor5-media-embed/src/mediaembed.js");
/* harmony import */ var _ckeditor_ckeditor5_table_src_table__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ckeditor/ckeditor5-table/src/table */ "./node_modules/@ckeditor/ckeditor5-table/src/table.js");
/* harmony import */ var _ckeditor_ckeditor5_block_quote_src_blockquote__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ckeditor/ckeditor5-block-quote/src/blockquote */ "./node_modules/@ckeditor/ckeditor5-block-quote/src/blockquote.js");
/* harmony import */ var _ckeditor_ckeditor5_paragraph_src_paragraph__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ckeditor/ckeditor5-paragraph/src/paragraph */ "./node_modules/@ckeditor/ckeditor5-paragraph/src/paragraph.js");
/* harmony import */ var _ckeditor_ckeditor5_alignment_src_alignment__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ckeditor/ckeditor5-alignment/src/alignment */ "./node_modules/@ckeditor/ckeditor5-alignment/src/alignment.js");
/* harmony import */ var _ckeditor_ckeditor5_font_src_font__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ckeditor/ckeditor5-font/src/font */ "./node_modules/@ckeditor/ckeditor5-font/src/font.js");
/* harmony import */ var _ckeditor_ckeditor5_ckfinder_src_ckfinder__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ckeditor/ckeditor5-ckfinder/src/ckfinder */ "./node_modules/@ckeditor/ckeditor5-ckfinder/src/ckfinder.js");
/* harmony import */ var _ckeditor_ckeditor5_adapter_ckfinder_src_uploadadapter__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter */ "./node_modules/@ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter.js");
/* harmony import */ var _CustomUploadAdapter__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./CustomUploadAdapter */ "./assets/js/controllers/CustomUploadAdapter.js");
/* harmony import */ var _CKEditor_InsertImage__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./CKEditor/InsertImage */ "./assets/js/controllers/CKEditor/InsertImage.js");
/* harmony import */ var _CKEditor_Widgets__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./CKEditor/Widgets */ "./assets/js/controllers/CKEditor/Widgets.js");

























/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  initialize() {
    this.uploadUrl = this.element.getAttribute("data-upload-url");
  }

  connect() {
    _ckeditor_ckeditor5_editor_classic_src_classiceditor__WEBPACK_IMPORTED_MODULE_1__["default"].create(this.element, {
      height: 500,
      plugins: [_ckeditor_ckeditor5_heading_src_heading__WEBPACK_IMPORTED_MODULE_3__["default"], _ckeditor_ckeditor5_essentials_src_essentials__WEBPACK_IMPORTED_MODULE_2__["default"], _ckeditor_ckeditor5_basic_styles_src_bold__WEBPACK_IMPORTED_MODULE_4__["default"], _ckeditor_ckeditor5_basic_styles_src_italic__WEBPACK_IMPORTED_MODULE_5__["default"], _ckeditor_ckeditor5_link_src_link__WEBPACK_IMPORTED_MODULE_6__["default"], _ckeditor_ckeditor5_paragraph_src_paragraph__WEBPACK_IMPORTED_MODULE_17__["default"], _ckeditor_ckeditor5_list_src_list__WEBPACK_IMPORTED_MODULE_7__["default"], _ckeditor_ckeditor5_image_src_image__WEBPACK_IMPORTED_MODULE_8__["default"], _ckeditor_ckeditor5_image_src_imagetoolbar__WEBPACK_IMPORTED_MODULE_9__["default"], _ckeditor_ckeditor5_image_src_imagecaption__WEBPACK_IMPORTED_MODULE_10__["default"], _ckeditor_ckeditor5_image_src_imagestyle__WEBPACK_IMPORTED_MODULE_11__["default"], _ckeditor_ckeditor5_image_src_imageresize__WEBPACK_IMPORTED_MODULE_12__["default"], _ckeditor_ckeditor5_image_src_imageupload__WEBPACK_IMPORTED_MODULE_13__["default"], _CKEditor_InsertImage__WEBPACK_IMPORTED_MODULE_23__["default"], _ckeditor_ckeditor5_media_embed_src_mediaembed__WEBPACK_IMPORTED_MODULE_14__["default"], _ckeditor_ckeditor5_table_src_table__WEBPACK_IMPORTED_MODULE_15__["default"], _ckeditor_ckeditor5_ckfinder_src_ckfinder__WEBPACK_IMPORTED_MODULE_20__["default"], _ckeditor_ckeditor5_adapter_ckfinder_src_uploadadapter__WEBPACK_IMPORTED_MODULE_21__["default"], _ckeditor_ckeditor5_block_quote_src_blockquote__WEBPACK_IMPORTED_MODULE_16__["default"], _ckeditor_ckeditor5_alignment_src_alignment__WEBPACK_IMPORTED_MODULE_18__["default"], _ckeditor_ckeditor5_font_src_font__WEBPACK_IMPORTED_MODULE_19__["default"], _CKEditor_Widgets__WEBPACK_IMPORTED_MODULE_24__["default"]],
      alignment: {
        options: ['left', 'right', 'center', 'justify']
      },
      toolbar: ['heading', '|', 'bold', 'italic', 'alignment', '|', 'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', '|', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'insertTable', 'imageUpload', 'mediaEmbed', '|', 'undo', 'redo', '|', 'widgets'],
      extraPlugins: [myCustomUploadAdapterPlugin],
      customUploadUrl: this.uploadUrl,
      image: {
        toolbar: ['imageTextAlternative', '|', 'imageStyle:alignLeft', 'imageStyle:full', 'imageStyle:alignRight'],
        styles: ['full', 'side', 'alignLeft', 'alignCenter', 'alignRight']
      },
      mediaEmbed: {
        previewsInData: true
      },
      link: {
        decorators: {
          isExternal: {
            mode: 'manual',
            label: 'Open in a new tab',
            attributes: {
              target: '_blank'
            }
          }
        }
      },
      fontFamily: {
        options: ['default', 'Arial, Helvetica, sans-serif', 'Courier New, Courier, monospace', 'Georgia, serif', 'Lucida Sans Unicode, Lucida Grande, sans-serif', 'Tahoma, Geneva, sans-serif', 'Times New Roman, Times, serif', 'Trebuchet MS, Helvetica, sans-serif', 'Verdana, Geneva, sans-serif', 'Ubuntu']
      }
    }).then(editor => {
      console.log('CKEDITOR is ready.');
    }).catch(error => {
      console.error(error);
    });
  }

});

function myCustomUploadAdapterPlugin(editor) {
  let uploadUrl = editor.config.get('customUploadUrl');

  editor.plugins.get('FileRepository').createUploadAdapter = loader => {
    return new _CustomUploadAdapter__WEBPACK_IMPORTED_MODULE_22__["default"](loader, uploadUrl);
  };
}

/***/ }),

/***/ "./assets/js/controllers/clipboard_controller.js":
/*!*******************************************************!*\
  !*** ./assets/js/controllers/clipboard_controller.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _class; });
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


class _class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  copy(event) {
    event.preventDefault();
    let tempInput = document.createElement("input");
    tempInput.style.position = "absolute";
    tempInput.style.left = "-1000px";
    tempInput.style.top = "-1000px";
    tempInput.value = this.text;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
    flash('Copied ' + this.text + ' to the clipboard.', 'success');
  }

  get text() {
    return this.sourceTarget.value;
  }

}

_defineProperty(_class, "targets", ["source"]);

/***/ }),

/***/ "./assets/js/controllers/credit_application_form_controller.js":
/*!*********************************************************************!*\
  !*** ./assets/js/controllers/credit_application_form_controller.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  submitApplication(event) {
    event.preventDefault();

    if (!this.element.querySelector('form').checkValidity()) {
      this.element.querySelector('form').reportValidity();
      return;
    }

    let submitUrl = this.data.get('submitUrl'),
        formData = new FormData(this.element.querySelector('form')),
        _self = this;

    let submitButton = event.target;
    submitButton.setAttribute('disabled', true);
    $.ajax({
      url: submitUrl,
      data: formData,
      method: 'POST',
      processData: false,
      contentType: false,
      success: function (response) {
        if (response.success) {
          if (_self.redirectUrl) {
            window.location.href = _self.redirectUrl;
          } else {
            _self.element.innerHTML = response.content;
          }
        } else {
          _self.element.innerHTML = response.content;
        }
      },
      error: function (error) {
        submitButton.removeAttribute('disabled');
        console.log(error);
      }
    });
  }

  get redirectUrl() {
    return this.data.get('redirectUrl');
  }

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/controllers/credit_controller.js":
/*!****************************************************!*\
  !*** ./assets/js/controllers/credit_controller.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    this.element.scrollIntoView({
      behavior: "smooth",
      block: "center"
    });
  }

});

/***/ }),

/***/ "./assets/js/controllers/credit_parameters_element_controller.js":
/*!***********************************************************************!*\
  !*** ./assets/js/controllers/credit_parameters_element_controller.js ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('credit-parameters-changed', data => {
      this.element.innerHTML = data[this.propertyName];
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

});

/***/ }),

/***/ "./assets/js/controllers/credit_parameters_field_controller.js":
/*!*********************************************************************!*\
  !*** ./assets/js/controllers/credit_parameters_field_controller.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('credit-parameters-changed', data => {
      this.element.value = data[this.propertyName];
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

});

/***/ }),

/***/ "./assets/js/controllers/current_address_controller.js":
/*!*************************************************************!*\
  !*** ./assets/js/controllers/current_address_controller.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    this.radioButtons = this.element.querySelectorAll('input');
    this.radioButtons.forEach(element => {
      element.addEventListener('change', this.toggle);
    });
  }

  toggle() {
    EventManager.fire('current-address-is-same-as-permanent', {
      isSame: this.value === '1'
    });
  }

});

/***/ }),

/***/ "./assets/js/controllers/current_address_section_controller.js":
/*!*********************************************************************!*\
  !*** ./assets/js/controllers/current_address_section_controller.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    if (this.isSameInitially) {
      this.element.classList.add('d-none');
    } else {
      this.element.classList.remove('d-none');
    }

    EventManager.listen('current-address-is-same-as-permanent', data => {
      if (data.isSame) {
        this.element.classList.add('d-none');
      } else {
        this.element.classList.remove('d-none');
      }
    });
  }

  get isSameInitially() {
    return this.element.dataset.isSame === '1';
  }

});

/***/ }),

/***/ "./assets/js/controllers/expandable_controller.js":
/*!********************************************************!*\
  !*** ./assets/js/controllers/expandable_controller.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _class; });
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");


function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


class _class extends stimulus__WEBPACK_IMPORTED_MODULE_1__["Controller"] {
  connect() {
    let title = this.data.get('title');

    if (title) {
      let currentUrl = window.location.href,
          parts = currentUrl.split('#'),
          fragment = null;

      if (parts.length > 1) {
        fragment = decodeURI(parts[parts.length - 1]).toLowerCase().replace(/ /g, '-');

        if (fragment === title.toLocaleLowerCase().replace(/ /g, '-')) {
          this.toggleContent();
          this.element.scrollIntoView({
            behavior: "smooth",
            block: "nearest"
          });
        }
      }
    }
  }

  toggleContent() {
    this.arrowTarget.classList.toggle('fa-angle-right');
    this.arrowTarget.classList.toggle('fa-angle-down');
    this.contentTarget.classList.toggle('not-visible');
  }

}

_defineProperty(_class, "targets", ['arrow', 'content']);

/***/ }),

/***/ "./assets/js/controllers/expiration_date_controller.js":
/*!*************************************************************!*\
  !*** ./assets/js/controllers/expiration_date_controller.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('master-field-changed', data => {
      if (data.propertyName === this.propertyName) {
        if (data.value === 'Fixed-term contract') {
          this.element.classList.remove('d-none');
        } else {
          this.element.classList.add('d-none');
        }
      }
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

});

/***/ }),

/***/ "./assets/js/controllers/faq_controller.js":
/*!*************************************************!*\
  !*** ./assets/js/controllers/faq_controller.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _class; });
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


class _class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    if (this.data.has('open')) {
      this.toggleAnswer();
      this.element.scrollIntoView({
        behavior: "smooth",
        block: "center"
      });
    }
  }

  toggleAnswer() {
    this.arrowTarget.classList.toggle('fa-angle-right');
    this.arrowTarget.classList.toggle('fa-angle-down');
    this.answerTarget.classList.toggle('not-visible');
  }

}

_defineProperty(_class, "targets", ['arrow', 'answer']);

/***/ }),

/***/ "./assets/js/controllers/header_module_controller.js":
/*!***********************************************************!*\
  !*** ./assets/js/controllers/header_module_controller.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  toggleBackgroundOpacity() {
    this.element.classList.toggle('bg-white');
  }

});

/***/ }),

/***/ "./assets/js/controllers/iban_controller.js":
/*!**************************************************!*\
  !*** ./assets/js/controllers/iban_controller.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('master-field-changed', data => {
      if (data.propertyName === this.propertyName) {
        if (data.value === '3e7115f4-eb2e-42b2-8df2-e5c5ed34fad3') {
          // Bank account
          this.element.classList.remove('d-none');
        } else {
          this.element.classList.add('d-none');
        }
      }
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

});

/***/ }),

/***/ "./assets/js/controllers/image_gallery_controller.js":
/*!***********************************************************!*\
  !*** ./assets/js/controllers/image_gallery_controller.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    this.componentId = this.element.getAttribute('id');
    this.element.addEventListener('click', this.showImageGallery);
    EventManager.listen('image-gallery.select-image', payload => {
      if (payload.componentId === this.componentId) {
        this.element.value = payload.path;
      }
    });
  }

  showImageGallery() {
    EventManager.fire('add-component', {
      type: 'image-gallery',
      id: this.id
    });
  }

});

/***/ }),

/***/ "./assets/js/controllers/job_opening_controller.js":
/*!*********************************************************!*\
  !*** ./assets/js/controllers/job_opening_controller.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _class; });
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


class _class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    if (this.data.get('expanded')) {
      this.toggleInfo();
      this.element.scrollIntoView({
        behavior: "smooth",
        block: "nearest"
      });
    } else {
      this.buttonTextTarget.innerHTML = this.data.get('textDown');
    }
  }

  toggleInfo() {
    this.arrowTarget.classList.toggle('fa-angle-down');
    this.arrowTarget.classList.toggle('fa-angle-up');
    this.fullInfoTarget.classList.toggle('not-visible');
    this.buttonTextTarget.innerHTML = this.textOnButton;
  }

  get isOpen() {
    return this.arrowTarget.classList.contains('fa-angle-up');
  }

  get textOnButton() {
    return this.isOpen ? this.data.get('textUp') : this.data.get('textDown');
  }

}

_defineProperty(_class, "targets", ['fullInfo', 'buttonText', 'arrow']);

/***/ }),

/***/ "./assets/js/controllers/locale_controller.js":
/*!****************************************************!*\
  !*** ./assets/js/controllers/locale_controller.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  switch(event) {
    let url = `/en/admin/locale/${this.url}`;
    event.preventDefault();
    $.ajax({
      method: 'POST',
      url: url,

      success() {
        window.location.href = window.location.href;
      },

      error(error) {
        console.log(error);
      }

    });
  }

  get url() {
    return this.data.get('locale');
  }

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/controllers/master_field_controller.js":
/*!**********************************************************!*\
  !*** ./assets/js/controllers/master_field_controller.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    this.element.addEventListener('change', () => {
      EventManager.fire('master-field-changed', {
        propertyName: this.propertyName,
        value: this.element.value
      });
    });
    $(document).ready(() => {
      EventManager.fire('master-field-changed', {
        propertyName: this.propertyName,
        value: this.element.value
      });
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/controllers/message-unread-count_controller.js":
/*!******************************************************************!*\
  !*** ./assets/js/controllers/message-unread-count_controller.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('customer-message-was-read', () => {
      this.element.innerHTML = this.count - 1;

      if (this.count === 0) {
        this.element.classList.add('d-none');
      }
    });
  }

  get count() {
    return parseInt(this.element.innerHTML);
  }

});

/***/ }),

/***/ "./assets/js/controllers/message_controller.js":
/*!*****************************************************!*\
  !*** ./assets/js/controllers/message_controller.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _class; });
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


class _class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  toggleContent() {
    this.arrowTarget.classList.toggle('fa-angle-right');
    this.arrowTarget.classList.toggle('fa-angle-down');
    this.contentTarget.classList.toggle('not-visible');

    if (this.isUnRead) {
      let _self = this;

      this.titleTarget.classList.remove('text-danger');
      $.ajax({
        method: 'POST',
        url: this.url,

        success() {
          EventManager.fire('customer-message-was-read');
        },

        error(error) {
          console.log(error['responseJSON']);

          _self.titleTarget.classList.add('text-danger');
        }

      });
    }
  }

  get isUnRead() {
    return this.titleTarget.classList.contains('text-danger');
  }

  get url() {
    return this.element.dataset.url;
  }

}

_defineProperty(_class, "targets", ['arrow', 'content', 'title']);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/controllers/online_application_form_controller.js":
/*!*********************************************************************!*\
  !*** ./assets/js/controllers/online_application_form_controller.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");


/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_1__["Controller"] {
  connect() {
    EventManager.listen('credit-parameters-changed', data => {
      if (window.location.href.indexOf('application=') != -1) {
        return;
      }

      let [url] = this.initialActon.split('?'),
          [domain, route, credit, creditDetails, isRefinance] = url.split('/');
      let queryParametersObject = {
        amount: data.amount,
        period: data.period,
        payment: data.payment
      },
          queryString = $.param(queryParametersObject),
          newUrl = `/${route}/${data.slug}/${creditDetails}/${isRefinance}?${queryString}`;
      this.element.setAttribute('action', newUrl);
    });
  }

  get initialActon() {
    return this.element.getAttribute('action');
  }

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/controllers/page_selector_controller.js":
/*!***********************************************************!*\
  !*** ./assets/js/controllers/page_selector_controller.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  redirect() {
    window.location.href = this.element.value;
  }

});

/***/ }),

/***/ "./assets/js/controllers/passport_type_controller.js":
/*!***********************************************************!*\
  !*** ./assets/js/controllers/passport_type_controller.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('master-field-changed', data => {
      if (data.propertyName === this.propertyName) {
        if (data.value === this.propertyValue) {
          this.element.classList.remove('d-none');
        } else {
          this.element.classList.add('d-none');
        }
      }
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

  get propertyValue() {
    return this.element.dataset.propertyValue;
  }

});

/***/ }),

/***/ "./assets/js/controllers/pensioner_documents_controller.js":
/*!*****************************************************************!*\
  !*** ./assets/js/controllers/pensioner_documents_controller.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('credit-parameters-changed', data => {
      if (data.slug === 'pensioner') {
        let asterisk = this.element.querySelector('i');

        if (asterisk !== null) {
          return;
        }

        asterisk = document.createElement('i');
        asterisk.className = 'fa fa-asterisk fa-required';
        this.element.prepend(asterisk);
      } else {
        let asterisk = this.element.querySelector('i');

        if (asterisk) {
          this.element.removeChild(asterisk);
        }
      }
    });
  }

});

/***/ }),

/***/ "./assets/js/controllers/phone_number_controller.js":
/*!**********************************************************!*\
  !*** ./assets/js/controllers/phone_number_controller.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  initialize() {
    this.setPhonePrefix();
  }

  onKeyUp() {
    this.setPhonePrefix();
  }

  setPhonePrefix() {
    if (!this.element.value.startsWith('380')) {
      this.element.value = '380';
    }
  }

});

/***/ }),

/***/ "./assets/js/controllers/pickr_controller.js":
/*!***************************************************!*\
  !*** ./assets/js/controllers/pickr_controller.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var _simonwep_pickr_dist_themes_classic_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @simonwep/pickr/dist/themes/classic.min.css */ "./node_modules/@simonwep/pickr/dist/themes/classic.min.css");
/* harmony import */ var _simonwep_pickr_dist_themes_classic_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_simonwep_pickr_dist_themes_classic_min_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _simonwep_pickr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @simonwep/pickr */ "./node_modules/@simonwep/pickr/dist/pickr.min.js");
/* harmony import */ var _simonwep_pickr__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_simonwep_pickr__WEBPACK_IMPORTED_MODULE_2__);

 // 'classic' theme


/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    const pickr = _simonwep_pickr__WEBPACK_IMPORTED_MODULE_2___default.a.create({
      el: this.element,
      theme: 'classic',
      useAsButton: true,
      default: this.element.value,
      swatches: ['rgba(244, 67, 54, 1)', 'rgba(233, 30, 99, 0.95)', 'rgba(156, 39, 176, 0.9)', 'rgba(103, 58, 183, 0.85)', 'rgba(63, 81, 181, 0.8)', 'rgba(33, 150, 243, 0.75)', 'rgba(3, 169, 244, 0.7)', 'rgba(0, 188, 212, 0.7)', 'rgba(0, 150, 136, 0.75)', 'rgba(76, 175, 80, 0.8)', 'rgba(139, 195, 74, 0.85)', 'rgba(205, 220, 57, 0.9)', 'rgba(255, 235, 59, 0.95)', 'rgba(255, 193, 7, 1)'],
      components: {
        preview: true,
        opacity: true,
        hue: true,
        interaction: {
          hex: true,
          rgba: true,
          hsla: true,
          hsva: true,
          cmyk: true,
          input: true,
          clear: true,
          save: true
        }
      }
    });
    pickr.on('save', (color, instance) => {
      if (color) {
        this.element.value = color.toHEXA();
      } else {
        this.element.value = '';
      }
    });
  }

});

/***/ }),

/***/ "./assets/js/controllers/pin_controller.js":
/*!*************************************************!*\
  !*** ./assets/js/controllers/pin_controller.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    $(document).ready(() => {
      EventManager.fire('pin-updated', {
        value: this.age(this.element.value.substring(0, 5))
      });
    });
  }

  update() {
    if (this.element.value.length < 5) {
      return;
    }

    EventManager.fire('pin-updated', {
      value: this.age(this.element.value.substring(0, 5))
    });
  }

  age(days) {
    if (days.length < 5) {
      return 0;
    }

    let baseDate = new Date(1900, 0, 1);
    let dateOfBirth = new Date(baseDate.valueOf());
    let today = new Date();
    dateOfBirth.setDate(dateOfBirth.getDate() + parseInt(days));
    return Math.floor((today - dateOfBirth) / (1000 * 60 * 60 * 24 * 365));
  }

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/controllers/poll_controller.js":
/*!**************************************************!*\
  !*** ./assets/js/controllers/poll_controller.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var sortablejs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sortablejs */ "./node_modules/sortablejs/modular/sortable.esm.js");



/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_1__["Controller"] {
  submit() {
    if (document.forms.poll.checkValidity() === false) {
      return;
    }

    let completedPolls = localStorage.getItem('completed_polls') || '',
        pollId = this.data.get('pollId');
    completedPolls = completedPolls.split('|');
    completedPolls.push(pollId);
    completedPolls = new Set(completedPolls);
    completedPolls = Array.from(completedPolls);
    localStorage.setItem('completed_polls', completedPolls.join('|'));
  }

});

/***/ }),

/***/ "./assets/js/controllers/query_controller.js":
/*!***************************************************!*\
  !*** ./assets/js/controllers/query_controller.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");


/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_1__["Controller"] {
  initialize() {
    this.parameter = this.data.get("parameter");
  }

  connect() {
    let _self = this;

    this.element.addEventListener('change', function () {
      let currentUrl = window.location.href,
          newUrl = '',
          regex,
          hasExistingValue;

      if (currentUrl.indexOf('?') < 0) {
        newUrl = `${currentUrl}?${_self.parameter}=${_self.element.value}`;
        return window.location.href = newUrl;
      }

      let pageRegex = new RegExp(/(page)=\d+/);
      currentUrl = currentUrl.replace(pageRegex, '$1=1');
      regex = new RegExp(`${_self.parameter}=`);
      hasExistingValue = currentUrl.match(regex);

      if (hasExistingValue) {
        let parts = currentUrl.split('?')[1].split('&');
        parts = parts.map(part => {
          if (part.split('=')[0] === _self.parameter) {
            return `${_self.parameter}=${_self.element.value}`;
          }

          return part;
        });
        newUrl = currentUrl.split('?')[0] + '?' + parts.join('&');
        return window.location.href = newUrl;
      } else {
        newUrl = `${currentUrl}&${_self.parameter}=${_self.element.value}`;
        return window.location.href = newUrl;
      }
    });
  }

});

/***/ }),

/***/ "./assets/js/controllers/salary_controller.js":
/*!****************************************************!*\
  !*** ./assets/js/controllers/salary_controller.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('master-field-changed', data => {
      if (data.propertyName === this.propertyName) {
        if (data.value === 'Bank account' || data.value === 'Cash') {
          this.element.classList.remove('d-none');
        } else {
          this.element.classList.add('d-none');
        }
      }
    });
  }

  get propertyName() {
    return this.element.dataset.propertyName;
  }

});

/***/ }),

/***/ "./assets/js/controllers/scroll_to_element_controller.js":
/*!***************************************************************!*\
  !*** ./assets/js/controllers/scroll_to_element_controller.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _class; });
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


class _class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  scroll() {
    if (this.element.dataset.mobileOnly) {
      let isMobile = window.matchMedia("(max-width: 576px)").matches;

      if (isMobile) {
        this.elementTarget.scrollIntoView({
          behavior: "smooth",
          block: "center"
        });
      }
    } else {
      this.elementTarget.scrollIntoView({
        behavior: "smooth",
        block: "center"
      });
    }
  }

}

_defineProperty(_class, "targets", ['element']);

/***/ }),

/***/ "./assets/js/controllers/scroll_to_invalid_form_control_controller.js":
/*!****************************************************************************!*\
  !*** ./assets/js/controllers/scroll_to_invalid_form_control_controller.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    let invalidControl = this.element.querySelector('.form-control.is-invalid');

    if (!invalidControl) {
      return;
    }

    invalidControl.scrollIntoView({
      behavior: "smooth",
      block: "center"
    });
  }

});

/***/ }),

/***/ "./assets/js/controllers/show_hide_file_upload_section_controller.js":
/*!***************************************************************************!*\
  !*** ./assets/js/controllers/show_hide_file_upload_section_controller.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('toggle-file-upload-section', data => {
      if (this.creditId === data.creditId) {
        if (data.isVisible) {
          this.element.classList.remove('d-none');
        } else {
          this.element.classList.add('d-none');
        }
      }
    });
  }

  get creditId() {
    return this.element.dataset.creditId;
  }

});

/***/ }),

/***/ "./assets/js/controllers/show_hide_repayment_plan_controller.js":
/*!**********************************************************************!*\
  !*** ./assets/js/controllers/show_hide_repayment_plan_controller.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");

/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    EventManager.listen('toggle-repayment-plan', data => {
      if (this.contract === data.contract) {
        if (data.isVisible) {
          this.element.classList.remove('d-none');
        } else {
          this.element.classList.add('d-none');
        }
      }
    });
  }

  get contract() {
    return this.element.dataset.contract;
  }

});

/***/ }),

/***/ "./assets/js/controllers/sort_links_controller.js":
/*!********************************************************!*\
  !*** ./assets/js/controllers/sort_links_controller.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var sortablejs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sortablejs */ "./node_modules/sortablejs/modular/sortable.esm.js");



/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_1__["Controller"] {
  connect() {
    let _self = this;

    sortablejs__WEBPACK_IMPORTED_MODULE_2__["default"].create(this.element, {
      ghostClass: 'blue-background-during-drag',
      animation: 300,
      onSort: function () {
        let inputs = _self.element.querySelectorAll('input');

        inputs.forEach((element, index) => {
          element.setAttribute('id', _self.updateId(element.getAttribute('id'), index));
          element.setAttribute('name', _self.updateName(element.getAttribute('name'), index));
        });
      }
    });
  }

  updateName(currentName, index) {
    let position = index % 2 === 0 ? index / 2 : (index - 1) / 2;
    return currentName.replace(/\[\d+\]/, `[${position}]`);
  }

  updateId(currentId, index) {
    let position = index % 2 === 0 ? index / 2 : (index - 1) / 2;
    return currentId.replace(/_\d+_/, `_${position}_`);
  }

});

/***/ }),

/***/ "./assets/js/controllers/sortable_controller.js":
/*!******************************************************!*\
  !*** ./assets/js/controllers/sortable_controller.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var sortablejs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sortablejs */ "./node_modules/sortablejs/modular/sortable.esm.js");


/* harmony default export */ __webpack_exports__["default"] = (class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    sortablejs__WEBPACK_IMPORTED_MODULE_1__["default"].create(this.element, {
      ghostClass: 'blue-background-during-drag',
      animation: 300
    });
  }

});

/***/ }),

/***/ "./assets/js/controllers/toggle_file_upload_section_controller.js":
/*!************************************************************************!*\
  !*** ./assets/js/controllers/toggle_file_upload_section_controller.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _class; });
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


class _class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  connect() {
    $(document).ready(() => {
      this.isVisible = this.element.dataset.isVisible === 'true';
      EventManager.fire('toggle-file-upload-section', {
        creditId: this.creditId,
        isVisible: this.isVisible
      });
    });
  }

  toggle() {
    this.isVisible = !this.isVisible;
    EventManager.fire('toggle-file-upload-section', {
      creditId: this.creditId,
      isVisible: this.isVisible
    });
  }

  get isVisible() {
    return this.element.dataset.isVisible === 'true';
  }

  set isVisible(value) {
    this.element.setAttribute('data-is-visible', value);

    if (value === true) {
      this.arrowTarget.classList.remove('fa-angle-right');
      this.arrowTarget.classList.add('fa-angle-down');
    } else {
      this.arrowTarget.classList.remove('fa-angle-down');
      this.arrowTarget.classList.add('fa-angle-right');
    }
  }

  get creditId() {
    return this.element.dataset.creditId;
  }

}

_defineProperty(_class, "targets", ["arrow"]);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/controllers/toggle_password_controller.js":
/*!*************************************************************!*\
  !*** ./assets/js/controllers/toggle_password_controller.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _class; });
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


class _class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  toggle() {
    if (this.passwordElementTarget.getAttribute('type') === 'password') {
      this.passwordElementTarget.setAttribute('type', 'text');
    } else {
      this.passwordElementTarget.setAttribute('type', 'password');
    }

    this.buttonElementTarget.classList.toggle('fa-eye');
    this.buttonElementTarget.classList.toggle('fa-eye-slash');
  }

}

_defineProperty(_class, "targets", ["passwordElement", "buttonElement"]);

/***/ }),

/***/ "./assets/js/controllers/toggle_repayment_plan_controller.js":
/*!*******************************************************************!*\
  !*** ./assets/js/controllers/toggle_repayment_plan_controller.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _class; });
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


class _class extends stimulus__WEBPACK_IMPORTED_MODULE_0__["Controller"] {
  toggle(event) {
    if (event.target.nodeName === 'BUTTON') {
      return;
    }

    this.isVisible = !this.isVisible;
    EventManager.fire('toggle-repayment-plan', {
      contract: this.contract,
      isVisible: this.isVisible
    });
  }

  get isVisible() {
    return this.element.dataset.isVisible === 'true';
  }

  set isVisible(value) {
    this.element.setAttribute('data-is-visible', value);

    if (value === true) {
      this.arrowTarget.classList.remove('fa-angle-right');
      this.arrowTarget.classList.add('fa-angle-down');
    } else {
      this.arrowTarget.classList.remove('fa-angle-down');
      this.arrowTarget.classList.add('fa-angle-right');
    }
  }

  get contract() {
    return this.element.dataset.contract;
  }

}

_defineProperty(_class, "targets", ["arrow"]);

/***/ }),

/***/ "./assets/js/custom.js":
/*!*****************************!*\
  !*** ./assets/js/custom.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {$(document).ready(function () {
  $(window).scroll(function () {
    if ($(document).scrollTop() > 50) {
      $('header').addClass('header-bgr');
    } else {
      $('header').removeClass('header-bgr');
    }
  }); //    $('nav a').click(function () {
  //        $('.nav-btn').toggleClass('open');
  //        $('.mobile-nav').toggleClass('open');
  //    });

  $('.nav-btn').click(function () {
    $(this).toggleClass('open');
    $('nav').toggleClass('open');
  });
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/infrastructure/Carousel.js":
/*!**********************************************!*\
  !*** ./assets/js/infrastructure/Carousel.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var flickity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! flickity */ "./node_modules/flickity/js/index.js");
/* harmony import */ var flickity__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(flickity__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var flickity_dist_flickity_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! flickity/dist/flickity.css */ "./node_modules/flickity/dist/flickity.css");
/* harmony import */ var flickity_dist_flickity_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(flickity_dist_flickity_css__WEBPACK_IMPORTED_MODULE_2__);



vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('carousel', {
  template: '#carousel',
  props: {
    wrapAround: {
      default: true
    },
    autoPlay: {
      default: false
    }
  },

  mounted() {
    new flickity__WEBPACK_IMPORTED_MODULE_1___default.a(this.$el, {
      wrapAround: this.wrapAround,
      autoPlay: this.autoPlay,
      cellAlign: 'left',
      contain: true
    });
  }

});

/***/ }),

/***/ "./assets/js/infrastructure/DeleteLink.js":
/*!************************************************!*\
  !*** ./assets/js/infrastructure/DeleteLink.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");


vue__WEBPACK_IMPORTED_MODULE_1__["default"].component('delete-link', {
  template: `<a href="" class="dropdown-item text-danger" @click.prevent="deleteAction"><i :class="iconClass"></i> <slot></slot></a>`,
  props: {
    targetUrl: {
      required: true,
      type: String
    },
    redirectUrl: {
      required: true,
      type: String
    },
    confirmationMessage: {
      required: false,
      type: String,
      default: 'Please, confirm you want to delete this item!'
    },
    iconClass: {
      required: false,
      type: String,
      default: 'fa fa-trash-o'
    }
  },
  methods: {
    deleteAction() {
      let _self = this;

      if (confirm(this.confirmationMessage)) {
        $.ajax({
          method: 'DELETE',
          url: this.targetUrl,

          success() {
            window.location.replace(_self.redirectUrl);
          },

          error(error) {
            console.log(error);
            window.location.replace(_self.redirectUrl);
          }

        });
      }
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/infrastructure/DeleteModuleItem.js":
/*!******************************************************!*\
  !*** ./assets/js/infrastructure/DeleteModuleItem.js ***!
  \******************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('delete-module-item', {
  template: `  <button type="submit" @click="deleteAction($event)" :form="formId" class="btn btn-link btn-lg"><span
                                                class="fa fa-minus-circle"></span>
                                    </button>`,
  props: {
    moduleItemId: {
      required: true,
      type: String
    },
    confirmationMessage: {
      required: false,
      type: String,
      default: 'Please, confirm you want to delete this module!'
    }
  },
  computed: {
    formId() {
      return "remove_page_item_" + this.moduleItemId;
    }

  },
  methods: {
    deleteAction(event) {
      if (!confirm(this.confirmationMessage)) {
        event.preventDefault();
      }
    }

  }
});

/***/ }),

/***/ "./assets/js/infrastructure/FileUploadButton.js":
/*!******************************************************!*\
  !*** ./assets/js/infrastructure/FileUploadButton.js ***!
  \******************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");


vue__WEBPACK_IMPORTED_MODULE_1__["default"].component('file-upload-button', {
  name: "FileUploadButton",
  delimiters: ['[[', ']]'],
  template: '#file-upload-button',
  props: {
    uploadUrl: {
      type: String,
      required: true
    },
    redirectUrl: {
      required: true,
      type: String
    },
    buttonText: {
      required: false,
      type: String,
      default: 'Upload'
    },
    fullWidth: {
      type: Boolean,
      required: false,
      default: false
    }
  },

  data() {
    return {
      uploading: false
    };
  },

  computed: {
    isUploading() {
      return this.uploading === true;
    }

  },
  methods: {
    selectFile() {
      this.$refs.fileInput.click();
    },

    uploadFile() {
      let file = this.$refs.fileInput.files[0],
          _self = this;

      if (!file) {
        return;
      }

      this.uploading = true;
      let formData = new FormData();
      formData.append("file", file);
      $.ajax({
        method: 'post',
        url: this.uploadUrl,
        data: formData,
        processData: false,
        contentType: false,

        success() {
          _self.uploading = false;
          window.location.replace(_self.redirectUrl);
          console.log(_self.redirectUrl);
        },

        error(error) {
          _self.uploading = false;
          flash(error['responseJSON'].detail, 'error');
        }

      });
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/infrastructure/FilesList.js":
/*!***********************************************!*\
  !*** ./assets/js/infrastructure/FilesList.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue2-dropzone */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue2-dropzone/dist/vue2Dropzone.min.css */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.min.css");
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_2__);



vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('files-list', {
  delimiters: ['[[', ']]'],
  template: `
<div>
    <vue-dropzone ref="customDropzone" id="dropzone" :options="dropzoneOptions" :useCustomSlot=true><slot></slot></vue-dropzone>
    
    <ul class="list-group mt-2">
        <li class="list-group-item d-flex justify-content-between" v-for="image in images" :key="image.id">
            <a :href="image.publicPath" target="_blank">[[ image.originalName ]]</a>
            <span style="font-size: 1.2em">
                <span v-if="deletedFileIndex === image.id" class="badge badge-secondary">
                    Are you sure?
                    <span class="badge badge-pill badge-danger" style="cursor:pointer;" @click="deleteImage(image.id)">Yes</span>
                    <span class="badge badge-pill badge-light" style="cursor: pointer;" @click="deletedFileIndex = -1">No</span>
                </span>
                <span class="fa fa-trash-o text-danger" style="cursor: pointer" data-toggle="tooltip" data-placement="top" title="Delete the image" @click="deletedFileIndex = image.id"></span>
            </span>
        </li>
    </ul> 
</div>
`,
  components: {
    vueDropzone: vue2_dropzone__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  props: {
    createUrl: {
      required: true,
      type: String
    },
    indexUrl: {
      required: true,
      type: String
    },
    deleteUrl: {
      required: true,
      type: String
    },
    thumbnailWidth: {
      required: false,
      type: Number,
      default: 150
    }
  },

  mounted() {
    this.getImages();
  },

  data() {
    let _self = this;

    return {
      dropzoneOptions: {
        url: this.createUrl,
        thumbnailWidth: this.thumbnailWidth,
        addRemoveLinks: true,
        paramName: 'file',

        init() {
          this.on('error', function (file, data) {
            if (data.detail) {
              flash(data.detail, 'error');
            }
          });
          this.on('success', function (file, data) {
            _self.images.push(data);

            flash('Image was uploaded.', 'success');
          });
        }

      },
      images: [],
      state: 'initial',
      deletedFileIndex: -1
    };
  },

  methods: {
    getImages() {
      let _self = this;

      $.ajax({
        method: 'GET',
        url: _self.indexUrl,

        success(response) {
          _self.images = JSON.parse(response);
        },

        error(error) {
          console.log(error);
        }

      });
    },

    deleteImage(id) {
      let _self = this,
          data = {
        image_id: id
      };

      $.ajax({
        method: 'DELETE',
        url: _self.deleteUrl,
        data: data,

        success() {
          let imageIndex = _self.images.findIndex(item => item.id === id);

          _self.images.splice(imageIndex, 1);

          flash('Image was deleted', 'success');
        },

        error(error) {
          console.log(error);
        }

      });
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/infrastructure/Flash.js":
/*!*******************************************!*\
  !*** ./assets/js/infrastructure/Flash.js ***!
  \*******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('flash', {
  template: '#flash',
  props: {
    'message': {
      required: true,
      type: String
    },
    'level': {
      required: false,
      type: String,
      default: 'success'
    }
  },

  data() {
    return {
      messageClass: 'success',
      show: false,
      hasBeenShown: false
    };
  },

  created() {
    this.messageClass = this.getMessageClass(this.level);
    this.flash();
  },

  methods: {
    flash() {
      if (this.hasBeenShown) {
        return;
      }

      this.messageClass = this.getMessageClass(this.level);
      this.show = true;
      this.hide();
    },

    hide() {
      this.hasBeenShown = true;
      setTimeout(() => {
        this.show = false;
      }, 5000);
    },

    getMessageClass(level) {
      if (level === 'error') {
        return 'danger';
      } else {
        return 'success';
      }
    }

  }
});

/***/ }),

/***/ "./assets/js/infrastructure/Flashes.js":
/*!*********************************************!*\
  !*** ./assets/js/infrastructure/Flashes.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('flashes', {
  template: '#flashes',
  props: {
    'flashes': {
      required: false,
      type: String,
      default: null
    }
  },

  data() {
    return {
      messages: []
    };
  },

  created() {
    let flashData = JSON.parse(this.flashes);

    for (let key in flashData) {
      flashData[key].forEach(message => this.messages.push({
        message: message,
        level: key
      }));
    }

    window.EventManager.listen('flash', messageData => this.addMessage(messageData));
  },

  methods: {
    addMessage(messageData) {
      this.messages.push(messageData);
    }

  }
});

/***/ }),

/***/ "./assets/js/infrastructure/InlineBooleanEdit.js":
/*!*******************************************************!*\
  !*** ./assets/js/infrastructure/InlineBooleanEdit.js ***!
  \*******************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('inline-boolean-edit', {
  template: `
<div class="form-check">
    <input class="form-check-input" type="checkbox" @change="update" v-model="value">
</div>
`,
  props: {
    url: {
      required: true,
      type: String
    },
    method: {
      required: false,
      type: String,
      default: 'POST'
    },
    settingValue: {
      required: true,
      type: String
    }
  },

  data() {
    return {
      state: 'display',
      value: ''
    };
  },

  mounted() {
    this.value = this.settingValue === '1';
  },

  methods: {
    update() {
      let _self = this;

      $.ajax({
        method: this.method,
        url: this.url,
        data: {
          value: this.value
        },

        success() {
          flash('Updated.', 'success');
        },

        error(error) {
          _self.value = _self.settingValue === '1';
          flash(error['responseJSON'].detail, 'error');
        }

      });
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/infrastructure/InlineValueEdit.js":
/*!*****************************************************!*\
  !*** ./assets/js/infrastructure/InlineValueEdit.js ***!
  \*****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('inline-value-edit', {
  template: `
<div class="form-group">
    <textarea rows="3" v-show="state === 'editing'" v-model="value" class="form-control" ref="editField">
    </textarea>
    <p v-if="state === 'display'" v-html="value" class="d-inline-block"></p>
    <i v-if="state === 'display'" class="fa fa-pencil-square-o" @click="state = 'editing'" style="cursor: pointer; font-size: 1.5em"></i>
    <i v-if="state === 'editing'" class="fa fa-save" @click="save" style="cursor: pointer; font-size: 1.5em"></i>
    <i v-if="state === 'editing'" class="fa fa-window-close" @click="state = 'display'" style="cursor: pointer; font-size: 1.5em"></i>
</div>
`,
  props: {
    url: {
      required: true,
      type: String
    },
    method: {
      required: false,
      type: String,
      default: 'POST'
    },
    settingValue: {
      required: false,
      type: String,
      default: ''
    }
  },

  data() {
    return {
      state: 'display',
      value: ''
    };
  },

  mounted() {
    this.value = this.settingValue;
  },

  methods: {
    save() {
      let _self = this;

      $.ajax({
        method: this.method,
        url: this.url,
        data: {
          value: this.value
        },

        success() {
          _self.state = 'display';
          flash('Update was successful.', 'success');
        },

        error(error) {
          _self.state = 'display';
          _self.value = _self.settingValue;
          flash(error['responseJSON'].detail, 'error');
        }

      });
    }

  },
  watch: {
    state: function (newState) {
      if (newState === 'editing') {
        this.$nextTick(() => {
          this.$refs.editField.focus();
          this.$refs.editField.select();
        });
      }
    }
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/infrastructure/Modal.js":
/*!*******************************************!*\
  !*** ./assets/js/infrastructure/Modal.js ***!
  \*******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('modal', {
  template: '#modal',
  delimiters: ['[[', ']]'],
  props: {
    title: {
      type: String,
      required: false,
      default: 'Default modal title'
    },
    primaryButton: {
      type: String,
      required: false,
      default: 'Save'
    },
    identifier: {
      type: String,
      required: true
    },
    size: {
      type: String,
      required: false,
      default: null
    },
    verticallyCentered: {
      type: Boolean,
      required: false,
      default: false
    },
    hasFooter: {
      required: false,
      type: Boolean,
      default: true
    }
  },
  computed: {
    sizeClass() {
      if (!this.size) {
        return '';
      }

      return `modal-${this.size}`;
    },

    verticallyCenteredClass() {
      if (this.verticallyCentered) {
        return 'modal-dialog-centered';
      } else {
        return '';
      }
    }

  }
});

/***/ }),

/***/ "./assets/js/infrastructure/PostLink.js":
/*!**********************************************!*\
  !*** ./assets/js/infrastructure/PostLink.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");


vue__WEBPACK_IMPORTED_MODULE_1__["default"].component('post-link', {
  template: `<a href="" :class="linkClass" @click.prevent="postAction"><i :class="iconClass"></i> <slot></slot></a>`,
  props: {
    targetUrl: {
      required: true,
      type: String
    },
    redirectUrl: {
      required: true,
      type: String
    },
    needsConfirmation: {
      required: false,
      type: Boolean,
      default: false
    },
    confirmationMessage: {
      required: false,
      type: String,
      default: 'Please, confirm the action.'
    },
    iconClass: {
      required: false,
      type: String,
      default: ''
    },
    linkClass: {
      required: false,
      type: String,
      default: 'dropdown-item'
    }
  },
  methods: {
    postAction() {
      let _self = this;

      if (this.needsConfirmation) {
        if (confirm(this.confirmationMessage)) {
          $.ajax({
            method: 'POST',
            url: this.targetUrl,

            success() {
              window.location.replace(_self.redirectUrl);
            },

            error(error) {
              if (error['responseJSON']['error']) {
                flash(error['responseJSON']['error'], 'error');
              }
            }

          });
        }
      } else {
        $.ajax({
          method: 'POST',
          url: this.targetUrl,

          success() {
            window.location.replace(_self.redirectUrl);
          },

          error(error) {
            if (error['responseJSON']['error']) {
              flash(error['responseJSON']['error'], 'error');
            }
          }

        });
      }
    }

  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/infrastructure/Slider.js":
/*!********************************************!*\
  !*** ./assets/js/infrastructure/Slider.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! slick-carousel */ "./node_modules/slick-carousel/slick/slick.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! slick-carousel/slick/slick.css */ "./node_modules/slick-carousel/slick/slick.css");
/* harmony import */ var slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var slick_carousel_slick_slick_theme_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! slick-carousel/slick/slick-theme.css */ "./node_modules/slick-carousel/slick/slick-theme.css");
/* harmony import */ var slick_carousel_slick_slick_theme_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(slick_carousel_slick_slick_theme_css__WEBPACK_IMPORTED_MODULE_3__);




vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('slider', {
  template: `<div class="credit-item"><slot></slot></div>`,
  props: {
    infinite: {
      default: true,
      type: Boolean,
      required: false
    },
    slidesToShow: {
      default: 3,
      type: Number,
      required: false
    },
    slidesToScroll: {
      default: 3,
      type: Number,
      required: false
    },
    dots: {
      default: false,
      type: Boolean,
      required: false
    },
    centerPadding: {
      default: '15px',
      type: String,
      required: false
    }
  },

  mounted() {
    $(this.$el).slick({
      // normal options...
      infinite: this.infinite,
      slidesToShow: this.slidesToShow,
      slidesToScroll: this.slidesToScroll,
      dots: this.dots,
      centerPadding: this.centerPadding,
      // the magic
      responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          centerPadding: '15px'
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerPadding: '15px'
        }
      }, {
        breakpoint: 200,
        settings: "unslick" // destroys slick

      }]
    });
  }

});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/infrastructure/Tab.js":
/*!*****************************************!*\
  !*** ./assets/js/infrastructure/Tab.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");


vue__WEBPACK_IMPORTED_MODULE_1__["default"].component('tab', {
  template: '#tab',
  props: {
    name: {
      required: true
    },
    selected: {
      default: false
    },
    parent: {
      required: false,
      type: String,
      default: null
    }
  },

  data() {
    return {
      isActive: false,
      isChildActive: false
    };
  },

  computed: {
    href() {
      return '#' + this.name.toLowerCase().replace(/ /g, '-');
    }

  },

  created() {
    EventManager.listen('childTabActive', this.activate);
  },

  mounted() {
    let currentUrl = window.location.href,
        parts = currentUrl.split('#'),
        fragment = null;

    if (parts.length > 1) {
      fragment = decodeURI(parts[parts.length - 1]).toLowerCase().replace(/ /g, '-');

      if (!this.isChildActive) {
        this.isActive = '#' + fragment === this.href;
      }

      if (this.isActive === true) {
        EventManager.fire('childTabActive', this.parent);
      }
    } else {
      this.isActive = this.selected;
    }
  },

  methods: {
    activate(parent) {
      if (this.name === parent) {
        this.isChildActive = true;
        this.isActive = true;
      }
    }

  }
});

/***/ }),

/***/ "./assets/js/infrastructure/Tabs.js":
/*!******************************************!*\
  !*** ./assets/js/infrastructure/Tabs.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__["default"].component('tabs', {
  template: '#tabs',
  delimiters: ['[[', ']]'],

  data() {
    return {
      tabs: []
    };
  },

  created() {
    this.tabs = this.$children;
  },

  methods: {
    selectTab(selectedTab) {
      this.tabs.forEach(tab => {
        tab.isActive = tab.href === selectedTab.href;
      });
    }

  }
});

/***/ }),

/***/ "./assets/js/utils/gmaps.js":
/*!**********************************!*\
  !*** ./assets/js/utils/gmaps.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return init; });
//TODO api key
const API_KEY = 'AIzaSyCVGtuP6VFAQtebWRs44HktcbnGDgPdomM';
const CALLBACK_NAME = 'gmapsCallback';
let initialized = !!window.google;
let resolveInitPromise;
let rejectInitPromise; // This promise handles the initialization
// status of the google maps script.

const initPromise = new Promise((resolve, reject) => {
  resolveInitPromise = resolve;
  rejectInitPromise = reject;
});
function init() {
  // If Google Maps already is initialized
  // the `initPromise` should get resolved
  // eventually.
  if (initialized) return initPromise;
  initialized = true; // The callback function is called by
  // the Google Maps script if it is
  // successfully loaded.

  window[CALLBACK_NAME] = () => resolveInitPromise(window.google); // We inject a new script tag into
  // the `<head>` of our HTML to load
  // the Google Maps script.


  const script = document.createElement('script');
  script.async = true;
  script.defer = true;
  script.src = `https://maps.googleapis.com/maps/api/js?key=${API_KEY}&callback=${CALLBACK_NAME}`;
  script.onerror = rejectInitPromise;
  document.querySelector('head').appendChild(script);
  return initPromise;
}

/***/ })

},[["./assets/js/app.js","runtime","vendors~app"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL2FwcC5zY3NzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2Jvb3RzdHJhcC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9BcHBseU9ubGluZUJ1dHRvbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9Db29raWVEZWNsYXJhdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9DcmVkaXRDYWxjdWxhdG9yLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb21wb25lbnRzL0NyZWRpdENvbXBhcmlzaW9uLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb21wb25lbnRzL0NyZWRpdE9mZmVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb21wb25lbnRzL0NyZWRpdFJlZmluYW5jZS5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9DcmVkaXRSZXBheW1lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbXBvbmVudHMvQ3JlZGl0UmVwYXltZW50QW1vdW50Rm9ybS5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9FbnRpdHlUcmFuc2xhdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9FbnRpdHlUcmFuc2xhdGlvbnNGb3JtLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb21wb25lbnRzL0Zyb250ZW5kVmFsaWRhdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9HbWFwcy5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9JbWFnZUdhbGxlcnkuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbXBvbmVudHMvUG9sbC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9Qb2xsQW5zd2VyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb21wb25lbnRzL1BvbGxQYXJ0aWNpcGF0aW9uLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb21wb25lbnRzL1BvbGxRdWVzdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9SZWRpcmVjdFRpbWVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb21wb25lbnRzL1NsaWRlck1vZHVsZS5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29tcG9uZW50cy9UZWxlcGhvbmVBcHBsaWNhdGlvbk5vdEFsbG93ZWQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbXBvbmVudHMvVHJhbnNsYXRpb25JdGVtLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb21wb25lbnRzL1RyYW5zbGF0aW9uc0xpc3QuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbXBvbmVudHMvWm9waW1DaGF0LmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycyBzeW5jIFxcLmpzJCIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvQ0tFZGl0b3IvQXBwbHlCdXR0b24uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL0NLRWRpdG9yL0hvcml6b250YWxSdWxlLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9DS0VkaXRvci9JbnNlcnRBcHBseUJ1dHRvbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvQ0tFZGl0b3IvSW5zZXJ0SG9yaXpvbnRhbFJ1bGUuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL0NLRWRpdG9yL0luc2VydEltYWdlLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9DS0VkaXRvci9JbnNlcnRJbWFnZVZpYVVSTC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvQ0tFZGl0b3IvV2lkZ2V0cy5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvQ0tFZGl0b3IvV2lkZ2V0c1VJLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9DdXN0b21VcGxvYWRBZGFwdGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9hZ2VfZmllbGRfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvYXZhaWxhYmxlLXBheW1lbnQtaW5zdHJ1bWVudHNfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvYmFja2dyb3VuZF9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9iYW5rX2NhcmRfbnVtYmVyX2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2Jvb3RzdHJhcF9maWxlX3VwbG9hZF9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9ib290c3RyYXBfbXVsdGlwbGVfZmlsZV91cGxvYWRfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvYnVsbGV0aW5fY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvY2FwdGNoYV9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9jYXJfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvY2FyZF9kYXRhX2NvbnNlbnRfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvY2hlY2stZW1haWwtY29uZmlybWVkX2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2NrZWRpdG9yX2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2NsaXBib2FyZF9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9jcmVkaXRfYXBwbGljYXRpb25fZm9ybV9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9jcmVkaXRfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvY3JlZGl0X3BhcmFtZXRlcnNfZWxlbWVudF9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9jcmVkaXRfcGFyYW1ldGVyc19maWVsZF9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9jdXJyZW50X2FkZHJlc3NfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvY3VycmVudF9hZGRyZXNzX3NlY3Rpb25fY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvZXhwYW5kYWJsZV9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9leHBpcmF0aW9uX2RhdGVfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvZmFxX2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2hlYWRlcl9tb2R1bGVfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvaWJhbl9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9pbWFnZV9nYWxsZXJ5X2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2pvYl9vcGVuaW5nX2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2xvY2FsZV9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9tYXN0ZXJfZmllbGRfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvbWVzc2FnZS11bnJlYWQtY291bnRfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvbWVzc2FnZV9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9vbmxpbmVfYXBwbGljYXRpb25fZm9ybV9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9wYWdlX3NlbGVjdG9yX2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3Bhc3Nwb3J0X3R5cGVfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvcGVuc2lvbmVyX2RvY3VtZW50c19jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9waG9uZV9udW1iZXJfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvcGlja3JfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvcGluX2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3BvbGxfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvcXVlcnlfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvc2FsYXJ5X2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3Njcm9sbF90b19lbGVtZW50X2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3Njcm9sbF90b19pbnZhbGlkX2Zvcm1fY29udHJvbF9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9zaG93X2hpZGVfZmlsZV91cGxvYWRfc2VjdGlvbl9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9zaG93X2hpZGVfcmVwYXltZW50X3BsYW5fY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvc29ydF9saW5rc19jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy9zb3J0YWJsZV9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy90b2dnbGVfZmlsZV91cGxvYWRfc2VjdGlvbl9jb250cm9sbGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9jb250cm9sbGVycy90b2dnbGVfcGFzc3dvcmRfY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvY29udHJvbGxlcnMvdG9nZ2xlX3JlcGF5bWVudF9wbGFuX2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2N1c3RvbS5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvaW5mcmFzdHJ1Y3R1cmUvQ2Fyb3VzZWwuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2luZnJhc3RydWN0dXJlL0RlbGV0ZUxpbmsuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2luZnJhc3RydWN0dXJlL0RlbGV0ZU1vZHVsZUl0ZW0uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2luZnJhc3RydWN0dXJlL0ZpbGVVcGxvYWRCdXR0b24uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2luZnJhc3RydWN0dXJlL0ZpbGVzTGlzdC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvaW5mcmFzdHJ1Y3R1cmUvRmxhc2guanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2luZnJhc3RydWN0dXJlL0ZsYXNoZXMuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2luZnJhc3RydWN0dXJlL0lubGluZUJvb2xlYW5FZGl0LmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9pbmZyYXN0cnVjdHVyZS9JbmxpbmVWYWx1ZUVkaXQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2luZnJhc3RydWN0dXJlL01vZGFsLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9pbmZyYXN0cnVjdHVyZS9Qb3N0TGluay5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvaW5mcmFzdHJ1Y3R1cmUvU2xpZGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9pbmZyYXN0cnVjdHVyZS9UYWIuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2luZnJhc3RydWN0dXJlL1RhYnMuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3V0aWxzL2dtYXBzLmpzIl0sIm5hbWVzIjpbIndpbmRvdyIsIkV2ZW50TWFuYWdlciIsImNvbnN0cnVjdG9yIiwidnVlIiwiVnVlIiwiZmlyZSIsImV2ZW50IiwiZGF0YSIsIiRlbWl0IiwibGlzdGVuIiwiY2FsbGJhY2siLCIkb24iLCJmbGFzaCIsIm1lc3NhZ2UiLCJsZXZlbCIsImFwcGxpY2F0aW9uIiwiQXBwbGljYXRpb24iLCJzdGFydCIsImNvbnRleHQiLCJyZXF1aXJlIiwibG9hZCIsImRlZmluaXRpb25zRnJvbUNvbnRleHQiLCJvcHRpb25zIiwiZGVsaW1pdGVycyIsImNvbmZpZyIsImlnbm9yZWRFbGVtZW50cyIsImFwcCIsImVsIiwic2hvd01vZGFsIiwiZHluYW1pY0NvbXBvbmVudHMiLCJtb3VudGVkIiwiYWRkQ29tcG9uZW50IiwicmVtb3ZlQ29tcG9uZW50IiwibWV0aG9kcyIsInB1c2giLCJjb21wb25lbnRJZCIsImluZGV4IiwiZmluZEluZGV4IiwiaXRlbSIsImlkIiwic3BsaWNlIiwiJCIsInRvb2x0aXAiLCJlbGVtZW50cyIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvckFsbCIsImZvckVhY2giLCJlbGVtZW50IiwiYWRkRXZlbnRMaXN0ZW5lciIsInNjcm9sbEludG9WaWV3Iiwic3VibWl0IiwiZmluZCIsInByb3AiLCJ0b2tlbiIsImhlYWQiLCJxdWVyeVNlbGVjdG9yIiwiYWpheFNldHVwIiwiYmVmb3JlU2VuZCIsInhociIsInNldHRpbmdzIiwic2V0UmVxdWVzdEhlYWRlciIsImNvbnRlbnQiLCJjb25zb2xlIiwiZXJyb3IiLCJjb21wb25lbnQiLCJuYW1lIiwidGVtcGxhdGUiLCJwcm9wcyIsImNoZWNrQ3VzdG9tZXJJc0FsbG93ZWRVcmwiLCJ0eXBlIiwiU3RyaW5nIiwicmVxdWlyZWQiLCJyZWRpcmVjdFVybCIsImNoZWNraW5nIiwiY29tcHV0ZWQiLCJpc0NoZWNraW5nIiwiY2hlY2tDdXN0b21lcklzRGVuaWVkIiwiX3NlbGYiLCJhamF4IiwibWV0aG9kIiwidXJsIiwic3VjY2VzcyIsInJlc3BvbnNlIiwiaXNEZW5pZWQiLCIkcmVmcyIsIm1vZGFsIiwiJGVsIiwiZ2V0QXR0cmlidXRlIiwibG9jYXRpb24iLCJyZXBsYWNlIiwibG9nIiwibG9hZEpzIiwiZGF0YVR5cGUiLCJhc3luYyIsInN0YW5kYWxvbmUiLCJCb29sZWFuIiwiZGVmYXVsdCIsImN1cnJlbmN5IiwibG9jYWxlIiwiY3JlZGl0IiwiY3JlZGl0QW1vdW50IiwiY3JlZGl0UGF5bWVudCIsImlzUmVmcmVzaGluZyIsImFtb3VudFN0ZXBzIiwicGF5bWVudFN0ZXBzIiwiYW1vdW50IiwicGF5bWVudCIsInBheW1lbnRXaXRoUGVuYWx0eSIsInBlcmlvZHMiLCJ0b3RhbCIsInRvdGFsV2l0aFBlbmFsdHkiLCJpbnRlcmVzdFJhdGVGaXJzdFBlcmlvZCIsImludGVyZXN0UmF0ZVNlY29uZFBlcmlvZCIsImFwcmMiLCJwYXltZW50U2NoZWR1bGUiLCJwZW5zaW9uZXIiLCJjcmVkaXRUaXRsZSIsImNyZWRpdFNsdWciLCJyZXF1aXJlbWVudHMiLCJjcmVkaXRQcm9kdWN0SWQiLCJjcmVkaXRQZXJpb2RJZCIsImV4cGVuc2VzIiwiZ2V0RGF0YSIsImRlZmF1bHRQYXJhbXMiLCJhbW91bnRDaGFuZ2VkIiwicGF5bWVudENoYW5nZWQiLCJwYXJhbXMiLCJjcmVkaXRIYXNDaGFuZ2VkIiwicGFyYW0iLCJtYXAiLCJ2YWx1ZSIsInBhcnNlRmxvYXQiLCJjcmVkaXREYXRhIiwidGl0bGUiLCJzbHVnIiwiZGVmYXVsdEFtb3VudCIsInByb2R1Y3RJZCIsImluaXRBbW91bnRTbGlkZXIiLCJpbml0UGF5bWVudFNsaWRlciIsInByb2R1Y3QiLCJwZXJpb2QiLCJ0b0ZpeGVkIiwicGFyYW1ldGVycyIsInBheW1lbnRTY2hlZHVsZUNoYW5nZWQiLCJkZWZhdWx0QW1vdW50Rm9yUGF5bWVudFNjaGVkdWxlIiwicGVuc2lvbmVyQ2hhbmdlZCIsImNoYW5nZVR5cGUiLCJuZXdUeXBlIiwiYW1vdW50U2xpZGVyIiwiZGVzdHJveSIsImlvblJhbmdlU2xpZGVyIiwic2tpbiIsImdyaWQiLCJncmlkX3NuYXAiLCJ2YWx1ZXMiLCJmcm9tIiwicG9zdGZpeCIsImZvcmNlX2VkZ2VzIiwib25GaW5pc2giLCJmcm9tX3ZhbHVlIiwidXBkYXRlIiwicGF5bWVudFNsaWRlciIsImFkanVzdGVkUGF5bWVudFN0ZXBzIiwibGVuZ3RoIiwiY3JlYXRlT2ZmZXIiLCJkdXJhdGlvbiIsImludGVyZXN0Rmlyc3RQZXJpb2QiLCJpbnRlcmVzdFNlY29uZFBlcmlvZCIsImludGVyZXN0QVBSQyIsImFwcGx5IiwiaHJlZiIsIk1hdGgiLCJjZWlsIiwiY29udGFpbmVyQ2xhc3MiLCJib3R0b21DbGFzcyIsIndhdGNoIiwicGF5bWVudFNjaGVtZVZpc2libGUiLCJwZW5zaW9uZXJWaXNpYmxlIiwiaW50ZXJlc3RSYXRlVmlzaWJsZSIsIm9mZmVycyIsInJlbW92ZU9mZmVyIiwiTnVtYmVyIiwiY2xvc2UiLCJjb250cmFjdCIsInJlZmluYW5jZVN1bSIsImdldFJlZmluYW5jZVN1bSIsImZvcm1hdHRlZFJlZmluYW5jZVN1bSIsInN0YXRlIiwicmVwYXltZW50U3VtIiwiZ2V0UmVwYXltZW50U3VtIiwicmVkaXJlY3RGb3JQYXltZW50IiwiaXNFYXJseVJlcGF5bWVudCIsInJlcGF5bWVudFVybCIsImZvcm1hdHRlZFJlcGF5bWVudFN1bSIsImluaXRpYWxBbW91bnQiLCJlcnJvcnMiLCJyZXNwb25zZUpTT04iLCJoYXNFcnJvciIsIm9iamVjdENsYXNzIiwiZm9yZWlnbktleSIsImZpZWxkIiwiZ2V0Q29udGVudCIsImVuY29kZVVSSSIsInNhdmVDb250ZW50IiwibG9jYWxlcyIsImxvY2FsZXNBcnJheSIsInRyYW5zbGF0aW9ucyIsInNwbGl0IiwicHJvcGVydHkiLCJsYWJlbCIsImVudGl0eSIsImlucHV0IiwidmFsaWRhdGUiLCJjbGFzc0xpc3QiLCJyZW1vdmUiLCJhZGQiLCJoYXNFcnJvcnMiLCJhZGRyZXNzIiwiem9vbSIsIm9mZmljZXMiLCJBcnJheSIsImNpdGllcyIsIm1hcmtlcjEiLCJtYXJrZXIyIiwib2ZmaWNlSWNvbiIsInNob3dNYXBJbml0aWFsbHkiLCJpc09mZmljZVNlbGVjdGVkIiwiZ2VvY29kZXIiLCJzZWxlY3RlZENpdHkiLCJzZWxlY3RlZE9mZmljZSIsImNpdHkiLCJ0ZWxlcGhvbmUiLCJ3b3JraW5nVGltZSIsImdvb2dsZU1hcExpbmsiLCJzdHlsZXMiLCJ3aWR0aCIsImhlaWdodCIsInRleHRDb2xvciIsInRleHRTaXplIiwibWFwU3R5bGVzIiwiaXNNYXBWaXNpYmxlIiwiZ21hcFR5cGUiLCJnb29nbGUiLCJtYXBzIiwiTWFwVHlwZUlkIiwiU0FURUxMSVRFIiwiSFlCUklEIiwiVEVSUkFJTiIsIlJPQURNQVAiLCJ0ZWxlcGhvbmVUZXh0Iiwid29ya2luZ1RpbWVUZXh0IiwicGFydHMiLCJjb21wdXRlWm9vbSIsImxvYWRNYXAiLCJnbWFwc0luaXQiLCJNYXAiLCJnbWFwIiwiR2VvY29kZXIiLCJzZXRPcHRpb25zIiwiZ2VvY29kZSIsInJlc3VsdHMiLCJzdGF0dXMiLCJFcnJvciIsInNldENlbnRlciIsImdlb21ldHJ5IiwiZml0Qm91bmRzIiwidmlld3BvcnQiLCJzZXRab29tIiwibWFwVHlwZUlkIiwiZ2VzdHVyZUhhbmRsaW5nIiwibWFya2VyQ2xpY2tIYW5kbGVyIiwibWFya2VyIiwiZ2V0UG9zaXRpb24iLCJzZWxlY3RPZmZpY2UiLCJtYXJrZXJzIiwiTWFya2VyIiwiaWNvbiIsImFkZExpc3RlbmVyIiwiTWFya2VyQ2x1c3RlcmVyIiwiZmlsdGVyIiwib2ZmaWNlIiwibGF0IiwibG5nIiwiZGVzZWxlY3RPZmZpY2UiLCJzaG93TWFwIiwiaW1hZ2VzIiwibW9kYWxFbGVtZW50IiwiaXNMb2FkaW5nIiwidXBsb2FkaW5nIiwib24iLCJjbG9zZUdhbGxlcnkiLCJvcGVuR2FsbGVyeSIsImdldEltYWdlcyIsIkpTT04iLCJwYXJzZSIsInNlbGVjdEltYWdlIiwicHVibGljUGF0aCIsInBhdGgiLCJzZWxlY3RGaWxlIiwiZmlsZUlucHV0IiwiY2xpY2siLCJ1cGxvYWRGaWxlIiwiZmlsZSIsImZpbGVzIiwiZm9ybURhdGEiLCJGb3JtRGF0YSIsImFwcGVuZCIsInByb2Nlc3NEYXRhIiwiY29udGVudFR5cGUiLCJ1bnNoaWZ0IiwiZGV0YWlsIiwiaXNVcGxvYWRpbmciLCJwb2xsSWQiLCJwb2xsIiwicXVlc3Rpb25zIiwibmV3UXVlc3Rpb24iLCJpc0FkZGluZyIsInNvcnRhYmxlIiwiY3JlYXRlZCIsImluaXRTb3J0YWJsZSIsImdldENyZWF0ZURhdGEiLCJ0ZXh0Iiwib25RdWVzdGlvbkRlbGV0ZWQiLCJjcmVhdGVRdWVzdGlvbiIsInF1ZXN0aW9uIiwicmVzZXRRdWVzdGlvbiIsIlNvcnRhYmxlIiwiY3JlYXRlIiwiZ2hvc3RDbGFzcyIsImFuaW1hdGlvbiIsIm9uU29ydCIsImNoaWxkcmVuIiwic29ydFF1ZXN0aW9ucyIsInNvcnRlZFF1ZXN0aW9ucyIsImFuc3dlciIsIk9iamVjdCIsImlzRWRpdGluZyIsImRlbGV0ZVN0YXRlIiwiZ2V0U2F2ZURhdGEiLCJoYXNDb21tZW50IiwiY29tbWVudEhlbHBUZXh0Iiwic2F2ZSIsImRlbGV0ZUFuc3dlciIsImNoYW5nZURlbGV0ZVN0YXRlIiwibmV3U3RhdGUiLCJhY3RpdmVQb2xscyIsImFjdGl2ZVBvbGxzRGF0YSIsInNlbGVjdGVkUG9sbCIsInNob3ciLCJyZWdleCIsIm1hdGNoIiwiaW5kZXhPZiIsInNldFRpbWVvdXQiLCJnZXRBY3RpdmVQb2xscyIsInNlbGVjdFBvbGwiLCJjb21wbGV0ZWRQb2xscyIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJub3RDb21wbGV0ZWRQb2xscyIsInBhcnNlSW50IiwicG9wIiwiZGVjbGluZVBvbGwiLCJzZXRJdGVtIiwiam9pbiIsInNob3dQb2xsIiwicG9sbFVybCIsInJvdXRlIiwib3BlbiIsIm5ld1ZhbHVlIiwiaXNPcGVuIiwibmV3QW5zd2VyIiwicXVlc3Rpb25JZCIsInRvZ2dsZU9wZW4iLCJkZWxldGVRdWVzdGlvbiIsIm9uQW5zd2VyRGVsZXRlZCIsImFuc3dlcnMiLCJjcmVhdGVBbnN3ZXIiLCJyZXNldEFuc3dlciIsInRvZ2dsZVNvcnRpbmciLCJzb3J0aW5nIiwic29ydEFuc3dlcnMiLCJzaG93QW5zd2VycyIsInNvcnRlZEFuc3dlcnMiLCJzZWNvbmRzIiwiZWxhcHNlZFRpbWUiLCJ0aW1lciIsInN0YXJ0VGltZXIiLCJzZXRJbnRlcnZhbCIsInRpY2siLCJyZW1haW5pbmdUaW1lIiwiY2xlYXJJbnRlcnZhbCIsImNlbnRlclBhZGRpbmciLCJzaXplIiwiY3JlYXRlU3R5bGUiLCJGbGlja2l0eSIsIml0ZW1zIiwid3JhcEFyb3VuZCIsImF1dG9QbGF5IiwicGFnZURvdHMiLCJjb250YWluIiwic2V0R2FsbGVyeVNpemUiLCJjb2xvciIsInN0eWxlRWxlbWVudCIsImNyZWF0ZUVsZW1lbnQiLCJjc3MiLCJjcmVhdGVUZXh0Tm9kZSIsImRvbWFpbiIsInRyYW5zbGF0aW9uS2V5IiwidHJhbnNsYXRpb25WYWx1ZSIsImRlZmF1bHRMb2NhbGUiLCJkZWZhdWx0VHJhbnNsYXRpb25WYWx1ZSIsImJhc2VVcmwiLCJhdXRoVG9rZW4iLCJ0cmFuc2xhdGlvbkl0ZW1zIiwib2xkVmFsdWUiLCJoZWFkZXJzIiwiY2FuY2VsIiwiZGVmYXVsdFRyYW5zbGF0aW9uVGV4dCIsIiRuZXh0VGljayIsImVkaXRGaWVsZCIsImZvY3VzIiwic2VsZWN0IiwiZG9tYWlucyIsInNlbGVjdGVkTG9jYWxlIiwic2VsZWN0ZWREb21haW4iLCJzZWxlY3RlZEtleSIsInNlbGVjdGVkVmFsdWUiLCJ0aW1lb3V0IiwiZ2V0TG9jYWxlcyIsImdldERvbWFpbnMiLCJnZXRUcmFuc2xhdGlvbnMiLCJkZWJvdW5jZWRHZXRUcmFuc2xhdGlvbnMiLCJjbGVhclRpbWVvdXQiLCJwYXlsb2FkIiwiY2xlYXIiLCJlbmFibGVab3BpbSIsIiR6b3BpbSIsImxpdmVjaGF0IiwidG9nZ2xlIiwiQXBwbHlCdXR0b25VSSIsImVkaXRvciIsImluaXQiLCJ1aSIsImNvbXBvbmVudEZhY3RvcnkiLCJidXR0b25WaWV3IiwiQnV0dG9uVmlldyIsInNldCIsIndpdGhUZXh0IiwiZXhlY3V0ZSIsIkFwcGx5QnV0dG9uRWRpdGluZyIsInJlcXVpcmVzIiwiV2lkZ2V0IiwiX2RlZmluZVNjaGVtYSIsIl9kZWZpbmVDb252ZXJ0ZXJzIiwiY29tbWFuZHMiLCJJbnNlcnRBcHBseUJ1dHRvbiIsInNjaGVtYSIsIm1vZGVsIiwicmVnaXN0ZXIiLCJpc09iamVjdCIsImFsbG93V2hlcmUiLCJhbGxvd0F0dHJpYnV0ZXMiLCJjb252ZXJzaW9uIiwiZm9yIiwiZWxlbWVudFRvRWxlbWVudCIsInZpZXciLCJjbGFzc2VzIiwidmlld0VsZW1lbnQiLCJtb2RlbFdyaXRlciIsImxpbmtIcmVmIiwiZ2V0Q2hpbGQiLCJidXR0b25UZXh0IiwiX3RleHREYXRhIiwibW9kZWxJdGVtIiwidmlld1dyaXRlciIsIndpZGdldEVsZW1lbnQiLCJjcmVhdGVBcHBseUJ1dHRvblZpZXciLCJ0b1dpZGdldCIsImFwcGx5QnV0dG9uVmlldyIsImNyZWF0ZUNvbnRhaW5lckVsZW1lbnQiLCJjbGFzcyIsInRoZUJ1dHRvbiIsImlubmVyVGV4dCIsImNyZWF0ZVRleHQiLCJpbnNlcnQiLCJjcmVhdGVQb3NpdGlvbkF0IiwiSG9yaXpvbnRhbFJ1bGUiLCJJbnNlcnRIb3Jpem9udGFsUnVsZSIsImFsbG93SW4iLCJpc0Jsb2NrIiwiY3JlYXRlSG9yaXpvbnRhbFJ1bGVWaWV3IiwicHJvbXB0IiwiY2hhbmdlIiwid3JpdGVyIiwiYnV0dG9uRWxlbWVudCIsImluc2VydENvbnRlbnQiLCJzZWxlY3Rpb24iLCJyZWZyZXNoIiwiaG9yaXpvbnRhbFJ1bGVFbGVtZW50Iiwic2V0U2VsZWN0aW9uIiwiSW5zZXJ0SW1hZ2UiLCJJbnNlcnRJbWFnZVZpYVVSTCIsImltYWdlSWNvbiIsImltYWdlVXJsIiwiaW1hZ2VFbGVtZW50Iiwic3JjIiwiV2lkZ2V0cyIsIldpZGdldHNVSSIsImRyb3Bkb3duVmlldyIsImNyZWF0ZURyb3Bkb3duIiwiYWRkTGlzdFRvRHJvcGRvd24iLCJnZXREcm9wZG93bkl0ZW1zRGVmaW5pdGlvbnMiLCJldnQiLCJzb3VyY2UiLCJjb21tYW5kUGFyYW0iLCJlZGl0aW5nIiwiaXRlbURlZmluaXRpb25zIiwiQ29sbGVjdGlvbiIsImltYWdlRGVmaW5pdGlvbiIsIk1vZGVsIiwiYXBwbHlCdXR0b25EZWZpbml0aW9uIiwiaG9yaXpvbnRhbFJ1bGVEZWZpbml0aW9uIiwiQ3VzdG9tVXBsb2FkQWRhcHRlciIsImxvYWRlciIsInVwbG9hZFVybCIsInVwbG9hZCIsInNlbGYiLCJ0aGVuIiwidXBsb2FkZWRGaWxlIiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJhYm9ydCIsIkNvbnRyb2xsZXIiLCJjb25uZWN0IiwiYWdlTGltaXQiLCJkYXRhc2V0IiwicHJvcGVydHlOYW1lIiwic2V0QXR0cmlidXRlIiwicmVtb3ZlQXR0cmlidXRlIiwiY3JlYXRlU3R5bGVUYWciLCJnZXQiLCJpbWFnZUNTUyIsImltYWdlTW9iaWxlQ1NTIiwiaGFzIiwicGFyZW50Tm9kZSIsImlubmVySFRNTCIsImZpbGVOYW1lcyIsImZpbGVOYW1lc1N0cmluZyIsInN1YnNjcmliZSIsImVtYWlsIiwiZW1haWxUYXJnZXQiLCJwcmV2ZW50RGVmYXVsdCIsImFwaUtleSIsImNhcHRoYUlucHV0IiwiZ3JlY2FwdGNoYSIsInJlYWR5IiwiYWN0aW9uIiwiZmV0Y2giLCJpbml0aWFsaXplIiwiQ2xhc3NpY0VkaXRvciIsInBsdWdpbnMiLCJIZWFkaW5nIiwiRXNzZW50aWFsc1BsdWdpbiIsIkJvbGRQbHVnaW4iLCJJdGFsaWNQbHVnaW4iLCJMaW5rUGx1Z2luIiwiUGFyYWdyYXBoUGx1Z2luIiwiTGlzdCIsIkltYWdlIiwiSW1hZ2VUb29sYmFyIiwiSW1hZ2VDYXB0aW9uIiwiSW1hZ2VTdHlsZSIsIkltYWdlUmVzaXplIiwiSW1hZ2VVcGxvYWQiLCJNZWRpYUVtYmVkIiwiVGFibGUiLCJDS0ZpbmRlciIsIkNLRmluZGVyQWRhcHRlciIsIkJsb2NrUXVvdGUiLCJBbGlnbm1lbnQiLCJGb250IiwiYWxpZ25tZW50IiwidG9vbGJhciIsImV4dHJhUGx1Z2lucyIsIm15Q3VzdG9tVXBsb2FkQWRhcHRlclBsdWdpbiIsImN1c3RvbVVwbG9hZFVybCIsImltYWdlIiwibWVkaWFFbWJlZCIsInByZXZpZXdzSW5EYXRhIiwibGluayIsImRlY29yYXRvcnMiLCJpc0V4dGVybmFsIiwibW9kZSIsImF0dHJpYnV0ZXMiLCJ0YXJnZXQiLCJmb250RmFtaWx5IiwiY2F0Y2giLCJjcmVhdGVVcGxvYWRBZGFwdGVyIiwiY29weSIsInRlbXBJbnB1dCIsInN0eWxlIiwicG9zaXRpb24iLCJsZWZ0IiwidG9wIiwiYm9keSIsImFwcGVuZENoaWxkIiwiZXhlY0NvbW1hbmQiLCJyZW1vdmVDaGlsZCIsInNvdXJjZVRhcmdldCIsInN1Ym1pdEFwcGxpY2F0aW9uIiwiY2hlY2tWYWxpZGl0eSIsInJlcG9ydFZhbGlkaXR5Iiwic3VibWl0VXJsIiwic3VibWl0QnV0dG9uIiwiYmVoYXZpb3IiLCJibG9jayIsInJhZGlvQnV0dG9ucyIsImlzU2FtZSIsImlzU2FtZUluaXRpYWxseSIsImN1cnJlbnRVcmwiLCJmcmFnbWVudCIsImRlY29kZVVSSSIsInRvTG93ZXJDYXNlIiwidG9Mb2NhbGVMb3dlckNhc2UiLCJ0b2dnbGVDb250ZW50IiwiYXJyb3dUYXJnZXQiLCJjb250ZW50VGFyZ2V0IiwidG9nZ2xlQW5zd2VyIiwiYW5zd2VyVGFyZ2V0IiwidG9nZ2xlQmFja2dyb3VuZE9wYWNpdHkiLCJzaG93SW1hZ2VHYWxsZXJ5IiwidG9nZ2xlSW5mbyIsImJ1dHRvblRleHRUYXJnZXQiLCJmdWxsSW5mb1RhcmdldCIsInRleHRPbkJ1dHRvbiIsImNvbnRhaW5zIiwic3dpdGNoIiwiY291bnQiLCJpc1VuUmVhZCIsInRpdGxlVGFyZ2V0IiwiaW5pdGlhbEFjdG9uIiwiY3JlZGl0RGV0YWlscyIsImlzUmVmaW5hbmNlIiwicXVlcnlQYXJhbWV0ZXJzT2JqZWN0IiwicXVlcnlTdHJpbmciLCJuZXdVcmwiLCJyZWRpcmVjdCIsInByb3BlcnR5VmFsdWUiLCJhc3RlcmlzayIsImNsYXNzTmFtZSIsInByZXBlbmQiLCJzZXRQaG9uZVByZWZpeCIsIm9uS2V5VXAiLCJzdGFydHNXaXRoIiwicGlja3IiLCJQaWNrciIsInRoZW1lIiwidXNlQXNCdXR0b24iLCJzd2F0Y2hlcyIsImNvbXBvbmVudHMiLCJwcmV2aWV3Iiwib3BhY2l0eSIsImh1ZSIsImludGVyYWN0aW9uIiwiaGV4IiwicmdiYSIsImhzbGEiLCJoc3ZhIiwiY215ayIsImluc3RhbmNlIiwidG9IRVhBIiwiYWdlIiwic3Vic3RyaW5nIiwiZGF5cyIsImJhc2VEYXRlIiwiRGF0ZSIsImRhdGVPZkJpcnRoIiwidmFsdWVPZiIsInRvZGF5Iiwic2V0RGF0ZSIsImdldERhdGUiLCJmbG9vciIsImZvcm1zIiwiU2V0IiwicGFyYW1ldGVyIiwiaGFzRXhpc3RpbmdWYWx1ZSIsInBhZ2VSZWdleCIsIlJlZ0V4cCIsInBhcnQiLCJzY3JvbGwiLCJtb2JpbGVPbmx5IiwiaXNNb2JpbGUiLCJtYXRjaE1lZGlhIiwibWF0Y2hlcyIsImVsZW1lbnRUYXJnZXQiLCJpbnZhbGlkQ29udHJvbCIsImNyZWRpdElkIiwiaXNWaXNpYmxlIiwiaW5wdXRzIiwidXBkYXRlSWQiLCJ1cGRhdGVOYW1lIiwiY3VycmVudE5hbWUiLCJjdXJyZW50SWQiLCJwYXNzd29yZEVsZW1lbnRUYXJnZXQiLCJidXR0b25FbGVtZW50VGFyZ2V0Iiwibm9kZU5hbWUiLCJzY3JvbGxUb3AiLCJhZGRDbGFzcyIsInJlbW92ZUNsYXNzIiwidG9nZ2xlQ2xhc3MiLCJjZWxsQWxpZ24iLCJ0YXJnZXRVcmwiLCJjb25maXJtYXRpb25NZXNzYWdlIiwiaWNvbkNsYXNzIiwiZGVsZXRlQWN0aW9uIiwiY29uZmlybSIsIm1vZHVsZUl0ZW1JZCIsImZvcm1JZCIsImZ1bGxXaWR0aCIsInZ1ZURyb3B6b25lIiwidnVlMkRyb3B6b25lIiwiY3JlYXRlVXJsIiwiaW5kZXhVcmwiLCJkZWxldGVVcmwiLCJ0aHVtYm5haWxXaWR0aCIsImRyb3B6b25lT3B0aW9ucyIsImFkZFJlbW92ZUxpbmtzIiwicGFyYW1OYW1lIiwiZGVsZXRlZEZpbGVJbmRleCIsImRlbGV0ZUltYWdlIiwiaW1hZ2VfaWQiLCJpbWFnZUluZGV4IiwibWVzc2FnZUNsYXNzIiwiaGFzQmVlblNob3duIiwiZ2V0TWVzc2FnZUNsYXNzIiwiaGlkZSIsIm1lc3NhZ2VzIiwiZmxhc2hEYXRhIiwiZmxhc2hlcyIsImtleSIsIm1lc3NhZ2VEYXRhIiwiYWRkTWVzc2FnZSIsInNldHRpbmdWYWx1ZSIsInByaW1hcnlCdXR0b24iLCJpZGVudGlmaWVyIiwidmVydGljYWxseUNlbnRlcmVkIiwiaGFzRm9vdGVyIiwic2l6ZUNsYXNzIiwidmVydGljYWxseUNlbnRlcmVkQ2xhc3MiLCJuZWVkc0NvbmZpcm1hdGlvbiIsImxpbmtDbGFzcyIsInBvc3RBY3Rpb24iLCJpbmZpbml0ZSIsInNsaWRlc1RvU2hvdyIsInNsaWRlc1RvU2Nyb2xsIiwiZG90cyIsInNsaWNrIiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZWxlY3RlZCIsInBhcmVudCIsImlzQWN0aXZlIiwiaXNDaGlsZEFjdGl2ZSIsImFjdGl2YXRlIiwidGFicyIsIiRjaGlsZHJlbiIsInNlbGVjdFRhYiIsInNlbGVjdGVkVGFiIiwidGFiIiwiQVBJX0tFWSIsIkNBTExCQUNLX05BTUUiLCJpbml0aWFsaXplZCIsInJlc29sdmVJbml0UHJvbWlzZSIsInJlamVjdEluaXRQcm9taXNlIiwiaW5pdFByb21pc2UiLCJzY3JpcHQiLCJkZWZlciIsIm9uZXJyb3IiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLHVDOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQUEsTUFBTSxDQUFDQyxZQUFQLEdBQXNCLElBQUksTUFBTTtBQUM1QkMsYUFBVyxHQUFHO0FBQ1YsU0FBS0MsR0FBTCxHQUFXLElBQUlDLDJDQUFKLEVBQVg7QUFDSDs7QUFFREMsTUFBSSxDQUFDQyxLQUFELEVBQVFDLElBQUksR0FBRyxJQUFmLEVBQXFCO0FBQ3JCLFNBQUtKLEdBQUwsQ0FBU0ssS0FBVCxDQUFlRixLQUFmLEVBQXNCQyxJQUF0QjtBQUNIOztBQUVERSxRQUFNLENBQUNILEtBQUQsRUFBUUksUUFBUixFQUFrQjtBQUNwQixTQUFLUCxHQUFMLENBQVNRLEdBQVQsQ0FBYUwsS0FBYixFQUFvQkksUUFBcEI7QUFDSDs7QUFYMkIsQ0FBVixFQUF0Qjs7QUFjQVYsTUFBTSxDQUFDWSxLQUFQLEdBQWUsVUFBVUMsT0FBVixFQUFtQkMsS0FBSyxHQUFHLFNBQTNCLEVBQXNDO0FBQ2pEZCxRQUFNLENBQUNDLFlBQVAsQ0FBb0JJLElBQXBCLENBQXlCLE9BQXpCLEVBQWtDO0FBQUNRLFdBQUQ7QUFBVUM7QUFBVixHQUFsQztBQUNILENBRkQ7O0FBSUEsTUFBTUMsV0FBVyxHQUFHQyxxREFBVyxDQUFDQyxLQUFaLEVBQXBCOztBQUNBLE1BQU1DLE9BQU8sR0FBR0Msb0VBQWhCOztBQUNBSixXQUFXLENBQUNLLElBQVosQ0FBaUJDLHdGQUFzQixDQUFDSCxPQUFELENBQXZDO0FBRUFkLDJDQUFHLENBQUNrQixPQUFKLENBQVlDLFVBQVosR0FBeUIsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQUF6QjtBQUNBbkIsMkNBQUcsQ0FBQ29CLE1BQUosQ0FBV0MsZUFBWCxHQUE2QixDQUFDLFNBQUQsQ0FBN0I7QUFFQSxNQUFNQyxHQUFHLEdBQUcsSUFBSXRCLDJDQUFKLENBQVE7QUFDaEJ1QixJQUFFLEVBQUUsTUFEWTtBQUVoQnBCLE1BQUksRUFBRTtBQUNGcUIsYUFBUyxFQUFFLEtBRFQ7QUFFRkMscUJBQWlCLEVBQUU7QUFGakIsR0FGVTs7QUFNaEJDLFNBQU8sR0FBRztBQUNON0IsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQixlQUFwQixFQUFxQyxLQUFLc0IsWUFBMUM7QUFDQTlCLGdCQUFZLENBQUNRLE1BQWIsQ0FBb0Isa0JBQXBCLEVBQXdDLEtBQUt1QixlQUE3QztBQUNILEdBVGU7O0FBVWhCQyxTQUFPLEVBQUU7QUFDTEYsZ0JBQVksQ0FBQ3hCLElBQUQsRUFBTztBQUNmLFdBQUtzQixpQkFBTCxDQUF1QkssSUFBdkIsQ0FBNEIzQixJQUE1QjtBQUNILEtBSEk7O0FBS0x5QixtQkFBZSxDQUFDRyxXQUFELEVBQWM7QUFDekIsVUFBSUMsS0FBSyxHQUFHLEtBQUtQLGlCQUFMLENBQXVCUSxTQUF2QixDQUFpQ0MsSUFBSSxJQUFJQSxJQUFJLENBQUNDLEVBQUwsS0FBWUosV0FBckQsQ0FBWjtBQUVBLFdBQUtOLGlCQUFMLENBQXVCVyxNQUF2QixDQUE4QkosS0FBOUIsRUFBcUMsQ0FBckM7QUFDSDs7QUFUSTtBQVZPLENBQVIsQ0FBWjtBQXVCQUssNkNBQUMsQ0FBQyxZQUFZO0FBQ1ZBLCtDQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2QkMsT0FBN0I7QUFDSCxDQUZBLENBQUQ7QUFJQSxJQUFJQyxRQUFRLEdBQUdDLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIseUJBQTFCLENBQWY7QUFFQUYsUUFBUSxDQUFDRyxPQUFULENBQWlCQyxPQUFPLElBQUk7QUFDeEJBLFNBQU8sQ0FBQ0MsZ0JBQVIsQ0FBeUIsU0FBekIsRUFBb0MsWUFBWTtBQUM1Q0QsV0FBTyxDQUFDRSxjQUFSLENBQXVCLEtBQXZCO0FBQ0gsR0FGRDtBQUdILENBSkQ7QUFNQVIsNkNBQUMsQ0FBQyxZQUFXO0FBQ1RBLCtDQUFDLENBQUMsTUFBRCxDQUFELENBQVVTLE1BQVYsQ0FBaUIsVUFBUzVDLEtBQVQsRUFBZ0I7QUFDN0JtQyxpREFBQyxDQUFDLElBQUQsQ0FBRCxDQUFRVSxJQUFSLENBQWEsdUJBQWIsRUFBc0NDLElBQXRDLENBQTJDLFVBQTNDLEVBQXVELElBQXZEO0FBQ0gsR0FGRDtBQUdILENBSkEsQ0FBRCxDOzs7Ozs7Ozs7OztBQ2xIQSw2Q0FBSUMsS0FBSyxHQUFHVCxRQUFRLENBQUNVLElBQVQsQ0FBY0MsYUFBZCxDQUE0Qix5QkFBNUIsQ0FBWjs7QUFFQSxJQUFJRixLQUFKLEVBQVc7QUFDUFosR0FBQyxDQUFDZSxTQUFGLENBQVk7QUFDUkMsY0FBVSxFQUFFLFVBQVVDLEdBQVYsRUFBZUMsUUFBZixFQUF5QjtBQUNqQ0QsU0FBRyxDQUFDRSxnQkFBSixDQUFxQixhQUFyQixFQUFvQ1AsS0FBSyxDQUFDUSxPQUExQztBQUNIO0FBSE8sR0FBWjtBQUtILENBTkQsTUFNTztBQUNIQyxTQUFPLENBQUNDLEtBQVIsQ0FBYyx1QkFBZDtBQUNILEM7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1ZEO0FBRUEzRCwyQ0FBRyxDQUFDNEQsU0FBSixDQUFjLHFCQUFkLEVBQXFDO0FBQ2pDQyxNQUFJLEVBQUUsbUJBRDJCO0FBRWpDMUMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FGcUI7QUFHakMyQyxVQUFRLEVBQUUsc0JBSHVCO0FBSWpDQyxPQUFLLEVBQUU7QUFDSEMsNkJBQXlCLEVBQUU7QUFDdkJDLFVBQUksRUFBRUMsTUFEaUI7QUFFdkJDLGNBQVEsRUFBRTtBQUZhLEtBRHhCO0FBS0hDLGVBQVcsRUFBRTtBQUNURCxjQUFRLEVBQUUsSUFERDtBQUVURixVQUFJLEVBQUVDO0FBRkc7QUFMVixHQUowQjs7QUFlakMvRCxNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0hrRSxjQUFRLEVBQUU7QUFEUCxLQUFQO0FBR0gsR0FuQmdDOztBQXFCakNDLFVBQVEsRUFBRTtBQUNOQyxjQUFVLEdBQUc7QUFDVCxhQUFPLEtBQUtGLFFBQUwsS0FBa0IsSUFBekI7QUFDSDs7QUFISyxHQXJCdUI7QUEyQmpDeEMsU0FBTyxFQUFFO0FBQ0wyQyx5QkFBcUIsR0FBRztBQUNwQixVQUFJQyxLQUFLLEdBQUcsSUFBWjs7QUFFQSxXQUFLSixRQUFMLEdBQWdCLElBQWhCO0FBRUFoQyxPQUFDLENBQUNxQyxJQUFGLENBQU87QUFDSEMsY0FBTSxFQUFFLEtBREw7QUFFSEMsV0FBRyxFQUFFLEtBQUtaLHlCQUZQOztBQUdIYSxlQUFPLENBQUNDLFFBQUQsRUFBVztBQUNkTCxlQUFLLENBQUNKLFFBQU4sR0FBaUIsS0FBakI7O0FBRUEsY0FBSVMsUUFBUSxDQUFDQyxRQUFiLEVBQXVCO0FBQ25CMUMsYUFBQyxDQUFDLE1BQU1vQyxLQUFLLENBQUNPLEtBQU4sQ0FBWUMsS0FBWixDQUFrQkMsR0FBbEIsQ0FBc0JDLFlBQXRCLENBQW1DLElBQW5DLENBQVAsQ0FBRCxDQUFrREYsS0FBbEQsQ0FBd0QsTUFBeEQ7QUFDSCxXQUZELE1BRU87QUFDSHJGLGtCQUFNLENBQUN3RixRQUFQLENBQWdCQyxPQUFoQixDQUF3QlosS0FBSyxDQUFDTCxXQUE5QjtBQUNIO0FBQ0osU0FYRTs7QUFZSFQsYUFBSyxDQUFDQSxLQUFELEVBQVE7QUFDVGMsZUFBSyxDQUFDSixRQUFOLEdBQWlCLEtBQWpCO0FBQ0FYLGlCQUFPLENBQUM0QixHQUFSLENBQVkzQixLQUFaO0FBQ0g7O0FBZkUsT0FBUDtBQWlCSDs7QUF2Qkk7QUEzQndCLENBQXJDLEU7Ozs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7QUFFQTNELDJDQUFHLENBQUM0RCxTQUFKLENBQWMsb0JBQWQsRUFBb0M7QUFDaENFLFVBQVEsRUFBRyxvQ0FEcUI7O0FBRWhDcEMsU0FBTyxHQUFHO0FBQ04sU0FBSzZELE1BQUwsQ0FBWSwwRUFBWixFQUF3RixZQUFXO0FBQy9GN0IsYUFBTyxDQUFDNEIsR0FBUixDQUFZLG1DQUFaO0FBQ0gsS0FGRDtBQUdILEdBTitCOztBQU9oQ3pELFNBQU8sRUFBRTtBQUNMMEQsVUFBTSxDQUFDWCxHQUFELEVBQU10RSxRQUFOLEVBQWdCO0FBQ2xCK0IsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hFLFdBQUcsRUFBRUEsR0FERjtBQUVIWSxnQkFBUSxFQUFFLFFBRlA7QUFHSFgsZUFBTyxFQUFFdkUsUUFITjtBQUlIbUYsYUFBSyxFQUFFO0FBSkosT0FBUDtBQU1IOztBQVJJO0FBUHVCLENBQXBDLEU7Ozs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQXpGLDJDQUFHLENBQUM0RCxTQUFKLENBQWMsbUJBQWQsRUFBbUM7QUFDL0J6QyxZQUFVLEVBQUUsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQURtQjtBQUUvQjJDLFVBQVEsRUFBRSxvQkFGcUI7QUFHL0JDLE9BQUssRUFBRTtBQUNIMkIsY0FBVSxFQUFFO0FBQ1J2QixjQUFRLEVBQUUsS0FERjtBQUVSRixVQUFJLEVBQUUwQixPQUZFO0FBR1JDLGFBQU8sRUFBRTtBQUhELEtBRFQ7QUFNSEMsWUFBUSxFQUFFO0FBQ04xQixjQUFRLEVBQUUsS0FESjtBQUVORixVQUFJLEVBQUVDLE1BRkE7QUFHTjBCLGFBQU8sRUFBRTtBQUhILEtBTlA7QUFXSEUsVUFBTSxFQUFFO0FBQ0ozQixjQUFRLEVBQUUsS0FETjtBQUVKRixVQUFJLEVBQUVDLE1BRkY7QUFHSjBCLGFBQU8sRUFBRTtBQUhMLEtBWEw7QUFnQkhHLFVBQU0sRUFBRTtBQUNKNUIsY0FBUSxFQUFFLEtBRE47QUFFSkYsVUFBSSxFQUFFQyxNQUZGO0FBR0owQixhQUFPLEVBQUU7QUFITCxLQWhCTDtBQXFCSEksZ0JBQVksRUFBRTtBQUNWN0IsY0FBUSxFQUFFLEtBREE7QUFFVkYsVUFBSSxFQUFFQyxNQUZJO0FBR1YwQixhQUFPLEVBQUU7QUFIQyxLQXJCWDtBQTBCSEssaUJBQWEsRUFBRTtBQUNYOUIsY0FBUSxFQUFFLEtBREM7QUFFWEYsVUFBSSxFQUFFQyxNQUZLO0FBR1gwQixhQUFPLEVBQUU7QUFIRTtBQTFCWixHQUh3Qjs7QUFtQy9CekYsTUFBSSxHQUFHO0FBQ0gsV0FBTztBQUNIK0Ysa0JBQVksRUFBRSxLQURYO0FBRUhDLGlCQUFXLEVBQUUsRUFGVjtBQUdIQyxrQkFBWSxFQUFFLEVBSFg7QUFJSG5DLFVBQUksRUFBRSxVQUpIO0FBS0hvQyxZQUFNLEVBQUUsT0FMTDtBQU1IQyxhQUFPLEVBQUUsTUFOTjtBQU9IQyx3QkFBa0IsRUFBRSxNQVBqQjtBQVFIQyxhQUFPLEVBQUUsRUFSTjtBQVNIQyxXQUFLLEVBQUUsT0FUSjtBQVVIQyxzQkFBZ0IsRUFBRSxPQVZmO0FBV0hDLDZCQUF1QixFQUFFLE1BWHRCO0FBWUhDLDhCQUF3QixFQUFFLE1BWnZCO0FBYUhDLFVBQUksRUFBQyxPQWJGO0FBY0hDLHFCQUFlLEVBQUUsUUFkZDtBQWVIQyxlQUFTLEVBQUUsSUFmUjtBQWdCSEMsaUJBQVcsRUFBRSxFQWhCVjtBQWlCSEMsZ0JBQVUsRUFBRSxFQWpCVDtBQWtCSEMsa0JBQVksRUFBRSxFQWxCWDtBQW1CSHRCLGFBQU8sRUFBRSxJQW5CTjtBQW9CSHVCLHFCQUFlLEVBQUUsRUFwQmQ7QUFxQkhDLG9CQUFjLEVBQUUsRUFyQmI7QUFzQkhDLGNBQVEsRUFBRTtBQXRCUCxLQUFQO0FBd0JILEdBNUQ4Qjs7QUE2RC9CM0YsU0FBTyxHQUFHO0FBQ04sU0FBSzRGLE9BQUwsQ0FBYSxLQUFLQyxhQUFMLEVBQWIsRUFBbUMsSUFBbkM7QUFFQTFILGdCQUFZLENBQUNRLE1BQWIsQ0FBb0IsZ0JBQXBCLEVBQXNDLEtBQUttSCxhQUEzQztBQUNBM0gsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQixpQkFBcEIsRUFBdUMsS0FBS29ILGNBQTVDO0FBQ0gsR0FsRThCOztBQW1FL0I1RixTQUFPLEVBQUU7QUFDTDBGLGlCQUFhLEdBQUc7QUFDWixVQUFJLEtBQUt4QixNQUFULEVBQWlCO0FBQ2IsZUFBTztBQUNIQSxnQkFBTSxFQUFFLEtBQUtBLE1BRFY7QUFFSE0sZ0JBQU0sRUFBRSxLQUFLTCxZQUZWO0FBR0hNLGlCQUFPLEVBQUUsS0FBS0w7QUFIWCxTQUFQO0FBS0g7O0FBRUQsYUFBTztBQUNIYyxpQkFBUyxFQUFFLElBRFI7QUFFSEQsdUJBQWUsRUFBRSxRQUZkO0FBR0g3QyxZQUFJLEVBQUUsVUFISDtBQUlIb0MsY0FBTSxFQUFFO0FBSkwsT0FBUDtBQU1ILEtBaEJJOztBQWtCTGlCLFdBQU8sQ0FBQ0ksTUFBTSxHQUFHLEtBQUtILGFBQUwsRUFBVixFQUFnQ0ksZ0JBQWdCLEdBQUcsS0FBbkQsRUFBMEQ7QUFDN0QsVUFBSS9DLEdBQUcsR0FBRyxxQkFBcUJ2QyxDQUFDLENBQUN1RixLQUFGLENBQVFGLE1BQVIsQ0FBL0I7QUFBQSxVQUNJakQsS0FBSyxHQUFHLElBRFo7O0FBR0EsV0FBS3lCLFlBQUwsR0FBb0IsSUFBcEI7QUFFQTdELE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIRSxXQUFHLEVBQUVBLEdBREY7QUFFSEQsY0FBTSxFQUFFLEtBRkw7QUFHSEUsZUFBTyxFQUFFLFVBQVVDLFFBQVYsRUFBb0I7QUFDekJMLGVBQUssQ0FBQzBCLFdBQU4sR0FBb0JyQixRQUFRLENBQUNxQixXQUFULENBQXFCMEIsR0FBckIsQ0FBeUJDLEtBQUssSUFBSUMsVUFBVSxDQUFDRCxLQUFELENBQTVDLENBQXBCO0FBQ0FyRCxlQUFLLENBQUMyQixZQUFOLEdBQXFCdEIsUUFBUSxDQUFDc0IsWUFBVCxDQUFzQnlCLEdBQXRCLENBQTBCQyxLQUFLLElBQUlDLFVBQVUsQ0FBQ0QsS0FBRCxDQUE3QyxDQUFyQjtBQUVBckQsZUFBSyxDQUFDcUMsZUFBTixHQUF3QmhDLFFBQVEsQ0FBQ2tELFVBQVQsQ0FBb0JqQyxNQUFwQixDQUEyQmUsZUFBbkQ7QUFDQXJDLGVBQUssQ0FBQ3NDLFNBQU4sR0FBa0JqQyxRQUFRLENBQUNrRCxVQUFULENBQW9CakMsTUFBcEIsQ0FBMkJnQixTQUEzQixLQUF5QyxJQUF6QyxHQUFnRCxLQUFoRCxHQUF3RCxJQUExRTtBQUNBdEMsZUFBSyxDQUFDUixJQUFOLEdBQWFhLFFBQVEsQ0FBQ2tELFVBQVQsQ0FBb0JqQyxNQUFwQixDQUEyQjlCLElBQXhDO0FBQ0FRLGVBQUssQ0FBQzRCLE1BQU4sR0FBZXZCLFFBQVEsQ0FBQ2tELFVBQVQsQ0FBb0IzQixNQUFuQztBQUNBNUIsZUFBSyxDQUFDNkIsT0FBTixHQUFnQnhCLFFBQVEsQ0FBQ2tELFVBQVQsQ0FBb0IxQixPQUFwQztBQUNBN0IsZUFBSyxDQUFDOEIsa0JBQU4sR0FBMkJ6QixRQUFRLENBQUNrRCxVQUFULENBQW9CekIsa0JBQS9DO0FBQ0E5QixlQUFLLENBQUMrQixPQUFOLEdBQWdCMUIsUUFBUSxDQUFDa0QsVUFBVCxDQUFvQnhCLE9BQXBDO0FBQ0EvQixlQUFLLENBQUNnQyxLQUFOLEdBQWMzQixRQUFRLENBQUNrRCxVQUFULENBQW9CdkIsS0FBbEM7QUFDQWhDLGVBQUssQ0FBQ2lDLGdCQUFOLEdBQXlCNUIsUUFBUSxDQUFDa0QsVUFBVCxDQUFvQnRCLGdCQUE3QztBQUNBakMsZUFBSyxDQUFDa0MsdUJBQU4sR0FBZ0M3QixRQUFRLENBQUNrRCxVQUFULENBQW9CckIsdUJBQXBEO0FBQ0FsQyxlQUFLLENBQUNtQyx3QkFBTixHQUFpQzlCLFFBQVEsQ0FBQ2tELFVBQVQsQ0FBb0JwQix3QkFBckQ7QUFDQW5DLGVBQUssQ0FBQ29DLElBQU4sR0FBYS9CLFFBQVEsQ0FBQ2tELFVBQVQsQ0FBb0JuQixJQUFqQztBQUNBcEMsZUFBSyxDQUFDdUMsV0FBTixHQUFvQmxDLFFBQVEsQ0FBQ2tELFVBQVQsQ0FBb0JqQyxNQUFwQixDQUEyQmtDLEtBQS9DO0FBQ0F4RCxlQUFLLENBQUN3QyxVQUFOLEdBQW1CbkMsUUFBUSxDQUFDa0QsVUFBVCxDQUFvQmpDLE1BQXBCLENBQTJCbUMsSUFBOUM7QUFDQXpELGVBQUssQ0FBQ3lDLFlBQU4sR0FBcUJwQyxRQUFRLENBQUNrRCxVQUFULENBQW9CakMsTUFBcEIsQ0FBMkJtQixZQUFoRDtBQUNBekMsZUFBSyxDQUFDbUIsT0FBTixHQUFnQmQsUUFBUSxDQUFDa0QsVUFBVCxDQUFvQmpDLE1BQXBCLENBQTJCb0MsYUFBM0M7QUFDQTFELGVBQUssQ0FBQzBDLGVBQU4sR0FBd0JyQyxRQUFRLENBQUNrRCxVQUFULENBQW9CSSxTQUE1QztBQUNBM0QsZUFBSyxDQUFDMkMsY0FBTixHQUF1QnRDLFFBQVEsQ0FBQ2tELFVBQVQsQ0FBb0JaLGNBQTNDO0FBQ0EzQyxlQUFLLENBQUM0QyxRQUFOLEdBQWlCdkMsUUFBUSxDQUFDa0QsVUFBVCxDQUFvQnRCLGdCQUFwQixHQUF1QzVCLFFBQVEsQ0FBQ2tELFVBQVQsQ0FBb0IzQixNQUE1RTtBQUVBNUIsZUFBSyxDQUFDeUIsWUFBTixHQUFxQixLQUFyQjs7QUFFQSxjQUFJeUIsZ0JBQUosRUFBc0I7QUFDbEJsRCxpQkFBSyxDQUFDNEQsZ0JBQU47QUFDSDs7QUFFRDVELGVBQUssQ0FBQzZELGlCQUFOOztBQUVBekksc0JBQVksQ0FBQ0ksSUFBYixDQUFrQiwyQkFBbEIsRUFBK0M7QUFDM0NzSSxtQkFBTyxFQUFFOUQsS0FBSyxDQUFDdUMsV0FENEI7QUFFM0NrQixnQkFBSSxFQUFFekQsS0FBSyxDQUFDd0MsVUFGK0I7QUFHM0NaLGtCQUFNLEVBQUU1QixLQUFLLENBQUM0QixNQUg2QjtBQUkzQ21DLGtCQUFNLEVBQUUvRCxLQUFLLENBQUMrQixPQUo2QjtBQUszQ0YsbUJBQU8sRUFBRTdCLEtBQUssQ0FBQzZCLE9BTDRCO0FBTTNDYSwyQkFBZSxFQUFFMUMsS0FBSyxDQUFDMEMsZUFOb0I7QUFPM0NDLDBCQUFjLEVBQUUzQyxLQUFLLENBQUMyQyxjQVBxQjtBQVEzQ0Ysd0JBQVksRUFBRXpDLEtBQUssQ0FBQ3lDO0FBUnVCLFdBQS9DO0FBVUgsU0E3Q0U7QUE4Q0h2RCxhQUFLLEVBQUUsVUFBVW1CLFFBQVYsRUFBb0I7QUFDdkJwQixpQkFBTyxDQUFDNEIsR0FBUixDQUFZUixRQUFaO0FBQ0g7QUFoREUsT0FBUDtBQWtESCxLQTFFSTs7QUE0RUwwQyxpQkFBYSxDQUFDTSxLQUFELEVBQVE7QUFDakIsV0FBS3pCLE1BQUwsR0FBY3lCLEtBQUssQ0FBQ1csT0FBTixDQUFjLENBQWQsQ0FBZDtBQUVBLFVBQUlDLFVBQVUsR0FBRztBQUNickMsY0FBTSxFQUFFLEtBQUtBLE1BREE7QUFFYlUsaUJBQVMsRUFBRSxLQUFLQSxTQUZIO0FBR2JELHVCQUFlLEVBQUUsS0FBS0EsZUFIVDtBQUliN0MsWUFBSSxFQUFFLEtBQUtBO0FBSkUsT0FBakI7QUFPQSxXQUFLcUQsT0FBTCxDQUFhb0IsVUFBYjtBQUNILEtBdkZJOztBQXlGTGpCLGtCQUFjLENBQUNLLEtBQUQsRUFBUTtBQUNsQixXQUFLdkIsa0JBQUwsR0FBMEJ1QixLQUFLLENBQUNXLE9BQU4sQ0FBYyxDQUFkLENBQTFCO0FBRUEsVUFBSUMsVUFBVSxHQUFHO0FBQ2JwQyxlQUFPLEVBQUUsS0FBS0Msa0JBREQ7QUFFYkYsY0FBTSxFQUFFLEtBQUtBLE1BRkE7QUFHYlUsaUJBQVMsRUFBRSxLQUFLQSxTQUhIO0FBSWJELHVCQUFlLEVBQUUsS0FBS0EsZUFKVDtBQUtiN0MsWUFBSSxFQUFFLEtBQUtBO0FBTEUsT0FBakI7QUFRQSxXQUFLcUQsT0FBTCxDQUFhb0IsVUFBYjtBQUNILEtBckdJOztBQXVHTEMsMEJBQXNCLEdBQUc7QUFDckIsVUFBSSxLQUFLN0IsZUFBTCxLQUF5QixTQUF6QixJQUFzQyxLQUFLN0MsSUFBTCxLQUFjLFVBQXhELEVBQW9FO0FBQ2hFLGFBQUs4QyxTQUFMLEdBQWlCLElBQWpCO0FBQ0g7O0FBRUQsVUFBSTJCLFVBQVUsR0FBRztBQUNiNUIsdUJBQWUsRUFBRSxLQUFLQSxlQURUO0FBRWI3QyxZQUFJLEVBQUUsS0FBS0EsSUFGRTtBQUdiOEMsaUJBQVMsRUFBRSxLQUFLQSxTQUhIO0FBSWJWLGNBQU0sRUFBRSxLQUFLdUM7QUFKQSxPQUFqQjtBQU9BLFdBQUt0QixPQUFMLENBQWFvQixVQUFiLEVBQXlCLElBQXpCO0FBQ0gsS0FwSEk7O0FBc0hMRyxvQkFBZ0IsR0FBRztBQUNmLFVBQUlILFVBQVUsR0FBRyxFQUFqQjs7QUFFQSxVQUFJLEtBQUszQixTQUFMLEtBQW1CLEtBQXZCLEVBQThCO0FBQzFCLGFBQUtELGVBQUwsR0FBdUIsU0FBdkI7QUFFQTRCLGtCQUFVLEdBQUc7QUFDVDVCLHlCQUFlLEVBQUUsS0FBS0EsZUFEYjtBQUVUN0MsY0FBSSxFQUFFLEtBQUtBLElBRkY7QUFHVDhDLG1CQUFTLEVBQUUsS0FBS0EsU0FIUDtBQUlUVixnQkFBTSxFQUFFLEtBQUt1QztBQUpKLFNBQWI7QUFNSCxPQVRELE1BU087QUFDSCxhQUFLOUIsZUFBTCxHQUF1QixRQUF2QjtBQUVBNEIsa0JBQVUsR0FBRztBQUNUNUIseUJBQWUsRUFBRSxLQUFLQSxlQURiO0FBRVQ3QyxjQUFJLEVBQUUsS0FBS0EsSUFGRjtBQUdUOEMsbUJBQVMsRUFBRSxLQUFLQSxTQUhQO0FBSVRWLGdCQUFNLEVBQUUsS0FBS3VDO0FBSkosU0FBYjtBQU1IOztBQUVELFdBQUt0QixPQUFMLENBQWFvQixVQUFiLEVBQXlCLElBQXpCO0FBQ0gsS0E5SUk7O0FBZ0pMSSxjQUFVLENBQUNDLE9BQUQsRUFBVTtBQUNoQixVQUFJLEtBQUs5RSxJQUFMLEtBQWM4RSxPQUFsQixFQUEyQjtBQUN2QjtBQUNIOztBQUVELFdBQUs5RSxJQUFMLEdBQVk4RSxPQUFaOztBQUVBLFVBQUlBLE9BQU8sS0FBSyxVQUFoQixFQUE0QjtBQUN4QixhQUFLaEMsU0FBTCxHQUFpQixJQUFqQjtBQUNIOztBQUVELFdBQUtELGVBQUwsR0FBdUIsUUFBdkI7QUFFQSxVQUFJNEIsVUFBVSxHQUFHO0FBQ2I1Qix1QkFBZSxFQUFFLEtBQUtBLGVBRFQ7QUFFYjdDLFlBQUksRUFBRSxLQUFLQSxJQUZFO0FBR2I4QyxpQkFBUyxFQUFFLEtBQUtBO0FBSEgsT0FBakI7O0FBTUEsVUFBSWdDLE9BQU8sS0FBSyxVQUFoQixFQUE0QjtBQUN4Qkwsa0JBQVUsQ0FBQ3JDLE1BQVgsR0FBb0IsT0FBcEI7QUFDQXFDLGtCQUFVLENBQUNwQyxPQUFYLEdBQXFCLEtBQXJCO0FBQ0g7O0FBRUQsV0FBS2dCLE9BQUwsQ0FBYW9CLFVBQWIsRUFBeUIsSUFBekI7QUFDSCxLQXpLSTs7QUEyS0xMLG9CQUFnQixHQUFHO0FBQ2YsVUFBSVcsWUFBWSxHQUFHM0csQ0FBQyxDQUFDLEtBQUsyQyxLQUFMLENBQVdxQixNQUFaLENBQUQsQ0FBcUJsRyxJQUFyQixDQUEwQixnQkFBMUIsQ0FBbkI7QUFBQSxVQUNJNkIsS0FBSyxHQUFHLEtBQUttRSxXQUFMLENBQWlCbEUsU0FBakIsQ0FBMkI2RixLQUFLLElBQUlBLEtBQUssQ0FBQ1csT0FBTixDQUFjLENBQWQsTUFBcUIsS0FBS3BDLE1BQTlELENBRFo7O0FBR0EsVUFBSTJDLFlBQUosRUFBa0I7QUFDZEEsb0JBQVksQ0FBQ0MsT0FBYjtBQUNIOztBQUVENUcsT0FBQyxDQUFDLEtBQUsyQyxLQUFMLENBQVdxQixNQUFaLENBQUQsQ0FBcUI2QyxjQUFyQixDQUFvQztBQUNoQ0MsWUFBSSxFQUFFLE1BRDBCO0FBRWhDQyxZQUFJLEVBQUUsSUFGMEI7QUFHaENDLGlCQUFTLEVBQUUsSUFIcUI7QUFJaENDLGNBQU0sRUFBRSxLQUFLbkQsV0FKbUI7QUFLaENvRCxZQUFJLEVBQUV2SCxLQUwwQjtBQU1oQ3dILGVBQU8sRUFBRSxNQUFNLEtBQUszRCxRQU5ZO0FBT2hDNEQsbUJBQVcsRUFBRSxLQVBtQjtBQVFoQ0MsZ0JBQVEsRUFBRSxVQUFVdkosSUFBVixFQUFnQjtBQUN0Qk4sc0JBQVksQ0FBQ0ksSUFBYixDQUFrQixnQkFBbEIsRUFBb0NFLElBQUksQ0FBQ3dKLFVBQXpDO0FBQ0g7QUFWK0IsT0FBcEM7QUFhQVgsa0JBQVksR0FBRzNHLENBQUMsQ0FBQyxLQUFLMkMsS0FBTCxDQUFXcUIsTUFBWixDQUFELENBQXFCbEcsSUFBckIsQ0FBMEIsZ0JBQTFCLENBQWY7QUFDQTZJLGtCQUFZLENBQUNZLE1BQWIsQ0FBb0I7QUFBQ0wsWUFBSSxFQUFFdkg7QUFBUCxPQUFwQjtBQUNILEtBbE1JOztBQW9NTHNHLHFCQUFpQixHQUFHO0FBQ2hCLFVBQUl1QixhQUFhLEdBQUd4SCxDQUFDLENBQUMsS0FBSzJDLEtBQUwsQ0FBV3NCLE9BQVosQ0FBRCxDQUFzQm5HLElBQXRCLENBQTJCLGdCQUEzQixDQUFwQjtBQUFBLFVBQ0kySixvQkFBb0IsR0FBRyxLQUFLMUQsWUFEaEM7O0FBR0EsVUFBSXlELGFBQUosRUFBbUI7QUFDZkEscUJBQWEsQ0FBQ1osT0FBZDtBQUNIOztBQUVELFVBQUlhLG9CQUFvQixDQUFDQyxNQUFyQixLQUFnQyxDQUFwQyxFQUF1QztBQUNuQ0QsNEJBQW9CLENBQUNoSSxJQUFyQixDQUEwQmdJLG9CQUFvQixDQUFDLENBQUQsQ0FBOUM7QUFDSDs7QUFFRCxVQUFJOUgsS0FBSyxHQUFHLEtBQUtvRSxZQUFMLENBQWtCbkUsU0FBbEIsQ0FBNEI2RixLQUFLLElBQUlBLEtBQUssQ0FBQ1csT0FBTixDQUFjLENBQWQsTUFBcUIsS0FBS2xDLGtCQUEvRCxDQUFaO0FBRUFsRSxPQUFDLENBQUMsS0FBSzJDLEtBQUwsQ0FBV3NCLE9BQVosQ0FBRCxDQUFzQjRDLGNBQXRCLENBQXFDO0FBQ2pDQyxZQUFJLEVBQUUsTUFEMkI7QUFFakNDLFlBQUksRUFBRSxJQUYyQjtBQUdqQ0MsaUJBQVMsRUFBRSxJQUhzQjtBQUlqQ0MsY0FBTSxFQUFFUSxvQkFKeUI7QUFLakNQLFlBQUksRUFBRXZILEtBTDJCO0FBTWpDd0gsZUFBTyxFQUFFLE1BQU0sS0FBSzNELFFBTmE7QUFPakM0RCxtQkFBVyxFQUFFLEtBUG9CO0FBUWpDQyxnQkFBUSxFQUFFLFVBQVV2SixJQUFWLEVBQWdCO0FBQ3RCTixzQkFBWSxDQUFDSSxJQUFiLENBQWtCLGlCQUFsQixFQUFxQ0UsSUFBSSxDQUFDd0osVUFBMUM7QUFDSDtBQVZnQyxPQUFyQztBQWFBRSxtQkFBYSxHQUFHeEgsQ0FBQyxDQUFDLEtBQUsyQyxLQUFMLENBQVdzQixPQUFaLENBQUQsQ0FBc0JuRyxJQUF0QixDQUEyQixnQkFBM0IsQ0FBaEI7QUFDQTBKLG1CQUFhLENBQUNELE1BQWQsQ0FBcUI7QUFBQ0wsWUFBSSxFQUFFdkg7QUFBUCxPQUFyQjtBQUNILEtBak9JOztBQW1PTGdJLGVBQVcsR0FBRztBQUNWbkssa0JBQVksQ0FBQ0ksSUFBYixDQUFrQixxQkFBbEIsRUFBeUM7QUFDckNnRSxZQUFJLEVBQUUsS0FBS0EsSUFEMEI7QUFFckNvQyxjQUFNLEVBQUUsS0FBS0EsTUFGd0I7QUFHckNDLGVBQU8sRUFBRSxLQUFLQyxrQkFIdUI7QUFJckNBLDBCQUFrQixFQUFFLEtBQUtBLGtCQUpZO0FBS3JDQyxlQUFPLEVBQUUsS0FBS0EsT0FMdUI7QUFNckN5RCxnQkFBUSxFQUFFLEtBQUtBLFFBTnNCO0FBT3JDeEQsYUFBSyxFQUFFLEtBQUtDLGdCQVB5QjtBQVFyQ0Esd0JBQWdCLEVBQUUsS0FBS0EsZ0JBUmM7QUFTckNJLHVCQUFlLEVBQUUsS0FBS0EsZUFUZTtBQVVyQ0gsK0JBQXVCLEVBQUUsS0FBS3VELG1CQVZPO0FBV3JDdEQsZ0NBQXdCLEVBQUUsS0FBS3VELG9CQVhNO0FBWXJDdEQsWUFBSSxFQUFFLEtBQUt1RCxZQVowQjtBQWFyQy9DLGdCQUFRLEVBQUUsS0FBS0EsUUFic0I7QUFjckNOLGlCQUFTLEVBQUUsS0FBS0EsU0FkcUI7QUFlckNrQixhQUFLLEVBQUUsS0FBS2pCLFdBZnlCO0FBZ0JyQ2tCLFlBQUksRUFBRSxLQUFLakIsVUFoQjBCO0FBaUJyQ0Msb0JBQVksRUFBRSxLQUFLQTtBQWpCa0IsT0FBekM7QUFtQkgsS0F2UEk7O0FBeVBMbUQsU0FBSyxHQUFHO0FBQ0osVUFBSXpGLEdBQUcsR0FBSSwwQkFBeUIsS0FBS3FDLFVBQVcsSUFBRyxLQUFLbkIsTUFBTyxFQUFuRTtBQUFBLFVBQ0lyQixLQUFLLEdBQUcsSUFEWjs7QUFHQXBDLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIRSxXQUFHLEVBQUVBLEdBREY7QUFFSEQsY0FBTSxFQUFFLEtBRkw7QUFHSEUsZUFBTyxFQUFFLFVBQVVDLFFBQVYsRUFBb0I7QUFDekJsRixnQkFBTSxDQUFDd0YsUUFBUCxDQUFnQmtGLElBQWhCLEdBQXdCLEdBQUV4RixRQUFTLFlBQVdMLEtBQUssQ0FBQzRCLE1BQU8sWUFBVzVCLEtBQUssQ0FBQzZCLE9BQVEsV0FBVTdCLEtBQUssQ0FBQytCLE9BQVEsRUFBNUc7QUFDSCxTQUxFO0FBTUg3QyxhQUFLLEVBQUUsVUFBVW1CLFFBQVYsRUFBb0I7QUFDdkJwQixpQkFBTyxDQUFDNEIsR0FBUixDQUFZUixRQUFaO0FBQ0g7QUFSRSxPQUFQO0FBVUg7O0FBdlFJLEdBbkVzQjtBQTRVL0JSLFVBQVEsRUFBRTtBQUNOMkYsWUFBUSxHQUFHO0FBQ1AsVUFBSSxLQUFLbkQsZUFBTCxLQUF5QixRQUE3QixFQUF1QztBQUNuQyxlQUFPeUQsSUFBSSxDQUFDQyxJQUFMLENBQVUsS0FBS2hFLE9BQUwsR0FBZSxJQUF6QixDQUFQO0FBQ0gsT0FGRCxNQUVPLElBQUksS0FBS00sZUFBTCxLQUF5QixVQUE3QixFQUF5QztBQUM1QyxlQUFPeUQsSUFBSSxDQUFDQyxJQUFMLENBQVUsS0FBS2hFLE9BQUwsR0FBZSxDQUF6QixDQUFQO0FBQ0gsT0FGTSxNQUVBO0FBQ0gsZUFBTyxLQUFLQSxPQUFaO0FBQ0g7QUFDSixLQVRLOztBQVdOMEQsdUJBQW1CLEdBQUc7QUFDbEIsYUFBTyxDQUFDLEtBQUt2RCx1QkFBTCxHQUErQixHQUFoQyxFQUFxQzhCLE9BQXJDLENBQTZDLENBQTdDLElBQWtELEdBQXpEO0FBQ0gsS0FiSzs7QUFlTjBCLHdCQUFvQixHQUFHO0FBQ25CLGFBQU8sQ0FBQyxLQUFLdkQsd0JBQUwsR0FBZ0MsR0FBakMsRUFBc0M2QixPQUF0QyxDQUE4QyxDQUE5QyxJQUFtRCxHQUExRDtBQUNILEtBakJLOztBQW1CTjJCLGdCQUFZLEdBQUc7QUFDWCxhQUFPLENBQUMsS0FBS3ZELElBQUwsR0FBWSxHQUFiLEVBQWtCNEIsT0FBbEIsQ0FBMEIsQ0FBMUIsSUFBK0IsR0FBdEM7QUFDSCxLQXJCSzs7QUF1Qk5OLGlCQUFhLEdBQUc7QUFDWixhQUFPLEtBQUt2QyxPQUFMLEtBQWlCLElBQWpCLEdBQXdCLEtBQUtTLE1BQTdCLEdBQXNDLEtBQUtULE9BQWxEO0FBQ0gsS0F6Qks7O0FBMEJOZ0QsbUNBQStCLEdBQUc7QUFDOUIsVUFBSSxLQUFLOUIsZUFBTCxLQUF5QixRQUE3QixFQUF1QztBQUNuQyxlQUFPLE9BQVA7QUFDSDs7QUFFRCxVQUFJLEtBQUtBLGVBQUwsS0FBeUIsVUFBN0IsRUFBeUM7QUFDckMsZUFBTyxPQUFQO0FBQ0g7O0FBRUQsVUFBSSxLQUFLQSxlQUFMLEtBQXlCLFNBQTdCLEVBQXdDO0FBQ3BDLFlBQUksS0FBS0MsU0FBTCxLQUFtQixJQUF2QixFQUE2QjtBQUN6QixpQkFBTyxPQUFQO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsaUJBQU8sT0FBUDtBQUNIO0FBQ0o7QUFDSixLQTFDSzs7QUEyQ04wRCxrQkFBYyxHQUFHO0FBQ2IsYUFBTztBQUNILG9CQUFZLEtBQUsvRSxVQUFMLEtBQW9CLEtBRDdCO0FBRUgsb0JBQVksS0FBS0EsVUFBTCxLQUFvQjtBQUY3QixPQUFQO0FBSUgsS0FoREs7O0FBaUROZ0YsZUFBVyxHQUFHO0FBQ1YsYUFBTztBQUNILHNCQUFjLEtBQUtoRixVQUFMLEtBQW9CO0FBRC9CLE9BQVA7QUFHSDs7QUFyREssR0E1VXFCO0FBbVkvQmlGLE9BQUssRUFBRTtBQUNIMUcsUUFBSSxFQUFFLFlBQVk7QUFDZHBFLGtCQUFZLENBQUNJLElBQWIsQ0FBa0IscUJBQWxCO0FBQ0g7QUFIRTtBQW5Zd0IsQ0FBbkMsRTs7Ozs7Ozs7Ozs7OztBQ0pBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBRCwyQ0FBRyxDQUFDNEQsU0FBSixDQUFjLG1CQUFkLEVBQW1DO0FBQy9CekMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FEbUI7QUFFL0IyQyxVQUFRLEVBQUUsb0JBRnFCO0FBRy9CQyxPQUFLLEVBQUU7QUFDSDZHLHdCQUFvQixFQUFFO0FBQ2xCekcsY0FBUSxFQUFFLEtBRFE7QUFFbEJGLFVBQUksRUFBRTBCLE9BRlk7QUFHbEJDLGFBQU8sRUFBRTtBQUhTLEtBRG5CO0FBTUhpRixvQkFBZ0IsRUFBRTtBQUNkMUcsY0FBUSxFQUFFLEtBREk7QUFFZEYsVUFBSSxFQUFFMEIsT0FGUTtBQUdkQyxhQUFPLEVBQUU7QUFISyxLQU5mO0FBV0hrRix1QkFBbUIsRUFBRTtBQUNqQjNHLGNBQVEsRUFBRSxLQURPO0FBRWpCRixVQUFJLEVBQUUwQixPQUZXO0FBR2pCQyxhQUFPLEVBQUU7QUFIUTtBQVhsQixHQUh3Qjs7QUFvQi9CekYsTUFBSSxHQUFHO0FBQ0gsV0FBTztBQUNINEssWUFBTSxFQUFFO0FBREwsS0FBUDtBQUdILEdBeEI4Qjs7QUF5Qi9CckosU0FBTyxHQUFHO0FBQ043QixnQkFBWSxDQUFDUSxNQUFiLENBQW9CLHFCQUFwQixFQUEyQyxLQUFLMkosV0FBaEQ7QUFDQW5LLGdCQUFZLENBQUNRLE1BQWIsQ0FBb0IscUJBQXBCLEVBQTJDLEtBQUsySyxXQUFoRDtBQUNBbkwsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQixxQkFBcEIsRUFBMkMsS0FBSzJLLFdBQWhEO0FBQ0gsR0E3QjhCOztBQThCL0JuSixTQUFPLEVBQUU7QUFDTG1JLGVBQVcsQ0FBQzdKLElBQUQsRUFBTztBQUNkLFVBQUksS0FBSzRLLE1BQUwsQ0FBWWhCLE1BQVosSUFBc0IsQ0FBMUIsRUFBNkI7QUFDekI7QUFDSDs7QUFFRCxXQUFLZ0IsTUFBTCxDQUFZakosSUFBWixDQUFpQjNCLElBQWpCO0FBQ0gsS0FQSTs7QUFTTDZLLGVBQVcsQ0FBQ2hKLEtBQUQsRUFBUTtBQUNmLFVBQUlBLEtBQUssS0FBSyxJQUFkLEVBQW9CO0FBQ2hCLGFBQUsrSSxNQUFMLEdBQWMsRUFBZDtBQUNIOztBQUVELFdBQUtBLE1BQUwsQ0FBWTNJLE1BQVosQ0FBbUJKLEtBQW5CLEVBQTBCLENBQTFCO0FBQ0g7O0FBZkk7QUE5QnNCLENBQW5DLEU7Ozs7Ozs7Ozs7OztBQ0pBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBaEMsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxjQUFkLEVBQThCO0FBQzFCekMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FEYztBQUUxQjJDLFVBQVEsRUFBRSxlQUZnQjtBQUcxQkMsT0FBSyxFQUFFO0FBQ0gvQixTQUFLLEVBQUU7QUFDSG1DLGNBQVEsRUFBRSxJQURQO0FBRUhGLFVBQUksRUFBRWdIO0FBRkgsS0FESjtBQUtIcEYsWUFBUSxFQUFFO0FBQ04xQixjQUFRLEVBQUUsS0FESjtBQUVORixVQUFJLEVBQUVDLE1BRkE7QUFHTjBCLGFBQU8sRUFBRTtBQUhILEtBTFA7QUFVSEUsVUFBTSxFQUFFO0FBQ0ozQixjQUFRLEVBQUUsS0FETjtBQUVKRixVQUFJLEVBQUVDLE1BRkY7QUFHSjBCLGFBQU8sRUFBRTtBQUhMLEtBVkw7QUFlSDNCLFFBQUksRUFBRTtBQUNGRSxjQUFRLEVBQUUsS0FEUjtBQUVGRixVQUFJLEVBQUVDLE1BRko7QUFHRjBCLGFBQU8sRUFBRTtBQUhQLEtBZkg7QUFvQkhTLFVBQU0sRUFBRTtBQUNKbEMsY0FBUSxFQUFFLEtBRE47QUFFSkYsVUFBSSxFQUFFQyxNQUZGO0FBR0owQixhQUFPLEVBQUU7QUFITCxLQXBCTDtBQXlCSFUsV0FBTyxFQUFFO0FBQ0xuQyxjQUFRLEVBQUUsS0FETDtBQUVMRixVQUFJLEVBQUVDLE1BRkQ7QUFHTDBCLGFBQU8sRUFBRTtBQUhKLEtBekJOO0FBOEJIVyxzQkFBa0IsRUFBRTtBQUNoQnBDLGNBQVEsRUFBRSxLQURNO0FBRWhCRixVQUFJLEVBQUVDLE1BRlU7QUFHaEIwQixhQUFPLEVBQUU7QUFITyxLQTlCakI7QUFtQ0hZLFdBQU8sRUFBRTtBQUNMckMsY0FBUSxFQUFFLEtBREw7QUFFTEYsVUFBSSxFQUFFZ0gsTUFGRDtBQUdMckYsYUFBTyxFQUFFO0FBSEosS0FuQ047QUF3Q0hxRSxZQUFRLEVBQUU7QUFDTjlGLGNBQVEsRUFBRSxLQURKO0FBRU5GLFVBQUksRUFBRWdILE1BRkE7QUFHTnJGLGFBQU8sRUFBRTtBQUhILEtBeENQO0FBNkNIYSxTQUFLLEVBQUU7QUFDSHRDLGNBQVEsRUFBRSxLQURQO0FBRUhGLFVBQUksRUFBRUMsTUFGSDtBQUdIMEIsYUFBTyxFQUFFO0FBSE4sS0E3Q0o7QUFrREhjLG9CQUFnQixFQUFFO0FBQ2R2QyxjQUFRLEVBQUUsS0FESTtBQUVkRixVQUFJLEVBQUVDLE1BRlE7QUFHZDBCLGFBQU8sRUFBRTtBQUhLLEtBbERmO0FBdURIZSwyQkFBdUIsRUFBRTtBQUNyQnhDLGNBQVEsRUFBRSxLQURXO0FBRXJCRixVQUFJLEVBQUVDLE1BRmU7QUFHckIwQixhQUFPLEVBQUU7QUFIWSxLQXZEdEI7QUE0REhnQiw0QkFBd0IsRUFBRTtBQUN0QnpDLGNBQVEsRUFBRSxLQURZO0FBRXRCRixVQUFJLEVBQUVDLE1BRmdCO0FBR3RCMEIsYUFBTyxFQUFFO0FBSGEsS0E1RHZCO0FBaUVIaUIsUUFBSSxFQUFFO0FBQ0YxQyxjQUFRLEVBQUUsS0FEUjtBQUVGRixVQUFJLEVBQUVDLE1BRko7QUFHRjBCLGFBQU8sRUFBRTtBQUhQLEtBakVIO0FBc0VIeUIsWUFBUSxFQUFFO0FBQ05sRCxjQUFRLEVBQUUsS0FESjtBQUVORixVQUFJLEVBQUVnSCxNQUZBO0FBR05yRixhQUFPLEVBQUU7QUFISCxLQXRFUDtBQTJFSGtCLG1CQUFlLEVBQUU7QUFDYjNDLGNBQVEsRUFBRSxLQURHO0FBRWJGLFVBQUksRUFBRUMsTUFGTztBQUdiMEIsYUFBTyxFQUFFO0FBSEksS0EzRWQ7QUFnRkhtQixhQUFTLEVBQUU7QUFDUDVDLGNBQVEsRUFBRSxLQURIO0FBRVBGLFVBQUksRUFBRUMsTUFGQztBQUdQMEIsYUFBTyxFQUFFO0FBSEYsS0FoRlI7QUFxRkhxQyxTQUFLLEVBQUU7QUFDSDlELGNBQVEsRUFBRSxLQURQO0FBRUhGLFVBQUksRUFBRUMsTUFGSDtBQUdIMEIsYUFBTyxFQUFFO0FBSE4sS0FyRko7QUEwRkhzQyxRQUFJLEVBQUU7QUFDRi9ELGNBQVEsRUFBRSxLQURSO0FBRUZGLFVBQUksRUFBRUMsTUFGSjtBQUdGMEIsYUFBTyxFQUFFO0FBSFAsS0ExRkg7QUErRkhzQixnQkFBWSxFQUFFO0FBQ1YvQyxjQUFRLEVBQUUsS0FEQTtBQUVWRixVQUFJLEVBQUVDLE1BRkk7QUFHVjBCLGFBQU8sRUFBRTtBQUhDO0FBL0ZYLEdBSG1CO0FBd0cxQi9ELFNBQU8sRUFBRTtBQUNMcUosU0FBSyxHQUFHO0FBQ0pyTCxrQkFBWSxDQUFDSSxJQUFiLENBQWtCLHFCQUFsQixFQUF5QyxLQUFLK0IsS0FBOUM7QUFDSCxLQUhJOztBQUtMcUksU0FBSyxHQUFHO0FBQ0osVUFBSXpGLEdBQUcsR0FBSSwwQkFBeUIsS0FBS3NELElBQUssSUFBRyxLQUFLcEMsTUFBTyxFQUE3RDtBQUFBLFVBQ0lyQixLQUFLLEdBQUcsSUFEWjs7QUFHQXBDLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIRSxXQUFHLEVBQUVBLEdBREY7QUFFSEQsY0FBTSxFQUFFLEtBRkw7QUFHSEUsZUFBTyxFQUFFLFVBQVVDLFFBQVYsRUFBb0I7QUFDekJsRixnQkFBTSxDQUFDd0YsUUFBUCxDQUFnQmtGLElBQWhCLEdBQXdCLEdBQUV4RixRQUFTLFlBQVdMLEtBQUssQ0FBQzRCLE1BQU8sWUFBVzVCLEtBQUssQ0FBQzZCLE9BQVEsV0FBVTdCLEtBQUssQ0FBQytCLE9BQVEsRUFBNUc7QUFDSCxTQUxFO0FBTUg3QyxhQUFLLEVBQUUsVUFBVW1CLFFBQVYsRUFBb0I7QUFDdkJwQixpQkFBTyxDQUFDNEIsR0FBUixDQUFZUixRQUFaO0FBQ0g7QUFSRSxPQUFQO0FBV0g7O0FBcEJJO0FBeEdpQixDQUE5QixFOzs7Ozs7Ozs7Ozs7O0FDSkE7QUFBQTtBQUFBO0FBRUE5RSwyQ0FBRyxDQUFDNEQsU0FBSixDQUFjLGtCQUFkLEVBQWtDO0FBQzlCekMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FEa0I7QUFFOUIyQyxVQUFRLEVBQUUsbUJBRm9CO0FBRzlCQyxPQUFLLEVBQUU7QUFDSG9ILFlBQVEsRUFBRTtBQUNOaEgsY0FBUSxFQUFFLElBREo7QUFFTkYsVUFBSSxFQUFFZ0g7QUFGQTtBQURQLEdBSHVCOztBQVM5QjlLLE1BQUksR0FBRztBQUNILFdBQU87QUFDSGlMLGtCQUFZLEVBQUU7QUFEWCxLQUFQO0FBR0gsR0FiNkI7O0FBYzlCdkosU0FBTyxFQUFFO0FBQ0x3SixtQkFBZSxHQUFHO0FBQ2QsVUFBSXpHLEdBQUcsR0FBSSxxQkFBb0IsS0FBS3VHLFFBQVMsc0JBQTdDO0FBQUEsVUFDSTFHLEtBQUssR0FBRyxJQURaOztBQUdBcEMsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLGNBQU0sRUFBRSxLQURMO0FBRUhDLFdBQUcsRUFBRUEsR0FGRjs7QUFHSEMsZUFBTyxDQUFDQyxRQUFELEVBQVc7QUFDZEwsZUFBSyxDQUFDMkcsWUFBTixHQUFxQnRHLFFBQVEsQ0FBQ3NHLFlBQTlCO0FBQ0gsU0FMRTs7QUFNSHpILGFBQUssQ0FBQ0EsS0FBRCxFQUFRO0FBQ1RuRCxlQUFLLENBQUNtRCxLQUFLLENBQUMsY0FBRCxDQUFOLEVBQXdCLE9BQXhCLENBQUw7QUFDSDs7QUFSRSxPQUFQO0FBVUg7O0FBZkksR0FkcUI7QUErQjlCVyxVQUFRLEVBQUU7QUFDTmdILHlCQUFxQixHQUFHO0FBQ3BCLFVBQUksS0FBS0YsWUFBTCxLQUFzQixJQUExQixFQUFnQztBQUM1QixlQUFPLEVBQVA7QUFDSDs7QUFFRCxVQUFJLEtBQUtBLFlBQUwsR0FBb0IsQ0FBeEIsRUFBMkI7QUFDdkIsZUFBUSxHQUFFLEtBQUtBLFlBQUwsQ0FBa0IzQyxPQUFsQixDQUEwQixDQUExQixDQUE2QixNQUF2QztBQUNIOztBQUVELGFBQU8sRUFBUDtBQUNILEtBWEs7O0FBWU44QyxTQUFLLEdBQUc7QUFDSixVQUFJLEtBQUtILFlBQUwsS0FBc0IsSUFBMUIsRUFBZ0M7QUFDNUIsZUFBTyxTQUFQO0FBQ0g7O0FBRUQsVUFBSSxLQUFLQSxZQUFMLEdBQW9CLENBQXhCLEVBQTJCO0FBQ3ZCLGVBQU8sV0FBUDtBQUNIOztBQUVELFVBQUksS0FBS0EsWUFBTCxLQUFzQixDQUExQixFQUE2QjtBQUN6QixlQUFPLHdCQUFQO0FBQ0g7O0FBRUQsVUFBSSxLQUFLQSxZQUFMLEtBQXNCLENBQUMsQ0FBM0IsRUFBOEI7QUFDMUIsZUFBTyxPQUFQO0FBQ0g7O0FBRUQsYUFBTyxTQUFQO0FBQ0g7O0FBOUJLO0FBL0JvQixDQUFsQyxFOzs7Ozs7Ozs7Ozs7O0FDRkE7QUFBQTtBQUFBO0FBRUFwTCwyQ0FBRyxDQUFDNEQsU0FBSixDQUFjLGtCQUFkLEVBQWtDO0FBQzlCekMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FEa0I7QUFFOUIyQyxVQUFRLEVBQUUsbUJBRm9CO0FBRzlCQyxPQUFLLEVBQUU7QUFDSG9ILFlBQVEsRUFBRTtBQUNOaEgsY0FBUSxFQUFFLElBREo7QUFFTkYsVUFBSSxFQUFFZ0g7QUFGQTtBQURQLEdBSHVCOztBQVM5QjlLLE1BQUksR0FBRztBQUNILFdBQU87QUFDSHFMLGtCQUFZLEVBQUU7QUFEWCxLQUFQO0FBR0gsR0FiNkI7O0FBYzlCM0osU0FBTyxFQUFFO0FBQ0w0SixtQkFBZSxHQUFHO0FBQ2QsVUFBSTdHLEdBQUcsR0FBSSxxQkFBb0IsS0FBS3VHLFFBQVMsc0JBQTdDO0FBQUEsVUFDSTFHLEtBQUssR0FBRyxJQURaOztBQUdBcEMsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLGNBQU0sRUFBRSxLQURMO0FBRUhDLFdBQUcsRUFBRUEsR0FGRjs7QUFHSEMsZUFBTyxDQUFDQyxRQUFELEVBQVc7QUFDZEwsZUFBSyxDQUFDK0csWUFBTixHQUFxQjFHLFFBQVEsQ0FBQzBHLFlBQTlCO0FBQ0gsU0FMRTs7QUFNSDdILGFBQUssQ0FBQ0EsS0FBRCxFQUFRO0FBQ1RuRCxlQUFLLENBQUNtRCxLQUFLLENBQUMsY0FBRCxDQUFOLEVBQXdCLE9BQXhCLENBQUw7QUFDSDs7QUFSRSxPQUFQO0FBVUgsS0FmSTs7QUFnQkwrSCxzQkFBa0IsR0FBRztBQUNqQixVQUFJOUcsR0FBRyxHQUFJLGlDQUFYO0FBQUEsVUFDSXpFLElBQUksR0FBRztBQUFDZ0wsZ0JBQVEsRUFBRSxLQUFLQSxRQUFoQjtBQUEwQjlFLGNBQU0sRUFBRSxLQUFLbUYsWUFBdkM7QUFBcURHLHdCQUFnQixFQUFFO0FBQXZFLE9BRFg7QUFHQXRKLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsTUFETDtBQUVIeEUsWUFBSSxFQUFFQSxJQUZIO0FBR0h5RSxXQUFHLEVBQUVBLEdBSEY7O0FBSUhDLGVBQU8sQ0FBQ0MsUUFBRCxFQUFXO0FBQ2RsRixnQkFBTSxDQUFDd0YsUUFBUCxDQUFnQmtGLElBQWhCLEdBQXVCeEYsUUFBUSxDQUFDOEcsWUFBaEM7QUFDSCxTQU5FOztBQU9IakksYUFBSyxDQUFDQSxLQUFELEVBQVE7QUFDVG5ELGVBQUssQ0FBQ21ELEtBQUssQ0FBQyxjQUFELENBQU4sRUFBd0IsT0FBeEIsQ0FBTDtBQUNIOztBQVRFLE9BQVA7QUFXSDs7QUEvQkksR0FkcUI7QUErQzlCVyxVQUFRLEVBQUU7QUFDTnVILHlCQUFxQixHQUFHO0FBQ3BCLFVBQUksS0FBS0wsWUFBTCxLQUFzQixJQUExQixFQUFnQztBQUM1QixlQUFPLEVBQVA7QUFDSDs7QUFFRCxVQUFJLEtBQUtBLFlBQUwsR0FBb0IsQ0FBeEIsRUFBMkI7QUFDdkIsZUFBUSxHQUFFLEtBQUtBLFlBQUwsQ0FBa0IvQyxPQUFsQixDQUEwQixDQUExQixDQUE2QixNQUF2QztBQUNIOztBQUVELGFBQU8sRUFBUDtBQUNILEtBWEs7O0FBWU44QyxTQUFLLEdBQUc7QUFDSixVQUFJLEtBQUtDLFlBQUwsS0FBc0IsSUFBMUIsRUFBZ0M7QUFDNUIsZUFBTyxTQUFQO0FBQ0g7O0FBRUQsVUFBSSxLQUFLQSxZQUFMLEdBQW9CLENBQXhCLEVBQTJCO0FBQ3ZCLGVBQU8sS0FBUDtBQUNIOztBQUVELFVBQUksS0FBS0EsWUFBTCxLQUFzQixDQUExQixFQUE2QjtBQUN6QixlQUFPLHdCQUFQO0FBQ0g7O0FBRUQsVUFBSSxLQUFLQSxZQUFMLEtBQXNCLENBQUMsQ0FBM0IsRUFBOEI7QUFDMUIsZUFBTyxPQUFQO0FBQ0g7O0FBRUQsYUFBTyxTQUFQO0FBQ0g7O0FBOUJLO0FBL0NvQixDQUFsQyxFOzs7Ozs7Ozs7Ozs7O0FDRkE7QUFBQTtBQUFBO0FBRUF4TCwyQ0FBRyxDQUFDNEQsU0FBSixDQUFjLDhCQUFkLEVBQThDO0FBQzFDekMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FEOEI7QUFFMUMyQyxVQUFRLEVBQUUsK0JBRmdDO0FBRzFDQyxPQUFLLEVBQUU7QUFDSCtILGlCQUFhLEVBQUU7QUFDWDNILGNBQVEsRUFBRSxLQURDO0FBRVhGLFVBQUksRUFBRWdILE1BRks7QUFHWHJGLGFBQU8sRUFBRTtBQUhFLEtBRFo7QUFNSHVGLFlBQVEsRUFBRTtBQUNOaEgsY0FBUSxFQUFFLElBREo7QUFFTkYsVUFBSSxFQUFFQztBQUZBO0FBTlAsR0FIbUM7O0FBYzFDL0QsTUFBSSxHQUFHO0FBQ0gsV0FBTztBQUNIa0csWUFBTSxFQUFFLElBREw7QUFFSDBGLFlBQU0sRUFBRTtBQUZMLEtBQVA7QUFJSCxHQW5CeUM7O0FBb0IxQ3JLLFNBQU8sR0FBRztBQUNOLFNBQUsyRSxNQUFMLEdBQWMsS0FBS3lGLGFBQUwsQ0FBbUJyRCxPQUFuQixDQUEyQixDQUEzQixDQUFkO0FBQ0gsR0F0QnlDOztBQXVCMUM1RyxTQUFPLEVBQUU7QUFDTGlCLFVBQU0sR0FBRztBQUNMLFVBQUk4QixHQUFHLEdBQUksaUNBQVg7QUFBQSxVQUNJekUsSUFBSSxHQUFHO0FBQUNnTCxnQkFBUSxFQUFFLEtBQUtBLFFBQWhCO0FBQTBCOUUsY0FBTSxFQUFFLEtBQUtBLE1BQXZDO0FBQStDc0Ysd0JBQWdCLEVBQUU7QUFBakUsT0FEWDtBQUFBLFVBRUlsSCxLQUFLLEdBQUcsSUFGWjs7QUFJQXBDLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsTUFETDtBQUVIeEUsWUFBSSxFQUFFQSxJQUZIO0FBR0h5RSxXQUFHLEVBQUVBLEdBSEY7O0FBSUhDLGVBQU8sQ0FBQ0MsUUFBRCxFQUFXO0FBQ2RsRixnQkFBTSxDQUFDd0YsUUFBUCxDQUFnQmtGLElBQWhCLEdBQXVCeEYsUUFBUSxDQUFDOEcsWUFBaEM7QUFDSCxTQU5FOztBQU9IakksYUFBSyxDQUFDbUIsUUFBRCxFQUFXO0FBQ1pMLGVBQUssQ0FBQ3NILE1BQU4sR0FBZWpILFFBQVEsQ0FBQ2tILFlBQVQsQ0FBc0JELE1BQXJDO0FBQ0g7O0FBVEUsT0FBUDtBQVdIOztBQWpCSSxHQXZCaUM7QUEwQzFDekgsVUFBUSxFQUFFO0FBQ04ySCxZQUFRLEdBQUc7QUFDUCxhQUFPLEtBQUtGLE1BQUwsQ0FBWWhDLE1BQVosR0FBcUIsQ0FBNUI7QUFDSDs7QUFISztBQTFDZ0MsQ0FBOUMsRTs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUVBL0osMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxvQkFBZCxFQUFvQztBQUNoQ3pDLFlBQVUsRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBRG9CO0FBRWhDMkMsVUFBUSxFQUFHOzs7Ozs7Q0FGcUI7QUFTaENDLE9BQUssRUFBRTtBQUNIbUksZUFBVyxFQUFFO0FBQ1QvSCxjQUFRLEVBQUUsSUFERDtBQUVURixVQUFJLEVBQUVDO0FBRkcsS0FEVjtBQUtIaUksY0FBVSxFQUFFO0FBQ1JoSSxjQUFRLEVBQUUsSUFERjtBQUVSRixVQUFJLEVBQUVDO0FBRkUsS0FMVDtBQVNIa0ksU0FBSyxFQUFFO0FBQ0hqSSxjQUFRLEVBQUUsSUFEUDtBQUVIRixVQUFJLEVBQUVDO0FBRkgsS0FUSjtBQWFINEIsVUFBTSxFQUFFO0FBQ0ozQixjQUFRLEVBQUUsSUFETjtBQUVKRixVQUFJLEVBQUVDO0FBRkY7QUFiTCxHQVR5Qjs7QUEyQmhDL0QsTUFBSSxHQUFHO0FBQ0gsV0FBTztBQUNIc0QsYUFBTyxFQUFFO0FBRE4sS0FBUDtBQUdILEdBL0IrQjs7QUFnQ2hDL0IsU0FBTyxHQUFHO0FBQ04sU0FBSzJLLFVBQUw7QUFDSCxHQWxDK0I7O0FBbUNoQ3hLLFNBQU8sRUFBRTtBQUNMd0ssY0FBVSxHQUFHO0FBQ1QsVUFBSXpILEdBQUcsR0FBSSxrQ0FBaUMwSCxTQUFTLENBQUMsS0FBS0osV0FBTixDQUFtQixJQUFHLEtBQUtDLFVBQVcsSUFBRyxLQUFLQyxLQUFNLElBQUcsS0FBS3RHLE1BQU8sRUFBeEg7QUFBQSxVQUNJckIsS0FBSyxHQUFHLElBRFo7O0FBR0FwQyxPQUFDLENBQUNxQyxJQUFGLENBQU87QUFDSEMsY0FBTSxFQUFFLEtBREw7QUFFSEMsV0FBRyxFQUFFQSxHQUZGOztBQUdIQyxlQUFPLENBQUNDLFFBQUQsRUFBVztBQUNkTCxlQUFLLENBQUNoQixPQUFOLEdBQWdCcUIsUUFBUSxDQUFDckIsT0FBekI7QUFDSCxTQUxFOztBQU1IRSxhQUFLLENBQUNBLEtBQUQsRUFBUTtBQUNURCxpQkFBTyxDQUFDNEIsR0FBUixDQUFZM0IsS0FBWjtBQUNIOztBQVJFLE9BQVA7QUFVSCxLQWZJOztBQWdCTDRJLGVBQVcsR0FBRztBQUNWLFVBQUkzSCxHQUFHLEdBQUksOEJBQTZCMEgsU0FBUyxDQUFDLEtBQUtKLFdBQU4sQ0FBbUIsSUFBRyxLQUFLQyxVQUFXLElBQUcsS0FBS0MsS0FBTSxJQUFHLEtBQUt0RyxNQUFPLEVBQXBIO0FBQUEsVUFDSXJCLEtBQUssR0FBRyxJQURaO0FBQUEsVUFFSXRFLElBQUksR0FBRztBQUFDc0QsZUFBTyxFQUFFLEtBQUtBO0FBQWYsT0FGWDs7QUFJQXBCLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsTUFETDtBQUVIQyxXQUFHLEVBQUVBLEdBRkY7QUFHSHpFLFlBQUksRUFBRUEsSUFISDs7QUFJSDBFLGVBQU8sR0FBRztBQUNOckUsZUFBSyxDQUFDLDBCQUFELEVBQTZCLFNBQTdCLENBQUw7QUFDSCxTQU5FOztBQU9IbUQsYUFBSyxDQUFDQSxLQUFELEVBQVE7QUFDVEQsaUJBQU8sQ0FBQzRCLEdBQVIsQ0FBWTNCLEtBQVo7QUFDQW5ELGVBQUssQ0FBQyxpQ0FBRCxFQUFvQyxPQUFwQyxDQUFMO0FBQ0g7O0FBVkUsT0FBUDtBQVlIOztBQWpDSTtBQW5DdUIsQ0FBcEMsRTs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUVBUiwyQ0FBRyxDQUFDNEQsU0FBSixDQUFjLDBCQUFkLEVBQTBDO0FBQ3RDekMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FEMEI7QUFFdEMyQyxVQUFRLEVBQUc7Ozs7Q0FGMkI7QUFPdENDLE9BQUssRUFBRTtBQUNIbUksZUFBVyxFQUFFO0FBQ1QvSCxjQUFRLEVBQUUsSUFERDtBQUVURixVQUFJLEVBQUVDO0FBRkcsS0FEVjtBQUtIaUksY0FBVSxFQUFFO0FBQ1JoSSxjQUFRLEVBQUUsSUFERjtBQUVSRixVQUFJLEVBQUVDO0FBRkUsS0FMVDtBQVNIa0ksU0FBSyxFQUFFO0FBQ0hqSSxjQUFRLEVBQUUsSUFEUDtBQUVIRixVQUFJLEVBQUVDO0FBRkgsS0FUSjtBQWFIc0ksV0FBTyxFQUFFO0FBQ0xySSxjQUFRLEVBQUUsSUFETDtBQUVMRixVQUFJLEVBQUVDO0FBRkQ7QUFiTixHQVArQjs7QUF5QnRDL0QsTUFBSSxHQUFHO0FBQ0gsV0FBTztBQUNIc00sa0JBQVksRUFBRSxFQURYO0FBRUhDLGtCQUFZLEVBQUU7QUFGWCxLQUFQO0FBSUgsR0E5QnFDOztBQStCdENoTCxTQUFPLEdBQUc7QUFDTixTQUFLK0ssWUFBTCxHQUFvQixLQUFLRCxPQUFMLENBQWFHLEtBQWIsQ0FBbUIsR0FBbkIsQ0FBcEI7QUFDSDs7QUFqQ3FDLENBQTFDLEU7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUVBM00sMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxxQkFBZCxFQUFxQztBQUNqQ3pDLFlBQVUsRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBRHFCO0FBRWpDMkMsVUFBUSxFQUFHOzs7Ozs7Ozs7Ozs7Ozs7O0tBRnNCO0FBbUJqQ0MsT0FBSyxFQUFFO0FBQ0g2SSxZQUFRLEVBQUU7QUFDTnpJLGNBQVEsRUFBRSxJQURKO0FBRU5GLFVBQUksRUFBRUM7QUFGQSxLQURQO0FBS0gySSxTQUFLLEVBQUU7QUFDSDFJLGNBQVEsRUFBRSxJQURQO0FBRUhGLFVBQUksRUFBRUM7QUFGSCxLQUxKO0FBU0hDLFlBQVEsRUFBRTtBQUNOQSxjQUFRLEVBQUUsS0FESjtBQUVORixVQUFJLEVBQUUwQixPQUZBO0FBR05DLGFBQU8sRUFBRTtBQUhILEtBVFA7QUFjSGtILFVBQU0sRUFBRTtBQUNKM0ksY0FBUSxFQUFFLElBRE47QUFFSkYsVUFBSSxFQUFFQztBQUZGO0FBZEwsR0FuQjBCOztBQXNDakMvRCxNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0g0TCxZQUFNLEVBQUU7QUFETCxLQUFQO0FBR0gsR0ExQ2dDOztBQTJDakNySyxTQUFPLEdBQUc7QUFDTixTQUFLc0QsS0FBTCxDQUFXLE9BQVgsSUFBdUIsS0FBS0UsR0FBTCxDQUFTL0IsYUFBVCxDQUF1QixPQUF2QixDQUF2QjtBQUVBLFNBQUs2QixLQUFMLENBQVcrSCxLQUFYLENBQWlCbkssZ0JBQWpCLENBQWtDLE9BQWxDLEVBQTJDLEtBQUtvSyxRQUFoRDtBQUNILEdBL0NnQzs7QUFnRGpDbkwsU0FBTyxFQUFFO0FBQ0xtTCxZQUFRLEdBQUc7QUFDUCxVQUFJcEksR0FBRyxHQUFJLCtCQUFYO0FBQUEsVUFDSXpFLElBQUksR0FBRztBQUFDeU0sZ0JBQVEsRUFBRSxLQUFLQSxRQUFoQjtBQUEwQkUsY0FBTSxFQUFFLEtBQUtBLE1BQXZDO0FBQStDaEYsYUFBSyxFQUFFLEtBQUs5QyxLQUFMLENBQVcrSCxLQUFYLENBQWlCakY7QUFBdkUsT0FEWDtBQUFBLFVBRUlyRCxLQUFLLEdBQUcsSUFGWjs7QUFJQXBDLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsTUFETDtBQUVIeEUsWUFBSSxFQUFFQSxJQUZIO0FBR0h5RSxXQUFHLEVBQUVBLEdBSEY7O0FBSUhDLGVBQU8sR0FBRztBQUNOSixlQUFLLENBQUNzSCxNQUFOLEdBQWUsRUFBZjs7QUFDQXRILGVBQUssQ0FBQ08sS0FBTixDQUFZK0gsS0FBWixDQUFrQkUsU0FBbEIsQ0FBNEJDLE1BQTVCLENBQW1DLFlBQW5DO0FBQ0gsU0FQRTs7QUFRSHZKLGFBQUssQ0FBQ21CLFFBQUQsRUFBVztBQUNaTCxlQUFLLENBQUNzSCxNQUFOLEdBQWVqSCxRQUFRLENBQUNrSCxZQUFULENBQXNCRCxNQUFyQzs7QUFFQSxjQUFJdEgsS0FBSyxDQUFDc0gsTUFBTixDQUFhaEMsTUFBYixHQUFzQixDQUExQixFQUE2QjtBQUN6QnRGLGlCQUFLLENBQUNPLEtBQU4sQ0FBWStILEtBQVosQ0FBa0JFLFNBQWxCLENBQTRCRSxHQUE1QixDQUFnQyxZQUFoQztBQUNIO0FBQ0o7O0FBZEUsT0FBUDtBQWdCSDs7QUF0QkksR0FoRHdCO0FBd0VqQzdJLFVBQVEsRUFBRTtBQUNOOEksYUFBUyxHQUFHO0FBQ1IsYUFBTyxLQUFLckIsTUFBTCxDQUFZaEMsTUFBWixHQUFxQixDQUE1QjtBQUNIOztBQUhLO0FBeEV1QixDQUFyQyxFOzs7Ozs7Ozs7Ozs7O0FDRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBL0osMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxPQUFkLEVBQXVCO0FBQ25CekMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FETztBQUVuQjJDLFVBQVEsRUFBRSxRQUZTO0FBR25CQyxPQUFLLEVBQUU7QUFDSHNKLFdBQU8sRUFBRTtBQUNMbEosY0FBUSxFQUFFLEtBREw7QUFFTEYsVUFBSSxFQUFFQyxNQUZEO0FBR0wwQixhQUFPLEVBQUU7QUFISixLQUROO0FBTUgwSCxRQUFJLEVBQUU7QUFDRm5KLGNBQVEsRUFBRSxLQURSO0FBRUZGLFVBQUksRUFBRWdILE1BRko7QUFHRnJGLGFBQU8sRUFBRTtBQUhQLEtBTkg7QUFXSDNCLFFBQUksRUFBRTtBQUNGRSxjQUFRLEVBQUUsS0FEUjtBQUVGRixVQUFJLEVBQUVDLE1BRko7QUFHRjBCLGFBQU8sRUFBRTtBQUhQLEtBWEg7QUFnQkgySCxXQUFPLEVBQUU7QUFDTHBKLGNBQVEsRUFBRSxLQURMO0FBRUxGLFVBQUksRUFBRXVKLEtBRkQ7QUFHTDVILGFBQU8sRUFBRTtBQUhKLEtBaEJOO0FBcUJINkgsVUFBTSxFQUFFO0FBQ0p0SixjQUFRLEVBQUUsS0FETjtBQUVKRixVQUFJLEVBQUV1SixLQUZGO0FBR0o1SCxhQUFPLEVBQUU7QUFITCxLQXJCTDtBQTBCSDhILFdBQU8sRUFBRTtBQUNMdkosY0FBUSxFQUFFLElBREw7QUFFTEYsVUFBSSxFQUFFQztBQUZELEtBMUJOO0FBOEJIeUosV0FBTyxFQUFFO0FBQ0x4SixjQUFRLEVBQUUsSUFETDtBQUVMRixVQUFJLEVBQUVDO0FBRkQsS0E5Qk47QUFrQ0gwSixjQUFVLEVBQUU7QUFDUnpKLGNBQVEsRUFBRSxJQURGO0FBRVJGLFVBQUksRUFBRUM7QUFGRSxLQWxDVDtBQXNDSDJKLG9CQUFnQixFQUFFO0FBQ2Q1SixVQUFJLEVBQUUwQixPQURRO0FBRWR4QixjQUFRLEVBQUUsS0FGSTtBQUdkeUIsYUFBTyxFQUFFO0FBSEs7QUF0Q2YsR0FIWTs7QUErQ25CekYsTUFBSSxHQUFHO0FBQ0gsV0FBTztBQUNIMk4sc0JBQWdCLEVBQUUsS0FEZjtBQUVIakcsU0FBRyxFQUFFLElBRkY7QUFHSGtHLGNBQVEsRUFBRSxJQUhQO0FBSUhDLGtCQUFZLEVBQUUsS0FBS1gsT0FKaEI7QUFLSFksb0JBQWMsRUFBRTtBQUNacEssWUFBSSxFQUFFLElBRE07QUFFWnFLLFlBQUksRUFBRSxJQUZNO0FBR1pDLGlCQUFTLEVBQUUsSUFIQztBQUlaQyxtQkFBVyxFQUFFLEVBSkQ7QUFLWkMscUJBQWEsRUFBRTtBQUxILE9BTGI7QUFZSEMsWUFBTSxFQUFFLENBQ0o7QUFDSTFKLFdBQUcsRUFBRSxLQUFLOEksT0FEZDtBQUVJYSxhQUFLLEVBQUUsRUFGWDtBQUdJQyxjQUFNLEVBQUUsRUFIWjtBQUlJQyxpQkFBUyxFQUFFLFNBSmY7QUFLSUMsZ0JBQVEsRUFBRTtBQUxkLE9BREksRUFRSjtBQUNJOUosV0FBRyxFQUFFLEtBQUsrSSxPQURkO0FBRUlZLGFBQUssRUFBRSxFQUZYO0FBR0lDLGNBQU0sRUFBRSxFQUhaO0FBSUlDLGlCQUFTLEVBQUUsU0FKZjtBQUtJQyxnQkFBUSxFQUFFO0FBTGQsT0FSSSxFQWVKO0FBQ0k5SixXQUFHLEVBQUUsS0FBSytJLE9BRGQ7QUFFSVksYUFBSyxFQUFFLEVBRlg7QUFHSUMsY0FBTSxFQUFFLEVBSFo7QUFJSUMsaUJBQVMsRUFBRSxTQUpmO0FBS0lDLGdCQUFRLEVBQUU7QUFMZCxPQWZJLENBWkw7QUFtQ0hDLGVBQVMsRUFBRSxDQUNQO0FBQ0ksdUJBQWUsZ0JBRG5CO0FBRUksdUJBQWUsa0JBRm5CO0FBR0ksbUJBQVcsQ0FDUDtBQUNJLG1CQUFTO0FBRGIsU0FETztBQUhmLE9BRE8sRUFVUDtBQUNJLHVCQUFlLFdBRG5CO0FBRUksdUJBQWUsS0FGbkI7QUFHSSxtQkFBVyxDQUNQO0FBQ0ksbUJBQVM7QUFEYixTQURPO0FBSGYsT0FWTyxFQW1CUDtBQUNJLHVCQUFlLEtBRG5CO0FBRUksdUJBQWUsS0FGbkI7QUFHSSxtQkFBVyxDQUNQO0FBQ0ksd0JBQWM7QUFEbEIsU0FETztBQUhmLE9BbkJPLEVBNEJQO0FBQ0ksdUJBQWUsTUFEbkI7QUFFSSx1QkFBZSxLQUZuQjtBQUdJLG1CQUFXLENBQ1A7QUFDSSx3QkFBYyxDQUFDO0FBRG5CLFNBRE8sRUFJUDtBQUNJLHVCQUFhO0FBRGpCLFNBSk87QUFIZixPQTVCTyxFQXdDUDtBQUNJLHVCQUFlLE1BRG5CO0FBRUksdUJBQWUsa0JBRm5CO0FBR0ksbUJBQVcsQ0FDUDtBQUNJLG1CQUFTO0FBRGIsU0FETztBQUhmLE9BeENPLEVBaURQO0FBQ0ksdUJBQWUsY0FEbkI7QUFFSSx1QkFBZSxLQUZuQjtBQUdJLG1CQUFXLENBQ1A7QUFDSSx3QkFBYztBQURsQixTQURPO0FBSGYsT0FqRE8sRUEwRFA7QUFDSSx1QkFBZSxlQURuQjtBQUVJLHVCQUFlLGFBRm5CO0FBR0ksbUJBQVcsQ0FDUDtBQUNJLHdCQUFjO0FBRGxCLFNBRE87QUFIZixPQTFETyxFQW1FUDtBQUNJLHVCQUFlLE9BRG5CO0FBRUksdUJBQWUsZUFGbkI7QUFHSSxtQkFBVyxDQUNQO0FBQ0ksd0JBQWM7QUFEbEIsU0FETyxFQUlQO0FBQ0ksbUJBQVM7QUFEYixTQUpPO0FBSGYsT0FuRU8sRUErRVA7QUFDSSx1QkFBZSxPQURuQjtBQUVJLHVCQUFlLGtCQUZuQjtBQUdJLG1CQUFXLENBQ1A7QUFDSSxtQkFBUztBQURiLFNBRE87QUFIZixPQS9FTyxFQXdGUDtBQUNJLHVCQUFlLE9BRG5CO0FBRUksdUJBQWUsb0JBRm5CO0FBR0ksbUJBQVcsQ0FDUDtBQUNJLG1CQUFTO0FBRGIsU0FETztBQUhmLE9BeEZPLENBbkNSO0FBcUlIQyxrQkFBWSxFQUFFO0FBcklYLEtBQVA7QUF1SUgsR0F2TGtCOztBQXdMbkJ0SyxVQUFRLEVBQUU7QUFDTnVLLFlBQVEsRUFBRSxZQUFZO0FBQ2xCLFVBQUksS0FBSzVLLElBQUwsS0FBYyxXQUFsQixFQUErQjtBQUMzQixlQUFPNkssTUFBTSxDQUFDQyxJQUFQLENBQVlDLFNBQVosQ0FBc0JDLFNBQTdCO0FBQ0gsT0FGRCxNQUVPLElBQUksS0FBS2hMLElBQUwsS0FBYyxRQUFsQixFQUE0QjtBQUMvQixlQUFPNkssTUFBTSxDQUFDQyxJQUFQLENBQVlDLFNBQVosQ0FBc0JFLE1BQTdCO0FBQ0gsT0FGTSxNQUVBLElBQUksS0FBS2pMLElBQUwsS0FBYyxTQUFsQixFQUE2QjtBQUNoQyxlQUFPNkssTUFBTSxDQUFDQyxJQUFQLENBQVlDLFNBQVosQ0FBc0JHLE9BQTdCO0FBQ0g7O0FBRUQsYUFBT0wsTUFBTSxDQUFDQyxJQUFQLENBQVlDLFNBQVosQ0FBc0JJLE9BQTdCO0FBQ0gsS0FYSzs7QUFhTkMsaUJBQWEsR0FBRztBQUNaLFVBQUksS0FBS3BCLGNBQUwsQ0FBb0JFLFNBQXhCLEVBQW1DO0FBQy9CLGVBQU8sS0FBS0YsY0FBTCxDQUFvQkUsU0FBM0I7QUFDSCxPQUZELE1BRU87QUFDSCxlQUFPLEtBQVA7QUFDSDtBQUNKLEtBbkJLOztBQXFCTm1CLG1CQUFlLEdBQUc7QUFDZCxVQUFJLE9BQU8sS0FBS3JCLGNBQUwsQ0FBb0JHLFdBQTNCLEtBQTJDLFFBQS9DLEVBQXlEO0FBQ3JEO0FBQ0g7O0FBRUQsVUFBSW1CLEtBQUssR0FBRyxLQUFLdEIsY0FBTCxDQUFvQkcsV0FBcEIsQ0FBZ0N6QixLQUFoQyxDQUFzQyxHQUF0QyxDQUFaO0FBRUEsYUFBUSxHQUFFNEMsS0FBSyxDQUFDLENBQUQsQ0FBSSxLQUFJQSxLQUFLLENBQUMsQ0FBRCxDQUFJLE1BQUtBLEtBQUssQ0FBQyxDQUFELENBQUksRUFBOUM7QUFDSCxLQTdCSzs7QUE4Qk5DLGVBQVcsR0FBRztBQUNWLFVBQUcsS0FBS3hCLFlBQUwsSUFBcUIsS0FBS1gsT0FBTCxLQUFpQixLQUFLVyxZQUE5QyxFQUE0RDtBQUN4RCxlQUFPLEVBQVA7QUFDSDs7QUFFRCxhQUFPLEtBQUtWLElBQVo7QUFDSDs7QUFwQ0ssR0F4TFM7O0FBOE5uQixRQUFNNUwsT0FBTixHQUFnQjtBQUNaLFNBQUtrTixZQUFMLEdBQW9CLEtBQUtmLGdCQUF6Qjs7QUFFQSxRQUFJLEtBQUtlLFlBQVQsRUFBdUI7QUFDbkIsWUFBTSxLQUFLYSxPQUFMLEVBQU47QUFDSDtBQUNKLEdBcE9rQjs7QUFxT25CNU4sU0FBTyxFQUFFO0FBQ0wsVUFBTTROLE9BQU4sR0FBZ0I7QUFDWixVQUFJO0FBQ0EsY0FBTVgsTUFBTSxHQUFHLE1BQU1ZLDREQUFTLEVBQTlCO0FBQ0EsY0FBTTdILEdBQUcsR0FBRyxJQUFJaUgsTUFBTSxDQUFDQyxJQUFQLENBQVlZLEdBQWhCLENBQW9CLEtBQUszSyxLQUFMLENBQVc0SyxJQUEvQixDQUFaO0FBQ0EsYUFBSzdCLFFBQUwsR0FBZ0IsSUFBSWUsTUFBTSxDQUFDQyxJQUFQLENBQVljLFFBQWhCLEVBQWhCO0FBQ0EsYUFBS2hJLEdBQUwsR0FBV0EsR0FBWDtBQUNBQSxXQUFHLENBQUNpSSxVQUFKLENBQWU7QUFBQ3hCLGdCQUFNLEVBQUUsS0FBS0s7QUFBZCxTQUFmO0FBRUEsYUFBS1osUUFBTCxDQUFjZ0MsT0FBZCxDQUFzQjtBQUFDMUMsaUJBQU8sRUFBRSxLQUFLQTtBQUFmLFNBQXRCLEVBQStDLENBQUMyQyxPQUFELEVBQVVDLE1BQVYsS0FBcUI7QUFDaEUsY0FBSUEsTUFBTSxLQUFLLElBQVgsSUFBbUIsQ0FBQ0QsT0FBTyxDQUFDLENBQUQsQ0FBL0IsRUFBb0M7QUFDaEMsa0JBQU0sSUFBSUUsS0FBSixDQUFVRCxNQUFWLENBQU47QUFDSDs7QUFDRHBJLGFBQUcsQ0FBQ3NJLFNBQUosQ0FBY0gsT0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXSSxRQUFYLENBQW9CaEwsUUFBbEM7QUFDQXlDLGFBQUcsQ0FBQ3dJLFNBQUosQ0FBY0wsT0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXSSxRQUFYLENBQW9CRSxRQUFsQztBQUNBekksYUFBRyxDQUFDMEksT0FBSixDQUFZLEtBQUtqRCxJQUFqQjtBQUNBekYsYUFBRyxDQUFDMkksU0FBSixHQUFnQixLQUFLM0IsUUFBckI7QUFDQWhILGFBQUcsQ0FBQzRJLGVBQUosR0FBc0IsYUFBdEI7QUFDSCxTQVREOztBQVdBLGNBQU1DLGtCQUFrQixHQUFJQyxNQUFELElBQVk7QUFDbkM5SSxhQUFHLENBQUMwSSxPQUFKLENBQVksRUFBWjtBQUNBMUksYUFBRyxDQUFDc0ksU0FBSixDQUFjUSxNQUFNLENBQUNDLFdBQVAsRUFBZDtBQUNBLGVBQUtDLFlBQUwsQ0FBa0JGLE1BQWxCO0FBQ0gsU0FKRDs7QUFNQSxjQUFNRyxPQUFPLEdBQUcsS0FBS3ZELE9BQUwsQ0FBYTFGLEdBQWIsQ0FBa0J6QyxRQUFELElBQWM7QUFDM0MsZ0JBQU11TCxNQUFNLEdBQUcsSUFBSTdCLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZZ0MsTUFBaEIsQ0FDWCxFQUFDLEdBQUczTCxRQUFKO0FBQWN5QyxlQUFkO0FBQW1CbUosZ0JBQUksRUFBRSxLQUFLcEQ7QUFBOUIsV0FEVyxDQUFmO0FBR0ErQyxnQkFBTSxDQUFDTSxXQUFQLENBQW1CLE9BQW5CLEVBQTRCLE1BQU1QLGtCQUFrQixDQUFDQyxNQUFELENBQXBEO0FBRUEsaUJBQU9BLE1BQVA7QUFDSCxTQVBlLENBQWhCO0FBU0EsWUFBSU8sOERBQUosQ0FBb0JySixHQUFwQixFQUF5QmlKLE9BQXpCLEVBQWtDO0FBQUN4QyxnQkFBTSxFQUFFLEtBQUtBO0FBQWQsU0FBbEM7QUFFSCxPQW5DRCxDQW1DRSxPQUFPM0ssS0FBUCxFQUFjO0FBQ1pELGVBQU8sQ0FBQ0MsS0FBUixDQUFjQSxLQUFkO0FBQ0g7QUFDSixLQXhDSTs7QUF5Q0x3TixVQUFNLEdBQUc7QUFFTCxXQUFLcEQsUUFBTCxDQUFjZ0MsT0FBZCxDQUFzQjtBQUFDMUMsZUFBTyxFQUFFLEtBQUtBLE9BQUwsR0FBZSxHQUFmLEdBQXFCLEtBQUtXO0FBQXBDLE9BQXRCLEVBQXlFLENBQUNnQyxPQUFELEVBQVVDLE1BQVYsS0FBcUI7QUFDMUYsWUFBSUEsTUFBTSxLQUFLLElBQVgsSUFBbUIsQ0FBQ0QsT0FBTyxDQUFDLENBQUQsQ0FBL0IsRUFBb0M7QUFDaEMsZ0JBQU0sSUFBSUUsS0FBSixDQUFVRCxNQUFWLENBQU47QUFDSDs7QUFDRCxhQUFLcEksR0FBTCxDQUFTc0ksU0FBVCxDQUFtQkgsT0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXSSxRQUFYLENBQW9CaEwsUUFBdkM7QUFDQSxhQUFLeUMsR0FBTCxDQUFTd0ksU0FBVCxDQUFtQkwsT0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXSSxRQUFYLENBQW9CRSxRQUF2QztBQUNBLGFBQUt6SSxHQUFMLENBQVMwSSxPQUFULENBQWlCLEtBQUtmLFdBQXRCO0FBQ0EsYUFBSzNILEdBQUwsQ0FBUzJJLFNBQVQsR0FBcUIsS0FBSzNCLFFBQTFCO0FBQ0EsYUFBS2hILEdBQUwsQ0FBUzRJLGVBQVQsR0FBMkIsYUFBM0I7QUFDSCxPQVREO0FBVUgsS0FyREk7O0FBc0RMSSxnQkFBWSxDQUFDTyxNQUFELEVBQVM7QUFDakIsV0FBS25ELGNBQUwsQ0FBb0JwSyxJQUFwQixHQUEyQnVOLE1BQU0sQ0FBQ25KLEtBQWxDO0FBQ0EsV0FBS2dHLGNBQUwsQ0FBb0JDLElBQXBCLEdBQTJCa0QsTUFBTSxDQUFDbEQsSUFBbEM7QUFDQSxXQUFLRCxjQUFMLENBQW9CRSxTQUFwQixHQUFnQ2lELE1BQU0sQ0FBQ2pELFNBQXZDO0FBQ0EsV0FBS0YsY0FBTCxDQUFvQlosT0FBcEIsR0FBOEIrRCxNQUFNLENBQUMvRCxPQUFyQztBQUNBLFdBQUtZLGNBQUwsQ0FBb0JHLFdBQXBCLEdBQWtDZ0QsTUFBTSxDQUFDaEQsV0FBekM7QUFDQSxXQUFLTixnQkFBTCxHQUF3QixJQUF4QjtBQUNBLFdBQUtHLGNBQUwsQ0FBb0JJLGFBQXBCLEdBQW9DLHNDQUFzQytDLE1BQU0sQ0FBQ1IsV0FBUCxHQUFxQlMsR0FBckIsRUFBdEMsR0FBbUUsR0FBbkUsR0FBeUVELE1BQU0sQ0FBQ1IsV0FBUCxHQUFxQlUsR0FBckIsRUFBN0c7QUFDSCxLQTlESTs7QUErRExDLGtCQUFjLEdBQUc7QUFDYixXQUFLekQsZ0JBQUwsR0FBd0IsS0FBeEI7QUFDSCxLQWpFSTs7QUFrRUwsVUFBTTBELE9BQU4sR0FBZ0I7QUFDWixXQUFLNUMsWUFBTCxHQUFvQixJQUFwQjtBQUVBLFlBQU0sS0FBS2EsT0FBTCxFQUFOO0FBQ0g7O0FBdEVJO0FBck9VLENBQXZCLEU7Ozs7Ozs7Ozs7OztBQ0pBO0FBQUE7QUFBQTtBQUVBelAsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxlQUFkLEVBQStCO0FBQzNCekMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FEZTtBQUUzQjJDLFVBQVEsRUFBRSxnQkFGaUI7QUFHM0JDLE9BQUssRUFBRTtBQUNIaEMsZUFBVyxFQUFFO0FBQ1RrQyxVQUFJLEVBQUVDLE1BREc7QUFFVEMsY0FBUSxFQUFFO0FBRkQ7QUFEVixHQUhvQjs7QUFTM0JoRSxNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0hzUixZQUFNLEVBQUUsRUFETDtBQUVIQyxrQkFBWSxFQUFFLElBRlg7QUFHSEMsZUFBUyxFQUFFLElBSFI7QUFJSEMsZUFBUyxFQUFFO0FBSlIsS0FBUDtBQU1ILEdBaEIwQjs7QUFpQjNCbFEsU0FBTyxHQUFHO0FBQ04sU0FBS2dRLFlBQUwsR0FBb0IsS0FBS3hNLEdBQUwsQ0FBUy9CLGFBQVQsQ0FBdUIsc0JBQXZCLENBQXBCO0FBRUFkLEtBQUMsQ0FBQyxLQUFLcVAsWUFBTixDQUFELENBQXFCRyxFQUFyQixDQUF3QixpQkFBeEIsRUFBMkMsS0FBS0MsWUFBaEQ7QUFFQSxTQUFLQyxXQUFMO0FBQ0gsR0F2QjBCOztBQXdCM0JsUSxTQUFPLEVBQUU7QUFDTG1RLGFBQVMsR0FBRztBQUNSLFVBQUlwTixHQUFHLEdBQUcsY0FBVjtBQUFBLFVBQ0lILEtBQUssR0FBRyxJQURaOztBQUdBcEMsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hFLFdBQUcsRUFBRUEsR0FERjtBQUVIRCxjQUFNLEVBQUUsS0FGTDtBQUdIRSxlQUFPLEVBQUUsVUFBVUMsUUFBVixFQUFvQjtBQUN6QkwsZUFBSyxDQUFDZ04sTUFBTixHQUFlUSxJQUFJLENBQUNDLEtBQUwsQ0FBV3BOLFFBQVgsQ0FBZjtBQUNBTCxlQUFLLENBQUNrTixTQUFOLEdBQWtCLEtBQWxCO0FBQ0gsU0FORTtBQU9IaE8sYUFBSyxFQUFFLFVBQVVtQixRQUFWLEVBQW9CO0FBQ3ZCcEIsaUJBQU8sQ0FBQzRCLEdBQVIsQ0FBWVIsUUFBWjtBQUNIO0FBVEUsT0FBUDtBQVdILEtBaEJJOztBQWtCTGlOLGVBQVcsR0FBRztBQUNWMVAsT0FBQyxDQUFDLEtBQUtxUCxZQUFOLENBQUQsQ0FBcUJ6TSxLQUFyQixDQUEyQixNQUEzQjtBQUNBLFdBQUsrTSxTQUFMO0FBQ0gsS0FyQkk7O0FBdUJMRixnQkFBWSxHQUFHO0FBQ1h6UCxPQUFDLENBQUMsS0FBS3FQLFlBQU4sQ0FBRCxDQUFxQnpNLEtBQXJCLENBQTJCLE1BQTNCO0FBQ0FwRixrQkFBWSxDQUFDSSxJQUFiLENBQWtCLGtCQUFsQixFQUFzQyxLQUFLOEIsV0FBM0M7QUFDSCxLQTFCSTs7QUE0QkxvUSxlQUFXLENBQUNDLFVBQUQsRUFBYTtBQUNwQnZTLGtCQUFZLENBQUNJLElBQWIsQ0FBa0IsNEJBQWxCLEVBQWdEO0FBQUNvUyxZQUFJLEVBQUVELFVBQVA7QUFBbUJyUSxtQkFBVyxFQUFFLEtBQUtBO0FBQXJDLE9BQWhEO0FBRUEsV0FBSytQLFlBQUw7QUFDSCxLQWhDSTs7QUFrQ0xRLGNBQVUsR0FBRztBQUNULFdBQUt0TixLQUFMLENBQVd1TixTQUFYLENBQXFCQyxLQUFyQjtBQUNILEtBcENJOztBQXNDTEMsY0FBVSxHQUFHO0FBQ1QsVUFBSUMsSUFBSSxHQUFHLEtBQUsxTixLQUFMLENBQVd1TixTQUFYLENBQXFCSSxLQUFyQixDQUEyQixDQUEzQixDQUFYO0FBQUEsVUFDSWxPLEtBQUssR0FBRyxJQURaOztBQUdBLFVBQUksQ0FBQ2lPLElBQUwsRUFBVztBQUNQO0FBQ0g7O0FBRUQsV0FBS2QsU0FBTCxHQUFpQixJQUFqQjtBQUVBLFVBQUlnQixRQUFRLEdBQUcsSUFBSUMsUUFBSixFQUFmO0FBQ0FELGNBQVEsQ0FBQ0UsTUFBVCxDQUFnQixNQUFoQixFQUF3QkosSUFBeEI7QUFFQXJRLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsTUFETDtBQUVIQyxXQUFHLEVBQUUsbUNBRkY7QUFHSHpFLFlBQUksRUFBRXlTLFFBSEg7QUFJSEcsbUJBQVcsRUFBRSxLQUpWO0FBS0hDLG1CQUFXLEVBQUUsS0FMVjs7QUFNSG5PLGVBQU8sQ0FBQ0MsUUFBRCxFQUFXO0FBQ2RMLGVBQUssQ0FBQ21OLFNBQU4sR0FBa0IsS0FBbEI7O0FBQ0FuTixlQUFLLENBQUNnTixNQUFOLENBQWF3QixPQUFiLENBQXFCbk8sUUFBckI7QUFDSCxTQVRFOztBQVVIbkIsYUFBSyxDQUFDQSxLQUFELEVBQVE7QUFDVGMsZUFBSyxDQUFDbU4sU0FBTixHQUFrQixLQUFsQjtBQUVBcFIsZUFBSyxDQUFDbUQsS0FBSyxDQUFDLGNBQUQsQ0FBTCxDQUFzQnVQLE1BQXZCLEVBQStCLE9BQS9CLENBQUw7QUFDSDs7QUFkRSxPQUFQO0FBZ0JIOztBQW5FSSxHQXhCa0I7QUE4RjNCNU8sVUFBUSxFQUFFO0FBQ042TyxlQUFXLEdBQUc7QUFDVixhQUFPLEtBQUt2QixTQUFMLEtBQW1CLElBQTFCO0FBQ0g7O0FBSEs7QUE5RmlCLENBQS9CLEU7Ozs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE1UiwyQ0FBRyxDQUFDNEQsU0FBSixDQUFjLE1BQWQsRUFBc0I7QUFDbEJ6QyxZQUFVLEVBQUUsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQURNO0FBRWxCMkMsVUFBUSxFQUFFLE9BRlE7QUFHbEJDLE9BQUssRUFBRTtBQUNIcVAsVUFBTSxFQUFFO0FBQ0pqUCxjQUFRLEVBQUUsSUFETjtBQUVKRixVQUFJLEVBQUVDO0FBRkYsS0FETDtBQUtINEIsVUFBTSxFQUFFO0FBQ0ozQixjQUFRLEVBQUUsS0FETjtBQUVKRixVQUFJLEVBQUVDLE1BRkY7QUFHSjBCLGFBQU8sRUFBRTtBQUhMO0FBTEwsR0FIVzs7QUFjbEJ6RixNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0hrVCxVQUFJLEVBQUU7QUFBQ0MsaUJBQVMsRUFBRTtBQUFaLE9BREg7QUFFSEMsaUJBQVcsRUFBRTtBQUFDdFAsWUFBSSxFQUFFO0FBQVAsT0FGVjtBQUdIdVAsY0FBUSxFQUFFLEtBSFA7QUFJSEMsY0FBUSxFQUFFO0FBSlAsS0FBUDtBQU1ILEdBckJpQjs7QUFzQmxCQyxTQUFPLEdBQUc7QUFDTixTQUFLcE0sT0FBTDtBQUNILEdBeEJpQjs7QUF5QmxCNUYsU0FBTyxHQUFHO0FBQ04sU0FBS2lTLFlBQUw7QUFDSCxHQTNCaUI7O0FBNEJsQjlSLFNBQU8sRUFBRTtBQUNMK1IsaUJBQWEsR0FBRztBQUNaLGFBQU87QUFDSEMsWUFBSSxFQUFFLEtBQUtOLFdBQUwsQ0FBaUJNLElBRHBCO0FBRUg1UCxZQUFJLEVBQUUsS0FBS3NQLFdBQUwsQ0FBaUJ0UCxJQUZwQjtBQUdIbVAsY0FBTSxFQUFFLEtBQUtDLElBQUwsQ0FBVWxSO0FBSGYsT0FBUDtBQUtILEtBUEk7O0FBU0xtRixXQUFPLEdBQUc7QUFDTixVQUFJMUMsR0FBRyxHQUFJLEdBQUUsS0FBS2tCLE1BQUwsS0FBZ0IsSUFBaEIsR0FBdUIsRUFBdkIsR0FBNEIsTUFBTSxLQUFLQSxNQUFPLGdCQUFlLEtBQUtzTixNQUFPLEVBQXRGO0FBQUEsVUFDSTNPLEtBQUssR0FBRyxJQURaOztBQUdBcEMsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLGNBQU0sRUFBRSxLQURMO0FBRUhDLFdBQUcsRUFBRUEsR0FGRjs7QUFHSEMsZUFBTyxDQUFDQyxRQUFELEVBQVc7QUFDZEwsZUFBSyxDQUFDNE8sSUFBTixHQUFhdk8sUUFBYjtBQUNILFNBTEU7O0FBTUhuQixhQUFLLENBQUNBLEtBQUQsRUFBUTtBQUNURCxpQkFBTyxDQUFDNEIsR0FBUixDQUFZM0IsS0FBWjtBQUNIOztBQVJFLE9BQVA7QUFVSCxLQXZCSTs7QUF5QkxtUSxxQkFBaUIsQ0FBQzNSLEVBQUQsRUFBSztBQUNsQixVQUFJSCxLQUFLLEdBQUcsS0FBS3FSLElBQUwsQ0FBVUMsU0FBVixDQUFvQnJSLFNBQXBCLENBQThCQyxJQUFJLElBQUlBLElBQUksQ0FBQ0MsRUFBTCxLQUFZQSxFQUFsRCxDQUFaO0FBRUEsV0FBS2tSLElBQUwsQ0FBVUMsU0FBVixDQUFvQmxSLE1BQXBCLENBQTJCSixLQUEzQixFQUFrQyxDQUFsQztBQUNILEtBN0JJOztBQStCTCtSLGtCQUFjLEdBQUc7QUFDYixVQUFJblAsR0FBRyxHQUFJLEdBQUUsS0FBS2tCLE1BQUwsS0FBZ0IsSUFBaEIsR0FBdUIsRUFBdkIsR0FBNEIsTUFBTSxLQUFLQSxNQUFPLHNCQUEzRDtBQUFBLFVBQ0kzRixJQUFJLEdBQUcsS0FBS3lULGFBQUwsRUFEWDtBQUFBLFVBRUluUCxLQUFLLEdBQUcsSUFGWjs7QUFJQXBDLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsTUFETDtBQUVIeEUsWUFBSSxFQUFFQSxJQUZIO0FBR0h5RSxXQUFHLEVBQUVBLEdBSEY7O0FBSUhDLGVBQU8sQ0FBQ0MsUUFBRCxFQUFXO0FBQ2QsY0FBSWtQLFFBQVEsR0FBR2xQLFFBQVEsQ0FBQyxTQUFELENBQXZCO0FBRUFrUCxrQkFBUSxDQUFDLFNBQUQsQ0FBUixHQUFzQixFQUF0QjtBQUVBdlAsZUFBSyxDQUFDK08sUUFBTixHQUFpQixLQUFqQjs7QUFFQS9PLGVBQUssQ0FBQzRPLElBQU4sQ0FBV0MsU0FBWCxDQUFxQnhSLElBQXJCLENBQTBCa1MsUUFBMUI7O0FBRUF2UCxlQUFLLENBQUNrUCxZQUFOOztBQUVBbFAsZUFBSyxDQUFDd1AsYUFBTjs7QUFFQXpULGVBQUssQ0FBQ3NFLFFBQVEsQ0FBQyxTQUFELENBQVQsRUFBc0IsU0FBdEIsQ0FBTDtBQUNILFNBbEJFOztBQW1CSG5CLGFBQUssQ0FBQ0EsS0FBRCxFQUFRO0FBQ1RELGlCQUFPLENBQUM0QixHQUFSLENBQVkzQixLQUFaO0FBQ0g7O0FBckJFLE9BQVA7QUF1QkgsS0EzREk7O0FBNkRMc1EsaUJBQWEsR0FBRztBQUNaLFdBQUtWLFdBQUwsR0FBbUI7QUFDZk0sWUFBSSxFQUFFLEVBRFM7QUFFZjVQLFlBQUksRUFBRSxlQUZTO0FBR2ZxUCxpQkFBUyxFQUFFO0FBSEksT0FBbkI7QUFLSCxLQW5FSTs7QUFxRUxLLGdCQUFZLEdBQUc7QUFDWCxVQUFJbFAsS0FBSyxHQUFHLElBQVo7O0FBRUEsV0FBS2dQLFFBQUwsR0FBZ0JTLGtEQUFRLENBQUNDLE1BQVQsQ0FBZ0IsS0FBS25QLEtBQUwsQ0FBV3NPLFNBQTNCLEVBQXNDO0FBQ2xEYyxrQkFBVSxFQUFFLDZCQURzQztBQUVsRCxrQkFBVSxjQUZ3QztBQUdsREMsaUJBQVMsRUFBRSxHQUh1QztBQUlsREMsY0FBTSxFQUFFLFlBQVk7QUFDaEIsY0FBSW5VLElBQUksR0FBRyxFQUFYOztBQUNBc0UsZUFBSyxDQUFDTyxLQUFOLENBQVlzTyxTQUFaLENBQXNCaUIsUUFBdEIsQ0FBK0I3UixPQUEvQixDQUF1Q1IsSUFBSSxJQUFJL0IsSUFBSSxDQUFDMkIsSUFBTCxDQUFVSSxJQUFJLENBQUNpRCxZQUFMLENBQWtCLFNBQWxCLENBQVYsQ0FBL0M7O0FBRUFWLGVBQUssQ0FBQytQLGFBQU4sQ0FBb0I7QUFBQ0MsMkJBQWUsRUFBRXRVO0FBQWxCLFdBQXBCO0FBQ0g7QUFUaUQsT0FBdEMsQ0FBaEI7QUFXSCxLQW5GSTs7QUFxRkxxVSxpQkFBYSxDQUFDclUsSUFBRCxFQUFPO0FBQ2hCLFVBQUl5RSxHQUFHLEdBQUksR0FBRSxLQUFLa0IsTUFBTCxLQUFnQixJQUFoQixHQUF1QixFQUF2QixHQUE0QixNQUFNLEtBQUtBLE1BQU8sZ0JBQWUsS0FBS3VOLElBQUwsQ0FBVWxSLEVBQUcsaUJBQXZGO0FBRUFFLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsTUFETDtBQUVIeEUsWUFBSSxFQUFFQSxJQUZIO0FBR0h5RSxXQUFHLEVBQUVBLEdBSEY7O0FBSUhDLGVBQU8sQ0FBQ0MsUUFBRCxFQUFXO0FBQ2R0RSxlQUFLLENBQUNzRSxRQUFELEVBQVcsU0FBWCxDQUFMO0FBQ0gsU0FORTs7QUFPSG5CLGFBQUssQ0FBQ0EsS0FBRCxFQUFRO0FBQ1RELGlCQUFPLENBQUM0QixHQUFSLENBQVkzQixLQUFaO0FBQ0g7O0FBVEUsT0FBUDtBQVdIOztBQW5HSTtBQTVCUyxDQUF0QixFOzs7Ozs7Ozs7Ozs7O0FDSEE7QUFBQTtBQUFBO0FBRUEzRCwyQ0FBRyxDQUFDNEQsU0FBSixDQUFjLGFBQWQsRUFBNkI7QUFDekJ6QyxZQUFVLEVBQUUsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQURhO0FBRXpCMkMsVUFBUSxFQUFFLGNBRmU7QUFHekJDLE9BQUssRUFBRTtBQUNIMlEsVUFBTSxFQUFFO0FBQ0p2USxjQUFRLEVBQUUsSUFETjtBQUVKRixVQUFJLEVBQUUwUTtBQUZGLEtBREw7QUFLSDdPLFVBQU0sRUFBRTtBQUNKM0IsY0FBUSxFQUFFLEtBRE47QUFFSkYsVUFBSSxFQUFFQyxNQUZGO0FBR0owQixhQUFPLEVBQUU7QUFITDtBQUxMLEdBSGtCOztBQWN6QnpGLE1BQUksR0FBRztBQUNILFdBQU87QUFDSHlVLGVBQVMsRUFBRSxLQURSO0FBRUhDLGlCQUFXLEVBQUU7QUFGVixLQUFQO0FBSUgsR0FuQndCOztBQW9CekJoVCxTQUFPLEVBQUU7QUFDTGlULGVBQVcsR0FBRztBQUNWLGFBQU87QUFDSGpCLFlBQUksRUFBRSxLQUFLYSxNQUFMLENBQVliLElBRGY7QUFFSGtCLGtCQUFVLEVBQUUsS0FBS0wsTUFBTCxDQUFZSyxVQUZyQjtBQUdIQyx1QkFBZSxFQUFFLEtBQUtOLE1BQUwsQ0FBWUssVUFBWixHQUF5QixLQUFLTCxNQUFMLENBQVlNLGVBQXJDLEdBQXVEO0FBSHJFLE9BQVA7QUFLSCxLQVBJOztBQVNMQyxRQUFJLEdBQUc7QUFDSCxVQUFJclEsR0FBRyxHQUFJLEdBQUUsS0FBS2tCLE1BQUwsS0FBZ0IsSUFBaEIsR0FBdUIsRUFBdkIsR0FBNEIsTUFBTSxLQUFLQSxNQUFPLGtCQUFpQixLQUFLNE8sTUFBTCxDQUFZdlMsRUFBRyxTQUEzRjtBQUFBLFVBQ0lzQyxLQUFLLEdBQUcsSUFEWjtBQUFBLFVBRUl0RSxJQUFJLEdBQUcsS0FBSzJVLFdBQUwsRUFGWDs7QUFJQXpTLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsTUFETDtBQUVIeEUsWUFBSSxFQUFFQSxJQUZIO0FBR0h5RSxXQUFHLEVBQUVBLEdBSEY7O0FBSUhDLGVBQU8sQ0FBQ0MsUUFBRCxFQUFXO0FBQ2RMLGVBQUssQ0FBQ21RLFNBQU4sR0FBa0IsS0FBbEI7QUFDQXBVLGVBQUssQ0FBQ3NFLFFBQUQsRUFBVyxTQUFYLENBQUw7QUFDSCxTQVBFOztBQVFIbkIsYUFBSyxDQUFDQSxLQUFELEVBQVE7QUFDVGMsZUFBSyxDQUFDbVEsU0FBTixHQUFrQixLQUFsQjtBQUNBcFUsZUFBSyxDQUFDbUQsS0FBSyxDQUFDLGNBQUQsQ0FBTixFQUF3QixPQUF4QixDQUFMO0FBQ0g7O0FBWEUsT0FBUDtBQWFILEtBM0JJOztBQTZCTHVSLGdCQUFZLEdBQUc7QUFDWCxVQUFJdFEsR0FBRyxHQUFJLEdBQUUsS0FBS2tCLE1BQUwsS0FBZ0IsSUFBaEIsR0FBdUIsRUFBdkIsR0FBNEIsTUFBTSxLQUFLQSxNQUFPLGtCQUFpQixLQUFLNE8sTUFBTCxDQUFZdlMsRUFBRyxTQUEzRjtBQUFBLFVBQ0lzQyxLQUFLLEdBQUcsSUFEWjs7QUFHQXBDLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsTUFETDtBQUVIQyxXQUFHLEVBQUVBLEdBRkY7O0FBR0hDLGVBQU8sQ0FBQ0MsUUFBRCxFQUFXO0FBQ2RMLGVBQUssQ0FBQ21RLFNBQU4sR0FBa0IsS0FBbEI7O0FBRUFuUSxlQUFLLENBQUMwUSxpQkFBTixDQUF3QixTQUF4Qjs7QUFFQTFRLGVBQUssQ0FBQ3JFLEtBQU4sQ0FBWSxrQkFBWixFQUFnQ3FFLEtBQUssQ0FBQ2lRLE1BQU4sQ0FBYXZTLEVBQTdDOztBQUVBM0IsZUFBSyxDQUFDc0UsUUFBRCxFQUFXLFNBQVgsQ0FBTDtBQUNILFNBWEU7O0FBWUhuQixhQUFLLENBQUNBLEtBQUQsRUFBUTtBQUNUYyxlQUFLLENBQUNtUSxTQUFOLEdBQWtCLEtBQWxCO0FBQ0FwVSxlQUFLLENBQUNtRCxLQUFLLENBQUMsY0FBRCxDQUFOLEVBQXdCLE9BQXhCLENBQUw7QUFDSDs7QUFmRSxPQUFQO0FBaUJILEtBbERJOztBQW9ETHdSLHFCQUFpQixDQUFDQyxRQUFELEVBQVc7QUFDeEIsV0FBS1AsV0FBTCxHQUFtQk8sUUFBbkI7QUFDSDs7QUF0REk7QUFwQmdCLENBQTdCLEU7Ozs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7QUFFQXBWLDJDQUFHLENBQUM0RCxTQUFKLENBQWMsb0JBQWQsRUFBb0M7QUFDaEN6QyxZQUFVLEVBQUUsQ0FBQyxJQUFELEVBQU8sSUFBUCxDQURvQjtBQUVoQzJDLFVBQVEsRUFBRSxxQkFGc0I7QUFHaENDLE9BQUssRUFBRTtBQUNIK0IsVUFBTSxFQUFFO0FBQ0ozQixjQUFRLEVBQUUsS0FETjtBQUVKRixVQUFJLEVBQUVDLE1BRkY7QUFHSjBCLGFBQU8sRUFBRTtBQUhMO0FBREwsR0FIeUI7O0FBVWhDekYsTUFBSSxHQUFHO0FBQ0gsV0FBTztBQUNIa1YsaUJBQVcsRUFBRSxFQURWO0FBRUhDLHFCQUFlLEVBQUUsRUFGZDtBQUdIQyxrQkFBWSxFQUFFLElBSFg7QUFJSEMsVUFBSSxFQUFFO0FBSkgsS0FBUDtBQU1ILEdBakIrQjs7QUFrQmhDOVQsU0FBTyxHQUFHO0FBQ04sVUFBTStULEtBQUssR0FBRywwQkFBZDs7QUFFQSxRQUFJN1YsTUFBTSxDQUFDd0YsUUFBUCxDQUFnQmtGLElBQWhCLENBQXFCb0wsS0FBckIsQ0FBMkJELEtBQTNCLE1BQXNDLElBQXRDLElBQ0E3VixNQUFNLENBQUN3RixRQUFQLENBQWdCa0YsSUFBaEIsQ0FBcUJxTCxPQUFyQixDQUE2QixXQUE3QixLQUE2QyxDQURqRCxFQUNvRDtBQUNoRCxXQUFLSCxJQUFMLEdBQVksS0FBWjtBQUNBO0FBQ0g7O0FBRURJLGNBQVUsQ0FBQyxLQUFLQyxjQUFOLEVBQXNCLElBQXRCLENBQVY7QUFDSCxHQTVCK0I7O0FBNkJoQ2hVLFNBQU8sRUFBRTtBQUNMZ1Usa0JBQWMsR0FBRztBQUNiLFVBQUlqUixHQUFHLEdBQUksMEJBQXlCLEtBQUtrQixNQUFPLEVBQWhEO0FBQUEsVUFDSXJCLEtBQUssR0FBRyxJQURaOztBQUdBcEMsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hFLFdBQUcsRUFBRUEsR0FERjtBQUVIRCxjQUFNLEVBQUUsS0FGTDtBQUdIRSxlQUFPLEVBQUUsVUFBVUMsUUFBVixFQUFvQjtBQUN6QkwsZUFBSyxDQUFDNlEsZUFBTixHQUF3QnhRLFFBQXhCO0FBQ0FMLGVBQUssQ0FBQzRRLFdBQU4sR0FBb0J2USxRQUFRLENBQUMrQyxHQUFULENBQWEzRixJQUFJLElBQUlBLElBQUksQ0FBQ0MsRUFBMUIsQ0FBcEI7O0FBQ0FzQyxlQUFLLENBQUNxUixVQUFOO0FBRUgsU0FSRTtBQVNIblMsYUFBSyxFQUFFLFVBQVVtQixRQUFWLEVBQW9CO0FBQ3ZCcEIsaUJBQU8sQ0FBQzRCLEdBQVIsQ0FBWVIsUUFBWjtBQUNIO0FBWEUsT0FBUDtBQWFILEtBbEJJOztBQW9CTGdSLGNBQVUsR0FBRztBQUNULFVBQUlDLGNBQWMsR0FBR0MsWUFBWSxDQUFDQyxPQUFiLENBQXFCLGlCQUFyQixLQUEyQyxFQUFoRTtBQUFBLFVBQ0lDLGlCQURKOztBQUdBLFVBQUksS0FBS2IsV0FBTCxDQUFpQnRMLE1BQWpCLEtBQTRCLENBQWhDLEVBQW1DO0FBQy9CO0FBQ0g7O0FBRUQsVUFBSSxDQUFDZ00sY0FBTCxFQUFxQjtBQUNqQixhQUFLUixZQUFMLEdBQW9CLEtBQUtGLFdBQUwsQ0FBaUIsQ0FBakIsQ0FBcEI7QUFDQTtBQUNIOztBQUVEVSxvQkFBYyxHQUFHQSxjQUFjLENBQUNwSixLQUFmLENBQXFCLEdBQXJCLENBQWpCO0FBQ0F1Six1QkFBaUIsR0FBRyxLQUFLYixXQUFMLENBQWlCbEUsTUFBakIsQ0FBd0JrQyxJQUFJLElBQUkwQyxjQUFjLENBQUM5VCxTQUFmLENBQXlCQyxJQUFJLElBQUlpVSxRQUFRLENBQUNqVSxJQUFELENBQVIsS0FBbUJtUixJQUFwRCxJQUE0RCxDQUE1RixDQUFwQjtBQUNBLFdBQUtrQyxZQUFMLEdBQW9CVyxpQkFBaUIsQ0FBQ0UsR0FBbEIsRUFBcEI7QUFDSCxLQXBDSTs7QUFzQ0xDLGVBQVcsR0FBRztBQUNWLFVBQUksQ0FBQyxLQUFLZCxZQUFWLEVBQXdCO0FBQ3BCO0FBQ0g7O0FBRUQsVUFBSVEsY0FBYyxHQUFHQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsaUJBQXJCLEtBQTJDLEVBQWhFO0FBQ0FGLG9CQUFjLEdBQUdBLGNBQWMsQ0FBQ3BKLEtBQWYsQ0FBcUIsR0FBckIsQ0FBakI7QUFDQW9KLG9CQUFjLENBQUNqVSxJQUFmLENBQW9CLEtBQUt5VCxZQUF6QjtBQUNBUyxrQkFBWSxDQUFDTSxPQUFiLENBQXFCLGlCQUFyQixFQUF3Q1AsY0FBYyxDQUFDUSxJQUFmLENBQW9CLEdBQXBCLENBQXhDO0FBQ0EsV0FBS3JMLEtBQUw7QUFDSCxLQWhESTs7QUFrRExzTCxZQUFRLEdBQUc7QUFDUCxVQUFJQyxPQUFPLEdBQUcsS0FBS25CLGVBQUwsQ0FBcUJuRSxNQUFyQixDQUE0QmpQLElBQUksSUFBSUEsSUFBSSxDQUFDQyxFQUFMLEtBQVksS0FBS29ULFlBQXJELEVBQW1FYSxHQUFuRSxHQUF5RU0sS0FBdkY7QUFFQTlXLFlBQU0sQ0FBQ3dGLFFBQVAsQ0FBZ0JrRixJQUFoQixHQUF1Qm1NLE9BQXZCLENBSE8sQ0FJUDtBQUNILEtBdkRJOztBQXlETHZMLFNBQUssR0FBRztBQUNKN0ksT0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUI0QyxLQUF6QixDQUErQixNQUEvQjtBQUNILEtBM0RJOztBQTZETDBSLFFBQUksR0FBRztBQUNILFVBQUksS0FBS25CLElBQVQsRUFBZTtBQUNYblQsU0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUI0QyxLQUF6QixDQUErQixNQUEvQjtBQUNIO0FBQ0o7O0FBakVJLEdBN0J1QjtBQWdHaEMwRixPQUFLLEVBQUU7QUFDSDRLLGdCQUFZLEVBQUUsVUFBVXFCLFFBQVYsRUFBb0I7QUFDOUIsVUFBSUEsUUFBSixFQUFjO0FBQ1YsYUFBS0QsSUFBTDtBQUNIO0FBQ0o7QUFMRTtBQWhHeUIsQ0FBcEMsRTs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTNXLDJDQUFHLENBQUM0RCxTQUFKLENBQWMsZUFBZCxFQUErQjtBQUMzQnpDLFlBQVUsRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBRGU7QUFFM0IyQyxVQUFRLEVBQUUsZ0JBRmlCO0FBRzNCQyxPQUFLLEVBQUU7QUFDSGlRLFlBQVEsRUFBRTtBQUNON1AsY0FBUSxFQUFFLElBREo7QUFFTkYsVUFBSSxFQUFFMFE7QUFGQSxLQURQO0FBS0g3TyxVQUFNLEVBQUU7QUFDSjNCLGNBQVEsRUFBRSxLQUROO0FBRUpGLFVBQUksRUFBRUMsTUFGRjtBQUdKMEIsYUFBTyxFQUFFO0FBSEw7QUFMTCxHQUhvQjs7QUFjM0J6RixNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0h5VSxlQUFTLEVBQUUsS0FEUjtBQUVIaUMsWUFBTSxFQUFFLEtBRkw7QUFHSHJELGNBQVEsRUFBRSxLQUhQO0FBSUhxQixpQkFBVyxFQUFFLFNBSlY7QUFLSGlDLGVBQVMsRUFBRTtBQUNQL0Isa0JBQVUsRUFBRTtBQURMLE9BTFI7QUFRSHRCLGNBQVEsRUFBRTtBQVJQLEtBQVA7QUFVSCxHQXpCMEI7O0FBMkIzQjVSLFNBQU8sRUFBRTtBQUNMaVQsZUFBVyxHQUFHO0FBQ1YsYUFBTztBQUNIakIsWUFBSSxFQUFFLEtBQUtHLFFBQUwsQ0FBY0gsSUFEakI7QUFFSDVQLFlBQUksRUFBRSxLQUFLK1AsUUFBTCxDQUFjL1A7QUFGakIsT0FBUDtBQUlILEtBTkk7O0FBUUwyUCxpQkFBYSxHQUFHO0FBQ1osYUFBTztBQUNIQyxZQUFJLEVBQUUsS0FBS2lELFNBQUwsQ0FBZWpELElBRGxCO0FBRUhrQixrQkFBVSxFQUFFLEtBQUsrQixTQUFMLENBQWUvQixVQUZ4QjtBQUdIQyx1QkFBZSxFQUFFLEtBQUs4QixTQUFMLENBQWUvQixVQUFmLEdBQTRCLEtBQUsrQixTQUFMLENBQWU5QixlQUEzQyxHQUE2RCxJQUgzRTtBQUlIK0Isa0JBQVUsRUFBRSxLQUFLL0MsUUFBTCxDQUFjN1I7QUFKdkIsT0FBUDtBQU1ILEtBZkk7O0FBaUJMOFMsUUFBSSxHQUFHO0FBQ0gsVUFBSXJRLEdBQUcsR0FBSSxHQUFFLEtBQUtrQixNQUFMLEtBQWdCLElBQWhCLEdBQXVCLEVBQXZCLEdBQTRCLE1BQU0sS0FBS0EsTUFBTyxvQkFBbUIsS0FBS2tPLFFBQUwsQ0FBYzdSLEVBQUcsU0FBL0Y7QUFBQSxVQUNJc0MsS0FBSyxHQUFHLElBRFo7QUFBQSxVQUVJdEUsSUFBSSxHQUFHLEtBQUsyVSxXQUFMLEVBRlg7O0FBSUF6UyxPQUFDLENBQUNxQyxJQUFGLENBQU87QUFDSEMsY0FBTSxFQUFFLE1BREw7QUFFSHhFLFlBQUksRUFBRUEsSUFGSDtBQUdIeUUsV0FBRyxFQUFFQSxHQUhGOztBQUlIQyxlQUFPLENBQUNDLFFBQUQsRUFBVztBQUNkTCxlQUFLLENBQUNtUSxTQUFOLEdBQWtCLEtBQWxCO0FBQ0FwVSxlQUFLLENBQUNzRSxRQUFELEVBQVcsU0FBWCxDQUFMO0FBQ0gsU0FQRTs7QUFRSG5CLGFBQUssQ0FBQ0EsS0FBRCxFQUFRO0FBQ1RjLGVBQUssQ0FBQ21RLFNBQU4sR0FBa0IsS0FBbEI7QUFDQXBVLGVBQUssQ0FBQ21ELEtBQUssQ0FBQyxjQUFELENBQU4sRUFBd0IsT0FBeEIsQ0FBTDtBQUNIOztBQVhFLE9BQVA7QUFhSCxLQW5DSTs7QUFxQ0xxVCxjQUFVLEdBQUc7QUFDVCxXQUFLSCxNQUFMLEdBQWMsQ0FBQyxLQUFLQSxNQUFwQjtBQUNILEtBdkNJOztBQXlDTEksa0JBQWMsR0FBRztBQUNiLFVBQUlyUyxHQUFHLEdBQUksR0FBRSxLQUFLa0IsTUFBTCxLQUFnQixJQUFoQixHQUF1QixFQUF2QixHQUE0QixNQUFNLEtBQUtBLE1BQU8sb0JBQW1CLEtBQUtrTyxRQUFMLENBQWM3UixFQUFHLFNBQS9GO0FBQUEsVUFDSXNDLEtBQUssR0FBRyxJQURaOztBQUdBcEMsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLGNBQU0sRUFBRSxNQURMO0FBRUhDLFdBQUcsRUFBRUEsR0FGRjs7QUFHSEMsZUFBTyxDQUFDQyxRQUFELEVBQVc7QUFDZEwsZUFBSyxDQUFDbVEsU0FBTixHQUFrQixLQUFsQjs7QUFFQW5RLGVBQUssQ0FBQzBRLGlCQUFOLENBQXdCLFNBQXhCOztBQUVBMVEsZUFBSyxDQUFDckUsS0FBTixDQUFZLG9CQUFaLEVBQWtDcUUsS0FBSyxDQUFDdVAsUUFBTixDQUFlN1IsRUFBakQ7O0FBRUEzQixlQUFLLENBQUNzRSxRQUFELEVBQVcsU0FBWCxDQUFMO0FBQ0gsU0FYRTs7QUFZSG5CLGFBQUssQ0FBQ0EsS0FBRCxFQUFRO0FBQ1RjLGVBQUssQ0FBQzBRLGlCQUFOLENBQXdCLFNBQXhCOztBQUVBM1UsZUFBSyxDQUFDbUQsS0FBSyxDQUFDLGNBQUQsQ0FBTixFQUF3QixPQUF4QixDQUFMO0FBQ0g7O0FBaEJFLE9BQVA7QUFrQkgsS0EvREk7O0FBaUVMdVQsbUJBQWUsQ0FBQy9VLEVBQUQsRUFBSztBQUNoQixVQUFJSCxLQUFLLEdBQUcsS0FBS2dTLFFBQUwsQ0FBY21ELE9BQWQsQ0FBc0JsVixTQUF0QixDQUFnQ3lTLE1BQU0sSUFBSUEsTUFBTSxDQUFDdlMsRUFBUCxLQUFjQSxFQUF4RCxDQUFaO0FBRUEsV0FBSzZSLFFBQUwsQ0FBY21ELE9BQWQsQ0FBc0IvVSxNQUF0QixDQUE2QkosS0FBN0IsRUFBb0MsQ0FBcEM7QUFDSCxLQXJFSTs7QUF1RUxtVCxxQkFBaUIsQ0FBQ0MsUUFBRCxFQUFXO0FBQ3hCLFdBQUtQLFdBQUwsR0FBbUJPLFFBQW5CO0FBQ0gsS0F6RUk7O0FBMkVMZ0MsZ0JBQVksR0FBRztBQUNYLFVBQUl4UyxHQUFHLEdBQUksR0FBRSxLQUFLa0IsTUFBTCxLQUFnQixJQUFoQixHQUF1QixFQUF2QixHQUE0QixNQUFNLEtBQUtBLE1BQU8sb0JBQTNEO0FBQUEsVUFDSTNGLElBQUksR0FBRyxLQUFLeVQsYUFBTCxFQURYO0FBQUEsVUFFSW5QLEtBQUssR0FBRyxJQUZaOztBQUlBcEMsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLGNBQU0sRUFBRSxNQURMO0FBRUh4RSxZQUFJLEVBQUVBLElBRkg7QUFHSHlFLFdBQUcsRUFBRUEsR0FIRjs7QUFJSEMsZUFBTyxDQUFDQyxRQUFELEVBQVc7QUFDZEwsZUFBSyxDQUFDK08sUUFBTixHQUFpQixLQUFqQjs7QUFFQS9PLGVBQUssQ0FBQ3VQLFFBQU4sQ0FBZW1ELE9BQWYsQ0FBdUJyVixJQUF2QixDQUE0QmdELFFBQVEsQ0FBQyxTQUFELENBQXBDOztBQUVBTCxlQUFLLENBQUM0UyxXQUFOOztBQUVBN1csZUFBSyxDQUFDc0UsUUFBUSxDQUFDLFNBQUQsQ0FBVCxFQUFzQixTQUF0QixDQUFMO0FBQ0gsU0FaRTs7QUFhSG5CLGFBQUssQ0FBQ0EsS0FBRCxFQUFRO0FBQ1RELGlCQUFPLENBQUM0QixHQUFSLENBQVkzQixLQUFaO0FBQ0g7O0FBZkUsT0FBUDtBQWlCSCxLQWpHSTs7QUFtR0wwVCxlQUFXLEdBQUc7QUFDVixXQUFLUCxTQUFMLEdBQWlCO0FBQ2JqRCxZQUFJLEVBQUUsRUFETztBQUVia0Isa0JBQVUsRUFBRSxLQUZDO0FBR2JDLHVCQUFlLEVBQUUsRUFISjtBQUlibUMsZUFBTyxFQUFFO0FBSkksT0FBakI7QUFNSCxLQTFHSTs7QUE0R0xHLGlCQUFhLEdBQUc7QUFDWixXQUFLQyxPQUFMLEdBQWUsQ0FBQyxLQUFLQSxPQUFyQjtBQUNILEtBOUdJOztBQWdITEMsZUFBVyxDQUFDclgsSUFBRCxFQUFPO0FBQ2QsVUFBSXlFLEdBQUcsR0FBSSxHQUFFLEtBQUtrQixNQUFMLEtBQWdCLElBQWhCLEdBQXVCLEVBQXZCLEdBQTRCLE1BQU0sS0FBS0EsTUFBTyxvQkFBbUIsS0FBS2tPLFFBQUwsQ0FBYzdSLEVBQUcsZUFBL0Y7QUFFQUUsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLGNBQU0sRUFBRSxNQURMO0FBRUh4RSxZQUFJLEVBQUVBLElBRkg7QUFHSHlFLFdBQUcsRUFBRUEsR0FIRjs7QUFJSEMsZUFBTyxDQUFDQyxRQUFELEVBQVc7QUFDZHRFLGVBQUssQ0FBQ3NFLFFBQUQsRUFBVyxTQUFYLENBQUw7QUFDSCxTQU5FOztBQU9IbkIsYUFBSyxDQUFDQSxLQUFELEVBQVE7QUFDVEQsaUJBQU8sQ0FBQzRCLEdBQVIsQ0FBWTNCLEtBQVo7QUFDSDs7QUFURSxPQUFQO0FBV0g7O0FBOUhJLEdBM0JrQjtBQTRKM0JXLFVBQVEsRUFBRTtBQUNObVQsZUFBVyxHQUFHO0FBQ1YsYUFBTyxDQUFDLEtBQUs3QyxTQUFOLElBQW1CLEtBQUtpQyxNQUEvQjtBQUNIOztBQUhLLEdBNUppQjtBQWtLM0JsTSxPQUFLLEVBQUU7QUFDSDhNLGVBQVcsRUFBRSxVQUFVYixRQUFWLEVBQW9CO0FBQzdCLFVBQUluUyxLQUFLLEdBQUcsSUFBWjs7QUFFQSxVQUFJbVMsUUFBSixFQUFjO0FBQ1YsYUFBS25ELFFBQUwsR0FBZ0JTLGtEQUFRLENBQUNDLE1BQVQsQ0FBZ0IsS0FBS25QLEtBQUwsQ0FBV21TLE9BQTNCLEVBQW9DO0FBQ2hEL0Msb0JBQVUsRUFBRSw2QkFEb0M7QUFFaEQsb0JBQVUsY0FGc0M7QUFHaERDLG1CQUFTLEVBQUUsR0FIcUM7QUFJaERDLGdCQUFNLEVBQUUsWUFBWTtBQUNoQixnQkFBSW5VLElBQUksR0FBRyxFQUFYOztBQUNBc0UsaUJBQUssQ0FBQ08sS0FBTixDQUFZbVMsT0FBWixDQUFvQjVDLFFBQXBCLENBQTZCN1IsT0FBN0IsQ0FBcUNSLElBQUksSUFBSS9CLElBQUksQ0FBQzJCLElBQUwsQ0FBVUksSUFBSSxDQUFDaUQsWUFBTCxDQUFrQixTQUFsQixDQUFWLENBQTdDOztBQUVBVixpQkFBSyxDQUFDK1MsV0FBTixDQUFrQjtBQUFDRSwyQkFBYSxFQUFFdlg7QUFBaEIsYUFBbEI7QUFDSDtBQVQrQyxTQUFwQyxDQUFoQjtBQVdILE9BWkQsTUFZTztBQUNILGFBQUtzVCxRQUFMLEdBQWdCLElBQWhCO0FBQ0g7QUFDSjtBQW5CRTtBQWxLb0IsQ0FBL0IsRTs7Ozs7Ozs7Ozs7OztBQ0hBO0FBQUE7QUFBQTtBQUVBelQsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxnQkFBZCxFQUFnQztBQUM1QnpDLFlBQVUsRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBRGdCO0FBRTVCMkMsVUFBUSxFQUFFLGlCQUZrQjtBQUc1QkMsT0FBSyxFQUFFO0FBQ0g0VCxXQUFPLEVBQUU7QUFDTHhULGNBQVEsRUFBRSxLQURMO0FBRUxGLFVBQUksRUFBRWdILE1BRkQ7QUFHTHJGLGFBQU8sRUFBRTtBQUhKLEtBRE47QUFNSHhCLGVBQVcsRUFBRTtBQUNURCxjQUFRLEVBQUUsSUFERDtBQUVURixVQUFJLEVBQUVDO0FBRkc7QUFOVixHQUhxQjs7QUFjNUIvRCxNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0h5WCxpQkFBVyxFQUFFLENBRFY7QUFFSEMsV0FBSyxFQUFFO0FBRkosS0FBUDtBQUlILEdBbkIyQjs7QUFvQjVCblcsU0FBTyxHQUFHO0FBQ04sU0FBS29XLFVBQUw7QUFDSCxHQXRCMkI7O0FBdUI1QmpXLFNBQU8sRUFBRTtBQUNMaVcsY0FBVSxHQUFHO0FBQ1QsV0FBS0QsS0FBTCxHQUFhRSxXQUFXLENBQUMsS0FBS0MsSUFBTixFQUFZLElBQVosQ0FBeEI7QUFDSCxLQUhJOztBQUtMQSxRQUFJLEdBQUc7QUFDSCxXQUFLSixXQUFMO0FBQ0g7O0FBUEksR0F2Qm1CO0FBZ0M1QnRULFVBQVEsRUFBRTtBQUNOMlQsaUJBQWEsR0FBRztBQUNaLGFBQU8sS0FBS04sT0FBTCxHQUFlLEtBQUtDLFdBQTNCO0FBQ0g7O0FBSEssR0FoQ2tCO0FBcUM1QmpOLE9BQUssRUFBRTtBQUNIc04saUJBQWEsQ0FBQ25RLEtBQUQsRUFBUTtBQUNqQixVQUFJQSxLQUFLLEtBQUssQ0FBZCxFQUFpQjtBQUNib1EscUJBQWEsQ0FBQyxLQUFLTCxLQUFOLENBQWI7QUFDQWpZLGNBQU0sQ0FBQ3dGLFFBQVAsQ0FBZ0JrRixJQUFoQixHQUF1QixLQUFLbEcsV0FBNUI7QUFDSDtBQUNKOztBQU5FO0FBckNxQixDQUFoQyxFOzs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQXBFLDJDQUFHLENBQUM0RCxTQUFKLENBQWMsZUFBZCxFQUErQjtBQUMzQkUsVUFBUSxFQUFHLDJEQURnQjtBQUUzQkMsT0FBSyxFQUFFO0FBQ0hvVSxpQkFBYSxFQUFFO0FBQ1h2UyxhQUFPLEVBQUUsTUFERTtBQUVYM0IsVUFBSSxFQUFFQyxNQUZLO0FBR1hDLGNBQVEsRUFBRTtBQUhDLEtBRFo7QUFNSHNLLGFBQVMsRUFBRTtBQUNQN0ksYUFBTyxFQUFFLE9BREY7QUFFUHpCLGNBQVEsRUFBRSxLQUZIO0FBR1BGLFVBQUksRUFBRUM7QUFIQyxLQU5SO0FBV0hrVSxRQUFJLEVBQUU7QUFDRnhTLGFBQU8sRUFBRSxRQURQO0FBRUZ6QixjQUFRLEVBQUUsS0FGUjtBQUdGRixVQUFJLEVBQUVDO0FBSEo7QUFYSCxHQUZvQjs7QUFtQjNCeEMsU0FBTyxHQUFHO0FBQ04sU0FBSzJXLFdBQUw7QUFFQSxRQUFJQywrQ0FBSixDQUFhLEtBQUtwVCxHQUFsQixFQUF1QjtBQUNuQnFULFdBQUssRUFBRSxDQURZO0FBRW5CQyxnQkFBVSxFQUFFLElBRk87QUFHbkJDLGNBQVEsRUFBRSxLQUhTO0FBSW5CQyxjQUFRLEVBQUUsS0FKUztBQUtuQkMsYUFBTyxFQUFFLElBTFU7QUFNbkJDLG9CQUFjLEVBQUU7QUFORyxLQUF2QjtBQVFILEdBOUIwQjs7QUErQjNCdFUsVUFBUSxFQUFFO0FBQ05nSyxVQUFNLEdBQUc7QUFDTCxVQUFJQSxNQUFNLEdBQUcsRUFBYjtBQUNBQSxZQUFNLENBQUN1SyxLQUFQLEdBQWUsS0FBS3BLLFNBQXBCOztBQUVBLFVBQUksS0FBSzJKLElBQUwsS0FBYyxPQUFsQixFQUEyQjtBQUN2QjlKLGNBQU0sQ0FBQ0UsTUFBUCxHQUFnQixPQUFoQjtBQUNILE9BRkQsTUFFTyxJQUFJLEtBQUs0SixJQUFMLEtBQWMsUUFBbEIsRUFBNEI7QUFDL0I5SixjQUFNLENBQUNFLE1BQVAsR0FBZ0IsT0FBaEI7QUFDSCxPQUZNLE1BRUE7QUFDSEYsY0FBTSxDQUFDRSxNQUFQLEdBQWdCLE9BQWhCO0FBQ0g7O0FBRUQsYUFBT0YsTUFBUDtBQUNIOztBQWRLLEdBL0JpQjtBQStDM0J6TSxTQUFPLEVBQUU7QUFDTHdXLGVBQVcsR0FBRztBQUNWLFVBQUlTLFlBQVksR0FBR3RXLFFBQVEsQ0FBQ3VXLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBbkI7QUFDQSxVQUFJQyxHQUFHLEdBQUd4VyxRQUFRLENBQUN5VyxjQUFULENBQXlCOzt3QkFFdkIsS0FBS3hLLFNBQVU7Ozs7OzthQUZqQixDQUFWO0FBU0FxSyxrQkFBWSxDQUFDaEcsTUFBYixDQUFvQmtHLEdBQXBCO0FBRUF4VyxjQUFRLENBQUNXLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0IyUCxNQUEvQixDQUFzQ2dHLFlBQXRDO0FBQ0g7O0FBZkk7QUEvQ2tCLENBQS9CLEU7Ozs7Ozs7Ozs7OztBQ05BO0FBQUE7QUFBQTtBQUVBOVksMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxtQ0FBZCxFQUFtRDtBQUMvQ0MsTUFBSSxFQUFFLGdDQUR5QztBQUUvQzFDLFlBQVUsRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBRm1DO0FBRy9DMkMsVUFBUSxFQUFFLG9DQUhxQztBQUkvQ0MsT0FBSyxFQUFFO0FBQ0h5UixRQUFJLEVBQUU7QUFDRnZSLFVBQUksRUFBRTBCLE9BREo7QUFFRnhCLGNBQVEsRUFBRSxLQUZSO0FBR0Z5QixhQUFPLEVBQUU7QUFIUDtBQURILEdBSndDOztBQVcvQ2xFLFNBQU8sR0FBRztBQUNOLFFBQUksS0FBSzhULElBQUwsS0FBYyxJQUFsQixFQUF3QjtBQUNwQm5ULE9BQUMsQ0FBQyxNQUFNLEtBQUsyQyxLQUFMLENBQVdDLEtBQVgsQ0FBaUJDLEdBQWpCLENBQXFCQyxZQUFyQixDQUFrQyxJQUFsQyxDQUFQLENBQUQsQ0FBaURGLEtBQWpELENBQXVELE1BQXZEO0FBQ0g7QUFDSjs7QUFmOEMsQ0FBbkQsRTs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUVBakYsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxrQkFBZCxFQUFrQztBQUM5QnpDLFlBQVUsRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBRGtCO0FBRTlCMkMsVUFBUSxFQUFFLG1CQUZvQjtBQUc5QkMsT0FBSyxFQUFFO0FBQ0g1QixNQUFFLEVBQUU7QUFDQThCLFVBQUksRUFBRUMsTUFETjtBQUVBQyxjQUFRLEVBQUU7QUFGVixLQUREO0FBS0gyQixVQUFNLEVBQUU7QUFDSjdCLFVBQUksRUFBRUMsTUFERjtBQUVKQyxjQUFRLEVBQUU7QUFGTixLQUxMO0FBU0grVSxVQUFNLEVBQUU7QUFDSmpWLFVBQUksRUFBRUMsTUFERjtBQUVKQyxjQUFRLEVBQUU7QUFGTixLQVRMO0FBYUhnVixrQkFBYyxFQUFFO0FBQ1psVixVQUFJLEVBQUVDLE1BRE07QUFFWkMsY0FBUSxFQUFFO0FBRkUsS0FiYjtBQWlCSGlWLG9CQUFnQixFQUFFO0FBQ2RuVixVQUFJLEVBQUVDLE1BRFE7QUFFZEMsY0FBUSxFQUFFLEtBRkk7QUFHZHlCLGFBQU8sRUFBRTtBQUhLLEtBakJmO0FBc0JIeVQsaUJBQWEsRUFBRTtBQUNYcFYsVUFBSSxFQUFFQyxNQURLO0FBRVhDLGNBQVEsRUFBRSxLQUZDO0FBR1h5QixhQUFPLEVBQUU7QUFIRSxLQXRCWjtBQTJCSDBULDJCQUF1QixFQUFFO0FBQ3JCclYsVUFBSSxFQUFFQyxNQURlO0FBRXJCQyxjQUFRLEVBQUUsS0FGVztBQUdyQnlCLGFBQU8sRUFBRTtBQUhZLEtBM0J0QjtBQWdDSDJULFdBQU8sRUFBRTtBQUNMdFYsVUFBSSxFQUFFQyxNQUREO0FBRUxDLGNBQVEsRUFBRSxLQUZMO0FBR0x5QixhQUFPLEVBQUU7QUFISixLQWhDTjtBQXFDSDRULGFBQVMsRUFBRTtBQUNQdlYsVUFBSSxFQUFFQyxNQURDO0FBRVBDLGNBQVEsRUFBRTtBQUZIO0FBckNSLEdBSHVCOztBQTZDOUJoRSxNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0hzWixzQkFBZ0IsRUFBRSxFQURmO0FBRUhsTyxXQUFLLEVBQUUsU0FGSjtBQUdIekQsV0FBSyxFQUFFLEVBSEo7QUFJSDRSLGNBQVEsRUFBRTtBQUpQLEtBQVA7QUFNSCxHQXBENkI7O0FBcUQ5QmhZLFNBQU8sR0FBRztBQUNOLFNBQUtvRyxLQUFMLEdBQWEsS0FBSzRSLFFBQUwsR0FBZ0IsS0FBS04sZ0JBQWxDO0FBQ0gsR0F2RDZCOztBQXdEOUJ2WCxTQUFPLEVBQUU7QUFDTG9ULFFBQUksR0FBRztBQUNILFVBQUl4USxLQUFLLEdBQUcsSUFBWjtBQUFBLFVBQ0lHLEdBQUcsR0FBSSxHQUFFLEtBQUsyVSxPQUFRLElBQUcsS0FBS3BYLEVBQUcsU0FEckM7O0FBR0FzQyxXQUFLLENBQUM4RyxLQUFOLEdBQWMsU0FBZDtBQUNBbEosT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLGNBQU0sRUFBRSxNQURMO0FBRUhDLFdBQUcsRUFBRUEsR0FGRjtBQUdIK1UsZUFBTyxFQUFFO0FBQ0wsMEJBQWdCLEtBQUtIO0FBRGhCLFNBSE47QUFNSHJaLFlBQUksRUFBRTtBQUFDMkgsZUFBSyxFQUFFLEtBQUtBO0FBQWIsU0FOSDs7QUFPSGpELGVBQU8sR0FBRztBQUNOSixlQUFLLENBQUNpVixRQUFOLEdBQWlCalYsS0FBSyxDQUFDcUQsS0FBdkI7QUFDQXJELGVBQUssQ0FBQzhHLEtBQU4sR0FBYyxTQUFkO0FBQ0EvSyxlQUFLLENBQUMsK0JBQUQsRUFBa0MsU0FBbEMsQ0FBTDtBQUNILFNBWEU7O0FBWUhtRCxhQUFLLENBQUNBLEtBQUQsRUFBUTtBQUNUYyxlQUFLLENBQUM4RyxLQUFOLEdBQWMsU0FBZDtBQUNBOUcsZUFBSyxDQUFDcUQsS0FBTixHQUFjckQsS0FBSyxDQUFDMlUsZ0JBQXBCO0FBQ0E1WSxlQUFLLENBQUNtRCxLQUFLLENBQUMsY0FBRCxDQUFMLENBQXNCdVAsTUFBdkIsRUFBK0IsT0FBL0IsQ0FBTDtBQUNIOztBQWhCRSxPQUFQO0FBa0JILEtBeEJJOztBQXlCTDBHLFVBQU0sR0FBRztBQUNMLFdBQUs5UixLQUFMLEdBQWEsS0FBSzRSLFFBQWxCO0FBQ0EsV0FBS25PLEtBQUwsR0FBYSxTQUFiO0FBQ0g7O0FBNUJJLEdBeERxQjtBQXVGOUJqSCxVQUFRLEVBQUU7QUFDTnVWLDBCQUFzQixHQUFHO0FBQ3JCLGFBQVEsR0FBRSxLQUFLUixhQUFjLEtBQUksS0FBS0MsdUJBQXdCLEVBQTlEO0FBQ0g7O0FBSEssR0F2Rm9CO0FBNkY5QjNPLE9BQUssRUFBRTtBQUNIWSxTQUFLLEVBQUUsVUFBUzZKLFFBQVQsRUFBbUI7QUFDdEIsVUFBSUEsUUFBUSxLQUFLLFNBQWpCLEVBQTRCO0FBQ3hCLGFBQUswRSxTQUFMLENBQWUsTUFBTTtBQUNqQixlQUFLOVUsS0FBTCxDQUFXK1UsU0FBWCxDQUFxQkMsS0FBckI7QUFDQSxlQUFLaFYsS0FBTCxDQUFXK1UsU0FBWCxDQUFxQkUsTUFBckI7QUFDSCxTQUhEO0FBSUg7QUFDSjtBQVJFO0FBN0Z1QixDQUFsQyxFOzs7Ozs7Ozs7Ozs7O0FDRkE7QUFBQTtBQUFBO0FBRUFqYSwyQ0FBRyxDQUFDNEQsU0FBSixDQUFjLG1CQUFkLEVBQW1DO0FBQy9CekMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FEbUI7QUFFL0IyQyxVQUFRLEVBQUUsb0JBRnFCO0FBRy9CQyxPQUFLLEVBQUU7QUFDSHlWLGFBQVMsRUFBRTtBQUNQdlYsVUFBSSxFQUFFQyxNQURDO0FBRVBDLGNBQVEsRUFBRTtBQUZILEtBRFI7QUFLSG9WLFdBQU8sRUFBRTtBQUNMdFYsVUFBSSxFQUFFQyxNQUREO0FBRUxDLGNBQVEsRUFBRSxLQUZMO0FBR0x5QixhQUFPLEVBQUU7QUFISjtBQUxOLEdBSHdCOztBQWMvQnpGLE1BQUksR0FBRztBQUNILFdBQU87QUFDSHNaLHNCQUFnQixFQUFFLEVBRGY7QUFFSGpOLGFBQU8sRUFBRSxFQUZOO0FBR0gwTixhQUFPLEVBQUUsRUFITjtBQUlIQyxvQkFBYyxFQUFFLEVBSmI7QUFLSEMsb0JBQWMsRUFBRSxFQUxiO0FBTUhDLGlCQUFXLEVBQUUsRUFOVjtBQU9IQyxtQkFBYSxFQUFFLEVBUFo7QUFRSGpCLG1CQUFhLEVBQUUsSUFSWjtBQVNIMUgsZUFBUyxFQUFFLElBVFI7QUFVSDRJLGFBQU8sRUFBRTtBQVZOLEtBQVA7QUFZSCxHQTNCOEI7O0FBNEIvQixRQUFNN1ksT0FBTixHQUFnQjtBQUNaLFVBQU0sS0FBSzhZLFVBQUwsRUFBTjtBQUNBLFVBQU0sS0FBS0MsVUFBTCxFQUFOO0FBQ0EsVUFBTSxLQUFLQyxlQUFMLEVBQU47QUFDSCxHQWhDOEI7O0FBaUMvQjdZLFNBQU8sRUFBRTtBQUNMOFksNEJBQXdCLEdBQUc7QUFDdkIsVUFBSSxLQUFLSixPQUFULEVBQWtCSyxZQUFZLENBQUMsS0FBS0wsT0FBTixDQUFaO0FBRWxCLFdBQUtBLE9BQUwsR0FBZTNFLFVBQVUsQ0FBQyxNQUFNO0FBQzVCLGFBQUs4RSxlQUFMO0FBQ0gsT0FGd0IsRUFFdEIsR0FGc0IsQ0FBekI7QUFHSCxLQVBJOztBQVNMLFVBQU1GLFVBQU4sR0FBbUI7QUFDZixVQUFJNVYsR0FBRyxHQUFJLEdBQUUsS0FBSzJVLE9BQVEsVUFBMUI7QUFBQSxVQUNJOVUsS0FBSyxHQUFHLElBRFo7O0FBR0EsWUFBTXBDLENBQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNURSxXQUFHLEVBQUVBLEdBREk7QUFFVEQsY0FBTSxFQUFFLEtBRkM7QUFHVGdWLGVBQU8sRUFBRTtBQUNMLDBCQUFnQixLQUFLSDtBQURoQixTQUhBO0FBTVQzVSxlQUFPLEVBQUUsVUFBVUMsUUFBVixFQUFvQjtBQUN6QkwsZUFBSyxDQUFDK0gsT0FBTixHQUFnQjFILFFBQVEsQ0FBQytWLE9BQVQsQ0FBaUJyTyxPQUFqQztBQUNBL0gsZUFBSyxDQUFDMFYsY0FBTixHQUF1QjFWLEtBQUssQ0FBQytILE9BQU4sQ0FBYyxDQUFkLENBQXZCO0FBQ0gsU0FUUTtBQVVUN0ksYUFBSyxFQUFFLFVBQVVtQixRQUFWLEVBQW9CO0FBQ3ZCcEIsaUJBQU8sQ0FBQzRCLEdBQVIsQ0FBWVIsUUFBWjtBQUNIO0FBWlEsT0FBUCxDQUFOO0FBY0gsS0EzQkk7O0FBNkJMLFVBQU0yVixVQUFOLEdBQW1CO0FBQ2YsVUFBSTdWLEdBQUcsR0FBSSxHQUFFLEtBQUsyVSxPQUFRLFVBQTFCO0FBQUEsVUFDSTlVLEtBQUssR0FBRyxJQURaOztBQUdBLFlBQU1wQyxDQUFDLENBQUNxQyxJQUFGLENBQU87QUFDVEUsV0FBRyxFQUFFQSxHQURJO0FBRVRELGNBQU0sRUFBRSxLQUZDO0FBR1RnVixlQUFPLEVBQUU7QUFDTCwwQkFBZ0IsS0FBS0g7QUFEaEIsU0FIQTtBQU1UM1UsZUFBTyxFQUFFLFVBQVVDLFFBQVYsRUFBb0I7QUFDekJMLGVBQUssQ0FBQ3lWLE9BQU4sR0FBZ0JwVixRQUFRLENBQUMrVixPQUFULENBQWlCWCxPQUFqQztBQUNBelYsZUFBSyxDQUFDMlYsY0FBTixHQUF1QjNWLEtBQUssQ0FBQ3lWLE9BQU4sQ0FBYyxDQUFkLENBQXZCO0FBQ0gsU0FUUTtBQVVUdlcsYUFBSyxFQUFFLFVBQVVtQixRQUFWLEVBQW9CO0FBQ3ZCcEIsaUJBQU8sQ0FBQzRCLEdBQVIsQ0FBWVIsUUFBWjtBQUNIO0FBWlEsT0FBUCxDQUFOO0FBY0gsS0EvQ0k7O0FBaURMLFVBQU00VixlQUFOLEdBQXdCO0FBQ3BCLFVBQUk5VixHQUFHLEdBQUksR0FBRSxLQUFLMlUsT0FBUSxnQkFBZSxLQUFLWSxjQUFlLFdBQVUsS0FBS0MsY0FBZSxRQUFPLEtBQUtDLFdBQVksVUFBUyxLQUFLQyxhQUFjLEVBQS9JO0FBQUEsVUFDSTdWLEtBQUssR0FBRyxJQURaOztBQUdBLFdBQUtrTixTQUFMLEdBQWlCLElBQWpCO0FBRUF0UCxPQUFDLENBQUNxQyxJQUFGLENBQU87QUFDSEUsV0FBRyxFQUFFQSxHQURGO0FBRUhELGNBQU0sRUFBRSxLQUZMO0FBR0hnVixlQUFPLEVBQUU7QUFDTCwwQkFBZ0IsS0FBS0g7QUFEaEIsU0FITjtBQU1IM1UsZUFBTyxFQUFFLFVBQVVDLFFBQVYsRUFBb0I7QUFDekJMLGVBQUssQ0FBQ2dWLGdCQUFOLEdBQXlCM1UsUUFBUSxDQUFDK1YsT0FBVCxDQUFpQm5PLFlBQTFDO0FBQ0FqSSxlQUFLLENBQUM0VSxhQUFOLEdBQXNCdlUsUUFBUSxDQUFDK1YsT0FBVCxDQUFpQnhCLGFBQXZDO0FBQ0E1VSxlQUFLLENBQUNrTixTQUFOLEdBQWtCLEtBQWxCO0FBQ0gsU0FWRTtBQVdIaE8sYUFBSyxFQUFFLFVBQVVtQixRQUFWLEVBQW9CO0FBQ3ZCcEIsaUJBQU8sQ0FBQzRCLEdBQVIsQ0FBWVIsUUFBWjtBQUNBTCxlQUFLLENBQUNrTixTQUFOLEdBQWtCLEtBQWxCO0FBQ0g7QUFkRSxPQUFQO0FBZ0JILEtBdkVJOztBQXlFTG1KLFNBQUssR0FBRztBQUNKLFdBQUtYLGNBQUwsR0FBc0IsS0FBSzNOLE9BQUwsQ0FBYSxDQUFiLENBQXRCO0FBQ0EsV0FBSzROLGNBQUwsR0FBc0IsS0FBS0YsT0FBTCxDQUFhLENBQWIsQ0FBdEI7QUFDQSxXQUFLRyxXQUFMLEdBQW1CLEVBQW5CO0FBQ0EsV0FBS0MsYUFBTCxHQUFxQixFQUFyQjtBQUNBLFdBQUtJLGVBQUw7QUFDSDs7QUEvRUk7QUFqQ3NCLENBQW5DLEU7Ozs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7QUFFQTFhLDJDQUFHLENBQUM0RCxTQUFKLENBQWMsWUFBZCxFQUE0QjtBQUN4QkUsVUFBUSxFQUFFLGFBRGM7QUFFeEJqQyxTQUFPLEVBQUU7QUFDTGtaLGVBQVcsR0FBRztBQUNWQyxZQUFNLENBQUNDLFFBQVAsQ0FBZ0JyYixNQUFoQixDQUF1QnNiLE1BQXZCO0FBQ0g7O0FBSEk7QUFGZSxDQUE1QixFOzs7Ozs7Ozs7OztBQ0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9FOzs7Ozs7Ozs7Ozs7QUMvRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVPLE1BQU1DLGFBQU4sQ0FBb0I7QUFDdkJyYixhQUFXLENBQUNzYixNQUFELEVBQVM7QUFDaEIsU0FBS0EsTUFBTCxHQUFjQSxNQUFkO0FBQ0g7O0FBRURDLE1BQUksR0FBRztBQUNILFVBQU1ELE1BQU0sR0FBRyxLQUFLQSxNQUFwQixDQURHLENBR0g7QUFDQTs7QUFDQUEsVUFBTSxDQUFDRSxFQUFQLENBQVVDLGdCQUFWLENBQTJCcE8sR0FBM0IsQ0FBK0IsYUFBL0IsRUFBOENySCxNQUFNLElBQUk7QUFFcEQ7QUFDQSxZQUFNMFYsVUFBVSxHQUFHLElBQUlDLG9GQUFKLENBQWUzVixNQUFmLENBQW5CO0FBRUEwVixnQkFBVSxDQUFDRSxHQUFYLENBQWU7QUFDWDdPLGFBQUssRUFBRSxjQURJO0FBRVg4TyxnQkFBUSxFQUFFLElBRkM7QUFHWHJaLGVBQU8sRUFBRTtBQUhFLE9BQWY7QUFNQWtaLGdCQUFVLENBQUMzSixFQUFYLENBQWMsU0FBZCxFQUF5QixNQUFNO0FBQzNCLGFBQUt1SixNQUFMLENBQVlRLE9BQVosQ0FBb0IsbUJBQXBCO0FBQ0gsT0FGRDtBQUlBLGFBQU9KLFVBQVA7QUFDSCxLQWhCRDtBQWlCSDs7QUEzQnNCO0FBOEJwQixNQUFNSyxrQkFBTixDQUF5QjtBQUM1Qi9iLGFBQVcsQ0FBQ3NiLE1BQUQsRUFBUztBQUNoQixTQUFLQSxNQUFMLEdBQWNBLE1BQWQ7QUFDSDs7QUFFRCxhQUFXVSxRQUFYLEdBQXNCO0FBQ2xCLFdBQU8sQ0FBQ0MsNkVBQUQsQ0FBUDtBQUNIOztBQUVEVixNQUFJLEdBQUc7QUFDSCxTQUFLVyxhQUFMOztBQUNBLFNBQUtDLGlCQUFMOztBQUVBLFNBQUtiLE1BQUwsQ0FBWWMsUUFBWixDQUFxQi9PLEdBQXJCLENBQXlCLG1CQUF6QixFQUE4QyxJQUFJZ1AsMERBQUosQ0FBc0IsS0FBS2YsTUFBM0IsQ0FBOUM7QUFDSDs7QUFFRFksZUFBYSxHQUFHO0FBQ1osVUFBTUksTUFBTSxHQUFHLEtBQUtoQixNQUFMLENBQVlpQixLQUFaLENBQWtCRCxNQUFqQztBQUVBQSxVQUFNLENBQUNFLFFBQVAsQ0FBZ0IsYUFBaEIsRUFBK0I7QUFDM0I7QUFDQUMsY0FBUSxFQUFFLElBRmlCO0FBSTNCO0FBQ0FDLGdCQUFVLEVBQUUsUUFMZTtBQU8zQkMscUJBQWUsRUFBRSxDQUFDLFVBQUQsRUFBYSxZQUFiO0FBUFUsS0FBL0I7QUFTSDs7QUFFRFIsbUJBQWlCLEdBQUc7QUFDaEIsVUFBTVMsVUFBVSxHQUFHLEtBQUt0QixNQUFMLENBQVlzQixVQUEvQixDQURnQixDQUdoQjs7QUFDQUEsY0FBVSxDQUFDQyxHQUFYLENBQWUsUUFBZixFQUF5QkMsZ0JBQXpCLENBQTBDO0FBQ3RDQyxVQUFJLEVBQUU7QUFDRmhaLFlBQUksRUFBRSxLQURKO0FBRUZpWixlQUFPLEVBQUUsQ0FBQyxhQUFEO0FBRlAsT0FEZ0M7QUFLdENULFdBQUssRUFBRSxDQUFDVSxXQUFELEVBQWNDLFdBQWQsS0FBOEI7QUFDakMsY0FBTUMsUUFBUSxHQUFHRixXQUFXLENBQUNHLFFBQVosQ0FBcUIsQ0FBckIsRUFBd0IvWCxZQUF4QixDQUFxQyxNQUFyQyxDQUFqQjs7QUFDQSxjQUFNZ1ksVUFBVSxHQUFHSixXQUFXLENBQUNHLFFBQVosQ0FBcUIsQ0FBckIsRUFBd0JBLFFBQXhCLENBQWlDLENBQWpDLEVBQW9DRSxTQUF2RDs7QUFFQSxlQUFPSixXQUFXLENBQUNqRSxhQUFaLENBQTBCLGFBQTFCLEVBQXlDO0FBQUNrRSxrQkFBRDtBQUFXRTtBQUFYLFNBQXpDLENBQVA7QUFDSDtBQVZxQyxLQUExQztBQWFBVCxjQUFVLENBQUNDLEdBQVgsQ0FBZSxpQkFBZixFQUFrQ0MsZ0JBQWxDLENBQW1EO0FBQy9DUCxXQUFLLEVBQUUsYUFEd0M7QUFFL0NRLFVBQUksRUFBRSxDQUFDUSxTQUFELEVBQVlDLFVBQVosS0FBMkI7QUFDN0IsY0FBTUMsYUFBYSxHQUFHQyxxQkFBcUIsQ0FBQ0gsU0FBRCxFQUFZQyxVQUFaLENBQTNDLENBRDZCLENBRzdCOztBQUNBLGVBQU9HLHFGQUFRLENBQUNGLGFBQUQsRUFBZ0JELFVBQWhCLENBQWY7QUFDSDtBQVA4QyxLQUFuRDtBQVVBWixjQUFVLENBQUNDLEdBQVgsQ0FBZSxjQUFmLEVBQStCQyxnQkFBL0IsQ0FBZ0Q7QUFDNUNQLFdBQUssRUFBRSxhQURxQztBQUU1Q1EsVUFBSSxFQUFFVztBQUZzQyxLQUFoRCxFQTNCZ0IsQ0FnQ2hCOztBQUNBLGFBQVNBLHFCQUFULENBQStCSCxTQUEvQixFQUEwQ0MsVUFBMUMsRUFBc0Q7QUFDbEQsWUFBTUwsUUFBUSxHQUFHSSxTQUFTLENBQUNsWSxZQUFWLENBQXVCLFVBQXZCLENBQWpCO0FBQ0EsWUFBTWdZLFVBQVUsR0FBR0UsU0FBUyxDQUFDbFksWUFBVixDQUF1QixZQUF2QixJQUF1Q2tZLFNBQVMsQ0FBQ2xZLFlBQVYsQ0FBdUIsWUFBdkIsQ0FBdkMsR0FBOEUsZUFBakcsQ0FGa0QsQ0FJbEQ7O0FBQ0EsWUFBTXVZLGVBQWUsR0FBR0osVUFBVSxDQUFDSyxzQkFBWCxDQUFrQyxLQUFsQyxFQUF5QztBQUM3REMsYUFBSyxFQUFFO0FBRHNELE9BQXpDLENBQXhCLENBTGtELENBU2xEOztBQUNBLFlBQU1DLFNBQVMsR0FBR1AsVUFBVSxDQUFDSyxzQkFBWCxDQUFrQyxHQUFsQyxFQUF1QztBQUNyREMsYUFBSyxFQUFFLHdCQUQ4QztBQUVyRHRULFlBQUksRUFBRTJTO0FBRitDLE9BQXZDLENBQWxCLENBVmtELENBZWxEOztBQUNBLFlBQU1hLFNBQVMsR0FBR1IsVUFBVSxDQUFDUyxVQUFYLENBQXNCWixVQUF0QixDQUFsQjtBQUNBRyxnQkFBVSxDQUFDVSxNQUFYLENBQWtCVixVQUFVLENBQUNXLGdCQUFYLENBQTRCSixTQUE1QixFQUF1QyxDQUF2QyxDQUFsQixFQUE2REMsU0FBN0QsRUFqQmtELENBbUJsRDs7QUFDQVIsZ0JBQVUsQ0FBQ1UsTUFBWCxDQUFrQlYsVUFBVSxDQUFDVyxnQkFBWCxDQUE0QlAsZUFBNUIsRUFBNkMsQ0FBN0MsQ0FBbEIsRUFBbUVHLFNBQW5FO0FBRUEsYUFBT0gsZUFBUDtBQUNIO0FBQ0o7O0FBdkYyQixDOzs7Ozs7Ozs7Ozs7QUNuQ2hDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFZSxNQUFNUSxjQUFOLENBQXFCO0FBQ2hDcGUsYUFBVyxDQUFDc2IsTUFBRCxFQUFTO0FBQ2hCLFNBQUtBLE1BQUwsR0FBY0EsTUFBZDtBQUNIOztBQUVEQyxNQUFJLEdBQUc7QUFDSCxVQUFNRCxNQUFNLEdBQUcsS0FBS0EsTUFBcEI7QUFDQSxVQUFNaUIsS0FBSyxHQUFHakIsTUFBTSxDQUFDaUIsS0FBckI7QUFDQSxVQUFNSyxVQUFVLEdBQUd0QixNQUFNLENBQUNzQixVQUExQjtBQUNBLFVBQU1OLE1BQU0sR0FBR0MsS0FBSyxDQUFDRCxNQUFyQjtBQUVBLFNBQUtoQixNQUFMLENBQVljLFFBQVosQ0FBcUIvTyxHQUFyQixDQUF5QixzQkFBekIsRUFBaUQsSUFBSWdSLDZEQUFKLENBQXlCLEtBQUsvQyxNQUE5QixDQUFqRCxFQU5HLENBUUg7O0FBQ0FnQixVQUFNLENBQUNFLFFBQVAsQ0FBZ0IsZ0JBQWhCLEVBQWtDO0FBQzlCOEIsYUFBTyxFQUFFLE9BRHFCO0FBRTlCN0IsY0FBUSxFQUFFLElBRm9CO0FBRzlCOEIsYUFBTyxFQUFFO0FBSHFCLEtBQWxDO0FBTUEzQixjQUFVLENBQUNDLEdBQVgsQ0FBZSxRQUFmLEVBQXlCQyxnQkFBekIsQ0FBMEM7QUFDdENDLFVBQUksRUFBRTtBQUNGaFosWUFBSSxFQUFFLElBREo7QUFFRmlaLGVBQU8sRUFBRSxDQUFDLGdCQUFEO0FBRlAsT0FEZ0M7QUFLdENULFdBQUssRUFBRSxDQUFDVSxXQUFELEVBQWNDLFdBQWQsS0FBOEI7QUFDakMsZUFBT0EsV0FBVyxDQUFDakUsYUFBWixDQUEwQixnQkFBMUIsQ0FBUDtBQUNIO0FBUHFDLEtBQTFDO0FBVUEyRCxjQUFVLENBQUNDLEdBQVgsQ0FBZSxpQkFBZixFQUFrQ0MsZ0JBQWxDLENBQW1EO0FBQy9DUCxXQUFLLEVBQUUsZ0JBRHdDO0FBRS9DUSxVQUFJLEVBQUUsQ0FBQ1EsU0FBRCxFQUFZQyxVQUFaLEtBQTJCO0FBQzdCLGNBQU1DLGFBQWEsR0FBR2Usd0JBQXdCLENBQUNqQixTQUFELEVBQVlDLFVBQVosQ0FBOUMsQ0FENkIsQ0FHN0I7O0FBQ0EsZUFBT0cscUZBQVEsQ0FBQ0YsYUFBRCxFQUFnQkQsVUFBaEIsQ0FBZjtBQUNIO0FBUDhDLEtBQW5EO0FBVUFaLGNBQVUsQ0FBQ0MsR0FBWCxDQUFlLGNBQWYsRUFBK0JDLGdCQUEvQixDQUFnRDtBQUM1Q1AsV0FBSyxFQUFFLGdCQURxQztBQUU1Q1EsVUFBSSxFQUFFeUI7QUFGc0MsS0FBaEQsRUFuQ0csQ0F3Q0g7O0FBQ0EsYUFBU0Esd0JBQVQsQ0FBa0NqQixTQUFsQyxFQUE2Q0MsVUFBN0MsRUFBeUQ7QUFFckQsYUFBT0EsVUFBVSxDQUFDSyxzQkFBWCxDQUFrQyxJQUFsQyxFQUF3QztBQUFDQyxhQUFLLEVBQUU7QUFBUixPQUF4QyxDQUFQO0FBQ0g7O0FBRUR4QyxVQUFNLENBQUNFLEVBQVAsQ0FBVUMsZ0JBQVYsQ0FBMkJwTyxHQUEzQixDQUErQixnQkFBL0IsRUFBaURySCxNQUFNLElBQUk7QUFDdkQsWUFBTStXLElBQUksR0FBRyxJQUFJcEIsb0ZBQUosQ0FBZTNWLE1BQWYsQ0FBYjtBQUVBK1csVUFBSSxDQUFDbkIsR0FBTCxDQUFTO0FBQ0w3TyxhQUFLLEVBQUUsaUJBREY7QUFFTDhPLGdCQUFRLEVBQUUsSUFGTDtBQUdMclosZUFBTyxFQUFFO0FBSEosT0FBVCxFQUh1RCxDQVN2RDs7QUFDQXVhLFVBQUksQ0FBQ2hMLEVBQUwsQ0FBUSxTQUFSLEVBQW1CLE1BQU07QUFDckIsYUFBS3VKLE1BQUwsQ0FBWVEsT0FBWixDQUFvQixzQkFBcEI7QUFDSCxPQUZEO0FBSUEsYUFBT2lCLElBQVA7QUFDSCxLQWZEO0FBZ0JIOztBQW5FK0IsQzs7Ozs7Ozs7Ozs7O0FDSnBDO0FBQUE7QUFBZSxNQUFNVixpQkFBTixDQUF3QjtBQUNuQ3JjLGFBQVcsQ0FBQ3NiLE1BQUQsRUFBUztBQUNoQixTQUFLQSxNQUFMLEdBQWNBLE1BQWQ7QUFDSDs7QUFFRFEsU0FBTyxHQUFHO0FBQ04sVUFBTXFCLFFBQVEsR0FBR3NCLE1BQU0sQ0FBQyxZQUFELENBQXZCO0FBQ0EsVUFBTXBCLFVBQVUsR0FBR29CLE1BQU0sQ0FBQyxhQUFELEVBQWdCLGVBQWhCLENBQXpCO0FBRUEsU0FBS25ELE1BQUwsQ0FBWWlCLEtBQVosQ0FBa0JtQyxNQUFsQixDQUF5QkMsTUFBTSxJQUFJO0FBQy9CLFlBQU1DLGFBQWEsR0FBR0QsTUFBTSxDQUFDMUYsYUFBUCxDQUFxQixhQUFyQixFQUFvQztBQUN0RGtFLGdCQUFRLEVBQUVBLFFBRDRDO0FBRXRERSxrQkFBVSxFQUFFQTtBQUYwQyxPQUFwQyxDQUF0QixDQUQrQixDQU0vQjs7QUFDQSxXQUFLL0IsTUFBTCxDQUFZaUIsS0FBWixDQUFrQnNDLGFBQWxCLENBQWdDRCxhQUFoQyxFQUErQyxLQUFLdEQsTUFBTCxDQUFZaUIsS0FBWixDQUFrQjdaLFFBQWxCLENBQTJCb2MsU0FBMUU7QUFDSCxLQVJEO0FBU0g7O0FBRURDLFNBQU8sR0FBRyxDQUNUOztBQXJCa0MsQzs7Ozs7Ozs7Ozs7O0FDQXZDO0FBQUE7QUFBZSxNQUFNVixvQkFBTixDQUEyQjtBQUN0Q3JlLGFBQVcsQ0FBQ3NiLE1BQUQsRUFBUztBQUNoQixTQUFLQSxNQUFMLEdBQWNBLE1BQWQ7QUFDSDs7QUFFRFEsU0FBTyxHQUFHO0FBQ04sU0FBS1IsTUFBTCxDQUFZaUIsS0FBWixDQUFrQm1DLE1BQWxCLENBQXlCQyxNQUFNLElBQUk7QUFDL0IsWUFBTUsscUJBQXFCLEdBQUdMLE1BQU0sQ0FBQzFGLGFBQVAsQ0FBcUIsZ0JBQXJCLENBQTlCLENBRCtCLENBRy9COztBQUNBMEYsWUFBTSxDQUFDVCxNQUFQLENBQWNjLHFCQUFkLEVBQXFDLEtBQUsxRCxNQUFMLENBQVlpQixLQUFaLENBQWtCN1osUUFBbEIsQ0FBMkJvYyxTQUEzQixDQUFxQzVFLEtBQTFFO0FBQ0F5RSxZQUFNLENBQUNNLFlBQVAsQ0FBb0JELHFCQUFwQixFQUEyQyxPQUEzQztBQUNILEtBTkQ7QUFPSDs7QUFFREQsU0FBTyxHQUFHLENBQ1Q7O0FBaEJxQyxDOzs7Ozs7Ozs7Ozs7QUNBMUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVlLE1BQU1HLFdBQU4sQ0FBa0I7QUFDN0JsZixhQUFXLENBQUNzYixNQUFELEVBQVM7QUFDaEIsU0FBS0EsTUFBTCxHQUFjQSxNQUFkO0FBQ0g7O0FBRURDLE1BQUksR0FBRztBQUNILFVBQU1ELE1BQU0sR0FBRyxLQUFLQSxNQUFwQjtBQUVBLFNBQUtBLE1BQUwsQ0FBWWMsUUFBWixDQUFxQi9PLEdBQXJCLENBQXlCLG1CQUF6QixFQUE4QyxJQUFJOFIsMERBQUosQ0FBc0IsS0FBSzdELE1BQTNCLENBQTlDO0FBRUFBLFVBQU0sQ0FBQ0UsRUFBUCxDQUFVQyxnQkFBVixDQUEyQnBPLEdBQTNCLENBQStCLGFBQS9CLEVBQThDckgsTUFBTSxJQUFJO0FBQ3BELFlBQU0rVyxJQUFJLEdBQUcsSUFBSXBCLG9GQUFKLENBQWUzVixNQUFmLENBQWI7QUFFQStXLFVBQUksQ0FBQ25CLEdBQUwsQ0FBUztBQUNMN08sYUFBSyxFQUFFLGNBREY7QUFFTG1FLFlBQUksRUFBRWtPLHNGQUZEO0FBR0w1YyxlQUFPLEVBQUU7QUFISixPQUFULEVBSG9ELENBU3BEOztBQUNBdWEsVUFBSSxDQUFDaEwsRUFBTCxDQUFRLFNBQVIsRUFBbUIsTUFBTTtBQUNyQixjQUFNc04sUUFBUSxHQUFHWixNQUFNLENBQUMsV0FBRCxDQUF2QjtBQUVBbkQsY0FBTSxDQUFDaUIsS0FBUCxDQUFhbUMsTUFBYixDQUFvQkMsTUFBTSxJQUFJO0FBQzFCLGdCQUFNVyxZQUFZLEdBQUdYLE1BQU0sQ0FBQzFGLGFBQVAsQ0FBcUIsT0FBckIsRUFBOEI7QUFDL0NzRyxlQUFHLEVBQUVGO0FBRDBDLFdBQTlCLENBQXJCLENBRDBCLENBSzFCOztBQUNBL0QsZ0JBQU0sQ0FBQ2lCLEtBQVAsQ0FBYXNDLGFBQWIsQ0FBMkJTLFlBQTNCLEVBQXlDaEUsTUFBTSxDQUFDaUIsS0FBUCxDQUFhN1osUUFBYixDQUFzQm9jLFNBQS9EO0FBQ0gsU0FQRDtBQVFILE9BWEQ7QUFhQSxhQUFPL0IsSUFBUDtBQUNILEtBeEJEO0FBeUJIOztBQW5DNEIsQzs7Ozs7Ozs7Ozs7O0FDSmpDO0FBQUE7QUFBZSxNQUFNb0MsaUJBQU4sQ0FBd0I7QUFDbkNuZixhQUFXLENBQUNzYixNQUFELEVBQVM7QUFDaEIsU0FBS0EsTUFBTCxHQUFjQSxNQUFkO0FBQ0g7O0FBRURRLFNBQU8sR0FBRztBQUNOLFVBQU1oWCxHQUFHLEdBQUcyWixNQUFNLENBQUMsV0FBRCxDQUFsQjtBQUVBLFNBQUtuRCxNQUFMLENBQVlpQixLQUFaLENBQWtCbUMsTUFBbEIsQ0FBeUJDLE1BQU0sSUFBSTtBQUMvQixZQUFNVyxZQUFZLEdBQUdYLE1BQU0sQ0FBQzFGLGFBQVAsQ0FBcUIsT0FBckIsRUFBOEI7QUFDL0NzRyxXQUFHLEVBQUV6YTtBQUQwQyxPQUE5QixDQUFyQixDQUQrQixDQUsvQjs7QUFDQSxXQUFLd1csTUFBTCxDQUFZaUIsS0FBWixDQUFrQnNDLGFBQWxCLENBQWdDUyxZQUFoQyxFQUE4QyxLQUFLaEUsTUFBTCxDQUFZaUIsS0FBWixDQUFrQjdaLFFBQWxCLENBQTJCb2MsU0FBekU7QUFDSCxLQVBEO0FBUUg7O0FBRURDLFNBQU8sR0FBRyxDQUNUOztBQW5Ca0MsQzs7Ozs7Ozs7Ozs7O0FDQXZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFZSxNQUFNUyxPQUFOLENBQWM7QUFDekJ4ZixhQUFXLENBQUNzYixNQUFELEVBQVM7QUFDaEIsU0FBS0EsTUFBTCxHQUFjQSxNQUFkO0FBQ0g7O0FBRUQsYUFBV1UsUUFBWCxHQUFzQjtBQUNsQixXQUFPLENBQUNELCtEQUFELEVBQXFCcUMsdURBQXJCLEVBQXFDL0MsMERBQXJDLEVBQW9Eb0Usa0RBQXBELENBQVA7QUFDSDs7QUFQd0IsQzs7Ozs7Ozs7Ozs7O0FDSjdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFFZSxNQUFNQSxTQUFOLENBQWdCO0FBQzNCemYsYUFBVyxDQUFDc2IsTUFBRCxFQUFTO0FBQ2hCLFNBQUtBLE1BQUwsR0FBY0EsTUFBZDtBQUNIOztBQUVEQyxNQUFJLEdBQUc7QUFDSCxVQUFNRCxNQUFNLEdBQUcsS0FBS0EsTUFBcEIsQ0FERyxDQUdIO0FBQ0E7O0FBQ0FBLFVBQU0sQ0FBQ0UsRUFBUCxDQUFVQyxnQkFBVixDQUEyQnBPLEdBQTNCLENBQStCLFNBQS9CLEVBQTBDckgsTUFBTSxJQUFJO0FBQ2hELFlBQU0wWixZQUFZLEdBQUdDLGdHQUFjLENBQUMzWixNQUFELENBQW5DLENBRGdELENBR2hEOztBQUNBNFoseUdBQWlCLENBQUNGLFlBQUQsRUFBZUcsMkJBQTJCLEVBQTFDLENBQWpCO0FBRUFILGtCQUFZLENBQUNoRSxVQUFiLENBQXdCRSxHQUF4QixDQUE0QjtBQUN4QjdPLGFBQUssRUFBRSxTQURpQjtBQUV4QnZLLGVBQU8sRUFBRSxJQUZlO0FBR3hCcVosZ0JBQVEsRUFBRTtBQUhjLE9BQTVCLEVBTmdELENBWWhEOztBQUNBNkQsa0JBQVksQ0FBQzNOLEVBQWIsQ0FBZ0IsU0FBaEIsRUFBNEIrTixHQUFELElBQVM7QUFDaEN4RSxjQUFNLENBQUNRLE9BQVAsQ0FBZWdFLEdBQUcsQ0FBQ0MsTUFBSixDQUFXQyxZQUExQjtBQUNBMUUsY0FBTSxDQUFDMkUsT0FBUCxDQUFlbEQsSUFBZixDQUFvQjdDLEtBQXBCO0FBQ0gsT0FIRDtBQUtBLGFBQU93RixZQUFQO0FBQ0gsS0FuQkQ7QUFvQkg7O0FBOUIwQjs7QUFpQy9CLFNBQVNHLDJCQUFULEdBQXVDO0FBQ25DLFFBQU1LLGVBQWUsR0FBRyxJQUFJQyxnRkFBSixFQUF4QjtBQUVBLFFBQU1DLGVBQWUsR0FBRztBQUNwQmpjLFFBQUksRUFBRSxRQURjO0FBRXBCb1ksU0FBSyxFQUFFLElBQUk4RCx3RUFBSixDQUFVO0FBQ2JMLGtCQUFZLEVBQUUsbUJBREQ7QUFFYmpULFdBQUssRUFBRSxlQUZNO0FBR2I4TyxjQUFRLEVBQUU7QUFIRyxLQUFWO0FBRmEsR0FBeEIsQ0FIbUMsQ0FZbkM7O0FBQ0FxRSxpQkFBZSxDQUFDN1MsR0FBaEIsQ0FBb0IrUyxlQUFwQjtBQUVBLFFBQU1FLHFCQUFxQixHQUFHO0FBQzFCbmMsUUFBSSxFQUFFLFFBRG9CO0FBRTFCb1ksU0FBSyxFQUFFLElBQUk4RCx3RUFBSixDQUFVO0FBQ2JMLGtCQUFZLEVBQUUsbUJBREQ7QUFFYmpULFdBQUssRUFBRSxjQUZNO0FBR2I4TyxjQUFRLEVBQUU7QUFIRyxLQUFWO0FBRm1CLEdBQTlCLENBZm1DLENBd0JuQzs7QUFDQXFFLGlCQUFlLENBQUM3UyxHQUFoQixDQUFvQmlULHFCQUFwQjtBQUVBLFFBQU1DLHdCQUF3QixHQUFHO0FBQzdCcGMsUUFBSSxFQUFFLFFBRHVCO0FBRTdCb1ksU0FBSyxFQUFFLElBQUk4RCx3RUFBSixDQUFVO0FBQ2JMLGtCQUFZLEVBQUUsc0JBREQ7QUFFYmpULFdBQUssRUFBRSxpQkFGTTtBQUdiOE8sY0FBUSxFQUFFO0FBSEcsS0FBVjtBQUZzQixHQUFqQyxDQTNCbUMsQ0FvQ25DOztBQUNBcUUsaUJBQWUsQ0FBQzdTLEdBQWhCLENBQW9Ca1Qsd0JBQXBCO0FBRUEsU0FBT0wsZUFBUDtBQUNILEM7Ozs7Ozs7Ozs7OztBQzlFRDtBQUFBO0FBQWUsTUFBTU0sbUJBQU4sQ0FBMEI7QUFDckN4Z0IsYUFBVyxDQUFDeWdCLE1BQUQsRUFBU0MsU0FBVCxFQUFvQjtBQUMzQjtBQUNBLFNBQUtELE1BQUwsR0FBY0EsTUFBZDtBQUNBLFNBQUtqZCxHQUFMLEdBQVcsSUFBWDtBQUNBLFNBQUtrZCxTQUFMLEdBQWlCQSxTQUFqQjtBQUNILEdBTm9DLENBUXJDOzs7QUFDQUMsUUFBTSxHQUFHO0FBQ0wsUUFBSTdiLEdBQUcsR0FBRyxLQUFLNGIsU0FBZjtBQUFBLFFBQ0lFLElBQUksR0FBRyxJQURYO0FBR0EsV0FBTyxLQUFLSCxNQUFMLENBQVk3TixJQUFaLENBQ0ZpTyxJQURFLENBQ0lDLFlBQVksSUFBSTtBQUNuQixhQUFPLElBQUlDLE9BQUosQ0FBYSxDQUFFQyxPQUFGLEVBQVdDLE1BQVgsS0FBdUI7QUFDdkMsY0FBTTVnQixJQUFJLEdBQUcsSUFBSTBTLFFBQUosRUFBYjtBQUNBMVMsWUFBSSxDQUFDMlMsTUFBTCxDQUFhLFFBQWIsRUFBdUI4TixZQUF2QjtBQUVBRixZQUFJLENBQUNwZCxHQUFMLEdBQVdqQixDQUFDLENBQUNxQyxJQUFGLENBQU87QUFDZEUsYUFBRyxFQUFFQSxHQURTO0FBRWR6RSxjQUZjO0FBR2Q2UyxxQkFBVyxFQUFFLEtBSEM7QUFJZEQscUJBQVcsRUFBRSxLQUpDO0FBS2Q5TyxjQUFJLEVBQUUsTUFMUTtBQU1kWSxpQkFBTyxFQUFFQyxRQUFRLElBQUk7QUFDakJnYyxtQkFBTyxDQUFDO0FBQ0psYixxQkFBTyxFQUFFZCxRQUFRLENBQUNGO0FBRGQsYUFBRCxDQUFQO0FBR0gsV0FWYTtBQVdkakIsZUFBSyxFQUFHbUIsUUFBRCxJQUFjO0FBQ2pCLGdCQUFJM0UsSUFBSSxHQUFHMkUsUUFBUSxDQUFDa0gsWUFBcEI7QUFDQStVLGtCQUFNLENBQUM1Z0IsSUFBSSxJQUFJQSxJQUFJLENBQUN3RCxLQUFiLEdBQXFCeEQsSUFBSSxDQUFDd0QsS0FBTCxDQUFXbEQsT0FBaEMsR0FBMEMsZ0JBQTNDLENBQU47QUFDSDtBQWRhLFNBQVAsQ0FBWDtBQWdCSCxPQXBCTSxDQUFQO0FBcUJILEtBdkJFLENBQVA7QUF3QkgsR0FyQ29DLENBdUNyQzs7O0FBQ0F1Z0IsT0FBSyxHQUFHO0FBQ0osUUFBSSxLQUFLMWQsR0FBVCxFQUFjO0FBQ1YsV0FBS0EsR0FBTCxDQUFTMGQsS0FBVDtBQUNIO0FBQ0o7O0FBNUNvQyxDOzs7Ozs7Ozs7Ozs7O0FDQXpDO0FBQUE7QUFBQTtBQUVlLDZFQUFjQyxtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFFO0FBQ0xyaEIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQixhQUFwQixFQUFtQ0YsSUFBSSxJQUFJO0FBQ3ZDLFVBQUlBLElBQUksQ0FBQzJILEtBQUwsSUFBYyxLQUFLcVosUUFBdkIsRUFBaUM7QUFDN0IsYUFBS3hlLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJDLE1BQXZCLENBQThCLFFBQTlCO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsYUFBS3ZLLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJFLEdBQXZCLENBQTJCLFFBQTNCO0FBQ0g7QUFDSixLQU5EO0FBT0g7O0FBRUQsTUFBSWdVLFFBQUosR0FBZTtBQUNYLFdBQU9oTCxRQUFRLENBQUMsS0FBS3hULE9BQUwsQ0FBYXllLE9BQWIsQ0FBcUJELFFBQXRCLENBQWY7QUFDSDs7QUFibUMsQzs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQUE7QUFBQTtBQUVlLDZFQUFjRixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFFO0FBQ0xyaEIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQixzQkFBcEIsRUFBNENGLElBQUksSUFBSTtBQUNoRCxVQUFJQSxJQUFJLENBQUNraEIsWUFBTCxLQUFzQixLQUFLQSxZQUEvQixFQUE2QztBQUN6QyxZQUFJbGhCLElBQUksQ0FBQzJILEtBQUwsS0FBZSxzQ0FBbkIsRUFBMkQ7QUFBRTtBQUN6RCxlQUFLbkYsT0FBTCxDQUFhMmUsWUFBYixDQUEwQixVQUExQixFQUFzQyxVQUF0QztBQUNILFNBRkQsTUFFTztBQUNILGVBQUszZSxPQUFMLENBQWE0ZSxlQUFiLENBQTZCLFVBQTdCO0FBQ0g7QUFDSjtBQUNKLEtBUkQ7QUFTSDs7QUFFRCxNQUFJRixZQUFKLEdBQW1CO0FBQ2YsV0FBTyxLQUFLMWUsT0FBTCxDQUFheWUsT0FBYixDQUFxQkMsWUFBNUI7QUFDSDs7QUFmbUMsQzs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQUE7QUFBQTtBQUVlLDZFQUFjSixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFHO0FBQ04sU0FBSy9lLEVBQUwsR0FBVSxLQUFLUSxPQUFMLENBQWF3QyxZQUFiLENBQTBCLElBQTFCLENBQVY7QUFFQSxTQUFLcWMsY0FBTCxDQUFvQixLQUFLcmhCLElBQUwsQ0FBVXNoQixHQUFWLENBQWMsT0FBZCxDQUFwQjtBQUNIOztBQUVERCxnQkFBYyxHQUFHO0FBQ2IsUUFBSTFJLFlBQVksR0FBR3RXLFFBQVEsQ0FBQ3VXLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBbkI7QUFFQSxRQUFJQyxHQUFHLEdBQUd4VyxRQUFRLENBQUN5VyxjQUFULENBQXlCO2NBQzdCLEtBQUt5SSxRQUFTO2NBQ2QsS0FBS0MsY0FBZTtTQUZoQixDQUFWO0FBSUE3SSxnQkFBWSxDQUFDaEcsTUFBYixDQUFvQmtHLEdBQXBCO0FBRUF4VyxZQUFRLENBQUNXLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0IyUCxNQUEvQixDQUFzQ2dHLFlBQXRDO0FBQ0g7O0FBRUQsTUFBSTRJLFFBQUosR0FBZTtBQUNYLFFBQUksS0FBS3ZoQixJQUFMLENBQVV5aEIsR0FBVixDQUFjLE9BQWQsQ0FBSixFQUE0QjtBQUN4QixhQUFRO2VBQ0wsS0FBS3pmLEVBQUc7d0NBQ2lCLEtBQUtoQyxJQUFMLENBQVVzaEIsR0FBVixDQUFjLE9BQWQsQ0FBdUI7Ozs7O2FBRm5EO0FBUUg7O0FBRUQsV0FBTyxFQUFQO0FBQ0g7O0FBRUQsTUFBSUUsY0FBSixHQUFxQjtBQUNqQixRQUFJLEtBQUt4aEIsSUFBTCxDQUFVeWhCLEdBQVYsQ0FBYyxhQUFkLENBQUosRUFBa0M7QUFDOUIsYUFBUTs7bUJBRUQsS0FBS3pmLEVBQUc7NENBQ2lCLEtBQUtoQyxJQUFMLENBQVVzaEIsR0FBVixDQUFjLGFBQWQsQ0FBNkI7Ozs7OzthQUg3RDtBQVVIOztBQUVELFdBQU8sRUFBUDtBQUNIOztBQWpEbUMsQzs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQUE7QUFBQTtBQUVlLDZFQUFjUixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFFO0FBQ0xyaEIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQixzQkFBcEIsRUFBNENGLElBQUksSUFBSTtBQUNoRCxVQUFJQSxJQUFJLENBQUNraEIsWUFBTCxLQUFzQixLQUFLQSxZQUEvQixFQUE2QztBQUN6QyxZQUFJbGhCLElBQUksQ0FBQzJILEtBQUwsS0FBZSxzQ0FBbkIsRUFBMkQ7QUFBRTtBQUN6RCxlQUFLbkYsT0FBTCxDQUFhc0ssU0FBYixDQUF1QkMsTUFBdkIsQ0FBOEIsUUFBOUI7QUFDSCxTQUZELE1BRU87QUFDSCxlQUFLdkssT0FBTCxDQUFhc0ssU0FBYixDQUF1QkUsR0FBdkIsQ0FBMkIsUUFBM0I7QUFDQSxlQUFLeEssT0FBTCxDQUFhRixnQkFBYixDQUE4QixRQUE5QixFQUF3Q0MsT0FBeEMsQ0FBZ0RDLE9BQU8sSUFBSUEsT0FBTyxDQUFDbUYsS0FBUixHQUFnQixFQUEzRTtBQUNIO0FBQ0o7QUFDSixLQVREO0FBVUg7O0FBRUQsTUFBSXVaLFlBQUosR0FBbUI7QUFDZixXQUFPLEtBQUsxZSxPQUFMLENBQWF5ZSxPQUFiLENBQXFCQyxZQUE1QjtBQUNIOztBQWhCbUMsQzs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQUE7QUFBQTtBQUVlLDZFQUFjSixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFHO0FBQ04sUUFBSXJVLEtBQUssR0FBRyxLQUFLbEssT0FBTCxDQUFha2YsVUFBYixDQUF3QjFlLGFBQXhCLENBQXNDLG9CQUF0QyxDQUFaO0FBQ0EwSixTQUFLLENBQUN5VSxZQUFOLENBQW1CLGFBQW5CLEVBQWtDLEtBQUszZSxPQUFMLENBQWF3QyxZQUFiLENBQTBCLG9CQUExQixDQUFsQztBQUNIOztBQUVEcVosUUFBTSxHQUFHO0FBQ0wsUUFBSTNSLEtBQUssR0FBRyxLQUFLbEssT0FBTCxDQUFha2YsVUFBYixDQUF3QjFlLGFBQXhCLENBQXNDLG9CQUF0QyxDQUFaOztBQUVBLFFBQUksS0FBS1IsT0FBTCxDQUFhZ1EsS0FBYixDQUFtQjVJLE1BQW5CLEdBQTRCLENBQWhDLEVBQW1DO0FBQy9COEMsV0FBSyxDQUFDaVYsU0FBTixHQUFrQixXQUFXLEtBQUtuZixPQUFMLENBQWFnUSxLQUFiLENBQW1CLENBQW5CLEVBQXNCOU8sSUFBakMsR0FBd0MsU0FBMUQ7QUFDSCxLQUZELE1BRU87QUFDSGdKLFdBQUssQ0FBQ2lWLFNBQU4sR0FBa0IsZUFBbEI7QUFDSDtBQUNKOztBQWRtQyxDOzs7Ozs7Ozs7Ozs7QUNGeEM7QUFBQTtBQUFBO0FBRWUsNkVBQWNiLG1EQUFkLENBQXlCO0FBQ3BDQyxTQUFPLEdBQUc7QUFDTixRQUFJclUsS0FBSyxHQUFHLEtBQUtsSyxPQUFMLENBQWFrZixVQUFiLENBQXdCMWUsYUFBeEIsQ0FBc0Msb0JBQXRDLENBQVo7QUFDQTBKLFNBQUssQ0FBQ3lVLFlBQU4sQ0FBbUIsYUFBbkIsRUFBa0MsS0FBSzNlLE9BQUwsQ0FBYXdDLFlBQWIsQ0FBMEIsb0JBQTFCLENBQWxDO0FBQ0g7O0FBRURxWixRQUFNLEdBQUc7QUFDTCxRQUFJM1IsS0FBSyxHQUFHLEtBQUtsSyxPQUFMLENBQWFrZixVQUFiLENBQXdCMWUsYUFBeEIsQ0FBc0Msb0JBQXRDLENBQVo7QUFBQSxRQUNJNGUsU0FBUyxHQUFHLEVBRGhCO0FBR0EsU0FBS3BmLE9BQUwsQ0FBYWdRLEtBQWIsQ0FBbUJqUSxPQUFuQixDQUEyQmdRLElBQUksSUFBSTtBQUNoQ3FQLGVBQVMsQ0FBQ2pnQixJQUFWLENBQWU0USxJQUFJLENBQUM3TyxJQUFwQjtBQUNGLEtBRkQ7QUFJQSxRQUFJbWUsZUFBZSxHQUFHRCxTQUFTLENBQUN4TCxJQUFWLENBQWUsSUFBZixDQUF0QjtBQUVBMUosU0FBSyxDQUFDaVYsU0FBTixHQUFrQixXQUFXRSxlQUFYLEdBQTZCLFNBQS9DO0FBQ0g7O0FBakJtQyxDOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0Z4QztBQUVlLHFCQUFjZixtREFBZCxDQUF5QjtBQUdwQ2dCLFdBQVMsQ0FBQy9oQixLQUFELEVBQVE7QUFDYixRQUFJZ2lCLEtBQUssR0FBRyxLQUFLQyxXQUFMLENBQWlCcmEsS0FBN0I7QUFBQSxRQUNJbEQsR0FBRyxHQUFHLEtBQUt6RSxJQUFMLENBQVVzaEIsR0FBVixDQUFjLGNBQWQsQ0FEVjtBQUFBLFFBRUloZCxLQUFLLEdBQUcsSUFGWjs7QUFJQXZFLFNBQUssQ0FBQ2tpQixjQUFOO0FBRUEvZixLQUFDLENBQUNxQyxJQUFGLENBQU87QUFDSEMsWUFBTSxFQUFFLE1BREw7QUFFSEMsU0FBRyxFQUFFQSxHQUZGO0FBR0h6RSxVQUFJLEVBQUU7QUFBQyxpQkFBUytoQjtBQUFWLE9BSEg7O0FBSUhyZCxhQUFPLENBQUNDLFFBQUQsRUFBVztBQUNkdEUsYUFBSyxDQUFDc0UsUUFBRCxFQUFXLFNBQVgsQ0FBTDtBQUNBTCxhQUFLLENBQUMwZCxXQUFOLENBQWtCcmEsS0FBbEIsR0FBMEIsRUFBMUI7QUFDSCxPQVBFOztBQVFIbkUsV0FBSyxDQUFDQSxLQUFELEVBQVE7QUFDVG5ELGFBQUssQ0FBQ21ELEtBQUssQ0FBQyxjQUFELENBQU4sRUFBd0IsT0FBeEIsQ0FBTDtBQUNIOztBQVZFLEtBQVA7QUFZSDs7QUF0Qm1DOzttQ0FDbkIsQ0FBQyxPQUFELEM7Ozs7Ozs7Ozs7Ozs7QUNIckI7QUFBQTtBQUFBO0FBRWUsNkVBQWNzZCxtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFHO0FBQ04sUUFBSW1CLE1BQU0sR0FBRyxLQUFLMWYsT0FBTCxDQUFhd0MsWUFBYixDQUEwQixjQUExQixDQUFiO0FBQ0EsUUFBSW1kLFdBQVcsR0FBRyxLQUFLM2YsT0FBdkI7QUFDQTRmLGNBQVUsQ0FBQ0MsS0FBWCxDQUFpQixZQUFZO0FBQ3pCRCxnQkFBVSxDQUFDM0csT0FBWCxDQUFtQnlHLE1BQW5CLEVBQTJCO0FBQUNJLGNBQU0sRUFBRTtBQUFULE9BQTNCLEVBQ0Y5QixJQURFLENBQ0csVUFBVTFkLEtBQVYsRUFBaUI7QUFDaEJxZixtQkFBVyxDQUFDeGEsS0FBWixHQUFvQjdFLEtBQXBCO0FBQ0gsT0FIRDtBQUlILEtBTEQ7QUFNSDs7QUFWbUMsQzs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQUE7QUFBQTtBQUVlLDZFQUFjZ2UsbURBQWQsQ0FBeUI7QUFDcENDLFNBQU8sR0FBRztBQUNOcmhCLGdCQUFZLENBQUNRLE1BQWIsQ0FBb0Isc0JBQXBCLEVBQTRDRixJQUFJLElBQUk7QUFDaEQsVUFBSUEsSUFBSSxDQUFDa2hCLFlBQUwsS0FBc0IsS0FBS0EsWUFBL0IsRUFBNkM7QUFDekMsWUFBSWxoQixJQUFJLENBQUMySCxLQUFMLEtBQWUsR0FBbkIsRUFBd0I7QUFDcEIsZUFBS25GLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJDLE1BQXZCLENBQThCLFFBQTlCO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsZUFBS3ZLLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJFLEdBQXZCLENBQTJCLFFBQTNCO0FBQ0g7QUFDSjtBQUNKLEtBUkQ7QUFTSDs7QUFFRCxNQUFJa1UsWUFBSixHQUFtQjtBQUNmLFdBQU8sS0FBSzFlLE9BQUwsQ0FBYXllLE9BQWIsQ0FBcUJDLFlBQTVCO0FBQ0g7O0FBZm1DLEM7Ozs7Ozs7Ozs7OztBQ0Z4QztBQUFBO0FBQUE7QUFFZSw2RUFBY0osbURBQWQsQ0FBeUI7QUFDcENDLFNBQU8sR0FBRTtBQUNMcmhCLGdCQUFZLENBQUNRLE1BQWIsQ0FBb0Isc0JBQXBCLEVBQTRDRixJQUFJLElBQUk7QUFDaEQsVUFBSUEsSUFBSSxDQUFDa2hCLFlBQUwsS0FBc0IsS0FBS0EsWUFBL0IsRUFBNkM7QUFDekMsWUFBSWxoQixJQUFJLENBQUMySCxLQUFMLEtBQWUsc0NBQW5CLEVBQTJEO0FBQUU7QUFDekQsZUFBS25GLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJDLE1BQXZCLENBQThCLFFBQTlCO0FBQ0EsZUFBS3ZLLE9BQUwsQ0FBYVEsYUFBYixDQUEyQixPQUEzQixFQUFvQ21lLFlBQXBDLENBQWlELFVBQWpELEVBQTZELFVBQTdEO0FBQ0gsU0FIRCxNQUdPO0FBQ0gsZUFBSzNlLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJFLEdBQXZCLENBQTJCLFFBQTNCO0FBQ0EsZUFBS3hLLE9BQUwsQ0FBYVEsYUFBYixDQUEyQixPQUEzQixFQUFvQ29lLGVBQXBDLENBQW9ELFVBQXBEO0FBQ0g7QUFDSjtBQUNKLEtBVkQ7QUFXSDs7QUFFRCxNQUFJRixZQUFKLEdBQW1CO0FBQ2YsV0FBTyxLQUFLMWUsT0FBTCxDQUFheWUsT0FBYixDQUFxQkMsWUFBNUI7QUFDSDs7QUFqQm1DLEM7Ozs7Ozs7Ozs7OztBQ0Z4QztBQUFBO0FBQUE7QUFFZSw2RUFBY0osbURBQWQsQ0FDZjtBQUNJQyxTQUFPLEdBQUc7QUFDTndCLFNBQUssQ0FBQyxLQUFLOWQsR0FBTixFQUFXO0FBQUNELFlBQU0sRUFBRTtBQUFULEtBQVgsQ0FBTDtBQUNIOztBQUVELE1BQUlDLEdBQUosR0FBVTtBQUNOLFdBQU8sS0FBS2pDLE9BQUwsQ0FBYXllLE9BQWIsQ0FBcUJ4YyxHQUE1QjtBQUNIOztBQVBMLEM7Ozs7Ozs7Ozs7OztBQ0hBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdlLDZFQUFjcWMsbURBQWQsQ0FBeUI7QUFDcEMwQixZQUFVLEdBQUc7QUFDVCxTQUFLbkMsU0FBTCxHQUFpQixLQUFLN2QsT0FBTCxDQUFhd0MsWUFBYixDQUEwQixpQkFBMUIsQ0FBakI7QUFDSDs7QUFFRCtiLFNBQU8sR0FBRztBQUNOMEIsZ0dBQWEsQ0FDUnpPLE1BREwsQ0FDWSxLQUFLeFIsT0FEakIsRUFDMEI7QUFDbEI2TCxZQUFNLEVBQUUsR0FEVTtBQUVsQnFVLGFBQU8sRUFBRSxDQUNMQywrRUFESyxFQUNJQyxxRkFESixFQUVMQyxpRkFGSyxFQUVPQyxtRkFGUCxFQUVxQkMseUVBRnJCLEVBR0xDLG9GQUhLLEVBSUxDLHlFQUpLLEVBS0xDLDJFQUxLLEVBS0VDLGtGQUxGLEVBS2dCQyxtRkFMaEIsRUFLOEJDLGlGQUw5QixFQUswQ0Msa0ZBTDFDLEVBS3VEQyxrRkFMdkQsRUFLb0UxRSw4REFMcEUsRUFNTDJFLHVGQU5LLEVBT0xDLDRFQVBLLEVBUUxDLGtGQVJLLEVBU0xDLCtGQVRLLEVBVUxDLHVGQVZLLEVBV0xDLG9GQVhLLEVBV01DLDBFQVhOLEVBWUwzRSwwREFaSyxDQUZTO0FBZ0JsQjRFLGVBQVMsRUFBRTtBQUNQaGpCLGVBQU8sRUFBRSxDQUFFLE1BQUYsRUFBVSxPQUFWLEVBQW1CLFFBQW5CLEVBQTZCLFNBQTdCO0FBREYsT0FoQk87QUFtQmxCaWpCLGFBQU8sRUFBRSxDQUFDLFNBQUQsRUFBWSxHQUFaLEVBQWlCLE1BQWpCLEVBQXlCLFFBQXpCLEVBQW1DLFdBQW5DLEVBQWdELEdBQWhELEVBQXFELFVBQXJELEVBQWlFLFlBQWpFLEVBQStFLFdBQS9FLEVBQTRGLHFCQUE1RixFQUFtSCxHQUFuSCxFQUF3SCxNQUF4SCxFQUFnSSxjQUFoSSxFQUFnSixjQUFoSixFQUFnSyxZQUFoSyxFQUE4SyxhQUE5SyxFQUE2TCxhQUE3TCxFQUE0TSxZQUE1TSxFQUEwTixHQUExTixFQUErTixNQUEvTixFQUF1TyxNQUF2TyxFQUErTyxHQUEvTyxFQUFvUCxTQUFwUCxDQW5CUztBQW9CbEJDLGtCQUFZLEVBQUUsQ0FBQ0MsMkJBQUQsQ0FwQkk7QUFxQmxCQyxxQkFBZSxFQUFFLEtBQUs5RCxTQXJCSjtBQXNCbEIrRCxXQUFLLEVBQUU7QUFDSEosZUFBTyxFQUFFLENBQUMsc0JBQUQsRUFBeUIsR0FBekIsRUFBOEIsc0JBQTlCLEVBQXNELGlCQUF0RCxFQUF5RSx1QkFBekUsQ0FETjtBQUVIN1YsY0FBTSxFQUFFLENBQ0osTUFESSxFQUVKLE1BRkksRUFHSixXQUhJLEVBSUosYUFKSSxFQUtKLFlBTEk7QUFGTCxPQXRCVztBQWdDbEJrVyxnQkFBVSxFQUFFO0FBQ1JDLHNCQUFjLEVBQUU7QUFEUixPQWhDTTtBQW1DbEJDLFVBQUksRUFBRTtBQUNGQyxrQkFBVSxFQUFFO0FBQ1JDLG9CQUFVLEVBQUU7QUFDUkMsZ0JBQUksRUFBRSxRQURFO0FBRVJoWSxpQkFBSyxFQUFFLG1CQUZDO0FBR1JpWSxzQkFBVSxFQUFFO0FBQ1JDLG9CQUFNLEVBQUU7QUFEQTtBQUhKO0FBREo7QUFEVixPQW5DWTtBQThDbEJDLGdCQUFVLEVBQUU7QUFDUjlqQixlQUFPLEVBQUUsQ0FDTCxTQURLLEVBRUwsOEJBRkssRUFHTCxpQ0FISyxFQUlMLGdCQUpLLEVBS0wsZ0RBTEssRUFNTCw0QkFOSyxFQU9MLCtCQVBLLEVBUUwscUNBUkssRUFTTCw2QkFUSyxFQVVMLFFBVks7QUFERDtBQTlDTSxLQUQxQixFQThES3lmLElBOURMLENBOERVdkYsTUFBTSxJQUFJO0FBQ1oxWCxhQUFPLENBQUM0QixHQUFSLENBQVksb0JBQVo7QUFDSCxLQWhFTCxFQWlFSzJmLEtBakVMLENBaUVXdGhCLEtBQUssSUFBSTtBQUNaRCxhQUFPLENBQUNDLEtBQVIsQ0FBY0EsS0FBZDtBQUNILEtBbkVMO0FBb0VIOztBQTFFbUM7O0FBNkV4QyxTQUFTMGdCLDJCQUFULENBQXFDakosTUFBckMsRUFBNkM7QUFDekMsTUFBSW9GLFNBQVMsR0FBR3BGLE1BQU0sQ0FBQ2hhLE1BQVAsQ0FBY3FnQixHQUFkLENBQWtCLGlCQUFsQixDQUFoQjs7QUFFQXJHLFFBQU0sQ0FBQ3lILE9BQVAsQ0FBZXBCLEdBQWYsQ0FBbUIsZ0JBQW5CLEVBQXFDeUQsbUJBQXJDLEdBQTREM0UsTUFBRCxJQUFZO0FBQ25FLFdBQU8sSUFBSUQsNkRBQUosQ0FBd0JDLE1BQXhCLEVBQWdDQyxTQUFoQyxDQUFQO0FBQ0gsR0FGRDtBQUdILEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0dEO0FBRWUscUJBQWNTLG1EQUFkLENBQXlCO0FBR3BDa0UsTUFBSSxDQUFDamxCLEtBQUQsRUFBUTtBQUNSQSxTQUFLLENBQUNraUIsY0FBTjtBQUVBLFFBQUlnRCxTQUFTLEdBQUc1aUIsUUFBUSxDQUFDdVcsYUFBVCxDQUF1QixPQUF2QixDQUFoQjtBQUNBcU0sYUFBUyxDQUFDQyxLQUFWLENBQWdCQyxRQUFoQixHQUEyQixVQUEzQjtBQUNBRixhQUFTLENBQUNDLEtBQVYsQ0FBZ0JFLElBQWhCLEdBQXVCLFNBQXZCO0FBQ0FILGFBQVMsQ0FBQ0MsS0FBVixDQUFnQkcsR0FBaEIsR0FBc0IsU0FBdEI7QUFDQUosYUFBUyxDQUFDdGQsS0FBVixHQUFrQixLQUFLK0wsSUFBdkI7QUFFQXJSLFlBQVEsQ0FBQ2lqQixJQUFULENBQWNDLFdBQWQsQ0FBMEJOLFNBQTFCO0FBQ0FBLGFBQVMsQ0FBQ25MLE1BQVY7QUFFQXpYLFlBQVEsQ0FBQ21qQixXQUFULENBQXFCLE1BQXJCO0FBQ0FuakIsWUFBUSxDQUFDaWpCLElBQVQsQ0FBY0csV0FBZCxDQUEwQlIsU0FBMUI7QUFFQTVrQixTQUFLLENBQUMsWUFBWSxLQUFLcVQsSUFBakIsR0FBd0Isb0JBQXpCLEVBQStDLFNBQS9DLENBQUw7QUFDSDs7QUFFRCxNQUFJQSxJQUFKLEdBQVc7QUFDUCxXQUFPLEtBQUtnUyxZQUFMLENBQWtCL2QsS0FBekI7QUFDSDs7QUF2Qm1DOzttQ0FDbkIsQ0FBRSxRQUFGLEM7Ozs7Ozs7Ozs7OztBQ0hyQjtBQUFBO0FBQUE7QUFFZSw2RUFBY21aLG1EQUFkLENBQXlCO0FBQ3BDNkUsbUJBQWlCLENBQUM1bEIsS0FBRCxFQUFRO0FBQ3JCQSxTQUFLLENBQUNraUIsY0FBTjs7QUFFQSxRQUFJLENBQUMsS0FBS3pmLE9BQUwsQ0FBYVEsYUFBYixDQUEyQixNQUEzQixFQUFtQzRpQixhQUFuQyxFQUFMLEVBQXlEO0FBQ3JELFdBQUtwakIsT0FBTCxDQUFhUSxhQUFiLENBQTJCLE1BQTNCLEVBQW1DNmlCLGNBQW5DO0FBQ0E7QUFDSDs7QUFFRCxRQUFJQyxTQUFTLEdBQUcsS0FBSzlsQixJQUFMLENBQVVzaEIsR0FBVixDQUFjLFdBQWQsQ0FBaEI7QUFBQSxRQUNJN08sUUFBUSxHQUFHLElBQUlDLFFBQUosQ0FBYSxLQUFLbFEsT0FBTCxDQUFhUSxhQUFiLENBQTJCLE1BQTNCLENBQWIsQ0FEZjtBQUFBLFFBRUlzQixLQUFLLEdBQUcsSUFGWjs7QUFHQSxRQUFJeWhCLFlBQVksR0FBR2htQixLQUFLLENBQUM2a0IsTUFBekI7QUFDQW1CLGdCQUFZLENBQUM1RSxZQUFiLENBQTBCLFVBQTFCLEVBQXNDLElBQXRDO0FBRUFqZixLQUFDLENBQUNxQyxJQUFGLENBQU87QUFDSEUsU0FBRyxFQUFFcWhCLFNBREY7QUFFSDlsQixVQUFJLEVBQUV5UyxRQUZIO0FBR0hqTyxZQUFNLEVBQUUsTUFITDtBQUlIb08saUJBQVcsRUFBRSxLQUpWO0FBS0hDLGlCQUFXLEVBQUUsS0FMVjtBQU1Ibk8sYUFBTyxFQUFFLFVBQVVDLFFBQVYsRUFBb0I7QUFDekIsWUFBSUEsUUFBUSxDQUFDRCxPQUFiLEVBQXNCO0FBQ2xCLGNBQUlKLEtBQUssQ0FBQ0wsV0FBVixFQUF1QjtBQUNuQnhFLGtCQUFNLENBQUN3RixRQUFQLENBQWdCa0YsSUFBaEIsR0FBdUI3RixLQUFLLENBQUNMLFdBQTdCO0FBQ0gsV0FGRCxNQUVPO0FBQ0hLLGlCQUFLLENBQUM5QixPQUFOLENBQWNtZixTQUFkLEdBQTBCaGQsUUFBUSxDQUFDckIsT0FBbkM7QUFDSDtBQUNKLFNBTkQsTUFNTztBQUNIZ0IsZUFBSyxDQUFDOUIsT0FBTixDQUFjbWYsU0FBZCxHQUEwQmhkLFFBQVEsQ0FBQ3JCLE9BQW5DO0FBQ0g7QUFDSixPQWhCRTtBQWlCSEUsV0FBSyxFQUFFLFVBQVVBLEtBQVYsRUFBaUI7QUFDcEJ1aUIsb0JBQVksQ0FBQzNFLGVBQWIsQ0FBNkIsVUFBN0I7QUFDQTdkLGVBQU8sQ0FBQzRCLEdBQVIsQ0FBWTNCLEtBQVo7QUFDSDtBQXBCRSxLQUFQO0FBdUJIOztBQUVELE1BQUlTLFdBQUosR0FBa0I7QUFDZCxXQUFPLEtBQUtqRSxJQUFMLENBQVVzaEIsR0FBVixDQUFjLGFBQWQsQ0FBUDtBQUNIOztBQTFDbUMsQzs7Ozs7Ozs7Ozs7OztBQ0Z4QztBQUFBO0FBQUE7QUFFZSw2RUFBY1IsbURBQWQsQ0FBeUI7QUFDcENDLFNBQU8sR0FBRztBQUNOLFNBQUt2ZSxPQUFMLENBQWFFLGNBQWIsQ0FBNEI7QUFBQ3NqQixjQUFRLEVBQUUsUUFBWDtBQUFxQkMsV0FBSyxFQUFFO0FBQTVCLEtBQTVCO0FBQ0g7O0FBSG1DLEM7Ozs7Ozs7Ozs7OztBQ0Z4QztBQUFBO0FBQUE7QUFFZSw2RUFBY25GLG1EQUFkLENBQXlCO0FBQ3BDQyxTQUFPLEdBQUU7QUFDTHJoQixnQkFBWSxDQUFDUSxNQUFiLENBQW9CLDJCQUFwQixFQUFpREYsSUFBSSxJQUFJO0FBQ3JELFdBQUt3QyxPQUFMLENBQWFtZixTQUFiLEdBQXlCM2hCLElBQUksQ0FBQyxLQUFLa2hCLFlBQU4sQ0FBN0I7QUFDSCxLQUZEO0FBR0g7O0FBRUQsTUFBSUEsWUFBSixHQUFvQjtBQUNoQixXQUFPLEtBQUsxZSxPQUFMLENBQWF5ZSxPQUFiLENBQXFCQyxZQUE1QjtBQUNIOztBQVRtQyxDOzs7Ozs7Ozs7Ozs7QUNGeEM7QUFBQTtBQUFBO0FBRWUsNkVBQWNKLG1EQUFkLENBQXlCO0FBQ3BDQyxTQUFPLEdBQUU7QUFDTHJoQixnQkFBWSxDQUFDUSxNQUFiLENBQW9CLDJCQUFwQixFQUFpREYsSUFBSSxJQUFJO0FBQ3JELFdBQUt3QyxPQUFMLENBQWFtRixLQUFiLEdBQXFCM0gsSUFBSSxDQUFDLEtBQUtraEIsWUFBTixDQUF6QjtBQUNILEtBRkQ7QUFHSDs7QUFFRCxNQUFJQSxZQUFKLEdBQW9CO0FBQ2hCLFdBQU8sS0FBSzFlLE9BQUwsQ0FBYXllLE9BQWIsQ0FBcUJDLFlBQTVCO0FBQ0g7O0FBVG1DLEM7Ozs7Ozs7Ozs7OztBQ0Z4QztBQUFBO0FBQUE7QUFFZSw2RUFBY0osbURBQWQsQ0FBeUI7QUFDcENDLFNBQU8sR0FBRTtBQUNMLFNBQUttRixZQUFMLEdBQW9CLEtBQUsxakIsT0FBTCxDQUFhRixnQkFBYixDQUE4QixPQUE5QixDQUFwQjtBQUVBLFNBQUs0akIsWUFBTCxDQUFrQjNqQixPQUFsQixDQUEwQkMsT0FBTyxJQUFJO0FBQ2pDQSxhQUFPLENBQUNDLGdCQUFSLENBQXlCLFFBQXpCLEVBQW1DLEtBQUtzWSxNQUF4QztBQUNILEtBRkQ7QUFHSDs7QUFFREEsUUFBTSxHQUFHO0FBQ0xyYixnQkFBWSxDQUFDSSxJQUFiLENBQWtCLHNDQUFsQixFQUEwRDtBQUN0RHFtQixZQUFNLEVBQUUsS0FBS3hlLEtBQUwsS0FBZTtBQUQrQixLQUExRDtBQUdIOztBQWJtQyxDOzs7Ozs7Ozs7Ozs7QUNGeEM7QUFBQTtBQUFBO0FBRWUsNkVBQWNtWixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFFO0FBQ0wsUUFBSSxLQUFLcUYsZUFBVCxFQUEwQjtBQUN0QixXQUFLNWpCLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJFLEdBQXZCLENBQTJCLFFBQTNCO0FBQ0gsS0FGRCxNQUVPO0FBQ0gsV0FBS3hLLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJDLE1BQXZCLENBQThCLFFBQTlCO0FBQ0g7O0FBRURyTixnQkFBWSxDQUFDUSxNQUFiLENBQW9CLHNDQUFwQixFQUE2REYsSUFBRCxJQUFVO0FBQ2xFLFVBQUlBLElBQUksQ0FBQ21tQixNQUFULEVBQWlCO0FBQ2IsYUFBSzNqQixPQUFMLENBQWFzSyxTQUFiLENBQXVCRSxHQUF2QixDQUEyQixRQUEzQjtBQUNILE9BRkQsTUFFTztBQUNILGFBQUt4SyxPQUFMLENBQWFzSyxTQUFiLENBQXVCQyxNQUF2QixDQUE4QixRQUE5QjtBQUNIO0FBQ0osS0FORDtBQU9IOztBQUVELE1BQUlxWixlQUFKLEdBQXNCO0FBQ2xCLFdBQU8sS0FBSzVqQixPQUFMLENBQWF5ZSxPQUFiLENBQXFCa0YsTUFBckIsS0FBZ0MsR0FBdkM7QUFDSDs7QUFuQm1DLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0Z4QztBQUVlLHFCQUFjckYsbURBQWQsQ0FBeUI7QUFHcENDLFNBQU8sR0FBRztBQUNOLFFBQUlqWixLQUFLLEdBQUcsS0FBSzlILElBQUwsQ0FBVXNoQixHQUFWLENBQWMsT0FBZCxDQUFaOztBQUNBLFFBQUd4WixLQUFILEVBQVM7QUFDTCxVQUFJdWUsVUFBVSxHQUFHNW1CLE1BQU0sQ0FBQ3dGLFFBQVAsQ0FBZ0JrRixJQUFqQztBQUFBLFVBQ0lpRixLQUFLLEdBQUdpWCxVQUFVLENBQUM3WixLQUFYLENBQWlCLEdBQWpCLENBRFo7QUFBQSxVQUVJOFosUUFBUSxHQUFHLElBRmY7O0FBSUEsVUFBSWxYLEtBQUssQ0FBQ3hGLE1BQU4sR0FBZSxDQUFuQixFQUFzQjtBQUNsQjBjLGdCQUFRLEdBQUdDLFNBQVMsQ0FBQ25YLEtBQUssQ0FBQ0EsS0FBSyxDQUFDeEYsTUFBTixHQUFlLENBQWhCLENBQU4sQ0FBVCxDQUFtQzRjLFdBQW5DLEdBQWlEdGhCLE9BQWpELENBQXlELElBQXpELEVBQStELEdBQS9ELENBQVg7O0FBQ0EsWUFBR29oQixRQUFRLEtBQUt4ZSxLQUFLLENBQUMyZSxpQkFBTixHQUEwQnZoQixPQUExQixDQUFrQyxJQUFsQyxFQUF3QyxHQUF4QyxDQUFoQixFQUE2RDtBQUN6RCxlQUFLd2hCLGFBQUw7QUFDQSxlQUFLbGtCLE9BQUwsQ0FBYUUsY0FBYixDQUE0QjtBQUFDc2pCLG9CQUFRLEVBQUUsUUFBWDtBQUFxQkMsaUJBQUssRUFBRTtBQUE1QixXQUE1QjtBQUNIO0FBQ0o7QUFDSjtBQUNKOztBQUVEUyxlQUFhLEdBQUc7QUFDWixTQUFLQyxXQUFMLENBQWlCN1osU0FBakIsQ0FBMkJpTyxNQUEzQixDQUFrQyxnQkFBbEM7QUFDQSxTQUFLNEwsV0FBTCxDQUFpQjdaLFNBQWpCLENBQTJCaU8sTUFBM0IsQ0FBa0MsZUFBbEM7QUFFQSxTQUFLNkwsYUFBTCxDQUFtQjlaLFNBQW5CLENBQTZCaU8sTUFBN0IsQ0FBb0MsYUFBcEM7QUFDSDs7QUF6Qm1DOzttQ0FDbkIsQ0FBQyxPQUFELEVBQVUsU0FBVixDOzs7Ozs7Ozs7Ozs7QUNIckI7QUFBQTtBQUFBO0FBRWUsNkVBQWMrRixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFFO0FBQ0xyaEIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQixzQkFBcEIsRUFBNENGLElBQUksSUFBSTtBQUNoRCxVQUFJQSxJQUFJLENBQUNraEIsWUFBTCxLQUFzQixLQUFLQSxZQUEvQixFQUE2QztBQUN6QyxZQUFJbGhCLElBQUksQ0FBQzJILEtBQUwsS0FBZSxxQkFBbkIsRUFBMEM7QUFDdEMsZUFBS25GLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJDLE1BQXZCLENBQThCLFFBQTlCO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsZUFBS3ZLLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJFLEdBQXZCLENBQTJCLFFBQTNCO0FBQ0g7QUFDSjtBQUNKLEtBUkQ7QUFTSDs7QUFFRCxNQUFJa1UsWUFBSixHQUFtQjtBQUNmLFdBQU8sS0FBSzFlLE9BQUwsQ0FBYXllLE9BQWIsQ0FBcUJDLFlBQTVCO0FBQ0g7O0FBZm1DLEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRnhDO0FBRWUscUJBQWNKLG1EQUFkLENBQXlCO0FBR3BDQyxTQUFPLEdBQUc7QUFDTixRQUFJLEtBQUsvZ0IsSUFBTCxDQUFVeWhCLEdBQVYsQ0FBYyxNQUFkLENBQUosRUFBMkI7QUFDdkIsV0FBS29GLFlBQUw7QUFFQSxXQUFLcmtCLE9BQUwsQ0FBYUUsY0FBYixDQUE0QjtBQUFDc2pCLGdCQUFRLEVBQUUsUUFBWDtBQUFxQkMsYUFBSyxFQUFFO0FBQTVCLE9BQTVCO0FBQ0g7QUFDSjs7QUFFRFksY0FBWSxHQUFHO0FBQ1gsU0FBS0YsV0FBTCxDQUFpQjdaLFNBQWpCLENBQTJCaU8sTUFBM0IsQ0FBa0MsZ0JBQWxDO0FBQ0EsU0FBSzRMLFdBQUwsQ0FBaUI3WixTQUFqQixDQUEyQmlPLE1BQTNCLENBQWtDLGVBQWxDO0FBRUEsU0FBSytMLFlBQUwsQ0FBa0JoYSxTQUFsQixDQUE0QmlPLE1BQTVCLENBQW1DLGFBQW5DO0FBQ0g7O0FBaEJtQzs7bUNBQ25CLENBQUMsT0FBRCxFQUFVLFFBQVYsQzs7Ozs7Ozs7Ozs7O0FDSHJCO0FBQUE7QUFBQTtBQUVlLDZFQUFjK0YsbURBQWQsQ0FBeUI7QUFDcENpRyx5QkFBdUIsR0FBRztBQUN0QixTQUFLdmtCLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJpTyxNQUF2QixDQUE4QixVQUE5QjtBQUNIOztBQUhtQyxDOzs7Ozs7Ozs7Ozs7QUNGeEM7QUFBQTtBQUFBO0FBRWUsNkVBQWMrRixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFFO0FBQ0xyaEIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQixzQkFBcEIsRUFBNENGLElBQUksSUFBSTtBQUNoRCxVQUFJQSxJQUFJLENBQUNraEIsWUFBTCxLQUFzQixLQUFLQSxZQUEvQixFQUE2QztBQUN6QyxZQUFJbGhCLElBQUksQ0FBQzJILEtBQUwsS0FBZSxzQ0FBbkIsRUFBMkQ7QUFBRTtBQUN6RCxlQUFLbkYsT0FBTCxDQUFhc0ssU0FBYixDQUF1QkMsTUFBdkIsQ0FBOEIsUUFBOUI7QUFDSCxTQUZELE1BRU87QUFDSCxlQUFLdkssT0FBTCxDQUFhc0ssU0FBYixDQUF1QkUsR0FBdkIsQ0FBMkIsUUFBM0I7QUFDSDtBQUNKO0FBQ0osS0FSRDtBQVNIOztBQUVELE1BQUlrVSxZQUFKLEdBQW1CO0FBQ2YsV0FBTyxLQUFLMWUsT0FBTCxDQUFheWUsT0FBYixDQUFxQkMsWUFBNUI7QUFDSDs7QUFmbUMsQzs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQUE7QUFBQTtBQUVlLDZFQUFjSixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFHO0FBQ04sU0FBS25mLFdBQUwsR0FBbUIsS0FBS1ksT0FBTCxDQUFhd0MsWUFBYixDQUEwQixJQUExQixDQUFuQjtBQUVBLFNBQUt4QyxPQUFMLENBQWFDLGdCQUFiLENBQThCLE9BQTlCLEVBQXVDLEtBQUt1a0IsZ0JBQTVDO0FBRUF0bkIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQiw0QkFBcEIsRUFBa0R3YSxPQUFPLElBQUk7QUFDekQsVUFBSUEsT0FBTyxDQUFDOVksV0FBUixLQUF3QixLQUFLQSxXQUFqQyxFQUE4QztBQUMxQyxhQUFLWSxPQUFMLENBQWFtRixLQUFiLEdBQXFCK1MsT0FBTyxDQUFDeEksSUFBN0I7QUFDSDtBQUNKLEtBSkQ7QUFLSDs7QUFFRDhVLGtCQUFnQixHQUFHO0FBQ2Z0bkIsZ0JBQVksQ0FBQ0ksSUFBYixDQUFrQixlQUFsQixFQUFtQztBQUFDZ0UsVUFBSSxFQUFFLGVBQVA7QUFBd0I5QixRQUFFLEVBQUUsS0FBS0E7QUFBakMsS0FBbkM7QUFDSDs7QUFmbUMsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGeEM7QUFFZSxxQkFBYzhlLG1EQUFkLENBQXlCO0FBR3BDQyxTQUFPLEdBQUc7QUFDTixRQUFHLEtBQUsvZ0IsSUFBTCxDQUFVc2hCLEdBQVYsQ0FBYyxVQUFkLENBQUgsRUFBNkI7QUFDekIsV0FBSzJGLFVBQUw7QUFDQSxXQUFLemtCLE9BQUwsQ0FBYUUsY0FBYixDQUE0QjtBQUFDc2pCLGdCQUFRLEVBQUUsUUFBWDtBQUFxQkMsYUFBSyxFQUFFO0FBQTVCLE9BQTVCO0FBQ0gsS0FIRCxNQUdLO0FBQ0QsV0FBS2lCLGdCQUFMLENBQXNCdkYsU0FBdEIsR0FBa0MsS0FBSzNoQixJQUFMLENBQVVzaEIsR0FBVixDQUFjLFVBQWQsQ0FBbEM7QUFDSDtBQUNKOztBQUVEMkYsWUFBVSxHQUFHO0FBQ1QsU0FBS04sV0FBTCxDQUFpQjdaLFNBQWpCLENBQTJCaU8sTUFBM0IsQ0FBa0MsZUFBbEM7QUFDQSxTQUFLNEwsV0FBTCxDQUFpQjdaLFNBQWpCLENBQTJCaU8sTUFBM0IsQ0FBa0MsYUFBbEM7QUFFQSxTQUFLb00sY0FBTCxDQUFvQnJhLFNBQXBCLENBQThCaU8sTUFBOUIsQ0FBcUMsYUFBckM7QUFFQSxTQUFLbU0sZ0JBQUwsQ0FBc0J2RixTQUF0QixHQUFrQyxLQUFLeUYsWUFBdkM7QUFDSDs7QUFFRCxNQUFJMVEsTUFBSixHQUFhO0FBQ1QsV0FBTyxLQUFLaVEsV0FBTCxDQUFpQjdaLFNBQWpCLENBQTJCdWEsUUFBM0IsQ0FBb0MsYUFBcEMsQ0FBUDtBQUNIOztBQUVELE1BQUlELFlBQUosR0FBbUI7QUFDZixXQUFPLEtBQUsxUSxNQUFMLEdBQWMsS0FBSzFXLElBQUwsQ0FBVXNoQixHQUFWLENBQWMsUUFBZCxDQUFkLEdBQXdDLEtBQUt0aEIsSUFBTCxDQUFVc2hCLEdBQVYsQ0FBYyxVQUFkLENBQS9DO0FBQ0g7O0FBM0JtQzs7bUNBQ25CLENBQUMsVUFBRCxFQUFhLFlBQWIsRUFBMkIsT0FBM0IsQzs7Ozs7Ozs7Ozs7O0FDSHJCO0FBQUE7QUFBQTtBQUVlLDZFQUFjUixtREFBZCxDQUF5QjtBQUNwQ3dHLFFBQU0sQ0FBQ3ZuQixLQUFELEVBQVE7QUFDVixRQUFJMEUsR0FBRyxHQUFJLG9CQUFtQixLQUFLQSxHQUFJLEVBQXZDO0FBRUExRSxTQUFLLENBQUNraUIsY0FBTjtBQUVBL2YsS0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLFlBQU0sRUFBRSxNQURMO0FBRUhDLFNBQUcsRUFBRUEsR0FGRjs7QUFHSEMsYUFBTyxHQUFHO0FBQ05qRixjQUFNLENBQUN3RixRQUFQLENBQWdCa0YsSUFBaEIsR0FBdUIxSyxNQUFNLENBQUN3RixRQUFQLENBQWdCa0YsSUFBdkM7QUFDSCxPQUxFOztBQU1IM0csV0FBSyxDQUFDQSxLQUFELEVBQVE7QUFDVEQsZUFBTyxDQUFDNEIsR0FBUixDQUFZM0IsS0FBWjtBQUNIOztBQVJFLEtBQVA7QUFVSDs7QUFFRCxNQUFJaUIsR0FBSixHQUFVO0FBQ04sV0FBTyxLQUFLekUsSUFBTCxDQUFVc2hCLEdBQVYsQ0FBYyxRQUFkLENBQVA7QUFDSDs7QUFwQm1DLEM7Ozs7Ozs7Ozs7Ozs7QUNGeEM7QUFBQTtBQUFBO0FBRWUsNkVBQWNSLG1EQUFkLENBQXlCO0FBQ3BDQyxTQUFPLEdBQUc7QUFDTixTQUFLdmUsT0FBTCxDQUFhQyxnQkFBYixDQUE4QixRQUE5QixFQUF3QyxNQUFNO0FBQzFDL0Msa0JBQVksQ0FBQ0ksSUFBYixDQUFrQixzQkFBbEIsRUFBMEM7QUFDdENvaEIsb0JBQVksRUFBRSxLQUFLQSxZQURtQjtBQUV0Q3ZaLGFBQUssRUFBRSxLQUFLbkYsT0FBTCxDQUFhbUY7QUFGa0IsT0FBMUM7QUFJSCxLQUxEO0FBT0F6RixLQUFDLENBQUNHLFFBQUQsQ0FBRCxDQUFZZ2dCLEtBQVosQ0FBa0IsTUFBTTtBQUNwQjNpQixrQkFBWSxDQUFDSSxJQUFiLENBQWtCLHNCQUFsQixFQUEwQztBQUN0Q29oQixvQkFBWSxFQUFFLEtBQUtBLFlBRG1CO0FBRXRDdlosYUFBSyxFQUFFLEtBQUtuRixPQUFMLENBQWFtRjtBQUZrQixPQUExQztBQUlILEtBTEQ7QUFNSDs7QUFFRCxNQUFJdVosWUFBSixHQUFtQjtBQUNmLFdBQU8sS0FBSzFlLE9BQUwsQ0FBYXllLE9BQWIsQ0FBcUJDLFlBQTVCO0FBQ0g7O0FBbkJtQyxDOzs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQUE7QUFBQTtBQUVlLDZFQUFjSixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFHO0FBQ05yaEIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQiwyQkFBcEIsRUFBaUQsTUFBTTtBQUNuRCxXQUFLc0MsT0FBTCxDQUFhbWYsU0FBYixHQUF5QixLQUFLNEYsS0FBTCxHQUFhLENBQXRDOztBQUVBLFVBQUksS0FBS0EsS0FBTCxLQUFlLENBQW5CLEVBQXNCO0FBQ2xCLGFBQUsva0IsT0FBTCxDQUFhc0ssU0FBYixDQUF1QkUsR0FBdkIsQ0FBMkIsUUFBM0I7QUFDSDtBQUNKLEtBTkQ7QUFPSDs7QUFFRCxNQUFJdWEsS0FBSixHQUFZO0FBQ1IsV0FBT3ZSLFFBQVEsQ0FBQyxLQUFLeFQsT0FBTCxDQUFhbWYsU0FBZCxDQUFmO0FBQ0g7O0FBYm1DLEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRnhDO0FBRWUscUJBQWNiLG1EQUFkLENBQXlCO0FBR3BDNEYsZUFBYSxHQUFHO0FBQ1osU0FBS0MsV0FBTCxDQUFpQjdaLFNBQWpCLENBQTJCaU8sTUFBM0IsQ0FBa0MsZ0JBQWxDO0FBQ0EsU0FBSzRMLFdBQUwsQ0FBaUI3WixTQUFqQixDQUEyQmlPLE1BQTNCLENBQWtDLGVBQWxDO0FBRUEsU0FBSzZMLGFBQUwsQ0FBbUI5WixTQUFuQixDQUE2QmlPLE1BQTdCLENBQW9DLGFBQXBDOztBQUVBLFFBQUksS0FBS3lNLFFBQVQsRUFBbUI7QUFDZixVQUFLbGpCLEtBQUssR0FBRyxJQUFiOztBQUVBLFdBQUttakIsV0FBTCxDQUFpQjNhLFNBQWpCLENBQTJCQyxNQUEzQixDQUFrQyxhQUFsQztBQUVBN0ssT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLGNBQU0sRUFBRSxNQURMO0FBRUhDLFdBQUcsRUFBRSxLQUFLQSxHQUZQOztBQUdIQyxlQUFPLEdBQUc7QUFDTmhGLHNCQUFZLENBQUNJLElBQWIsQ0FBa0IsMkJBQWxCO0FBQ0gsU0FMRTs7QUFNSDBELGFBQUssQ0FBQ0EsS0FBRCxFQUFRO0FBQ1RELGlCQUFPLENBQUM0QixHQUFSLENBQVkzQixLQUFLLENBQUMsY0FBRCxDQUFqQjs7QUFDQWMsZUFBSyxDQUFDbWpCLFdBQU4sQ0FBa0IzYSxTQUFsQixDQUE0QkUsR0FBNUIsQ0FBZ0MsYUFBaEM7QUFDSDs7QUFURSxPQUFQO0FBV0g7QUFDSjs7QUFFRCxNQUFJd2EsUUFBSixHQUFlO0FBQ1gsV0FBTyxLQUFLQyxXQUFMLENBQWlCM2EsU0FBakIsQ0FBMkJ1YSxRQUEzQixDQUFvQyxhQUFwQyxDQUFQO0FBQ0g7O0FBRUQsTUFBSTVpQixHQUFKLEdBQVU7QUFDTixXQUFPLEtBQUtqQyxPQUFMLENBQWF5ZSxPQUFiLENBQXFCeGMsR0FBNUI7QUFDSDs7QUFsQ21DOzttQ0FDbkIsQ0FBQyxPQUFELEVBQVUsU0FBVixFQUFxQixPQUFyQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNIckI7QUFFZSw2RUFBY3FjLG1EQUFkLENBQXlCO0FBQ3BDQyxTQUFPLEdBQUc7QUFDTnJoQixnQkFBWSxDQUFDUSxNQUFiLENBQW9CLDJCQUFwQixFQUFpREYsSUFBSSxJQUFJO0FBQ3JELFVBQUlQLE1BQU0sQ0FBQ3dGLFFBQVAsQ0FBZ0JrRixJQUFoQixDQUFxQnFMLE9BQXJCLENBQTZCLGNBQTdCLEtBQWdELENBQUMsQ0FBckQsRUFBd0Q7QUFDcEQ7QUFDSDs7QUFFRCxVQUFJLENBQUMvUSxHQUFELElBQVMsS0FBS2lqQixZQUFMLENBQWtCbGIsS0FBbEIsQ0FBd0IsR0FBeEIsQ0FBYjtBQUFBLFVBQ0ksQ0FBQ3VNLE1BQUQsRUFBU3hDLEtBQVQsRUFBZ0IzUSxNQUFoQixFQUF3QitoQixhQUF4QixFQUF1Q0MsV0FBdkMsSUFBc0RuakIsR0FBRyxDQUFDK0gsS0FBSixDQUFVLEdBQVYsQ0FEMUQ7QUFHQSxVQUFJcWIscUJBQXFCLEdBQUc7QUFBQzNoQixjQUFNLEVBQUVsRyxJQUFJLENBQUNrRyxNQUFkO0FBQXNCbUMsY0FBTSxFQUFFckksSUFBSSxDQUFDcUksTUFBbkM7QUFBMkNsQyxlQUFPLEVBQUVuRyxJQUFJLENBQUNtRztBQUF6RCxPQUE1QjtBQUFBLFVBQ0kyaEIsV0FBVyxHQUFHNWxCLENBQUMsQ0FBQ3VGLEtBQUYsQ0FBUW9nQixxQkFBUixDQURsQjtBQUFBLFVBRUlFLE1BQU0sR0FBSSxJQUFHeFIsS0FBTSxJQUFHdlcsSUFBSSxDQUFDK0gsSUFBSyxJQUFHNGYsYUFBYyxJQUFHQyxXQUFZLElBQUdFLFdBQVksRUFGbkY7QUFJQSxXQUFLdGxCLE9BQUwsQ0FBYTJlLFlBQWIsQ0FBMEIsUUFBMUIsRUFBb0M0RyxNQUFwQztBQUNILEtBYkQ7QUFjSDs7QUFFRCxNQUFJTCxZQUFKLEdBQW1CO0FBQ2YsV0FBTyxLQUFLbGxCLE9BQUwsQ0FBYXdDLFlBQWIsQ0FBMEIsUUFBMUIsQ0FBUDtBQUNIOztBQXBCbUMsQzs7Ozs7Ozs7Ozs7OztBQ0Z4QztBQUFBO0FBQUE7QUFFZSw2RUFBYzhiLG1EQUFkLENBQXlCO0FBQ3BDa0gsVUFBUSxHQUFHO0FBQ1B2b0IsVUFBTSxDQUFDd0YsUUFBUCxDQUFnQmtGLElBQWhCLEdBQXVCLEtBQUszSCxPQUFMLENBQWFtRixLQUFwQztBQUNIOztBQUhtQyxDOzs7Ozs7Ozs7Ozs7QUNGeEM7QUFBQTtBQUFBO0FBRWUsNkVBQWNtWixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFFO0FBQ0xyaEIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQixzQkFBcEIsRUFBNENGLElBQUksSUFBSTtBQUNoRCxVQUFJQSxJQUFJLENBQUNraEIsWUFBTCxLQUFzQixLQUFLQSxZQUEvQixFQUE2QztBQUN6QyxZQUFJbGhCLElBQUksQ0FBQzJILEtBQUwsS0FBZSxLQUFLc2dCLGFBQXhCLEVBQXVDO0FBQ25DLGVBQUt6bEIsT0FBTCxDQUFhc0ssU0FBYixDQUF1QkMsTUFBdkIsQ0FBOEIsUUFBOUI7QUFDSCxTQUZELE1BRU87QUFDSCxlQUFLdkssT0FBTCxDQUFhc0ssU0FBYixDQUF1QkUsR0FBdkIsQ0FBMkIsUUFBM0I7QUFDSDtBQUNKO0FBQ0osS0FSRDtBQVNIOztBQUVELE1BQUlrVSxZQUFKLEdBQW1CO0FBQ2YsV0FBTyxLQUFLMWUsT0FBTCxDQUFheWUsT0FBYixDQUFxQkMsWUFBNUI7QUFDSDs7QUFFRCxNQUFJK0csYUFBSixHQUFvQjtBQUNoQixXQUFPLEtBQUt6bEIsT0FBTCxDQUFheWUsT0FBYixDQUFxQmdILGFBQTVCO0FBQ0g7O0FBbkJtQyxDOzs7Ozs7Ozs7Ozs7QUNGeEM7QUFBQTtBQUFBO0FBRWUsNkVBQWNuSCxtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFFO0FBQ0xyaEIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQiwyQkFBcEIsRUFBaURGLElBQUksSUFBSTtBQUVyRCxVQUFJQSxJQUFJLENBQUMrSCxJQUFMLEtBQWMsV0FBbEIsRUFBK0I7QUFDM0IsWUFBSW1nQixRQUFRLEdBQUcsS0FBSzFsQixPQUFMLENBQWFRLGFBQWIsQ0FBMkIsR0FBM0IsQ0FBZjs7QUFFQSxZQUFJa2xCLFFBQVEsS0FBSyxJQUFqQixFQUF1QjtBQUNuQjtBQUNIOztBQUVEQSxnQkFBUSxHQUFHN2xCLFFBQVEsQ0FBQ3VXLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBWDtBQUVBc1AsZ0JBQVEsQ0FBQ0MsU0FBVCxHQUFxQiw0QkFBckI7QUFFQSxhQUFLM2xCLE9BQUwsQ0FBYTRsQixPQUFiLENBQXFCRixRQUFyQjtBQUNILE9BWkQsTUFZTztBQUNILFlBQUlBLFFBQVEsR0FBRyxLQUFLMWxCLE9BQUwsQ0FBYVEsYUFBYixDQUEyQixHQUEzQixDQUFmOztBQUVBLFlBQUlrbEIsUUFBSixFQUFjO0FBQ1YsZUFBSzFsQixPQUFMLENBQWFpakIsV0FBYixDQUF5QnlDLFFBQXpCO0FBQ0g7QUFDSjtBQUNKLEtBckJEO0FBc0JIOztBQXhCbUMsQzs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQUE7QUFBQTtBQUVlLDZFQUFjcEgsbURBQWQsQ0FBeUI7QUFDcEMwQixZQUFVLEdBQUc7QUFDVCxTQUFLNkYsY0FBTDtBQUNIOztBQUNEQyxTQUFPLEdBQUc7QUFDUCxTQUFLRCxjQUFMO0FBQ0Y7O0FBRURBLGdCQUFjLEdBQUc7QUFDYixRQUFHLENBQUMsS0FBSzdsQixPQUFMLENBQWFtRixLQUFiLENBQW1CNGdCLFVBQW5CLENBQThCLEtBQTlCLENBQUosRUFBMkM7QUFDdkMsV0FBSy9sQixPQUFMLENBQWFtRixLQUFiLEdBQXFCLEtBQXJCO0FBQ0g7QUFDSjs7QUFabUMsQzs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0NBQ3dEOztBQUN4RDtBQUVlLDZFQUFjbVosbURBQWQsQ0FBeUI7QUFDcENDLFNBQU8sR0FBRztBQUNOLFVBQU15SCxLQUFLLEdBQUdDLHNEQUFLLENBQUN6VSxNQUFOLENBQWE7QUFDdkI1UyxRQUFFLEVBQUUsS0FBS29CLE9BRGM7QUFFdkJrbUIsV0FBSyxFQUFFLFNBRmdCO0FBR3ZCQyxpQkFBVyxFQUFFLElBSFU7QUFJdkJsakIsYUFBTyxFQUFFLEtBQUtqRCxPQUFMLENBQWFtRixLQUpDO0FBS3ZCaWhCLGNBQVEsRUFBRSxDQUNOLHNCQURNLEVBRU4seUJBRk0sRUFHTix5QkFITSxFQUlOLDBCQUpNLEVBS04sd0JBTE0sRUFNTiwwQkFOTSxFQU9OLHdCQVBNLEVBUU4sd0JBUk0sRUFTTix5QkFUTSxFQVVOLHdCQVZNLEVBV04sMEJBWE0sRUFZTix5QkFaTSxFQWFOLDBCQWJNLEVBY04sc0JBZE0sQ0FMYTtBQXNCdkJDLGdCQUFVLEVBQUU7QUFDUkMsZUFBTyxFQUFFLElBREQ7QUFFUkMsZUFBTyxFQUFFLElBRkQ7QUFHUkMsV0FBRyxFQUFFLElBSEc7QUFJUkMsbUJBQVcsRUFBRTtBQUNUQyxhQUFHLEVBQUUsSUFESTtBQUVUQyxjQUFJLEVBQUUsSUFGRztBQUdUQyxjQUFJLEVBQUUsSUFIRztBQUlUQyxjQUFJLEVBQUUsSUFKRztBQUtUQyxjQUFJLEVBQUUsSUFMRztBQU1UMWMsZUFBSyxFQUFFLElBTkU7QUFPVCtOLGVBQUssRUFBRSxJQVBFO0FBUVQ3RixjQUFJLEVBQUU7QUFSRztBQUpMO0FBdEJXLEtBQWIsQ0FBZDtBQXVDQTBULFNBQUssQ0FBQzlXLEVBQU4sQ0FBUyxNQUFULEVBQWlCLENBQUNnSCxLQUFELEVBQVE2USxRQUFSLEtBQXFCO0FBQ2xDLFVBQUk3USxLQUFKLEVBQVc7QUFDUCxhQUFLbFcsT0FBTCxDQUFhbUYsS0FBYixHQUFxQitRLEtBQUssQ0FBQzhRLE1BQU4sRUFBckI7QUFDSCxPQUZELE1BRU87QUFDSCxhQUFLaG5CLE9BQUwsQ0FBYW1GLEtBQWIsR0FBcUIsRUFBckI7QUFDSDtBQUNKLEtBTkQ7QUFPSDs7QUFoRG1DLEM7Ozs7Ozs7Ozs7OztBQ0p4QztBQUFBO0FBQUE7QUFFZSw2RUFBY21aLG1EQUFkLENBQXlCO0FBQ3BDQyxTQUFPLEdBQUU7QUFDTDdlLEtBQUMsQ0FBQ0csUUFBRCxDQUFELENBQVlnZ0IsS0FBWixDQUFrQixNQUFNO0FBQ3BCM2lCLGtCQUFZLENBQUNJLElBQWIsQ0FBa0IsYUFBbEIsRUFBaUM7QUFDN0I2SCxhQUFLLEVBQUUsS0FBSzhoQixHQUFMLENBQVMsS0FBS2puQixPQUFMLENBQWFtRixLQUFiLENBQW1CK2hCLFNBQW5CLENBQTZCLENBQTdCLEVBQWdDLENBQWhDLENBQVQ7QUFEc0IsT0FBakM7QUFHSCxLQUpEO0FBS0g7O0FBRURqZ0IsUUFBTSxHQUFHO0FBQ0wsUUFBSSxLQUFLakgsT0FBTCxDQUFhbUYsS0FBYixDQUFtQmlDLE1BQW5CLEdBQTRCLENBQWhDLEVBQW1DO0FBQy9CO0FBQ0g7O0FBRURsSyxnQkFBWSxDQUFDSSxJQUFiLENBQWtCLGFBQWxCLEVBQWlDO0FBQzdCNkgsV0FBSyxFQUFFLEtBQUs4aEIsR0FBTCxDQUFTLEtBQUtqbkIsT0FBTCxDQUFhbUYsS0FBYixDQUFtQitoQixTQUFuQixDQUE2QixDQUE3QixFQUFnQyxDQUFoQyxDQUFUO0FBRHNCLEtBQWpDO0FBR0g7O0FBRURELEtBQUcsQ0FBQ0UsSUFBRCxFQUFPO0FBQ04sUUFBSUEsSUFBSSxDQUFDL2YsTUFBTCxHQUFjLENBQWxCLEVBQXFCO0FBQ2pCLGFBQU8sQ0FBUDtBQUNIOztBQUVELFFBQUlnZ0IsUUFBUSxHQUFHLElBQUlDLElBQUosQ0FBUyxJQUFULEVBQWUsQ0FBZixFQUFrQixDQUFsQixDQUFmO0FBQ0EsUUFBSUMsV0FBVyxHQUFHLElBQUlELElBQUosQ0FBU0QsUUFBUSxDQUFDRyxPQUFULEVBQVQsQ0FBbEI7QUFDQSxRQUFJQyxLQUFLLEdBQUcsSUFBSUgsSUFBSixFQUFaO0FBRUFDLGVBQVcsQ0FBQ0csT0FBWixDQUFvQkgsV0FBVyxDQUFDSSxPQUFaLEtBQXdCbFUsUUFBUSxDQUFDMlQsSUFBRCxDQUFwRDtBQUVBLFdBQU92ZixJQUFJLENBQUMrZixLQUFMLENBQVcsQ0FBQ0gsS0FBSyxHQUFHRixXQUFULEtBQXlCLE9BQU8sRUFBUCxHQUFZLEVBQVosR0FBaUIsRUFBakIsR0FBc0IsR0FBL0MsQ0FBWCxDQUFQO0FBQ0g7O0FBL0JtQyxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQ0E7QUFFZSw2RUFBY2hKLG1EQUFkLENBQXlCO0FBQ3BDbmUsUUFBTSxHQUFHO0FBQ0wsUUFBSU4sUUFBUSxDQUFDK25CLEtBQVQsQ0FBZWxYLElBQWYsQ0FBb0IwUyxhQUFwQixPQUF3QyxLQUE1QyxFQUFtRDtBQUMvQztBQUNIOztBQUVELFFBQUloUSxjQUFjLEdBQUdDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixpQkFBckIsS0FBMkMsRUFBaEU7QUFBQSxRQUNJN0MsTUFBTSxHQUFHLEtBQUtqVCxJQUFMLENBQVVzaEIsR0FBVixDQUFjLFFBQWQsQ0FEYjtBQUdBMUwsa0JBQWMsR0FBR0EsY0FBYyxDQUFDcEosS0FBZixDQUFxQixHQUFyQixDQUFqQjtBQUNBb0osa0JBQWMsQ0FBQ2pVLElBQWYsQ0FBb0JzUixNQUFwQjtBQUNBMkMsa0JBQWMsR0FBRyxJQUFJeVUsR0FBSixDQUFRelUsY0FBUixDQUFqQjtBQUNBQSxrQkFBYyxHQUFHdkksS0FBSyxDQUFDakUsSUFBTixDQUFXd00sY0FBWCxDQUFqQjtBQUVBQyxnQkFBWSxDQUFDTSxPQUFiLENBQXFCLGlCQUFyQixFQUF3Q1AsY0FBYyxDQUFDUSxJQUFmLENBQW9CLEdBQXBCLENBQXhDO0FBQ0g7O0FBZm1DLEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSHhDO0FBRWUsNkVBQWMwSyxtREFBZCxDQUF5QjtBQUNwQzBCLFlBQVUsR0FBRztBQUNULFNBQUs4SCxTQUFMLEdBQWlCLEtBQUt0cUIsSUFBTCxDQUFVc2hCLEdBQVYsQ0FBYyxXQUFkLENBQWpCO0FBQ0g7O0FBRURQLFNBQU8sR0FBRztBQUNOLFFBQUl6YyxLQUFLLEdBQUcsSUFBWjs7QUFFQSxTQUFLOUIsT0FBTCxDQUFhQyxnQkFBYixDQUE4QixRQUE5QixFQUF3QyxZQUFZO0FBQ2hELFVBQUk0akIsVUFBVSxHQUFHNW1CLE1BQU0sQ0FBQ3dGLFFBQVAsQ0FBZ0JrRixJQUFqQztBQUFBLFVBQ0k0ZCxNQUFNLEdBQUcsRUFEYjtBQUFBLFVBRUl6UyxLQUZKO0FBQUEsVUFFV2lWLGdCQUZYOztBQUlBLFVBQUlsRSxVQUFVLENBQUM3USxPQUFYLENBQW1CLEdBQW5CLElBQTBCLENBQTlCLEVBQWlDO0FBQzdCdVMsY0FBTSxHQUFJLEdBQUUxQixVQUFXLElBQUcvaEIsS0FBSyxDQUFDZ21CLFNBQVUsSUFBR2htQixLQUFLLENBQUM5QixPQUFOLENBQWNtRixLQUFNLEVBQWpFO0FBRUEsZUFBT2xJLE1BQU0sQ0FBQ3dGLFFBQVAsQ0FBZ0JrRixJQUFoQixHQUF1QjRkLE1BQTlCO0FBQ0g7O0FBRUQsVUFBSXlDLFNBQVMsR0FBRyxJQUFJQyxNQUFKLENBQVcsWUFBWCxDQUFoQjtBQUNBcEUsZ0JBQVUsR0FBR0EsVUFBVSxDQUFDbmhCLE9BQVgsQ0FBbUJzbEIsU0FBbkIsRUFBOEIsTUFBOUIsQ0FBYjtBQUVBbFYsV0FBSyxHQUFHLElBQUltVixNQUFKLENBQVksR0FBRW5tQixLQUFLLENBQUNnbUIsU0FBVSxHQUE5QixDQUFSO0FBQ0FDLHNCQUFnQixHQUFHbEUsVUFBVSxDQUFDOVEsS0FBWCxDQUFpQkQsS0FBakIsQ0FBbkI7O0FBRUEsVUFBSWlWLGdCQUFKLEVBQXNCO0FBQ2xCLFlBQUluYixLQUFLLEdBQUdpWCxVQUFVLENBQUM3WixLQUFYLENBQWlCLEdBQWpCLEVBQXNCLENBQXRCLEVBQXlCQSxLQUF6QixDQUErQixHQUEvQixDQUFaO0FBRUE0QyxhQUFLLEdBQUdBLEtBQUssQ0FBQzFILEdBQU4sQ0FBVWdqQixJQUFJLElBQUk7QUFDdEIsY0FBSUEsSUFBSSxDQUFDbGUsS0FBTCxDQUFXLEdBQVgsRUFBZ0IsQ0FBaEIsTUFBdUJsSSxLQUFLLENBQUNnbUIsU0FBakMsRUFBNEM7QUFDeEMsbUJBQVEsR0FBRWhtQixLQUFLLENBQUNnbUIsU0FBVSxJQUFHaG1CLEtBQUssQ0FBQzlCLE9BQU4sQ0FBY21GLEtBQU0sRUFBakQ7QUFDSDs7QUFFRCxpQkFBTytpQixJQUFQO0FBQ0gsU0FOTyxDQUFSO0FBUUEzQyxjQUFNLEdBQUcxQixVQUFVLENBQUM3WixLQUFYLENBQWlCLEdBQWpCLEVBQXNCLENBQXRCLElBQTJCLEdBQTNCLEdBQWlDNEMsS0FBSyxDQUFDZ0gsSUFBTixDQUFXLEdBQVgsQ0FBMUM7QUFFQSxlQUFPM1csTUFBTSxDQUFDd0YsUUFBUCxDQUFnQmtGLElBQWhCLEdBQXVCNGQsTUFBOUI7QUFDSCxPQWRELE1BY087QUFDSEEsY0FBTSxHQUFJLEdBQUUxQixVQUFXLElBQUcvaEIsS0FBSyxDQUFDZ21CLFNBQVUsSUFBR2htQixLQUFLLENBQUM5QixPQUFOLENBQWNtRixLQUFNLEVBQWpFO0FBRUEsZUFBT2xJLE1BQU0sQ0FBQ3dGLFFBQVAsQ0FBZ0JrRixJQUFoQixHQUF1QjRkLE1BQTlCO0FBQ0g7QUFDSixLQXBDRDtBQXFDSDs7QUE3Q21DLEM7Ozs7Ozs7Ozs7OztBQ0Z4QztBQUFBO0FBQUE7QUFFZSw2RUFBY2pILG1EQUFkLENBQXlCO0FBQ3BDQyxTQUFPLEdBQUU7QUFDTHJoQixnQkFBWSxDQUFDUSxNQUFiLENBQW9CLHNCQUFwQixFQUE0Q0YsSUFBSSxJQUFJO0FBQ2hELFVBQUlBLElBQUksQ0FBQ2toQixZQUFMLEtBQXNCLEtBQUtBLFlBQS9CLEVBQTZDO0FBQ3pDLFlBQUlsaEIsSUFBSSxDQUFDMkgsS0FBTCxLQUFlLGNBQWYsSUFBaUMzSCxJQUFJLENBQUMySCxLQUFMLEtBQWUsTUFBcEQsRUFBNEQ7QUFDeEQsZUFBS25GLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJDLE1BQXZCLENBQThCLFFBQTlCO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsZUFBS3ZLLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJFLEdBQXZCLENBQTJCLFFBQTNCO0FBQ0g7QUFDSjtBQUNKLEtBUkQ7QUFTSDs7QUFFRCxNQUFJa1UsWUFBSixHQUFtQjtBQUNmLFdBQU8sS0FBSzFlLE9BQUwsQ0FBYXllLE9BQWIsQ0FBcUJDLFlBQTVCO0FBQ0g7O0FBZm1DLEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRnhDO0FBRWUscUJBQWNKLG1EQUFkLENBQXlCO0FBR3BDNkosUUFBTSxHQUFHO0FBQ0wsUUFBSSxLQUFLbm9CLE9BQUwsQ0FBYXllLE9BQWIsQ0FBcUIySixVQUF6QixFQUFxQztBQUNqQyxVQUFJQyxRQUFRLEdBQUdwckIsTUFBTSxDQUFDcXJCLFVBQVAsQ0FBa0Isb0JBQWxCLEVBQXdDQyxPQUF2RDs7QUFFQSxVQUFJRixRQUFKLEVBQWM7QUFDVixhQUFLRyxhQUFMLENBQW1CdG9CLGNBQW5CLENBQWtDO0FBQUNzakIsa0JBQVEsRUFBRSxRQUFYO0FBQXFCQyxlQUFLLEVBQUU7QUFBNUIsU0FBbEM7QUFDSDtBQUNKLEtBTkQsTUFNTTtBQUNGLFdBQUsrRSxhQUFMLENBQW1CdG9CLGNBQW5CLENBQWtDO0FBQUNzakIsZ0JBQVEsRUFBRSxRQUFYO0FBQXFCQyxhQUFLLEVBQUU7QUFBNUIsT0FBbEM7QUFDSDtBQUNKOztBQWJtQzs7bUNBQ25CLENBQUMsU0FBRCxDOzs7Ozs7Ozs7Ozs7QUNIckI7QUFBQTtBQUFBO0FBRWUsNkVBQWNuRixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFHO0FBQ04sUUFBSWtLLGNBQWMsR0FBRyxLQUFLem9CLE9BQUwsQ0FBYVEsYUFBYixDQUEyQiwwQkFBM0IsQ0FBckI7O0FBRUMsUUFBSSxDQUFDaW9CLGNBQUwsRUFBcUI7QUFDakI7QUFDSDs7QUFFRkEsa0JBQWMsQ0FBQ3ZvQixjQUFmLENBQThCO0FBQUNzakIsY0FBUSxFQUFFLFFBQVg7QUFBcUJDLFdBQUssRUFBRTtBQUE1QixLQUE5QjtBQUNIOztBQVRtQyxDOzs7Ozs7Ozs7Ozs7QUNGeEM7QUFBQTtBQUFBO0FBRWUsNkVBQWNuRixtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFHO0FBQ05yaEIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQiw0QkFBcEIsRUFBa0RGLElBQUksSUFBSTtBQUN0RCxVQUFJLEtBQUtrckIsUUFBTCxLQUFrQmxyQixJQUFJLENBQUNrckIsUUFBM0IsRUFBcUM7QUFDakMsWUFBSWxyQixJQUFJLENBQUNtckIsU0FBVCxFQUFvQjtBQUNoQixlQUFLM29CLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJDLE1BQXZCLENBQThCLFFBQTlCO0FBQ0gsU0FGRCxNQUVPO0FBQ0gsZUFBS3ZLLE9BQUwsQ0FBYXNLLFNBQWIsQ0FBdUJFLEdBQXZCLENBQTJCLFFBQTNCO0FBQ0g7QUFDSjtBQUNKLEtBUkQ7QUFTSDs7QUFFRCxNQUFJa2UsUUFBSixHQUFlO0FBQ1gsV0FBTyxLQUFLMW9CLE9BQUwsQ0FBYXllLE9BQWIsQ0FBcUJpSyxRQUE1QjtBQUNIOztBQWZtQyxDOzs7Ozs7Ozs7Ozs7QUNGeEM7QUFBQTtBQUFBO0FBRWUsNkVBQWNwSyxtREFBZCxDQUF5QjtBQUNwQ0MsU0FBTyxHQUFHO0FBQ05yaEIsZ0JBQVksQ0FBQ1EsTUFBYixDQUFvQix1QkFBcEIsRUFBNkNGLElBQUksSUFBSTtBQUNqRCxVQUFJLEtBQUtnTCxRQUFMLEtBQWtCaEwsSUFBSSxDQUFDZ0wsUUFBM0IsRUFBcUM7QUFDakMsWUFBSWhMLElBQUksQ0FBQ21yQixTQUFULEVBQW9CO0FBQ2hCLGVBQUszb0IsT0FBTCxDQUFhc0ssU0FBYixDQUF1QkMsTUFBdkIsQ0FBOEIsUUFBOUI7QUFDSCxTQUZELE1BRU87QUFDSCxlQUFLdkssT0FBTCxDQUFhc0ssU0FBYixDQUF1QkUsR0FBdkIsQ0FBMkIsUUFBM0I7QUFDSDtBQUNKO0FBQ0osS0FSRDtBQVNIOztBQUVELE1BQUloQyxRQUFKLEdBQWU7QUFDWCxXQUFPLEtBQUt4SSxPQUFMLENBQWF5ZSxPQUFiLENBQXFCalcsUUFBNUI7QUFDSDs7QUFmbUMsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRnhDO0FBQ0E7QUFFZSw2RUFBYzhWLG1EQUFkLENBQXlCO0FBQ3BDQyxTQUFPLEdBQUc7QUFDTixRQUFJemMsS0FBSyxHQUFHLElBQVo7O0FBRUF5UCxzREFBUSxDQUFDQyxNQUFULENBQWdCLEtBQUt4UixPQUFyQixFQUE4QjtBQUMxQnlSLGdCQUFVLEVBQUUsNkJBRGM7QUFFMUJDLGVBQVMsRUFBRSxHQUZlO0FBRzFCQyxZQUFNLEVBQUUsWUFBWTtBQUNoQixZQUFJaVgsTUFBTSxHQUFHOW1CLEtBQUssQ0FBQzlCLE9BQU4sQ0FBY0YsZ0JBQWQsQ0FBK0IsT0FBL0IsQ0FBYjs7QUFFQThvQixjQUFNLENBQUM3b0IsT0FBUCxDQUFlLENBQUNDLE9BQUQsRUFBVVgsS0FBVixLQUFvQjtBQUMvQlcsaUJBQU8sQ0FBQzJlLFlBQVIsQ0FBcUIsSUFBckIsRUFBMkI3YyxLQUFLLENBQUMrbUIsUUFBTixDQUFlN29CLE9BQU8sQ0FBQ3dDLFlBQVIsQ0FBcUIsSUFBckIsQ0FBZixFQUEyQ25ELEtBQTNDLENBQTNCO0FBQ0FXLGlCQUFPLENBQUMyZSxZQUFSLENBQXFCLE1BQXJCLEVBQTZCN2MsS0FBSyxDQUFDZ25CLFVBQU4sQ0FBaUI5b0IsT0FBTyxDQUFDd0MsWUFBUixDQUFxQixNQUFyQixDQUFqQixFQUErQ25ELEtBQS9DLENBQTdCO0FBQ0gsU0FIRDtBQUlIO0FBVnlCLEtBQTlCO0FBWUg7O0FBRUR5cEIsWUFBVSxDQUFDQyxXQUFELEVBQWMxcEIsS0FBZCxFQUFxQjtBQUMzQixRQUFJc2pCLFFBQVEsR0FBR3RqQixLQUFLLEdBQUcsQ0FBUixLQUFjLENBQWQsR0FBa0JBLEtBQUssR0FBRyxDQUExQixHQUE4QixDQUFDQSxLQUFLLEdBQUcsQ0FBVCxJQUFjLENBQTNEO0FBRUEsV0FBTzBwQixXQUFXLENBQUNybUIsT0FBWixDQUFvQixTQUFwQixFQUFnQyxJQUFHaWdCLFFBQVMsR0FBNUMsQ0FBUDtBQUNIOztBQUVEa0csVUFBUSxDQUFDRyxTQUFELEVBQVkzcEIsS0FBWixFQUFtQjtBQUN2QixRQUFJc2pCLFFBQVEsR0FBR3RqQixLQUFLLEdBQUcsQ0FBUixLQUFjLENBQWQsR0FBa0JBLEtBQUssR0FBRyxDQUExQixHQUE4QixDQUFDQSxLQUFLLEdBQUcsQ0FBVCxJQUFjLENBQTNEO0FBRUEsV0FBTzJwQixTQUFTLENBQUN0bUIsT0FBVixDQUFrQixPQUFsQixFQUE0QixJQUFHaWdCLFFBQVMsR0FBeEMsQ0FBUDtBQUNIOztBQTVCbUMsQzs7Ozs7Ozs7Ozs7O0FDSHhDO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFZSw2RUFBY3JFLG1EQUFkLENBQXlCO0FBQ3BDQyxTQUFPLEdBQUc7QUFDTmhOLHNEQUFRLENBQUNDLE1BQVQsQ0FBZ0IsS0FBS3hSLE9BQXJCLEVBQThCO0FBQzFCeVIsZ0JBQVUsRUFBRSw2QkFEYztBQUUxQkMsZUFBUyxFQUFFO0FBRmUsS0FBOUI7QUFJSDs7QUFObUMsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNIeEM7QUFFZSxxQkFBYzRNLG1EQUFkLENBQXlCO0FBR3BDQyxTQUFPLEdBQUc7QUFDTjdlLEtBQUMsQ0FBQ0csUUFBRCxDQUFELENBQVlnZ0IsS0FBWixDQUFrQixNQUFNO0FBQ3BCLFdBQUs4SSxTQUFMLEdBQWlCLEtBQUszb0IsT0FBTCxDQUFheWUsT0FBYixDQUFxQmtLLFNBQXJCLEtBQW1DLE1BQXBEO0FBRUF6ckIsa0JBQVksQ0FBQ0ksSUFBYixDQUFrQiw0QkFBbEIsRUFBZ0Q7QUFDNUNvckIsZ0JBQVEsRUFBRSxLQUFLQSxRQUQ2QjtBQUU1Q0MsaUJBQVMsRUFBRSxLQUFLQTtBQUY0QixPQUFoRDtBQUlILEtBUEQ7QUFRSDs7QUFFRHBRLFFBQU0sR0FBRztBQUNMLFNBQUtvUSxTQUFMLEdBQWlCLENBQUMsS0FBS0EsU0FBdkI7QUFFQXpyQixnQkFBWSxDQUFDSSxJQUFiLENBQWtCLDRCQUFsQixFQUFnRDtBQUM1Q29yQixjQUFRLEVBQUUsS0FBS0EsUUFENkI7QUFFNUNDLGVBQVMsRUFBRSxLQUFLQTtBQUY0QixLQUFoRDtBQUlIOztBQUVELE1BQUlBLFNBQUosR0FBZ0I7QUFDWixXQUFPLEtBQUszb0IsT0FBTCxDQUFheWUsT0FBYixDQUFxQmtLLFNBQXJCLEtBQW1DLE1BQTFDO0FBQ0g7O0FBRUQsTUFBSUEsU0FBSixDQUFjeGpCLEtBQWQsRUFBcUI7QUFDakIsU0FBS25GLE9BQUwsQ0FBYTJlLFlBQWIsQ0FBMEIsaUJBQTFCLEVBQTZDeFosS0FBN0M7O0FBRUEsUUFBSUEsS0FBSyxLQUFLLElBQWQsRUFBb0I7QUFDaEIsV0FBS2dmLFdBQUwsQ0FBaUI3WixTQUFqQixDQUEyQkMsTUFBM0IsQ0FBa0MsZ0JBQWxDO0FBQ0EsV0FBSzRaLFdBQUwsQ0FBaUI3WixTQUFqQixDQUEyQkUsR0FBM0IsQ0FBK0IsZUFBL0I7QUFDSCxLQUhELE1BR087QUFDSCxXQUFLMlosV0FBTCxDQUFpQjdaLFNBQWpCLENBQTJCQyxNQUEzQixDQUFrQyxlQUFsQztBQUNBLFdBQUs0WixXQUFMLENBQWlCN1osU0FBakIsQ0FBMkJFLEdBQTNCLENBQStCLGdCQUEvQjtBQUNIO0FBQ0o7O0FBRUQsTUFBSWtlLFFBQUosR0FBZTtBQUNYLFdBQU8sS0FBSzFvQixPQUFMLENBQWF5ZSxPQUFiLENBQXFCaUssUUFBNUI7QUFDSDs7QUF6Q21DOzttQ0FDbkIsQ0FBRSxPQUFGLEM7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0hyQjtBQUVlLHFCQUFjcEssbURBQWQsQ0FBeUI7QUFHcEMvRixRQUFNLEdBQUc7QUFDTCxRQUFJLEtBQUswUSxxQkFBTCxDQUEyQnptQixZQUEzQixDQUF3QyxNQUF4QyxNQUFvRCxVQUF4RCxFQUFvRTtBQUNoRSxXQUFLeW1CLHFCQUFMLENBQTJCdEssWUFBM0IsQ0FBd0MsTUFBeEMsRUFBZ0QsTUFBaEQ7QUFDSCxLQUZELE1BRU87QUFDSCxXQUFLc0sscUJBQUwsQ0FBMkJ0SyxZQUEzQixDQUF3QyxNQUF4QyxFQUFnRCxVQUFoRDtBQUNIOztBQUVELFNBQUt1SyxtQkFBTCxDQUF5QjVlLFNBQXpCLENBQW1DaU8sTUFBbkMsQ0FBMEMsUUFBMUM7QUFDQSxTQUFLMlEsbUJBQUwsQ0FBeUI1ZSxTQUF6QixDQUFtQ2lPLE1BQW5DLENBQTBDLGNBQTFDO0FBQ0g7O0FBWm1DOzttQ0FDbkIsQ0FBRSxpQkFBRixFQUFxQixlQUFyQixDOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0hyQjtBQUVlLHFCQUFjK0YsbURBQWQsQ0FBeUI7QUFHcEMvRixRQUFNLENBQUNoYixLQUFELEVBQVE7QUFDVixRQUFJQSxLQUFLLENBQUM2a0IsTUFBTixDQUFhK0csUUFBYixLQUEwQixRQUE5QixFQUF3QztBQUNwQztBQUNIOztBQUVELFNBQUtSLFNBQUwsR0FBaUIsQ0FBQyxLQUFLQSxTQUF2QjtBQUVBenJCLGdCQUFZLENBQUNJLElBQWIsQ0FBa0IsdUJBQWxCLEVBQTJDO0FBQ3ZDa0wsY0FBUSxFQUFFLEtBQUtBLFFBRHdCO0FBRXZDbWdCLGVBQVMsRUFBRSxLQUFLQTtBQUZ1QixLQUEzQztBQUlIOztBQUVELE1BQUlBLFNBQUosR0FBZ0I7QUFDWixXQUFPLEtBQUszb0IsT0FBTCxDQUFheWUsT0FBYixDQUFxQmtLLFNBQXJCLEtBQW1DLE1BQTFDO0FBQ0g7O0FBRUQsTUFBSUEsU0FBSixDQUFjeGpCLEtBQWQsRUFBcUI7QUFDakIsU0FBS25GLE9BQUwsQ0FBYTJlLFlBQWIsQ0FBMEIsaUJBQTFCLEVBQTZDeFosS0FBN0M7O0FBRUEsUUFBSUEsS0FBSyxLQUFLLElBQWQsRUFBb0I7QUFDaEIsV0FBS2dmLFdBQUwsQ0FBaUI3WixTQUFqQixDQUEyQkMsTUFBM0IsQ0FBa0MsZ0JBQWxDO0FBQ0EsV0FBSzRaLFdBQUwsQ0FBaUI3WixTQUFqQixDQUEyQkUsR0FBM0IsQ0FBK0IsZUFBL0I7QUFDSCxLQUhELE1BR087QUFDSCxXQUFLMlosV0FBTCxDQUFpQjdaLFNBQWpCLENBQTJCQyxNQUEzQixDQUFrQyxlQUFsQztBQUNBLFdBQUs0WixXQUFMLENBQWlCN1osU0FBakIsQ0FBMkJFLEdBQTNCLENBQStCLGdCQUEvQjtBQUNIO0FBQ0o7O0FBRUQsTUFBSWhDLFFBQUosR0FBZTtBQUNYLFdBQU8sS0FBS3hJLE9BQUwsQ0FBYXllLE9BQWIsQ0FBcUJqVyxRQUE1QjtBQUNIOztBQWxDbUM7O21DQUNuQixDQUFFLE9BQUYsQzs7Ozs7Ozs7Ozs7QUNIckI5SSwwQ0FBQyxDQUFDRyxRQUFELENBQUQsQ0FBWWdnQixLQUFaLENBQWtCLFlBQVk7QUFFMUJuZ0IsR0FBQyxDQUFDekMsTUFBRCxDQUFELENBQVVrckIsTUFBVixDQUFpQixZQUFZO0FBQ3pCLFFBQUl6b0IsQ0FBQyxDQUFDRyxRQUFELENBQUQsQ0FBWXVwQixTQUFaLEtBQTBCLEVBQTlCLEVBQWtDO0FBQzlCMXBCLE9BQUMsQ0FBQyxRQUFELENBQUQsQ0FBWTJwQixRQUFaLENBQXFCLFlBQXJCO0FBQ0gsS0FGRCxNQUVPO0FBQ0gzcEIsT0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZNHBCLFdBQVosQ0FBd0IsWUFBeEI7QUFDSDtBQUNKLEdBTkQsRUFGMEIsQ0FXOUI7QUFDQTtBQUNBO0FBQ0E7O0FBRUk1cEIsR0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjbVEsS0FBZCxDQUFvQixZQUFZO0FBQzVCblEsS0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRNnBCLFdBQVIsQ0FBb0IsTUFBcEI7QUFDQTdwQixLQUFDLENBQUMsS0FBRCxDQUFELENBQVM2cEIsV0FBVCxDQUFxQixNQUFyQjtBQUNILEdBSEQ7QUFLSCxDQXJCRCxFOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUFsc0IsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxVQUFkLEVBQTBCO0FBQ3RCRSxVQUFRLEVBQUUsV0FEWTtBQUV0QkMsT0FBSyxFQUFFO0FBQ0h5VSxjQUFVLEVBQUU7QUFBQzVTLGFBQU8sRUFBRTtBQUFWLEtBRFQ7QUFFSDZTLFlBQVEsRUFBRTtBQUFDN1MsYUFBTyxFQUFFO0FBQVY7QUFGUCxHQUZlOztBQU10QmxFLFNBQU8sR0FBRztBQUNOLFFBQUk0VywrQ0FBSixDQUFhLEtBQUtwVCxHQUFsQixFQUF1QjtBQUNuQnNULGdCQUFVLEVBQUUsS0FBS0EsVUFERTtBQUVuQkMsY0FBUSxFQUFFLEtBQUtBLFFBRkk7QUFHbkIwVCxlQUFTLEVBQUUsTUFIUTtBQUluQnhULGFBQU8sRUFBRTtBQUpVLEtBQXZCO0FBTUg7O0FBYnFCLENBQTFCLEU7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSkE7QUFFQTNZLDJDQUFHLENBQUM0RCxTQUFKLENBQWMsYUFBZCxFQUE2QjtBQUN6QkUsVUFBUSxFQUFHLHlIQURjO0FBRXpCQyxPQUFLLEVBQUU7QUFDSHFvQixhQUFTLEVBQUU7QUFDUGpvQixjQUFRLEVBQUUsSUFESDtBQUVQRixVQUFJLEVBQUVDO0FBRkMsS0FEUjtBQUtIRSxlQUFXLEVBQUU7QUFDVEQsY0FBUSxFQUFFLElBREQ7QUFFVEYsVUFBSSxFQUFFQztBQUZHLEtBTFY7QUFTSG1vQix1QkFBbUIsRUFBRTtBQUNqQmxvQixjQUFRLEVBQUUsS0FETztBQUVqQkYsVUFBSSxFQUFFQyxNQUZXO0FBR2pCMEIsYUFBTyxFQUFFO0FBSFEsS0FUbEI7QUFjSDBtQixhQUFTLEVBQUU7QUFDUG5vQixjQUFRLEVBQUUsS0FESDtBQUVQRixVQUFJLEVBQUVDLE1BRkM7QUFHUDBCLGFBQU8sRUFBRTtBQUhGO0FBZFIsR0FGa0I7QUFzQnpCL0QsU0FBTyxFQUFFO0FBQ0wwcUIsZ0JBQVksR0FBRztBQUNYLFVBQUk5bkIsS0FBSyxHQUFHLElBQVo7O0FBRUEsVUFBRytuQixPQUFPLENBQUMsS0FBS0gsbUJBQU4sQ0FBVixFQUFxQztBQUNqQ2hxQixTQUFDLENBQUNxQyxJQUFGLENBQU87QUFDSEMsZ0JBQU0sRUFBRSxRQURMO0FBRUhDLGFBQUcsRUFBRSxLQUFLd25CLFNBRlA7O0FBR0h2bkIsaUJBQU8sR0FBRztBQUNOakYsa0JBQU0sQ0FBQ3dGLFFBQVAsQ0FBZ0JDLE9BQWhCLENBQXdCWixLQUFLLENBQUNMLFdBQTlCO0FBQ0gsV0FMRTs7QUFNSFQsZUFBSyxDQUFDQSxLQUFELEVBQVE7QUFDVEQsbUJBQU8sQ0FBQzRCLEdBQVIsQ0FBWTNCLEtBQVo7QUFDQS9ELGtCQUFNLENBQUN3RixRQUFQLENBQWdCQyxPQUFoQixDQUF3QlosS0FBSyxDQUFDTCxXQUE5QjtBQUNIOztBQVRFLFNBQVA7QUFXSDtBQUNKOztBQWpCSTtBQXRCZ0IsQ0FBN0IsRTs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUVBcEUsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxvQkFBZCxFQUFvQztBQUNoQ0UsVUFBUSxFQUFHOzs4Q0FEcUI7QUFJaENDLE9BQUssRUFBRTtBQUNIMG9CLGdCQUFZLEVBQUU7QUFDVnRvQixjQUFRLEVBQUUsSUFEQTtBQUVWRixVQUFJLEVBQUVDO0FBRkksS0FEWDtBQUtIbW9CLHVCQUFtQixFQUFFO0FBQ2pCbG9CLGNBQVEsRUFBRSxLQURPO0FBRWpCRixVQUFJLEVBQUVDLE1BRlc7QUFHakIwQixhQUFPLEVBQUU7QUFIUTtBQUxsQixHQUp5QjtBQWVoQ3RCLFVBQVEsRUFBRTtBQUNOb29CLFVBQU0sR0FBRztBQUNMLGFBQU8sc0JBQXNCLEtBQUtELFlBQWxDO0FBQ0g7O0FBSEssR0Fmc0I7QUFvQmhDNXFCLFNBQU8sRUFBRTtBQUNMMHFCLGdCQUFZLENBQUNyc0IsS0FBRCxFQUFRO0FBQ2hCLFVBQUcsQ0FBQ3NzQixPQUFPLENBQUMsS0FBS0gsbUJBQU4sQ0FBWCxFQUFzQztBQUNwQ25zQixhQUFLLENBQUNraUIsY0FBTjtBQUNEO0FBQ0o7O0FBTEk7QUFwQnVCLENBQXBDLEU7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRkE7QUFFQXBpQiwyQ0FBRyxDQUFDNEQsU0FBSixDQUFjLG9CQUFkLEVBQW9DO0FBQ2hDQyxNQUFJLEVBQUUsa0JBRDBCO0FBRWhDMUMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FGb0I7QUFHaEMyQyxVQUFRLEVBQUUscUJBSHNCO0FBSWhDQyxPQUFLLEVBQUU7QUFDSHljLGFBQVMsRUFBRTtBQUNQdmMsVUFBSSxFQUFFQyxNQURDO0FBRVBDLGNBQVEsRUFBRTtBQUZILEtBRFI7QUFLSEMsZUFBVyxFQUFFO0FBQ1RELGNBQVEsRUFBRSxJQUREO0FBRVRGLFVBQUksRUFBRUM7QUFGRyxLQUxWO0FBU0hpWixjQUFVLEVBQUU7QUFDUmhaLGNBQVEsRUFBRSxLQURGO0FBRVJGLFVBQUksRUFBRUMsTUFGRTtBQUdSMEIsYUFBTyxFQUFFO0FBSEQsS0FUVDtBQWNIK21CLGFBQVMsRUFBRTtBQUNQMW9CLFVBQUksRUFBRTBCLE9BREM7QUFFUHhCLGNBQVEsRUFBRSxLQUZIO0FBR1B5QixhQUFPLEVBQUU7QUFIRjtBQWRSLEdBSnlCOztBQXlCaEN6RixNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0h5UixlQUFTLEVBQUU7QUFEUixLQUFQO0FBR0gsR0E3QitCOztBQStCaEN0TixVQUFRLEVBQUU7QUFDTjZPLGVBQVcsR0FBRztBQUNWLGFBQU8sS0FBS3ZCLFNBQUwsS0FBbUIsSUFBMUI7QUFDSDs7QUFISyxHQS9Cc0I7QUFxQ2hDL1AsU0FBTyxFQUFFO0FBQ0x5USxjQUFVLEdBQUc7QUFDVCxXQUFLdE4sS0FBTCxDQUFXdU4sU0FBWCxDQUFxQkMsS0FBckI7QUFDSCxLQUhJOztBQUtMQyxjQUFVLEdBQUc7QUFDVCxVQUFJQyxJQUFJLEdBQUcsS0FBSzFOLEtBQUwsQ0FBV3VOLFNBQVgsQ0FBcUJJLEtBQXJCLENBQTJCLENBQTNCLENBQVg7QUFBQSxVQUNJbE8sS0FBSyxHQUFHLElBRFo7O0FBR0EsVUFBSSxDQUFDaU8sSUFBTCxFQUFXO0FBQ1A7QUFDSDs7QUFFRCxXQUFLZCxTQUFMLEdBQWlCLElBQWpCO0FBRUEsVUFBSWdCLFFBQVEsR0FBRyxJQUFJQyxRQUFKLEVBQWY7QUFDQUQsY0FBUSxDQUFDRSxNQUFULENBQWdCLE1BQWhCLEVBQXdCSixJQUF4QjtBQUVBclEsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLGNBQU0sRUFBRSxNQURMO0FBRUhDLFdBQUcsRUFBRSxLQUFLNGIsU0FGUDtBQUdIcmdCLFlBQUksRUFBRXlTLFFBSEg7QUFJSEcsbUJBQVcsRUFBRSxLQUpWO0FBS0hDLG1CQUFXLEVBQUUsS0FMVjs7QUFNSG5PLGVBQU8sR0FBRztBQUNOSixlQUFLLENBQUNtTixTQUFOLEdBQWtCLEtBQWxCO0FBRUFoUyxnQkFBTSxDQUFDd0YsUUFBUCxDQUFnQkMsT0FBaEIsQ0FBd0JaLEtBQUssQ0FBQ0wsV0FBOUI7QUFBMkNWLGlCQUFPLENBQUM0QixHQUFSLENBQVliLEtBQUssQ0FBQ0wsV0FBbEI7QUFDOUMsU0FWRTs7QUFXSFQsYUFBSyxDQUFDQSxLQUFELEVBQVE7QUFDVGMsZUFBSyxDQUFDbU4sU0FBTixHQUFrQixLQUFsQjtBQUVBcFIsZUFBSyxDQUFDbUQsS0FBSyxDQUFDLGNBQUQsQ0FBTCxDQUFzQnVQLE1BQXZCLEVBQStCLE9BQS9CLENBQUw7QUFDSDs7QUFmRSxPQUFQO0FBaUJIOztBQW5DSTtBQXJDdUIsQ0FBcEMsRTs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBbFQsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxZQUFkLEVBQTRCO0FBQ3hCekMsWUFBVSxFQUFFLENBQUMsSUFBRCxFQUFPLElBQVAsQ0FEWTtBQUV4QjJDLFVBQVEsRUFBRzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NBRmE7QUFxQnhCa2xCLFlBQVUsRUFBRTtBQUNSNEQsZUFBVyxFQUFFQyxvREFBWUE7QUFEakIsR0FyQlk7QUF3QnhCOW9CLE9BQUssRUFBRTtBQUNIK29CLGFBQVMsRUFBRTtBQUNQM29CLGNBQVEsRUFBRSxJQURIO0FBRVBGLFVBQUksRUFBRUM7QUFGQyxLQURSO0FBS0g2b0IsWUFBUSxFQUFFO0FBQ041b0IsY0FBUSxFQUFFLElBREo7QUFFTkYsVUFBSSxFQUFFQztBQUZBLEtBTFA7QUFTSDhvQixhQUFTLEVBQUU7QUFDUDdvQixjQUFRLEVBQUUsSUFESDtBQUVQRixVQUFJLEVBQUVDO0FBRkMsS0FUUjtBQWFIK29CLGtCQUFjLEVBQUU7QUFDWjlvQixjQUFRLEVBQUUsS0FERTtBQUVaRixVQUFJLEVBQUVnSCxNQUZNO0FBR1pyRixhQUFPLEVBQUU7QUFIRztBQWJiLEdBeEJpQjs7QUEyQ3hCbEUsU0FBTyxHQUFHO0FBQ04sU0FBS3NRLFNBQUw7QUFDSCxHQTdDdUI7O0FBOEN4QjdSLE1BQUksR0FBRztBQUNILFFBQUlzRSxLQUFLLEdBQUcsSUFBWjs7QUFFQSxXQUFPO0FBQ0h5b0IscUJBQWUsRUFBRTtBQUNidG9CLFdBQUcsRUFBRSxLQUFLa29CLFNBREc7QUFFYkcsc0JBQWMsRUFBRSxLQUFLQSxjQUZSO0FBR2JFLHNCQUFjLEVBQUUsSUFISDtBQUliQyxpQkFBUyxFQUFFLE1BSkU7O0FBS2IvUixZQUFJLEdBQUc7QUFDSCxlQUFLeEosRUFBTCxDQUFRLE9BQVIsRUFBaUIsVUFBU2EsSUFBVCxFQUFldlMsSUFBZixFQUFxQjtBQUNsQyxnQkFBSUEsSUFBSSxDQUFDK1MsTUFBVCxFQUFpQjtBQUNiMVMsbUJBQUssQ0FBQ0wsSUFBSSxDQUFDK1MsTUFBTixFQUFjLE9BQWQsQ0FBTDtBQUNIO0FBQ0osV0FKRDtBQU1BLGVBQUtyQixFQUFMLENBQVEsU0FBUixFQUFtQixVQUFTYSxJQUFULEVBQWV2UyxJQUFmLEVBQXFCO0FBQ3BDc0UsaUJBQUssQ0FBQ2dOLE1BQU4sQ0FBYTNQLElBQWIsQ0FBa0IzQixJQUFsQjs7QUFFQUssaUJBQUssQ0FBQyxxQkFBRCxFQUF3QixTQUF4QixDQUFMO0FBQ0gsV0FKRDtBQUtIOztBQWpCWSxPQURkO0FBb0JIaVIsWUFBTSxFQUFFLEVBcEJMO0FBcUJIbEcsV0FBSyxFQUFFLFNBckJKO0FBc0JIOGhCLHNCQUFnQixFQUFFLENBQUM7QUF0QmhCLEtBQVA7QUF3QkgsR0F6RXVCOztBQTBFeEJ4ckIsU0FBTyxFQUFFO0FBQ0xtUSxhQUFTLEdBQUc7QUFDUixVQUFJdk4sS0FBSyxHQUFHLElBQVo7O0FBRUFwQyxPQUFDLENBQUNxQyxJQUFGLENBQU87QUFDSEMsY0FBTSxFQUFFLEtBREw7QUFFSEMsV0FBRyxFQUFFSCxLQUFLLENBQUNzb0IsUUFGUjs7QUFHSGxvQixlQUFPLENBQUNDLFFBQUQsRUFBVztBQUNkTCxlQUFLLENBQUNnTixNQUFOLEdBQWVRLElBQUksQ0FBQ0MsS0FBTCxDQUFXcE4sUUFBWCxDQUFmO0FBQ0gsU0FMRTs7QUFNSG5CLGFBQUssQ0FBQ0EsS0FBRCxFQUFRO0FBQ1RELGlCQUFPLENBQUM0QixHQUFSLENBQVkzQixLQUFaO0FBQ0g7O0FBUkUsT0FBUDtBQVVILEtBZEk7O0FBZUwycEIsZUFBVyxDQUFDbnJCLEVBQUQsRUFBSztBQUNaLFVBQUlzQyxLQUFLLEdBQUcsSUFBWjtBQUFBLFVBQ0l0RSxJQUFJLEdBQUc7QUFBQ290QixnQkFBUSxFQUFFcHJCO0FBQVgsT0FEWDs7QUFHQUUsT0FBQyxDQUFDcUMsSUFBRixDQUFPO0FBQ0hDLGNBQU0sRUFBRSxRQURMO0FBRUhDLFdBQUcsRUFBRUgsS0FBSyxDQUFDdW9CLFNBRlI7QUFHSDdzQixZQUFJLEVBQUVBLElBSEg7O0FBSUgwRSxlQUFPLEdBQUc7QUFDTixjQUFJMm9CLFVBQVUsR0FBRy9vQixLQUFLLENBQUNnTixNQUFOLENBQWF4UCxTQUFiLENBQXVCQyxJQUFJLElBQUlBLElBQUksQ0FBQ0MsRUFBTCxLQUFZQSxFQUEzQyxDQUFqQjs7QUFFQXNDLGVBQUssQ0FBQ2dOLE1BQU4sQ0FBYXJQLE1BQWIsQ0FBb0JvckIsVUFBcEIsRUFBZ0MsQ0FBaEM7O0FBRUFodEIsZUFBSyxDQUFDLG1CQUFELEVBQXNCLFNBQXRCLENBQUw7QUFDSCxTQVZFOztBQVdIbUQsYUFBSyxDQUFDQSxLQUFELEVBQVE7QUFDVEQsaUJBQU8sQ0FBQzRCLEdBQVIsQ0FBWTNCLEtBQVo7QUFDSDs7QUFiRSxPQUFQO0FBZUg7O0FBbENJO0FBMUVlLENBQTVCLEU7Ozs7Ozs7Ozs7Ozs7QUNKQTtBQUFBO0FBQUE7QUFFQTNELDJDQUFHLENBQUM0RCxTQUFKLENBQWMsT0FBZCxFQUF1QjtBQUNuQkUsVUFBUSxFQUFFLFFBRFM7QUFFbkJDLE9BQUssRUFBRTtBQUNILGVBQVc7QUFDUEksY0FBUSxFQUFFLElBREg7QUFFUEYsVUFBSSxFQUFFQztBQUZDLEtBRFI7QUFLSCxhQUFTO0FBQ0xDLGNBQVEsRUFBRSxLQURMO0FBRUxGLFVBQUksRUFBRUMsTUFGRDtBQUdMMEIsYUFBTyxFQUFFO0FBSEo7QUFMTixHQUZZOztBQWFuQnpGLE1BQUksR0FBRztBQUNILFdBQU87QUFDSHN0QixrQkFBWSxFQUFFLFNBRFg7QUFFSGpZLFVBQUksRUFBRSxLQUZIO0FBR0hrWSxrQkFBWSxFQUFFO0FBSFgsS0FBUDtBQUtILEdBbkJrQjs7QUFvQm5CaGEsU0FBTyxHQUFHO0FBQ04sU0FBSytaLFlBQUwsR0FBb0IsS0FBS0UsZUFBTCxDQUFxQixLQUFLanRCLEtBQTFCLENBQXBCO0FBRUEsU0FBS0YsS0FBTDtBQUNILEdBeEJrQjs7QUF5Qm5CcUIsU0FBTyxFQUFFO0FBQ0xyQixTQUFLLEdBQUc7QUFDSixVQUFJLEtBQUtrdEIsWUFBVCxFQUF1QjtBQUNuQjtBQUNIOztBQUVELFdBQUtELFlBQUwsR0FBb0IsS0FBS0UsZUFBTCxDQUFxQixLQUFLanRCLEtBQTFCLENBQXBCO0FBQ0EsV0FBSzhVLElBQUwsR0FBWSxJQUFaO0FBQ0EsV0FBS29ZLElBQUw7QUFDSCxLQVRJOztBQVdMQSxRQUFJLEdBQUc7QUFDSCxXQUFLRixZQUFMLEdBQW9CLElBQXBCO0FBRUE5WCxnQkFBVSxDQUFDLE1BQU07QUFDYixhQUFLSixJQUFMLEdBQVksS0FBWjtBQUNILE9BRlMsRUFFUCxJQUZPLENBQVY7QUFHSCxLQWpCSTs7QUFtQkxtWSxtQkFBZSxDQUFDanRCLEtBQUQsRUFBUTtBQUNuQixVQUFJQSxLQUFLLEtBQUssT0FBZCxFQUF1QjtBQUNuQixlQUFPLFFBQVA7QUFDSCxPQUZELE1BRU87QUFDSCxlQUFPLFNBQVA7QUFDSDtBQUNKOztBQXpCSTtBQXpCVSxDQUF2QixFOzs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7QUFFQVYsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxTQUFkLEVBQXlCO0FBQ3JCRSxVQUFRLEVBQUUsVUFEVztBQUVyQkMsT0FBSyxFQUFFO0FBQ0gsZUFBVztBQUNQSSxjQUFRLEVBQUUsS0FESDtBQUVQRixVQUFJLEVBQUVDLE1BRkM7QUFHUDBCLGFBQU8sRUFBRTtBQUhGO0FBRFIsR0FGYzs7QUFTckJ6RixNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0gwdEIsY0FBUSxFQUFFO0FBRFAsS0FBUDtBQUdILEdBYm9COztBQWNyQm5hLFNBQU8sR0FBRztBQUNOLFFBQUlvYSxTQUFTLEdBQUc3YixJQUFJLENBQUNDLEtBQUwsQ0FBVyxLQUFLNmIsT0FBaEIsQ0FBaEI7O0FBRUEsU0FBSyxJQUFJQyxHQUFULElBQWdCRixTQUFoQixFQUEyQjtBQUN2QkEsZUFBUyxDQUFDRSxHQUFELENBQVQsQ0FBZXRyQixPQUFmLENBQXVCakMsT0FBTyxJQUFJLEtBQUtvdEIsUUFBTCxDQUFjL3JCLElBQWQsQ0FBbUI7QUFBQ3JCLGVBQU8sRUFBRUEsT0FBVjtBQUFtQkMsYUFBSyxFQUFFc3RCO0FBQTFCLE9BQW5CLENBQWxDO0FBQ0g7O0FBRURwdUIsVUFBTSxDQUFDQyxZQUFQLENBQW9CUSxNQUFwQixDQUNJLE9BREosRUFDYTR0QixXQUFXLElBQUksS0FBS0MsVUFBTCxDQUFnQkQsV0FBaEIsQ0FENUI7QUFHSCxHQXhCb0I7O0FBeUJyQnBzQixTQUFPLEVBQUU7QUFDTHFzQixjQUFVLENBQUNELFdBQUQsRUFBYztBQUNwQixXQUFLSixRQUFMLENBQWMvckIsSUFBZCxDQUFtQm1zQixXQUFuQjtBQUNIOztBQUhJO0FBekJZLENBQXpCLEU7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUVBanVCLDJDQUFHLENBQUM0RCxTQUFKLENBQWMscUJBQWQsRUFBcUM7QUFDakNFLFVBQVEsRUFBRzs7OztDQURzQjtBQU1qQ0MsT0FBSyxFQUFFO0FBQ0hhLE9BQUcsRUFBRTtBQUNEVCxjQUFRLEVBQUUsSUFEVDtBQUVERixVQUFJLEVBQUVDO0FBRkwsS0FERjtBQUtIUyxVQUFNLEVBQUU7QUFDSlIsY0FBUSxFQUFFLEtBRE47QUFFSkYsVUFBSSxFQUFFQyxNQUZGO0FBR0owQixhQUFPLEVBQUU7QUFITCxLQUxMO0FBVUh1b0IsZ0JBQVksRUFBRTtBQUNWaHFCLGNBQVEsRUFBRSxJQURBO0FBRVZGLFVBQUksRUFBRUM7QUFGSTtBQVZYLEdBTjBCOztBQXFCakMvRCxNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0hvTCxXQUFLLEVBQUUsU0FESjtBQUVIekQsV0FBSyxFQUFFO0FBRkosS0FBUDtBQUlILEdBMUJnQzs7QUEyQmpDcEcsU0FBTyxHQUFHO0FBQ04sU0FBS29HLEtBQUwsR0FBYSxLQUFLcW1CLFlBQUwsS0FBc0IsR0FBbkM7QUFDSCxHQTdCZ0M7O0FBOEJqQ3RzQixTQUFPLEVBQUU7QUFDTCtILFVBQU0sR0FBRztBQUNMLFVBQUluRixLQUFLLEdBQUcsSUFBWjs7QUFFQXBDLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsS0FBS0EsTUFEVjtBQUVIQyxXQUFHLEVBQUUsS0FBS0EsR0FGUDtBQUdIekUsWUFBSSxFQUFFO0FBQUMySCxlQUFLLEVBQUUsS0FBS0E7QUFBYixTQUhIOztBQUlIakQsZUFBTyxHQUFHO0FBQ05yRSxlQUFLLENBQUMsVUFBRCxFQUFhLFNBQWIsQ0FBTDtBQUNILFNBTkU7O0FBT0htRCxhQUFLLENBQUNBLEtBQUQsRUFBUTtBQUNUYyxlQUFLLENBQUNxRCxLQUFOLEdBQWNyRCxLQUFLLENBQUMwcEIsWUFBTixLQUF1QixHQUFyQztBQUNBM3RCLGVBQUssQ0FBQ21ELEtBQUssQ0FBQyxjQUFELENBQUwsQ0FBc0J1UCxNQUF2QixFQUErQixPQUEvQixDQUFMO0FBQ0g7O0FBVkUsT0FBUDtBQVlIOztBQWhCSTtBQTlCd0IsQ0FBckMsRTs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUVBbFQsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxtQkFBZCxFQUFtQztBQUMvQkUsVUFBUSxFQUFHOzs7Ozs7Ozs7Q0FEb0I7QUFXL0JDLE9BQUssRUFBRTtBQUNIYSxPQUFHLEVBQUU7QUFDRFQsY0FBUSxFQUFFLElBRFQ7QUFFREYsVUFBSSxFQUFFQztBQUZMLEtBREY7QUFLSFMsVUFBTSxFQUFFO0FBQ0pSLGNBQVEsRUFBRSxLQUROO0FBRUpGLFVBQUksRUFBRUMsTUFGRjtBQUdKMEIsYUFBTyxFQUFFO0FBSEwsS0FMTDtBQVVIdW9CLGdCQUFZLEVBQUU7QUFDVmhxQixjQUFRLEVBQUUsS0FEQTtBQUVWRixVQUFJLEVBQUVDLE1BRkk7QUFHVjBCLGFBQU8sRUFBRTtBQUhDO0FBVlgsR0FYd0I7O0FBMkIvQnpGLE1BQUksR0FBRztBQUNILFdBQU87QUFDSG9MLFdBQUssRUFBRSxTQURKO0FBRUh6RCxXQUFLLEVBQUU7QUFGSixLQUFQO0FBSUgsR0FoQzhCOztBQWlDL0JwRyxTQUFPLEdBQUc7QUFDTixTQUFLb0csS0FBTCxHQUFhLEtBQUtxbUIsWUFBbEI7QUFDSCxHQW5DOEI7O0FBb0MvQnRzQixTQUFPLEVBQUU7QUFDTG9ULFFBQUksR0FBRztBQUNILFVBQUl4USxLQUFLLEdBQUcsSUFBWjs7QUFFQXBDLE9BQUMsQ0FBQ3FDLElBQUYsQ0FBTztBQUNIQyxjQUFNLEVBQUUsS0FBS0EsTUFEVjtBQUVIQyxXQUFHLEVBQUUsS0FBS0EsR0FGUDtBQUdIekUsWUFBSSxFQUFFO0FBQUMySCxlQUFLLEVBQUUsS0FBS0E7QUFBYixTQUhIOztBQUlIakQsZUFBTyxHQUFHO0FBQ05KLGVBQUssQ0FBQzhHLEtBQU4sR0FBYyxTQUFkO0FBQ0EvSyxlQUFLLENBQUMsd0JBQUQsRUFBMkIsU0FBM0IsQ0FBTDtBQUNILFNBUEU7O0FBUUhtRCxhQUFLLENBQUNBLEtBQUQsRUFBUTtBQUNUYyxlQUFLLENBQUM4RyxLQUFOLEdBQWMsU0FBZDtBQUNBOUcsZUFBSyxDQUFDcUQsS0FBTixHQUFjckQsS0FBSyxDQUFDMHBCLFlBQXBCO0FBQ0EzdEIsZUFBSyxDQUFDbUQsS0FBSyxDQUFDLGNBQUQsQ0FBTCxDQUFzQnVQLE1BQXZCLEVBQStCLE9BQS9CLENBQUw7QUFDSDs7QUFaRSxPQUFQO0FBY0g7O0FBbEJJLEdBcENzQjtBQXdEL0J2SSxPQUFLLEVBQUU7QUFDSFksU0FBSyxFQUFFLFVBQVM2SixRQUFULEVBQW1CO0FBQ3RCLFVBQUlBLFFBQVEsS0FBSyxTQUFqQixFQUE0QjtBQUN4QixhQUFLMEUsU0FBTCxDQUFlLE1BQU07QUFDakIsZUFBSzlVLEtBQUwsQ0FBVytVLFNBQVgsQ0FBcUJDLEtBQXJCO0FBQ0EsZUFBS2hWLEtBQUwsQ0FBVytVLFNBQVgsQ0FBcUJFLE1BQXJCO0FBQ0gsU0FIRDtBQUlIO0FBQ0o7QUFSRTtBQXhEd0IsQ0FBbkMsRTs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUVBamEsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxPQUFkLEVBQXVCO0FBQ25CRSxVQUFRLEVBQUUsUUFEUztBQUVuQjNDLFlBQVUsRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBRk87QUFHbkI0QyxPQUFLLEVBQUU7QUFDSGtFLFNBQUssRUFBRTtBQUNIaEUsVUFBSSxFQUFFQyxNQURIO0FBRUhDLGNBQVEsRUFBRSxLQUZQO0FBR0h5QixhQUFPLEVBQUU7QUFITixLQURKO0FBTUh3b0IsaUJBQWEsRUFBRTtBQUNYbnFCLFVBQUksRUFBRUMsTUFESztBQUVYQyxjQUFRLEVBQUUsS0FGQztBQUdYeUIsYUFBTyxFQUFFO0FBSEUsS0FOWjtBQVdIeW9CLGNBQVUsRUFBRTtBQUNScHFCLFVBQUksRUFBRUMsTUFERTtBQUVSQyxjQUFRLEVBQUU7QUFGRixLQVhUO0FBZUhpVSxRQUFJLEVBQUU7QUFDRm5VLFVBQUksRUFBRUMsTUFESjtBQUVGQyxjQUFRLEVBQUUsS0FGUjtBQUdGeUIsYUFBTyxFQUFFO0FBSFAsS0FmSDtBQW9CSDBvQixzQkFBa0IsRUFBRTtBQUNoQnJxQixVQUFJLEVBQUUwQixPQURVO0FBRWhCeEIsY0FBUSxFQUFFLEtBRk07QUFHaEJ5QixhQUFPLEVBQUU7QUFITyxLQXBCakI7QUF5Qkgyb0IsYUFBUyxFQUFFO0FBQ1BwcUIsY0FBUSxFQUFFLEtBREg7QUFFUEYsVUFBSSxFQUFFMEIsT0FGQztBQUdQQyxhQUFPLEVBQUU7QUFIRjtBQXpCUixHQUhZO0FBa0NuQnRCLFVBQVEsRUFBRTtBQUNOa3FCLGFBQVMsR0FBRztBQUNSLFVBQUksQ0FBQyxLQUFLcFcsSUFBVixFQUFnQjtBQUNaLGVBQU8sRUFBUDtBQUNIOztBQUVELGFBQVEsU0FBUSxLQUFLQSxJQUFLLEVBQTFCO0FBQ0gsS0FQSzs7QUFRTnFXLDJCQUF1QixHQUFHO0FBQ3RCLFVBQUksS0FBS0gsa0JBQVQsRUFBNkI7QUFDekIsZUFBTyx1QkFBUDtBQUNILE9BRkQsTUFFTztBQUNILGVBQU8sRUFBUDtBQUNIO0FBQ0o7O0FBZEs7QUFsQ1MsQ0FBdkIsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGQTtBQUVBdHVCLDJDQUFHLENBQUM0RCxTQUFKLENBQWMsV0FBZCxFQUEyQjtBQUN2QkUsVUFBUSxFQUFHLHdHQURZO0FBRXZCQyxPQUFLLEVBQUU7QUFDSHFvQixhQUFTLEVBQUU7QUFDUGpvQixjQUFRLEVBQUUsSUFESDtBQUVQRixVQUFJLEVBQUVDO0FBRkMsS0FEUjtBQUtIRSxlQUFXLEVBQUU7QUFDVEQsY0FBUSxFQUFFLElBREQ7QUFFVEYsVUFBSSxFQUFFQztBQUZHLEtBTFY7QUFTSHdxQixxQkFBaUIsRUFBRTtBQUNmdnFCLGNBQVEsRUFBRSxLQURLO0FBRWZGLFVBQUksRUFBRTBCLE9BRlM7QUFHZkMsYUFBTyxFQUFFO0FBSE0sS0FUaEI7QUFjSHltQix1QkFBbUIsRUFBRTtBQUNqQmxvQixjQUFRLEVBQUUsS0FETztBQUVqQkYsVUFBSSxFQUFFQyxNQUZXO0FBR2pCMEIsYUFBTyxFQUFFO0FBSFEsS0FkbEI7QUFtQkgwbUIsYUFBUyxFQUFFO0FBQ1Bub0IsY0FBUSxFQUFFLEtBREg7QUFFUEYsVUFBSSxFQUFFQyxNQUZDO0FBR1AwQixhQUFPLEVBQUU7QUFIRixLQW5CUjtBQXdCSCtvQixhQUFTLEVBQUU7QUFDUHhxQixjQUFRLEVBQUUsS0FESDtBQUVQRixVQUFJLEVBQUVDLE1BRkM7QUFHUDBCLGFBQU8sRUFBRTtBQUhGO0FBeEJSLEdBRmdCO0FBZ0N2Qi9ELFNBQU8sRUFBRTtBQUNMK3NCLGNBQVUsR0FBRztBQUNULFVBQUlucUIsS0FBSyxHQUFHLElBQVo7O0FBRUEsVUFBSSxLQUFLaXFCLGlCQUFULEVBQTRCO0FBQ3hCLFlBQUlsQyxPQUFPLENBQUMsS0FBS0gsbUJBQU4sQ0FBWCxFQUF1QztBQUNuQ2hxQixXQUFDLENBQUNxQyxJQUFGLENBQU87QUFDSEMsa0JBQU0sRUFBRSxNQURMO0FBRUhDLGVBQUcsRUFBRSxLQUFLd25CLFNBRlA7O0FBR0h2bkIsbUJBQU8sR0FBRztBQUNOakYsb0JBQU0sQ0FBQ3dGLFFBQVAsQ0FBZ0JDLE9BQWhCLENBQXdCWixLQUFLLENBQUNMLFdBQTlCO0FBQ0gsYUFMRTs7QUFNSFQsaUJBQUssQ0FBQ0EsS0FBRCxFQUFRO0FBQ1Qsa0JBQUlBLEtBQUssQ0FBQyxjQUFELENBQUwsQ0FBc0IsT0FBdEIsQ0FBSixFQUFvQztBQUNoQ25ELHFCQUFLLENBQUNtRCxLQUFLLENBQUMsY0FBRCxDQUFMLENBQXNCLE9BQXRCLENBQUQsRUFBaUMsT0FBakMsQ0FBTDtBQUNIO0FBQ0o7O0FBVkUsV0FBUDtBQVlIO0FBQ0osT0FmRCxNQWVPO0FBQ0h0QixTQUFDLENBQUNxQyxJQUFGLENBQU87QUFDSEMsZ0JBQU0sRUFBRSxNQURMO0FBRUhDLGFBQUcsRUFBRSxLQUFLd25CLFNBRlA7O0FBR0h2bkIsaUJBQU8sR0FBRztBQUNOakYsa0JBQU0sQ0FBQ3dGLFFBQVAsQ0FBZ0JDLE9BQWhCLENBQXdCWixLQUFLLENBQUNMLFdBQTlCO0FBQ0gsV0FMRTs7QUFNSFQsZUFBSyxDQUFDQSxLQUFELEVBQVE7QUFDVCxnQkFBSUEsS0FBSyxDQUFDLGNBQUQsQ0FBTCxDQUFzQixPQUF0QixDQUFKLEVBQW9DO0FBQ2hDbkQsbUJBQUssQ0FBQ21ELEtBQUssQ0FBQyxjQUFELENBQUwsQ0FBc0IsT0FBdEIsQ0FBRCxFQUFpQyxPQUFqQyxDQUFMO0FBQ0g7QUFDSjs7QUFWRSxTQUFQO0FBWUg7QUFDSjs7QUFqQ0k7QUFoQ2MsQ0FBM0IsRTs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBM0QsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxRQUFkLEVBQXdCO0FBQ3BCRSxVQUFRLEVBQUcsOENBRFM7QUFFcEJDLE9BQUssRUFBRTtBQUNIOHFCLFlBQVEsRUFBRTtBQUNOanBCLGFBQU8sRUFBRSxJQURIO0FBRU4zQixVQUFJLEVBQUUwQixPQUZBO0FBR054QixjQUFRLEVBQUU7QUFISixLQURQO0FBTUgycUIsZ0JBQVksRUFBRTtBQUNWbHBCLGFBQU8sRUFBRSxDQURDO0FBRVYzQixVQUFJLEVBQUVnSCxNQUZJO0FBR1Y5RyxjQUFRLEVBQUU7QUFIQSxLQU5YO0FBV0g0cUIsa0JBQWMsRUFBRTtBQUNabnBCLGFBQU8sRUFBRSxDQURHO0FBRVozQixVQUFJLEVBQUVnSCxNQUZNO0FBR1o5RyxjQUFRLEVBQUU7QUFIRSxLQVhiO0FBZ0JINnFCLFFBQUksRUFBRTtBQUNGcHBCLGFBQU8sRUFBRSxLQURQO0FBRUYzQixVQUFJLEVBQUUwQixPQUZKO0FBR0Z4QixjQUFRLEVBQUU7QUFIUixLQWhCSDtBQXFCSGdVLGlCQUFhLEVBQUU7QUFDWHZTLGFBQU8sRUFBRSxNQURFO0FBRVgzQixVQUFJLEVBQUVDLE1BRks7QUFHWEMsY0FBUSxFQUFFO0FBSEM7QUFyQlosR0FGYTs7QUE2QnBCekMsU0FBTyxHQUFHO0FBQ05XLEtBQUMsQ0FBQyxLQUFLNkMsR0FBTixDQUFELENBQVkrcEIsS0FBWixDQUFrQjtBQUNkO0FBQ0FKLGNBQVEsRUFBRSxLQUFLQSxRQUZEO0FBR2RDLGtCQUFZLEVBQUUsS0FBS0EsWUFITDtBQUlkQyxvQkFBYyxFQUFFLEtBQUtBLGNBSlA7QUFLZEMsVUFBSSxFQUFFLEtBQUtBLElBTEc7QUFNZDdXLG1CQUFhLEVBQUUsS0FBS0EsYUFOTjtBQVFkO0FBQ0ErVyxnQkFBVSxFQUFFLENBQUM7QUFFVEMsa0JBQVUsRUFBRSxJQUZIO0FBR1Q1ckIsZ0JBQVEsRUFBRTtBQUNOdXJCLHNCQUFZLEVBQUUsQ0FEUjtBQUVOQyx3QkFBYyxFQUFFLENBRlY7QUFHTkYsa0JBQVEsRUFBRSxJQUhKO0FBSU4xVyx1QkFBYSxFQUFFO0FBSlQ7QUFIRCxPQUFELEVBVVQ7QUFDQ2dYLGtCQUFVLEVBQUUsR0FEYjtBQUVDNXJCLGdCQUFRLEVBQUU7QUFDTnVyQixzQkFBWSxFQUFFLENBRFI7QUFFTkMsd0JBQWMsRUFBRSxDQUZWO0FBR041Vyx1QkFBYSxFQUFFO0FBSFQ7QUFGWCxPQVZTLEVBa0JaO0FBQ0lnWCxrQkFBVSxFQUFFLEdBRGhCO0FBRUk1ckIsZ0JBQVEsRUFBRSxTQUZkLENBRXdCOztBQUZ4QixPQWxCWTtBQVRFLEtBQWxCO0FBaUNIOztBQS9EbUIsQ0FBeEIsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTEE7QUFFQXZELDJDQUFHLENBQUM0RCxTQUFKLENBQWMsS0FBZCxFQUFxQjtBQUNqQkUsVUFBUSxFQUFFLE1BRE87QUFFakJDLE9BQUssRUFBRTtBQUNIRixRQUFJLEVBQUU7QUFBRU0sY0FBUSxFQUFFO0FBQVosS0FESDtBQUVIaXJCLFlBQVEsRUFBRTtBQUFFeHBCLGFBQU8sRUFBRTtBQUFYLEtBRlA7QUFHSHlwQixVQUFNLEVBQUU7QUFDSmxyQixjQUFRLEVBQUUsS0FETjtBQUVKRixVQUFJLEVBQUVDLE1BRkY7QUFHSjBCLGFBQU8sRUFBRTtBQUhMO0FBSEwsR0FGVTs7QUFZakJ6RixNQUFJLEdBQUc7QUFDSCxXQUFPO0FBQ0htdkIsY0FBUSxFQUFFLEtBRFA7QUFFSEMsbUJBQWEsRUFBRTtBQUZaLEtBQVA7QUFJSCxHQWpCZ0I7O0FBbUJqQmpyQixVQUFRLEVBQUU7QUFDTmdHLFFBQUksR0FBRztBQUNILGFBQU8sTUFBTSxLQUFLekcsSUFBTCxDQUFVOGlCLFdBQVYsR0FBd0J0aEIsT0FBeEIsQ0FBZ0MsSUFBaEMsRUFBc0MsR0FBdEMsQ0FBYjtBQUNIOztBQUhLLEdBbkJPOztBQXlCakJxTyxTQUFPLEdBQUc7QUFDTjdULGdCQUFZLENBQUNRLE1BQWIsQ0FBb0IsZ0JBQXBCLEVBQXNDLEtBQUttdkIsUUFBM0M7QUFDSCxHQTNCZ0I7O0FBNkJqQjl0QixTQUFPLEdBQUc7QUFDTixRQUFJOGtCLFVBQVUsR0FBRzVtQixNQUFNLENBQUN3RixRQUFQLENBQWdCa0YsSUFBakM7QUFBQSxRQUNJaUYsS0FBSyxHQUFHaVgsVUFBVSxDQUFDN1osS0FBWCxDQUFpQixHQUFqQixDQURaO0FBQUEsUUFFSThaLFFBQVEsR0FBRyxJQUZmOztBQUlBLFFBQUlsWCxLQUFLLENBQUN4RixNQUFOLEdBQWUsQ0FBbkIsRUFBc0I7QUFDbEIwYyxjQUFRLEdBQUdDLFNBQVMsQ0FBQ25YLEtBQUssQ0FBQ0EsS0FBSyxDQUFDeEYsTUFBTixHQUFlLENBQWhCLENBQU4sQ0FBVCxDQUFtQzRjLFdBQW5DLEdBQWlEdGhCLE9BQWpELENBQXlELElBQXpELEVBQStELEdBQS9ELENBQVg7O0FBRUEsVUFBSSxDQUFDLEtBQUtrcUIsYUFBVixFQUF5QjtBQUNyQixhQUFLRCxRQUFMLEdBQWlCLE1BQU03SSxRQUFQLEtBQXFCLEtBQUtuYyxJQUExQztBQUNIOztBQUVELFVBQUksS0FBS2dsQixRQUFMLEtBQWtCLElBQXRCLEVBQTRCO0FBQ3hCenZCLG9CQUFZLENBQUNJLElBQWIsQ0FBa0IsZ0JBQWxCLEVBQW9DLEtBQUtvdkIsTUFBekM7QUFDSDtBQUNKLEtBVkQsTUFVTztBQUNILFdBQUtDLFFBQUwsR0FBZ0IsS0FBS0YsUUFBckI7QUFDSDtBQUNKLEdBL0NnQjs7QUFpRGpCdnRCLFNBQU8sRUFBRTtBQUNMMnRCLFlBQVEsQ0FBQ0gsTUFBRCxFQUFTO0FBQ2IsVUFBSSxLQUFLeHJCLElBQUwsS0FBY3dyQixNQUFsQixFQUEwQjtBQUN0QixhQUFLRSxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsYUFBS0QsUUFBTCxHQUFnQixJQUFoQjtBQUNIO0FBQ0o7O0FBTkk7QUFqRFEsQ0FBckIsRTs7Ozs7Ozs7Ozs7O0FDRkE7QUFBQTtBQUFBO0FBRUF0dkIsMkNBQUcsQ0FBQzRELFNBQUosQ0FBYyxNQUFkLEVBQXNCO0FBQ2xCRSxVQUFRLEVBQUUsT0FEUTtBQUVsQjNDLFlBQVUsRUFBRSxDQUFDLElBQUQsRUFBTyxJQUFQLENBRk07O0FBR2xCaEIsTUFBSSxHQUFHO0FBQ0gsV0FBTztBQUFFc3ZCLFVBQUksRUFBRTtBQUFSLEtBQVA7QUFDSCxHQUxpQjs7QUFPbEIvYixTQUFPLEdBQUc7QUFDTixTQUFLK2IsSUFBTCxHQUFZLEtBQUtDLFNBQWpCO0FBQ0gsR0FUaUI7O0FBV2xCN3RCLFNBQU8sRUFBRTtBQUNMOHRCLGFBQVMsQ0FBQ0MsV0FBRCxFQUFjO0FBQ25CLFdBQUtILElBQUwsQ0FBVS9zQixPQUFWLENBQWtCbXRCLEdBQUcsSUFBSTtBQUNyQkEsV0FBRyxDQUFDUCxRQUFKLEdBQWdCTyxHQUFHLENBQUN2bEIsSUFBSixLQUFhc2xCLFdBQVcsQ0FBQ3RsQixJQUF6QztBQUNILE9BRkQ7QUFHSDs7QUFMSTtBQVhTLENBQXRCLEU7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUNBLE1BQU13bEIsT0FBTyxHQUFHLHlDQUFoQjtBQUNBLE1BQU1DLGFBQWEsR0FBRyxlQUF0QjtBQUVBLElBQUlDLFdBQVcsR0FBRyxDQUFDLENBQUNwd0IsTUFBTSxDQUFDa1AsTUFBM0I7QUFDQSxJQUFJbWhCLGtCQUFKO0FBQ0EsSUFBSUMsaUJBQUosQyxDQUNBO0FBQ0E7O0FBQ0EsTUFBTUMsV0FBVyxHQUFHLElBQUl0UCxPQUFKLENBQVksQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLEtBQXFCO0FBQ2pEa1Asb0JBQWtCLEdBQUduUCxPQUFyQjtBQUNBb1AsbUJBQWlCLEdBQUduUCxNQUFwQjtBQUNILENBSG1CLENBQXBCO0FBS2UsU0FBUzFGLElBQVQsR0FBZ0I7QUFDM0I7QUFDQTtBQUNBO0FBQ0EsTUFBSTJVLFdBQUosRUFBaUIsT0FBT0csV0FBUDtBQUVqQkgsYUFBVyxHQUFHLElBQWQsQ0FOMkIsQ0FPM0I7QUFDQTtBQUNBOztBQUNBcHdCLFFBQU0sQ0FBQ213QixhQUFELENBQU4sR0FBd0IsTUFBTUUsa0JBQWtCLENBQUNyd0IsTUFBTSxDQUFDa1AsTUFBUixDQUFoRCxDQVYyQixDQVkzQjtBQUNBO0FBQ0E7OztBQUNBLFFBQU1zaEIsTUFBTSxHQUFHNXRCLFFBQVEsQ0FBQ3VXLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBZjtBQUNBcVgsUUFBTSxDQUFDM3FCLEtBQVAsR0FBZSxJQUFmO0FBQ0EycUIsUUFBTSxDQUFDQyxLQUFQLEdBQWUsSUFBZjtBQUNBRCxRQUFNLENBQUMvUSxHQUFQLEdBQWMsK0NBQThDeVEsT0FBUSxhQUFZQyxhQUFjLEVBQTlGO0FBQ0FLLFFBQU0sQ0FBQ0UsT0FBUCxHQUFpQkosaUJBQWpCO0FBQ0ExdEIsVUFBUSxDQUFDVyxhQUFULENBQXVCLE1BQXZCLEVBQStCdWlCLFdBQS9CLENBQTJDMEssTUFBM0M7QUFFQSxTQUFPRCxXQUFQO0FBQ0gsQyIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCJpbXBvcnQgJy4uL2Nzcy9hcHAuc2Nzcyc7XHJcblxyXG5pbXBvcnQgJCBmcm9tICdqcXVlcnknO1xyXG5cclxuaW1wb3J0ICcuL2Jvb3RzdHJhcCc7XHJcbmltcG9ydCAnLi9jdXN0b20nO1xyXG5cclxuaW1wb3J0ICdib290c3RyYXAnO1xyXG5cclxuaW1wb3J0ICdib290c3RyYXAtZGF0ZXBpY2tlcic7XHJcbmltcG9ydCAnYm9vdHN0cmFwLWRhdGVwaWNrZXIvZGlzdC9jc3MvYm9vdHN0cmFwLWRhdGVwaWNrZXIuc3RhbmRhbG9uZS5jc3MnXHJcblxyXG5pbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcbmltcG9ydCAnLi9pbmZyYXN0cnVjdHVyZS9GbGFzaCc7XHJcbmltcG9ydCAnLi9pbmZyYXN0cnVjdHVyZS9GbGFzaGVzJztcclxuaW1wb3J0ICcuL2luZnJhc3RydWN0dXJlL1RhYic7XHJcbmltcG9ydCAnLi9pbmZyYXN0cnVjdHVyZS9UYWJzJztcclxuaW1wb3J0ICcuL2luZnJhc3RydWN0dXJlL01vZGFsJztcclxuaW1wb3J0ICcuL2luZnJhc3RydWN0dXJlL0Nhcm91c2VsJztcclxuaW1wb3J0ICcuL2luZnJhc3RydWN0dXJlL0RlbGV0ZUxpbmsnO1xyXG5pbXBvcnQgJy4vaW5mcmFzdHJ1Y3R1cmUvRGVsZXRlTW9kdWxlSXRlbSc7XHJcbmltcG9ydCAnLi9pbmZyYXN0cnVjdHVyZS9Qb3N0TGluayc7XHJcbmltcG9ydCAnLi9pbmZyYXN0cnVjdHVyZS9JbmxpbmVWYWx1ZUVkaXQnO1xyXG5pbXBvcnQgJy4vaW5mcmFzdHJ1Y3R1cmUvSW5saW5lQm9vbGVhbkVkaXQnO1xyXG5pbXBvcnQgJy4vaW5mcmFzdHJ1Y3R1cmUvRmlsZVVwbG9hZEJ1dHRvbic7XHJcbmltcG9ydCAnLi9pbmZyYXN0cnVjdHVyZS9GaWxlc0xpc3QnO1xyXG5pbXBvcnQgJy4vaW5mcmFzdHJ1Y3R1cmUvU2xpZGVyJztcclxuaW1wb3J0ICcuL2NvbXBvbmVudHMvRW50aXR5VHJhbnNsYXRpb24nO1xyXG5pbXBvcnQgJy4vY29tcG9uZW50cy9FbnRpdHlUcmFuc2xhdGlvbnNGb3JtJztcclxuaW1wb3J0ICcuL2NvbXBvbmVudHMvQ3JlZGl0Q2FsY3VsYXRvcic7XHJcbmltcG9ydCAnLi9jb21wb25lbnRzL0NyZWRpdE9mZmVyJztcclxuaW1wb3J0ICcuL2NvbXBvbmVudHMvQ3JlZGl0Q29tcGFyaXNpb24nO1xyXG5pbXBvcnQgJy4vY29tcG9uZW50cy9Qb2xsQW5zd2VyJztcclxuaW1wb3J0ICcuL2NvbXBvbmVudHMvUG9sbFF1ZXN0aW9uJztcclxuaW1wb3J0ICcuL2NvbXBvbmVudHMvUG9sbCc7XHJcbmltcG9ydCAnLi9jb21wb25lbnRzL0dtYXBzJztcclxuaW1wb3J0ICcuL2NvbXBvbmVudHMvU2xpZGVyTW9kdWxlJztcclxuaW1wb3J0ICcuL2NvbXBvbmVudHMvUG9sbFBhcnRpY2lwYXRpb24nO1xyXG5pbXBvcnQgJy4vY29tcG9uZW50cy9JbWFnZUdhbGxlcnknO1xyXG5pbXBvcnQgJy4vY29tcG9uZW50cy9Db29raWVEZWNsYXJhdGlvbic7XHJcbmltcG9ydCAnLi9jb21wb25lbnRzL1pvcGltQ2hhdCc7XHJcbmltcG9ydCAnLi9jb21wb25lbnRzL0NyZWRpdFJlcGF5bWVudCc7XHJcbmltcG9ydCAnLi9jb21wb25lbnRzL0NyZWRpdFJlZmluYW5jZSc7XHJcbmltcG9ydCAnLi9jb21wb25lbnRzL0NyZWRpdFJlcGF5bWVudEFtb3VudEZvcm0nO1xyXG5pbXBvcnQgJy4vY29tcG9uZW50cy9Gcm9udGVuZFZhbGlkYXRpb24nO1xyXG5pbXBvcnQgJy4vY29tcG9uZW50cy9BcHBseU9ubGluZUJ1dHRvbic7XHJcbmltcG9ydCAnLi9jb21wb25lbnRzL1RlbGVwaG9uZUFwcGxpY2F0aW9uTm90QWxsb3dlZCc7XHJcbmltcG9ydCAnLi9jb21wb25lbnRzL1JlZGlyZWN0VGltZXInO1xyXG5pbXBvcnQgJy4vY29tcG9uZW50cy9UcmFuc2xhdGlvbkl0ZW0nO1xyXG5pbXBvcnQgJy4vY29tcG9uZW50cy9UcmFuc2xhdGlvbnNMaXN0JztcclxuXHJcbmltcG9ydCB7QXBwbGljYXRpb259IGZyb20gXCJzdGltdWx1c1wiXHJcbmltcG9ydCB7ZGVmaW5pdGlvbnNGcm9tQ29udGV4dH0gZnJvbSBcInN0aW11bHVzL3dlYnBhY2staGVscGVyc1wiXHJcblxyXG53aW5kb3cuRXZlbnRNYW5hZ2VyID0gbmV3IGNsYXNzIHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMudnVlID0gbmV3IFZ1ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGZpcmUoZXZlbnQsIGRhdGEgPSBudWxsKSB7XHJcbiAgICAgICAgdGhpcy52dWUuJGVtaXQoZXZlbnQsIGRhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIGxpc3RlbihldmVudCwgY2FsbGJhY2spIHtcclxuICAgICAgICB0aGlzLnZ1ZS4kb24oZXZlbnQsIGNhbGxiYWNrKTtcclxuICAgIH1cclxufTtcclxuXHJcbndpbmRvdy5mbGFzaCA9IGZ1bmN0aW9uIChtZXNzYWdlLCBsZXZlbCA9ICdzdWNjZXNzJykge1xyXG4gICAgd2luZG93LkV2ZW50TWFuYWdlci5maXJlKCdmbGFzaCcsIHttZXNzYWdlLCBsZXZlbH0pO1xyXG59O1xyXG5cclxuY29uc3QgYXBwbGljYXRpb24gPSBBcHBsaWNhdGlvbi5zdGFydCgpO1xyXG5jb25zdCBjb250ZXh0ID0gcmVxdWlyZS5jb250ZXh0KFwiLi9jb250cm9sbGVyc1wiLCB0cnVlLCAvXFwuanMkLyk7XHJcbmFwcGxpY2F0aW9uLmxvYWQoZGVmaW5pdGlvbnNGcm9tQ29udGV4dChjb250ZXh0KSk7XHJcblxyXG5WdWUub3B0aW9ucy5kZWxpbWl0ZXJzID0gWydbWycsICddXSddO1xyXG5WdWUuY29uZmlnLmlnbm9yZWRFbGVtZW50cyA9IFsneC10cmFucyddO1xyXG5cclxuY29uc3QgYXBwID0gbmV3IFZ1ZSh7XHJcbiAgICBlbDogJyNhcHAnLFxyXG4gICAgZGF0YToge1xyXG4gICAgICAgIHNob3dNb2RhbDogZmFsc2UsXHJcbiAgICAgICAgZHluYW1pY0NvbXBvbmVudHM6IFtdXHJcbiAgICB9LFxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgICBFdmVudE1hbmFnZXIubGlzdGVuKCdhZGQtY29tcG9uZW50JywgdGhpcy5hZGRDb21wb25lbnQpO1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ3JlbW92ZS1jb21wb25lbnQnLCB0aGlzLnJlbW92ZUNvbXBvbmVudCk7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGFkZENvbXBvbmVudChkYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZHluYW1pY0NvbXBvbmVudHMucHVzaChkYXRhKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICByZW1vdmVDb21wb25lbnQoY29tcG9uZW50SWQpIHtcclxuICAgICAgICAgICAgbGV0IGluZGV4ID0gdGhpcy5keW5hbWljQ29tcG9uZW50cy5maW5kSW5kZXgoaXRlbSA9PiBpdGVtLmlkID09PSBjb21wb25lbnRJZCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmR5bmFtaWNDb21wb25lbnRzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgfSxcclxuICAgIH1cclxufSk7XHJcblxyXG4kKGZ1bmN0aW9uICgpIHtcclxuICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKClcclxufSk7XHJcblxyXG5sZXQgZWxlbWVudHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCdpbnB1dCwgc2VsZWN0LCB0ZXh0YXJlYScpO1xyXG5cclxuZWxlbWVudHMuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignaW52YWxpZCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBlbGVtZW50LnNjcm9sbEludG9WaWV3KGZhbHNlKTtcclxuICAgIH0pO1xyXG59KTtcclxuXHJcbiQoZnVuY3Rpb24oKSB7XHJcbiAgICAkKFwiZm9ybVwiKS5zdWJtaXQoZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICAkKHRoaXMpLmZpbmQoJ2J1dHRvblt0eXBlPVwic3VibWl0XCJdJykucHJvcChcImRpc2FibGVkXCIsIHRydWUpO1xyXG4gICAgfSk7XHJcbn0pOyIsImxldCB0b2tlbiA9IGRvY3VtZW50LmhlYWQucXVlcnlTZWxlY3RvcignbWV0YVtuYW1lPVwiY3NyZi10b2tlblwiXScpO1xyXG5cclxuaWYgKHRva2VuKSB7XHJcbiAgICAkLmFqYXhTZXR1cCh7XHJcbiAgICAgICAgYmVmb3JlU2VuZDogZnVuY3Rpb24gKHhociwgc2V0dGluZ3MpIHtcclxuICAgICAgICAgICAgeGhyLnNldFJlcXVlc3RIZWFkZXIoXCJYLUNTUkZUb2tlblwiLCB0b2tlbi5jb250ZW50KTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufSBlbHNlIHtcclxuICAgIGNvbnNvbGUuZXJyb3IoJ0NTUkYgdG9rZW4gbm90IGZvdW5kLicpO1xyXG59IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgnYXBwbHktb25saW5lLWJ1dHRvbicsIHtcclxuICAgIG5hbWU6IFwiQXBwbHlPbmxpbmVCdXR0b25cIixcclxuICAgIGRlbGltaXRlcnM6IFsnW1snLCAnXV0nXSxcclxuICAgIHRlbXBsYXRlOiAnI2FwcGx5LW9ubGluZS1idXR0b24nLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBjaGVja0N1c3RvbWVySXNBbGxvd2VkVXJsOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIHJlZGlyZWN0VXJsOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmdcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgY2hlY2tpbmc6IGZhbHNlXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGlzQ2hlY2tpbmcoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNoZWNraW5nID09PSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGNoZWNrQ3VzdG9tZXJJc0RlbmllZCgpIHtcclxuICAgICAgICAgICAgbGV0IF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgIHRoaXMuY2hlY2tpbmcgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ2dldCcsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHRoaXMuY2hlY2tDdXN0b21lcklzQWxsb3dlZFVybCxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5jaGVja2luZyA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuaXNEZW5pZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJCgnIycgKyBfc2VsZi4kcmVmcy5tb2RhbC4kZWwuZ2V0QXR0cmlidXRlKCdpZCcpKS5tb2RhbCgnc2hvdycpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZXBsYWNlKF9zZWxmLnJlZGlyZWN0VXJsKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5jaGVja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdjb29raWUtZGVjbGFyYXRpb24nLCB7XHJcbiAgICB0ZW1wbGF0ZTogYDxkaXYgaWQ9XCJDb29raWVEZWNsYXJhdGlvblwiPjwvZGl2PmAsXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIHRoaXMubG9hZEpzKCdodHRwczovL2NvbnNlbnQuY29va2llYm90LmNvbS9lMTVhYWY5Zi03Nzg4LTRhNDUtOTZlZS1jZDUxYWU2OTI2NTAvY2QuanMnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ0xvYWRlZCBjb29raWUgZGVjbGFyYXRpb24gc2NyaXB0LicpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBsb2FkSnModXJsLCBjYWxsYmFjaykge1xyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgICAgICBkYXRhVHlwZTogJ3NjcmlwdCcsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBjYWxsYmFjayxcclxuICAgICAgICAgICAgICAgIGFzeW5jOiB0cnVlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5pbXBvcnQgU2xpZGVyIGZyb20gJ2lvbi1yYW5nZXNsaWRlcic7XHJcbmltcG9ydCAnaW9uLXJhbmdlc2xpZGVyL2Nzcy9pb24ucmFuZ2VTbGlkZXIuY3NzJztcclxuXHJcblZ1ZS5jb21wb25lbnQoJ2NyZWRpdC1jYWxjdWxhdG9yJywge1xyXG4gICAgZGVsaW1pdGVyczogWydbWycsICddXSddLFxyXG4gICAgdGVtcGxhdGU6ICcjY3JlZGl0LWNhbGN1bGF0b3InLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBzdGFuZGFsb25lOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgZGVmYXVsdDogZmFsc2VcclxuICAgICAgICB9LFxyXG4gICAgICAgIGN1cnJlbmN5OiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAn0LPRgNC9LidcclxuICAgICAgICB9LFxyXG4gICAgICAgIGxvY2FsZToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgZGVmYXVsdDogJ3VhJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY3JlZGl0OiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBudWxsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjcmVkaXRBbW91bnQ6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IG51bGxcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNyZWRpdFBheW1lbnQ6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IG51bGxcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBpc1JlZnJlc2hpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICBhbW91bnRTdGVwczogW10sXHJcbiAgICAgICAgICAgIHBheW1lbnRTdGVwczogW10sXHJcbiAgICAgICAgICAgIHR5cGU6ICdwZXJzb25hbCcsXHJcbiAgICAgICAgICAgIGFtb3VudDogMjAwMC4wMCxcclxuICAgICAgICAgICAgcGF5bWVudDogMTIwLjAwLFxyXG4gICAgICAgICAgICBwYXltZW50V2l0aFBlbmFsdHk6IDEyMC4wMCxcclxuICAgICAgICAgICAgcGVyaW9kczogMzYsXHJcbiAgICAgICAgICAgIHRvdGFsOiA0MzIwLjAwLFxyXG4gICAgICAgICAgICB0b3RhbFdpdGhQZW5hbHR5OiA0MzIwLjAwLFxyXG4gICAgICAgICAgICBpbnRlcmVzdFJhdGVGaXJzdFBlcmlvZDogMy4wNTk5LFxyXG4gICAgICAgICAgICBpbnRlcmVzdFJhdGVTZWNvbmRQZXJpb2Q6IDAuMDEyOCxcclxuICAgICAgICAgICAgYXByYzoxMS4zODE3LFxyXG4gICAgICAgICAgICBwYXltZW50U2NoZWR1bGU6ICd3ZWVrbHknLFxyXG4gICAgICAgICAgICBwZW5zaW9uZXI6ICdubycsXHJcbiAgICAgICAgICAgIGNyZWRpdFRpdGxlOiAnJyxcclxuICAgICAgICAgICAgY3JlZGl0U2x1ZzogJycsXHJcbiAgICAgICAgICAgIHJlcXVpcmVtZW50czogJycsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgICAgIGNyZWRpdFByb2R1Y3RJZDogJycsXHJcbiAgICAgICAgICAgIGNyZWRpdFBlcmlvZElkOiAnJyxcclxuICAgICAgICAgICAgZXhwZW5zZXM6IDIwODJcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgICB0aGlzLmdldERhdGEodGhpcy5kZWZhdWx0UGFyYW1zKCksIHRydWUpO1xyXG5cclxuICAgICAgICBFdmVudE1hbmFnZXIubGlzdGVuKCdhbW91bnQtY2hhbmdlZCcsIHRoaXMuYW1vdW50Q2hhbmdlZCk7XHJcbiAgICAgICAgRXZlbnRNYW5hZ2VyLmxpc3RlbigncGF5bWVudC1jaGFuZ2VkJywgdGhpcy5wYXltZW50Q2hhbmdlZCk7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGRlZmF1bHRQYXJhbXMoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmNyZWRpdCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICBjcmVkaXQ6IHRoaXMuY3JlZGl0LFxyXG4gICAgICAgICAgICAgICAgICAgIGFtb3VudDogdGhpcy5jcmVkaXRBbW91bnQsXHJcbiAgICAgICAgICAgICAgICAgICAgcGF5bWVudDogdGhpcy5jcmVkaXRQYXltZW50XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBwZW5zaW9uZXI6ICdubycsXHJcbiAgICAgICAgICAgICAgICBwYXltZW50U2NoZWR1bGU6ICd3ZWVrbHknLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ3BlcnNvbmFsJyxcclxuICAgICAgICAgICAgICAgIGFtb3VudDogMjAwMC4wMFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0RGF0YShwYXJhbXMgPSB0aGlzLmRlZmF1bHRQYXJhbXMoKSwgY3JlZGl0SGFzQ2hhbmdlZCA9IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSAnL2VuL2NyZWRpdC1kYXRhPycgKyAkLnBhcmFtKHBhcmFtcyksXHJcbiAgICAgICAgICAgICAgICBfc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmlzUmVmcmVzaGluZyA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuYW1vdW50U3RlcHMgPSByZXNwb25zZS5hbW91bnRTdGVwcy5tYXAodmFsdWUgPT4gcGFyc2VGbG9hdCh2YWx1ZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnBheW1lbnRTdGVwcyA9IHJlc3BvbnNlLnBheW1lbnRTdGVwcy5tYXAodmFsdWUgPT4gcGFyc2VGbG9hdCh2YWx1ZSkpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5wYXltZW50U2NoZWR1bGUgPSByZXNwb25zZS5jcmVkaXREYXRhLmNyZWRpdC5wYXltZW50U2NoZWR1bGU7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYucGVuc2lvbmVyID0gcmVzcG9uc2UuY3JlZGl0RGF0YS5jcmVkaXQucGVuc2lvbmVyID09PSB0cnVlID8gJ3llcycgOiAnbm8nO1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnR5cGUgPSByZXNwb25zZS5jcmVkaXREYXRhLmNyZWRpdC50eXBlO1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmFtb3VudCA9IHJlc3BvbnNlLmNyZWRpdERhdGEuYW1vdW50O1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnBheW1lbnQgPSByZXNwb25zZS5jcmVkaXREYXRhLnBheW1lbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYucGF5bWVudFdpdGhQZW5hbHR5ID0gcmVzcG9uc2UuY3JlZGl0RGF0YS5wYXltZW50V2l0aFBlbmFsdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYucGVyaW9kcyA9IHJlc3BvbnNlLmNyZWRpdERhdGEucGVyaW9kcztcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi50b3RhbCA9IHJlc3BvbnNlLmNyZWRpdERhdGEudG90YWw7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYudG90YWxXaXRoUGVuYWx0eSA9IHJlc3BvbnNlLmNyZWRpdERhdGEudG90YWxXaXRoUGVuYWx0eTtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5pbnRlcmVzdFJhdGVGaXJzdFBlcmlvZCA9IHJlc3BvbnNlLmNyZWRpdERhdGEuaW50ZXJlc3RSYXRlRmlyc3RQZXJpb2Q7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuaW50ZXJlc3RSYXRlU2Vjb25kUGVyaW9kID0gcmVzcG9uc2UuY3JlZGl0RGF0YS5pbnRlcmVzdFJhdGVTZWNvbmRQZXJpb2Q7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuYXByYyA9IHJlc3BvbnNlLmNyZWRpdERhdGEuYXByYztcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5jcmVkaXRUaXRsZSA9IHJlc3BvbnNlLmNyZWRpdERhdGEuY3JlZGl0LnRpdGxlO1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmNyZWRpdFNsdWcgPSByZXNwb25zZS5jcmVkaXREYXRhLmNyZWRpdC5zbHVnO1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnJlcXVpcmVtZW50cyA9IHJlc3BvbnNlLmNyZWRpdERhdGEuY3JlZGl0LnJlcXVpcmVtZW50cztcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5kZWZhdWx0ID0gcmVzcG9uc2UuY3JlZGl0RGF0YS5jcmVkaXQuZGVmYXVsdEFtb3VudDtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5jcmVkaXRQcm9kdWN0SWQgPSByZXNwb25zZS5jcmVkaXREYXRhLnByb2R1Y3RJZDtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5jcmVkaXRQZXJpb2RJZCA9IHJlc3BvbnNlLmNyZWRpdERhdGEuY3JlZGl0UGVyaW9kSWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuZXhwZW5zZXMgPSByZXNwb25zZS5jcmVkaXREYXRhLnRvdGFsV2l0aFBlbmFsdHkgLSByZXNwb25zZS5jcmVkaXREYXRhLmFtb3VudDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuaXNSZWZyZXNoaW5nID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjcmVkaXRIYXNDaGFuZ2VkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF9zZWxmLmluaXRBbW91bnRTbGlkZXIoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmluaXRQYXltZW50U2xpZGVyKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIEV2ZW50TWFuYWdlci5maXJlKCdjcmVkaXQtcGFyYW1ldGVycy1jaGFuZ2VkJywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0OiBfc2VsZi5jcmVkaXRUaXRsZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2x1ZzogX3NlbGYuY3JlZGl0U2x1ZyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYW1vdW50OiBfc2VsZi5hbW91bnQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBlcmlvZDogX3NlbGYucGVyaW9kcyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGF5bWVudDogX3NlbGYucGF5bWVudCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3JlZGl0UHJvZHVjdElkOiBfc2VsZi5jcmVkaXRQcm9kdWN0SWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNyZWRpdFBlcmlvZElkOiBfc2VsZi5jcmVkaXRQZXJpb2RJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZW1lbnRzOiBfc2VsZi5yZXF1aXJlbWVudHNcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBhbW91bnRDaGFuZ2VkKHZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYW1vdW50ID0gdmFsdWUudG9GaXhlZCgyKTtcclxuXHJcbiAgICAgICAgICAgIGxldCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgYW1vdW50OiB0aGlzLmFtb3VudCxcclxuICAgICAgICAgICAgICAgIHBlbnNpb25lcjogdGhpcy5wZW5zaW9uZXIsXHJcbiAgICAgICAgICAgICAgICBwYXltZW50U2NoZWR1bGU6IHRoaXMucGF5bWVudFNjaGVkdWxlLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogdGhpcy50eXBlXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICB0aGlzLmdldERhdGEocGFyYW1ldGVycyk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgcGF5bWVudENoYW5nZWQodmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy5wYXltZW50V2l0aFBlbmFsdHkgPSB2YWx1ZS50b0ZpeGVkKDIpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICBwYXltZW50OiB0aGlzLnBheW1lbnRXaXRoUGVuYWx0eSxcclxuICAgICAgICAgICAgICAgIGFtb3VudDogdGhpcy5hbW91bnQsXHJcbiAgICAgICAgICAgICAgICBwZW5zaW9uZXI6IHRoaXMucGVuc2lvbmVyLFxyXG4gICAgICAgICAgICAgICAgcGF5bWVudFNjaGVkdWxlOiB0aGlzLnBheW1lbnRTY2hlZHVsZSxcclxuICAgICAgICAgICAgICAgIHR5cGU6IHRoaXMudHlwZVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5nZXREYXRhKHBhcmFtZXRlcnMpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHBheW1lbnRTY2hlZHVsZUNoYW5nZWQoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnBheW1lbnRTY2hlZHVsZSA9PT0gJ21vbnRobHknICYmIHRoaXMudHlwZSA9PT0gJ3BlcnNvbmFsJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wZW5zaW9uZXIgPSAnbm8nO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgIHBheW1lbnRTY2hlZHVsZTogdGhpcy5wYXltZW50U2NoZWR1bGUsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiB0aGlzLnR5cGUsXHJcbiAgICAgICAgICAgICAgICBwZW5zaW9uZXI6IHRoaXMucGVuc2lvbmVyLFxyXG4gICAgICAgICAgICAgICAgYW1vdW50OiB0aGlzLmRlZmF1bHRBbW91bnRGb3JQYXltZW50U2NoZWR1bGVcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0RGF0YShwYXJhbWV0ZXJzLCB0cnVlKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBwZW5zaW9uZXJDaGFuZ2VkKCkge1xyXG4gICAgICAgICAgICBsZXQgcGFyYW1ldGVycyA9IHt9O1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMucGVuc2lvbmVyID09PSAneWVzJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wYXltZW50U2NoZWR1bGUgPSAnbW9udGhseSc7XHJcblxyXG4gICAgICAgICAgICAgICAgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgICAgICBwYXltZW50U2NoZWR1bGU6IHRoaXMucGF5bWVudFNjaGVkdWxlLFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IHRoaXMudHlwZSxcclxuICAgICAgICAgICAgICAgICAgICBwZW5zaW9uZXI6IHRoaXMucGVuc2lvbmVyLFxyXG4gICAgICAgICAgICAgICAgICAgIGFtb3VudDogdGhpcy5kZWZhdWx0QW1vdW50Rm9yUGF5bWVudFNjaGVkdWxlXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wYXltZW50U2NoZWR1bGUgPSAnd2Vla2x5JztcclxuXHJcbiAgICAgICAgICAgICAgICBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHBheW1lbnRTY2hlZHVsZTogdGhpcy5wYXltZW50U2NoZWR1bGUsXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogdGhpcy50eXBlLFxyXG4gICAgICAgICAgICAgICAgICAgIHBlbnNpb25lcjogdGhpcy5wZW5zaW9uZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgYW1vdW50OiB0aGlzLmRlZmF1bHRBbW91bnRGb3JQYXltZW50U2NoZWR1bGVcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0RGF0YShwYXJhbWV0ZXJzLCB0cnVlKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjaGFuZ2VUeXBlKG5ld1R5cGUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudHlwZSA9PT0gbmV3VHlwZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnR5cGUgPSBuZXdUeXBlO1xyXG5cclxuICAgICAgICAgICAgaWYgKG5ld1R5cGUgPT09ICdidXNpbmVzcycpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucGVuc2lvbmVyID0gJ25vJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5wYXltZW50U2NoZWR1bGUgPSAnd2Vla2x5JztcclxuXHJcbiAgICAgICAgICAgIGxldCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgcGF5bWVudFNjaGVkdWxlOiB0aGlzLnBheW1lbnRTY2hlZHVsZSxcclxuICAgICAgICAgICAgICAgIHR5cGU6IHRoaXMudHlwZSxcclxuICAgICAgICAgICAgICAgIHBlbnNpb25lcjogdGhpcy5wZW5zaW9uZXJcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGlmIChuZXdUeXBlID09PSAnYnVzaW5lc3MnKSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbWV0ZXJzLmFtb3VudCA9IDE2MDAuMDA7XHJcbiAgICAgICAgICAgICAgICBwYXJhbWV0ZXJzLnBheW1lbnQgPSA5Mi4wMDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5nZXREYXRhKHBhcmFtZXRlcnMsIHRydWUpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGluaXRBbW91bnRTbGlkZXIoKSB7XHJcbiAgICAgICAgICAgIGxldCBhbW91bnRTbGlkZXIgPSAkKHRoaXMuJHJlZnMuYW1vdW50KS5kYXRhKFwiaW9uUmFuZ2VTbGlkZXJcIiksXHJcbiAgICAgICAgICAgICAgICBpbmRleCA9IHRoaXMuYW1vdW50U3RlcHMuZmluZEluZGV4KHZhbHVlID0+IHZhbHVlLnRvRml4ZWQoMikgPT09IHRoaXMuYW1vdW50KTtcclxuXHJcbiAgICAgICAgICAgIGlmIChhbW91bnRTbGlkZXIpIHtcclxuICAgICAgICAgICAgICAgIGFtb3VudFNsaWRlci5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICQodGhpcy4kcmVmcy5hbW91bnQpLmlvblJhbmdlU2xpZGVyKHtcclxuICAgICAgICAgICAgICAgIHNraW46ICdmbGF0JyxcclxuICAgICAgICAgICAgICAgIGdyaWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBncmlkX3NuYXA6IHRydWUsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZXM6IHRoaXMuYW1vdW50U3RlcHMsXHJcbiAgICAgICAgICAgICAgICBmcm9tOiBpbmRleCxcclxuICAgICAgICAgICAgICAgIHBvc3RmaXg6ICcgJyArIHRoaXMuY3VycmVuY3ksXHJcbiAgICAgICAgICAgICAgICBmb3JjZV9lZGdlczogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaDogZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICBFdmVudE1hbmFnZXIuZmlyZSgnYW1vdW50LWNoYW5nZWQnLCBkYXRhLmZyb21fdmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGFtb3VudFNsaWRlciA9ICQodGhpcy4kcmVmcy5hbW91bnQpLmRhdGEoXCJpb25SYW5nZVNsaWRlclwiKTtcclxuICAgICAgICAgICAgYW1vdW50U2xpZGVyLnVwZGF0ZSh7ZnJvbTogaW5kZXh9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBpbml0UGF5bWVudFNsaWRlcigpIHtcclxuICAgICAgICAgICAgbGV0IHBheW1lbnRTbGlkZXIgPSAkKHRoaXMuJHJlZnMucGF5bWVudCkuZGF0YShcImlvblJhbmdlU2xpZGVyXCIpLFxyXG4gICAgICAgICAgICAgICAgYWRqdXN0ZWRQYXltZW50U3RlcHMgPSB0aGlzLnBheW1lbnRTdGVwcztcclxuXHJcbiAgICAgICAgICAgIGlmIChwYXltZW50U2xpZGVyKSB7XHJcbiAgICAgICAgICAgICAgICBwYXltZW50U2xpZGVyLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGFkanVzdGVkUGF5bWVudFN0ZXBzLmxlbmd0aCA9PT0gMSkge1xyXG4gICAgICAgICAgICAgICAgYWRqdXN0ZWRQYXltZW50U3RlcHMucHVzaChhZGp1c3RlZFBheW1lbnRTdGVwc1swXSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBpbmRleCA9IHRoaXMucGF5bWVudFN0ZXBzLmZpbmRJbmRleCh2YWx1ZSA9PiB2YWx1ZS50b0ZpeGVkKDIpID09PSB0aGlzLnBheW1lbnRXaXRoUGVuYWx0eSk7XHJcblxyXG4gICAgICAgICAgICAkKHRoaXMuJHJlZnMucGF5bWVudCkuaW9uUmFuZ2VTbGlkZXIoe1xyXG4gICAgICAgICAgICAgICAgc2tpbjogJ2ZsYXQnLFxyXG4gICAgICAgICAgICAgICAgZ3JpZDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGdyaWRfc25hcDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIHZhbHVlczogYWRqdXN0ZWRQYXltZW50U3RlcHMsXHJcbiAgICAgICAgICAgICAgICBmcm9tOiBpbmRleCxcclxuICAgICAgICAgICAgICAgIHBvc3RmaXg6ICcgJyArIHRoaXMuY3VycmVuY3ksXHJcbiAgICAgICAgICAgICAgICBmb3JjZV9lZGdlczogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBvbkZpbmlzaDogZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICBFdmVudE1hbmFnZXIuZmlyZSgncGF5bWVudC1jaGFuZ2VkJywgZGF0YS5mcm9tX3ZhbHVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBwYXltZW50U2xpZGVyID0gJCh0aGlzLiRyZWZzLnBheW1lbnQpLmRhdGEoXCJpb25SYW5nZVNsaWRlclwiKTtcclxuICAgICAgICAgICAgcGF5bWVudFNsaWRlci51cGRhdGUoe2Zyb206IGluZGV4fSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY3JlYXRlT2ZmZXIoKSB7XHJcbiAgICAgICAgICAgIEV2ZW50TWFuYWdlci5maXJlKCdjcmVhdGUtY3JlZGl0LW9mZmVyJywge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogdGhpcy50eXBlLFxyXG4gICAgICAgICAgICAgICAgYW1vdW50OiB0aGlzLmFtb3VudCxcclxuICAgICAgICAgICAgICAgIHBheW1lbnQ6IHRoaXMucGF5bWVudFdpdGhQZW5hbHR5LFxyXG4gICAgICAgICAgICAgICAgcGF5bWVudFdpdGhQZW5hbHR5OiB0aGlzLnBheW1lbnRXaXRoUGVuYWx0eSxcclxuICAgICAgICAgICAgICAgIHBlcmlvZHM6IHRoaXMucGVyaW9kcyxcclxuICAgICAgICAgICAgICAgIGR1cmF0aW9uOiB0aGlzLmR1cmF0aW9uLFxyXG4gICAgICAgICAgICAgICAgdG90YWw6IHRoaXMudG90YWxXaXRoUGVuYWx0eSxcclxuICAgICAgICAgICAgICAgIHRvdGFsV2l0aFBlbmFsdHk6IHRoaXMudG90YWxXaXRoUGVuYWx0eSxcclxuICAgICAgICAgICAgICAgIHBheW1lbnRTY2hlZHVsZTogdGhpcy5wYXltZW50U2NoZWR1bGUsXHJcbiAgICAgICAgICAgICAgICBpbnRlcmVzdFJhdGVGaXJzdFBlcmlvZDogdGhpcy5pbnRlcmVzdEZpcnN0UGVyaW9kLFxyXG4gICAgICAgICAgICAgICAgaW50ZXJlc3RSYXRlU2Vjb25kUGVyaW9kOiB0aGlzLmludGVyZXN0U2Vjb25kUGVyaW9kLFxyXG4gICAgICAgICAgICAgICAgYXByYzogdGhpcy5pbnRlcmVzdEFQUkMsXHJcbiAgICAgICAgICAgICAgICBleHBlbnNlczogdGhpcy5leHBlbnNlcyxcclxuICAgICAgICAgICAgICAgIHBlbnNpb25lcjogdGhpcy5wZW5zaW9uZXIsXHJcbiAgICAgICAgICAgICAgICB0aXRsZTogdGhpcy5jcmVkaXRUaXRsZSxcclxuICAgICAgICAgICAgICAgIHNsdWc6IHRoaXMuY3JlZGl0U2x1ZyxcclxuICAgICAgICAgICAgICAgIHJlcXVpcmVtZW50czogdGhpcy5yZXF1aXJlbWVudHMsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGFwcGx5KCkge1xyXG4gICAgICAgICAgICBsZXQgdXJsID0gYC9nZXQtYXBwbGljYXRpb24tcm91dGUvJHt0aGlzLmNyZWRpdFNsdWd9LyR7dGhpcy5sb2NhbGV9YCxcclxuICAgICAgICAgICAgICAgIF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGAke3Jlc3BvbnNlfS8/YW1vdW50PSR7X3NlbGYuYW1vdW50fSZwYXltZW50PSR7X3NlbGYucGF5bWVudH0mcGVyaW9kPSR7X3NlbGYucGVyaW9kc31gO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGR1cmF0aW9uKCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5wYXltZW50U2NoZWR1bGUgPT09ICd3ZWVrbHknKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gTWF0aC5jZWlsKHRoaXMucGVyaW9kcyAvIDQuMzMpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucGF5bWVudFNjaGVkdWxlID09PSAnYml3ZWVrbHknKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gTWF0aC5jZWlsKHRoaXMucGVyaW9kcyAvIDIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucGVyaW9kcztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGludGVyZXN0Rmlyc3RQZXJpb2QoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAodGhpcy5pbnRlcmVzdFJhdGVGaXJzdFBlcmlvZCAqIDEwMCkudG9GaXhlZCgyKSArICclJztcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBpbnRlcmVzdFNlY29uZFBlcmlvZCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuICh0aGlzLmludGVyZXN0UmF0ZVNlY29uZFBlcmlvZCAqIDEwMCkudG9GaXhlZCgyKSArICclJztcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBpbnRlcmVzdEFQUkMoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAodGhpcy5hcHJjICogMTAwKS50b0ZpeGVkKDIpICsgJyUnO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGRlZmF1bHRBbW91bnQoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHQgPT09IG51bGwgPyB0aGlzLmFtb3VudCA6IHRoaXMuZGVmYXVsdDtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGRlZmF1bHRBbW91bnRGb3JQYXltZW50U2NoZWR1bGUoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnBheW1lbnRTY2hlZHVsZSA9PT0gJ3dlZWtseScpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAyMDAwLjAwO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5wYXltZW50U2NoZWR1bGUgPT09ICdiaXdlZWtseScpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAyMDAwLjAwO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5wYXltZW50U2NoZWR1bGUgPT09ICdtb250aGx5Jykge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucGVuc2lvbmVyID09PSAnbm8nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDIwMDAuMDA7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAxMDAwLjAwO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjb250YWluZXJDbGFzcygpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICdjb2wtbWQtNic6IHRoaXMuc3RhbmRhbG9uZSA9PT0gZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAnY29sLXhsLTQnOiB0aGlzLnN0YW5kYWxvbmUgPT09IGZhbHNlXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGJvdHRvbUNsYXNzKCkge1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgJ3N0YW5kYWxvbmUnOiB0aGlzLnN0YW5kYWxvbmUgPT09IHRydWVcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAgIHR5cGU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgRXZlbnRNYW5hZ2VyLmZpcmUoJ2NyZWRpdC10eXBlLWNoYW5nZWQnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuaW1wb3J0IFNsaWRlciBmcm9tICdpb24tcmFuZ2VzbGlkZXInO1xyXG5pbXBvcnQgJ2lvbi1yYW5nZXNsaWRlci9jc3MvaW9uLnJhbmdlU2xpZGVyLmNzcyc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdjcmVkaXQtY29tcGFyaXNvbicsIHtcclxuICAgIGRlbGltaXRlcnM6IFsnW1snLCAnXV0nXSxcclxuICAgIHRlbXBsYXRlOiAnI2NyZWRpdC1jb21wYXJpc29uJyxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgcGF5bWVudFNjaGVtZVZpc2libGU6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBCb29sZWFuLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiB0cnVlXHJcbiAgICAgICAgfSxcclxuICAgICAgICBwZW5zaW9uZXJWaXNpYmxlOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgZGVmYXVsdDogdHJ1ZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaW50ZXJlc3RSYXRlVmlzaWJsZToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IEJvb2xlYW4sXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IHRydWVcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBvZmZlcnM6IFtdXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgRXZlbnRNYW5hZ2VyLmxpc3RlbignY3JlYXRlLWNyZWRpdC1vZmZlcicsIHRoaXMuY3JlYXRlT2ZmZXIpO1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ3JlbW92ZS1jcmVkaXQtb2ZmZXInLCB0aGlzLnJlbW92ZU9mZmVyKTtcclxuICAgICAgICBFdmVudE1hbmFnZXIubGlzdGVuKCdjcmVkaXQtdHlwZS1jaGFuZ2VkJywgdGhpcy5yZW1vdmVPZmZlcik7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGNyZWF0ZU9mZmVyKGRhdGEpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMub2ZmZXJzLmxlbmd0aCA+PSAyKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMub2ZmZXJzLnB1c2goZGF0YSlcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICByZW1vdmVPZmZlcihpbmRleCkge1xyXG4gICAgICAgICAgICBpZiAoaW5kZXggPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMub2ZmZXJzID0gW107XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMub2ZmZXJzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcbmltcG9ydCBTbGlkZXIgZnJvbSAnaW9uLXJhbmdlc2xpZGVyJztcclxuaW1wb3J0ICdpb24tcmFuZ2VzbGlkZXIvY3NzL2lvbi5yYW5nZVNsaWRlci5jc3MnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgnY3JlZGl0LW9mZmVyJywge1xyXG4gICAgZGVsaW1pdGVyczogWydbWycsICddXSddLFxyXG4gICAgdGVtcGxhdGU6ICcjY3JlZGl0LW9mZmVyJyxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgaW5kZXg6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHR5cGU6IE51bWJlclxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY3VycmVuY3k6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICfQs9GA0L0uJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbG9jYWxlOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAndWEnXHJcbiAgICAgICAgfSxcclxuICAgICAgICB0eXBlOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAncGVyc29uYWwnXHJcbiAgICAgICAgfSxcclxuICAgICAgICBhbW91bnQ6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IDBcclxuICAgICAgICB9LFxyXG4gICAgICAgIHBheW1lbnQ6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IDBcclxuICAgICAgICB9LFxyXG4gICAgICAgIHBheW1lbnRXaXRoUGVuYWx0eToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgZGVmYXVsdDogMFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgcGVyaW9kczoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IE51bWJlcixcclxuICAgICAgICAgICAgZGVmYXVsdDogMFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZHVyYXRpb246IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBOdW1iZXIsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IDBcclxuICAgICAgICB9LFxyXG4gICAgICAgIHRvdGFsOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAwXHJcbiAgICAgICAgfSxcclxuICAgICAgICB0b3RhbFdpdGhQZW5hbHR5OiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAwXHJcbiAgICAgICAgfSxcclxuICAgICAgICBpbnRlcmVzdFJhdGVGaXJzdFBlcmlvZDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgZGVmYXVsdDogMFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaW50ZXJlc3RSYXRlU2Vjb25kUGVyaW9kOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAwXHJcbiAgICAgICAgfSxcclxuICAgICAgICBhcHJjOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZXhwZW5zZXM6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBOdW1iZXIsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICcnXHJcbiAgICAgICAgfSxcclxuICAgICAgICBwYXltZW50U2NoZWR1bGU6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICd3ZWVrbHknXHJcbiAgICAgICAgfSxcclxuICAgICAgICBwZW5zaW9uZXI6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICdubydcclxuICAgICAgICB9LFxyXG4gICAgICAgIHRpdGxlOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnaUNyZWRpdCdcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNsdWc6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICdpY3JlZGl0J1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgcmVxdWlyZW1lbnRzOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnb25seSBJRCBjYXJkIG5lZWRlZCdcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGNsb3NlKCkge1xyXG4gICAgICAgICAgICBFdmVudE1hbmFnZXIuZmlyZSgncmVtb3ZlLWNyZWRpdC1vZmZlcicsIHRoaXMuaW5kZXgpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGFwcGx5KCkge1xyXG4gICAgICAgICAgICBsZXQgdXJsID0gYC9nZXQtYXBwbGljYXRpb24tcm91dGUvJHt0aGlzLnNsdWd9LyR7dGhpcy5sb2NhbGV9YCxcclxuICAgICAgICAgICAgICAgIF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGAke3Jlc3BvbnNlfS8/YW1vdW50PSR7X3NlbGYuYW1vdW50fSZwYXltZW50PSR7X3NlbGYucGF5bWVudH0mcGVyaW9kPSR7X3NlbGYucGVyaW9kc31gO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuXHJcblZ1ZS5jb21wb25lbnQoJ2NyZWRpdC1yZWZpbmFuY2UnLCB7XHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICB0ZW1wbGF0ZTogJyNjcmVkaXQtcmVmaW5hbmNlJyxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgY29udHJhY3Q6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHR5cGU6IE51bWJlclxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJlZmluYW5jZVN1bTogbnVsbFxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgZ2V0UmVmaW5hbmNlU3VtKCkge1xyXG4gICAgICAgICAgICBsZXQgdXJsID0gYC9jdXN0b21lci1wcm9maWxlLyR7dGhpcy5jb250cmFjdH0vY2FsY3VsYXRlLXJlZmluYW5jZWAsXHJcbiAgICAgICAgICAgICAgICBfc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnR0VUJyxcclxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzcyhyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnJlZmluYW5jZVN1bSA9IHJlc3BvbnNlLnJlZmluYW5jZVN1bTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcihlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGZsYXNoKGVycm9yWydyZXNwb25zZUpTT04nXSwgJ2Vycm9yJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGZvcm1hdHRlZFJlZmluYW5jZVN1bSgpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucmVmaW5hbmNlU3VtID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnJlZmluYW5jZVN1bSA+IDApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBgJHt0aGlzLnJlZmluYW5jZVN1bS50b0ZpeGVkKDIpfSBVQUhgO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzdGF0ZSgpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucmVmaW5hbmNlU3VtID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ2luaXRpYWwnO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5yZWZpbmFuY2VTdW0gPiAwKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ3JlZmluYW5jZSc7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnJlZmluYW5jZVN1bSA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdyZWZpbmFuY2Vfbm90X3Bvc3NpYmxlJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMucmVmaW5hbmNlU3VtID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdlcnJvcic7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiAnaW5pdGlhbCc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdjcmVkaXQtcmVwYXltZW50Jywge1xyXG4gICAgZGVsaW1pdGVyczogWydbWycsICddXSddLFxyXG4gICAgdGVtcGxhdGU6ICcjY3JlZGl0LXJlcGF5bWVudCcsXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIGNvbnRyYWN0OiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBOdW1iZXJcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICByZXBheW1lbnRTdW06IG51bGxcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGdldFJlcGF5bWVudFN1bSgpIHtcclxuICAgICAgICAgICAgbGV0IHVybCA9IGAvY3VzdG9tZXItcHJvZmlsZS8ke3RoaXMuY29udHJhY3R9L2NhbGN1bGF0ZS1yZXBheW1lbnRgLFxyXG4gICAgICAgICAgICAgICAgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5yZXBheW1lbnRTdW0gPSByZXNwb25zZS5yZXBheW1lbnRTdW07XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBmbGFzaChlcnJvclsncmVzcG9uc2VKU09OJ10sICdlcnJvcicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHJlZGlyZWN0Rm9yUGF5bWVudCgpIHtcclxuICAgICAgICAgICAgbGV0IHVybCA9IGAvY3VzdG9tZXItcHJvZmlsZS9yZXBheW1lbnQtdXJsYCxcclxuICAgICAgICAgICAgICAgIGRhdGEgPSB7Y29udHJhY3Q6IHRoaXMuY29udHJhY3QsIGFtb3VudDogdGhpcy5yZXBheW1lbnRTdW0sIGlzRWFybHlSZXBheW1lbnQ6IHRydWV9O1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzcyhyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gcmVzcG9uc2UucmVwYXltZW50VXJsO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxhc2goZXJyb3JbJ3Jlc3BvbnNlSlNPTiddLCAnZXJyb3InKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgZm9ybWF0dGVkUmVwYXltZW50U3VtKCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5yZXBheW1lbnRTdW0gPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMucmVwYXltZW50U3VtID4gMCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGAke3RoaXMucmVwYXltZW50U3VtLnRvRml4ZWQoMil9IFVBSGA7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICB9LFxyXG4gICAgICAgIHN0YXRlKCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5yZXBheW1lbnRTdW0gPT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnaW5pdGlhbCc7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnJlcGF5bWVudFN1bSA+IDApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAncGF5JztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMucmVwYXltZW50U3VtID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ3JlcGF5bWVudF9ub3RfcG9zc2libGUnO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5yZXBheW1lbnRTdW0gPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ2Vycm9yJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuICdpbml0aWFsJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuXHJcblZ1ZS5jb21wb25lbnQoJ2NyZWRpdC1yZXBheW1lbnQtYW1vdW50LWZvcm0nLCB7XHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICB0ZW1wbGF0ZTogJyNjcmVkaXQtcmVwYXltZW50LWFtb3VudC1mb3JtJyxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgaW5pdGlhbEFtb3VudDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IE51bWJlcixcclxuICAgICAgICAgICAgZGVmYXVsdDogMFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY29udHJhY3Q6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZ1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGFtb3VudDogMC4wMCxcclxuICAgICAgICAgICAgZXJyb3JzOiBbXVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIHRoaXMuYW1vdW50ID0gdGhpcy5pbml0aWFsQW1vdW50LnRvRml4ZWQoMik7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHN1Ym1pdCgpIHtcclxuICAgICAgICAgICAgbGV0IHVybCA9IGAvY3VzdG9tZXItcHJvZmlsZS9yZXBheW1lbnQtdXJsYCxcclxuICAgICAgICAgICAgICAgIGRhdGEgPSB7Y29udHJhY3Q6IHRoaXMuY29udHJhY3QsIGFtb3VudDogdGhpcy5hbW91bnQsIGlzRWFybHlSZXBheW1lbnQ6IGZhbHNlfSxcclxuICAgICAgICAgICAgICAgIF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICAgICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IHJlc3BvbnNlLnJlcGF5bWVudFVybDtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcihyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmVycm9ycyA9IHJlc3BvbnNlLnJlc3BvbnNlSlNPTi5lcnJvcnM7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGhhc0Vycm9yKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5lcnJvcnMubGVuZ3RoID4gMDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuXHJcblZ1ZS5jb21wb25lbnQoJ2VudGl0eS10cmFuc2xhdGlvbicsIHtcclxuICAgIGRlbGltaXRlcnM6IFsnW1snLCAnXV0nXSxcclxuICAgIHRlbXBsYXRlOiBgXHJcbjxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCI+XHJcbiAgICA8bGFiZWwgZm9yPVwiY29udGVudFwiIGNsYXNzPVwiY29udHJvbC1sYWJlbCB0ZXh0LXVwcGVyY2FzZSB0ZXh0LXByaW1hcnkgZm9udC13ZWlnaHQtYm9sZGVyXCI+W1tsb2NhbGVdXTwvbGFiZWw+XHJcbiAgICA8c3BhbiBjbGFzcz1cImZhIGZhLWNsb3VkLXVwbG9hZCB0ZXh0LWRhcmtcIiBAY2xpY2s9XCJzYXZlQ29udGVudFwiIHN0eWxlPVwiZm9udC1zaXplOiAxLjVlbTsgY3Vyc29yOiBwb2ludGVyO1wiPjwvc3Bhbj5cclxuICAgIDx0ZXh0YXJlYSBuYW1lPVwiY29udGVudFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgcm93cz1cIjNcIiB2LW1vZGVsPVwiY29udGVudFwiPjwvdGV4dGFyZWE+XHJcbjwvZGl2PlxyXG5gLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBvYmplY3RDbGFzczoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmb3JlaWduS2V5OiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmdcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZpZWxkOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmdcclxuICAgICAgICB9LFxyXG4gICAgICAgIGxvY2FsZToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgY29udGVudDogJ0xvYWRpbmcuLi4nXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgdGhpcy5nZXRDb250ZW50KCk7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGdldENvbnRlbnQoKSB7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSBgL2FkbWluL2VudGl0eS10cmFuc2xhdGlvbnMvZ2V0LyR7ZW5jb2RlVVJJKHRoaXMub2JqZWN0Q2xhc3MpfS8ke3RoaXMuZm9yZWlnbktleX0vJHt0aGlzLmZpZWxkfS8ke3RoaXMubG9jYWxlfWAsXHJcbiAgICAgICAgICAgICAgICBfc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnR0VUJyxcclxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzcyhyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmNvbnRlbnQgPSByZXNwb25zZS5jb250ZW50O1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNhdmVDb250ZW50KCkge1xyXG4gICAgICAgICAgICBsZXQgdXJsID0gYC9hZG1pbi9lbnRpdHktdHJhbnNsYXRpb25zLyR7ZW5jb2RlVVJJKHRoaXMub2JqZWN0Q2xhc3MpfS8ke3RoaXMuZm9yZWlnbktleX0vJHt0aGlzLmZpZWxkfS8ke3RoaXMubG9jYWxlfWAsXHJcbiAgICAgICAgICAgICAgICBfc2VsZiA9IHRoaXMsXHJcbiAgICAgICAgICAgICAgICBkYXRhID0ge2NvbnRlbnQ6IHRoaXMuY29udGVudH07XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZsYXNoKCdUcmFuc2xhdGlvbiB3YXMgdXBkYXRlZC4nLCAnc3VjY2VzcycpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIGZsYXNoKCdUcmFuc2xhdGlvbiBjb3VsZCBub3QgYmUgc2F2ZWQuJywgJ2Vycm9yJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgnZW50aXR5LXRyYW5zbGF0aW9ucy1mb3JtJywge1xyXG4gICAgZGVsaW1pdGVyczogWydbWycsICddXSddLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuPGRpdj5cclxuICAgIDxlbnRpdHktdHJhbnNsYXRpb24gdi1mb3I9XCJsb2NhbGUgaW4gbG9jYWxlc0FycmF5XCIgOmtleT1cImxvY2FsZVwiIDpvYmplY3QtY2xhc3M9XCJvYmplY3RDbGFzc1wiIDpmaWVsZD1cImZpZWxkXCIgOmZvcmVpZ24ta2V5PVwiZm9yZWlnbktleVwiIDpsb2NhbGU9XCJsb2NhbGVcIj48L2VudGl0eS10cmFuc2xhdGlvbj5cclxuPC9kaXY+XHJcbmAsXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIG9iamVjdENsYXNzOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmdcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZvcmVpZ25LZXk6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZmllbGQ6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbG9jYWxlczoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbG9jYWxlc0FycmF5OiBbXSxcclxuICAgICAgICAgICAgdHJhbnNsYXRpb25zOiB7fVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIHRoaXMubG9jYWxlc0FycmF5ID0gdGhpcy5sb2NhbGVzLnNwbGl0KCd8Jyk7XHJcbiAgICB9XHJcbn0pOyIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuXHJcblZ1ZS5jb21wb25lbnQoJ2Zyb250ZW5kLXZhbGlkYXRpb24nLCB7XHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgPGxhYmVsIGZvcj1cImFtb3VudFwiIGNsYXNzPVwicmVxdWlyZWRcIj5cclxuICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1hc3RlcmlzayBmYS1yZXF1aXJlZFwiIHYtaWY9XCJyZXF1aXJlZFwiPjwvaT5cclxuICAgICAgICAgICAgPHNwYW4gdi10ZXh0PVwibGFiZWxcIj48L3NwYW4+XHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW52YWxpZC1mZWVkYmFjayBkLWJsb2NrXCIgdi1mb3I9XCJlcnJvciBpbiBlcnJvcnNcIiB2LWlmPVwiaGFzRXJyb3JzXCI+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImQtYmxvY2tcIj5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImZvcm0tZXJyb3ItaWNvbiBiYWRnZSBiYWRnZS1kYW5nZXIgdGV4dC11cHBlcmNhc2VcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgRVJST1JcclxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJmb3JtLWVycm9yLW1lc3NhZ2VcIiB2LXRleHQ9XCJlcnJvclwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgIDxzbG90Pjwvc2xvdD5cclxuICAgICAgPC9kaXY+XHJcbiAgICBgLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBwcm9wZXJ0eToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBsYWJlbDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfSxcclxuICAgICAgICByZXF1aXJlZDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IEJvb2xlYW4sXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IHRydWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVudGl0eToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgZXJyb3JzOiBbXVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIHRoaXMuJHJlZnNbJ2lucHV0J10gPSAodGhpcy4kZWwucXVlcnlTZWxlY3RvcignaW5wdXQnKSk7XHJcblxyXG4gICAgICAgIHRoaXMuJHJlZnMuaW5wdXQuYWRkRXZlbnRMaXN0ZW5lcignaW5wdXQnLCB0aGlzLnZhbGlkYXRlKTtcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgdmFsaWRhdGUoKSB7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSBgL2Zyb250ZW5kLXZhbGlkYXRpb24vdmFsaWRhdGVgLFxyXG4gICAgICAgICAgICAgICAgZGF0YSA9IHtwcm9wZXJ0eTogdGhpcy5wcm9wZXJ0eSwgZW50aXR5OiB0aGlzLmVudGl0eSwgdmFsdWU6IHRoaXMuJHJlZnMuaW5wdXQudmFsdWV9LFxyXG4gICAgICAgICAgICAgICAgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzcygpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5lcnJvcnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi4kcmVmcy5pbnB1dC5jbGFzc0xpc3QucmVtb3ZlKCdpcy1pbnZhbGlkJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5lcnJvcnMgPSByZXNwb25zZS5yZXNwb25zZUpTT04uZXJyb3JzO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoX3NlbGYuZXJyb3JzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgX3NlbGYuJHJlZnMuaW5wdXQuY2xhc3NMaXN0LmFkZCgnaXMtaW52YWxpZCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgaGFzRXJyb3JzKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5lcnJvcnMubGVuZ3RoID4gMDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuaW1wb3J0IGdtYXBzSW5pdCBmcm9tICcuLi91dGlscy9nbWFwcyc7XHJcbmltcG9ydCBNYXJrZXJDbHVzdGVyZXIgZnJvbSAnQGdvb2dsZS9tYXJrZXJjbHVzdGVyZXInO1xyXG5cclxuVnVlLmNvbXBvbmVudCgnZ21hcHMnLCB7XHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICB0ZW1wbGF0ZTogJyNnbWFwcycsXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIGFkZHJlc3M6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICdVa3JhaW5lJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgem9vbToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IE51bWJlcixcclxuICAgICAgICAgICAgZGVmYXVsdDogNlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdHlwZToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgZGVmYXVsdDogbnVsbFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgb2ZmaWNlczoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IEFycmF5LFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBudWxsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjaXRpZXM6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBBcnJheSxcclxuICAgICAgICAgICAgZGVmYXVsdDogbnVsbFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbWFya2VyMToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbWFya2VyMjoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgb2ZmaWNlSWNvbjoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzaG93TWFwSW5pdGlhbGx5OiB7XHJcbiAgICAgICAgICAgIHR5cGU6IEJvb2xlYW4sXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgZGVmYXVsdDogdHJ1ZVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGlzT2ZmaWNlU2VsZWN0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBtYXA6IG51bGwsXHJcbiAgICAgICAgICAgIGdlb2NvZGVyOiBudWxsLFxyXG4gICAgICAgICAgICBzZWxlY3RlZENpdHk6IHRoaXMuYWRkcmVzcyxcclxuICAgICAgICAgICAgc2VsZWN0ZWRPZmZpY2U6IHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBjaXR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgdGVsZXBob25lOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgd29ya2luZ1RpbWU6IFtdLFxyXG4gICAgICAgICAgICAgICAgZ29vZ2xlTWFwTGluazogbnVsbFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBzdHlsZXM6IFtcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICB1cmw6IHRoaXMubWFya2VyMSxcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogNDAsXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0MCxcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0Q29sb3I6ICcjZmZmZmZmJyxcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0U2l6ZTogMTBcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsOiB0aGlzLm1hcmtlcjIsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDQwLFxyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDAsXHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dENvbG9yOiAnI2ZmZmZmZicsXHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dFNpemU6IDExXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHVybDogdGhpcy5tYXJrZXIyLFxyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiA0MCxcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwLFxyXG4gICAgICAgICAgICAgICAgICAgIHRleHRDb2xvcjogJyNmZmZmZmYnLFxyXG4gICAgICAgICAgICAgICAgICAgIHRleHRTaXplOiAxMlxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgbWFwU3R5bGVzOiBbXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgXCJmZWF0dXJlVHlwZVwiOiBcImFkbWluaXN0cmF0aXZlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImxhYmVscy50ZXh0LmZpbGxcIixcclxuICAgICAgICAgICAgICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImNvbG9yXCI6IFwiIzBjMGIwYlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJsYW5kc2NhcGVcIixcclxuICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwiYWxsXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJjb2xvclwiOiBcIiNmMmYyZjJcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwicG9pXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImFsbFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIFwic3R5bGVyc1wiOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidmlzaWJpbGl0eVwiOiBcIm9mZlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJyb2FkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImFsbFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIFwic3R5bGVyc1wiOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwic2F0dXJhdGlvblwiOiAtMTAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGlnaHRuZXNzXCI6IDQ1XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJyb2FkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImxhYmVscy50ZXh0LmZpbGxcIixcclxuICAgICAgICAgICAgICAgICAgICBcInN0eWxlcnNcIjogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImNvbG9yXCI6IFwiIzA5MDkwOVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJyb2FkLmhpZ2h3YXlcIixcclxuICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwiYWxsXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ2aXNpYmlsaXR5XCI6IFwic2ltcGxpZmllZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJyb2FkLmFydGVyaWFsXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJlbGVtZW50VHlwZVwiOiBcImxhYmVscy5pY29uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ2aXNpYmlsaXR5XCI6IFwic2ltcGxpZmllZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJ3YXRlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJnZW9tZXRyeS5maWxsXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ2aXNpYmlsaXR5XCI6IFwib25cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImNvbG9yXCI6IFwiI2IzYzBjYlwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIFwiZmVhdHVyZVR5cGVcIjogXCJ3YXRlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiZWxlbWVudFR5cGVcIjogXCJsYWJlbHMudGV4dC5maWxsXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJjb2xvclwiOiBcIiM5YjdmN2ZcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBcImZlYXR1cmVUeXBlXCI6IFwid2F0ZXJcIixcclxuICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IFwibGFiZWxzLnRleHQuc3Ryb2tlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJzdHlsZXJzXCI6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJjb2xvclwiOiBcIiNmZWY3ZjdcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICBpc01hcFZpc2libGU6IGZhbHNlLFxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGdtYXBUeXBlOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnR5cGUgPT09ICdzYXRlbGxpdGUnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZ29vZ2xlLm1hcHMuTWFwVHlwZUlkLlNBVEVMTElURTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnR5cGUgPT09ICdoeWJyaWQnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZ29vZ2xlLm1hcHMuTWFwVHlwZUlkLkhZQlJJRDtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnR5cGUgPT09ICd0ZXJyYWluJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGdvb2dsZS5tYXBzLk1hcFR5cGVJZC5URVJSQUlOO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gZ29vZ2xlLm1hcHMuTWFwVHlwZUlkLlJPQURNQVA7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdGVsZXBob25lVGV4dCgpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRPZmZpY2UudGVsZXBob25lKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zZWxlY3RlZE9mZmljZS50ZWxlcGhvbmU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ24vYSc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB3b3JraW5nVGltZVRleHQoKSB7XHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgdGhpcy5zZWxlY3RlZE9mZmljZS53b3JraW5nVGltZSAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IHBhcnRzID0gdGhpcy5zZWxlY3RlZE9mZmljZS53b3JraW5nVGltZS5zcGxpdCgnLCcpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGAke3BhcnRzWzBdfTogJHtwYXJ0c1sxXX0gLSAke3BhcnRzWzJdfWA7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjb21wdXRlWm9vbSgpIHtcclxuICAgICAgICAgICAgaWYodGhpcy5zZWxlY3RlZENpdHkgJiYgdGhpcy5hZGRyZXNzICE9PSB0aGlzLnNlbGVjdGVkQ2l0eSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIDEzO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy56b29tO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBhc3luYyBtb3VudGVkKCkge1xyXG4gICAgICAgIHRoaXMuaXNNYXBWaXNpYmxlID0gdGhpcy5zaG93TWFwSW5pdGlhbGx5O1xyXG5cclxuICAgICAgICBpZiAodGhpcy5pc01hcFZpc2libGUpIHtcclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5sb2FkTWFwKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBhc3luYyBsb2FkTWFwKCkge1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZ29vZ2xlID0gYXdhaXQgZ21hcHNJbml0KCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKHRoaXMuJHJlZnMuZ21hcCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdlb2NvZGVyID0gbmV3IGdvb2dsZS5tYXBzLkdlb2NvZGVyKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1hcCA9IG1hcDtcclxuICAgICAgICAgICAgICAgIG1hcC5zZXRPcHRpb25zKHtzdHlsZXM6IHRoaXMubWFwU3R5bGVzfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5nZW9jb2Rlci5nZW9jb2RlKHthZGRyZXNzOiB0aGlzLmFkZHJlc3N9LCAocmVzdWx0cywgc3RhdHVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHN0YXR1cyAhPT0gJ09LJyB8fCAhcmVzdWx0c1swXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3Ioc3RhdHVzKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgbWFwLnNldENlbnRlcihyZXN1bHRzWzBdLmdlb21ldHJ5LmxvY2F0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICBtYXAuZml0Qm91bmRzKHJlc3VsdHNbMF0uZ2VvbWV0cnkudmlld3BvcnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcC5zZXRab29tKHRoaXMuem9vbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFwLm1hcFR5cGVJZCA9IHRoaXMuZ21hcFR5cGU7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFwLmdlc3R1cmVIYW5kbGluZyA9ICdjb29wZXJhdGl2ZSc7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBtYXJrZXJDbGlja0hhbmRsZXIgPSAobWFya2VyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFwLnNldFpvb20oMTcpO1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcC5zZXRDZW50ZXIobWFya2VyLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0T2ZmaWNlKG1hcmtlcik7XHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IG1hcmtlcnMgPSB0aGlzLm9mZmljZXMubWFwKChsb2NhdGlvbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5sb2NhdGlvbiwgbWFwLCBpY29uOiB0aGlzLm9mZmljZUljb259XHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICBtYXJrZXIuYWRkTGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4gbWFya2VyQ2xpY2tIYW5kbGVyKG1hcmtlcikpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbWFya2VyO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgbmV3IE1hcmtlckNsdXN0ZXJlcihtYXAsIG1hcmtlcnMsIHtzdHlsZXM6IHRoaXMuc3R5bGVzfSk7XHJcblxyXG4gICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGZpbHRlcigpIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZ2VvY29kZXIuZ2VvY29kZSh7YWRkcmVzczogdGhpcy5hZGRyZXNzICsgJywnICsgdGhpcy5zZWxlY3RlZENpdHl9LCAocmVzdWx0cywgc3RhdHVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoc3RhdHVzICE9PSAnT0snIHx8ICFyZXN1bHRzWzBdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKHN0YXR1cyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1hcC5zZXRDZW50ZXIocmVzdWx0c1swXS5nZW9tZXRyeS5sb2NhdGlvbik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1hcC5maXRCb3VuZHMocmVzdWx0c1swXS5nZW9tZXRyeS52aWV3cG9ydCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1hcC5zZXRab29tKHRoaXMuY29tcHV0ZVpvb20pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5tYXAubWFwVHlwZUlkID0gdGhpcy5nbWFwVHlwZTtcclxuICAgICAgICAgICAgICAgIHRoaXMubWFwLmdlc3R1cmVIYW5kbGluZyA9ICdjb29wZXJhdGl2ZSc7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2VsZWN0T2ZmaWNlKG9mZmljZSkge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkT2ZmaWNlLm5hbWUgPSBvZmZpY2UudGl0bGU7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPZmZpY2UuY2l0eSA9IG9mZmljZS5jaXR5O1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkT2ZmaWNlLnRlbGVwaG9uZSA9IG9mZmljZS50ZWxlcGhvbmU7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPZmZpY2UuYWRkcmVzcyA9IG9mZmljZS5hZGRyZXNzO1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkT2ZmaWNlLndvcmtpbmdUaW1lID0gb2ZmaWNlLndvcmtpbmdUaW1lO1xyXG4gICAgICAgICAgICB0aGlzLmlzT2ZmaWNlU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkT2ZmaWNlLmdvb2dsZU1hcExpbmsgPSBcImh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9kaXIvL1wiICsgb2ZmaWNlLmdldFBvc2l0aW9uKCkubGF0KCkgKyBcIixcIiArIG9mZmljZS5nZXRQb3NpdGlvbigpLmxuZygpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGVzZWxlY3RPZmZpY2UoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNPZmZpY2VTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYXN5bmMgc2hvd01hcCgpIHtcclxuICAgICAgICAgICAgdGhpcy5pc01hcFZpc2libGUgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5sb2FkTWFwKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdpbWFnZS1nYWxsZXJ5Jywge1xyXG4gICAgZGVsaW1pdGVyczogWydbWycsICddXSddLFxyXG4gICAgdGVtcGxhdGU6ICcjaW1hZ2UtZ2FsbGVyeScsXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIGNvbXBvbmVudElkOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWVcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBpbWFnZXM6IFtdLFxyXG4gICAgICAgICAgICBtb2RhbEVsZW1lbnQ6IG51bGwsXHJcbiAgICAgICAgICAgIGlzTG9hZGluZzogdHJ1ZSxcclxuICAgICAgICAgICAgdXBsb2FkaW5nOiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIHRoaXMubW9kYWxFbGVtZW50ID0gdGhpcy4kZWwucXVlcnlTZWxlY3RvcignI2ltYWdlLWdhbGxlcnktbW9kYWwnKTtcclxuXHJcbiAgICAgICAgJCh0aGlzLm1vZGFsRWxlbWVudCkub24oJ2hpZGRlbi5icy5tb2RhbCcsIHRoaXMuY2xvc2VHYWxsZXJ5KTtcclxuXHJcbiAgICAgICAgdGhpcy5vcGVuR2FsbGVyeSgpO1xyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBnZXRJbWFnZXMoKSB7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSAnL2FkbWluL2ltYWdlJyxcclxuICAgICAgICAgICAgICAgIF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5pbWFnZXMgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5pc0xvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBvcGVuR2FsbGVyeSgpIHtcclxuICAgICAgICAgICAgJCh0aGlzLm1vZGFsRWxlbWVudCkubW9kYWwoJ3Nob3cnKTtcclxuICAgICAgICAgICAgdGhpcy5nZXRJbWFnZXMoKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjbG9zZUdhbGxlcnkoKSB7XHJcbiAgICAgICAgICAgICQodGhpcy5tb2RhbEVsZW1lbnQpLm1vZGFsKCdoaWRlJyk7XHJcbiAgICAgICAgICAgIEV2ZW50TWFuYWdlci5maXJlKCdyZW1vdmUtY29tcG9uZW50JywgdGhpcy5jb21wb25lbnRJZCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2VsZWN0SW1hZ2UocHVibGljUGF0aCkge1xyXG4gICAgICAgICAgICBFdmVudE1hbmFnZXIuZmlyZSgnaW1hZ2UtZ2FsbGVyeS5zZWxlY3QtaW1hZ2UnLCB7cGF0aDogcHVibGljUGF0aCwgY29tcG9uZW50SWQ6IHRoaXMuY29tcG9uZW50SWR9KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuY2xvc2VHYWxsZXJ5KCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2VsZWN0RmlsZSgpIHtcclxuICAgICAgICAgICAgdGhpcy4kcmVmcy5maWxlSW5wdXQuY2xpY2soKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB1cGxvYWRGaWxlKCkge1xyXG4gICAgICAgICAgICBsZXQgZmlsZSA9IHRoaXMuJHJlZnMuZmlsZUlucHV0LmZpbGVzWzBdLFxyXG4gICAgICAgICAgICAgICAgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFmaWxlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMudXBsb2FkaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgIGxldCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5hcHBlbmQoXCJmaWxlXCIsIGZpbGUpO1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ3Bvc3QnLFxyXG4gICAgICAgICAgICAgICAgdXJsOiAnL2FkbWluL2ZpbGVzL2ltYWdlL2NyZWF0ZT9mbGFzaD0wJyxcclxuICAgICAgICAgICAgICAgIGRhdGE6IGZvcm1EYXRhLFxyXG4gICAgICAgICAgICAgICAgcHJvY2Vzc0RhdGE6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgY29udGVudFR5cGU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzcyhyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnVwbG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmltYWdlcy51bnNoaWZ0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcihlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnVwbG9hZGluZyA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBmbGFzaChlcnJvclsncmVzcG9uc2VKU09OJ10uZGV0YWlsLCAnZXJyb3InKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGlzVXBsb2FkaW5nKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy51cGxvYWRpbmcgPT09IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcbmltcG9ydCBTb3J0YWJsZSBmcm9tIFwic29ydGFibGVqc1wiO1xyXG5cclxuVnVlLmNvbXBvbmVudCgncG9sbCcsIHtcclxuICAgIGRlbGltaXRlcnM6IFsnW1snLCAnXV0nXSxcclxuICAgIHRlbXBsYXRlOiAnI3BvbGwnLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBwb2xsSWQ6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbG9jYWxlOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAndWEnXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcG9sbDoge3F1ZXN0aW9uczogW119LFxyXG4gICAgICAgICAgICBuZXdRdWVzdGlvbjoge3R5cGU6ICdzaW5nbGVfYW5zd2VyJ30sXHJcbiAgICAgICAgICAgIGlzQWRkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgc29ydGFibGU6IG51bGxcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgICAgICB0aGlzLmdldERhdGEoKTtcclxuICAgIH0sXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIHRoaXMuaW5pdFNvcnRhYmxlKCk7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGdldENyZWF0ZURhdGEoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0OiB0aGlzLm5ld1F1ZXN0aW9uLnRleHQsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiB0aGlzLm5ld1F1ZXN0aW9uLnR5cGUsXHJcbiAgICAgICAgICAgICAgICBwb2xsSWQ6IHRoaXMucG9sbC5pZFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0RGF0YSgpIHtcclxuICAgICAgICAgICAgbGV0IHVybCA9IGAke3RoaXMubG9jYWxlID09PSAndWEnID8gJycgOiAnLycgKyB0aGlzLmxvY2FsZX0vYWRtaW4vcG9sbHMvJHt0aGlzLnBvbGxJZH1gLFxyXG4gICAgICAgICAgICAgICAgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5wb2xsID0gcmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIG9uUXVlc3Rpb25EZWxldGVkKGlkKSB7XHJcbiAgICAgICAgICAgIGxldCBpbmRleCA9IHRoaXMucG9sbC5xdWVzdGlvbnMuZmluZEluZGV4KGl0ZW0gPT4gaXRlbS5pZCA9PT0gaWQpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5wb2xsLnF1ZXN0aW9ucy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGNyZWF0ZVF1ZXN0aW9uKCkge1xyXG4gICAgICAgICAgICBsZXQgdXJsID0gYCR7dGhpcy5sb2NhbGUgPT09ICd1YScgPyAnJyA6ICcvJyArIHRoaXMubG9jYWxlfS9hZG1pbi9xdWVzdGlvbnMvbmV3YCxcclxuICAgICAgICAgICAgICAgIGRhdGEgPSB0aGlzLmdldENyZWF0ZURhdGEoKSxcclxuICAgICAgICAgICAgICAgIF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICAgICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcXVlc3Rpb24gPSByZXNwb25zZVsncGF5bG9hZCddO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBxdWVzdGlvblsnYW5zd2VycyddID0gW107XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmlzQWRkaW5nID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnBvbGwucXVlc3Rpb25zLnB1c2gocXVlc3Rpb24pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5pbml0U29ydGFibGUoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYucmVzZXRRdWVzdGlvbigpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBmbGFzaChyZXNwb25zZVsnbWVzc2FnZSddLCAnc3VjY2VzcycpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICByZXNldFF1ZXN0aW9uKCkge1xyXG4gICAgICAgICAgICB0aGlzLm5ld1F1ZXN0aW9uID0ge1xyXG4gICAgICAgICAgICAgICAgdGV4dDogJycsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnc2luZ2xlX2Fuc3dlcicsXHJcbiAgICAgICAgICAgICAgICBxdWVzdGlvbnM6IFtdXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgaW5pdFNvcnRhYmxlKCkge1xyXG4gICAgICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5zb3J0YWJsZSA9IFNvcnRhYmxlLmNyZWF0ZSh0aGlzLiRyZWZzLnF1ZXN0aW9ucywge1xyXG4gICAgICAgICAgICAgICAgZ2hvc3RDbGFzczogJ2JsdWUtYmFja2dyb3VuZC1kdXJpbmctZHJhZycsXHJcbiAgICAgICAgICAgICAgICAnaGFuZGxlJzogJy5zb3J0LWhhbmRsZScsXHJcbiAgICAgICAgICAgICAgICBhbmltYXRpb246IDMwMCxcclxuICAgICAgICAgICAgICAgIG9uU29ydDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkYXRhID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuJHJlZnMucXVlc3Rpb25zLmNoaWxkcmVuLmZvckVhY2goaXRlbSA9PiBkYXRhLnB1c2goaXRlbS5nZXRBdHRyaWJ1dGUoJ2RhdGEtaWQnKSkpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5zb3J0UXVlc3Rpb25zKHtzb3J0ZWRRdWVzdGlvbnM6IGRhdGF9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc29ydFF1ZXN0aW9ucyhkYXRhKSB7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSBgJHt0aGlzLmxvY2FsZSA9PT0gJ3VhJyA/ICcnIDogJy8nICsgdGhpcy5sb2NhbGV9L2FkbWluL3BvbGxzLyR7dGhpcy5wb2xsLmlkfS9zb3J0LXF1ZXN0aW9uc2A7XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxhc2gocmVzcG9uc2UsICdzdWNjZXNzJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgncG9sbC1hbnN3ZXInLCB7XHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICB0ZW1wbGF0ZTogJyNwb2xsLWFuc3dlcicsXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIGFuc3dlcjoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogT2JqZWN0XHJcbiAgICAgICAgfSxcclxuICAgICAgICBsb2NhbGU6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICd1YSdcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBpc0VkaXRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICBkZWxldGVTdGF0ZTogJ2luaXRpYWwnXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBnZXRTYXZlRGF0YSgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMuYW5zd2VyLnRleHQsXHJcbiAgICAgICAgICAgICAgICBoYXNDb21tZW50OiB0aGlzLmFuc3dlci5oYXNDb21tZW50LFxyXG4gICAgICAgICAgICAgICAgY29tbWVudEhlbHBUZXh0OiB0aGlzLmFuc3dlci5oYXNDb21tZW50ID8gdGhpcy5hbnN3ZXIuY29tbWVudEhlbHBUZXh0IDogbnVsbFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgc2F2ZSgpIHtcclxuICAgICAgICAgICAgbGV0IHVybCA9IGAke3RoaXMubG9jYWxlID09PSAndWEnID8gJycgOiAnLycgKyB0aGlzLmxvY2FsZX0vYWRtaW4vYW5zd2Vycy8ke3RoaXMuYW5zd2VyLmlkfS91cGRhdGVgLFxyXG4gICAgICAgICAgICAgICAgX3NlbGYgPSB0aGlzLFxyXG4gICAgICAgICAgICAgICAgZGF0YSA9IHRoaXMuZ2V0U2F2ZURhdGEoKTtcclxuXHJcbiAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICAgICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5pc0VkaXRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBmbGFzaChyZXNwb25zZSwgJ3N1Y2Nlc3MnKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcihlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmlzRWRpdGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGZsYXNoKGVycm9yWydyZXNwb25zZUpTT04nXSwgJ2Vycm9yJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGRlbGV0ZUFuc3dlcigpIHtcclxuICAgICAgICAgICAgbGV0IHVybCA9IGAke3RoaXMubG9jYWxlID09PSAndWEnID8gJycgOiAnLycgKyB0aGlzLmxvY2FsZX0vYWRtaW4vYW5zd2Vycy8ke3RoaXMuYW5zd2VyLmlkfS9kZWxldGVgLFxyXG4gICAgICAgICAgICAgICAgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuaXNFZGl0aW5nID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmNoYW5nZURlbGV0ZVN0YXRlKCdpbml0aWFsJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLiRlbWl0KCdhbnN3ZXJXYXNEZWxldGVkJywgX3NlbGYuYW5zd2VyLmlkKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZmxhc2gocmVzcG9uc2UsICdzdWNjZXNzJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5pc0VkaXRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBmbGFzaChlcnJvclsncmVzcG9uc2VKU09OJ10sICdlcnJvcicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjaGFuZ2VEZWxldGVTdGF0ZShuZXdTdGF0ZSkge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGV0ZVN0YXRlID0gbmV3U3RhdGU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdwb2xsLXBhcnRpY2lwYXRpb24nLCB7XHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICB0ZW1wbGF0ZTogJyNwb2xsLXBhcnRpY2lwYXRpb24nLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBsb2NhbGU6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICd1YSdcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBhY3RpdmVQb2xsczoge30sXHJcbiAgICAgICAgICAgIGFjdGl2ZVBvbGxzRGF0YTogW10sXHJcbiAgICAgICAgICAgIHNlbGVjdGVkUG9sbDogbnVsbCxcclxuICAgICAgICAgICAgc2hvdzogdHJ1ZVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIGNvbnN0IHJlZ2V4ID0gL1xcL3BvbGxcXC9cXGR7MX1cXC9jb21wbGV0ZS9nO1xyXG5cclxuICAgICAgICBpZiAod2luZG93LmxvY2F0aW9uLmhyZWYubWF0Y2gocmVnZXgpICE9PSBudWxsIHx8XHJcbiAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmLmluZGV4T2YoJy9lbi9hZG1pbicpID49IDApIHtcclxuICAgICAgICAgICAgdGhpcy5zaG93ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHNldFRpbWVvdXQodGhpcy5nZXRBY3RpdmVQb2xscywgMzAwMCk7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGdldEFjdGl2ZVBvbGxzKCkge1xyXG4gICAgICAgICAgICBsZXQgdXJsID0gYC9lbi9wb2xsL2FjdGl2ZT9sb2NhbGU9JHt0aGlzLmxvY2FsZX1gLFxyXG4gICAgICAgICAgICAgICAgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnR0VUJyxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmFjdGl2ZVBvbGxzRGF0YSA9IHJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmFjdGl2ZVBvbGxzID0gcmVzcG9uc2UubWFwKGl0ZW0gPT4gaXRlbS5pZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuc2VsZWN0UG9sbCgpO1xyXG5cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzZWxlY3RQb2xsKCkge1xyXG4gICAgICAgICAgICBsZXQgY29tcGxldGVkUG9sbHMgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY29tcGxldGVkX3BvbGxzJykgfHwgJycsXHJcbiAgICAgICAgICAgICAgICBub3RDb21wbGV0ZWRQb2xscztcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmFjdGl2ZVBvbGxzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoIWNvbXBsZXRlZFBvbGxzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkUG9sbCA9IHRoaXMuYWN0aXZlUG9sbHNbMF07XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbXBsZXRlZFBvbGxzID0gY29tcGxldGVkUG9sbHMuc3BsaXQoJ3wnKTtcclxuICAgICAgICAgICAgbm90Q29tcGxldGVkUG9sbHMgPSB0aGlzLmFjdGl2ZVBvbGxzLmZpbHRlcihwb2xsID0+IGNvbXBsZXRlZFBvbGxzLmZpbmRJbmRleChpdGVtID0+IHBhcnNlSW50KGl0ZW0pID09PSBwb2xsKSA8IDApO1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkUG9sbCA9IG5vdENvbXBsZXRlZFBvbGxzLnBvcCgpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGRlY2xpbmVQb2xsKCkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuc2VsZWN0ZWRQb2xsKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBjb21wbGV0ZWRQb2xscyA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdjb21wbGV0ZWRfcG9sbHMnKSB8fCAnJztcclxuICAgICAgICAgICAgY29tcGxldGVkUG9sbHMgPSBjb21wbGV0ZWRQb2xscy5zcGxpdCgnfCcpO1xyXG4gICAgICAgICAgICBjb21wbGV0ZWRQb2xscy5wdXNoKHRoaXMuc2VsZWN0ZWRQb2xsKTtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2NvbXBsZXRlZF9wb2xscycsIGNvbXBsZXRlZFBvbGxzLmpvaW4oJ3wnKSk7XHJcbiAgICAgICAgICAgIHRoaXMuY2xvc2UoKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzaG93UG9sbCgpIHtcclxuICAgICAgICAgICAgbGV0IHBvbGxVcmwgPSB0aGlzLmFjdGl2ZVBvbGxzRGF0YS5maWx0ZXIoaXRlbSA9PiBpdGVtLmlkID09PSB0aGlzLnNlbGVjdGVkUG9sbCkucG9wKCkucm91dGU7XHJcblxyXG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IHBvbGxVcmw7XHJcbiAgICAgICAgICAgIC8vIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gYC9lbi9wb2xsLyR7dGhpcy5zZWxlY3RlZFBvbGx9L2NvbXBsZXRlYDtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjbG9zZSgpIHtcclxuICAgICAgICAgICAgJCgnI3BvbGxfcGFydGljaXBhdGlvbicpLm1vZGFsKCdoaWRlJyk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgb3BlbigpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc2hvdykge1xyXG4gICAgICAgICAgICAgICAgJCgnI3BvbGxfcGFydGljaXBhdGlvbicpLm1vZGFsKCdzaG93Jyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgd2F0Y2g6IHtcclxuICAgICAgICBzZWxlY3RlZFBvbGw6IGZ1bmN0aW9uIChuZXdWYWx1ZSkge1xyXG4gICAgICAgICAgICBpZiAobmV3VmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMub3BlbigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcbmltcG9ydCBTb3J0YWJsZSBmcm9tIFwic29ydGFibGVqc1wiXHJcblxyXG5WdWUuY29tcG9uZW50KCdwb2xsLXF1ZXN0aW9uJywge1xyXG4gICAgZGVsaW1pdGVyczogWydbWycsICddXSddLFxyXG4gICAgdGVtcGxhdGU6ICcjcG9sbC1xdWVzdGlvbicsXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIHF1ZXN0aW9uOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBPYmplY3RcclxuICAgICAgICB9LFxyXG4gICAgICAgIGxvY2FsZToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgZGVmYXVsdDogJ3VhJ1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGlzRWRpdGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgIGlzT3BlbjogZmFsc2UsXHJcbiAgICAgICAgICAgIGlzQWRkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgZGVsZXRlU3RhdGU6ICdpbml0aWFsJyxcclxuICAgICAgICAgICAgbmV3QW5zd2VyOiB7XHJcbiAgICAgICAgICAgICAgICBoYXNDb21tZW50OiBmYWxzZVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBzb3J0YWJsZTogbnVsbCxcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBnZXRTYXZlRGF0YSgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMucXVlc3Rpb24udGV4dCxcclxuICAgICAgICAgICAgICAgIHR5cGU6IHRoaXMucXVlc3Rpb24udHlwZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0Q3JlYXRlRGF0YSgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIHRleHQ6IHRoaXMubmV3QW5zd2VyLnRleHQsXHJcbiAgICAgICAgICAgICAgICBoYXNDb21tZW50OiB0aGlzLm5ld0Fuc3dlci5oYXNDb21tZW50LFxyXG4gICAgICAgICAgICAgICAgY29tbWVudEhlbHBUZXh0OiB0aGlzLm5ld0Fuc3dlci5oYXNDb21tZW50ID8gdGhpcy5uZXdBbnN3ZXIuY29tbWVudEhlbHBUZXh0IDogbnVsbCxcclxuICAgICAgICAgICAgICAgIHF1ZXN0aW9uSWQ6IHRoaXMucXVlc3Rpb24uaWRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHNhdmUoKSB7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSBgJHt0aGlzLmxvY2FsZSA9PT0gJ3VhJyA/ICcnIDogJy8nICsgdGhpcy5sb2NhbGV9L2FkbWluL3F1ZXN0aW9ucy8ke3RoaXMucXVlc3Rpb24uaWR9L3VwZGF0ZWAsXHJcbiAgICAgICAgICAgICAgICBfc2VsZiA9IHRoaXMsXHJcbiAgICAgICAgICAgICAgICBkYXRhID0gdGhpcy5nZXRTYXZlRGF0YSgpO1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzcyhyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmlzRWRpdGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGZsYXNoKHJlc3BvbnNlLCAnc3VjY2VzcycpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuaXNFZGl0aW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxhc2goZXJyb3JbJ3Jlc3BvbnNlSlNPTiddLCAnZXJyb3InKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdG9nZ2xlT3BlbigpIHtcclxuICAgICAgICAgICAgdGhpcy5pc09wZW4gPSAhdGhpcy5pc09wZW47XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZGVsZXRlUXVlc3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSBgJHt0aGlzLmxvY2FsZSA9PT0gJ3VhJyA/ICcnIDogJy8nICsgdGhpcy5sb2NhbGV9L2FkbWluL3F1ZXN0aW9ucy8ke3RoaXMucXVlc3Rpb24uaWR9L2RlbGV0ZWAsXHJcbiAgICAgICAgICAgICAgICBfc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5pc0VkaXRpbmcgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuY2hhbmdlRGVsZXRlU3RhdGUoJ2luaXRpYWwnKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuJGVtaXQoJ3F1ZXN0aW9uV2FzRGVsZXRlZCcsIF9zZWxmLnF1ZXN0aW9uLmlkKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZmxhc2gocmVzcG9uc2UsICdzdWNjZXNzJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5jaGFuZ2VEZWxldGVTdGF0ZSgnaW5pdGlhbCcpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBmbGFzaChlcnJvclsncmVzcG9uc2VKU09OJ10sICdlcnJvcicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBvbkFuc3dlckRlbGV0ZWQoaWQpIHtcclxuICAgICAgICAgICAgbGV0IGluZGV4ID0gdGhpcy5xdWVzdGlvbi5hbnN3ZXJzLmZpbmRJbmRleChhbnN3ZXIgPT4gYW5zd2VyLmlkID09PSBpZCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnF1ZXN0aW9uLmFuc3dlcnMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBjaGFuZ2VEZWxldGVTdGF0ZShuZXdTdGF0ZSkge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGV0ZVN0YXRlID0gbmV3U3RhdGU7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY3JlYXRlQW5zd2VyKCkge1xyXG4gICAgICAgICAgICBsZXQgdXJsID0gYCR7dGhpcy5sb2NhbGUgPT09ICd1YScgPyAnJyA6ICcvJyArIHRoaXMubG9jYWxlfS9hZG1pbi9hbnN3ZXJzL25ld2AsXHJcbiAgICAgICAgICAgICAgICBkYXRhID0gdGhpcy5nZXRDcmVhdGVEYXRhKCksXHJcbiAgICAgICAgICAgICAgICBfc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuaXNBZGRpbmcgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYucXVlc3Rpb24uYW5zd2Vycy5wdXNoKHJlc3BvbnNlWydwYXlsb2FkJ10pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5yZXNldEFuc3dlcigpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBmbGFzaChyZXNwb25zZVsnbWVzc2FnZSddLCAnc3VjY2VzcycpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICByZXNldEFuc3dlcigpIHtcclxuICAgICAgICAgICAgdGhpcy5uZXdBbnN3ZXIgPSB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0OiAnJyxcclxuICAgICAgICAgICAgICAgIGhhc0NvbW1lbnQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgY29tbWVudEhlbHBUZXh0OiAnJyxcclxuICAgICAgICAgICAgICAgIGFuc3dlcnM6IFtdXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdG9nZ2xlU29ydGluZygpIHtcclxuICAgICAgICAgICAgdGhpcy5zb3J0aW5nID0gIXRoaXMuc29ydGluZztcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBzb3J0QW5zd2VycyhkYXRhKSB7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSBgJHt0aGlzLmxvY2FsZSA9PT0gJ3VhJyA/ICcnIDogJy8nICsgdGhpcy5sb2NhbGV9L2FkbWluL3F1ZXN0aW9ucy8ke3RoaXMucXVlc3Rpb24uaWR9L3NvcnQtYW5zd2Vyc2A7XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxhc2gocmVzcG9uc2UsICdzdWNjZXNzJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICBzaG93QW5zd2VycygpIHtcclxuICAgICAgICAgICAgcmV0dXJuICF0aGlzLmlzRWRpdGluZyAmJiB0aGlzLmlzT3BlbjtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIHdhdGNoOiB7XHJcbiAgICAgICAgc2hvd0Fuc3dlcnM6IGZ1bmN0aW9uIChuZXdWYWx1ZSkge1xyXG4gICAgICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgaWYgKG5ld1ZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNvcnRhYmxlID0gU29ydGFibGUuY3JlYXRlKHRoaXMuJHJlZnMuYW5zd2Vycywge1xyXG4gICAgICAgICAgICAgICAgICAgIGdob3N0Q2xhc3M6ICdibHVlLWJhY2tncm91bmQtZHVyaW5nLWRyYWcnLFxyXG4gICAgICAgICAgICAgICAgICAgICdoYW5kbGUnOiAnLnNvcnQtaGFuZGxlJyxcclxuICAgICAgICAgICAgICAgICAgICBhbmltYXRpb246IDMwMCxcclxuICAgICAgICAgICAgICAgICAgICBvblNvcnQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGRhdGEgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgX3NlbGYuJHJlZnMuYW5zd2Vycy5jaGlsZHJlbi5mb3JFYWNoKGl0ZW0gPT4gZGF0YS5wdXNoKGl0ZW0uZ2V0QXR0cmlidXRlKCdkYXRhLWlkJykpKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF9zZWxmLnNvcnRBbnN3ZXJzKHtzb3J0ZWRBbnN3ZXJzOiBkYXRhfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNvcnRhYmxlID0gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgncmVkaXJlY3QtdGltZXInLCB7XHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICB0ZW1wbGF0ZTogJyNyZWRpcmVjdC10aW1lcicsXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIHNlY29uZHM6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBOdW1iZXIsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IDMwXHJcbiAgICAgICAgfSxcclxuICAgICAgICByZWRpcmVjdFVybDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgZWxhcHNlZFRpbWU6IDAsXHJcbiAgICAgICAgICAgIHRpbWVyOiBudWxsXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgdGhpcy5zdGFydFRpbWVyKCk7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHN0YXJ0VGltZXIoKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGltZXIgPSBzZXRJbnRlcnZhbCh0aGlzLnRpY2ssIDEwMDApXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgdGljaygpIHtcclxuICAgICAgICAgICAgdGhpcy5lbGFwc2VkVGltZSsrO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIHJlbWFpbmluZ1RpbWUoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnNlY29uZHMgLSB0aGlzLmVsYXBzZWRUaW1lO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAgIHJlbWFpbmluZ1RpbWUodmFsdWUpIHtcclxuICAgICAgICAgICAgaWYgKHZhbHVlID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKHRoaXMudGltZXIpO1xyXG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSB0aGlzLnJlZGlyZWN0VXJsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcbmltcG9ydCBTbGlkZXIgZnJvbSAnc2xpY2stY2Fyb3VzZWwnO1xyXG5pbXBvcnQgJ3NsaWNrLWNhcm91c2VsL3NsaWNrL3NsaWNrLmNzcyc7XHJcbmltcG9ydCAnc2xpY2stY2Fyb3VzZWwvc2xpY2svc2xpY2stdGhlbWUuY3NzJztcclxuaW1wb3J0IEZsaWNraXR5IGZyb20gXCJmbGlja2l0eVwiO1xyXG5cclxuVnVlLmNvbXBvbmVudCgnc2xpZGVyLW1vZHVsZScsIHtcclxuICAgIHRlbXBsYXRlOiBgPGRpdiBjbGFzcz1cImNhcm91c2VsXCIgOnN0eWxlPVwic3R5bGVzXCI+PHNsb3Q+PC9zbG90PjwvZGl2PmAsXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIGNlbnRlclBhZGRpbmc6IHtcclxuICAgICAgICAgICAgZGVmYXVsdDogJzE1cHgnLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdGV4dENvbG9yOiB7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICd3aGl0ZScsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzaXplOiB7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICdtZWRpdW0nLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZ1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIHRoaXMuY3JlYXRlU3R5bGUoKTtcclxuXHJcbiAgICAgICAgbmV3IEZsaWNraXR5KHRoaXMuJGVsLCB7XHJcbiAgICAgICAgICAgIGl0ZW1zOiAxLFxyXG4gICAgICAgICAgICB3cmFwQXJvdW5kOiB0cnVlLFxyXG4gICAgICAgICAgICBhdXRvUGxheTogZmFsc2UsXHJcbiAgICAgICAgICAgIHBhZ2VEb3RzOiBmYWxzZSxcclxuICAgICAgICAgICAgY29udGFpbjogdHJ1ZSxcclxuICAgICAgICAgICAgc2V0R2FsbGVyeVNpemU6IGZhbHNlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgc3R5bGVzKCkge1xyXG4gICAgICAgICAgICBsZXQgc3R5bGVzID0ge307XHJcbiAgICAgICAgICAgIHN0eWxlcy5jb2xvciA9IHRoaXMudGV4dENvbG9yO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuc2l6ZSA9PT0gJ3NtYWxsJykge1xyXG4gICAgICAgICAgICAgICAgc3R5bGVzLmhlaWdodCA9ICcyMDBweCc7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5zaXplID09PSAnbWVkaXVtJykge1xyXG4gICAgICAgICAgICAgICAgc3R5bGVzLmhlaWdodCA9ICc0MDBweCc7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBzdHlsZXMuaGVpZ2h0ID0gJzYwMHB4JztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHN0eWxlcztcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGNyZWF0ZVN0eWxlKCkge1xyXG4gICAgICAgICAgICBsZXQgc3R5bGVFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3R5bGUnKTtcclxuICAgICAgICAgICAgbGV0IGNzcyA9IGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGBcclxuICAgICAgICAgICAgLmZsaWNraXR5LWJ1dHRvbi1pY29uIHtcclxuICAgICAgICAgICAgICAgIGZpbGw6ICR7dGhpcy50ZXh0Q29sb3J9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC5mbGlja2l0eS1idXR0b246aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYCk7XHJcbiAgICAgICAgICAgIHN0eWxlRWxlbWVudC5hcHBlbmQoY3NzKTtcclxuXHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2hlYWQnKS5hcHBlbmQoc3R5bGVFbGVtZW50KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuXHJcblZ1ZS5jb21wb25lbnQoJ3RlbGVwaG9uZS1hcHBsaWNhdGlvbi1ub3QtYWxsb3dlZCcsIHtcclxuICAgIG5hbWU6IFwiVGVsZXBob25lQXBwbGljYXRpb25Ob3RBbGxvd2VkXCIsXHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICB0ZW1wbGF0ZTogJyN0ZWxlcGhvbmUtYXBwbGljYXRpb24tbm90LWFsbG93ZWQnLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBzaG93OiB7XHJcbiAgICAgICAgICAgIHR5cGU6IEJvb2xlYW4sXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgZGVmYXVsdDogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgICBpZiAodGhpcy5zaG93ID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICQoJyMnICsgdGhpcy4kcmVmcy5tb2RhbC4kZWwuZ2V0QXR0cmlidXRlKCdpZCcpKS5tb2RhbCgnc2hvdycpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgndHJhbnNsYXRpb24taXRlbScsIHtcclxuICAgIGRlbGltaXRlcnM6IFsnW1snLCAnXV0nXSxcclxuICAgIHRlbXBsYXRlOiAnI3RyYW5zbGF0aW9uLWl0ZW0nLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBpZDoge1xyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlXHJcbiAgICAgICAgfSxcclxuICAgICAgICBsb2NhbGU6IHtcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZG9tYWluOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgfSxcclxuICAgICAgICB0cmFuc2xhdGlvbktleToge1xyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlXHJcbiAgICAgICAgfSxcclxuICAgICAgICB0cmFuc2xhdGlvblZhbHVlOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBudWxsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBkZWZhdWx0TG9jYWxlOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBudWxsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBkZWZhdWx0VHJhbnNsYXRpb25WYWx1ZToge1xyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgZGVmYXVsdDogbnVsbFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYmFzZVVybDoge1xyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgZGVmYXVsdDogJydcclxuICAgICAgICB9LFxyXG4gICAgICAgIGF1dGhUb2tlbjoge1xyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgdHJhbnNsYXRpb25JdGVtczogW10sXHJcbiAgICAgICAgICAgIHN0YXRlOiAnZGlzcGxheScsXHJcbiAgICAgICAgICAgIHZhbHVlOiAnJyxcclxuICAgICAgICAgICAgb2xkVmFsdWU6ICcnXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMub2xkVmFsdWUgPSB0aGlzLnRyYW5zbGF0aW9uVmFsdWU7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHNhdmUoKSB7XHJcbiAgICAgICAgICAgIGxldCBfc2VsZiA9IHRoaXMsXHJcbiAgICAgICAgICAgICAgICB1cmwgPSBgJHt0aGlzLmJhc2VVcmx9LyR7dGhpcy5pZH0vdXBkYXRlYDtcclxuXHJcbiAgICAgICAgICAgIF9zZWxmLnN0YXRlID0gJ2Rpc3BsYXknO1xyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgICAgICAgICAgICAnWC1BdXRoLVRva2VuJzogdGhpcy5hdXRoVG9rZW5cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7dmFsdWU6IHRoaXMudmFsdWV9LFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzcygpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5vbGRWYWx1ZSA9IF9zZWxmLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnN0YXRlID0gJ2Rpc3BsYXknO1xyXG4gICAgICAgICAgICAgICAgICAgIGZsYXNoKCdUcmFuc2xhdGlvbiB0ZXh0IHdhcyB1cGRhdGVkLicsICdzdWNjZXNzJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5zdGF0ZSA9ICdkaXNwbGF5JztcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi52YWx1ZSA9IF9zZWxmLnRyYW5zbGF0aW9uVmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxhc2goZXJyb3JbJ3Jlc3BvbnNlSlNPTiddLmRldGFpbCwgJ2Vycm9yJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2FuY2VsKCkge1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5vbGRWYWx1ZTtcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZSA9ICdkaXNwbGF5JztcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgZGVmYXVsdFRyYW5zbGF0aW9uVGV4dCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGAke3RoaXMuZGVmYXVsdExvY2FsZX06ICR7dGhpcy5kZWZhdWx0VHJhbnNsYXRpb25WYWx1ZX1gO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgd2F0Y2g6IHtcclxuICAgICAgICBzdGF0ZTogZnVuY3Rpb24obmV3U3RhdGUpIHtcclxuICAgICAgICAgICAgaWYgKG5ld1N0YXRlID09PSAnZWRpdGluZycpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuJG5leHRUaWNrKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLiRyZWZzLmVkaXRGaWVsZC5mb2N1cygpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJHJlZnMuZWRpdEZpZWxkLnNlbGVjdCgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pO1xyXG4iLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcblxyXG5WdWUuY29tcG9uZW50KCd0cmFuc2xhdGlvbnMtbGlzdCcsIHtcclxuICAgIGRlbGltaXRlcnM6IFsnW1snLCAnXV0nXSxcclxuICAgIHRlbXBsYXRlOiAnI3RyYW5zbGF0aW9ucy1saXN0JyxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgYXV0aFRva2VuOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIGJhc2VVcmw6IHtcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICcnXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgdHJhbnNsYXRpb25JdGVtczogW10sXHJcbiAgICAgICAgICAgIGxvY2FsZXM6IFtdLFxyXG4gICAgICAgICAgICBkb21haW5zOiBbXSxcclxuICAgICAgICAgICAgc2VsZWN0ZWRMb2NhbGU6ICcnLFxyXG4gICAgICAgICAgICBzZWxlY3RlZERvbWFpbjogJycsXHJcbiAgICAgICAgICAgIHNlbGVjdGVkS2V5OiAnJyxcclxuICAgICAgICAgICAgc2VsZWN0ZWRWYWx1ZTogJycsXHJcbiAgICAgICAgICAgIGRlZmF1bHRMb2NhbGU6IG51bGwsXHJcbiAgICAgICAgICAgIGlzTG9hZGluZzogdHJ1ZSxcclxuICAgICAgICAgICAgdGltZW91dDogbnVsbFxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBhc3luYyBtb3VudGVkKCkge1xyXG4gICAgICAgIGF3YWl0IHRoaXMuZ2V0TG9jYWxlcygpO1xyXG4gICAgICAgIGF3YWl0IHRoaXMuZ2V0RG9tYWlucygpO1xyXG4gICAgICAgIGF3YWl0IHRoaXMuZ2V0VHJhbnNsYXRpb25zKCk7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGRlYm91bmNlZEdldFRyYW5zbGF0aW9ucygpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGltZW91dCkgY2xlYXJUaW1lb3V0KHRoaXMudGltZW91dCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0VHJhbnNsYXRpb25zKCk7XHJcbiAgICAgICAgICAgIH0sIDMwMCk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgYXN5bmMgZ2V0TG9jYWxlcygpIHtcclxuICAgICAgICAgICAgbGV0IHVybCA9IGAke3RoaXMuYmFzZVVybH0vbG9jYWxlc2AsXHJcbiAgICAgICAgICAgICAgICBfc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICBhd2FpdCAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxyXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgICAgICAgICAgICdYLUF1dGgtVG9rZW4nOiB0aGlzLmF1dGhUb2tlblxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmxvY2FsZXMgPSByZXNwb25zZS5wYXlsb2FkLmxvY2FsZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuc2VsZWN0ZWRMb2NhbGUgPSBfc2VsZi5sb2NhbGVzWzBdO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGFzeW5jIGdldERvbWFpbnMoKSB7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSBgJHt0aGlzLmJhc2VVcmx9L2RvbWFpbnNgLFxyXG4gICAgICAgICAgICAgICAgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgYXdhaXQgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnR0VUJyxcclxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgICAgICAgICAgICAnWC1BdXRoLVRva2VuJzogdGhpcy5hdXRoVG9rZW5cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5kb21haW5zID0gcmVzcG9uc2UucGF5bG9hZC5kb21haW5zO1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnNlbGVjdGVkRG9tYWluID0gX3NlbGYuZG9tYWluc1swXTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBhc3luYyBnZXRUcmFuc2xhdGlvbnMoKSB7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSBgJHt0aGlzLmJhc2VVcmx9L2xpc3Q/bG9jYWxlPSR7dGhpcy5zZWxlY3RlZExvY2FsZX0mZG9tYWluPSR7dGhpcy5zZWxlY3RlZERvbWFpbn0ma2V5PSR7dGhpcy5zZWxlY3RlZEtleX0mdmFsdWU9JHt0aGlzLnNlbGVjdGVkVmFsdWV9YCxcclxuICAgICAgICAgICAgICAgIF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgIHRoaXMuaXNMb2FkaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXHJcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJ1gtQXV0aC1Ub2tlbic6IHRoaXMuYXV0aFRva2VuXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYudHJhbnNsYXRpb25JdGVtcyA9IHJlc3BvbnNlLnBheWxvYWQudHJhbnNsYXRpb25zO1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmRlZmF1bHRMb2NhbGUgPSByZXNwb25zZS5wYXlsb2FkLmRlZmF1bHRMb2NhbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuaXNMb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5pc0xvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgY2xlYXIoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRMb2NhbGUgPSB0aGlzLmxvY2FsZXNbMF07XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWREb21haW4gPSB0aGlzLmRvbWFpbnNbMF07XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRLZXkgPSAnJztcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFZhbHVlID0gJyc7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VHJhbnNsYXRpb25zKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTtcclxuIiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgnem9waW0tY2hhdCcsIHtcclxuICAgIHRlbXBsYXRlOiAnI3pvcGltLWNoYXQnLFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGVuYWJsZVpvcGltKCkge1xyXG4gICAgICAgICAgICAkem9waW0ubGl2ZWNoYXQud2luZG93LnRvZ2dsZSgpO1xyXG4gICAgICAgIH0sXHJcbiAgICB9XHJcbn0pOyIsInZhciBtYXAgPSB7XG5cdFwiLi9DS0VkaXRvci9BcHBseUJ1dHRvbi5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL0NLRWRpdG9yL0FwcGx5QnV0dG9uLmpzXCIsXG5cdFwiLi9DS0VkaXRvci9Ib3Jpem9udGFsUnVsZS5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL0NLRWRpdG9yL0hvcml6b250YWxSdWxlLmpzXCIsXG5cdFwiLi9DS0VkaXRvci9JbnNlcnRBcHBseUJ1dHRvbi5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL0NLRWRpdG9yL0luc2VydEFwcGx5QnV0dG9uLmpzXCIsXG5cdFwiLi9DS0VkaXRvci9JbnNlcnRIb3Jpem9udGFsUnVsZS5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL0NLRWRpdG9yL0luc2VydEhvcml6b250YWxSdWxlLmpzXCIsXG5cdFwiLi9DS0VkaXRvci9JbnNlcnRJbWFnZS5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL0NLRWRpdG9yL0luc2VydEltYWdlLmpzXCIsXG5cdFwiLi9DS0VkaXRvci9JbnNlcnRJbWFnZVZpYVVSTC5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL0NLRWRpdG9yL0luc2VydEltYWdlVmlhVVJMLmpzXCIsXG5cdFwiLi9DS0VkaXRvci9XaWRnZXRzLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvQ0tFZGl0b3IvV2lkZ2V0cy5qc1wiLFxuXHRcIi4vQ0tFZGl0b3IvV2lkZ2V0c1VJLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvQ0tFZGl0b3IvV2lkZ2V0c1VJLmpzXCIsXG5cdFwiLi9DdXN0b21VcGxvYWRBZGFwdGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvQ3VzdG9tVXBsb2FkQWRhcHRlci5qc1wiLFxuXHRcIi4vYWdlX2ZpZWxkX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9hZ2VfZmllbGRfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vYXZhaWxhYmxlLXBheW1lbnQtaW5zdHJ1bWVudHNfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2F2YWlsYWJsZS1wYXltZW50LWluc3RydW1lbnRzX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL2JhY2tncm91bmRfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2JhY2tncm91bmRfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vYmFua19jYXJkX251bWJlcl9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvYmFua19jYXJkX251bWJlcl9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9ib290c3RyYXBfZmlsZV91cGxvYWRfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2Jvb3RzdHJhcF9maWxlX3VwbG9hZF9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9ib290c3RyYXBfbXVsdGlwbGVfZmlsZV91cGxvYWRfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2Jvb3RzdHJhcF9tdWx0aXBsZV9maWxlX3VwbG9hZF9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9idWxsZXRpbl9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvYnVsbGV0aW5fY29udHJvbGxlci5qc1wiLFxuXHRcIi4vY2FwdGNoYV9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvY2FwdGNoYV9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9jYXJfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2Nhcl9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9jYXJkX2RhdGFfY29uc2VudF9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvY2FyZF9kYXRhX2NvbnNlbnRfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vY2hlY2stZW1haWwtY29uZmlybWVkX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9jaGVjay1lbWFpbC1jb25maXJtZWRfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vY2tlZGl0b3JfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2NrZWRpdG9yX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL2NsaXBib2FyZF9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvY2xpcGJvYXJkX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL2NyZWRpdF9hcHBsaWNhdGlvbl9mb3JtX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9jcmVkaXRfYXBwbGljYXRpb25fZm9ybV9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9jcmVkaXRfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2NyZWRpdF9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9jcmVkaXRfcGFyYW1ldGVyc19lbGVtZW50X2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9jcmVkaXRfcGFyYW1ldGVyc19lbGVtZW50X2NvbnRyb2xsZXIuanNcIixcblx0XCIuL2NyZWRpdF9wYXJhbWV0ZXJzX2ZpZWxkX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9jcmVkaXRfcGFyYW1ldGVyc19maWVsZF9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9jdXJyZW50X2FkZHJlc3NfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2N1cnJlbnRfYWRkcmVzc19jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9jdXJyZW50X2FkZHJlc3Nfc2VjdGlvbl9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvY3VycmVudF9hZGRyZXNzX3NlY3Rpb25fY29udHJvbGxlci5qc1wiLFxuXHRcIi4vZXhwYW5kYWJsZV9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvZXhwYW5kYWJsZV9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9leHBpcmF0aW9uX2RhdGVfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2V4cGlyYXRpb25fZGF0ZV9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9mYXFfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2ZhcV9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9oZWFkZXJfbW9kdWxlX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9oZWFkZXJfbW9kdWxlX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL2liYW5fY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL2liYW5fY29udHJvbGxlci5qc1wiLFxuXHRcIi4vaW1hZ2VfZ2FsbGVyeV9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvaW1hZ2VfZ2FsbGVyeV9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9qb2Jfb3BlbmluZ19jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvam9iX29wZW5pbmdfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vbG9jYWxlX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9sb2NhbGVfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vbWFzdGVyX2ZpZWxkX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9tYXN0ZXJfZmllbGRfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vbWVzc2FnZS11bnJlYWQtY291bnRfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL21lc3NhZ2UtdW5yZWFkLWNvdW50X2NvbnRyb2xsZXIuanNcIixcblx0XCIuL21lc3NhZ2VfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL21lc3NhZ2VfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vb25saW5lX2FwcGxpY2F0aW9uX2Zvcm1fY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL29ubGluZV9hcHBsaWNhdGlvbl9mb3JtX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL3BhZ2Vfc2VsZWN0b3JfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3BhZ2Vfc2VsZWN0b3JfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vcGFzc3BvcnRfdHlwZV9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvcGFzc3BvcnRfdHlwZV9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9wZW5zaW9uZXJfZG9jdW1lbnRzX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9wZW5zaW9uZXJfZG9jdW1lbnRzX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL3Bob25lX251bWJlcl9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvcGhvbmVfbnVtYmVyX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL3BpY2tyX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9waWNrcl9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9waW5fY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3Bpbl9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9wb2xsX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9wb2xsX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL3F1ZXJ5X2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9xdWVyeV9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9zYWxhcnlfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3NhbGFyeV9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi9zY3JvbGxfdG9fZWxlbWVudF9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvc2Nyb2xsX3RvX2VsZW1lbnRfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vc2Nyb2xsX3RvX2ludmFsaWRfZm9ybV9jb250cm9sX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy9zY3JvbGxfdG9faW52YWxpZF9mb3JtX2NvbnRyb2xfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vc2hvd19oaWRlX2ZpbGVfdXBsb2FkX3NlY3Rpb25fY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3Nob3dfaGlkZV9maWxlX3VwbG9hZF9zZWN0aW9uX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL3Nob3dfaGlkZV9yZXBheW1lbnRfcGxhbl9jb250cm9sbGVyLmpzXCI6IFwiLi9hc3NldHMvanMvY29udHJvbGxlcnMvc2hvd19oaWRlX3JlcGF5bWVudF9wbGFuX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL3NvcnRfbGlua3NfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3NvcnRfbGlua3NfY29udHJvbGxlci5qc1wiLFxuXHRcIi4vc29ydGFibGVfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3NvcnRhYmxlX2NvbnRyb2xsZXIuanNcIixcblx0XCIuL3RvZ2dsZV9maWxlX3VwbG9hZF9zZWN0aW9uX2NvbnRyb2xsZXIuanNcIjogXCIuL2Fzc2V0cy9qcy9jb250cm9sbGVycy90b2dnbGVfZmlsZV91cGxvYWRfc2VjdGlvbl9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi90b2dnbGVfcGFzc3dvcmRfY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3RvZ2dsZV9wYXNzd29yZF9jb250cm9sbGVyLmpzXCIsXG5cdFwiLi90b2dnbGVfcmVwYXltZW50X3BsYW5fY29udHJvbGxlci5qc1wiOiBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzL3RvZ2dsZV9yZXBheW1lbnRfcGxhbl9jb250cm9sbGVyLmpzXCJcbn07XG5cblxuZnVuY3Rpb24gd2VicGFja0NvbnRleHQocmVxKSB7XG5cdHZhciBpZCA9IHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpO1xuXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhpZCk7XG59XG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKSB7XG5cdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8obWFwLCByZXEpKSB7XG5cdFx0dmFyIGUgPSBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiICsgcmVxICsgXCInXCIpO1xuXHRcdGUuY29kZSA9ICdNT0RVTEVfTk9UX0ZPVU5EJztcblx0XHR0aHJvdyBlO1xuXHR9XG5cdHJldHVybiBtYXBbcmVxXTtcbn1cbndlYnBhY2tDb250ZXh0LmtleXMgPSBmdW5jdGlvbiB3ZWJwYWNrQ29udGV4dEtleXMoKSB7XG5cdHJldHVybiBPYmplY3Qua2V5cyhtYXApO1xufTtcbndlYnBhY2tDb250ZXh0LnJlc29sdmUgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmU7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tDb250ZXh0O1xud2VicGFja0NvbnRleHQuaWQgPSBcIi4vYXNzZXRzL2pzL2NvbnRyb2xsZXJzIHN5bmMgcmVjdXJzaXZlIFxcXFwuanMkXCI7IiwiaW1wb3J0IHt0b1dpZGdldH0gZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS13aWRnZXQvc3JjL3V0aWxzJztcclxuaW1wb3J0IFdpZGdldCBmcm9tICdAY2tlZGl0b3IvY2tlZGl0b3I1LXdpZGdldC9zcmMvd2lkZ2V0JztcclxuaW1wb3J0IEJ1dHRvblZpZXcgZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS11aS9zcmMvYnV0dG9uL2J1dHRvbnZpZXcnO1xyXG5pbXBvcnQgSW5zZXJ0QXBwbHlCdXR0b24gZnJvbSBcIi4vSW5zZXJ0QXBwbHlCdXR0b25cIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBBcHBseUJ1dHRvblVJIHtcclxuICAgIGNvbnN0cnVjdG9yKGVkaXRvcikge1xyXG4gICAgICAgIHRoaXMuZWRpdG9yID0gZWRpdG9yO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXQoKSB7XHJcbiAgICAgICAgY29uc3QgZWRpdG9yID0gdGhpcy5lZGl0b3I7XHJcblxyXG4gICAgICAgIC8vIFRoZSBcImFwcGx5QnV0dG9uXCIgYnV0dG9uIG11c3QgYmUgcmVnaXN0ZXJlZCBhbW9uZyB0aGUgVUkgY29tcG9uZW50cyBvZiB0aGUgZWRpdG9yXHJcbiAgICAgICAgLy8gdG8gYmUgZGlzcGxheWVkIGluIHRoZSB0b29sYmFyLlxyXG4gICAgICAgIGVkaXRvci51aS5jb21wb25lbnRGYWN0b3J5LmFkZCgnYXBwbHlCdXR0b24nLCBsb2NhbGUgPT4ge1xyXG5cclxuICAgICAgICAgICAgLy8gVGhlIGJ1dHRvbiB3aWxsIGJlIGFuIGluc3RhbmNlIG9mIEJ1dHRvblZpZXcuXHJcbiAgICAgICAgICAgIGNvbnN0IGJ1dHRvblZpZXcgPSBuZXcgQnV0dG9uVmlldyhsb2NhbGUpO1xyXG5cclxuICAgICAgICAgICAgYnV0dG9uVmlldy5zZXQoe1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBcHBseSBCdXR0b24nLFxyXG4gICAgICAgICAgICAgICAgd2l0aFRleHQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICB0b29sdGlwOiB0cnVlXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgYnV0dG9uVmlldy5vbignZXhlY3V0ZScsICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWRpdG9yLmV4ZWN1dGUoJ2luc2VydEFwcGx5QnV0dG9uJylcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gYnV0dG9uVmlldztcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEFwcGx5QnV0dG9uRWRpdGluZyB7XHJcbiAgICBjb25zdHJ1Y3RvcihlZGl0b3IpIHtcclxuICAgICAgICB0aGlzLmVkaXRvciA9IGVkaXRvcjtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgZ2V0IHJlcXVpcmVzKCkge1xyXG4gICAgICAgIHJldHVybiBbV2lkZ2V0XTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0KCkge1xyXG4gICAgICAgIHRoaXMuX2RlZmluZVNjaGVtYSgpO1xyXG4gICAgICAgIHRoaXMuX2RlZmluZUNvbnZlcnRlcnMoKTtcclxuXHJcbiAgICAgICAgdGhpcy5lZGl0b3IuY29tbWFuZHMuYWRkKCdpbnNlcnRBcHBseUJ1dHRvbicsIG5ldyBJbnNlcnRBcHBseUJ1dHRvbih0aGlzLmVkaXRvcikpO1xyXG4gICAgfVxyXG5cclxuICAgIF9kZWZpbmVTY2hlbWEoKSB7XHJcbiAgICAgICAgY29uc3Qgc2NoZW1hID0gdGhpcy5lZGl0b3IubW9kZWwuc2NoZW1hO1xyXG5cclxuICAgICAgICBzY2hlbWEucmVnaXN0ZXIoJ2FwcGx5QnV0dG9uJywge1xyXG4gICAgICAgICAgICAvLyBCZWhhdmVzIGxpa2UgYSBzZWxmLWNvbnRhaW5lZCBvYmplY3QgKGUuZy4gYW4gaW1hZ2UpLlxyXG4gICAgICAgICAgICBpc09iamVjdDogdHJ1ZSxcclxuXHJcbiAgICAgICAgICAgIC8vIEFsbG93IGluIHBsYWNlcyB3aGVyZSBvdGhlciBibG9ja3MgYXJlIGFsbG93ZWQgKGUuZy4gZGlyZWN0bHkgaW4gdGhlIHJvb3QpLlxyXG4gICAgICAgICAgICBhbGxvd1doZXJlOiAnJGJsb2NrJyxcclxuXHJcbiAgICAgICAgICAgIGFsbG93QXR0cmlidXRlczogWydsaW5rSHJlZicsICdidXR0b25UZXh0J11cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfZGVmaW5lQ29udmVydGVycygpIHtcclxuICAgICAgICBjb25zdCBjb252ZXJzaW9uID0gdGhpcy5lZGl0b3IuY29udmVyc2lvbjtcclxuXHJcbiAgICAgICAgLy8gPGFwcGx5QnV0dG9uPiBjb252ZXJ0ZXJzXHJcbiAgICAgICAgY29udmVyc2lvbi5mb3IoJ3VwY2FzdCcpLmVsZW1lbnRUb0VsZW1lbnQoe1xyXG4gICAgICAgICAgICB2aWV3OiB7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnZGl2JyxcclxuICAgICAgICAgICAgICAgIGNsYXNzZXM6IFsnYXBwbHlCdXR0b24nXVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBtb2RlbDogKHZpZXdFbGVtZW50LCBtb2RlbFdyaXRlcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbGlua0hyZWYgPSB2aWV3RWxlbWVudC5nZXRDaGlsZCgwKS5nZXRBdHRyaWJ1dGUoJ2hyZWYnKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGJ1dHRvblRleHQgPSB2aWV3RWxlbWVudC5nZXRDaGlsZCgwKS5nZXRDaGlsZCgwKS5fdGV4dERhdGE7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG1vZGVsV3JpdGVyLmNyZWF0ZUVsZW1lbnQoJ2FwcGx5QnV0dG9uJywge2xpbmtIcmVmLCBidXR0b25UZXh0fSApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNvbnZlcnNpb24uZm9yKCdlZGl0aW5nRG93bmNhc3QnKS5lbGVtZW50VG9FbGVtZW50KHtcclxuICAgICAgICAgICAgbW9kZWw6ICdhcHBseUJ1dHRvbicsXHJcbiAgICAgICAgICAgIHZpZXc6IChtb2RlbEl0ZW0sIHZpZXdXcml0ZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHdpZGdldEVsZW1lbnQgPSBjcmVhdGVBcHBseUJ1dHRvblZpZXcobW9kZWxJdGVtLCB2aWV3V3JpdGVyKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBFbmFibGUgd2lkZ2V0IGhhbmRsaW5nIG9uIGEgcGxhY2Vob2xkZXIgZWxlbWVudCBpbnNpZGUgdGhlIGVkaXRpbmcgdmlldy5cclxuICAgICAgICAgICAgICAgIHJldHVybiB0b1dpZGdldCh3aWRnZXRFbGVtZW50LCB2aWV3V3JpdGVyKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb252ZXJzaW9uLmZvcignZGF0YURvd25jYXN0JykuZWxlbWVudFRvRWxlbWVudCh7XHJcbiAgICAgICAgICAgIG1vZGVsOiAnYXBwbHlCdXR0b24nLFxyXG4gICAgICAgICAgICB2aWV3OiBjcmVhdGVBcHBseUJ1dHRvblZpZXdcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gSGVscGVyIG1ldGhvZCBmb3IgYm90aCBkb3duY2FzdCBjb252ZXJ0ZXJzLlxyXG4gICAgICAgIGZ1bmN0aW9uIGNyZWF0ZUFwcGx5QnV0dG9uVmlldyhtb2RlbEl0ZW0sIHZpZXdXcml0ZXIpIHtcclxuICAgICAgICAgICAgY29uc3QgbGlua0hyZWYgPSBtb2RlbEl0ZW0uZ2V0QXR0cmlidXRlKCdsaW5rSHJlZicpO1xyXG4gICAgICAgICAgICBjb25zdCBidXR0b25UZXh0ID0gbW9kZWxJdGVtLmdldEF0dHJpYnV0ZSgnYnV0dG9uVGV4dCcpID8gbW9kZWxJdGVtLmdldEF0dHJpYnV0ZSgnYnV0dG9uVGV4dCcpIDogJ9Cf0L7QtNCw0YLQuCDQt9Cw0Y/QstC60YMnO1xyXG5cclxuICAgICAgICAgICAgLy8gQ3JlYXRlIHRoZSBjb250YWluZXIgZGl2XHJcbiAgICAgICAgICAgIGNvbnN0IGFwcGx5QnV0dG9uVmlldyA9IHZpZXdXcml0ZXIuY3JlYXRlQ29udGFpbmVyRWxlbWVudCgnZGl2Jywge1xyXG4gICAgICAgICAgICAgICAgY2xhc3M6ICdhcHBseUJ1dHRvbicsXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQ3JlYXRlIHRoZSBidXR0b25cclxuICAgICAgICAgICAgY29uc3QgdGhlQnV0dG9uID0gdmlld1dyaXRlci5jcmVhdGVDb250YWluZXJFbGVtZW50KCdhJywge1xyXG4gICAgICAgICAgICAgICAgY2xhc3M6ICdidG4gYnRuLW91dGxpbmUtZGFuZ2VyJyxcclxuICAgICAgICAgICAgICAgIGhyZWY6IGxpbmtIcmVmXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gSW5zZXJ0IHRoZSBidXR0b24ncyB0ZXh0LlxyXG4gICAgICAgICAgICBjb25zdCBpbm5lclRleHQgPSB2aWV3V3JpdGVyLmNyZWF0ZVRleHQoYnV0dG9uVGV4dCk7XHJcbiAgICAgICAgICAgIHZpZXdXcml0ZXIuaW5zZXJ0KHZpZXdXcml0ZXIuY3JlYXRlUG9zaXRpb25BdCh0aGVCdXR0b24sIDApLCBpbm5lclRleHQpO1xyXG5cclxuICAgICAgICAgICAgLy8gSW5zZXJ0IHRoZSBidXR0b25cclxuICAgICAgICAgICAgdmlld1dyaXRlci5pbnNlcnQodmlld1dyaXRlci5jcmVhdGVQb3NpdGlvbkF0KGFwcGx5QnV0dG9uVmlldywgMCksIHRoZUJ1dHRvbik7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gYXBwbHlCdXR0b25WaWV3O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSIsImltcG9ydCB7dG9XaWRnZXR9IGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtd2lkZ2V0L3NyYy91dGlscyc7XHJcbmltcG9ydCBCdXR0b25WaWV3IGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtdWkvc3JjL2J1dHRvbi9idXR0b252aWV3JztcclxuaW1wb3J0IEluc2VydEhvcml6b250YWxSdWxlIGZyb20gJy4vSW5zZXJ0SG9yaXpvbnRhbFJ1bGUnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSG9yaXpvbnRhbFJ1bGUge1xyXG4gICAgY29uc3RydWN0b3IoZWRpdG9yKSB7XHJcbiAgICAgICAgdGhpcy5lZGl0b3IgPSBlZGl0b3I7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdCgpIHtcclxuICAgICAgICBjb25zdCBlZGl0b3IgPSB0aGlzLmVkaXRvcjtcclxuICAgICAgICBjb25zdCBtb2RlbCA9IGVkaXRvci5tb2RlbDtcclxuICAgICAgICBjb25zdCBjb252ZXJzaW9uID0gZWRpdG9yLmNvbnZlcnNpb247XHJcbiAgICAgICAgY29uc3Qgc2NoZW1hID0gbW9kZWwuc2NoZW1hO1xyXG5cclxuICAgICAgICB0aGlzLmVkaXRvci5jb21tYW5kcy5hZGQoJ2luc2VydEhvcml6b250YWxSdWxlJywgbmV3IEluc2VydEhvcml6b250YWxSdWxlKHRoaXMuZWRpdG9yKSk7XHJcblxyXG4gICAgICAgIC8vIENvbmZpZ3VyZSBuZXcgaG9yaXpvbnRhbCBydWxlIHNjaGVtYVxyXG4gICAgICAgIHNjaGVtYS5yZWdpc3RlcignaG9yaXpvbnRhbFJ1bGUnLCB7XHJcbiAgICAgICAgICAgIGFsbG93SW46ICckcm9vdCcsXHJcbiAgICAgICAgICAgIGlzT2JqZWN0OiB0cnVlLFxyXG4gICAgICAgICAgICBpc0Jsb2NrOiB0cnVlLFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb252ZXJzaW9uLmZvcigndXBjYXN0JykuZWxlbWVudFRvRWxlbWVudCh7XHJcbiAgICAgICAgICAgIHZpZXc6IHtcclxuICAgICAgICAgICAgICAgIG5hbWU6ICdocicsXHJcbiAgICAgICAgICAgICAgICBjbGFzc2VzOiBbJ2hvcml6b250YWxSdWxlJ11cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgbW9kZWw6ICh2aWV3RWxlbWVudCwgbW9kZWxXcml0ZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBtb2RlbFdyaXRlci5jcmVhdGVFbGVtZW50KCdob3Jpem9udGFsUnVsZScpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNvbnZlcnNpb24uZm9yKCdlZGl0aW5nRG93bmNhc3QnKS5lbGVtZW50VG9FbGVtZW50KHtcclxuICAgICAgICAgICAgbW9kZWw6ICdob3Jpem9udGFsUnVsZScsXHJcbiAgICAgICAgICAgIHZpZXc6IChtb2RlbEl0ZW0sIHZpZXdXcml0ZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHdpZGdldEVsZW1lbnQgPSBjcmVhdGVIb3Jpem9udGFsUnVsZVZpZXcobW9kZWxJdGVtLCB2aWV3V3JpdGVyKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBFbmFibGUgd2lkZ2V0IGhhbmRsaW5nIG9uIGEgcGxhY2Vob2xkZXIgZWxlbWVudCBpbnNpZGUgdGhlIGVkaXRpbmcgdmlldy5cclxuICAgICAgICAgICAgICAgIHJldHVybiB0b1dpZGdldCh3aWRnZXRFbGVtZW50LCB2aWV3V3JpdGVyKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb252ZXJzaW9uLmZvcignZGF0YURvd25jYXN0JykuZWxlbWVudFRvRWxlbWVudCh7XHJcbiAgICAgICAgICAgIG1vZGVsOiAnaG9yaXpvbnRhbFJ1bGUnLFxyXG4gICAgICAgICAgICB2aWV3OiBjcmVhdGVIb3Jpem9udGFsUnVsZVZpZXdcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gSGVscGVyIG1ldGhvZCBmb3IgYm90aCBkb3duY2FzdCBjb252ZXJ0ZXJzLlxyXG4gICAgICAgIGZ1bmN0aW9uIGNyZWF0ZUhvcml6b250YWxSdWxlVmlldyhtb2RlbEl0ZW0sIHZpZXdXcml0ZXIpIHtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiB2aWV3V3JpdGVyLmNyZWF0ZUNvbnRhaW5lckVsZW1lbnQoJ2hyJywge2NsYXNzOiAnaG9yaXpvbnRhbFJ1bGUnfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBlZGl0b3IudWkuY29tcG9uZW50RmFjdG9yeS5hZGQoJ2hvcml6b250YWxSdWxlJywgbG9jYWxlID0+IHtcclxuICAgICAgICAgICAgY29uc3QgdmlldyA9IG5ldyBCdXR0b25WaWV3KGxvY2FsZSk7XHJcblxyXG4gICAgICAgICAgICB2aWV3LnNldCh7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0hvcml6b250YWwgTGluZScsXHJcbiAgICAgICAgICAgICAgICB3aXRoVGV4dDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIHRvb2x0aXA6IHRydWVcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBDYWxsYmFjayBleGVjdXRlZCBvbmNlIHRoZSBidXR0b24gaXMgY2xpY2tlZC5cclxuICAgICAgICAgICAgdmlldy5vbignZXhlY3V0ZScsICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWRpdG9yLmV4ZWN1dGUoJ2luc2VydEhvcml6b250YWxSdWxlJylcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdmlldztcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufSIsImV4cG9ydCBkZWZhdWx0IGNsYXNzIEluc2VydEFwcGx5QnV0dG9uIHtcclxuICAgIGNvbnN0cnVjdG9yKGVkaXRvcikge1xyXG4gICAgICAgIHRoaXMuZWRpdG9yID0gZWRpdG9yO1xyXG4gICAgfVxyXG5cclxuICAgIGV4ZWN1dGUoKSB7XHJcbiAgICAgICAgY29uc3QgbGlua0hyZWYgPSBwcm9tcHQoJ0J1dHRvbiBVUkwnKTtcclxuICAgICAgICBjb25zdCBidXR0b25UZXh0ID0gcHJvbXB0KCdCdXR0b24gdGV4dCcsICfQn9C+0LTQsNGC0Lgg0LfQsNGP0LLQutGDJyk7XHJcblxyXG4gICAgICAgIHRoaXMuZWRpdG9yLm1vZGVsLmNoYW5nZSh3cml0ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBidXR0b25FbGVtZW50ID0gd3JpdGVyLmNyZWF0ZUVsZW1lbnQoJ2FwcGx5QnV0dG9uJywge1xyXG4gICAgICAgICAgICAgICAgbGlua0hyZWY6IGxpbmtIcmVmLFxyXG4gICAgICAgICAgICAgICAgYnV0dG9uVGV4dDogYnV0dG9uVGV4dFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEluc2VydCB0aGUgaW1hZ2UgaW4gdGhlIGN1cnJlbnQgc2VsZWN0aW9uIGxvY2F0aW9uLlxyXG4gICAgICAgICAgICB0aGlzLmVkaXRvci5tb2RlbC5pbnNlcnRDb250ZW50KGJ1dHRvbkVsZW1lbnQsIHRoaXMuZWRpdG9yLm1vZGVsLmRvY3VtZW50LnNlbGVjdGlvbik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaCgpIHtcclxuICAgIH1cclxufSIsImV4cG9ydCBkZWZhdWx0IGNsYXNzIEluc2VydEhvcml6b250YWxSdWxlIHtcclxuICAgIGNvbnN0cnVjdG9yKGVkaXRvcikge1xyXG4gICAgICAgIHRoaXMuZWRpdG9yID0gZWRpdG9yO1xyXG4gICAgfVxyXG5cclxuICAgIGV4ZWN1dGUoKSB7XHJcbiAgICAgICAgdGhpcy5lZGl0b3IubW9kZWwuY2hhbmdlKHdyaXRlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGhvcml6b250YWxSdWxlRWxlbWVudCA9IHdyaXRlci5jcmVhdGVFbGVtZW50KCdob3Jpem9udGFsUnVsZScpO1xyXG5cclxuICAgICAgICAgICAgLy8gSW5zZXJ0IHRoZSBociBhdCB0aGUgZW5kIG9mIHRoZSBzZWxlY3Rpb25cclxuICAgICAgICAgICAgd3JpdGVyLmluc2VydChob3Jpem9udGFsUnVsZUVsZW1lbnQsIHRoaXMuZWRpdG9yLm1vZGVsLmRvY3VtZW50LnNlbGVjdGlvbi5mb2N1cyk7XHJcbiAgICAgICAgICAgIHdyaXRlci5zZXRTZWxlY3Rpb24oaG9yaXpvbnRhbFJ1bGVFbGVtZW50LCAnYWZ0ZXInKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoKCkge1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IEJ1dHRvblZpZXcgZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS11aS9zcmMvYnV0dG9uL2J1dHRvbnZpZXcnO1xyXG5pbXBvcnQgaW1hZ2VJY29uIGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtY29yZS90aGVtZS9pY29ucy9pbWFnZS5zdmcnO1xyXG5pbXBvcnQgSW5zZXJ0SW1hZ2VWaWFVUkwgZnJvbSBcIi4vSW5zZXJ0SW1hZ2VWaWFVUkxcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEluc2VydEltYWdlIHtcclxuICAgIGNvbnN0cnVjdG9yKGVkaXRvcikge1xyXG4gICAgICAgIHRoaXMuZWRpdG9yID0gZWRpdG9yO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXQoKSB7XHJcbiAgICAgICAgY29uc3QgZWRpdG9yID0gdGhpcy5lZGl0b3I7XHJcblxyXG4gICAgICAgIHRoaXMuZWRpdG9yLmNvbW1hbmRzLmFkZCgnaW5zZXJ0SW1hZ2VWaWFVUkwnLCBuZXcgSW5zZXJ0SW1hZ2VWaWFVUkwodGhpcy5lZGl0b3IpKTtcclxuXHJcbiAgICAgICAgZWRpdG9yLnVpLmNvbXBvbmVudEZhY3RvcnkuYWRkKCdpbnNlcnRJbWFnZScsIGxvY2FsZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZpZXcgPSBuZXcgQnV0dG9uVmlldyhsb2NhbGUpO1xyXG5cclxuICAgICAgICAgICAgdmlldy5zZXQoe1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdJbnNlcnQgaW1hZ2UnLFxyXG4gICAgICAgICAgICAgICAgaWNvbjogaW1hZ2VJY29uLFxyXG4gICAgICAgICAgICAgICAgdG9vbHRpcDogdHJ1ZVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIENhbGxiYWNrIGV4ZWN1dGVkIG9uY2UgdGhlIGltYWdlIGlzIGNsaWNrZWQuXHJcbiAgICAgICAgICAgIHZpZXcub24oJ2V4ZWN1dGUnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBpbWFnZVVybCA9IHByb21wdCgnSW1hZ2UgVVJMJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgZWRpdG9yLm1vZGVsLmNoYW5nZSh3cml0ZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGltYWdlRWxlbWVudCA9IHdyaXRlci5jcmVhdGVFbGVtZW50KCdpbWFnZScsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3JjOiBpbWFnZVVybFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBJbnNlcnQgdGhlIGltYWdlIGluIHRoZSBjdXJyZW50IHNlbGVjdGlvbiBsb2NhdGlvbi5cclxuICAgICAgICAgICAgICAgICAgICBlZGl0b3IubW9kZWwuaW5zZXJ0Q29udGVudChpbWFnZUVsZW1lbnQsIGVkaXRvci5tb2RlbC5kb2N1bWVudC5zZWxlY3Rpb24pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHZpZXc7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0iLCJleHBvcnQgZGVmYXVsdCBjbGFzcyBJbnNlcnRJbWFnZVZpYVVSTCB7XHJcbiAgICBjb25zdHJ1Y3RvcihlZGl0b3IpIHtcclxuICAgICAgICB0aGlzLmVkaXRvciA9IGVkaXRvcjtcclxuICAgIH1cclxuXHJcbiAgICBleGVjdXRlKCkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHByb21wdCgnSW1hZ2UgVVJMJyk7XHJcblxyXG4gICAgICAgIHRoaXMuZWRpdG9yLm1vZGVsLmNoYW5nZSh3cml0ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBpbWFnZUVsZW1lbnQgPSB3cml0ZXIuY3JlYXRlRWxlbWVudCgnaW1hZ2UnLCB7XHJcbiAgICAgICAgICAgICAgICBzcmM6IHVybFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEluc2VydCB0aGUgaW1hZ2UgaW4gdGhlIGN1cnJlbnQgc2VsZWN0aW9uIGxvY2F0aW9uLlxyXG4gICAgICAgICAgICB0aGlzLmVkaXRvci5tb2RlbC5pbnNlcnRDb250ZW50KGltYWdlRWxlbWVudCwgdGhpcy5lZGl0b3IubW9kZWwuZG9jdW1lbnQuc2VsZWN0aW9uKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoKCkge1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHtBcHBseUJ1dHRvbkVkaXRpbmcsIEFwcGx5QnV0dG9uVUl9IGZyb20gJy4vQXBwbHlCdXR0b24nO1xyXG5pbXBvcnQgSG9yaXpvbnRhbFJ1bGUgZnJvbSAnLi9Ib3Jpem9udGFsUnVsZSc7XHJcbmltcG9ydCBXaWRnZXRzVUkgZnJvbSAnLi9XaWRnZXRzVUknO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgV2lkZ2V0cyB7XHJcbiAgICBjb25zdHJ1Y3RvcihlZGl0b3IpIHtcclxuICAgICAgICB0aGlzLmVkaXRvciA9IGVkaXRvcjtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgZ2V0IHJlcXVpcmVzKCkge1xyXG4gICAgICAgIHJldHVybiBbQXBwbHlCdXR0b25FZGl0aW5nLCBIb3Jpem9udGFsUnVsZSwgQXBwbHlCdXR0b25VSSwgV2lkZ2V0c1VJXTtcclxuICAgIH1cclxufVxyXG5cclxuIiwiaW1wb3J0IHthZGRMaXN0VG9Ecm9wZG93biwgY3JlYXRlRHJvcGRvd259IGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtdWkvc3JjL2Ryb3Bkb3duL3V0aWxzJztcclxuXHJcbmltcG9ydCBDb2xsZWN0aW9uIGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtdXRpbHMvc3JjL2NvbGxlY3Rpb24nO1xyXG5pbXBvcnQgTW9kZWwgZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS11aS9zcmMvbW9kZWwnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgV2lkZ2V0c1VJIHtcclxuICAgIGNvbnN0cnVjdG9yKGVkaXRvcikge1xyXG4gICAgICAgIHRoaXMuZWRpdG9yID0gZWRpdG9yO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXQoKSB7XHJcbiAgICAgICAgY29uc3QgZWRpdG9yID0gdGhpcy5lZGl0b3I7XHJcblxyXG4gICAgICAgIC8vIFRoZSBcInBsYWNlaG9sZGVyXCIgZHJvcGRvd24gbXVzdCBiZSByZWdpc3RlcmVkIGFtb25nIHRoZSBVSSBjb21wb25lbnRzIG9mIHRoZSBlZGl0b3JcclxuICAgICAgICAvLyB0byBiZSBkaXNwbGF5ZWQgaW4gdGhlIHRvb2xiYXIuXHJcbiAgICAgICAgZWRpdG9yLnVpLmNvbXBvbmVudEZhY3RvcnkuYWRkKCd3aWRnZXRzJywgbG9jYWxlID0+IHtcclxuICAgICAgICAgICAgY29uc3QgZHJvcGRvd25WaWV3ID0gY3JlYXRlRHJvcGRvd24obG9jYWxlKTtcclxuXHJcbiAgICAgICAgICAgIC8vIFBvcHVsYXRlIHRoZSBsaXN0IGluIHRoZSBkcm9wZG93biB3aXRoIGl0ZW1zLlxyXG4gICAgICAgICAgICBhZGRMaXN0VG9Ecm9wZG93bihkcm9wZG93blZpZXcsIGdldERyb3Bkb3duSXRlbXNEZWZpbml0aW9ucygpKTtcclxuXHJcbiAgICAgICAgICAgIGRyb3Bkb3duVmlldy5idXR0b25WaWV3LnNldCh7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1dpZGdldHMnLFxyXG4gICAgICAgICAgICAgICAgdG9vbHRpcDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIHdpdGhUZXh0OiB0cnVlXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gRXhlY3V0ZSB0aGUgY29tbWFuZCB3aGVuIHRoZSBkcm9wZG93biBpdGVtIGlzIGNsaWNrZWQgKGV4ZWN1dGVkKS5cclxuICAgICAgICAgICAgZHJvcGRvd25WaWV3Lm9uKCdleGVjdXRlJywgKGV2dCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgZWRpdG9yLmV4ZWN1dGUoZXZ0LnNvdXJjZS5jb21tYW5kUGFyYW0pO1xyXG4gICAgICAgICAgICAgICAgZWRpdG9yLmVkaXRpbmcudmlldy5mb2N1cygpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBkcm9wZG93blZpZXc7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldERyb3Bkb3duSXRlbXNEZWZpbml0aW9ucygpIHtcclxuICAgIGNvbnN0IGl0ZW1EZWZpbml0aW9ucyA9IG5ldyBDb2xsZWN0aW9uKCk7XHJcblxyXG4gICAgY29uc3QgaW1hZ2VEZWZpbml0aW9uID0ge1xyXG4gICAgICAgIHR5cGU6ICdidXR0b24nLFxyXG4gICAgICAgIG1vZGVsOiBuZXcgTW9kZWwoe1xyXG4gICAgICAgICAgICBjb21tYW5kUGFyYW06ICdpbnNlcnRJbWFnZVZpYVVSTCcsXHJcbiAgICAgICAgICAgIGxhYmVsOiAnSW1hZ2UgdmlhIFVSTCcsXHJcbiAgICAgICAgICAgIHdpdGhUZXh0OiB0cnVlXHJcbiAgICAgICAgfSlcclxuICAgIH07XHJcblxyXG4gICAgLy8gQWRkIHRoZSBpdGVtIGRlZmluaXRpb24gdG8gdGhlIGNvbGxlY3Rpb24uXHJcbiAgICBpdGVtRGVmaW5pdGlvbnMuYWRkKGltYWdlRGVmaW5pdGlvbik7XHJcblxyXG4gICAgY29uc3QgYXBwbHlCdXR0b25EZWZpbml0aW9uID0ge1xyXG4gICAgICAgIHR5cGU6ICdidXR0b24nLFxyXG4gICAgICAgIG1vZGVsOiBuZXcgTW9kZWwoe1xyXG4gICAgICAgICAgICBjb21tYW5kUGFyYW06ICdpbnNlcnRBcHBseUJ1dHRvbicsXHJcbiAgICAgICAgICAgIGxhYmVsOiAnQXBwbHkgQnV0dG9uJyxcclxuICAgICAgICAgICAgd2l0aFRleHQ6IHRydWVcclxuICAgICAgICB9KVxyXG4gICAgfTtcclxuXHJcbiAgICAvLyBBZGQgdGhlIGl0ZW0gZGVmaW5pdGlvbiB0byB0aGUgY29sbGVjdGlvbi5cclxuICAgIGl0ZW1EZWZpbml0aW9ucy5hZGQoYXBwbHlCdXR0b25EZWZpbml0aW9uKTtcclxuXHJcbiAgICBjb25zdCBob3Jpem9udGFsUnVsZURlZmluaXRpb24gPSB7XHJcbiAgICAgICAgdHlwZTogJ2J1dHRvbicsXHJcbiAgICAgICAgbW9kZWw6IG5ldyBNb2RlbCh7XHJcbiAgICAgICAgICAgIGNvbW1hbmRQYXJhbTogJ2luc2VydEhvcml6b250YWxSdWxlJyxcclxuICAgICAgICAgICAgbGFiZWw6ICdIb3Jpem9udGFsIExpbmUnLFxyXG4gICAgICAgICAgICB3aXRoVGV4dDogdHJ1ZVxyXG4gICAgICAgIH0pXHJcbiAgICB9O1xyXG5cclxuICAgIC8vIEFkZCB0aGUgaXRlbSBkZWZpbml0aW9uIHRvIHRoZSBjb2xsZWN0aW9uLlxyXG4gICAgaXRlbURlZmluaXRpb25zLmFkZChob3Jpem9udGFsUnVsZURlZmluaXRpb24pO1xyXG5cclxuICAgIHJldHVybiBpdGVtRGVmaW5pdGlvbnM7XHJcbn0iLCJleHBvcnQgZGVmYXVsdCBjbGFzcyBDdXN0b21VcGxvYWRBZGFwdGVyIHtcclxuICAgIGNvbnN0cnVjdG9yKGxvYWRlciwgdXBsb2FkVXJsKSB7XHJcbiAgICAgICAgLy8gQ0tFZGl0b3IgNSdzIEZpbGVMb2FkZXIgaW5zdGFuY2UuXHJcbiAgICAgICAgdGhpcy5sb2FkZXIgPSBsb2FkZXI7XHJcbiAgICAgICAgdGhpcy54aHIgPSBudWxsO1xyXG4gICAgICAgIHRoaXMudXBsb2FkVXJsID0gdXBsb2FkVXJsO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFN0YXJ0cyB0aGUgdXBsb2FkIHByb2Nlc3MuXHJcbiAgICB1cGxvYWQoKSB7XHJcbiAgICAgICAgbGV0IHVybCA9IHRoaXMudXBsb2FkVXJsLFxyXG4gICAgICAgICAgICBzZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMubG9hZGVyLmZpbGVcclxuICAgICAgICAgICAgLnRoZW4oIHVwbG9hZGVkRmlsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoICggcmVzb2x2ZSwgcmVqZWN0ICkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGRhdGEgPSBuZXcgRm9ybURhdGEoKTtcclxuICAgICAgICAgICAgICAgICAgICBkYXRhLmFwcGVuZCggJ3VwbG9hZCcsIHVwbG9hZGVkRmlsZSApO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBzZWxmLnhociA9ICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50VHlwZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3NEYXRhOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OiByZXNwb25zZS51cmxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcjogKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgZGF0YSA9IHJlc3BvbnNlLnJlc3BvbnNlSlNPTjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdChkYXRhICYmIGRhdGEuZXJyb3IgPyBkYXRhLmVycm9yLm1lc3NhZ2UgOiAnVXBsb2FkIGZhaWxlZC4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSApO1xyXG4gICAgICAgICAgICB9ICk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQWJvcnRzIHRoZSB1cGxvYWQgcHJvY2Vzcy5cclxuICAgIGFib3J0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnhocikge1xyXG4gICAgICAgICAgICB0aGlzLnhoci5hYm9ydCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgY29ubmVjdCgpe1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ3Bpbi11cGRhdGVkJywgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChkYXRhLnZhbHVlID49IHRoaXMuYWdlTGltaXQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGFnZUxpbWl0KCkge1xyXG4gICAgICAgIHJldHVybiBwYXJzZUludCh0aGlzLmVsZW1lbnQuZGF0YXNldC5hZ2VMaW1pdCk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIGNvbm5lY3QoKXtcclxuICAgICAgICBFdmVudE1hbmFnZXIubGlzdGVuKCdtYXN0ZXItZmllbGQtY2hhbmdlZCcsIGRhdGEgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZGF0YS5wcm9wZXJ0eU5hbWUgPT09IHRoaXMucHJvcGVydHlOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YS52YWx1ZSA9PT0gJzNlNzExNWY0LWViMmUtNDJiMi04ZGYyLWU1YzVlZDM0ZmFkMycpIHsgLy8gQmFuayBhY2NvdW50XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZSgncmVxdWlyZWQnLCAncmVxdWlyZWQnKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LnJlbW92ZUF0dHJpYnV0ZSgncmVxdWlyZWQnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHByb3BlcnR5TmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQucHJvcGVydHlOYW1lO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHtDb250cm9sbGVyfSBmcm9tIFwic3RpbXVsdXNcIlxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIGNvbm5lY3QoKSB7XHJcbiAgICAgICAgdGhpcy5pZCA9IHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2lkJyk7XHJcblxyXG4gICAgICAgIHRoaXMuY3JlYXRlU3R5bGVUYWcodGhpcy5kYXRhLmdldCgnaW1hZ2UnKSk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlU3R5bGVUYWcoKSB7XHJcbiAgICAgICAgbGV0IHN0eWxlRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3N0eWxlJyk7XHJcblxyXG4gICAgICAgIGxldCBjc3MgPSBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShgXHJcbiAgICAgICAgICAgICR7dGhpcy5pbWFnZUNTU31cclxuICAgICAgICAgICAgJHt0aGlzLmltYWdlTW9iaWxlQ1NTfVxyXG4gICAgICAgIGApO1xyXG4gICAgICAgIHN0eWxlRWxlbWVudC5hcHBlbmQoY3NzKTtcclxuXHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignaGVhZCcpLmFwcGVuZChzdHlsZUVsZW1lbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpbWFnZUNTUygpIHtcclxuICAgICAgICBpZiAodGhpcy5kYXRhLmhhcygnaW1hZ2UnKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gYFxyXG4gICAgICAgICAgICAjJHt0aGlzLmlkfSB7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJHt0aGlzLmRhdGEuZ2V0KCdpbWFnZScpfSk7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAnJztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaW1hZ2VNb2JpbGVDU1MoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGF0YS5oYXMoJ2ltYWdlTW9iaWxlJykpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGBcclxuICAgICAgICAgICAgQG1lZGlhKG1heC13aWR0aDo1NzZweCkge1xyXG4gICAgICAgICAgICAgICAgIyR7dGhpcy5pZH0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgke3RoaXMuZGF0YS5nZXQoJ2ltYWdlTW9iaWxlJyl9KTtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBgO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHtDb250cm9sbGVyfSBmcm9tIFwic3RpbXVsdXNcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCl7XHJcbiAgICAgICAgRXZlbnRNYW5hZ2VyLmxpc3RlbignbWFzdGVyLWZpZWxkLWNoYW5nZWQnLCBkYXRhID0+IHtcclxuICAgICAgICAgICAgaWYgKGRhdGEucHJvcGVydHlOYW1lID09PSB0aGlzLnByb3BlcnR5TmFtZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEudmFsdWUgPT09ICczZTcxMTVmNC1lYjJlLTQyYjItOGRmMi1lNWM1ZWQzNGZhZDMnKSB7IC8vIEJhbmsgYWNjb3VudFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2Qtbm9uZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKCdzZWxlY3QnKS5mb3JFYWNoKGVsZW1lbnQgPT4gZWxlbWVudC52YWx1ZSA9ICcnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHByb3BlcnR5TmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQucHJvcGVydHlOYW1lO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHtDb250cm9sbGVyfSBmcm9tIFwic3RpbXVsdXNcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIGxldCBsYWJlbCA9IHRoaXMuZWxlbWVudC5wYXJlbnROb2RlLnF1ZXJ5U2VsZWN0b3IoJy5jdXN0b20tZmlsZS1sYWJlbCcpO1xyXG4gICAgICAgIGxhYmVsLnNldEF0dHJpYnV0ZSgnZGF0YS1icm93c2UnLCB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLWJyb3dzZS1idXR0b24nKSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlKCkge1xyXG4gICAgICAgIGxldCBsYWJlbCA9IHRoaXMuZWxlbWVudC5wYXJlbnROb2RlLnF1ZXJ5U2VsZWN0b3IoJy5jdXN0b20tZmlsZS1sYWJlbCcpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5lbGVtZW50LmZpbGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgbGFiZWwuaW5uZXJIVE1MID0gXCI8c3Bhbj5cIiArIHRoaXMuZWxlbWVudC5maWxlc1swXS5uYW1lICsgXCI8L3NwYW4+XCI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbGFiZWwuaW5uZXJIVE1MID0gXCI8c3Bhbj48L3NwYW4+XCI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiaW1wb3J0IHtDb250cm9sbGVyfSBmcm9tIFwic3RpbXVsdXNcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIGxldCBsYWJlbCA9IHRoaXMuZWxlbWVudC5wYXJlbnROb2RlLnF1ZXJ5U2VsZWN0b3IoJy5jdXN0b20tZmlsZS1sYWJlbCcpO1xyXG4gICAgICAgIGxhYmVsLnNldEF0dHJpYnV0ZSgnZGF0YS1icm93c2UnLCB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLWJyb3dzZS1idXR0b24nKSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlKCkge1xyXG4gICAgICAgIGxldCBsYWJlbCA9IHRoaXMuZWxlbWVudC5wYXJlbnROb2RlLnF1ZXJ5U2VsZWN0b3IoJy5jdXN0b20tZmlsZS1sYWJlbCcpLFxyXG4gICAgICAgICAgICBmaWxlTmFtZXMgPSBbXTtcclxuXHJcbiAgICAgICAgdGhpcy5lbGVtZW50LmZpbGVzLmZvckVhY2goZmlsZSA9PiB7XHJcbiAgICAgICAgICAgZmlsZU5hbWVzLnB1c2goZmlsZS5uYW1lKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgbGV0IGZpbGVOYW1lc1N0cmluZyA9IGZpbGVOYW1lcy5qb2luKCcsICcpO1xyXG5cclxuICAgICAgICBsYWJlbC5pbm5lckhUTUwgPSBcIjxzcGFuPlwiICsgZmlsZU5hbWVzU3RyaW5nICsgXCI8L3NwYW4+XCI7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgc3RhdGljIHRhcmdldHMgPSBbJ2VtYWlsJ107XHJcblxyXG4gICAgc3Vic2NyaWJlKGV2ZW50KSB7XHJcbiAgICAgICAgbGV0IGVtYWlsID0gdGhpcy5lbWFpbFRhcmdldC52YWx1ZSxcclxuICAgICAgICAgICAgdXJsID0gdGhpcy5kYXRhLmdldCgnc3Vic2NyaWJlVXJsJyksXHJcbiAgICAgICAgICAgIF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgIHVybDogdXJsLFxyXG4gICAgICAgICAgICBkYXRhOiB7J2VtYWlsJzogZW1haWx9LFxyXG4gICAgICAgICAgICBzdWNjZXNzKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICBmbGFzaChyZXNwb25zZSwgJ3N1Y2Nlc3MnKTtcclxuICAgICAgICAgICAgICAgIF9zZWxmLmVtYWlsVGFyZ2V0LnZhbHVlID0gJyc7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICBmbGFzaChlcnJvclsncmVzcG9uc2VKU09OJ10sICdlcnJvcicpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgY29ubmVjdCgpIHtcclxuICAgICAgICBsZXQgYXBpS2V5ID0gdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1hcGkta2V5Jyk7XHJcbiAgICAgICAgbGV0IGNhcHRoYUlucHV0ID0gdGhpcy5lbGVtZW50O1xyXG4gICAgICAgIGdyZWNhcHRjaGEucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBncmVjYXB0Y2hhLmV4ZWN1dGUoYXBpS2V5LCB7YWN0aW9uOiAnaG9tZXBhZ2UnfVxyXG4gICAgICAgICkudGhlbihmdW5jdGlvbiAodG9rZW4pIHtcclxuICAgICAgICAgICAgICAgIGNhcHRoYUlucHV0LnZhbHVlID0gdG9rZW47XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIGNvbm5lY3QoKSB7XHJcbiAgICAgICAgRXZlbnRNYW5hZ2VyLmxpc3RlbignbWFzdGVyLWZpZWxkLWNoYW5nZWQnLCBkYXRhID0+IHtcclxuICAgICAgICAgICAgaWYgKGRhdGEucHJvcGVydHlOYW1lID09PSB0aGlzLnByb3BlcnR5TmFtZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEudmFsdWUgPT09ICcxJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2Qtbm9uZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcHJvcGVydHlOYW1lKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVsZW1lbnQuZGF0YXNldC5wcm9wZXJ0eU5hbWU7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIGNvbm5lY3QoKXtcclxuICAgICAgICBFdmVudE1hbmFnZXIubGlzdGVuKCdtYXN0ZXItZmllbGQtY2hhbmdlZCcsIGRhdGEgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZGF0YS5wcm9wZXJ0eU5hbWUgPT09IHRoaXMucHJvcGVydHlOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YS52YWx1ZSA9PT0gJzNlNzExNWY0LWViMmUtNDJiMi04ZGYyLWU1YzVlZDM0ZmFkMycpIHsgLy8gQmFuayBhY2NvdW50XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ2Qtbm9uZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKCdpbnB1dCcpLnNldEF0dHJpYnV0ZSgncmVxdWlyZWQnLCAncmVxdWlyZWQnKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2Qtbm9uZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKCdpbnB1dCcpLnJlbW92ZUF0dHJpYnV0ZSgncmVxdWlyZWQnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHByb3BlcnR5TmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQucHJvcGVydHlOYW1lO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gJ3N0aW11bHVzJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlclxyXG57XHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIGZldGNoKHRoaXMudXJsLCB7bWV0aG9kOiAnUE9TVCd9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgdXJsKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVsZW1lbnQuZGF0YXNldC51cmw7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuaW1wb3J0IENsYXNzaWNFZGl0b3IgZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS1lZGl0b3ItY2xhc3NpYy9zcmMvY2xhc3NpY2VkaXRvcic7XHJcbmltcG9ydCBFc3NlbnRpYWxzUGx1Z2luIGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtZXNzZW50aWFscy9zcmMvZXNzZW50aWFscyc7XHJcbmltcG9ydCBIZWFkaW5nIGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtaGVhZGluZy9zcmMvaGVhZGluZyc7XHJcbmltcG9ydCBCb2xkUGx1Z2luIGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtYmFzaWMtc3R5bGVzL3NyYy9ib2xkJztcclxuaW1wb3J0IEl0YWxpY1BsdWdpbiBmcm9tICdAY2tlZGl0b3IvY2tlZGl0b3I1LWJhc2ljLXN0eWxlcy9zcmMvaXRhbGljJztcclxuaW1wb3J0IExpbmtQbHVnaW4gZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS1saW5rL3NyYy9saW5rJztcclxuaW1wb3J0IExpc3QgZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS1saXN0L3NyYy9saXN0JztcclxuaW1wb3J0IEltYWdlIGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtaW1hZ2Uvc3JjL2ltYWdlJztcclxuaW1wb3J0IEltYWdlVG9vbGJhciBmcm9tICdAY2tlZGl0b3IvY2tlZGl0b3I1LWltYWdlL3NyYy9pbWFnZXRvb2xiYXInO1xyXG5pbXBvcnQgSW1hZ2VDYXB0aW9uIGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtaW1hZ2Uvc3JjL2ltYWdlY2FwdGlvbic7XHJcbmltcG9ydCBJbWFnZVN0eWxlIGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtaW1hZ2Uvc3JjL2ltYWdlc3R5bGUnO1xyXG5pbXBvcnQgSW1hZ2VSZXNpemUgZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS1pbWFnZS9zcmMvaW1hZ2VyZXNpemUnO1xyXG5pbXBvcnQgSW1hZ2VVcGxvYWQgZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS1pbWFnZS9zcmMvaW1hZ2V1cGxvYWQnO1xyXG5pbXBvcnQgTWVkaWFFbWJlZCBmcm9tICdAY2tlZGl0b3IvY2tlZGl0b3I1LW1lZGlhLWVtYmVkL3NyYy9tZWRpYWVtYmVkJztcclxuaW1wb3J0IFRhYmxlIGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtdGFibGUvc3JjL3RhYmxlJztcclxuaW1wb3J0IEJsb2NrUXVvdGUgZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS1ibG9jay1xdW90ZS9zcmMvYmxvY2txdW90ZSc7XHJcbmltcG9ydCBQYXJhZ3JhcGhQbHVnaW4gZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS1wYXJhZ3JhcGgvc3JjL3BhcmFncmFwaCc7XHJcbmltcG9ydCBBbGlnbm1lbnQgZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS1hbGlnbm1lbnQvc3JjL2FsaWdubWVudCc7XHJcbmltcG9ydCBGb250IGZyb20gJ0Bja2VkaXRvci9ja2VkaXRvcjUtZm9udC9zcmMvZm9udCc7XHJcbmltcG9ydCBDS0ZpbmRlciBmcm9tICdAY2tlZGl0b3IvY2tlZGl0b3I1LWNrZmluZGVyL3NyYy9ja2ZpbmRlcic7XHJcbmltcG9ydCBDS0ZpbmRlckFkYXB0ZXIgZnJvbSAnQGNrZWRpdG9yL2NrZWRpdG9yNS1hZGFwdGVyLWNrZmluZGVyL3NyYy91cGxvYWRhZGFwdGVyJztcclxuaW1wb3J0IEN1c3RvbVVwbG9hZEFkYXB0ZXIgZnJvbSAnLi9DdXN0b21VcGxvYWRBZGFwdGVyJztcclxuaW1wb3J0IEluc2VydEltYWdlIGZyb20gJy4vQ0tFZGl0b3IvSW5zZXJ0SW1hZ2UnO1xyXG5pbXBvcnQgV2lkZ2V0cyBmcm9tICcuL0NLRWRpdG9yL1dpZGdldHMnO1xyXG5cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBpbml0aWFsaXplKCkge1xyXG4gICAgICAgIHRoaXMudXBsb2FkVXJsID0gdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcImRhdGEtdXBsb2FkLXVybFwiKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIENsYXNzaWNFZGl0b3JcclxuICAgICAgICAgICAgLmNyZWF0ZSh0aGlzLmVsZW1lbnQsIHtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogNTAwLFxyXG4gICAgICAgICAgICAgICAgcGx1Z2luczogW1xyXG4gICAgICAgICAgICAgICAgICAgIEhlYWRpbmcsIEVzc2VudGlhbHNQbHVnaW4sXHJcbiAgICAgICAgICAgICAgICAgICAgQm9sZFBsdWdpbiwgSXRhbGljUGx1Z2luLCBMaW5rUGx1Z2luLFxyXG4gICAgICAgICAgICAgICAgICAgIFBhcmFncmFwaFBsdWdpbixcclxuICAgICAgICAgICAgICAgICAgICBMaXN0LFxyXG4gICAgICAgICAgICAgICAgICAgIEltYWdlLCBJbWFnZVRvb2xiYXIsIEltYWdlQ2FwdGlvbiwgSW1hZ2VTdHlsZSwgSW1hZ2VSZXNpemUsIEltYWdlVXBsb2FkLCBJbnNlcnRJbWFnZSxcclxuICAgICAgICAgICAgICAgICAgICBNZWRpYUVtYmVkLFxyXG4gICAgICAgICAgICAgICAgICAgIFRhYmxlLFxyXG4gICAgICAgICAgICAgICAgICAgIENLRmluZGVyLFxyXG4gICAgICAgICAgICAgICAgICAgIENLRmluZGVyQWRhcHRlcixcclxuICAgICAgICAgICAgICAgICAgICBCbG9ja1F1b3RlLFxyXG4gICAgICAgICAgICAgICAgICAgIEFsaWdubWVudCwgRm9udCxcclxuICAgICAgICAgICAgICAgICAgICBXaWRnZXRzXHJcbiAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAgICAgYWxpZ25tZW50OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb3B0aW9uczogWyAnbGVmdCcsICdyaWdodCcsICdjZW50ZXInLCAnanVzdGlmeScgXVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHRvb2xiYXI6IFsnaGVhZGluZycsICd8JywgJ2JvbGQnLCAnaXRhbGljJywgJ2FsaWdubWVudCcsICd8JywgJ2ZvbnRTaXplJywgJ2ZvbnRGYW1pbHknLCAnZm9udENvbG9yJywgJ2ZvbnRCYWNrZ3JvdW5kQ29sb3InLCAnfCcsICdsaW5rJywgJ2J1bGxldGVkTGlzdCcsICdudW1iZXJlZExpc3QnLCAnYmxvY2tRdW90ZScsICdpbnNlcnRUYWJsZScsICdpbWFnZVVwbG9hZCcsICdtZWRpYUVtYmVkJywgJ3wnLCAndW5kbycsICdyZWRvJywgJ3wnLCAnd2lkZ2V0cyddLFxyXG4gICAgICAgICAgICAgICAgZXh0cmFQbHVnaW5zOiBbbXlDdXN0b21VcGxvYWRBZGFwdGVyUGx1Z2luXSxcclxuICAgICAgICAgICAgICAgIGN1c3RvbVVwbG9hZFVybDogdGhpcy51cGxvYWRVcmwsXHJcbiAgICAgICAgICAgICAgICBpbWFnZToge1xyXG4gICAgICAgICAgICAgICAgICAgIHRvb2xiYXI6IFsnaW1hZ2VUZXh0QWx0ZXJuYXRpdmUnLCAnfCcsICdpbWFnZVN0eWxlOmFsaWduTGVmdCcsICdpbWFnZVN0eWxlOmZ1bGwnLCAnaW1hZ2VTdHlsZTphbGlnblJpZ2h0J10sXHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVzOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdmdWxsJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ3NpZGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnYWxpZ25MZWZ0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ2FsaWduQ2VudGVyJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ2FsaWduUmlnaHQnLFxyXG4gICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBtZWRpYUVtYmVkOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJldmlld3NJbkRhdGE6IHRydWUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgbGluazoge1xyXG4gICAgICAgICAgICAgICAgICAgIGRlY29yYXRvcnM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNFeHRlcm5hbDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbW9kZTogJ21hbnVhbCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ09wZW4gaW4gYSBuZXcgdGFiJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJpYnV0ZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXQ6ICdfYmxhbmsnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZm9udEZhbWlseToge1xyXG4gICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ2RlZmF1bHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnQXJpYWwsIEhlbHZldGljYSwgc2Fucy1zZXJpZicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdDb3VyaWVyIE5ldywgQ291cmllciwgbW9ub3NwYWNlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ0dlb3JnaWEsIHNlcmlmJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ0x1Y2lkYSBTYW5zIFVuaWNvZGUsIEx1Y2lkYSBHcmFuZGUsIHNhbnMtc2VyaWYnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGFob21hLCBHZW5ldmEsIHNhbnMtc2VyaWYnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnVGltZXMgTmV3IFJvbWFuLCBUaW1lcywgc2VyaWYnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnVHJlYnVjaGV0IE1TLCBIZWx2ZXRpY2EsIHNhbnMtc2VyaWYnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnVmVyZGFuYSwgR2VuZXZhLCBzYW5zLXNlcmlmJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ1VidW50dSdcclxuICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAudGhlbihlZGl0b3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ0NLRURJVE9SIGlzIHJlYWR5LicpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBteUN1c3RvbVVwbG9hZEFkYXB0ZXJQbHVnaW4oZWRpdG9yKSB7XHJcbiAgICBsZXQgdXBsb2FkVXJsID0gZWRpdG9yLmNvbmZpZy5nZXQoJ2N1c3RvbVVwbG9hZFVybCcpO1xyXG5cclxuICAgIGVkaXRvci5wbHVnaW5zLmdldCgnRmlsZVJlcG9zaXRvcnknKS5jcmVhdGVVcGxvYWRBZGFwdGVyID0gKGxvYWRlcikgPT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgQ3VzdG9tVXBsb2FkQWRhcHRlcihsb2FkZXIsIHVwbG9hZFVybCk7XHJcbiAgICB9O1xyXG59IiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCJzdGltdWx1c1wiXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgc3RhdGljIHRhcmdldHMgPSBbIFwic291cmNlXCIgXTtcclxuXHJcbiAgICBjb3B5KGV2ZW50KSB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgbGV0IHRlbXBJbnB1dCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiKTtcclxuICAgICAgICB0ZW1wSW5wdXQuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCI7XHJcbiAgICAgICAgdGVtcElucHV0LnN0eWxlLmxlZnQgPSBcIi0xMDAwcHhcIjtcclxuICAgICAgICB0ZW1wSW5wdXQuc3R5bGUudG9wID0gXCItMTAwMHB4XCI7XHJcbiAgICAgICAgdGVtcElucHV0LnZhbHVlID0gdGhpcy50ZXh0O1xyXG5cclxuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHRlbXBJbnB1dCk7XHJcbiAgICAgICAgdGVtcElucHV0LnNlbGVjdCgpO1xyXG5cclxuICAgICAgICBkb2N1bWVudC5leGVjQ29tbWFuZChcImNvcHlcIik7XHJcbiAgICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZCh0ZW1wSW5wdXQpO1xyXG5cclxuICAgICAgICBmbGFzaCgnQ29waWVkICcgKyB0aGlzLnRleHQgKyAnIHRvIHRoZSBjbGlwYm9hcmQuJywgJ3N1Y2Nlc3MnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgdGV4dCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zb3VyY2VUYXJnZXQudmFsdWU7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBzdWJtaXRBcHBsaWNhdGlvbihldmVudCkge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2Zvcm0nKS5jaGVja1ZhbGlkaXR5KCkpIHtcclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2Zvcm0nKS5yZXBvcnRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgc3VibWl0VXJsID0gdGhpcy5kYXRhLmdldCgnc3VibWl0VXJsJyksXHJcbiAgICAgICAgICAgIGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKCdmb3JtJykpLFxyXG4gICAgICAgICAgICBfc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgbGV0IHN1Ym1pdEJ1dHRvbiA9IGV2ZW50LnRhcmdldDtcclxuICAgICAgICBzdWJtaXRCdXR0b24uc2V0QXR0cmlidXRlKCdkaXNhYmxlZCcsIHRydWUpO1xyXG5cclxuICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICB1cmw6IHN1Ym1pdFVybCxcclxuICAgICAgICAgICAgZGF0YTogZm9ybURhdGEsXHJcbiAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxyXG4gICAgICAgICAgICBwcm9jZXNzRGF0YTogZmFsc2UsXHJcbiAgICAgICAgICAgIGNvbnRlbnRUeXBlOiBmYWxzZSxcclxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChfc2VsZi5yZWRpcmVjdFVybCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IF9zZWxmLnJlZGlyZWN0VXJsO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF9zZWxmLmVsZW1lbnQuaW5uZXJIVE1MID0gcmVzcG9uc2UuY29udGVudDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLmVsZW1lbnQuaW5uZXJIVE1MID0gcmVzcG9uc2UuY29udGVudDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uIChlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgc3VibWl0QnV0dG9uLnJlbW92ZUF0dHJpYnV0ZSgnZGlzYWJsZWQnKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBnZXQgcmVkaXJlY3RVcmwoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0YS5nZXQoJ3JlZGlyZWN0VXJsJyk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5zY3JvbGxJbnRvVmlldyh7YmVoYXZpb3I6IFwic21vb3RoXCIsIGJsb2NrOiBcImNlbnRlclwifSk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIGNvbm5lY3QoKXtcclxuICAgICAgICBFdmVudE1hbmFnZXIubGlzdGVuKCdjcmVkaXQtcGFyYW1ldGVycy1jaGFuZ2VkJywgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5pbm5lckhUTUwgPSBkYXRhW3RoaXMucHJvcGVydHlOYW1lXTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBwcm9wZXJ0eU5hbWUgKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVsZW1lbnQuZGF0YXNldC5wcm9wZXJ0eU5hbWU7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIGNvbm5lY3QoKXtcclxuICAgICAgICBFdmVudE1hbmFnZXIubGlzdGVuKCdjcmVkaXQtcGFyYW1ldGVycy1jaGFuZ2VkJywgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC52YWx1ZSA9IGRhdGFbdGhpcy5wcm9wZXJ0eU5hbWVdO1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHByb3BlcnR5TmFtZSAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5kYXRhc2V0LnByb3BlcnR5TmFtZTtcclxuICAgIH1cclxufSIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgY29ubmVjdCgpe1xyXG4gICAgICAgIHRoaXMucmFkaW9CdXR0b25zID0gdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ2lucHV0Jyk7XHJcblxyXG4gICAgICAgIHRoaXMucmFkaW9CdXR0b25zLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgdGhpcy50b2dnbGUpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZSgpIHtcclxuICAgICAgICBFdmVudE1hbmFnZXIuZmlyZSgnY3VycmVudC1hZGRyZXNzLWlzLXNhbWUtYXMtcGVybWFuZW50Jywge1xyXG4gICAgICAgICAgICBpc1NhbWU6IHRoaXMudmFsdWUgPT09ICcxJ1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHtDb250cm9sbGVyfSBmcm9tIFwic3RpbXVsdXNcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCl7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTYW1lSW5pdGlhbGx5KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkLW5vbmUnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnZC1ub25lJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBFdmVudE1hbmFnZXIubGlzdGVuKCdjdXJyZW50LWFkZHJlc3MtaXMtc2FtZS1hcy1wZXJtYW5lbnQnLCAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZGF0YS5pc1NhbWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlzU2FtZUluaXRpYWxseSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQuaXNTYW1lID09PSAnMSc7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBzdGF0aWMgdGFyZ2V0cyA9IFsnYXJyb3cnLCAnY29udGVudCddO1xyXG5cclxuICAgIGNvbm5lY3QoKSB7XHJcbiAgICAgICAgbGV0IHRpdGxlID0gdGhpcy5kYXRhLmdldCgndGl0bGUnKTtcclxuICAgICAgICBpZih0aXRsZSl7XHJcbiAgICAgICAgICAgIGxldCBjdXJyZW50VXJsID0gd2luZG93LmxvY2F0aW9uLmhyZWYsXHJcbiAgICAgICAgICAgICAgICBwYXJ0cyA9IGN1cnJlbnRVcmwuc3BsaXQoJyMnKSxcclxuICAgICAgICAgICAgICAgIGZyYWdtZW50ID0gbnVsbDtcclxuXHJcbiAgICAgICAgICAgIGlmIChwYXJ0cy5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgICAgICBmcmFnbWVudCA9IGRlY29kZVVSSShwYXJ0c1twYXJ0cy5sZW5ndGggLSAxXSkudG9Mb3dlckNhc2UoKS5yZXBsYWNlKC8gL2csICctJyk7XHJcbiAgICAgICAgICAgICAgICBpZihmcmFnbWVudCA9PT0gdGl0bGUudG9Mb2NhbGVMb3dlckNhc2UoKS5yZXBsYWNlKC8gL2csICctJykpe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlQ29udGVudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5zY3JvbGxJbnRvVmlldyh7YmVoYXZpb3I6IFwic21vb3RoXCIsIGJsb2NrOiBcIm5lYXJlc3RcIn0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZUNvbnRlbnQoKSB7XHJcbiAgICAgICAgdGhpcy5hcnJvd1RhcmdldC5jbGFzc0xpc3QudG9nZ2xlKCdmYS1hbmdsZS1yaWdodCcpO1xyXG4gICAgICAgIHRoaXMuYXJyb3dUYXJnZXQuY2xhc3NMaXN0LnRvZ2dsZSgnZmEtYW5nbGUtZG93bicpO1xyXG5cclxuICAgICAgICB0aGlzLmNvbnRlbnRUYXJnZXQuY2xhc3NMaXN0LnRvZ2dsZSgnbm90LXZpc2libGUnKTtcclxuICAgIH1cclxufSIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgY29ubmVjdCgpe1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ21hc3Rlci1maWVsZC1jaGFuZ2VkJywgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChkYXRhLnByb3BlcnR5TmFtZSA9PT0gdGhpcy5wcm9wZXJ0eU5hbWUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLnZhbHVlID09PSAnRml4ZWQtdGVybSBjb250cmFjdCcpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnZC1ub25lJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHByb3BlcnR5TmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQucHJvcGVydHlOYW1lO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCJzdGltdWx1c1wiXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgc3RhdGljIHRhcmdldHMgPSBbJ2Fycm93JywgJ2Fuc3dlciddO1xyXG5cclxuICAgIGNvbm5lY3QoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGF0YS5oYXMoJ29wZW4nKSkge1xyXG4gICAgICAgICAgICB0aGlzLnRvZ2dsZUFuc3dlcigpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnNjcm9sbEludG9WaWV3KHtiZWhhdmlvcjogXCJzbW9vdGhcIiwgYmxvY2s6IFwiY2VudGVyXCJ9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdG9nZ2xlQW5zd2VyKCkge1xyXG4gICAgICAgIHRoaXMuYXJyb3dUYXJnZXQuY2xhc3NMaXN0LnRvZ2dsZSgnZmEtYW5nbGUtcmlnaHQnKTtcclxuICAgICAgICB0aGlzLmFycm93VGFyZ2V0LmNsYXNzTGlzdC50b2dnbGUoJ2ZhLWFuZ2xlLWRvd24nKTtcclxuXHJcbiAgICAgICAgdGhpcy5hbnN3ZXJUYXJnZXQuY2xhc3NMaXN0LnRvZ2dsZSgnbm90LXZpc2libGUnKTtcclxuICAgIH1cclxufSIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICB0b2dnbGVCYWNrZ3JvdW5kT3BhY2l0eSgpIHtcclxuICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnRvZ2dsZSgnYmctd2hpdGUnKTtcclxuICAgIH1cclxufSIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgY29ubmVjdCgpe1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ21hc3Rlci1maWVsZC1jaGFuZ2VkJywgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChkYXRhLnByb3BlcnR5TmFtZSA9PT0gdGhpcy5wcm9wZXJ0eU5hbWUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLnZhbHVlID09PSAnM2U3MTE1ZjQtZWIyZS00MmIyLThkZjItZTVjNWVkMzRmYWQzJykgeyAvLyBCYW5rIGFjY291bnRcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnZC1ub25lJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHByb3BlcnR5TmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQucHJvcGVydHlOYW1lO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHtDb250cm9sbGVyfSBmcm9tIFwic3RpbXVsdXNcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIHRoaXMuY29tcG9uZW50SWQgPSB0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKCdpZCcpO1xyXG5cclxuICAgICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLnNob3dJbWFnZUdhbGxlcnkpO1xyXG5cclxuICAgICAgICBFdmVudE1hbmFnZXIubGlzdGVuKCdpbWFnZS1nYWxsZXJ5LnNlbGVjdC1pbWFnZScsIHBheWxvYWQgPT4ge1xyXG4gICAgICAgICAgICBpZiAocGF5bG9hZC5jb21wb25lbnRJZCA9PT0gdGhpcy5jb21wb25lbnRJZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LnZhbHVlID0gcGF5bG9hZC5wYXRoO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd0ltYWdlR2FsbGVyeSgpIHtcclxuICAgICAgICBFdmVudE1hbmFnZXIuZmlyZSgnYWRkLWNvbXBvbmVudCcsIHt0eXBlOiAnaW1hZ2UtZ2FsbGVyeScsIGlkOiB0aGlzLmlkfSk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBzdGF0aWMgdGFyZ2V0cyA9IFsnZnVsbEluZm8nLCAnYnV0dG9uVGV4dCcsICdhcnJvdyddO1xyXG5cclxuICAgIGNvbm5lY3QoKSB7XHJcbiAgICAgICAgaWYodGhpcy5kYXRhLmdldCgnZXhwYW5kZWQnKSl7XHJcbiAgICAgICAgICAgIHRoaXMudG9nZ2xlSW5mbygpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuc2Nyb2xsSW50b1ZpZXcoe2JlaGF2aW9yOiBcInNtb290aFwiLCBibG9jazogXCJuZWFyZXN0XCJ9KTtcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgdGhpcy5idXR0b25UZXh0VGFyZ2V0LmlubmVySFRNTCA9IHRoaXMuZGF0YS5nZXQoJ3RleHREb3duJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZUluZm8oKSB7XHJcbiAgICAgICAgdGhpcy5hcnJvd1RhcmdldC5jbGFzc0xpc3QudG9nZ2xlKCdmYS1hbmdsZS1kb3duJyk7XHJcbiAgICAgICAgdGhpcy5hcnJvd1RhcmdldC5jbGFzc0xpc3QudG9nZ2xlKCdmYS1hbmdsZS11cCcpO1xyXG5cclxuICAgICAgICB0aGlzLmZ1bGxJbmZvVGFyZ2V0LmNsYXNzTGlzdC50b2dnbGUoJ25vdC12aXNpYmxlJyk7XHJcblxyXG4gICAgICAgIHRoaXMuYnV0dG9uVGV4dFRhcmdldC5pbm5lckhUTUwgPSB0aGlzLnRleHRPbkJ1dHRvbjtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaXNPcGVuKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFycm93VGFyZ2V0LmNsYXNzTGlzdC5jb250YWlucygnZmEtYW5nbGUtdXAnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgdGV4dE9uQnV0dG9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzT3BlbiA/IHRoaXMuZGF0YS5nZXQoJ3RleHRVcCcpIDogdGhpcy5kYXRhLmdldCgndGV4dERvd24nKTtcclxuICAgIH1cclxufSIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBzd2l0Y2goZXZlbnQpIHtcclxuICAgICAgICBsZXQgdXJsID0gYC9lbi9hZG1pbi9sb2NhbGUvJHt0aGlzLnVybH1gO1xyXG5cclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgIHN1Y2Nlc3MoKSB7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnJvcihlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHVybCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5kYXRhLmdldCgnbG9jYWxlJyk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIGNvbm5lY3QoKSB7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsICgpID0+IHtcclxuICAgICAgICAgICAgRXZlbnRNYW5hZ2VyLmZpcmUoJ21hc3Rlci1maWVsZC1jaGFuZ2VkJywge1xyXG4gICAgICAgICAgICAgICAgcHJvcGVydHlOYW1lOiB0aGlzLnByb3BlcnR5TmFtZSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmVsZW1lbnQudmFsdWVcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgJChkb2N1bWVudCkucmVhZHkoKCkgPT4ge1xyXG4gICAgICAgICAgICBFdmVudE1hbmFnZXIuZmlyZSgnbWFzdGVyLWZpZWxkLWNoYW5nZWQnLCB7XHJcbiAgICAgICAgICAgICAgICBwcm9wZXJ0eU5hbWU6IHRoaXMucHJvcGVydHlOYW1lLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZWxlbWVudC52YWx1ZVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBwcm9wZXJ0eU5hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5kYXRhc2V0LnByb3BlcnR5TmFtZTtcclxuICAgIH1cclxufSIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ2N1c3RvbWVyLW1lc3NhZ2Utd2FzLXJlYWQnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5pbm5lckhUTUwgPSB0aGlzLmNvdW50IC0gMTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmNvdW50ID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnZC1ub25lJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgY291bnQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHBhcnNlSW50KHRoaXMuZWxlbWVudC5pbm5lckhUTUwpO1xyXG4gICAgfVxyXG59XHJcbiIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBzdGF0aWMgdGFyZ2V0cyA9IFsnYXJyb3cnLCAnY29udGVudCcsICd0aXRsZSddO1xyXG5cclxuICAgIHRvZ2dsZUNvbnRlbnQoKSB7XHJcbiAgICAgICAgdGhpcy5hcnJvd1RhcmdldC5jbGFzc0xpc3QudG9nZ2xlKCdmYS1hbmdsZS1yaWdodCcpO1xyXG4gICAgICAgIHRoaXMuYXJyb3dUYXJnZXQuY2xhc3NMaXN0LnRvZ2dsZSgnZmEtYW5nbGUtZG93bicpO1xyXG5cclxuICAgICAgICB0aGlzLmNvbnRlbnRUYXJnZXQuY2xhc3NMaXN0LnRvZ2dsZSgnbm90LXZpc2libGUnKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuaXNVblJlYWQpIHtcclxuICAgICAgICAgICAgbGV0ICBfc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnRpdGxlVGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoJ3RleHQtZGFuZ2VyJyk7XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHRoaXMudXJsLFxyXG4gICAgICAgICAgICAgICAgc3VjY2VzcygpIHtcclxuICAgICAgICAgICAgICAgICAgICBFdmVudE1hbmFnZXIuZmlyZSgnY3VzdG9tZXItbWVzc2FnZS13YXMtcmVhZCcpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3JbJ3Jlc3BvbnNlSlNPTiddKTtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi50aXRsZVRhcmdldC5jbGFzc0xpc3QuYWRkKCd0ZXh0LWRhbmdlcicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlzVW5SZWFkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnRpdGxlVGFyZ2V0LmNsYXNzTGlzdC5jb250YWlucygndGV4dC1kYW5nZXInKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgdXJsKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVsZW1lbnQuZGF0YXNldC51cmw7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHtDb250cm9sbGVyfSBmcm9tIFwic3RpbXVsdXNcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ2NyZWRpdC1wYXJhbWV0ZXJzLWNoYW5nZWQnLCBkYXRhID0+IHtcclxuICAgICAgICAgICAgaWYgKHdpbmRvdy5sb2NhdGlvbi5ocmVmLmluZGV4T2YoJ2FwcGxpY2F0aW9uPScpICE9IC0xKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBbdXJsLF0gPSB0aGlzLmluaXRpYWxBY3Rvbi5zcGxpdCgnPycpLFxyXG4gICAgICAgICAgICAgICAgW2RvbWFpbiwgcm91dGUsIGNyZWRpdCwgY3JlZGl0RGV0YWlscywgaXNSZWZpbmFuY2VdID0gdXJsLnNwbGl0KCcvJyk7XHJcblxyXG4gICAgICAgICAgICBsZXQgcXVlcnlQYXJhbWV0ZXJzT2JqZWN0ID0ge2Ftb3VudDogZGF0YS5hbW91bnQsIHBlcmlvZDogZGF0YS5wZXJpb2QsIHBheW1lbnQ6IGRhdGEucGF5bWVudH0sXHJcbiAgICAgICAgICAgICAgICBxdWVyeVN0cmluZyA9ICQucGFyYW0ocXVlcnlQYXJhbWV0ZXJzT2JqZWN0KSxcclxuICAgICAgICAgICAgICAgIG5ld1VybCA9IGAvJHtyb3V0ZX0vJHtkYXRhLnNsdWd9LyR7Y3JlZGl0RGV0YWlsc30vJHtpc1JlZmluYW5jZX0/JHtxdWVyeVN0cmluZ31gO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZSgnYWN0aW9uJywgbmV3VXJsKTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBpbml0aWFsQWN0b24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2FjdGlvbicpO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCJzdGltdWx1c1wiXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgcmVkaXJlY3QoKSB7XHJcbiAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSB0aGlzLmVsZW1lbnQudmFsdWU7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIGNvbm5lY3QoKXtcclxuICAgICAgICBFdmVudE1hbmFnZXIubGlzdGVuKCdtYXN0ZXItZmllbGQtY2hhbmdlZCcsIGRhdGEgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZGF0YS5wcm9wZXJ0eU5hbWUgPT09IHRoaXMucHJvcGVydHlOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YS52YWx1ZSA9PT0gdGhpcy5wcm9wZXJ0eVZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ2Qtbm9uZScpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnZC1ub25lJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBwcm9wZXJ0eU5hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5kYXRhc2V0LnByb3BlcnR5TmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcHJvcGVydHlWYWx1ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQucHJvcGVydHlWYWx1ZTtcclxuICAgIH1cclxufSIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgY29ubmVjdCgpe1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ2NyZWRpdC1wYXJhbWV0ZXJzLWNoYW5nZWQnLCBkYXRhID0+IHtcclxuXHJcbiAgICAgICAgICAgIGlmIChkYXRhLnNsdWcgPT09ICdwZW5zaW9uZXInKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgYXN0ZXJpc2sgPSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcignaScpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChhc3RlcmlzayAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBhc3RlcmlzayA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2knKTtcclxuXHJcbiAgICAgICAgICAgICAgICBhc3Rlcmlzay5jbGFzc05hbWUgPSAnZmEgZmEtYXN0ZXJpc2sgZmEtcmVxdWlyZWQnO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5wcmVwZW5kKGFzdGVyaXNrKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGxldCBhc3RlcmlzayA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKCdpJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGFzdGVyaXNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LnJlbW92ZUNoaWxkKGFzdGVyaXNrKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBpbml0aWFsaXplKCkge1xyXG4gICAgICAgIHRoaXMuc2V0UGhvbmVQcmVmaXgoKTtcclxuICAgIH1cclxuICAgIG9uS2V5VXAoKSB7XHJcbiAgICAgICB0aGlzLnNldFBob25lUHJlZml4KCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0UGhvbmVQcmVmaXgoKSB7XHJcbiAgICAgICAgaWYoIXRoaXMuZWxlbWVudC52YWx1ZS5zdGFydHNXaXRoKCczODAnKSApIHtcclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnZhbHVlID0gJzM4MCc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiaW1wb3J0IHtDb250cm9sbGVyfSBmcm9tIFwic3RpbXVsdXNcIjtcclxuaW1wb3J0ICdAc2ltb253ZXAvcGlja3IvZGlzdC90aGVtZXMvY2xhc3NpYy5taW4uY3NzJzsgICAvLyAnY2xhc3NpYycgdGhlbWVcclxuaW1wb3J0IFBpY2tyIGZyb20gJ0BzaW1vbndlcC9waWNrcic7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgY29ubmVjdCgpIHtcclxuICAgICAgICBjb25zdCBwaWNrciA9IFBpY2tyLmNyZWF0ZSh7XHJcbiAgICAgICAgICAgIGVsOiB0aGlzLmVsZW1lbnQsXHJcbiAgICAgICAgICAgIHRoZW1lOiAnY2xhc3NpYycsXHJcbiAgICAgICAgICAgIHVzZUFzQnV0dG9uOiB0cnVlLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiB0aGlzLmVsZW1lbnQudmFsdWUsXHJcbiAgICAgICAgICAgIHN3YXRjaGVzOiBbXHJcbiAgICAgICAgICAgICAgICAncmdiYSgyNDQsIDY3LCA1NCwgMSknLFxyXG4gICAgICAgICAgICAgICAgJ3JnYmEoMjMzLCAzMCwgOTksIDAuOTUpJyxcclxuICAgICAgICAgICAgICAgICdyZ2JhKDE1NiwgMzksIDE3NiwgMC45KScsXHJcbiAgICAgICAgICAgICAgICAncmdiYSgxMDMsIDU4LCAxODMsIDAuODUpJyxcclxuICAgICAgICAgICAgICAgICdyZ2JhKDYzLCA4MSwgMTgxLCAwLjgpJyxcclxuICAgICAgICAgICAgICAgICdyZ2JhKDMzLCAxNTAsIDI0MywgMC43NSknLFxyXG4gICAgICAgICAgICAgICAgJ3JnYmEoMywgMTY5LCAyNDQsIDAuNyknLFxyXG4gICAgICAgICAgICAgICAgJ3JnYmEoMCwgMTg4LCAyMTIsIDAuNyknLFxyXG4gICAgICAgICAgICAgICAgJ3JnYmEoMCwgMTUwLCAxMzYsIDAuNzUpJyxcclxuICAgICAgICAgICAgICAgICdyZ2JhKDc2LCAxNzUsIDgwLCAwLjgpJyxcclxuICAgICAgICAgICAgICAgICdyZ2JhKDEzOSwgMTk1LCA3NCwgMC44NSknLFxyXG4gICAgICAgICAgICAgICAgJ3JnYmEoMjA1LCAyMjAsIDU3LCAwLjkpJyxcclxuICAgICAgICAgICAgICAgICdyZ2JhKDI1NSwgMjM1LCA1OSwgMC45NSknLFxyXG4gICAgICAgICAgICAgICAgJ3JnYmEoMjU1LCAxOTMsIDcsIDEpJ1xyXG4gICAgICAgICAgICBdLFxyXG5cclxuICAgICAgICAgICAgY29tcG9uZW50czoge1xyXG4gICAgICAgICAgICAgICAgcHJldmlldzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIG9wYWNpdHk6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBodWU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBpbnRlcmFjdGlvbjoge1xyXG4gICAgICAgICAgICAgICAgICAgIGhleDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICByZ2JhOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGhzbGE6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgaHN2YTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBjbXlrOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGlucHV0OiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGNsZWFyOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIHNhdmU6IHRydWVcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBwaWNrci5vbignc2F2ZScsIChjb2xvciwgaW5zdGFuY2UpID0+IHtcclxuICAgICAgICAgICAgaWYgKGNvbG9yKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQudmFsdWUgPSBjb2xvci50b0hFWEEoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC52YWx1ZSA9ICcnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIGNvbm5lY3QoKXtcclxuICAgICAgICAkKGRvY3VtZW50KS5yZWFkeSgoKSA9PiB7XHJcbiAgICAgICAgICAgIEV2ZW50TWFuYWdlci5maXJlKCdwaW4tdXBkYXRlZCcsIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmFnZSh0aGlzLmVsZW1lbnQudmFsdWUuc3Vic3RyaW5nKDAsIDUpKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5lbGVtZW50LnZhbHVlLmxlbmd0aCA8IDUpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgRXZlbnRNYW5hZ2VyLmZpcmUoJ3Bpbi11cGRhdGVkJywge1xyXG4gICAgICAgICAgICB2YWx1ZTogdGhpcy5hZ2UodGhpcy5lbGVtZW50LnZhbHVlLnN1YnN0cmluZygwLCA1KSlcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIGFnZShkYXlzKSB7XHJcbiAgICAgICAgaWYgKGRheXMubGVuZ3RoIDwgNSkge1xyXG4gICAgICAgICAgICByZXR1cm4gMDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBiYXNlRGF0ZSA9IG5ldyBEYXRlKDE5MDAsIDAsIDEpO1xyXG4gICAgICAgIGxldCBkYXRlT2ZCaXJ0aCA9IG5ldyBEYXRlKGJhc2VEYXRlLnZhbHVlT2YoKSk7XHJcbiAgICAgICAgbGV0IHRvZGF5ID0gbmV3IERhdGUoKTtcclxuXHJcbiAgICAgICAgZGF0ZU9mQmlydGguc2V0RGF0ZShkYXRlT2ZCaXJ0aC5nZXREYXRlKCkgKyBwYXJzZUludChkYXlzKSk7XHJcblxyXG4gICAgICAgIHJldHVybiBNYXRoLmZsb29yKCh0b2RheSAtIGRhdGVPZkJpcnRoKSAvICgxMDAwICogNjAgKiA2MCAqIDI0ICogMzY1KSk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcInN0aW11bHVzXCJcclxuaW1wb3J0IFNvcnRhYmxlIGZyb20gXCJzb3J0YWJsZWpzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBzdWJtaXQoKSB7XHJcbiAgICAgICAgaWYgKGRvY3VtZW50LmZvcm1zLnBvbGwuY2hlY2tWYWxpZGl0eSgpID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgY29tcGxldGVkUG9sbHMgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY29tcGxldGVkX3BvbGxzJykgfHwgJycsXHJcbiAgICAgICAgICAgIHBvbGxJZCA9IHRoaXMuZGF0YS5nZXQoJ3BvbGxJZCcpO1xyXG5cclxuICAgICAgICBjb21wbGV0ZWRQb2xscyA9IGNvbXBsZXRlZFBvbGxzLnNwbGl0KCd8Jyk7XHJcbiAgICAgICAgY29tcGxldGVkUG9sbHMucHVzaChwb2xsSWQpO1xyXG4gICAgICAgIGNvbXBsZXRlZFBvbGxzID0gbmV3IFNldChjb21wbGV0ZWRQb2xscyk7XHJcbiAgICAgICAgY29tcGxldGVkUG9sbHMgPSBBcnJheS5mcm9tKGNvbXBsZXRlZFBvbGxzKTtcclxuXHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2NvbXBsZXRlZF9wb2xscycsIGNvbXBsZXRlZFBvbGxzLmpvaW4oJ3wnKSk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBpbml0aWFsaXplKCkge1xyXG4gICAgICAgIHRoaXMucGFyYW1ldGVyID0gdGhpcy5kYXRhLmdldChcInBhcmFtZXRlclwiKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIGxldCBfc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgIHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGxldCBjdXJyZW50VXJsID0gd2luZG93LmxvY2F0aW9uLmhyZWYsXHJcbiAgICAgICAgICAgICAgICBuZXdVcmwgPSAnJyxcclxuICAgICAgICAgICAgICAgIHJlZ2V4LCBoYXNFeGlzdGluZ1ZhbHVlO1xyXG5cclxuICAgICAgICAgICAgaWYgKGN1cnJlbnRVcmwuaW5kZXhPZignPycpIDwgMCkge1xyXG4gICAgICAgICAgICAgICAgbmV3VXJsID0gYCR7Y3VycmVudFVybH0/JHtfc2VsZi5wYXJhbWV0ZXJ9PSR7X3NlbGYuZWxlbWVudC52YWx1ZX1gO1xyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiB3aW5kb3cubG9jYXRpb24uaHJlZiA9IG5ld1VybDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IHBhZ2VSZWdleCA9IG5ldyBSZWdFeHAoLyhwYWdlKT1cXGQrLyk7XHJcbiAgICAgICAgICAgIGN1cnJlbnRVcmwgPSBjdXJyZW50VXJsLnJlcGxhY2UocGFnZVJlZ2V4LCAnJDE9MScpO1xyXG5cclxuICAgICAgICAgICAgcmVnZXggPSBuZXcgUmVnRXhwKGAke19zZWxmLnBhcmFtZXRlcn09YCk7XHJcbiAgICAgICAgICAgIGhhc0V4aXN0aW5nVmFsdWUgPSBjdXJyZW50VXJsLm1hdGNoKHJlZ2V4KTtcclxuXHJcbiAgICAgICAgICAgIGlmIChoYXNFeGlzdGluZ1ZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcGFydHMgPSBjdXJyZW50VXJsLnNwbGl0KCc/JylbMV0uc3BsaXQoJyYnKTtcclxuXHJcbiAgICAgICAgICAgICAgICBwYXJ0cyA9IHBhcnRzLm1hcChwYXJ0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocGFydC5zcGxpdCgnPScpWzBdID09PSBfc2VsZi5wYXJhbWV0ZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGAke19zZWxmLnBhcmFtZXRlcn09JHtfc2VsZi5lbGVtZW50LnZhbHVlfWA7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcGFydDtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIG5ld1VybCA9IGN1cnJlbnRVcmwuc3BsaXQoJz8nKVswXSArICc/JyArIHBhcnRzLmpvaW4oJyYnKTtcclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gd2luZG93LmxvY2F0aW9uLmhyZWYgPSBuZXdVcmw7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBuZXdVcmwgPSBgJHtjdXJyZW50VXJsfSYke19zZWxmLnBhcmFtZXRlcn09JHtfc2VsZi5lbGVtZW50LnZhbHVlfWA7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gbmV3VXJsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxufSIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgY29ubmVjdCgpe1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ21hc3Rlci1maWVsZC1jaGFuZ2VkJywgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChkYXRhLnByb3BlcnR5TmFtZSA9PT0gdGhpcy5wcm9wZXJ0eU5hbWUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLnZhbHVlID09PSAnQmFuayBhY2NvdW50JyB8fCBkYXRhLnZhbHVlID09PSAnQ2FzaCcpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnZC1ub25lJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHByb3BlcnR5TmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQucHJvcGVydHlOYW1lO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCJzdGltdWx1c1wiXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgc3RhdGljIHRhcmdldHMgPSBbJ2VsZW1lbnQnXTtcclxuXHJcbiAgICBzY3JvbGwoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZWxlbWVudC5kYXRhc2V0Lm1vYmlsZU9ubHkpIHtcclxuICAgICAgICAgICAgbGV0IGlzTW9iaWxlID0gd2luZG93Lm1hdGNoTWVkaWEoXCIobWF4LXdpZHRoOiA1NzZweClcIikubWF0Y2hlcztcclxuXHJcbiAgICAgICAgICAgIGlmIChpc01vYmlsZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50VGFyZ2V0LnNjcm9sbEludG9WaWV3KHtiZWhhdmlvcjogXCJzbW9vdGhcIiwgYmxvY2s6IFwiY2VudGVyXCJ9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZXtcclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50VGFyZ2V0LnNjcm9sbEludG9WaWV3KHtiZWhhdmlvcjogXCJzbW9vdGhcIiwgYmxvY2s6IFwiY2VudGVyXCJ9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCJzdGltdWx1c1wiXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgY29ubmVjdCgpIHtcclxuICAgICAgICBsZXQgaW52YWxpZENvbnRyb2wgPSB0aGlzLmVsZW1lbnQucXVlcnlTZWxlY3RvcignLmZvcm0tY29udHJvbC5pcy1pbnZhbGlkJyk7XHJcblxyXG4gICAgICAgICBpZiAoIWludmFsaWRDb250cm9sKSB7XHJcbiAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgIH1cclxuXHJcbiAgICAgICAgaW52YWxpZENvbnRyb2wuc2Nyb2xsSW50b1ZpZXcoe2JlaGF2aW9yOiBcInNtb290aFwiLCBibG9jazogXCJjZW50ZXJcIn0pO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHtDb250cm9sbGVyfSBmcm9tIFwic3RpbXVsdXNcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ3RvZ2dsZS1maWxlLXVwbG9hZC1zZWN0aW9uJywgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmNyZWRpdElkID09PSBkYXRhLmNyZWRpdElkKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YS5pc1Zpc2libGUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnZC1ub25lJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBjcmVkaXRJZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQuY3JlZGl0SWQ7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIGNvbm5lY3QoKSB7XHJcbiAgICAgICAgRXZlbnRNYW5hZ2VyLmxpc3RlbigndG9nZ2xlLXJlcGF5bWVudC1wbGFuJywgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmNvbnRyYWN0ID09PSBkYXRhLmNvbnRyYWN0KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YS5pc1Zpc2libGUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnZC1ub25lJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdkLW5vbmUnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBjb250cmFjdCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQuY29udHJhY3Q7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcInN0aW11bHVzXCJcclxuaW1wb3J0IFNvcnRhYmxlIGZyb20gXCJzb3J0YWJsZWpzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIGxldCBfc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgIFNvcnRhYmxlLmNyZWF0ZSh0aGlzLmVsZW1lbnQsIHtcclxuICAgICAgICAgICAgZ2hvc3RDbGFzczogJ2JsdWUtYmFja2dyb3VuZC1kdXJpbmctZHJhZycsXHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogMzAwLFxyXG4gICAgICAgICAgICBvblNvcnQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIGxldCBpbnB1dHMgPSBfc2VsZi5lbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ2lucHV0Jyk7XHJcblxyXG4gICAgICAgICAgICAgICAgaW5wdXRzLmZvckVhY2goKGVsZW1lbnQsIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2lkJywgX3NlbGYudXBkYXRlSWQoZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2lkJyksIGluZGV4KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ25hbWUnLCBfc2VsZi51cGRhdGVOYW1lKGVsZW1lbnQuZ2V0QXR0cmlidXRlKCduYW1lJyksIGluZGV4KSk7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlTmFtZShjdXJyZW50TmFtZSwgaW5kZXgpIHtcclxuICAgICAgICBsZXQgcG9zaXRpb24gPSBpbmRleCAlIDIgPT09IDAgPyBpbmRleCAvIDIgOiAoaW5kZXggLSAxKSAvIDI7XHJcblxyXG4gICAgICAgIHJldHVybiBjdXJyZW50TmFtZS5yZXBsYWNlKC9cXFtcXGQrXFxdLywgYFske3Bvc2l0aW9ufV1gKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVJZChjdXJyZW50SWQsIGluZGV4KSB7XHJcbiAgICAgICAgbGV0IHBvc2l0aW9uID0gaW5kZXggJSAyID09PSAwID8gaW5kZXggLyAyIDogKGluZGV4IC0gMSkgLyAyO1xyXG5cclxuICAgICAgICByZXR1cm4gY3VycmVudElkLnJlcGxhY2UoL19cXGQrXy8sIGBfJHtwb3NpdGlvbn1fYCk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcInN0aW11bHVzXCJcclxuaW1wb3J0IFNvcnRhYmxlIGZyb20gXCJzb3J0YWJsZWpzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBjb25uZWN0KCkge1xyXG4gICAgICAgIFNvcnRhYmxlLmNyZWF0ZSh0aGlzLmVsZW1lbnQsIHtcclxuICAgICAgICAgICAgZ2hvc3RDbGFzczogJ2JsdWUtYmFja2dyb3VuZC1kdXJpbmctZHJhZycsXHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogMzAwXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQge0NvbnRyb2xsZXJ9IGZyb20gXCJzdGltdWx1c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb250cm9sbGVyIHtcclxuICAgIHN0YXRpYyB0YXJnZXRzID0gWyBcImFycm93XCIgXVxyXG5cclxuICAgIGNvbm5lY3QoKSB7XHJcbiAgICAgICAgJChkb2N1bWVudCkucmVhZHkoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmlzVmlzaWJsZSA9IHRoaXMuZWxlbWVudC5kYXRhc2V0LmlzVmlzaWJsZSA9PT0gJ3RydWUnO1xyXG5cclxuICAgICAgICAgICAgRXZlbnRNYW5hZ2VyLmZpcmUoJ3RvZ2dsZS1maWxlLXVwbG9hZC1zZWN0aW9uJywge1xyXG4gICAgICAgICAgICAgICAgY3JlZGl0SWQ6IHRoaXMuY3JlZGl0SWQsXHJcbiAgICAgICAgICAgICAgICBpc1Zpc2libGU6IHRoaXMuaXNWaXNpYmxlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZSgpIHtcclxuICAgICAgICB0aGlzLmlzVmlzaWJsZSA9ICF0aGlzLmlzVmlzaWJsZTtcclxuXHJcbiAgICAgICAgRXZlbnRNYW5hZ2VyLmZpcmUoJ3RvZ2dsZS1maWxlLXVwbG9hZC1zZWN0aW9uJywge1xyXG4gICAgICAgICAgICBjcmVkaXRJZDogdGhpcy5jcmVkaXRJZCxcclxuICAgICAgICAgICAgaXNWaXNpYmxlOiB0aGlzLmlzVmlzaWJsZVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc1Zpc2libGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5kYXRhc2V0LmlzVmlzaWJsZSA9PT0gJ3RydWUnO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBpc1Zpc2libGUodmFsdWUpIHtcclxuICAgICAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKCdkYXRhLWlzLXZpc2libGUnLCB2YWx1ZSk7XHJcblxyXG4gICAgICAgIGlmICh2YWx1ZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLmFycm93VGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoJ2ZhLWFuZ2xlLXJpZ2h0Jyk7XHJcbiAgICAgICAgICAgIHRoaXMuYXJyb3dUYXJnZXQuY2xhc3NMaXN0LmFkZCgnZmEtYW5nbGUtZG93bicpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXJyb3dUYXJnZXQuY2xhc3NMaXN0LnJlbW92ZSgnZmEtYW5nbGUtZG93bicpO1xyXG4gICAgICAgICAgICB0aGlzLmFycm93VGFyZ2V0LmNsYXNzTGlzdC5hZGQoJ2ZhLWFuZ2xlLXJpZ2h0Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBjcmVkaXRJZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQuY3JlZGl0SWQ7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcInN0aW11bHVzXCJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29udHJvbGxlciB7XHJcbiAgICBzdGF0aWMgdGFyZ2V0cyA9IFsgXCJwYXNzd29yZEVsZW1lbnRcIiwgXCJidXR0b25FbGVtZW50XCIgXTtcclxuXHJcbiAgICB0b2dnbGUoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucGFzc3dvcmRFbGVtZW50VGFyZ2V0LmdldEF0dHJpYnV0ZSgndHlwZScpID09PSAncGFzc3dvcmQnKSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFzc3dvcmRFbGVtZW50VGFyZ2V0LnNldEF0dHJpYnV0ZSgndHlwZScsICd0ZXh0Jyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5wYXNzd29yZEVsZW1lbnRUYXJnZXQuc2V0QXR0cmlidXRlKCd0eXBlJywgJ3Bhc3N3b3JkJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmJ1dHRvbkVsZW1lbnRUYXJnZXQuY2xhc3NMaXN0LnRvZ2dsZSgnZmEtZXllJyk7XHJcbiAgICAgICAgdGhpcy5idXR0b25FbGVtZW50VGFyZ2V0LmNsYXNzTGlzdC50b2dnbGUoJ2ZhLWV5ZS1zbGFzaCcpO1xyXG4gICAgfVxyXG59XHJcbiIsImltcG9ydCB7Q29udHJvbGxlcn0gZnJvbSBcInN0aW11bHVzXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgc3RhdGljIHRhcmdldHMgPSBbIFwiYXJyb3dcIiBdO1xyXG5cclxuICAgIHRvZ2dsZShldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC50YXJnZXQubm9kZU5hbWUgPT09ICdCVVRUT04nKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuaXNWaXNpYmxlID0gIXRoaXMuaXNWaXNpYmxlO1xyXG5cclxuICAgICAgICBFdmVudE1hbmFnZXIuZmlyZSgndG9nZ2xlLXJlcGF5bWVudC1wbGFuJywge1xyXG4gICAgICAgICAgICBjb250cmFjdDogdGhpcy5jb250cmFjdCxcclxuICAgICAgICAgICAgaXNWaXNpYmxlOiB0aGlzLmlzVmlzaWJsZVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc1Zpc2libGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5kYXRhc2V0LmlzVmlzaWJsZSA9PT0gJ3RydWUnO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBpc1Zpc2libGUodmFsdWUpIHtcclxuICAgICAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKCdkYXRhLWlzLXZpc2libGUnLCB2YWx1ZSk7XHJcblxyXG4gICAgICAgIGlmICh2YWx1ZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLmFycm93VGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoJ2ZhLWFuZ2xlLXJpZ2h0Jyk7XHJcbiAgICAgICAgICAgIHRoaXMuYXJyb3dUYXJnZXQuY2xhc3NMaXN0LmFkZCgnZmEtYW5nbGUtZG93bicpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXJyb3dUYXJnZXQuY2xhc3NMaXN0LnJlbW92ZSgnZmEtYW5nbGUtZG93bicpO1xyXG4gICAgICAgICAgICB0aGlzLmFycm93VGFyZ2V0LmNsYXNzTGlzdC5hZGQoJ2ZhLWFuZ2xlLXJpZ2h0Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBjb250cmFjdCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmRhdGFzZXQuY29udHJhY3Q7XHJcbiAgICB9XHJcbn0iLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKCQoZG9jdW1lbnQpLnNjcm9sbFRvcCgpID4gNTApIHtcclxuICAgICAgICAgICAgJCgnaGVhZGVyJykuYWRkQ2xhc3MoJ2hlYWRlci1iZ3InKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAkKCdoZWFkZXInKS5yZW1vdmVDbGFzcygnaGVhZGVyLWJncicpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuXHJcbi8vICAgICQoJ25hdiBhJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4vLyAgICAgICAgJCgnLm5hdi1idG4nKS50b2dnbGVDbGFzcygnb3BlbicpO1xyXG4vLyAgICAgICAgJCgnLm1vYmlsZS1uYXYnKS50b2dnbGVDbGFzcygnb3BlbicpO1xyXG4vLyAgICB9KTtcclxuXHJcbiAgICAkKCcubmF2LWJ0bicpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdvcGVuJyk7XHJcbiAgICAgICAgJCgnbmF2JykudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcclxuICAgIH0pO1xyXG5cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5pbXBvcnQgRmxpY2tpdHkgZnJvbSAnZmxpY2tpdHknO1xyXG5pbXBvcnQgJ2ZsaWNraXR5L2Rpc3QvZmxpY2tpdHkuY3NzJztcclxuXHJcblZ1ZS5jb21wb25lbnQoJ2Nhcm91c2VsJywge1xyXG4gICAgdGVtcGxhdGU6ICcjY2Fyb3VzZWwnLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICB3cmFwQXJvdW5kOiB7ZGVmYXVsdDogdHJ1ZX0sXHJcbiAgICAgICAgYXV0b1BsYXk6IHtkZWZhdWx0OiBmYWxzZX1cclxuICAgIH0sXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIG5ldyBGbGlja2l0eSh0aGlzLiRlbCwge1xyXG4gICAgICAgICAgICB3cmFwQXJvdW5kOiB0aGlzLndyYXBBcm91bmQsXHJcbiAgICAgICAgICAgIGF1dG9QbGF5OiB0aGlzLmF1dG9QbGF5LFxyXG4gICAgICAgICAgICBjZWxsQWxpZ246ICdsZWZ0JyxcclxuICAgICAgICAgICAgY29udGFpbjogdHJ1ZVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdkZWxldGUtbGluaycsIHtcclxuICAgIHRlbXBsYXRlOiBgPGEgaHJlZj1cIlwiIGNsYXNzPVwiZHJvcGRvd24taXRlbSB0ZXh0LWRhbmdlclwiIEBjbGljay5wcmV2ZW50PVwiZGVsZXRlQWN0aW9uXCI+PGkgOmNsYXNzPVwiaWNvbkNsYXNzXCI+PC9pPiA8c2xvdD48L3Nsb3Q+PC9hPmAsXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIHRhcmdldFVybDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfSxcclxuICAgICAgICByZWRpcmVjdFVybDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjb25maXJtYXRpb25NZXNzYWdlOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnUGxlYXNlLCBjb25maXJtIHlvdSB3YW50IHRvIGRlbGV0ZSB0aGlzIGl0ZW0hJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaWNvbkNsYXNzOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnZmEgZmEtdHJhc2gtbydcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGRlbGV0ZUFjdGlvbigpIHtcclxuICAgICAgICAgICAgbGV0IF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgIGlmKGNvbmZpcm0odGhpcy5jb25maXJtYXRpb25NZXNzYWdlKSl7XHJcbiAgICAgICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgICAgIG1ldGhvZDogJ0RFTEVURScsXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsOiB0aGlzLnRhcmdldFVybCxcclxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVwbGFjZShfc2VsZi5yZWRpcmVjdFVybCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBlcnJvcihlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZXBsYWNlKF9zZWxmLnJlZGlyZWN0VXJsKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgnZGVsZXRlLW1vZHVsZS1pdGVtJywge1xyXG4gICAgdGVtcGxhdGU6IGAgIDxidXR0b24gdHlwZT1cInN1Ym1pdFwiIEBjbGljaz1cImRlbGV0ZUFjdGlvbigkZXZlbnQpXCIgOmZvcm09XCJmb3JtSWRcIiBjbGFzcz1cImJ0biBidG4tbGluayBidG4tbGdcIj48c3BhblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImZhIGZhLW1pbnVzLWNpcmNsZVwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+YCxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgbW9kdWxlSXRlbUlkOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmdcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNvbmZpcm1hdGlvbk1lc3NhZ2U6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICdQbGVhc2UsIGNvbmZpcm0geW91IHdhbnQgdG8gZGVsZXRlIHRoaXMgbW9kdWxlISdcclxuICAgICAgICB9LFxyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgZm9ybUlkKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gXCJyZW1vdmVfcGFnZV9pdGVtX1wiICsgdGhpcy5tb2R1bGVJdGVtSWQ7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBkZWxldGVBY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICAgICAgaWYoIWNvbmZpcm0odGhpcy5jb25maXJtYXRpb25NZXNzYWdlKSl7XHJcbiAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgnZmlsZS11cGxvYWQtYnV0dG9uJywge1xyXG4gICAgbmFtZTogXCJGaWxlVXBsb2FkQnV0dG9uXCIsXHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICB0ZW1wbGF0ZTogJyNmaWxlLXVwbG9hZC1idXR0b24nLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICB1cGxvYWRVcmw6IHtcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgcmVkaXJlY3RVcmw6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYnV0dG9uVGV4dDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgZGVmYXVsdDogJ1VwbG9hZCdcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZ1bGxXaWR0aDoge1xyXG4gICAgICAgICAgICB0eXBlOiBCb29sZWFuLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IGZhbHNlXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHVwbG9hZGluZzogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgaXNVcGxvYWRpbmcoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnVwbG9hZGluZyA9PT0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBzZWxlY3RGaWxlKCkge1xyXG4gICAgICAgICAgICB0aGlzLiRyZWZzLmZpbGVJbnB1dC5jbGljaygpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIHVwbG9hZEZpbGUoKSB7XHJcbiAgICAgICAgICAgIGxldCBmaWxlID0gdGhpcy4kcmVmcy5maWxlSW5wdXQuZmlsZXNbMF0sXHJcbiAgICAgICAgICAgICAgICBfc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgICAgICBpZiAoIWZpbGUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy51cGxvYWRpbmcgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgbGV0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLmFwcGVuZChcImZpbGVcIiwgZmlsZSk7XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAncG9zdCcsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHRoaXMudXBsb2FkVXJsLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogZm9ybURhdGEsXHJcbiAgICAgICAgICAgICAgICBwcm9jZXNzRGF0YTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBjb250ZW50VHlwZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIF9zZWxmLnVwbG9hZGluZyA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVwbGFjZShfc2VsZi5yZWRpcmVjdFVybCk7Y29uc29sZS5sb2coX3NlbGYucmVkaXJlY3RVcmwpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYudXBsb2FkaW5nID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGZsYXNoKGVycm9yWydyZXNwb25zZUpTT04nXS5kZXRhaWwsICdlcnJvcicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuaW1wb3J0IHZ1ZTJEcm9wem9uZSBmcm9tICd2dWUyLWRyb3B6b25lJ1xyXG5pbXBvcnQgJ3Z1ZTItZHJvcHpvbmUvZGlzdC92dWUyRHJvcHpvbmUubWluLmNzcydcclxuXHJcblZ1ZS5jb21wb25lbnQoJ2ZpbGVzLWxpc3QnLCB7XHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG48ZGl2PlxyXG4gICAgPHZ1ZS1kcm9wem9uZSByZWY9XCJjdXN0b21Ecm9wem9uZVwiIGlkPVwiZHJvcHpvbmVcIiA6b3B0aW9ucz1cImRyb3B6b25lT3B0aW9uc1wiIDp1c2VDdXN0b21TbG90PXRydWU+PHNsb3Q+PC9zbG90PjwvdnVlLWRyb3B6b25lPlxyXG4gICAgXHJcbiAgICA8dWwgY2xhc3M9XCJsaXN0LWdyb3VwIG10LTJcIj5cclxuICAgICAgICA8bGkgY2xhc3M9XCJsaXN0LWdyb3VwLWl0ZW0gZC1mbGV4IGp1c3RpZnktY29udGVudC1iZXR3ZWVuXCIgdi1mb3I9XCJpbWFnZSBpbiBpbWFnZXNcIiA6a2V5PVwiaW1hZ2UuaWRcIj5cclxuICAgICAgICAgICAgPGEgOmhyZWY9XCJpbWFnZS5wdWJsaWNQYXRoXCIgdGFyZ2V0PVwiX2JsYW5rXCI+W1sgaW1hZ2Uub3JpZ2luYWxOYW1lIF1dPC9hPlxyXG4gICAgICAgICAgICA8c3BhbiBzdHlsZT1cImZvbnQtc2l6ZTogMS4yZW1cIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIHYtaWY9XCJkZWxldGVkRmlsZUluZGV4ID09PSBpbWFnZS5pZFwiIGNsYXNzPVwiYmFkZ2UgYmFkZ2Utc2Vjb25kYXJ5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgQXJlIHlvdSBzdXJlP1xyXG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1kYW5nZXJcIiBzdHlsZT1cImN1cnNvcjpwb2ludGVyO1wiIEBjbGljaz1cImRlbGV0ZUltYWdlKGltYWdlLmlkKVwiPlllczwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtbGlnaHRcIiBzdHlsZT1cImN1cnNvcjogcG9pbnRlcjtcIiBAY2xpY2s9XCJkZWxldGVkRmlsZUluZGV4ID0gLTFcIj5Obzwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiZmEgZmEtdHJhc2gtbyB0ZXh0LWRhbmdlclwiIHN0eWxlPVwiY3Vyc29yOiBwb2ludGVyXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgZGF0YS1wbGFjZW1lbnQ9XCJ0b3BcIiB0aXRsZT1cIkRlbGV0ZSB0aGUgaW1hZ2VcIiBAY2xpY2s9XCJkZWxldGVkRmlsZUluZGV4ID0gaW1hZ2UuaWRcIj48L3NwYW4+XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICA8L2xpPlxyXG4gICAgPC91bD4gXHJcbjwvZGl2PlxyXG5gLFxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAgIHZ1ZURyb3B6b25lOiB2dWUyRHJvcHpvbmVcclxuICAgIH0sXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIGNyZWF0ZVVybDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBpbmRleFVybDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBkZWxldGVVcmw6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdGh1bWJuYWlsV2lkdGg6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBOdW1iZXIsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IDE1MFxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgIHRoaXMuZ2V0SW1hZ2VzKCk7XHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBkcm9wem9uZU9wdGlvbnM6IHtcclxuICAgICAgICAgICAgICAgIHVybDogdGhpcy5jcmVhdGVVcmwsXHJcbiAgICAgICAgICAgICAgICB0aHVtYm5haWxXaWR0aDogdGhpcy50aHVtYm5haWxXaWR0aCxcclxuICAgICAgICAgICAgICAgIGFkZFJlbW92ZUxpbmtzOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgcGFyYW1OYW1lOiAnZmlsZScsXHJcbiAgICAgICAgICAgICAgICBpbml0KCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub24oJ2Vycm9yJywgZnVuY3Rpb24oZmlsZSwgZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5kZXRhaWwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZsYXNoKGRhdGEuZGV0YWlsLCAnZXJyb3InKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9uKCdzdWNjZXNzJywgZnVuY3Rpb24oZmlsZSwgZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBfc2VsZi5pbWFnZXMucHVzaChkYXRhKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZsYXNoKCdJbWFnZSB3YXMgdXBsb2FkZWQuJywgJ3N1Y2Nlc3MnKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgaW1hZ2VzOiBbXSxcclxuICAgICAgICAgICAgc3RhdGU6ICdpbml0aWFsJyxcclxuICAgICAgICAgICAgZGVsZXRlZEZpbGVJbmRleDogLTFcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIGdldEltYWdlcygpIHtcclxuICAgICAgICAgICAgbGV0IF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxyXG4gICAgICAgICAgICAgICAgdXJsOiBfc2VsZi5pbmRleFVybCxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi5pbWFnZXMgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcihlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBkZWxldGVJbWFnZShpZCkge1xyXG4gICAgICAgICAgICBsZXQgX3NlbGYgPSB0aGlzLFxyXG4gICAgICAgICAgICAgICAgZGF0YSA9IHtpbWFnZV9pZDogaWR9O1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ0RFTEVURScsXHJcbiAgICAgICAgICAgICAgICB1cmw6IF9zZWxmLmRlbGV0ZVVybCxcclxuICAgICAgICAgICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBpbWFnZUluZGV4ID0gX3NlbGYuaW1hZ2VzLmZpbmRJbmRleChpdGVtID0+IGl0ZW0uaWQgPT09IGlkKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuaW1hZ2VzLnNwbGljZShpbWFnZUluZGV4LCAxKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZmxhc2goJ0ltYWdlIHdhcyBkZWxldGVkJywgJ3N1Y2Nlc3MnKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvcihlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdmbGFzaCcsIHtcclxuICAgIHRlbXBsYXRlOiAnI2ZsYXNoJyxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgJ21lc3NhZ2UnOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmdcclxuICAgICAgICB9LFxyXG4gICAgICAgICdsZXZlbCc6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICdzdWNjZXNzJ1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIG1lc3NhZ2VDbGFzczogJ3N1Y2Nlc3MnLFxyXG4gICAgICAgICAgICBzaG93OiBmYWxzZSxcclxuICAgICAgICAgICAgaGFzQmVlblNob3duOiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIHRoaXMubWVzc2FnZUNsYXNzID0gdGhpcy5nZXRNZXNzYWdlQ2xhc3ModGhpcy5sZXZlbCk7XHJcblxyXG4gICAgICAgIHRoaXMuZmxhc2goKTtcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgZmxhc2goKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmhhc0JlZW5TaG93bikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VDbGFzcyA9IHRoaXMuZ2V0TWVzc2FnZUNsYXNzKHRoaXMubGV2ZWwpO1xyXG4gICAgICAgICAgICB0aGlzLnNob3cgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmhpZGUoKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBoaWRlKCkge1xyXG4gICAgICAgICAgICB0aGlzLmhhc0JlZW5TaG93biA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hvdyA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9LCA1MDAwKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBnZXRNZXNzYWdlQ2xhc3MobGV2ZWwpIHtcclxuICAgICAgICAgICAgaWYgKGxldmVsID09PSAnZXJyb3InKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ2Rhbmdlcic7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ3N1Y2Nlc3MnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdmbGFzaGVzJywge1xyXG4gICAgdGVtcGxhdGU6ICcjZmxhc2hlcycsXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgICdmbGFzaGVzJzoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgZGVmYXVsdDogbnVsbFxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIG1lc3NhZ2VzOiBbXVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIGxldCBmbGFzaERhdGEgPSBKU09OLnBhcnNlKHRoaXMuZmxhc2hlcyk7XHJcblxyXG4gICAgICAgIGZvciAobGV0IGtleSBpbiBmbGFzaERhdGEpIHtcclxuICAgICAgICAgICAgZmxhc2hEYXRhW2tleV0uZm9yRWFjaChtZXNzYWdlID0+IHRoaXMubWVzc2FnZXMucHVzaCh7bWVzc2FnZTogbWVzc2FnZSwgbGV2ZWw6IGtleX0pKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHdpbmRvdy5FdmVudE1hbmFnZXIubGlzdGVuKFxyXG4gICAgICAgICAgICAnZmxhc2gnLCBtZXNzYWdlRGF0YSA9PiB0aGlzLmFkZE1lc3NhZ2UobWVzc2FnZURhdGEpXHJcbiAgICAgICAgKTtcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgYWRkTWVzc2FnZShtZXNzYWdlRGF0YSkge1xyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VzLnB1c2gobWVzc2FnZURhdGEpXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdpbmxpbmUtYm9vbGVhbi1lZGl0Jywge1xyXG4gICAgdGVtcGxhdGU6IGBcclxuPGRpdiBjbGFzcz1cImZvcm0tY2hlY2tcIj5cclxuICAgIDxpbnB1dCBjbGFzcz1cImZvcm0tY2hlY2staW5wdXRcIiB0eXBlPVwiY2hlY2tib3hcIiBAY2hhbmdlPVwidXBkYXRlXCIgdi1tb2RlbD1cInZhbHVlXCI+XHJcbjwvZGl2PlxyXG5gLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICB1cmw6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbWV0aG9kOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnUE9TVCdcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldHRpbmdWYWx1ZToge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgc3RhdGU6ICdkaXNwbGF5JyxcclxuICAgICAgICAgICAgdmFsdWU6ICcnXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMuc2V0dGluZ1ZhbHVlID09PSAnMSc7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICAgICAgbGV0IF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6IHRoaXMubWV0aG9kLFxyXG4gICAgICAgICAgICAgICAgdXJsOiB0aGlzLnVybCxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHt2YWx1ZTogdGhpcy52YWx1ZX0sXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZsYXNoKCdVcGRhdGVkLicsICdzdWNjZXNzJyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IoZXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBfc2VsZi52YWx1ZSA9IF9zZWxmLnNldHRpbmdWYWx1ZSA9PT0gJzEnO1xyXG4gICAgICAgICAgICAgICAgICAgIGZsYXNoKGVycm9yWydyZXNwb25zZUpTT04nXS5kZXRhaWwsICdlcnJvcicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuXHJcblZ1ZS5jb21wb25lbnQoJ2lubGluZS12YWx1ZS1lZGl0Jywge1xyXG4gICAgdGVtcGxhdGU6IGBcclxuPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgIDx0ZXh0YXJlYSByb3dzPVwiM1wiIHYtc2hvdz1cInN0YXRlID09PSAnZWRpdGluZydcIiB2LW1vZGVsPVwidmFsdWVcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHJlZj1cImVkaXRGaWVsZFwiPlxyXG4gICAgPC90ZXh0YXJlYT5cclxuICAgIDxwIHYtaWY9XCJzdGF0ZSA9PT0gJ2Rpc3BsYXknXCIgdi1odG1sPVwidmFsdWVcIiBjbGFzcz1cImQtaW5saW5lLWJsb2NrXCI+PC9wPlxyXG4gICAgPGkgdi1pZj1cInN0YXRlID09PSAnZGlzcGxheSdcIiBjbGFzcz1cImZhIGZhLXBlbmNpbC1zcXVhcmUtb1wiIEBjbGljaz1cInN0YXRlID0gJ2VkaXRpbmcnXCIgc3R5bGU9XCJjdXJzb3I6IHBvaW50ZXI7IGZvbnQtc2l6ZTogMS41ZW1cIj48L2k+XHJcbiAgICA8aSB2LWlmPVwic3RhdGUgPT09ICdlZGl0aW5nJ1wiIGNsYXNzPVwiZmEgZmEtc2F2ZVwiIEBjbGljaz1cInNhdmVcIiBzdHlsZT1cImN1cnNvcjogcG9pbnRlcjsgZm9udC1zaXplOiAxLjVlbVwiPjwvaT5cclxuICAgIDxpIHYtaWY9XCJzdGF0ZSA9PT0gJ2VkaXRpbmcnXCIgY2xhc3M9XCJmYSBmYS13aW5kb3ctY2xvc2VcIiBAY2xpY2s9XCJzdGF0ZSA9ICdkaXNwbGF5J1wiIHN0eWxlPVwiY3Vyc29yOiBwb2ludGVyOyBmb250LXNpemU6IDEuNWVtXCI+PC9pPlxyXG48L2Rpdj5cclxuYCxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgdXJsOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmdcclxuICAgICAgICB9LFxyXG4gICAgICAgIG1ldGhvZDoge1xyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgZGVmYXVsdDogJ1BPU1QnXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZXR0aW5nVmFsdWU6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICcnXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgc3RhdGU6ICdkaXNwbGF5JyxcclxuICAgICAgICAgICAgdmFsdWU6ICcnXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMuc2V0dGluZ1ZhbHVlO1xyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBzYXZlKCkge1xyXG4gICAgICAgICAgICBsZXQgX3NlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogdGhpcy5tZXRob2QsXHJcbiAgICAgICAgICAgICAgICB1cmw6IHRoaXMudXJsLFxyXG4gICAgICAgICAgICAgICAgZGF0YToge3ZhbHVlOiB0aGlzLnZhbHVlfSxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3MoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuc3RhdGUgPSAnZGlzcGxheSc7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxhc2goJ1VwZGF0ZSB3YXMgc3VjY2Vzc2Z1bC4nLCAnc3VjY2VzcycpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYuc3RhdGUgPSAnZGlzcGxheSc7XHJcbiAgICAgICAgICAgICAgICAgICAgX3NlbGYudmFsdWUgPSBfc2VsZi5zZXR0aW5nVmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxhc2goZXJyb3JbJ3Jlc3BvbnNlSlNPTiddLmRldGFpbCwgJ2Vycm9yJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAgIHN0YXRlOiBmdW5jdGlvbihuZXdTdGF0ZSkge1xyXG4gICAgICAgICAgICBpZiAobmV3U3RhdGUgPT09ICdlZGl0aW5nJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kbmV4dFRpY2soKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJHJlZnMuZWRpdEZpZWxkLmZvY3VzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kcmVmcy5lZGl0RmllbGQuc2VsZWN0KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgnbW9kYWwnLCB7XHJcbiAgICB0ZW1wbGF0ZTogJyNtb2RhbCcsXHJcbiAgICBkZWxpbWl0ZXJzOiBbJ1tbJywgJ11dJ10sXHJcbiAgICBwcm9wczoge1xyXG4gICAgICAgIHRpdGxlOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnRGVmYXVsdCBtb2RhbCB0aXRsZSdcclxuICAgICAgICB9LFxyXG4gICAgICAgIHByaW1hcnlCdXR0b246IHtcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICdTYXZlJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaWRlbnRpZmllcjoge1xyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzaXplOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBudWxsXHJcbiAgICAgICAgfSxcclxuICAgICAgICB2ZXJ0aWNhbGx5Q2VudGVyZWQ6IHtcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaGFzRm9vdGVyOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgZGVmYXVsdDogdHJ1ZVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIHNpemVDbGFzcygpIHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnNpemUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGBtb2RhbC0ke3RoaXMuc2l6ZX1gO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdmVydGljYWxseUNlbnRlcmVkQ2xhc3MoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnZlcnRpY2FsbHlDZW50ZXJlZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdtb2RhbC1kaWFsb2ctY2VudGVyZWQnO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCJpbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdwb3N0LWxpbmsnLCB7XHJcbiAgICB0ZW1wbGF0ZTogYDxhIGhyZWY9XCJcIiA6Y2xhc3M9XCJsaW5rQ2xhc3NcIiBAY2xpY2sucHJldmVudD1cInBvc3RBY3Rpb25cIj48aSA6Y2xhc3M9XCJpY29uQ2xhc3NcIj48L2k+IDxzbG90Pjwvc2xvdD48L2E+YCxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgdGFyZ2V0VXJsOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmdcclxuICAgICAgICB9LFxyXG4gICAgICAgIHJlZGlyZWN0VXJsOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmdcclxuICAgICAgICB9LFxyXG4gICAgICAgIG5lZWRzQ29uZmlybWF0aW9uOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgZGVmYXVsdDogZmFsc2VcclxuICAgICAgICB9LFxyXG4gICAgICAgIGNvbmZpcm1hdGlvbk1lc3NhZ2U6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICdQbGVhc2UsIGNvbmZpcm0gdGhlIGFjdGlvbi4nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBpY29uQ2xhc3M6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICcnXHJcbiAgICAgICAgfSxcclxuICAgICAgICBsaW5rQ2xhc3M6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6ICdkcm9wZG93bi1pdGVtJ1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgcG9zdEFjdGlvbigpIHtcclxuICAgICAgICAgICAgbGV0IF9zZWxmID0gdGhpcztcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLm5lZWRzQ29uZmlybWF0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoY29uZmlybSh0aGlzLmNvbmZpcm1hdGlvbk1lc3NhZ2UpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybDogdGhpcy50YXJnZXRVcmwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVwbGFjZShfc2VsZi5yZWRpcmVjdFVybCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXJyb3JbJ3Jlc3BvbnNlSlNPTiddWydlcnJvciddKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmxhc2goZXJyb3JbJ3Jlc3BvbnNlSlNPTiddWydlcnJvciddLCAnZXJyb3InKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICAgICAgICAgICAgICB1cmw6IHRoaXMudGFyZ2V0VXJsLFxyXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZXBsYWNlKF9zZWxmLnJlZGlyZWN0VXJsKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlcnJvclsncmVzcG9uc2VKU09OJ11bJ2Vycm9yJ10pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZsYXNoKGVycm9yWydyZXNwb25zZUpTT04nXVsnZXJyb3InXSwgJ2Vycm9yJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5pbXBvcnQgU2xpZGVyIGZyb20gJ3NsaWNrLWNhcm91c2VsJztcclxuaW1wb3J0ICdzbGljay1jYXJvdXNlbC9zbGljay9zbGljay5jc3MnO1xyXG5pbXBvcnQgJ3NsaWNrLWNhcm91c2VsL3NsaWNrL3NsaWNrLXRoZW1lLmNzcyc7XHJcblxyXG5WdWUuY29tcG9uZW50KCdzbGlkZXInLCB7XHJcbiAgICB0ZW1wbGF0ZTogYDxkaXYgY2xhc3M9XCJjcmVkaXQtaXRlbVwiPjxzbG90Pjwvc2xvdD48L2Rpdj5gLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBpbmZpbml0ZToge1xyXG4gICAgICAgICAgICBkZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgICAgICB0eXBlOiBCb29sZWFuLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2VcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzoge1xyXG4gICAgICAgICAgICBkZWZhdWx0OiAzLFxyXG4gICAgICAgICAgICB0eXBlOiBOdW1iZXIsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IHtcclxuICAgICAgICAgICAgZGVmYXVsdDogMyxcclxuICAgICAgICAgICAgdHlwZTogTnVtYmVyLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2VcclxuICAgICAgICB9LFxyXG4gICAgICAgIGRvdHM6IHtcclxuICAgICAgICAgICAgZGVmYXVsdDogZmFsc2UsXHJcbiAgICAgICAgICAgIHR5cGU6IEJvb2xlYW4sXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY2VudGVyUGFkZGluZzoge1xyXG4gICAgICAgICAgICBkZWZhdWx0OiAnMTVweCcsXHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgIH0sXHJcbiAgICBtb3VudGVkKCkge1xyXG4gICAgICAgICQodGhpcy4kZWwpLnNsaWNrKHtcclxuICAgICAgICAgICAgLy8gbm9ybWFsIG9wdGlvbnMuLi5cclxuICAgICAgICAgICAgaW5maW5pdGU6IHRoaXMuaW5maW5pdGUsXHJcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogdGhpcy5zbGlkZXNUb1Nob3csXHJcbiAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiB0aGlzLnNsaWRlc1RvU2Nyb2xsLFxyXG4gICAgICAgICAgICBkb3RzOiB0aGlzLmRvdHMsXHJcbiAgICAgICAgICAgIGNlbnRlclBhZGRpbmc6IHRoaXMuY2VudGVyUGFkZGluZyxcclxuXHJcbiAgICAgICAgICAgIC8vIHRoZSBtYWdpY1xyXG4gICAgICAgICAgICByZXNwb25zaXZlOiBbe1xyXG5cclxuICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDEwMjQsXHJcbiAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMixcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMixcclxuICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBjZW50ZXJQYWRkaW5nOiAnMTVweCcsXHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICBicmVha3BvaW50OiA3NjgsXHJcbiAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcclxuICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgICAgICAgICAgICAgICBjZW50ZXJQYWRkaW5nOiAnMTVweCcsXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDIwMCxcclxuICAgICAgICAgICAgICAgIHNldHRpbmdzOiBcInVuc2xpY2tcIiAvLyBkZXN0cm95cyBzbGlja1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufSk7IiwiaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5cclxuVnVlLmNvbXBvbmVudCgndGFiJywge1xyXG4gICAgdGVtcGxhdGU6ICcjdGFiJyxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgbmFtZTogeyByZXF1aXJlZDogdHJ1ZSB9LFxyXG4gICAgICAgIHNlbGVjdGVkOiB7IGRlZmF1bHQ6IGZhbHNlIH0sXHJcbiAgICAgICAgcGFyZW50OiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBudWxsXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGlzQWN0aXZlOiBmYWxzZSxcclxuICAgICAgICAgICAgaXNDaGlsZEFjdGl2ZTogZmFsc2VcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGhyZWYoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnIycgKyB0aGlzLm5hbWUudG9Mb3dlckNhc2UoKS5yZXBsYWNlKC8gL2csICctJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICAgIEV2ZW50TWFuYWdlci5saXN0ZW4oJ2NoaWxkVGFiQWN0aXZlJywgdGhpcy5hY3RpdmF0ZSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgICAgbGV0IGN1cnJlbnRVcmwgPSB3aW5kb3cubG9jYXRpb24uaHJlZixcclxuICAgICAgICAgICAgcGFydHMgPSBjdXJyZW50VXJsLnNwbGl0KCcjJyksXHJcbiAgICAgICAgICAgIGZyYWdtZW50ID0gbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKHBhcnRzLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgZnJhZ21lbnQgPSBkZWNvZGVVUkkocGFydHNbcGFydHMubGVuZ3RoIC0gMV0pLnRvTG93ZXJDYXNlKCkucmVwbGFjZSgvIC9nLCAnLScpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCF0aGlzLmlzQ2hpbGRBY3RpdmUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNBY3RpdmUgPSAoJyMnICsgZnJhZ21lbnQpID09PSB0aGlzLmhyZWY7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlzQWN0aXZlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICBFdmVudE1hbmFnZXIuZmlyZSgnY2hpbGRUYWJBY3RpdmUnLCB0aGlzLnBhcmVudCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmlzQWN0aXZlID0gdGhpcy5zZWxlY3RlZDtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBhY3RpdmF0ZShwYXJlbnQpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMubmFtZSA9PT0gcGFyZW50KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzQ2hpbGRBY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pc0FjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyIsImltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuXHJcblZ1ZS5jb21wb25lbnQoJ3RhYnMnLCB7XHJcbiAgICB0ZW1wbGF0ZTogJyN0YWJzJyxcclxuICAgIGRlbGltaXRlcnM6IFsnW1snLCAnXV0nXSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHsgdGFiczogW10gfTtcclxuICAgIH0sXHJcblxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgICAgICB0aGlzLnRhYnMgPSB0aGlzLiRjaGlsZHJlbjtcclxuICAgIH0sXHJcblxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHNlbGVjdFRhYihzZWxlY3RlZFRhYikge1xyXG4gICAgICAgICAgICB0aGlzLnRhYnMuZm9yRWFjaCh0YWIgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGFiLmlzQWN0aXZlID0gKHRhYi5ocmVmID09PSBzZWxlY3RlZFRhYi5ocmVmKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTsiLCIvL1RPRE8gYXBpIGtleVxyXG5jb25zdCBBUElfS0VZID0gJ0FJemFTeUNWR3R1UDZWRkFRdGViV1JzNDRIa3RjYm5HRGdQZG9tTSc7XHJcbmNvbnN0IENBTExCQUNLX05BTUUgPSAnZ21hcHNDYWxsYmFjayc7XHJcblxyXG5sZXQgaW5pdGlhbGl6ZWQgPSAhIXdpbmRvdy5nb29nbGU7XHJcbmxldCByZXNvbHZlSW5pdFByb21pc2U7XHJcbmxldCByZWplY3RJbml0UHJvbWlzZTtcclxuLy8gVGhpcyBwcm9taXNlIGhhbmRsZXMgdGhlIGluaXRpYWxpemF0aW9uXHJcbi8vIHN0YXR1cyBvZiB0aGUgZ29vZ2xlIG1hcHMgc2NyaXB0LlxyXG5jb25zdCBpbml0UHJvbWlzZSA9IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgIHJlc29sdmVJbml0UHJvbWlzZSA9IHJlc29sdmU7XHJcbiAgICByZWplY3RJbml0UHJvbWlzZSA9IHJlamVjdDtcclxufSk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBpbml0KCkge1xyXG4gICAgLy8gSWYgR29vZ2xlIE1hcHMgYWxyZWFkeSBpcyBpbml0aWFsaXplZFxyXG4gICAgLy8gdGhlIGBpbml0UHJvbWlzZWAgc2hvdWxkIGdldCByZXNvbHZlZFxyXG4gICAgLy8gZXZlbnR1YWxseS5cclxuICAgIGlmIChpbml0aWFsaXplZCkgcmV0dXJuIGluaXRQcm9taXNlO1xyXG5cclxuICAgIGluaXRpYWxpemVkID0gdHJ1ZTtcclxuICAgIC8vIFRoZSBjYWxsYmFjayBmdW5jdGlvbiBpcyBjYWxsZWQgYnlcclxuICAgIC8vIHRoZSBHb29nbGUgTWFwcyBzY3JpcHQgaWYgaXQgaXNcclxuICAgIC8vIHN1Y2Nlc3NmdWxseSBsb2FkZWQuXHJcbiAgICB3aW5kb3dbQ0FMTEJBQ0tfTkFNRV0gPSAoKSA9PiByZXNvbHZlSW5pdFByb21pc2Uod2luZG93Lmdvb2dsZSk7XHJcblxyXG4gICAgLy8gV2UgaW5qZWN0IGEgbmV3IHNjcmlwdCB0YWcgaW50b1xyXG4gICAgLy8gdGhlIGA8aGVhZD5gIG9mIG91ciBIVE1MIHRvIGxvYWRcclxuICAgIC8vIHRoZSBHb29nbGUgTWFwcyBzY3JpcHQuXHJcbiAgICBjb25zdCBzY3JpcHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcclxuICAgIHNjcmlwdC5hc3luYyA9IHRydWU7XHJcbiAgICBzY3JpcHQuZGVmZXIgPSB0cnVlO1xyXG4gICAgc2NyaXB0LnNyYyA9IGBodHRwczovL21hcHMuZ29vZ2xlYXBpcy5jb20vbWFwcy9hcGkvanM/a2V5PSR7QVBJX0tFWX0mY2FsbGJhY2s9JHtDQUxMQkFDS19OQU1FfWA7XHJcbiAgICBzY3JpcHQub25lcnJvciA9IHJlamVjdEluaXRQcm9taXNlO1xyXG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignaGVhZCcpLmFwcGVuZENoaWxkKHNjcmlwdCk7XHJcblxyXG4gICAgcmV0dXJuIGluaXRQcm9taXNlO1xyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==