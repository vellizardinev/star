@echo off
cd /D %~dp0%\..

php bin\console doctrine:database:drop --force
php bin\console doctrine:database:create
php bin\console doctrine:schema:update --force
php bin\console doctrine:fixtures:load --no-interaction
php bin\console mail:update-templates --no-interaction

pause

