#!/usr/bin/env bash
symfony php bin/console doctrine:database:drop --force --env=test
symfony php bin/console doctrine:database:create --env=test
symfony php bin/console doctrine:schema:create --env=test
symfony php bin/console doctrine:fixtures:load --no-interaction --env=test
symfony php bin/console mail:update-templates --no-interaction --env=test

cd features/data

cp test_db.sqlite temp_db.sqlite