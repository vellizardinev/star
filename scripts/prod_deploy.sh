#!/usr/bin/env bash

php bin/console sirius:maintenance on
git pull
php bin/console cache:clear --no-interaction
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console data:update
php bin/console mail:add-templates --no-interaction
php bin/console sirius:translation:download
php bin/console cache:clear --no-interaction
php bin/console sirius:maintenance off