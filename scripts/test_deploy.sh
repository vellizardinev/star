#!/usr/bin/env bash

git reset --hard
/opt/cpanel/ea-php73/root/usr/bin/php -c ~/php73_cli.ini bin/console sirius:maintenance on
git pull
/opt/cpanel/ea-php73/root/usr/bin/php -c ~/php73_cli.ini bin/console doctrine:migrations:migrate --no-interaction
/opt/cpanel/ea-php73/root/usr/bin/php -c ~/php73_cli.ini bin/console data:update
/opt/cpanel/ea-php73/root/usr/bin/php -c ~/php73_cli.ini bin/console mail:add-templates --no-interaction
/opt/cpanel/ea-php73/root/usr/bin/php -c ~/php73_cli.ini bin/console sirius:translation:download
/opt/cpanel/ea-php73/root/usr/bin/php -c ~/php73_cli.ini bin/console cache:clear --no-interaction
/opt/cpanel/ea-php73/root/usr/bin/php -c ~/php73_cli.ini bin/console sirius:maintenance off